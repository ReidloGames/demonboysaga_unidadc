﻿# TODO: Translation updated at 2021-07-08 13:36

# game/Code/Eventos/openingTuto/openingTuto.rpy:6
translate english openingTuto_8111b466:

    # narrador "Antes de empezar, este juego es de elección libre.(Sandbox)"
    narrador "Before you start, this game is completely free choice (Sandbox)."

# game/Code/Eventos/openingTuto/openingTuto.rpy:8
translate english openingTuto_0fd9dcd9:

    # narrador "Dispones de una barra de tiempo con sus días y horas correspondientes."
    narrador "You have a time bar with corresponding days and hours."

# game/Code/Eventos/openingTuto/openingTuto.rpy:10
translate english openingTuto_ad5aa7b6:

    # narrador "Podrás desplazarte libremente y escoger la ruta deseada."
    narrador "You will be able to move freely and choose the desired route."

# game/Code/Eventos/openingTuto/openingTuto.rpy:12
translate english openingTuto_cdc85211:

    # narrador "Tiene un sistema de inventario donde podrás recoger o comprar objetos siempre que dispongas de dinero."
    narrador "It has an inventory system where you will be able to collect or buy items as long as you have money."

# game/Code/Eventos/openingTuto/openingTuto.rpy:14
translate english openingTuto_c58b8c35:

    # narrador "Durante el juego habrá avisos como estos para advertirte de cambios en los stats de los personajes."
    narrador "During the game there will be prompts like these to warn you of changes in the characters stats."

# game/Code/Eventos/openingTuto/openingTuto.rpy:20
translate english openingTuto_fae49b4f:

    # narrador "La aventura dispone de animaciones como esta"
    narrador "The adventure has animations like this."

# game/Code/Eventos/openingTuto/openingTuto.rpy:28
translate english openingTuto_5f6873b7:

    # narrador "El juego se encuentra en desarrollo.\nSi te gusta, apoya el proyecto."
    narrador "The game is in development. \nIf you like it, support the project."

# game/Code/Eventos/openingTuto/openingTuto.rpy:31
translate english openingTuto_d596d538:

    # narrador "disfruta del juego"
    narrador "Enjoy the game"
# TODO: Translation updated at 2021-08-27 11:11

# game/Code/Eventos/openingTuto/openingTuto.rpy:6
translate english openingTuto_293df658:

    # "-If you change the language, close the game and run it again.\n\n-Si cambias el idioma, cierra el juego y ejecutalo de nuevo."
    "-If you change the language, close the game and run it again.\n\n-Si cambias el idioma, cierra el juego y ejecutalo de nuevo."

# game/Code/Eventos/openingTuto/openingTuto.rpy:12
translate english openingTuto_7695c17e:

    # narrador "El reloj de arena hara que pase el tiempo. (Ej: de tarde a noche)"
    narrador "The hourglass will make the time pass. (Ex: from afternoon to evening)"

# game/Code/Eventos/openingTuto/openingTuto.rpy:16
translate english openingTuto_07a7e13f:

    # narrador "Para desplazarte tienes que seleccionar una puerta o el final del camino."
    narrador "To scroll you have to select a door or the end of the path."

# game/Code/Eventos/openingTuto/openingTuto.rpy:20
translate english openingTuto_c16239e4:

    # narrador "El HUD mostrara el nombre de la zona seleccionada."
    narrador "The HUD will show the name of the selected zone."

# game/Code/Eventos/openingTuto/openingTuto.rpy:23
translate english openingTuto_f9d6d77d:

    # narrador "Hay zonas que, para volver a la zona anterior hay que darle click a la flecha roja."
    narrador "There are zones that, to go back to the previous zone you have to click on the red arrow."

# game/Code/Eventos/openingTuto/openingTuto.rpy:29
translate english openingTuto_aefc768d:

    # narrador "Este icono muestra donde se encuentran las personas que viven en casa en ese momento."
    narrador "This icon shows where the people living in the house are at that moment."

# game/Code/Eventos/openingTuto/openingTuto.rpy:33
translate english openingTuto_d3ac0c45:

    # narrador "El juego tiene un sistema de inventario donde podrás recoger o comprar objetos siempre que dispongas de dinero."
    narrador "The game has an inventory system where you can pick up or buy items as long as you have money."

# game/Code/Eventos/openingTuto/openingTuto.rpy:36
translate english openingTuto_6d3d7c09:

    # narrador "El juego dispone de un sistema de ayudas para poder avanzar en la aventura"
    narrador "The game has a help system to be able to advance in the adventure."
# TODO: Translation updated at 2021-08-31 20:00

# game/Code/Eventos/openingTuto/openingTuto.rpy:30
translate english openingTuto_022d7b41:

    # narrador "Puedes desplazarte a la zona indicada haciendo click"
    narrador "You can scroll to the indicated area by clicking on"

# game/Code/Eventos/openingTuto/openingTuto.rpy:32
translate english openingTuto_44e92b0b:

    # narrador "En el mapa dispones de un acceso directo para volver a tu habitación"
    narrador "On the map you have a shortcut to return to your room."
