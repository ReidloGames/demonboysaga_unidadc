﻿# TODO: Translation updated at 2021-07-08 13:36

# game/Code/Eventos/openingIntro/openingIntro.rpy:9
translate english openingIntro_48c76bd1:

    # "Esta grabando..."
    "It's recording..."

# game/Code/Eventos/openingIntro/openingIntro.rpy:14
translate english openingIntro_66cf179d:

    # "Ahora si."
    "Now"

# game/Code/Eventos/openingIntro/openingIntro.rpy:16
translate english openingIntro_84d37ae2:

    # "Hola me llamo"
    "Hello, my name is"

# game/Code/Eventos/openingIntro/openingIntro.rpy:23
translate english openingIntro_7701eb82:

    # prota "Y tengo 21 años"
    prota "And I am 21 years old"

# game/Code/Eventos/openingIntro/openingIntro.rpy:25
translate english openingIntro_1e4d320c:

    # prota "Os voy a contar como todo puede dar un giro de 180 grados y cambiarte la vida de la noche a la mañana."
    prota "I am going to tell you how everything can take a 180-degree turn and change your life overnight."

# game/Code/Eventos/openingIntro/openingIntro.rpy:32
translate english openingIntro_80da137d:

    # "!!RIIIIIINNNGGGG!!"
    "!!RIIIIIINNNGGGG!!"

# game/Code/Eventos/openingIntro/openingIntro.rpy:39
translate english openingIntro_dc26c29d:

    # prota "ARGGHH, que pereza..."
    prota "ARGGHH, how lazy..."

# game/Code/Eventos/openingIntro/openingIntro.rpy:40
translate english openingIntro_7713b8a5:

    # prota "Otro dia aburrido mas, en el que no pasara nada interesante y que encima toca levantarse para ir a la universidad... "
    prota "Another boring day, in which nothing interesting will happen and on top of that it is time to get up to go to the university..."

# game/Code/Eventos/openingIntro/openingIntro.rpy:47
translate english historiaOpening_88da4d9f:

    # prota "Hace 5 años mis padres fallecieron en un accidente de tráfico.Pero la familia Spencer, amigos intimos de mis padres difuntos me adoptaron y ahora son mi familia."
    prota "Five years ago my parents passed away in a car accident. But the Spencer family, close friends of my deceased parents adopted me and are now my family."

# game/Code/Eventos/openingIntro/openingIntro.rpy:49
translate english historiaOpening_94f228ae:

    # prota "Después de esto todo iba genial, una gran casa, familia adinerada, sin preocupaciones…"
    prota "After that everything was going great, big house, wealthy family, no worries..."

# game/Code/Eventos/openingIntro/openingIntro.rpy:51
translate english historiaOpening_d41d5b1c:

    # prota "Pero todo cambio. [nombrePad] que es mi [situPad], fue despedido de su trabajo y la familia empezó a desmoronarse."
    prota "But everything changed. [nombrePad], who is my [situPad], was laid off from his job and the family began to fall apart"

# game/Code/Eventos/openingIntro/openingIntro.rpy:53
translate english historiaOpening_402c100d:

    # prota "No se cual fue la razón pero algo raro pasó."
    prota "I don't know what the reason was but something strange happened."

# game/Code/Eventos/openingIntro/openingIntro.rpy:55
translate english historiaOpening_dd95a24f:

    # prota "A partir de ahí vinieron los problemas."
    prota "From there came the problems."

# game/Code/Eventos/openingIntro/openingIntro.rpy:57
translate english historiaOpening_40960964:

    # prota "De tener dinero y ser una familia privilegiada vinieron las deudas y las discusiones familiares."
    prota "From having money and being a privileged family came debts and family arguments."

# game/Code/Eventos/openingIntro/openingIntro.rpy:59
translate english historiaOpening_2bc3c653:

    # prota "[nombreMad] que es mi [situMad] tuvo que buscarse un empleo para tapar agujeros..."
    prota "[nombreMad] who is my [situMad] had to find a job to cover holes...."

translate english strings:

    # game/Code/Eventos/openingIntro/openingIntro.rpy:19
    old "Tu nombre?"
    new "Your name?"
