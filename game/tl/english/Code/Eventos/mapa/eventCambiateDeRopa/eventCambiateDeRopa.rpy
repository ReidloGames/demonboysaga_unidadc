﻿# TODO: Translation updated at 2021-08-27 11:11

# game/Code/Eventos/mapa/eventCambiateDeRopa/eventCambiateDeRopa.rpy:7
translate english eventCambiateDeRopa_021cd70d:

    # protaPensa "No puedo salir con esta ropa."
    protaPensa "I can't go out in these clothes."

# game/Code/Eventos/mapa/eventCambiateDeRopa/eventCambiateDeRopa.rpy:8
translate english eventCambiateDeRopa_8d90f243:

    # protaPensa "Tengo que ir a cambiarme."
    protaPensa "I have to go change."

# game/Code/Eventos/mapa/eventCambiateDeRopa/eventCambiateDeRopa.rpy:12
translate english eventCambiateDeRopa_a131487d:

    # "Unos minutos mas tarde..."
    "A few minutes later..."

# game/Code/Eventos/mapa/eventCambiateDeRopa/eventCambiateDeRopa.rpy:14
translate english eventCambiateDeRopa_6dd14bf2:

    # protaPensa "Ahora sí que ya estoy listo."
    protaPensa "Now I am ready"
