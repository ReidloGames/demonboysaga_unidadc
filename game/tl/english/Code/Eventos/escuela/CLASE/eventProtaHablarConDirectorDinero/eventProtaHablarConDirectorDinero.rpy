﻿# TODO: Translation updated at 2021-08-27 11:11

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:9
translate english eventProtaHablarConDirectorDinero_fa4f2214:

    # alicia "Un momento."
    alicia "One moment."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:11
translate english eventProtaHablarConDirectorDinero_d0ff9b17:

    # alicia "El director quiere verte."
    alicia "The director wants to see you."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:15
translate english eventProtaHablarConDirectorDinero_6900df8f:

    # protaPensa "A mí me quiere ver el director? ¿qué es lo que querrá?"
    protaPensa "The director wants to see me...what does he want?"

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:17
translate english eventProtaHablarConDirectorDinero_d2d7c2b0:

    # alicia "A saber, que has hecho ya..."
    alicia "To know, what have you already done..."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:18
translate english eventProtaHablarConDirectorDinero_2460dd1a:

    # alicia "El director está en su despacho en el segundo piso"
    alicia "The director is in his office on the second floor."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:20
translate english eventProtaHablarConDirectorDinero_c0fbc920:

    # prota "Ok, ahora iré a verle."
    prota "Ok, I'll go see him now."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:32
translate english eventProtaHablarConDirectorDinero_d9b6cc43:

    # "TOC TOC"
    "TOC TOC"

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:34
translate english eventProtaHablarConDirectorDinero_08cec69f:

    # "Unos minutos antes..."
    "A few minutes earlier..."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:40
translate english eventProtaHablarConDirectorDinero_1b4da312:

    # directorUni "Si quieres que limpie todo tu expediente y que apruebe todas tus asignaturas..."
    directorUni "If you want me to clean up your entire record and pass all your subjects..."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:42
translate english eventProtaHablarConDirectorDinero_8ba84ad7:

    # directorUni "Ya sabes lo que tienes que hacer."
    directorUni "You know what you have to do."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:44
translate english eventProtaHablarConDirectorDinero_de3d405a:

    # claire "Por favor... ¿No puede haber otra manera? No quiero hacer esto..."
    claire "Please... Can't there be another way? I don't want to do this..."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:46
translate english eventProtaHablarConDirectorDinero_9b569954:

    # directorUni "Es esto o ya te digo que tu carrera habrá terminado para siempre."
    directorUni "It's either this or I'm telling you, your career will be over forever."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:48
translate english eventProtaHablarConDirectorDinero_ff0ed1ff:

    # directorUni "Arrodíllate y empieza..."
    directorUni "Get down on your knees and start..."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:52
translate english eventProtaHablarConDirectorDinero_43426718:

    # claire "Por favor... no me hagas esto..."
    claire "Please... don't do this to me..."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:53
translate english eventProtaHablarConDirectorDinero_bc136321:

    # directorUni "Sácala y empieza a mamar."
    directorUni "Take it out and start sucking."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:55
translate english eventProtaHablarConDirectorDinero_91dfe82d:

    # claire "yo..."
    claire "I..."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:57
translate english eventProtaHablarConDirectorDinero_09024a28:

    # directorUni "¡¡¡Ahora!!!"
    directorUni "Now!!!!"

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:59
translate english eventProtaHablarConDirectorDinero_1ec30b34:

    # "¡¡¡PAM!!!"
    "PAM!!!!"

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:65
translate english eventProtaHablarConDirectorDinero_91d16b86:

    # narrador "Le quita los pantalones"
    narrador "She takes off his pants"

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:77
translate english eventProtaHablarConDirectorDinero_2eb304ad:

    # clairePensa "Le está creciendo..."
    clairePensa "It's getting bigger..."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:79
translate english eventProtaHablarConDirectorDinero_9e57770a:

    # clairePensa "La tiene bastante grande. Espero que con esto se corra rápido y termine esta pesadilla."
    clairePensa "He's pretty big. I hope with this he cums fast and this nightmare ends."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:117
translate english eventProtaHablarConDirectorDinero2_7d64ff1c:

    # directorUni "Métetela en la boca."
    directorUni "Put it in your mouth."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:119
translate english eventProtaHablarConDirectorDinero2_0759b9c0:

    # claire "¿Qué? Pero pensé que con esto ya era suficiente. Eso es demasiado…"
    claire "What? But I thought this was enough. That's too much..."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:121
translate english eventProtaHablarConDirectorDinero2_af8da784:

    # directorUni "¡Silencio! Aquí las ordenes las doy yo. Esto solo acaba de empezar."
    directorUni "Quiet! I give the orders here. This has only just begun."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:123
translate english eventProtaHablarConDirectorDinero2_abf02ddf:

    # directorUni "O haces lo que te digo o tu futuro será el de fregar suelos."
    directorUni "Either you do as I say or your future will be scrubbing floors."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:124
translate english eventProtaHablarConDirectorDinero2_b792b5f0:

    # directorUni "Abre la boca y empieza a chupar."
    directorUni "Open your mouth and start sucking."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:126
translate english eventProtaHablarConDirectorDinero2_f791ab0b:

    # "sniff..."
    "sniff..."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:130
translate english eventProtaHablarConDirectorDinero2_01eb6be6:

    # clairePensa "Dios mío que estoy haciendo... Tengo que aprobar este curso no me queda otra."
    clairePensa "My God what am I doing.... I have to pass this course I have no choice."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:131
translate english eventProtaHablarConDirectorDinero2_9cf3c774:

    # clairePensa "Sabe a salado."
    clairePensa "It tastes salty."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:141
translate english eventProtaHablarConDirectorDinero2_717e7cc8:

    # directorUni "Así me gusta calladita y obediente. Empieza a moverte y succiona bien."
    directorUni "That's how I like it, quiet and obedient. Start moving and suck it well."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:163
translate english eventProtaHablarConDirectorDinero3_41888473:

    # directorUni "Esta bien pero seguro que puedes hacerlo mejor... Métela más."
    directorUni "That's good but I'm sure you can do better.... Put it in more."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:165
translate english eventProtaHablarConDirectorDinero3_4df732fd:

    # claire "Por favor ya está, hice todo lo que me pediste."
    claire "Please that's it, I did everything you asked me to do."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:167
translate english eventProtaHablarConDirectorDinero3_a7476e7d:

    # directorUni "¡¡Abre la boca bien y métela toda!!"
    directorUni "Open your mouth wide and put it all in!!!"

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:169
translate english eventProtaHablarConDirectorDinero3_d2acd487:

    # claire "Ya no puedo más por favor… "
    claire "I can't take it anymore please..."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:173
translate english eventProtaHablarConDirectorDinero3_365867ef:

    # directorUni "Quita las manos."
    directorUni "Hands off."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:175
translate english eventProtaHablarConDirectorDinero3_c657ce69:

    # claire "Por favor..."
    claire "Please..."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:176
translate english eventProtaHablarConDirectorDinero3_2f4b3dee:

    # directorUni "Quita las manos, no lo volveré a repetir."
    directorUni "Take your hands off, I won't repeat it again."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:205
translate english bucle1MamadaProfundaDirector_9cb3f655:

    # directorUni "Así muy bien.{p=0.5}{nw}"
    directorUni "That's very good. {p=0.5}{nw}"

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:212
translate english bucle1MamadaProfundaDirector_54111852:

    # directorUni "Te voy a rellenar todos los agujeros. Ya veras...{p=1}{nw}"
    directorUni "I'm going to fill all your holes. You'll see...{p=1}{nw}"

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:232
translate english bucle1MamadaProfundaDirector_1858d560:

    # directorUni "Serás mi putita y te hare disfrutar...{p=1}{nw}"
    directorUni "You'll be my little whore and I'll make you enjoy...{p=1}{nw}"

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:263
translate english bucle2MamadaProfundaDirector_2a43292d:

    # directorUni "¿Te gusta cómo te follo la boca? Se que lo estas disfrutando.{p=1.8}{nw}"
    directorUni "Do you like the way I fuck your mouth? I know you're enjoying it.{p=1.8}{nw}"

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:276
translate english bucle2MamadaProfundaDirector_b934ddd0:

    # directorUni "Te dicho que abras bien la boca.{p=1}{nw}"
    directorUni "I told you to open your mouth wide.{p=1}{nw}"

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:289
translate english eventProtaHablarConDirectorDinero4_a8a2df5f:

    # directorUni "Toda adentro. Así me gusta, que te portes bien."
    directorUni "All in. This is how I like it, that you behave yourself."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:291
translate english eventProtaHablarConDirectorDinero4_03a11545:

    # "Toc Toc!!!"
    "Knock Knock!!!"

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:293
translate english eventProtaHablarConDirectorDinero4_d5964cca:

    # directorUni "Joder metete debajo de la mesa y no hagas ruido."
    directorUni "Fuck, get under the table and don't make any noise."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:295
translate english eventProtaHablarConDirectorDinero4_9334fb36:

    # directorUni "Adelante."
    directorUni "Come in."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:299
translate english eventProtaHablarConDirectorDinero4_f1dc61e7:

    # prota "Buenos días."
    prota "Good morning."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:301
translate english eventProtaHablarConDirectorDinero4_407c51d9:

    # protaPensa "Juraría que estaba hablando con alguien..."
    protaPensa "I could have sworn he was talking to someone..."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:303
translate english eventProtaHablarConDirectorDinero4_f6f0a8e6:

    # directorUni "Buenos días. No hace falta que te sientes, será breve."
    directorUni "Good morning. No need to sit down, it will be brief."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:305
translate english eventProtaHablarConDirectorDinero4_1913aa6b:

    # prota "Alicia me ha dicho que querías hablar conmigo."
    prota "Alicia told me you wanted to talk to me."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:307
translate english eventProtaHablarConDirectorDinero4_4349a299:

    # directorUni "Así es..."
    directorUni "That's right..."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:308
translate english eventProtaHablarConDirectorDinero4_9cc86b87:

    # directorUni "Quería hablar contigo porque este mes no hemos recibido el pago de la Universidad."
    directorUni "I wanted to talk to you because we haven't received payment this month."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:310
translate english eventProtaHablarConDirectorDinero4_a3175c81:

    # prota "Pero eso no es posible..."
    prota "But that's not possible..."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:312
translate english eventProtaHablarConDirectorDinero4_ffae8ed4:

    # directorUni "Sabemos que nunca hemos tenido problemas de impago contigo, pero este mes no hemos recibido el ingreso."
    directorUni "We know we've never had any non-payment problems with you, but this month we haven't received it."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:314
translate english eventProtaHablarConDirectorDinero4_eb1c34f4:

    # directorUni "Esta Universidad es de las mejores del país con un personal muy profesional y muy respetado. Solo acude gente importante. Por ahora haremos la vista gorda, pero espero que soluciones el problema lo antes posible, o tomaremos cartas en el asunto."
    directorUni "This University is one of the best in the country with a very professional and highly respected staff. Only important people attend. We will turn a blind eye for now, but I hope you solve the problem as soon as possible, or we will take action."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:316
translate english eventProtaHablarConDirectorDinero4_19878be9:

    # prota "Yo... no sabía nada... Lo hablare en casa."
    prota "I... I... I didn't know anything... I'll talk about it at home."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:318
translate english eventProtaHablarConDirectorDinero4_ba8aea9e:

    # directorUni "Bien, solo era eso. Por favor cierra la puerta al salir."
    directorUni "Well, it was just that. Please close the door on your way out."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:319
translate english eventProtaHablarConDirectorDinero4_d7fe1f2f:

    # directorUni "Gracias."
    directorUni "Thank you."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:321
translate english eventProtaHablarConDirectorDinero4_019015ee:

    # prota "Esta bien, adiós."
    prota "Okay, bye."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:325
translate english eventProtaHablarConDirectorDinero4_3968dfb8:

    # protaPensa "Oh mierda... esto no me está gustando... Tendré que hablar con [nombreMad3] y [nombrePad3]. Espero que solo sea un mal entendido."
    protaPensa "Oh shit... this isn't sitting well with me.... I'll have to talk to [nombreMad3] and [nombrePad3]. I hope it's just a misunderstanding."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:342
translate english eventProtaHablarConDirectorDinero_Part2_23b9c2ae:

    # directorUni "Nos han interrumpido, pero no pasa nada, levántate."
    directorUni "We've been interrupted, but it's okay, get up."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:344
translate english eventProtaHablarConDirectorDinero_Part2_0a8a6373:

    # claire "Yo ya me tengo que ir."
    claire "I have to go now."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:346
translate english eventProtaHablarConDirectorDinero_Part2_6b1d2e13:

    # directorUni "Tranquila, solo un poco mas y ya estara..."
    directorUni "Don't worry, just a little more and it will be..."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:348
translate english eventProtaHablarConDirectorDinero_Part2_e56ed859:

    # directorUni "A ver que tienes por aquí."
    directorUni "Let's see what you've got here."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:350
translate english eventProtaHablarConDirectorDinero_Part2_f64e8e1f:

    # claire "No por favor todo menos eso... ¡¡¡Por favor!!!"
    claire "No please anything but that..... Please!!!"

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:352
translate english eventProtaHablarConDirectorDinero_Part2_ea93aec1:

    # directorUni "¡¡Silencio!! bien que hace un rato me la estabas chupando entera y sin rechistar. Se muy bien que esto te gusta."
    directorUni "Quiet!!! well a while ago you were sucking me off whole and without a whimper. I know very well that you like this."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:354
translate english eventProtaHablarConDirectorDinero_Part2_ee4d563a:

    # claire "Espera no..."
    claire "Wait no..."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:360
translate english eventProtaHablarConDirectorDinero_Part2_41454888:

    # directorUni "Voy a disfrutar esto y quiero que estés calladita o tendrás problemas."
    directorUni "I'm going to enjoy this and I want you to be quiet or you'll be in trouble."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:366
translate english eventProtaHablarConDirectorDinero_Part2_5b5ee127:

    # directorUni "Pero si estas chorreando... Lo estabas deseando."
    directorUni "But you're dripping.... You were looking forward to it."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:379
translate english eventProtaHablarConDirectorDinero_Part2_98005ed6:

    # directorUni "Vienes a clase con esta ropa de puta y encima decías que no querías."
    directorUni "You come to class in these slutty clothes and on top of that you were saying you didn't want to."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:381
translate english eventProtaHablarConDirectorDinero_Part2_e60f3b76:

    # directorUni "Tremenda puta estas hecha... No paras de chorrear y tienes todo el coño abierto."
    directorUni "What a whore you are.... You don't stop squirting and you have your whole pussy open."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:383
translate english eventProtaHablarConDirectorDinero_Part2_970d9a75:

    # claire "Por favor ya no mas..."
    claire "Please no more..."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:385
translate english eventProtaHablarConDirectorDinero_Part2_5d3b7bd2:

    # directorUni "Tu coño no dice lo mismo, mira lo abierto que lo tienes y como gotea..."
    directorUni "Your pussy doesn't say the same, look how open you have it and how it drips..."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:387
translate english eventProtaHablarConDirectorDinero_Part2_73e97ac9:

    # directorUni "¿Y esos gemidos?"
    directorUni "And those moans?"

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:395
translate english eventProtaHablarConDirectorDinero_Part2_ed34556b:

    # directorUni "¿Porque tienes el coño tan abierto y chorreando?"
    directorUni "Why is your pussy so open and dripping?"

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:396
translate english eventProtaHablarConDirectorDinero_Part2_dadbda63:

    # directorUni "hahahaha"
    directorUni "hahahaha"

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:400
translate english eventProtaHablarConDirectorDinero_Part2_aa0be0ee:

    # clairePensa "{size=15}{i}Como siga así voy a perder la conciencia. Este desgraciado sabe lo que hace, me está volviendo loca con lo que hace y no puedo evitarlo...{/size}{/i}"
    clairePensa "{size=15}{i}If he keep this up I'm going to lose my mind. This motherfucker knows what he's doing, he's driving me crazy with what he's doing and I can't help it...{/size}{/i}"

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:402
translate english eventProtaHablarConDirectorDinero_Part2_c3efcb68:

    # directorUni "Prepárate porque ahora te lo abriré más."
    directorUni "Get ready because now I'm going to open it wider."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:403
translate english eventProtaHablarConDirectorDinero_Part2_d3227777:

    # clairePensa "..."
    clairePensa "..."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:405
translate english eventProtaHablarConDirectorDinero_Part2_0517bdac:

    # clairePensa "{size=15}{i}Dios no puedo moverme estoy paralizada, este cerdo me va a follar. No para de frotarme el clítoris y abrirme el coño con mis bragas.{/size}{/i}"
    clairePensa "{size=15}{i}God I can't move I'm paralyzed, this pig is going to fuck me. He keeps rubbing my clit and spreading my pussy open with my panties.{/size}{/i}"

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:407
translate english eventProtaHablarConDirectorDinero_Part2_368a4418:

    # claire "haaaaaa haaay"
    claire "haaaaaaaa haaay"

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:411
translate english eventProtaHablarConDirectorDinero_Part2_15fabb8d:

    # directorUni "Tremenda puta estas hecha, mira cómo te abres de piernas tu sola y te colocas en buena posición. Y tu coño no para de gotear."
    directorUni "what a slut you are, look how you spread your legs all by yourself and get in a good position. And your pussy doesn't stop dripping."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:417
translate english eventProtaHablarConDirectorDinero_Part2_5b0ffe26:

    # directorUni "Te estas corriendo y no dices nada. Te está encantando."
    directorUni "You're cumming and you don't say anything. You're liking it."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:427
translate english eventProtaHablarConDirectorDinero_Part2_3525a2d6:

    # directorUni "Cada vez chorreas más, se acabaron los juegos..."
    directorUni "You're dripping more and more, the games are over..."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:429
translate english eventProtaHablarConDirectorDinero_Part2_3ef9a45a:

    # clairePensa "{size=15}{i}No puedo más, me tiene bajo su control, necesito hacer esto para pasar el semestre.{/size}{/i}"
    clairePensa "{size=15}{i}I can't take it anymore, he's got me under his control, I need to do this to get through the semester.{/size}{/i}"

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:431
translate english eventProtaHablarConDirectorDinero_Part2_0414ccc7:

    # claire "aaaaaaaay"
    claire "aaaaaaaay"

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:435
translate english eventProtaHablarConDirectorDinero_Part2_984753e9:

    # directorUni "Te rellenare todo el coño, solo te falta suplicar por ello."
    directorUni "I'll stuff your whole pussy, all you need to do is beg for it."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:443
translate english eventProtaHablarConDirectorDinero_Part2_13bde2e3:

    # claire "¡¡No!! eso no!!"
    claire "No!!! not that!!!"

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:445
translate english eventProtaHablarConDirectorDinero_Part2_987e5ebc:

    # directorUni "¡¡Calla!! pero si sabes que lo estas suplicando."
    directorUni "Shut up!!! you know you're begging for it."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:446
translate english eventProtaHablarConDirectorDinero_Part2_4afd0d3f:

    # narrador "¡Un momento!"
    narrador "Just a moment!"

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:447
translate english eventProtaHablarConDirectorDinero_Part2_ff26c485:

    # narrador "Siento interrumpir, pero…"
    narrador "Sorry to interrupt, but..."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:448
translate english eventProtaHablarConDirectorDinero_Part2_39adfbb3:

    # narrador "Estas jugando al juego Demon Boy Saga, no al Demon Director Saga."
    narrador "You're playing Demon Boy Saga, not Demon Director Saga."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:449
translate english eventProtaHablarConDirectorDinero_Part2_3be1a51b:

    # narrador "Ya hemos visto demasiado del director..."
    narrador "We've already seen too much of the director..."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:450
translate english eventProtaHablarConDirectorDinero_Part2_13ab73d7:

    # narrador "Volvamos donde dejamos la aventura de nuestro protagonista..."
    narrador "Let's pick up where we left off in our protagonist's adventure..."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:452
translate english eventProtaHablarConDirectorDinero_Part2_7e53afc1:

    # narrador "XD"
    narrador "XD"

translate english strings:

    # game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:93
    old "Hacer otra Paja"
    new "Do another Handjob"

    # game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:147
    old "Hacer otra Mamada"
    new "Do another blowjob"

    # game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:183
    old "Hacer otra garganta profunda camara1"
    new "Do another deep throat camara1"

    # game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:183
    old "Hacer otra garganta profunda camara2"
    new "Do another deep throat camara2"

    # game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:332
    old "Seguir con [nombreProta2]"
    new "Continue with [nombreProta2]"

    # game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:332
    old "Seguir con el Director"
    new "Continue with Director"
# TODO: Translation updated at 2021-11-02 08:57

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:41
translate english eventProtaHablarConDirectorDinero_e973241d:

    # "Atención escena NTR, quieres activarla?"
    "Attention NTR scene, do you want to activate it?"

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:111
translate english eventProtaHablarConDirectorDinero_f1dc61e7:

    # prota "Buenos días."
    prota "Good morning."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:115
translate english eventProtaHablarConDirectorDinero_f6f0a8e6:

    # directorUni "Buenos días. No hace falta que te sientes, será breve."
    directorUni "Good morning. No need to sit down, it will be brief."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:117
translate english eventProtaHablarConDirectorDinero_1913aa6b:

    # prota "Alicia me ha dicho que querías hablar conmigo."
    prota "Alicia told me you wanted to talk to me."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:119
translate english eventProtaHablarConDirectorDinero_4349a299:

    # directorUni "Así es..."
    directorUni "That's right..."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:120
translate english eventProtaHablarConDirectorDinero_9cc86b87:

    # directorUni "Quería hablar contigo porque este mes no hemos recibido el pago de la Universidad."
    directorUni "I wanted to talk to you because we haven't received payment this month."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:122
translate english eventProtaHablarConDirectorDinero_a3175c81:

    # prota "Pero eso no es posible..."
    prota "But that's not possible..."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:124
translate english eventProtaHablarConDirectorDinero_ffae8ed4:

    # directorUni "Sabemos que nunca hemos tenido problemas de impago contigo, pero este mes no hemos recibido el ingreso."
    directorUni "We know we've never had any non-payment problems with you, but this month we haven't received it."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:126
translate english eventProtaHablarConDirectorDinero_eb1c34f4:

    # directorUni "Esta Universidad es de las mejores del país con un personal muy profesional y muy respetado. Solo acude gente importante. Por ahora haremos la vista gorda, pero espero que soluciones el problema lo antes posible, o tomaremos cartas en el asunto."
    directorUni "This University is one of the best in the country with a very professional and highly respected staff. Only important people attend. We will turn a blind eye for now, but I hope you solve the problem as soon as possible, or we will take action."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:128
translate english eventProtaHablarConDirectorDinero_19878be9:

    # prota "Yo... no sabía nada... Lo hablare en casa."
    prota "I... I... I didn't know anything... I'll talk about it at home."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:130
translate english eventProtaHablarConDirectorDinero_ba8aea9e:

    # directorUni "Bien, solo era eso. Por favor cierra la puerta al salir."
    directorUni "Well, it was just that. Please close the door on your way out."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:131
translate english eventProtaHablarConDirectorDinero_d7fe1f2f:

    # directorUni "Gracias."
    directorUni "Thank you."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:133
translate english eventProtaHablarConDirectorDinero_019015ee:

    # prota "Esta bien, adiós."
    prota "Okay, bye."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:138
translate english eventProtaHablarConDirectorDinero_3968dfb8:

    # protaPensa "Oh mierda... esto no me está gustando... Tendré que hablar con [nombreMad3] y [nombrePad3]. Espero que solo sea un mal entendido."
    protaPensa "Oh shit... this isn't sitting well with me.... I'll have to talk to [nombreMad3] and [nombrePad3]. I hope it's just a misunderstanding."

translate english strings:

    # game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:42
    old "SI"
    new "YES"

    # game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:42
    old "NO"
    new "NO"
# TODO: Translation updated at 2021-11-03 12:10

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:111
translate english eventProtaHablarConDirectorDinero_2a2fb77a:

    # directorUni "Claire estas teniendo muy malas notas y sabes que tienes que mejorar.Tu futuro esta en juego"
    directorUni "Claire, you are getting very bad grades and you know you have to improve. Your future is at stake."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:113
translate english eventProtaHablarConDirectorDinero_721aea44:

    # claire "Intentare mejorar..."
    claire "I will try to improve..."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:115
translate english eventProtaHablarConDirectorDinero_645647a0:

    # claire "gracias."
    claire "thank you"

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:117
translate english eventProtaHablarConDirectorDinero_d9b6cc43_1:

    # "TOC TOC"
    "KNOCK KNOCK"

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:121
translate english eventProtaHablarConDirectorDinero_aadf6eb6:

    # protaPensa "El director tambien ha llamado a Claire?"
    protaPensa "The director has also called Claire?"
# TODO: Translation updated at 2022-02-07 21:19

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:592
translate english eventProtaHablarConDirectorDineroEscenaReproductor_fa4f2214:

    # alicia "Un momento."
    alicia "One moment."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:594
translate english eventProtaHablarConDirectorDineroEscenaReproductor_d0ff9b17:

    # alicia "El director quiere verte."
    alicia "The director wants to see you."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:598
translate english eventProtaHablarConDirectorDineroEscenaReproductor_6900df8f:

    # protaPensa "A mí me quiere ver el director? ¿qué es lo que querrá?"
    protaPensa "The director wants to see me...what does he want?"

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:600
translate english eventProtaHablarConDirectorDineroEscenaReproductor_d2d7c2b0:

    # alicia "A saber, que has hecho ya..."
    alicia "To know, what have you already done..."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:601
translate english eventProtaHablarConDirectorDineroEscenaReproductor_2460dd1a:

    # alicia "El director está en su despacho en el segundo piso"
    alicia "The director is in his office on the second floor."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:603
translate english eventProtaHablarConDirectorDineroEscenaReproductor_c0fbc920:

    # prota "Ok, ahora iré a verle."
    prota "OK, I'll go see him now."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:616
translate english eventProtaHablarConDirectorDineroEscenaReproductor_d9b6cc43:

    # "TOC TOC"
    "KNOCK KNOCK"

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:618
translate english eventProtaHablarConDirectorDineroEscenaReproductor_08cec69f:

    # "Unos minutos antes..."
    "A few minutes earlier..."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:624
translate english eventProtaHablarConDirectorDineroEscenaReproductor_e973241d:

    # "Atención escena NTR, quieres activarla?"
    "Attention NTR scene, do you want to activate it?"

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:637
translate english eventProtaHablarConDirectorDineroEscenaReproductor_1b4da312:

    # directorUni "Si quieres que limpie todo tu expediente y que apruebe todas tus asignaturas..."
    directorUni "If you want me to clean up your entire record and pass all your subjects..."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:639
translate english eventProtaHablarConDirectorDineroEscenaReproductor_8ba84ad7:

    # directorUni "Ya sabes lo que tienes que hacer."
    directorUni "You know what you have to do."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:641
translate english eventProtaHablarConDirectorDineroEscenaReproductor_de3d405a:

    # claire "Por favor... ¿No puede haber otra manera? No quiero hacer esto..."
    claire "Please... Can't there be another way? I don't want to do this..."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:643
translate english eventProtaHablarConDirectorDineroEscenaReproductor_9b569954:

    # directorUni "Es esto o ya te digo que tu carrera habrá terminado para siempre."
    directorUni "It's either this or I'm telling you, your career will be over forever."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:647
translate english eventProtaHablarConDirectorDineroEscenaReproductor_ff0ed1ff:

    # directorUni "Arrodíllate y empieza..."
    directorUni "Get down on your knees and start..."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:652
translate english eventProtaHablarConDirectorDineroEscenaReproductor_43426718:

    # claire "Por favor... no me hagas esto..."
    claire "Please... don't do this to me..."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:653
translate english eventProtaHablarConDirectorDineroEscenaReproductor_bc136321:

    # directorUni "Sácala y empieza a mamar."
    directorUni "Take it out and start sucking."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:655
translate english eventProtaHablarConDirectorDineroEscenaReproductor_91dfe82d:

    # claire "yo..."
    claire "I..."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:657
translate english eventProtaHablarConDirectorDineroEscenaReproductor_09024a28:

    # directorUni "¡¡¡Ahora!!!"
    directorUni "Now!!!!"

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:660
translate english eventProtaHablarConDirectorDineroEscenaReproductor_1ec30b34:

    # "¡¡¡PAM!!!"
    "PAM!!!!"

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:667
translate english eventProtaHablarConDirectorDineroEscenaReproductor_91d16b86:

    # narrador "Le quita los pantalones"
    narrador "She takes off his pants"

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:679
translate english eventProtaHablarConDirectorDineroEscenaReproductor_2eb304ad:

    # clairePensa "Le está creciendo..."
    clairePensa "It's getting bigger..."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:681
translate english eventProtaHablarConDirectorDineroEscenaReproductor_9e57770a:

    # clairePensa "La tiene bastante grande. Espero que con esto se corra rápido y termine esta pesadilla."
    clairePensa "He's pretty big. I hope with this he cums fast and this nightmare ends."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:696
translate english eventProtaHablarConDirectorDineroEscenaReproductor_2a2fb77a:

    # directorUni "Claire estas teniendo muy malas notas y sabes que tienes que mejorar.Tu futuro esta en juego"
    directorUni "Claire, you are getting very bad grades and you know you have to improve. Your future is at stake."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:698
translate english eventProtaHablarConDirectorDineroEscenaReproductor_721aea44:

    # claire "Intentare mejorar..."
    claire "I will try to improve..."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:700
translate english eventProtaHablarConDirectorDineroEscenaReproductor_645647a0:

    # claire "gracias."
    claire "thank you"

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:702
translate english eventProtaHablarConDirectorDineroEscenaReproductor_d9b6cc43_1:

    # "TOC TOC"
    "KNOCK KNOCK"

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:706
translate english eventProtaHablarConDirectorDineroEscenaReproductor_aadf6eb6:

    # protaPensa "El director tambien ha llamado a Claire?"
    protaPensa "The director has also called Claire?"

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:710
translate english eventProtaHablarConDirectorDineroEscenaReproductor_f1dc61e7:

    # prota "Buenos días."
    prota "Good morning."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:714
translate english eventProtaHablarConDirectorDineroEscenaReproductor_f6f0a8e6:

    # directorUni "Buenos días. No hace falta que te sientes, será breve."
    directorUni "Good morning. No need to sit down, it will be brief."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:716
translate english eventProtaHablarConDirectorDineroEscenaReproductor_1913aa6b:

    # prota "Alicia me ha dicho que querías hablar conmigo."
    prota "Alicia told me you wanted to talk to me."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:718
translate english eventProtaHablarConDirectorDineroEscenaReproductor_4349a299:

    # directorUni "Así es..."
    directorUni "That's right..."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:719
translate english eventProtaHablarConDirectorDineroEscenaReproductor_9cc86b87:

    # directorUni "Quería hablar contigo porque este mes no hemos recibido el pago de la Universidad."
    directorUni "I wanted to talk to you because we haven't received payment this month."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:721
translate english eventProtaHablarConDirectorDineroEscenaReproductor_a3175c81:

    # prota "Pero eso no es posible..."
    prota "But that's not possible..."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:723
translate english eventProtaHablarConDirectorDineroEscenaReproductor_ffae8ed4:

    # directorUni "Sabemos que nunca hemos tenido problemas de impago contigo, pero este mes no hemos recibido el ingreso."
    directorUni "We know we've never had any non-payment problems with you, but this month we haven't received it."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:725
translate english eventProtaHablarConDirectorDineroEscenaReproductor_eb1c34f4:

    # directorUni "Esta Universidad es de las mejores del país con un personal muy profesional y muy respetado. Solo acude gente importante. Por ahora haremos la vista gorda, pero espero que soluciones el problema lo antes posible, o tomaremos cartas en el asunto."
    directorUni "This University is one of the best in the country with a very professional and highly respected staff. Only important people attend. We will turn a blind eye for now, but I hope you solve the problem as soon as possible, or we will take action."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:727
translate english eventProtaHablarConDirectorDineroEscenaReproductor_19878be9:

    # prota "Yo... no sabía nada... Lo hablare en casa."
    prota "I... I... I didn't know anything... I'll talk about it at home."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:729
translate english eventProtaHablarConDirectorDineroEscenaReproductor_ba8aea9e:

    # directorUni "Bien, solo era eso. Por favor cierra la puerta al salir."
    directorUni "Well, it was just that. Please close the door on your way out."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:730
translate english eventProtaHablarConDirectorDineroEscenaReproductor_d7fe1f2f:

    # directorUni "Gracias."
    directorUni "Thank you."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:732
translate english eventProtaHablarConDirectorDineroEscenaReproductor_019015ee:

    # prota "Esta bien, adiós."
    prota "Okay, bye."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:737
translate english eventProtaHablarConDirectorDineroEscenaReproductor_3968dfb8:

    # protaPensa "Oh mierda... esto no me está gustando... Tendré que hablar con [nombreMad3] y [nombrePad3]. Espero que solo sea un mal entendido."
    protaPensa "Oh shit... this isn't sitting well with me.... I'll have to talk to [nombreMad3] and [nombrePad3]. I hope it's just a misunderstanding."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:771
translate english eventProtaHablarConDirectorDinero2EscenaReproductor_7d64ff1c:

    # directorUni "Métetela en la boca."
    directorUni "Put it in your mouth."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:773
translate english eventProtaHablarConDirectorDinero2EscenaReproductor_0759b9c0:

    # claire "¿Qué? Pero pensé que con esto ya era suficiente. Eso es demasiado…"
    claire "What? But I thought this was enough. That's too much..."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:775
translate english eventProtaHablarConDirectorDinero2EscenaReproductor_af8da784:

    # directorUni "¡Silencio! Aquí las ordenes las doy yo. Esto solo acaba de empezar."
    directorUni "Quiet! I give the orders here. This has only just begun."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:777
translate english eventProtaHablarConDirectorDinero2EscenaReproductor_abf02ddf:

    # directorUni "O haces lo que te digo o tu futuro será el de fregar suelos."
    directorUni "Either you do as I say or your future will be scrubbing floors."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:778
translate english eventProtaHablarConDirectorDinero2EscenaReproductor_b792b5f0:

    # directorUni "Abre la boca y empieza a chupar."
    directorUni "Open your mouth and start sucking."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:781
translate english eventProtaHablarConDirectorDinero2EscenaReproductor_f791ab0b:

    # "sniff..."
    "sniff..."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:785
translate english eventProtaHablarConDirectorDinero2EscenaReproductor_01eb6be6:

    # clairePensa "Dios mío que estoy haciendo... Tengo que aprobar este curso no me queda otra."
    clairePensa "My God what am I doing.... I have to pass this course I have no choice."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:786
translate english eventProtaHablarConDirectorDinero2EscenaReproductor_9cf3c774:

    # clairePensa "Sabe a salado."
    clairePensa "It tastes salty."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:800
translate english eventProtaHablarConDirectorDinero2EscenaReproductor_717e7cc8:

    # directorUni "Así me gusta calladita y obediente. Empieza a moverte y succiona bien."
    directorUni "That's how I like it, quiet and obedient. Start moving and suck it well."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:823
translate english eventProtaHablarConDirectorDinero3EscenaReproductor_41888473:

    # directorUni "Esta bien pero seguro que puedes hacerlo mejor... Métela más."
    directorUni "That's good but I'm sure you can do better.... Put it in more."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:825
translate english eventProtaHablarConDirectorDinero3EscenaReproductor_4df732fd:

    # claire "Por favor ya está, hice todo lo que me pediste."
    claire "Please that's it, I did everything you asked me to do."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:827
translate english eventProtaHablarConDirectorDinero3EscenaReproductor_a7476e7d:

    # directorUni "¡¡Abre la boca bien y métela toda!!"
    directorUni "Open your mouth wide and put it all in!!!"

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:830
translate english eventProtaHablarConDirectorDinero3EscenaReproductor_d2acd487:

    # claire "Ya no puedo más por favor… "
    claire "I can't take it anymore please..."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:834
translate english eventProtaHablarConDirectorDinero3EscenaReproductor_365867ef:

    # directorUni "Quita las manos."
    directorUni "Hands off."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:836
translate english eventProtaHablarConDirectorDinero3EscenaReproductor_c657ce69:

    # claire "Por favor..."
    claire "Please..."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:837
translate english eventProtaHablarConDirectorDinero3EscenaReproductor_2f4b3dee:

    # directorUni "Quita las manos, no lo volveré a repetir."
    directorUni "Take your hands off, I won't repeat it again."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:871
translate english bucle1MamadaProfundaDirectorEscenaReproductor_9cb3f655:

    # directorUni "Así muy bien.{p=0.5}{nw}"
    directorUni "That's very good. {p=0.5}{nw}"

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:878
translate english bucle1MamadaProfundaDirectorEscenaReproductor_54111852:

    # directorUni "Te voy a rellenar todos los agujeros. Ya veras...{p=1}{nw}"
    directorUni "I'm going to fill all your holes. You'll see...{p=1}{nw}"

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:898
translate english bucle1MamadaProfundaDirectorEscenaReproductor_1858d560:

    # directorUni "Serás mi putita y te hare disfrutar...{p=1}{nw}"
    directorUni "You'll be my little whore and I'll make you enjoy...{p=1}{nw}"

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:930
translate english bucle2MamadaProfundaDirectorEscenaReproductor_2a43292d:

    # directorUni "¿Te gusta cómo te follo la boca? Se que lo estas disfrutando.{p=1.8}{nw}"
    directorUni "Do you like the way I fuck your mouth? I know you're enjoying it.{p=1.8}{nw}"

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:943
translate english bucle2MamadaProfundaDirectorEscenaReproductor_b934ddd0:

    # directorUni "Te dicho que abras bien la boca.{p=1}{nw}"
    directorUni "I told you to open your mouth wide.{p=1}{nw}"

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:956
translate english eventProtaHablarConDirectorDinero4EscenaReproductor_a8a2df5f:

    # directorUni "Toda adentro. Así me gusta, que te portes bien."
    directorUni "All in. This is how I like it, that you behave yourself."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:960
translate english eventProtaHablarConDirectorDinero4EscenaReproductor_03a11545:

    # "Toc Toc!!!"
    "Knock Knock!!!"

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:962
translate english eventProtaHablarConDirectorDinero4EscenaReproductor_d5964cca:

    # directorUni "Joder metete debajo de la mesa y no hagas ruido."
    directorUni "Fuck, get under the table and don't make any noise."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:966
translate english eventProtaHablarConDirectorDinero4EscenaReproductor_9334fb36:

    # directorUni "Adelante."
    directorUni "Come in."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:972
translate english eventProtaHablarConDirectorDinero4EscenaReproductor_f1dc61e7:

    # prota "Buenos días."
    prota "Good morning."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:974
translate english eventProtaHablarConDirectorDinero4EscenaReproductor_407c51d9:

    # protaPensa "Juraría que estaba hablando con alguien..."
    protaPensa "I could have sworn he was talking to someone..."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:976
translate english eventProtaHablarConDirectorDinero4EscenaReproductor_f6f0a8e6:

    # directorUni "Buenos días. No hace falta que te sientes, será breve."
    directorUni "Good morning. No need to sit down, it will be brief."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:978
translate english eventProtaHablarConDirectorDinero4EscenaReproductor_1913aa6b:

    # prota "Alicia me ha dicho que querías hablar conmigo."
    prota "Alicia told me you wanted to talk to me."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:980
translate english eventProtaHablarConDirectorDinero4EscenaReproductor_4349a299:

    # directorUni "Así es..."
    directorUni "That's right..."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:981
translate english eventProtaHablarConDirectorDinero4EscenaReproductor_9cc86b87:

    # directorUni "Quería hablar contigo porque este mes no hemos recibido el pago de la Universidad."
    directorUni "I wanted to talk to you because we haven't received payment this month."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:983
translate english eventProtaHablarConDirectorDinero4EscenaReproductor_a3175c81:

    # prota "Pero eso no es posible..."
    prota "But that's not possible..."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:985
translate english eventProtaHablarConDirectorDinero4EscenaReproductor_ffae8ed4:

    # directorUni "Sabemos que nunca hemos tenido problemas de impago contigo, pero este mes no hemos recibido el ingreso."
    directorUni "We know we've never had any non-payment problems with you, but this month we haven't received it."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:987
translate english eventProtaHablarConDirectorDinero4EscenaReproductor_eb1c34f4:

    # directorUni "Esta Universidad es de las mejores del país con un personal muy profesional y muy respetado. Solo acude gente importante. Por ahora haremos la vista gorda, pero espero que soluciones el problema lo antes posible, o tomaremos cartas en el asunto."
    directorUni "This University is one of the best in the country with a very professional and highly respected staff. Only important people attend. We will turn a blind eye for now, but I hope you solve the problem as soon as possible, or we will take action."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:989
translate english eventProtaHablarConDirectorDinero4EscenaReproductor_19878be9:

    # prota "Yo... no sabía nada... Lo hablare en casa."
    prota "I... I... I didn't know anything... I'll talk about it at home."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:991
translate english eventProtaHablarConDirectorDinero4EscenaReproductor_ba8aea9e:

    # directorUni "Bien, solo era eso. Por favor cierra la puerta al salir."
    directorUni "Well, it was just that. Please close the door on your way out."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:992
translate english eventProtaHablarConDirectorDinero4EscenaReproductor_d7fe1f2f:

    # directorUni "Gracias."
    directorUni "Thank you."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:994
translate english eventProtaHablarConDirectorDinero4EscenaReproductor_019015ee:

    # prota "Esta bien, adiós."
    prota "Okay, bye."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:999
translate english eventProtaHablarConDirectorDinero4EscenaReproductor_3968dfb8:

    # protaPensa "Oh mierda... esto no me está gustando... Tendré que hablar con [nombreMad3] y [nombrePad3]. Espero que solo sea un mal entendido."
    protaPensa "Oh shit... this isn't sitting well with me.... I'll have to talk to [nombreMad3] and [nombrePad3]. I hope it's just a misunderstanding."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:1004
translate english eventProtaHablarConDirectorDinero4EscenaReproductor_23b9c2ae:

    # directorUni "Nos han interrumpido, pero no pasa nada, levántate."
    directorUni "We've been interrupted, but it's okay, get up."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:1006
translate english eventProtaHablarConDirectorDinero4EscenaReproductor_0a8a6373:

    # claire "Yo ya me tengo que ir."
    claire "I have to go now."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:1008
translate english eventProtaHablarConDirectorDinero4EscenaReproductor_6b1d2e13:

    # directorUni "Tranquila, solo un poco mas y ya estara..."
    directorUni "Don't worry, just a little more and it will be..."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:1012
translate english eventProtaHablarConDirectorDinero4EscenaReproductor_e56ed859:

    # directorUni "A ver que tienes por aquí."
    directorUni "Let's see what you've got here."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:1014
translate english eventProtaHablarConDirectorDinero4EscenaReproductor_f64e8e1f:

    # claire "No por favor todo menos eso... ¡¡¡Por favor!!!"
    claire "No please anything but that..... Please!!!"

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:1017
translate english eventProtaHablarConDirectorDinero4EscenaReproductor_ea93aec1:

    # directorUni "¡¡Silencio!! bien que hace un rato me la estabas chupando entera y sin rechistar. Se muy bien que esto te gusta."
    directorUni "Quiet!!! well a while ago you were sucking me off whole and without a whimper. I know very well that you like this."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:1019
translate english eventProtaHablarConDirectorDinero4EscenaReproductor_ee4d563a:

    # claire "Espera no..."
    claire "Wait no..."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:1025
translate english eventProtaHablarConDirectorDinero4EscenaReproductor_41454888:

    # directorUni "Voy a disfrutar esto y quiero que estés calladita o tendrás problemas."
    directorUni "I'm going to enjoy this and I want you to be quiet or you'll be in trouble."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:1032
translate english eventProtaHablarConDirectorDinero4EscenaReproductor_5b5ee127:

    # directorUni "Pero si estas chorreando... Lo estabas deseando."
    directorUni "But you're dripping.... You were looking forward to it."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:1051
translate english eventProtaHablarConDirectorDinero4EscenaReproductor_98005ed6:

    # directorUni "Vienes a clase con esta ropa de puta y encima decías que no querías."
    directorUni "You come to class in these slutty clothes and on top of that you were saying you didn't want to."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:1054
translate english eventProtaHablarConDirectorDinero4EscenaReproductor_e60f3b76:

    # directorUni "Tremenda puta estas hecha... No paras de chorrear y tienes todo el coño abierto."
    directorUni "What a whore you are.... You don't stop squirting and you have your whole pussy open."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:1057
translate english eventProtaHablarConDirectorDinero4EscenaReproductor_970d9a75:

    # claire "Por favor ya no mas..."
    claire "Please no more..."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:1060
translate english eventProtaHablarConDirectorDinero4EscenaReproductor_5d3b7bd2:

    # directorUni "Tu coño no dice lo mismo, mira lo abierto que lo tienes y como gotea..."
    directorUni "Your pussy doesn't say the same, look how open you have it and how it drips..."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:1063
translate english eventProtaHablarConDirectorDinero4EscenaReproductor_73e97ac9:

    # directorUni "¿Y esos gemidos?"
    directorUni "And those moans?"

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:1073
translate english eventProtaHablarConDirectorDinero4EscenaReproductor_ed34556b:

    # directorUni "¿Porque tienes el coño tan abierto y chorreando?"
    directorUni "Why is your pussy so open and dripping?"

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:1074
translate english eventProtaHablarConDirectorDinero4EscenaReproductor_dadbda63:

    # directorUni "hahahaha"
    directorUni "hahahaha"

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:1078
translate english eventProtaHablarConDirectorDinero4EscenaReproductor_aa0be0ee:

    # clairePensa "{size=15}{i}Como siga así voy a perder la conciencia. Este desgraciado sabe lo que hace, me está volviendo loca con lo que hace y no puedo evitarlo...{/size}{/i}"
    clairePensa "{size=15}{i}If he keep this up I'm going to lose my mind. This motherfucker knows what he's doing, he's driving me crazy with what he's doing and I can't help it...{/size}{/i}"

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:1081
translate english eventProtaHablarConDirectorDinero4EscenaReproductor_c3efcb68:

    # directorUni "Prepárate porque ahora te lo abriré más."
    directorUni "Get ready because now I'm going to open it wider."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:1083
translate english eventProtaHablarConDirectorDinero4EscenaReproductor_d3227777:

    # clairePensa "..."
    clairePensa "..."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:1085
translate english eventProtaHablarConDirectorDinero4EscenaReproductor_0517bdac:

    # clairePensa "{size=15}{i}Dios no puedo moverme estoy paralizada, este cerdo me va a follar. No para de frotarme el clítoris y abrirme el coño con mis bragas.{/size}{/i}"
    clairePensa "{size=15}{i}God I can't move I'm paralyzed, this pig is going to fuck me. He keeps rubbing my clit and spreading my pussy open with my panties.{/size}{/i}"

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:1088
translate english eventProtaHablarConDirectorDinero4EscenaReproductor_368a4418:

    # claire "haaaaaa haaay"
    claire "haaaaaaaa haaay"

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:1097
translate english eventProtaHablarConDirectorDinero4EscenaReproductor_15fabb8d:

    # directorUni "Tremenda puta estas hecha, mira cómo te abres de piernas tu sola y te colocas en buena posición. Y tu coño no para de gotear."
    directorUni "what a slut you are, look how you spread your legs all by yourself and get in a good position. And your pussy doesn't stop dripping."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:1108
translate english eventProtaHablarConDirectorDinero4EscenaReproductor_5b0ffe26:

    # directorUni "Te estas corriendo y no dices nada. Te está encantando."
    directorUni "You're cumming and you don't say anything. You're liking it."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:1122
translate english eventProtaHablarConDirectorDinero4EscenaReproductor_3525a2d6:

    # directorUni "Cada vez chorreas más, se acabaron los juegos..."
    directorUni "You're dripping more and more, the games are over..."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:1124
translate english eventProtaHablarConDirectorDinero4EscenaReproductor_3ef9a45a:

    # clairePensa "{size=15}{i}No puedo más, me tiene bajo su control, necesito hacer esto para pasar el semestre.{/size}{/i}"
    clairePensa "{size=15}{i}I can't take it anymore, he's got me under his control, I need to do this to get through the semester.{/size}{/i}"

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:1126
translate english eventProtaHablarConDirectorDinero4EscenaReproductor_0414ccc7:

    # claire "aaaaaaaay"
    claire "aaaaaaaay"

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:1131
translate english eventProtaHablarConDirectorDinero4EscenaReproductor_984753e9:

    # directorUni "Te rellenare todo el coño, solo te falta suplicar por ello."
    directorUni "I'll stuff your whole pussy, all you need to do is beg for it."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:1141
translate english eventProtaHablarConDirectorDinero4EscenaReproductor_13bde2e3:

    # claire "¡¡No!! eso no!!"
    claire "No!!! not that!!!"

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:1145
translate english eventProtaHablarConDirectorDinero4EscenaReproductor_987e5ebc:

    # directorUni "¡¡Calla!! pero si sabes que lo estas suplicando."
    directorUni "Shut up!!! you know you're begging for it."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:1146
translate english eventProtaHablarConDirectorDinero4EscenaReproductor_4afd0d3f:

    # narrador "¡Un momento!"
    narrador "Just a moment!"

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:1147
translate english eventProtaHablarConDirectorDinero4EscenaReproductor_ff26c485:

    # narrador "Siento interrumpir, pero…"
    narrador "Sorry to interrupt, but..."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:1148
translate english eventProtaHablarConDirectorDinero4EscenaReproductor_39adfbb3:

    # narrador "Estas jugando al juego Demon Boy Saga, no al Demon Director Saga."
    narrador "You're playing Demon Boy Saga, not Demon Director Saga."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:1149
translate english eventProtaHablarConDirectorDinero4EscenaReproductor_3be1a51b:

    # narrador "Ya hemos visto demasiado del director..."
    narrador "We've already seen too much of the director..."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:1150
translate english eventProtaHablarConDirectorDinero4EscenaReproductor_13ab73d7:

    # narrador "Volvamos donde dejamos la aventura de nuestro protagonista..."
    narrador "Let's pick up where we left off in our protagonist's adventure..."

# game/Code/Eventos/escuela/CLASE/eventProtaHablarConDirectorDinero/eventProtaHablarConDirectorDinero.rpy:1152
translate english eventProtaHablarConDirectorDinero4EscenaReproductor_7e53afc1:

    # narrador "XD"
    narrador "XD"
