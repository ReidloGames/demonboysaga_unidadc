﻿# TODO: Translation updated at 2022-04-03 09:13

# game/Code/Eventos/escuela/CLASE/eventClaireNotasClaseAlicia/eventClaireNotasClaseAlicia.rpy:7
translate english eventClaireNotasClaseAlicia_941fb407:

    # alicia "Dejar de hablar que empezamos la clase."
    alicia "Stop talking now that class has started."

# game/Code/Eventos/escuela/CLASE/eventClaireNotasClaseAlicia/eventClaireNotasClaseAlicia.rpy:11
translate english eventClaireNotasClaseAlicia_0e5e4767:

    # alicia "Antes de nada, comentar que hay gente que como siga así no aprobara las asignaturas."
    alicia "First of all, please know that there are people who, if they continue like this, will not pass this subject."

# game/Code/Eventos/escuela/CLASE/eventClaireNotasClaseAlicia/eventClaireNotasClaseAlicia.rpy:13
translate english eventClaireNotasClaseAlicia_8c3c2127:

    # alicia "Claire he detectado un error en tu expediente y lo he rectificado. Por algún error tenías un aprobado, pero ya lo he corregido."
    alicia "Claire I have spotted an error in your file and I have rectified it. Due to some mistake you had been given a pass, but I have already corrected it."

# game/Code/Eventos/escuela/CLASE/eventClaireNotasClaseAlicia/eventClaireNotasClaseAlicia.rpy:15
translate english eventClaireNotasClaseAlicia_68890722:

    # alicia "Lo siento pero por ahora estas suspendida."
    alicia "I'm sorry but for now you are out of the class."

# game/Code/Eventos/escuela/CLASE/eventClaireNotasClaseAlicia/eventClaireNotasClaseAlicia.rpy:21
translate english eventClaireNotasClaseAlicia_6ed4bb27:

    # claire "¿Que?"
    claire "What?"

# game/Code/Eventos/escuela/CLASE/eventClaireNotasClaseAlicia/eventClaireNotasClaseAlicia.rpy:23
translate english eventClaireNotasClaseAlicia_7d46a533:

    # claire "¡Eso no es posible!"
    claire "That's not possible!"

# game/Code/Eventos/escuela/CLASE/eventClaireNotasClaseAlicia/eventClaireNotasClaseAlicia.rpy:25
translate english eventClaireNotasClaseAlicia_e8b6a0e7:

    # alicia "Yo no tengo la culpa de que no te esfuerces en clase y no estudies para los exámenes."
    alicia "I am not to blame for you not paying attention in class and not studying for exams."

# game/Code/Eventos/escuela/CLASE/eventClaireNotasClaseAlicia/eventClaireNotasClaseAlicia.rpy:27
translate english eventClaireNotasClaseAlicia_f6720849:

    # claire "Pero yo estaba aprobada."
    claire "But I was approved."

# game/Code/Eventos/escuela/CLASE/eventClaireNotasClaseAlicia/eventClaireNotasClaseAlicia.rpy:29
translate english eventClaireNotasClaseAlicia_3ea3a725:

    # alicia "Ya te dije que las notas estaban equivocadas y no me repliques más."
    alicia "I already told you that the grade was wrong and don't talk back about it anymore."

# game/Code/Eventos/escuela/CLASE/eventClaireNotasClaseAlicia/eventClaireNotasClaseAlicia.rpy:31
translate english eventClaireNotasClaseAlicia_9d295638:

    # claire "Eso no es justo, no puede ser."
    claire "That's not fair, it can't be."

# game/Code/Eventos/escuela/CLASE/eventClaireNotasClaseAlicia/eventClaireNotasClaseAlicia.rpy:33
translate english eventClaireNotasClaseAlicia_98f92876:

    # alicia "Vete al pasillo y te aireas un poco y así nos dejas hacer clase."
    alicia "Go to the hallway and get a little air and let us do class."

# game/Code/Eventos/escuela/CLASE/eventClaireNotasClaseAlicia/eventClaireNotasClaseAlicia.rpy:36
translate english eventClaireNotasClaseAlicia_25c9b323:

    # claire "¡Esto es un asco!"
    claire "This is horrible!"

# game/Code/Eventos/escuela/CLASE/eventClaireNotasClaseAlicia/eventClaireNotasClaseAlicia.rpy:38
translate english eventClaireNotasClaseAlicia_b9f66a65:

    # alicia "Continuemos con la clase."
    alicia "Let's continue with the class."

# game/Code/Eventos/escuela/CLASE/eventClaireNotasClaseAlicia/eventClaireNotasClaseAlicia.rpy:40
translate english eventClaireNotasClaseAlicia_36ffff45:

    # alicia "¿Te hace gracia?"
    alicia "Is it funny to you?"

# game/Code/Eventos/escuela/CLASE/eventClaireNotasClaseAlicia/eventClaireNotasClaseAlicia.rpy:42
translate english eventClaireNotasClaseAlicia_4375b247:

    # prota "¡Pero si no hice nada!"
    prota "But I didn't do anything!"

# game/Code/Eventos/escuela/CLASE/eventClaireNotasClaseAlicia/eventClaireNotasClaseAlicia.rpy:44
translate english eventClaireNotasClaseAlicia_12088b8d:

    # alicia "Vete tu también al pasillo que te ira bien."
    alicia "You also go to the corridor as well."

# game/Code/Eventos/escuela/CLASE/eventClaireNotasClaseAlicia/eventClaireNotasClaseAlicia.rpy:46
translate english eventClaireNotasClaseAlicia_b1e4d5e7:

    # prota "¡Maldita sea!"
    prota "Dammit!"

# game/Code/Eventos/escuela/CLASE/eventClaireNotasClaseAlicia/eventClaireNotasClaseAlicia.rpy:51
translate english eventClaireNotasClaseAlicia_52af3ac2:

    # claire "¿Qué haces tu aquí? Lo que me faltaba..."
    claire "What are you doing here? Am I missing something..."

# game/Code/Eventos/escuela/CLASE/eventClaireNotasClaseAlicia/eventClaireNotasClaseAlicia.rpy:53
translate english eventClaireNotasClaseAlicia_28d78973:

    # prota "A mí no me digas, se enfadó contigo y también lo pago conmigo."
    prota "What do you think. She got angry with you and I also have to pay for it."

# game/Code/Eventos/escuela/CLASE/eventClaireNotasClaseAlicia/eventClaireNotasClaseAlicia.rpy:55
translate english eventClaireNotasClaseAlicia_9fb785e2:

    # claire "Arghh.. no me molestes."
    claire "Arghh.. don't bother me."

# game/Code/Eventos/escuela/CLASE/eventClaireNotasClaseAlicia/eventClaireNotasClaseAlicia.rpy:59
translate english eventClaireNotasClaseAlicia_7b4bb106:

    # protaPensa "Es la primera vez que estoy junto a Claire sin que nadie moleste."
    protaPensa "It's the first time I've seen Claire look so bothered about something."

# game/Code/Eventos/escuela/CLASE/eventClaireNotasClaseAlicia/eventClaireNotasClaseAlicia.rpy:63
translate english eventClaireNotasClaseAlicia_8e80e5af:

    # prota "No sabía que te importaban tanto los estudios."
    prota "I didn't know you cared so much about your studies."

# game/Code/Eventos/escuela/CLASE/eventClaireNotasClaseAlicia/eventClaireNotasClaseAlicia.rpy:65
translate english eventClaireNotasClaseAlicia_7d8919b6:

    # claire "Es evidente que me importan. Metete en tus asuntos perdedor."
    claire "Clearly I care about them. Mind your own business, loser."

# game/Code/Eventos/escuela/CLASE/eventClaireNotasClaseAlicia/eventClaireNotasClaseAlicia.rpy:68
translate english eventClaireNotasClaseAlicia_c4b0472c:

    # protaPensa "Está claro que los tocamientos del director están relacionados con sus estudios. Una lástima que no tenga pruebas. Aunque meterme con el director me daría muchos problemas."
    protaPensa "It's obvious that the director's touching is related to her studies. A pity I have no proof. Although messing with the director would give me a lot of problems."

# game/Code/Eventos/escuela/CLASE/eventClaireNotasClaseAlicia/eventClaireNotasClaseAlicia.rpy:76
translate english eventClaireNotasClaseAlicia_e6a64702:

    # prota "Adiós."
    prota "Good bye"

translate english strings:

    # game/Code/Eventos/escuela/CLASE/eventClaireNotasClaseAlicia/eventClaireNotasClaseAlicia.rpy:60
    old "Preguntar por los estudios"
    new "Ask about studies"
