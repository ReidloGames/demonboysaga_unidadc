﻿# TODO: Translation updated at 2021-08-27 11:11

# game/Code/Eventos/escuela/CLASE/eventClaseAliciaLlegoTarde/eventClaseAliciaLlegoTarde.rpy:11
translate english eventClaseAliciaLlegoTarde_376d4a30:

    # protaPensa "Oh joder, he llegado tarde."
    protaPensa "Oh crap, I'm late."

# game/Code/Eventos/escuela/CLASE/eventClaseAliciaLlegoTarde/eventClaseAliciaLlegoTarde.rpy:13
translate english eventClaseAliciaLlegoTarde_e8f6ed13:

    # protaPensa "Y esta es Alicia, la profesora que tanto me quiere..."
    protaPensa "And this is Alicia, the teacher who loves me so much...."

# game/Code/Eventos/escuela/CLASE/eventClaseAliciaLlegoTarde/eventClaseAliciaLlegoTarde.rpy:16
translate english eventClaseAliciaLlegoTarde_08c72dba:

    # protaPensa "Como mujer esta cañón, pero su carácter es el de una serpiente."
    protaPensa "As a woman she's hot, but her character is that of a snake."

# game/Code/Eventos/escuela/CLASE/eventClaseAliciaLlegoTarde/eventClaseAliciaLlegoTarde.rpy:18
translate english eventClaseAliciaLlegoTarde_baaa2438:

    # protaPensa "Aunque creo que me tiene manía... Siempre me ataca gratuitamente"
    protaPensa "Although I think she's got it in for me.... She always attacks me gratuitously"

# game/Code/Eventos/escuela/CLASE/eventClaseAliciaLlegoTarde/eventClaseAliciaLlegoTarde.rpy:20
translate english eventClaseAliciaLlegoTarde_c4d5d54d:

    # prota "Perdón."
    prota "Sorry."

# game/Code/Eventos/escuela/CLASE/eventClaseAliciaLlegoTarde/eventClaseAliciaLlegoTarde.rpy:22
translate english eventClaseAliciaLlegoTarde_cffc6eb9:

    # alicia "Hombre quien tenemos aquí..."
    alicia "Man, who do we have here...."

# game/Code/Eventos/escuela/CLASE/eventClaseAliciaLlegoTarde/eventClaseAliciaLlegoTarde.rpy:23
translate english eventClaseAliciaLlegoTarde_f2a9c16a:

    # alicia "Es una falta de respeto hacia mi y tus compañeros el llegar tarde e interrumpir las clases."
    alicia "It's disrespectful to me and your classmates to be late and disrupt classes."

# game/Code/Eventos/escuela/CLASE/eventClaseAliciaLlegoTarde/eventClaseAliciaLlegoTarde.rpy:25
translate english eventClaseAliciaLlegoTarde_20848218:

    # alicia "Siéntate ya! y empieza a tomar apuntes"
    alicia "Sit down now and start taking notes!"

# game/Code/Eventos/escuela/CLASE/eventClaseAliciaLlegoTarde/eventClaseAliciaLlegoTarde.rpy:31
translate english eventClaseAliciaLlegoTarde_47d2e60b:

    # protaPensa "Mierda, lo que me faltaba!! Me dejado los bolis en la taquilla"
    protaPensa "Shit, that's all I need! I left my pens in the locker."

# game/Code/Eventos/escuela/CLASE/eventClaseAliciaLlegoTarde/eventClaseAliciaLlegoTarde.rpy:35
translate english eventClaseAliciaLlegoTarde_84d6c0be:

    # alicia "Que pasa [nombreProta2], no sabes llegar puntual. ¿Tampoco sabes escribir?"
    alicia "What's the matter [nombreProta2], you can't be on time. Can't you write either?"

# game/Code/Eventos/escuela/CLASE/eventClaseAliciaLlegoTarde/eventClaseAliciaLlegoTarde.rpy:39
translate english eventClaseAliciaLlegoTarde_e1e30f22:

    # prota "Me dejado los bolis en mi taquilla."
    prota "I left my pens in my locker."

# game/Code/Eventos/escuela/CLASE/eventClaseAliciaLlegoTarde/eventClaseAliciaLlegoTarde.rpy:43
translate english eventClaseAliciaLlegoTarde_f82d1bc2:

    # alicia "Muy bien un aplauso para [nombreProta2], que viene a clase tarde y encima sin el material necesario."
    alicia "Very good a round of applause for [nombreProta2], who comes to class late and on top of that without the necessary material."

# game/Code/Eventos/escuela/CLASE/eventClaseAliciaLlegoTarde/eventClaseAliciaLlegoTarde.rpy:45
translate english eventClaseAliciaLlegoTarde_ad9ce31b:

    # protaPensa "Joder!!"
    protaPensa "Damn!!!"

# game/Code/Eventos/escuela/CLASE/eventClaseAliciaLlegoTarde/eventClaseAliciaLlegoTarde.rpy:48
translate english eventClaseAliciaLlegoTarde_5a069473:

    # protaPensa "Uf, suerte que me acorde de coger los bolis"
    protaPensa "Uff, lucky I remembered to take the pens."

# game/Code/Eventos/escuela/CLASE/eventClaseAliciaLlegoTarde/eventClaseAliciaLlegoTarde.rpy:53
translate english eventClaseAliciaLlegoTarde_011bd86b:

    # alicia "Y recordar alumnos lo que no hay que hacer es lo que hace [nombreProta2]. Doy por finalizada la clase."
    alicia "And remember students what not to do is what [nombreProta2] does. I'm ending the class."

# game/Code/Eventos/escuela/CLASE/eventClaseAliciaLlegoTarde/eventClaseAliciaLlegoTarde.rpy:55
translate english eventClaseAliciaLlegoTarde_a3c9ff90:

    # protaPensa "Juro que un día de estos me vengare, por hacerme pasar todo esto."
    protaPensa "I swear that one of these days I'll get even, for putting me through all this."
