﻿# TODO: Translation updated at 2021-08-27 11:11

# game/Code/Eventos/escuela/PASILLOWc/eventLuciaClaireJack/eventLuciaClaireJack.rpy:13
translate english eventLuciaClaireJack_b1693e6c:

    # protaPensa "Y ahí están Jack y Claire"
    protaPensa "And there's Jack and Claire."

# game/Code/Eventos/escuela/PASILLOWc/eventLuciaClaireJack/eventLuciaClaireJack.rpy:19
translate english eventLuciaClaireJack_66d9bd87:

    # protaPensa "Claire es la chica más popular de la escuela"
    protaPensa "Claire is the most popular girl in school."

# game/Code/Eventos/escuela/PASILLOWc/eventLuciaClaireJack/eventLuciaClaireJack.rpy:20
translate english eventLuciaClaireJack_cfdb3489:

    # protaPensa "Es una tía egocéntrica y problemática. Habré hablado con ella 3 veces en total como mucho. Ella está en otra liga."
    protaPensa "She's a self-centered, troubled broad. I will have talked to her 3 times total at most. She's in another league."

# game/Code/Eventos/escuela/PASILLOWc/eventLuciaClaireJack/eventLuciaClaireJack.rpy:21
translate english eventLuciaClaireJack_57b8f782:

    # protaPensa "Todos los chicos hablan de ella y a todos nos gustaría follárnosla"
    protaPensa "All the guys talk about her and we'd all like to fuck her."

# game/Code/Eventos/escuela/PASILLOWc/eventLuciaClaireJack/eventLuciaClaireJack.rpy:27
translate english eventLuciaClaireJack_31c5b871:

    # protaPensa "Desgraciadamente está saliendo con el tonto de Jack."
    protaPensa "Unfortunately she's dating dumb Jack."

# game/Code/Eventos/escuela/PASILLOWc/eventLuciaClaireJack/eventLuciaClaireJack.rpy:28
translate english eventLuciaClaireJack_442127a6:

    # protaPensa "Jack es el novio de Claire. El típico hombre musculitos y sin cerebro."
    protaPensa "Jack is Claire's boyfriend. The typical brawny, brainless muscle man."

# game/Code/Eventos/escuela/PASILLOWc/eventLuciaClaireJack/eventLuciaClaireJack.rpy:30
translate english eventLuciaClaireJack_f2214d00:

    # protaPensa "Es un chulo y se lo tiene muy creído. Tengo que vigilar con él o puede darme problemas."
    protaPensa "He's a pimp and he's very cocky. I have to watch out for him or he might get me in trouble."

# game/Code/Eventos/escuela/PASILLOWc/eventLuciaClaireJack/eventLuciaClaireJack.rpy:34
translate english eventLuciaClaireJack_37bbd6c0:

    # jack "Venga chacha a limpiar bien!"
    jack "Come on maid clean up good!"

# game/Code/Eventos/escuela/PASILLOWc/eventLuciaClaireJack/eventLuciaClaireJack.rpy:36
translate english eventLuciaClaireJack_e3d5eda3:

    # "ZAAASS!!!"
    "¡¡¡ZAAASS!!!"

# game/Code/Eventos/escuela/PASILLOWc/eventLuciaClaireJack/eventLuciaClaireJack.rpy:46
translate english eventLuciaClaireJack_e2ae12f1:

    # jack "Así, muy bien."
    jack "Like that, very good."

# game/Code/Eventos/escuela/PASILLOWc/eventLuciaClaireJack/eventLuciaClaireJack.rpy:56
translate english eventLuciaClaireJack_479a84ef:

    # jack "Sal de mi camino enano!"
    jack "Get out of my way midget!"

# game/Code/Eventos/escuela/PASILLOWc/eventLuciaClaireJack/eventLuciaClaireJack.rpy:61
translate english eventLuciaClaireJack_ed6eed97:

    # claire "Adiós perdedor!"
    claire "Goodbye loser!"

# game/Code/Eventos/escuela/PASILLOWc/eventLuciaClaireJack/eventLuciaClaireJack.rpy:65
translate english eventLuciaClaireJack_4c017471:

    # prota "No te preocupes son idiotas."
    prota "Don't worry they're idiots."

# game/Code/Eventos/escuela/PASILLOWc/eventLuciaClaireJack/eventLuciaClaireJack.rpy:67
translate english eventLuciaClaireJack_cc559dc5:

    # lucia "No me respetan y me tratan mal porque soy la de la limpieza. Con razón mi hijo no viene a saludarme."
    lucia "They don't respect me and treat me badly because I'm the cleaning lady. No wonder my son doesn't come to greet me."

# game/Code/Eventos/escuela/PASILLOWc/eventLuciaClaireJack/eventLuciaClaireJack.rpy:69
translate english eventLuciaClaireJack_77c9a390:

    # lucia "Seguro que le da vergüenza ver a su madre limpiar la escuela."
    lucia "I'm sure he's embarrassed to see his mother cleaning the school."

# game/Code/Eventos/escuela/PASILLOWc/eventLuciaClaireJack/eventLuciaClaireJack.rpy:71
translate english eventLuciaClaireJack_d3f43c50:

    # prota "No digas tonterías, eres una persona fuerte y muy bonita para decir esas cosas."
    prota "Don't talk nonsense, you're a strong person and too pretty to say such things."

# game/Code/Eventos/escuela/PASILLOWc/eventLuciaClaireJack/eventLuciaClaireJack.rpy:75
translate english eventLuciaClaireJack_1a5c1f07:

    # lucia "Gracias cielo. Me alegra que Mike tenga como amigo una persona tan buena y que se preocupe por los demás."
    lucia "Thank you, honey. I'm glad Mike has such a good and caring person as a friend."

# game/Code/Eventos/escuela/PASILLOWc/eventLuciaClaireJack/eventLuciaClaireJack.rpy:77
translate english eventLuciaClaireJack_199ad7f2:

    # prota "Mierda, llegare tarde a clase. Lo siento me tengo que ir!"
    prota "Crap, I'm going to be late for class. Sorry I have to go!"

# game/Code/Eventos/escuela/PASILLOWc/eventLuciaClaireJack/eventLuciaClaireJack.rpy:86
translate english esquivarGolpeJackPasilloWc_5cb95416:

    # jack "Enanooo!!"
    jack "Dwarf!!!"

# game/Code/Eventos/escuela/PASILLOWc/eventLuciaClaireJack/eventLuciaClaireJack.rpy:94
translate english noEsquivarGolpeJackPasilloWc_b90ea492:

    # jack "Eres un pringado!!!"
    jack "You're a sucker!!!"
