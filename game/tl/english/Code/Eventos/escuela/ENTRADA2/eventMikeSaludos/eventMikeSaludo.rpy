﻿# TODO: Translation updated at 2021-08-06 14:00

# game/Code/Eventos/escuela/ENTRADA2/eventMikeSaludos/eventMikeSaludo.rpy:5
translate english eventMikeSaludo_feccc9c0:

    # protaPensa "Este es Mike"
    protaPensa "This is Mike"

# game/Code/Eventos/escuela/ENTRADA2/eventMikeSaludos/eventMikeSaludo.rpy:7
translate english eventMikeSaludo_93cfd31e:

    # protaPensa "Es mi mejor amigo, nos conocemos desde hace muchos años."
    protaPensa "He's my best friend, we've known each other for many years."

# game/Code/Eventos/escuela/ENTRADA2/eventMikeSaludos/eventMikeSaludo.rpy:9
translate english eventMikeSaludo_6b9e33ab:

    # protaPensa "El vive cerca de casa, así que podemos decir que somos casi vecinos."
    protaPensa "He lives close to home, so we can say we're almost neighbors"

# game/Code/Eventos/escuela/ENTRADA2/eventMikeSaludos/eventMikeSaludo.rpy:11
translate english eventMikeSaludo_399d20a9:

    # prota "!Que tal hermano!"
    prota "What's up bro!"

# game/Code/Eventos/escuela/ENTRADA2/eventMikeSaludos/eventMikeSaludo.rpy:17
translate english eventMikeSaludo_b9af3d13:

    # mike "Menos mal [nombreProta2], ya pense que no llegarías a tiempo."
    mike "Good thing [nombreProta2], I already thought you wouldn't make it on time."

# game/Code/Eventos/escuela/ENTRADA2/eventMikeSaludos/eventMikeSaludo.rpy:19
translate english eventMikeSaludo_e63dcf56:

    # mike "Venga vamos a clase, no tenemos mucho tiempo o llegaremos tarde."
    mike "Come on let's go to class, we don't have much time or we'll be late."

# game/Code/Eventos/escuela/ENTRADA2/eventMikeSaludos/eventMikeSaludo.rpy:23
translate english eventMikeSaludo_0717adb9:

    # prota "Solo necesito 1 minuto, tengo que ir a mear."
    prota "I just need 1 minute, I have to go pee."

# game/Code/Eventos/escuela/ENTRADA2/eventMikeSaludos/eventMikeSaludo.rpy:25
translate english eventMikeSaludo_5953d79c:

    # mike "Ostia tio,yo voy tirando, no quiero tener problemas. Date prisa o Alicia cargara contra ti otra vez."
    mike "Geez man, I'm pulling, I don't want to get in trouble. Hurry up or Alicia will charge at you again."

# game/Code/Eventos/escuela/ENTRADA2/eventMikeSaludos/eventMikeSaludo.rpy:27
translate english eventMikeSaludo_c958fab6:

    # prota "Si, no te preocupes, no quiero escuchar a esa bruja. En nada estoy allí."
    prota "Yeah, don't worry, I don't want to listen to that witch. I'll be there in no time."

# game/Code/Eventos/escuela/ENTRADA2/eventMikeSaludos/eventMikeSaludo.rpy:29
translate english eventMikeSaludo_fe45ab57:

    # mike "Nos vemos en clase."
    mike "I'll see you in class."
# TODO: Translation updated at 2021-08-27 11:11

# game/Code/Eventos/escuela/ENTRADA2/eventMikeSaludos/eventMikeSaludo.rpy:11
translate english eventMikeSaludo_2b6ce482:

    # prota "¡Que tal hermano!"
    prota "What's up bro!"
