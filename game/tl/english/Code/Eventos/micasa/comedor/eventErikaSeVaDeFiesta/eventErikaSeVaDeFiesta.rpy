﻿# TODO: Translation updated at 2021-12-12 13:06

# game/Code/Eventos/micasa/comedor/eventErikaSeVaDeFiesta/eventErikaSeVaDeFiesta.rpy:20
translate english eventErikaSeVaDeFiesta_1b296779:

    # protaPensa "¿A dónde va?"
    protaPensa "Where are you going?"

# game/Code/Eventos/micasa/comedor/eventErikaSeVaDeFiesta/eventErikaSeVaDeFiesta.rpy:22
translate english eventErikaSeVaDeFiesta_bc738185:

    # protaPensa "¿Se va de fiesta? Menudo vestido más corto lleva..."
    protaPensa "Is she going to a party? What a short dress she's wearing..."

# game/Code/Eventos/micasa/comedor/eventErikaSeVaDeFiesta/eventErikaSeVaDeFiesta.rpy:24
translate english eventErikaSeVaDeFiesta_fe31ad90:

    # "Sigue intentándolo..."
    "Keep trying..."

# game/Code/Eventos/micasa/comedor/eventErikaSeVaDeFiesta/eventErikaSeVaDeFiesta.rpy:33
translate english eventErikaSeVaDeFiesta_a8000182:

    # protaPensa "Parece que se marcha de fiesta otra vez..."
    protaPensa "Looks like she's going partying again..."
