﻿# TODO: Translation updated at 2022-02-16 11:36

# game/Code/Eventos/micasa/comedor/eventPadVuelveTrabjoTriste/eventPadVuelveTrabjoTriste.rpy:10
translate english eventPadVuelveTrabjoTriste_1d995bfc:

    # protaPensa "Oh, es [situPad]. Probablemente haya regresado de su reunión de negocios."
    protaPensa "Oh, it's [situPad]. Probably back from his business meeting."

# game/Code/Eventos/micasa/comedor/eventPadVuelveTrabjoTriste/eventPadVuelveTrabjoTriste.rpy:17
translate english eventPadVuelveTrabjoTriste_ac5571eb:

    # padPensa "Oh Dios, ¿qué demonios hice?"
    padPensa "Oh God, what the hell did I do?"

# game/Code/Eventos/micasa/comedor/eventPadVuelveTrabjoTriste/eventPadVuelveTrabjoTriste.rpy:19
translate english eventPadVuelveTrabjoTriste_e7fe32a0:

    # protaPensa "Parece cansado. Debe haber sido un día ocupado."
    protaPensa "He looks tired. He must have had a busy day."

# game/Code/Eventos/micasa/comedor/eventPadVuelveTrabjoTriste/eventPadVuelveTrabjoTriste.rpy:21
translate english eventPadVuelveTrabjoTriste_1b5c5f5a:

    # protaPensa "No me ha visto."
    protaPensa "He hasn't noticed me."

# game/Code/Eventos/micasa/comedor/eventPadVuelveTrabjoTriste/eventPadVuelveTrabjoTriste.rpy:28
translate english eventPadVuelveTrabjoTriste_f7106cc7:

    # protaPensa "Va directamente a su habitación. Debe querer una noche temprana."
    protaPensa "He's going straight to his room. He must want an early night."

# game/Code/Eventos/micasa/comedor/eventPadVuelveTrabjoTriste/eventPadVuelveTrabjoTriste.rpy:31
translate english eventPadVuelveTrabjoTriste_270c2995:

    # protaPensa "Esta es una buena oportunidad para pedir algo de dinero de bolsillo mientras está solo."
    protaPensa "This is a good opportunity to ask for some pocket money while he's alone."
# TODO: Translation updated at 2022-03-27 17:09

# game/Code/Eventos/micasa/comedor/eventPadVuelveTrabjoTriste/eventPadVuelveTrabjoTriste.rpy:10
translate english eventPadVuelveTrabjoTriste_516386a9:

    # protaPensa "Oh, es [situPad2]. Ha llegado pronto de su reunión de negocios."
    protaPensa "Oh, it's [situPad2]. He's come back early from his business meeting."

# game/Code/Eventos/micasa/comedor/eventPadVuelveTrabjoTriste/eventPadVuelveTrabjoTriste.rpy:19
translate english eventPadVuelveTrabjoTriste_d34aae69:

    # protaPensa "Parece cansado. Habrá sido un día ajetreado."
    protaPensa "He looks tired. It must have been a busy day."

# game/Code/Eventos/micasa/comedor/eventPadVuelveTrabjoTriste/eventPadVuelveTrabjoTriste.rpy:28
translate english eventPadVuelveTrabjoTriste_b91b8aa9:

    # protaPensa "Va directamente a su habitación. Querrá ir a descansar."
    protaPensa "He's going straight to his room. He must want to get some rest."

# game/Code/Eventos/micasa/comedor/eventPadVuelveTrabjoTriste/eventPadVuelveTrabjoTriste.rpy:31
translate english eventPadVuelveTrabjoTriste_eaf0ab52:

    # protaPensa "Ahora que está solo es una buena oportunidad para pedirle algo de dinero."
    protaPensa "Now that he is alone it is a good opportunity to ask him for some money."
