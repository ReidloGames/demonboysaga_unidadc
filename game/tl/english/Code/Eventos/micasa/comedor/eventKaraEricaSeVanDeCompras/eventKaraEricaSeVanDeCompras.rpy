﻿# TODO: Translation updated at 2022-04-04 15:35

# game/Code/Eventos/micasa/comedor/eventKaraEricaSeVanDeCompras/eventKaraEricaSeVanDeCompras.rpy:11
translate english eventKaraEricaSeVanDeCompras_80e6ed22:

    # protaPensa "Menudo culo tiene Kara." with dissolve
    protaPensa "What an ass Kara has." with dissolve

# game/Code/Eventos/micasa/comedor/eventKaraEricaSeVanDeCompras/eventKaraEricaSeVanDeCompras.rpy:13
translate english eventKaraEricaSeVanDeCompras_06d1e642:

    # hermanMay "Vamos Erica, eres una tardona."
    hermanMay "Come on Erica, you're always late."

# game/Code/Eventos/micasa/comedor/eventKaraEricaSeVanDeCompras/eventKaraEricaSeVanDeCompras.rpy:17
translate english eventKaraEricaSeVanDeCompras_7af2fd6d:

    # hermanMed "¡Ya estoy!"
    hermanMed "I'm ready!"

# game/Code/Eventos/micasa/comedor/eventKaraEricaSeVanDeCompras/eventKaraEricaSeVanDeCompras.rpy:19
translate english eventKaraEricaSeVanDeCompras_ccee06d9:

    # hermanMay "Podríamos ir por el centro. Hay unas tiendas con cosas muy bonitas."
    hermanMay "We could go downtown. There are some shops with very nice things."

# game/Code/Eventos/micasa/comedor/eventKaraEricaSeVanDeCompras/eventKaraEricaSeVanDeCompras.rpy:21
translate english eventKaraEricaSeVanDeCompras_ee8f98ff:

    # hermanMay "Adiós, nos vamos de compras."
    hermanMay "Later. We're going shopping."

# game/Code/Eventos/micasa/comedor/eventKaraEricaSeVanDeCompras/eventKaraEricaSeVanDeCompras.rpy:23
translate english eventKaraEricaSeVanDeCompras_93b102d5:

    # prota "¡Pasarlo bien!"
    prota "Have fun!"
