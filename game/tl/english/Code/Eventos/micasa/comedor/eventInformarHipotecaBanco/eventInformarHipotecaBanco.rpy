﻿# TODO: Translation updated at 2021-10-13 15:38

# game/Code/Eventos/micasa/comedor/eventInformarHipotecaBanco/eventInformarHipotecaBanco.rpy:6
translate english eventInformarHipotecaBanco_e81a1dd4:

    # "DING DONG"
    "DING DONG"

# game/Code/Eventos/micasa/comedor/eventInformarHipotecaBanco/eventInformarHipotecaBanco.rpy:23
translate english eventInformarHipotecaBanco_765944d8:

    # mad "Hola, buenos días."
    mad "Hello, good morning."

# game/Code/Eventos/micasa/comedor/eventInformarHipotecaBanco/eventInformarHipotecaBanco.rpy:25
translate english eventInformarHipotecaBanco_15a0b223:

    # daren "Hola señora, somos trabajadores del banco National Bank, yo soy Daren y mi compañero se llama Adriano. Venimos a informarle de la situación de esta casa."
    daren "Hello madam, we are workers from National Bank, I am Daren and my partner's name is Adriano. We are here to inform you of the situation of this house."

# game/Code/Eventos/micasa/comedor/eventInformarHipotecaBanco/eventInformarHipotecaBanco.rpy:27
translate english eventInformarHipotecaBanco_ef7f4db5:

    # daren "Como sabrá esta casa ha pasado a ser propiedad del banco. A partir de ahora tendrá que cumplir con el nuevo contrato y pagar la hipoteca. Aquí está la firma de un tal John ... antiguo propietario"
    daren "As you know this house has become the property of the bank. From now on you will have to comply with the new contract and pay the mortgage. Here is the signature of a certain John ... former owner."

# game/Code/Eventos/micasa/comedor/eventInformarHipotecaBanco/eventInformarHipotecaBanco.rpy:28
translate english eventInformarHipotecaBanco_3c7b3c37:

    # daren "Le enviaremos toda la documentación por burofax, esto solo ha sido una visita de cortesía"
    daren "We will send you all the documentation by burofax, this was just a courtesy visit."

# game/Code/Eventos/micasa/comedor/eventInformarHipotecaBanco/eventInformarHipotecaBanco.rpy:30
translate english eventInformarHipotecaBanco_294e8679:

    # daren "Que tenga un buen día."
    daren "Have a nice day."

# game/Code/Eventos/micasa/comedor/eventInformarHipotecaBanco/eventInformarHipotecaBanco.rpy:40
translate english eventInformarHipotecaBanco_3b0d73a2:

    # madPensa "Esto es la peor pesadilla que se pueda tener."
    madPensa "This is the worst nightmare you can have."

# game/Code/Eventos/micasa/comedor/eventInformarHipotecaBanco/eventInformarHipotecaBanco.rpy:42
translate english eventInformarHipotecaBanco_f979a42d:

    # prota "¿Que te pasa [nombreMad3]?"
    prota "What's wrong with you [nombreMad3]?"

# game/Code/Eventos/micasa/comedor/eventInformarHipotecaBanco/eventInformarHipotecaBanco.rpy:44
translate english eventInformarHipotecaBanco_c855da9e:

    # mad "Son trabajadores del banco. Han venido a recordar que habrá que pagar la deuda de [nombrePad3]..."
    mad "They're bank workers. They've come to remind us that [nombrePad3]'s debt will have to be paid..."

# game/Code/Eventos/micasa/comedor/eventInformarHipotecaBanco/eventInformarHipotecaBanco.rpy:48
translate english eventInformarHipotecaBanco_5b04b476:

    # mad "Con lo que gane [nombrePad3], será imposible hacer frente a tantos gastos."
    mad "With what [nombrePad3] earns, it will be impossible to meet so many expenses."

# game/Code/Eventos/micasa/comedor/eventInformarHipotecaBanco/eventInformarHipotecaBanco.rpy:51
translate english eventInformarHipotecaBanco_1d083d9c:

    # prota "[nombreMad3], haremos lo que me dijiste."
    prota "[nombreMad3], we will do what you told me."

# game/Code/Eventos/micasa/comedor/eventInformarHipotecaBanco/eventInformarHipotecaBanco.rpy:53
translate english eventInformarHipotecaBanco_68ab7ddc:

    # prota "Juntos superaremos esto."
    prota "Together we'll get through this."

# game/Code/Eventos/micasa/comedor/eventInformarHipotecaBanco/eventInformarHipotecaBanco.rpy:71
translate english abrazoMadDelante_e36f27a6:

    # prota "No voy a dejarte sola y que lo pases mal."
    prota "I'm not going to leave you alone and let you have a hard time."

# game/Code/Eventos/micasa/comedor/eventInformarHipotecaBanco/eventInformarHipotecaBanco.rpy:92
translate english abrazoMadDelante_3bfb2716:

    # prota "No quiero verte mal. Seguro que podremos superar esto si nos ayudamos."
    prota "I don't want to see you in pain. I'm sure we can get through this if we help each other."

# game/Code/Eventos/micasa/comedor/eventInformarHipotecaBanco/eventInformarHipotecaBanco.rpy:95
translate english abrazoMadDelante_37e31d15:

    # mad "Eh... sí, supongo..."
    mad "Uh... yeah, I guess..."

# game/Code/Eventos/micasa/comedor/eventInformarHipotecaBanco/eventInformarHipotecaBanco.rpy:111
translate english noAbrazar_e36f27a6:

    # prota "No voy a dejarte sola y que lo pases mal."
    prota "I'm not going to leave you alone and let you have a hard time."

# game/Code/Eventos/micasa/comedor/eventInformarHipotecaBanco/eventInformarHipotecaBanco.rpy:133
translate english noAbrazar_adbfc1c5:

    # prota "Todo saldrá bien."
    prota "It'll be all right."

# game/Code/Eventos/micasa/comedor/eventInformarHipotecaBanco/eventInformarHipotecaBanco.rpy:138
translate english noAbrazar_638695fb:

    # mad "Eh... sí, supongo que tienes razón..."
    mad "Uh... yeah, I guess you're right..."

# game/Code/Eventos/micasa/comedor/eventInformarHipotecaBanco/eventInformarHipotecaBanco.rpy:148
translate english continuarInformarHipoteca_c8f5995f:

    # prota "Voy a buscar un empleo. Quiero ayudar en todo lo que pueda..."
    prota "I'm going to look for a job. I want to help in any way I can..."

# game/Code/Eventos/micasa/comedor/eventInformarHipotecaBanco/eventInformarHipotecaBanco.rpy:150
translate english continuarInformarHipoteca_ec76baf7:

    # mad "Gracias [nombreProta2], es muy amable por tu parte, pero tu ya tienes muchas cosas como la Universidad."
    mad "Thank you [nombreProta2], that's very kind of you, but you already have a lot of things like college."

# game/Code/Eventos/micasa/comedor/eventInformarHipotecaBanco/eventInformarHipotecaBanco.rpy:152
translate english continuarInformarHipoteca_259d7585:

    # prota "No te preocupes, encontrare el modo de colaborar y que podamos salir de esta situación. "
    prota "Don't worry, I'll find a way to collaborate and we can get out of this situation."

# game/Code/Eventos/micasa/comedor/eventInformarHipotecaBanco/eventInformarHipotecaBanco.rpy:154
translate english continuarInformarHipoteca_1c231a14:

    # mad "Gracias por preocuparte tanto y querer ayudar."
    mad "Thank you for caring so much and wanting to help."

# game/Code/Eventos/micasa/comedor/eventInformarHipotecaBanco/eventInformarHipotecaBanco.rpy:156
translate english continuarInformarHipoteca_b952c591:

    # prota "De nada."
    prota "You're welcome."

# game/Code/Eventos/micasa/comedor/eventInformarHipotecaBanco/eventInformarHipotecaBanco.rpy:163
translate english continuarInformarHipoteca_311aa28f:

    # protaPensa "Iré a mi habitación a pensar y buscar un empleo por internet."
    protaPensa "I'll go to my room to think and look for a job online."

# game/Code/Eventos/micasa/comedor/eventInformarHipotecaBanco/eventInformarHipotecaBanco.rpy:169
translate english continuarInformarHipoteca2_c8f5995f:

    # prota "Voy a buscar un empleo. Quiero ayudar en todo lo que pueda..."
    prota "I'm going to look for a job. I want to help in any way I can..."

# game/Code/Eventos/micasa/comedor/eventInformarHipotecaBanco/eventInformarHipotecaBanco.rpy:171
translate english continuarInformarHipoteca2_1322e5e3:

    # mad "Gracias [nombreProta2], es muy amable por tu parte, pero tú ya tienes muchas cosas como la Universidad."
    mad "Thank you [nombreProta2], that's very kind of you, but you already have a lot of things like college."

# game/Code/Eventos/micasa/comedor/eventInformarHipotecaBanco/eventInformarHipotecaBanco.rpy:173
translate english continuarInformarHipoteca2_6dd3deba:

    # prota "No te preocupes encontrare el modo de colaborar y que podamos salir de esta situación."
    prota "Don't worry I will find a way to collaborate and we can get out of this situation."

# game/Code/Eventos/micasa/comedor/eventInformarHipotecaBanco/eventInformarHipotecaBanco.rpy:175
translate english continuarInformarHipoteca2_1aabf95c:

    # mad "Gracias por querer ayudar."
    mad "Thank you for wanting to help."

# game/Code/Eventos/micasa/comedor/eventInformarHipotecaBanco/eventInformarHipotecaBanco.rpy:177
translate english continuarInformarHipoteca2_b952c591:

    # prota "De nada."
    prota "You're welcome."

# game/Code/Eventos/micasa/comedor/eventInformarHipotecaBanco/eventInformarHipotecaBanco.rpy:184
translate english continuarInformarHipoteca2_311aa28f:

    # protaPensa "Iré a mi habitación a pensar y buscar un empleo por internet."
    protaPensa "I'll go to my room to think and look for a job online."

translate english strings:

    # game/Code/Eventos/micasa/comedor/eventInformarHipotecaBanco/eventInformarHipotecaBanco.rpy:57
    old "Abrazar a [nombreMad3]"
    new "Hug [nombreMad3]."

    # game/Code/Eventos/micasa/comedor/eventInformarHipotecaBanco/eventInformarHipotecaBanco.rpy:57
    old "No abrazar"
    new "No hugging"

    # game/Code/Eventos/micasa/comedor/eventInformarHipotecaBanco/eventInformarHipotecaBanco.rpy:105
    old "Abrazar por detras"
    new "Hug from behind"
