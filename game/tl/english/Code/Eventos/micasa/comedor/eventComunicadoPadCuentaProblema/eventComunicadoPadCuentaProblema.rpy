﻿# TODO: Translation updated at 2021-08-27 11:11

# game/Code/Eventos/micasa/comedor/eventComunicadoPadCuentaProblema/eventComunicadoPadCuentaProblema.rpy:11
translate english eventComunicadoPadCuentaProblema_5aeea818:

    # mad "Hola cielo, ¿has podido dormir bien?"
    mad "Hi honey, did you get a good night's sleep?"

# game/Code/Eventos/micasa/comedor/eventComunicadoPadCuentaProblema/eventComunicadoPadCuentaProblema.rpy:14
translate english eventComunicadoPadCuentaProblema_d21b4919:

    # mad "Parece que hayas visto un fantasma."
    mad "You look like you've seen a ghost."

# game/Code/Eventos/micasa/comedor/eventComunicadoPadCuentaProblema/eventComunicadoPadCuentaProblema.rpy:15
translate english eventComunicadoPadCuentaProblema_12b21e62:

    # mad "Tranquilo, es normal con lo que viste ayer..."
    mad "Calm down, it's normal with what you saw yesterday..."

# game/Code/Eventos/micasa/comedor/eventComunicadoPadCuentaProblema/eventComunicadoPadCuentaProblema.rpy:17
translate english eventComunicadoPadCuentaProblema_6762295d:

    # mad "Juntos lo superaremos."
    mad "Together we'll get through this."

# game/Code/Eventos/micasa/comedor/eventComunicadoPadCuentaProblema/eventComunicadoPadCuentaProblema.rpy:21
translate english eventComunicadoPadCuentaProblema_1c892241:

    # "Se sientan..."
    "They Sit down..."

# game/Code/Eventos/micasa/comedor/eventComunicadoPadCuentaProblema/eventComunicadoPadCuentaProblema.rpy:25
translate english eventComunicadoPadCuentaProblema_0c54d6fc:

    # pad "Siento teneros que avisar así."
    pad "I'm sorry to have to inform you like this."

# game/Code/Eventos/micasa/comedor/eventComunicadoPadCuentaProblema/eventComunicadoPadCuentaProblema.rpy:27
translate english eventComunicadoPadCuentaProblema_9ae79323:

    # pad "Pero os tengo que comunicar algo importante."
    pad "But I have something important to tell you."

# game/Code/Eventos/micasa/comedor/eventComunicadoPadCuentaProblema/eventComunicadoPadCuentaProblema.rpy:29
translate english eventComunicadoPadCuentaProblema_e6146ad2:

    # pad "[nombreMad3] y [nombreProta2] ya saben de la situación y ahora os tenía que informar a vosotras."
    pad "[nombreMad3] and [nombreProta2] already know about the situation and now I had to inform you."

# game/Code/Eventos/micasa/comedor/eventComunicadoPadCuentaProblema/eventComunicadoPadCuentaProblema.rpy:31
translate english eventComunicadoPadCuentaProblema_2b9aef0a:

    # pad "Esto os sorprenderá, es difícil para mí y es un golpe duro de asimilar, pero ahora tenemos que estar unidos para superar este problema."
    pad "This will surprise you, it is difficult for me and it is a hard blow to assimilate, but now we have to be united to overcome this problem."

# game/Code/Eventos/micasa/comedor/eventComunicadoPadCuentaProblema/eventComunicadoPadCuentaProblema.rpy:33
translate english eventComunicadoPadCuentaProblema_3991c759:

    # hermanMay "¿Pero dinos que está pasando?"
    hermanMay "But tell us what's going on?"

# game/Code/Eventos/micasa/comedor/eventComunicadoPadCuentaProblema/eventComunicadoPadCuentaProblema.rpy:35
translate english eventComunicadoPadCuentaProblema_65080257:

    # hermanMed "Dinos ya, lo que está pasando."
    hermanMed "Tell us already, what's going on."

# game/Code/Eventos/micasa/comedor/eventComunicadoPadCuentaProblema/eventComunicadoPadCuentaProblema.rpy:39
translate english eventComunicadoPadCuentaProblema_0e8722cd:

    # pad "Hace unos días perdí el trabajo por una irresponsabilidad de la que estoy muy arrepentido."
    pad "A few days ago I lost my job because of an irresponsibility for which I am very sorry."

# game/Code/Eventos/micasa/comedor/eventComunicadoPadCuentaProblema/eventComunicadoPadCuentaProblema.rpy:43
translate english eventComunicadoPadCuentaProblema_f08a7829:

    # pad "A parte de que he tenido que hipotecar la casa."
    pad "Apart from the fact that I had to mortgage the house."

# game/Code/Eventos/micasa/comedor/eventComunicadoPadCuentaProblema/eventComunicadoPadCuentaProblema.rpy:45
translate english eventComunicadoPadCuentaProblema_228ef0ec:

    # hermanMay "Oh dios santo, esto tiene que ser una broma."
    hermanMay "Oh my goodness, this has to be a joke."

# game/Code/Eventos/micasa/comedor/eventComunicadoPadCuentaProblema/eventComunicadoPadCuentaProblema.rpy:47
translate english eventComunicadoPadCuentaProblema_32ef1a27:

    # hermanMed "Pero ¿cómo es posible?"
    hermanMed "But how is that possible?"

# game/Code/Eventos/micasa/comedor/eventComunicadoPadCuentaProblema/eventComunicadoPadCuentaProblema.rpy:49
translate english eventComunicadoPadCuentaProblema_071ba275:

    # pad "Cometí un fallo y ahora tendré que pagarlo."
    pad "I made a mistake and now I'm going to have to pay for it."

# game/Code/Eventos/micasa/comedor/eventComunicadoPadCuentaProblema/eventComunicadoPadCuentaProblema.rpy:51
translate english eventComunicadoPadCuentaProblema_274e069c:

    # hermanMed "¡¡¡Lo vamos a pagar todos!!!"
    hermanMed "We're all going to pay for it!!!"

# game/Code/Eventos/micasa/comedor/eventComunicadoPadCuentaProblema/eventComunicadoPadCuentaProblema.rpy:55
translate english eventComunicadoPadCuentaProblema_716f4376:

    # pad "Lo siento. Es cierto, mi fallo también lo pagareis vosotros y lo siento mucho. A partir de ahora ya no podréis depender del dinero como antes."
    pad "I'm sorry. That's right, my mistake will also be paid by you and I'm really sorry. From now on you guys won't be able to depend on money like before."

# game/Code/Eventos/micasa/comedor/eventComunicadoPadCuentaProblema/eventComunicadoPadCuentaProblema.rpy:57
translate english eventComunicadoPadCuentaProblema_ff45ec6a:

    # pad "[nombreMay], sé que te prometí que pagaría la inversión en tu negocio, pero no podre cumplir esa promesa."
    pad "[nombreMay], I know I promised you that I would pay for the investment in your business, but I won't be able to keep that promise."

# game/Code/Eventos/micasa/comedor/eventComunicadoPadCuentaProblema/eventComunicadoPadCuentaProblema.rpy:59
translate english eventComunicadoPadCuentaProblema_9ab320f5:

    # hermanMay "Pero si acabo de pedir un crédito, no podre hacerme cargo yo sola. No tengo beneficios suficientes, acabo de abrir el negocio y solo tengo deudas por la inversión."
    hermanMay "But I just took out a loan, I won't be able to take care of it on my own. I don't have enough profit, I just opened the business and I only have debts for the investment."

# game/Code/Eventos/micasa/comedor/eventComunicadoPadCuentaProblema/eventComunicadoPadCuentaProblema.rpy:61
translate english eventComunicadoPadCuentaProblema_64b8d05f:

    # pad "Lo siento de verdad, soy consciente de que os he fallado."
    pad "I am truly sorry, I am aware that I have failed you."

# game/Code/Eventos/micasa/comedor/eventComunicadoPadCuentaProblema/eventComunicadoPadCuentaProblema.rpy:63
translate english eventComunicadoPadCuentaProblema_26261357:

    # pad "[nombreMed], a ti desgraciadamente también te afectara. No podre asumir otro año de tu master. Por suerte ya lo tienes pagado, pero tendrás que esforzarte más y aprobarlo."
    pad "[nombreMed], you will unfortunately be affected too. I will not be able to take on another year of your master's degree. Luckily you already have it paid for, but you will have to work harder and pass it."

# game/Code/Eventos/micasa/comedor/eventComunicadoPadCuentaProblema/eventComunicadoPadCuentaProblema.rpy:65
translate english eventComunicadoPadCuentaProblema_0957566c:

    # hermanMed "..."
    hermanMed "..."

# game/Code/Eventos/micasa/comedor/eventComunicadoPadCuentaProblema/eventComunicadoPadCuentaProblema.rpy:67
translate english eventComunicadoPadCuentaProblema_bc5b7eea:

    # pad "Lo siento Sofia, ya que por mi culpa tendrás que volver a trabajar. Por mucho que quiera es imposible encontrar un trabajo como el que tenía y que nos permita vivir como antes."
    pad "I'm sorry Sofia, because of me you will have to go back to work. As much as I want it's impossible to find a job like the one I had and that will allow us to live like before."

# game/Code/Eventos/micasa/comedor/eventComunicadoPadCuentaProblema/eventComunicadoPadCuentaProblema.rpy:69
translate english eventComunicadoPadCuentaProblema_315b4afb:

    # mad "..."
    mad "..."

# game/Code/Eventos/micasa/comedor/eventComunicadoPadCuentaProblema/eventComunicadoPadCuentaProblema.rpy:71
translate english eventComunicadoPadCuentaProblema_db1ff083:

    # pad "He hablado con mi primo George y me ha ofrecido un trabajo de conductor de camión. No es mucho, pero ayudara..."
    pad "I talked to my cousin George and he offered me a job as a truck driver. It's not much, but it will help..."

# game/Code/Eventos/micasa/comedor/eventComunicadoPadCuentaProblema/eventComunicadoPadCuentaProblema.rpy:72
translate english eventComunicadoPadCuentaProblema_21b89c02:

    # pad "Es todo muy precipitado lo sé. Pero no hay más tiempo y me tengo que marchar ahora."
    pad "It's all very rushed I know. But there is no more time and I have to leave now."

# game/Code/Eventos/micasa/comedor/eventComunicadoPadCuentaProblema/eventComunicadoPadCuentaProblema.rpy:78
translate english eventComunicadoPadCuentaProblema_b9fa35e5:

    # hermanMay "Como me has podido hacer esto. No sé qué clase de tontería habrás hecho, pero me has decepcionado y creo que no solo a mí."
    hermanMay "How could you do this to me. I don't know what kind of nonsense you've done, but you've disappointed me and I don't think it's just me."

# game/Code/Eventos/micasa/comedor/eventComunicadoPadCuentaProblema/eventComunicadoPadCuentaProblema.rpy:80
translate english eventComunicadoPadCuentaProblema_ac094a79:

    # pad "Lo siento, espero que podáis perdonarme."
    pad "I'm sorry, I hope you can forgive me."

# game/Code/Eventos/micasa/comedor/eventComunicadoPadCuentaProblema/eventComunicadoPadCuentaProblema.rpy:82
translate english eventComunicadoPadCuentaProblema_a131487d:

    # "Unos minutos mas tarde..."
    "A few minutes later..."

# game/Code/Eventos/micasa/comedor/eventComunicadoPadCuentaProblema/eventComunicadoPadCuentaProblema.rpy:84
translate english eventComunicadoPadCuentaProblema_19f09f93:

    # pad "Ya es hora de irme con mi primo."
    pad "It's time for me to leave with my cousin."

# game/Code/Eventos/micasa/comedor/eventComunicadoPadCuentaProblema/eventComunicadoPadCuentaProblema.rpy:88
translate english eventComunicadoPadCuentaProblema_c8d2e35b:

    # pad "[nombreProta2], ahora serás el hombre de la casa. Tendrás que cuidar de ellas."
    pad "[nombreProta2], you will now be the man of the house. You'll have to take care of them."

# game/Code/Eventos/micasa/comedor/eventComunicadoPadCuentaProblema/eventComunicadoPadCuentaProblema.rpy:89
translate english eventComunicadoPadCuentaProblema_cba4305c:

    # pad "Estoy seguro de que lo harás muy bien."
    pad "I'm sure you'll do just fine."

# game/Code/Eventos/micasa/comedor/eventComunicadoPadCuentaProblema/eventComunicadoPadCuentaProblema.rpy:90
translate english eventComunicadoPadCuentaProblema_0e5ee2bb:

    # pad "Bueno a excepción de Max, que aún va con pañales."
    pad "Well except for Max, he's still in diapers."

# game/Code/Eventos/micasa/comedor/eventComunicadoPadCuentaProblema/eventComunicadoPadCuentaProblema.rpy:94
translate english eventComunicadoPadCuentaProblema_157e2bec:

    # pad "Me voy, os iré informando."
    pad "I'm off, I'll keep you posted."

# game/Code/Eventos/micasa/comedor/eventComunicadoPadCuentaProblema/eventComunicadoPadCuentaProblema.rpy:96
translate english eventComunicadoPadCuentaProblema_2eb8cf79:

    # pad "Lo siento querida."
    pad "Sorry dear."

# game/Code/Eventos/micasa/comedor/eventComunicadoPadCuentaProblema/eventComunicadoPadCuentaProblema.rpy:103
translate english eventComunicadoPadCuentaProblema_6d3803c2:

    # protaPensa "[nombrePad3] ya se fue, y [nombreMad3] está muy enfadada. Ni le ha dirigido la palabra, pero es normal después de lo que hizo."
    protaPensa "[nombrePad3] has already left, and [nombreMad3] is very angry. She hasn't even said a word to him, but that's normal after what he did."

# game/Code/Eventos/micasa/comedor/eventComunicadoPadCuentaProblema/eventComunicadoPadCuentaProblema.rpy:105
translate english eventComunicadoPadCuentaProblema_3ddbfab4:

    # protaPensa "Necesito ir al baño para refrescarme un poco y aclarar la mente."
    protaPensa "I need to go to the bathroom to freshen up a bit and clear my head."

# game/Code/Eventos/micasa/comedor/eventComunicadoPadCuentaProblema/eventComunicadoPadCuentaProblema.rpy:113
translate english eventComunicadoPadCuentaProblema_f7ab7e49:

    # protaPensa "Ahora las cosas van a cambiar mucho..."
    protaPensa "Now things are going to change a lot..."

# game/Code/Eventos/micasa/comedor/eventComunicadoPadCuentaProblema/eventComunicadoPadCuentaProblema.rpy:115
translate english eventComunicadoPadCuentaProblema_4aaa4087:

    # pad "Ahora seras el hombre de la casa..."
    pad "Now you're going to be the man of the house..."

# game/Code/Eventos/micasa/comedor/eventComunicadoPadCuentaProblema/eventComunicadoPadCuentaProblema.rpy:117
translate english eventComunicadoPadCuentaProblema_20930c16:

    # protaPensa "Es cierto, voy a ser el hombre de la casa..."
    protaPensa "That's right, I'm going to be the man of the house..."

# game/Code/Eventos/micasa/comedor/eventComunicadoPadCuentaProblema/eventComunicadoPadCuentaProblema.rpy:119
translate english eventComunicadoPadCuentaProblema_c87eb553:

    # protaPensa "..."
    protaPensa "..."

# game/Code/Eventos/micasa/comedor/eventComunicadoPadCuentaProblema/eventComunicadoPadCuentaProblema.rpy:123
translate english eventComunicadoPadCuentaProblema_0d64ee29:

    # protaPensa "Otra vez ese sueño..."
    protaPensa "There's that dream again..."

# game/Code/Eventos/micasa/comedor/eventComunicadoPadCuentaProblema/eventComunicadoPadCuentaProblema.rpy:125
translate english eventComunicadoPadCuentaProblema_328a814a:

    # mad "Lo quieres?"
    mad "You want it?"

# game/Code/Eventos/micasa/comedor/eventComunicadoPadCuentaProblema/eventComunicadoPadCuentaProblema.rpy:127
translate english eventComunicadoPadCuentaProblema_b6ef79d3:

    # protaPensa "No puedo dejar de pensar en eso..."
    protaPensa "I can't stop thinking about it..."

# game/Code/Eventos/micasa/comedor/eventComunicadoPadCuentaProblema/eventComunicadoPadCuentaProblema.rpy:129
translate english eventComunicadoPadCuentaProblema_4aaa4087_1:

    # pad "Ahora seras el hombre de la casa..."
    pad "Now you'll be the man of the house..."

# game/Code/Eventos/micasa/comedor/eventComunicadoPadCuentaProblema/eventComunicadoPadCuentaProblema.rpy:137
translate english eventComunicadoPadCuentaProblema_4bbe77ba:

    # protaPensa "Oh dios no puedo parar..."
    protaPensa "Oh god I can't stop..."

# game/Code/Eventos/micasa/comedor/eventComunicadoPadCuentaProblema/eventComunicadoPadCuentaProblema.rpy:141
translate english eventComunicadoPadCuentaProblema_c87eb553_1:

    # protaPensa "..."
    protaPensa "..."

# game/Code/Eventos/micasa/comedor/eventComunicadoPadCuentaProblema/eventComunicadoPadCuentaProblema.rpy:143
translate english eventComunicadoPadCuentaProblema_378186ec:

    # prota "Y como os iba diciendo..."
    prota "And as I was telling you..."

# game/Code/Eventos/micasa/comedor/eventComunicadoPadCuentaProblema/eventComunicadoPadCuentaProblema.rpy:145
translate english eventComunicadoPadCuentaProblema_9bf37216:

    # prota "Así es como empieza una nueva etapa."
    prota "This is how a new phase begins."

# game/Code/Eventos/micasa/comedor/eventComunicadoPadCuentaProblema/eventComunicadoPadCuentaProblema.rpy:153
translate english eventComunicadoPadCuentaProblema_3842a9ae:

    # "FIN VERSIÓN 0.2"
    "END VERSION 0.2"
