﻿# TODO: Translation updated at 2022-03-27 17:09

# game/Code/Eventos/micasa/entradaCasa/eventEscenaSofiaKaraPruebaTraje/eventEscenaSofiaKaraPruebaTraje.rpy:7
translate english eventEscenaSofiaKaraPruebaTraje_a80a2fb2:

    # protaPensa "Oigo voces en el comedor de [nombreMad3] y Kara."
    protaPensa "I hear voices in the dining room, [nombreMad3] and Kara."

# game/Code/Eventos/micasa/entradaCasa/eventEscenaSofiaKaraPruebaTraje/eventEscenaSofiaKaraPruebaTraje.rpy:14
translate english eventEscenaSofiaKaraPruebaTraje_9c3489d2:

    # hermanMay "Venga vamos a tu habitación y pruébatelo."
    hermanMay "Come on. Come to your room and try it."

# game/Code/Eventos/micasa/entradaCasa/eventEscenaSofiaKaraPruebaTraje/eventEscenaSofiaKaraPruebaTraje.rpy:24
translate english eventEscenaSofiaKaraPruebaTraje_f5993c64:

    # mad "No deberías haberte gastado dinero. Soy consciente que acabas de abrir el negocio y que papa se fue sin ayudarte con los pagos como te prometió."
    mad "You shouldn't have spent money on me. I am aware that you have just opened the business and that your father left without helping you with the payments as he promised."

# game/Code/Eventos/micasa/entradaCasa/eventEscenaSofiaKaraPruebaTraje/eventEscenaSofiaKaraPruebaTraje.rpy:26
translate english eventEscenaSofiaKaraPruebaTraje_1f599513:

    # hermanMay "Mejor no hablemos de eso. A parte tenías los pantalones de yoga un poco gastados y no me importa hacerle un regalo a mi madre."
    hermanMay "We better not talk about it. Apart from that, your yoga pants are a little worn out and I don't mind giving my mother a gift."

# game/Code/Eventos/micasa/entradaCasa/eventEscenaSofiaKaraPruebaTraje/eventEscenaSofiaKaraPruebaTraje.rpy:32
translate english eventEscenaSofiaKaraPruebaTraje_89601c71:

    # hermanMay "Mama, sigues teniendo un cuerpo impresionante. Tienes los pechos de una adolescente y eso que Max siempre esta enganchado a tu teta. Si quisieras podrías volver a modelar."
    hermanMay "Mama, you still have an impressive body. You have the breasts of a teenager and max is always hooked to your tits. If you wanted you could go back to modeling."

# game/Code/Eventos/micasa/entradaCasa/eventEscenaSofiaKaraPruebaTraje/eventEscenaSofiaKaraPruebaTraje.rpy:34
translate english eventEscenaSofiaKaraPruebaTraje_b6b5e7a7:

    # mad "No digas tonterías."
    mad "Don't talk nonsense."

# game/Code/Eventos/micasa/entradaCasa/eventEscenaSofiaKaraPruebaTraje/eventEscenaSofiaKaraPruebaTraje.rpy:36
translate english eventEscenaSofiaKaraPruebaTraje_52ff45e4:

    # hermanMay "Déjamelas tocar."
    hermanMay "Let me touch them."

# game/Code/Eventos/micasa/entradaCasa/eventEscenaSofiaKaraPruebaTraje/eventEscenaSofiaKaraPruebaTraje.rpy:40
translate english eventEscenaSofiaKaraPruebaTraje_d5582551:

    # hermanMay "Como se nota que están hinchadas y llenas de leche. Max estará contento."
    hermanMay "As you can tell they are swollen and full of milk. Max will be happy."

# game/Code/Eventos/micasa/entradaCasa/eventEscenaSofiaKaraPruebaTraje/eventEscenaSofiaKaraPruebaTraje.rpy:42
translate english eventEscenaSofiaKaraPruebaTraje_c87eb553:

    # protaPensa "..."
    protaPensa "..."

# game/Code/Eventos/micasa/entradaCasa/eventEscenaSofiaKaraPruebaTraje/eventEscenaSofiaKaraPruebaTraje.rpy:44
translate english eventEscenaSofiaKaraPruebaTraje_0a69aeb7:

    # mad "Venga deja de hacer tonterías y tráeme el equipo de gimnasia."
    mad "Come, stop messing around and bring me the gym equipment."

# game/Code/Eventos/micasa/entradaCasa/eventEscenaSofiaKaraPruebaTraje/eventEscenaSofiaKaraPruebaTraje.rpy:46
translate english eventEscenaSofiaKaraPruebaTraje_61207694:

    # hermanMay "Que suerte tienes mama de tener este cuerpo. Ahora que no está papa tendrás que ir con cuidado. Hahaha"
    hermanMay "How lucky you are, Mom, to have this body. Now that Dad is gone you will have to be careful. Hahaha!"

# game/Code/Eventos/micasa/entradaCasa/eventEscenaSofiaKaraPruebaTraje/eventEscenaSofiaKaraPruebaTraje.rpy:48
translate english eventEscenaSofiaKaraPruebaTraje_c9697220:

    # hermanMay "Se quedo la puerta medio abierta."
    hermanMay "The door was left half open."

# game/Code/Eventos/micasa/entradaCasa/eventEscenaSofiaKaraPruebaTraje/eventEscenaSofiaKaraPruebaTraje.rpy:54
translate english eventEscenaSofiaKaraPruebaTraje_009daefa:

    # protaPensa "Uff! Por Poco..."
    protaPensa "Uff! So close..."

# game/Code/Eventos/micasa/entradaCasa/eventEscenaSofiaKaraPruebaTraje/eventEscenaSofiaKaraPruebaTraje.rpy:55
translate english eventEscenaSofiaKaraPruebaTraje_69ae627a:

    # "FIN VERSION 0.5 SOFIA"
    "END VERSION 0.5 SOFIA"
