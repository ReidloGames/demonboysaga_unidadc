﻿# TODO: Translation updated at 2021-12-12 13:06

# game/Code/Eventos/micasa/pasillo1/eventMaySeHaDejadoLLavesTienda/eventMaySeHaDejadoLLavesTienda.rpy:8
translate english eventMaySeHaDejadoLLavesTienda_c71c2190:

    # "¡RIIIING!"
    "RIIIING!"

# game/Code/Eventos/micasa/pasillo1/eventMaySeHaDejadoLLavesTienda/eventMaySeHaDejadoLLavesTienda.rpy:11
translate english eventMaySeHaDejadoLLavesTienda_019e2f8c:

    # hermanMay "Hola [nombreProta], soy Kara."
    hermanMay "Hi [nombreProta], It's me. Kara."

# game/Code/Eventos/micasa/pasillo1/eventMaySeHaDejadoLLavesTienda/eventMaySeHaDejadoLLavesTienda.rpy:13
translate english eventMaySeHaDejadoLLavesTienda_7b8db449:

    # prota "Hola Kara"
    prota "Hi Kara."

# game/Code/Eventos/micasa/pasillo1/eventMaySeHaDejadoLLavesTienda/eventMaySeHaDejadoLLavesTienda.rpy:14
translate english eventMaySeHaDejadoLLavesTienda_c9689330:

    # hermanMay "Necesito que me hagas un favor. Creo que me dejado el bolso con las llaves en mi habitación y no puedo abrir la tienda."
    hermanMay "I need you to do me a favor. I think I left my purse with my keys in my room, and I can't open the store."

# game/Code/Eventos/micasa/pasillo1/eventMaySeHaDejadoLLavesTienda/eventMaySeHaDejadoLLavesTienda.rpy:16
translate english eventMaySeHaDejadoLLavesTienda_c6bf79b0:

    # hermanMay "Puedes comprobarlo?"
    hermanMay "Can you check?"

# game/Code/Eventos/micasa/pasillo1/eventMaySeHaDejadoLLavesTienda/eventMaySeHaDejadoLLavesTienda.rpy:18
translate english eventMaySeHaDejadoLLavesTienda_7483ad6c:

    # prota "¡Claro!"
    prota "Sure!"

# game/Code/Eventos/micasa/pasillo1/eventMaySeHaDejadoLLavesTienda/eventMaySeHaDejadoLLavesTienda.rpy:22
translate english eventMaySeHaDejadoLLavesTienda_23b358af:

    # protaPensa "Aquí está el bolso."
    protaPensa "Here's the purse."

# game/Code/Eventos/micasa/pasillo1/eventMaySeHaDejadoLLavesTienda/eventMaySeHaDejadoLLavesTienda.rpy:25
translate english eventMaySeHaDejadoLLavesTienda_6b9e7226:

    # protaPensa "A ver que tenemos por aquí..."
    protaPensa "Let's see what we have over here..."

# game/Code/Eventos/micasa/pasillo1/eventMaySeHaDejadoLLavesTienda/eventMaySeHaDejadoLLavesTienda.rpy:31
translate english eventMaySeHaDejadoLLavesTienda_9b0890e8:

    # protaPensa "Un támpax... jejeje"
    protaPensa "A tampax... hehehehe."

# game/Code/Eventos/micasa/pasillo1/eventMaySeHaDejadoLLavesTienda/eventMaySeHaDejadoLLavesTienda.rpy:34
translate english eventMaySeHaDejadoLLavesTienda_f6d940ec:

    # protaPensa "Un spray pimienta..."
    protaPensa "A pepper spray..."

# game/Code/Eventos/micasa/pasillo1/eventMaySeHaDejadoLLavesTienda/eventMaySeHaDejadoLLavesTienda.rpy:39
translate english eventMaySeHaDejadoLLavesTienda_adc1441e:

    # protaPensa "¡Aquí están las llaves!"
    protaPensa "Here are the keys!"

# game/Code/Eventos/micasa/pasillo1/eventMaySeHaDejadoLLavesTienda/eventMaySeHaDejadoLLavesTienda.rpy:42
translate english eventMaySeHaDejadoLLavesTienda_67a0a290:

    # protaPensa "¡Y la llave de su habitación!"
    protaPensa "And the key to her room!"

# game/Code/Eventos/micasa/pasillo1/eventMaySeHaDejadoLLavesTienda/eventMaySeHaDejadoLLavesTienda.rpy:43
translate english eventMaySeHaDejadoLLavesTienda_41562ca0:

    # protaPensa "Me voy a llevarle el bolso, pero antes pasare por el cerrajero para hacer una copia de la llave."
    protaPensa "I'm going to take her purse, but first I'll stop by the locksmith to make a copy of the key."

# game/Code/Eventos/micasa/pasillo1/eventMaySeHaDejadoLLavesTienda/eventMaySeHaDejadoLLavesTienda.rpy:45
translate english eventMaySeHaDejadoLLavesTienda_8cef71ea:

    # protaPensa "Hoy es mi día de suerte..."
    protaPensa "Today is my lucky day..."

# game/Code/Eventos/micasa/pasillo1/eventMaySeHaDejadoLLavesTienda/eventMaySeHaDejadoLLavesTienda.rpy:47
translate english eventMaySeHaDejadoLLavesTienda_3b020a3d:

    # narrador "Ahora tienes una nueva zona en el mapa."
    narrador "Now you have a new area on the map."

# game/Code/Eventos/micasa/pasillo1/eventMaySeHaDejadoLLavesTienda/eventMaySeHaDejadoLLavesTienda.rpy:50
translate english eventMaySeHaDejadoLLavesTienda_a131487d:

    # "Unos minutos mas tarde..."
    "A few minutes later..."

# game/Code/Eventos/micasa/pasillo1/eventMaySeHaDejadoLLavesTienda/eventMaySeHaDejadoLLavesTienda.rpy:56
translate english eventMaySeHaDejadoLLavesTienda_4d3aef01:

    # hermanMay "Muchas gracias por hacerme el favor."
    hermanMay "Thank you so much for doing me the favor."

# game/Code/Eventos/micasa/pasillo1/eventMaySeHaDejadoLLavesTienda/eventMaySeHaDejadoLLavesTienda.rpy:62
translate english eventMaySeHaDejadoLLavesTienda_30135d53:

    # prota "Ya sabes que me encanta ayudarte..."
    prota "You know I love to help you..."
