﻿# TODO: Translation updated at 2022-02-07 21:19

# game/Code/Eventos/micasa/hermanaPeq/eventDiarioPersonalPeq/eventDiarioPersonalPeq.rpy:8
translate english eventDiarioPersonalPeq_910af7be:

    # protaPensa "¿Qué hace Dana?"
    protaPensa "What's Dana up to?"#What does Dana do?

# game/Code/Eventos/micasa/hermanaPeq/eventDiarioPersonalPeq/eventDiarioPersonalPeq.rpy:14
translate english eventDiarioPersonalPeq_d839faff:

    # protaPensa "¿Es su diario personal?"
    protaPensa "Is that her personal diary?"#Is it your personal diary?

# game/Code/Eventos/micasa/hermanaPeq/eventDiarioPersonalPeq/eventDiarioPersonalPeq.rpy:18
translate english eventDiarioPersonalPeq_dbe9dc22:

    # protaPensa "Necesito ver ese diario..."
    protaPensa "I need to see that diary..."
