﻿# TODO: Translation updated at 2021-10-13 15:38

# game/Code/Eventos/micasa/miHabitacion/eventEncuentraTrabajo/eventEncuentraTrabajo.rpy:7
translate english eventEncuentraTrabajo_9f82f30f:

    # protaPensa "Voy a buscar en internet algún empleo"
    protaPensa "I'm going to search the internet for a job."

# game/Code/Eventos/micasa/miHabitacion/eventEncuentraTrabajo/eventEncuentraTrabajo.rpy:10
translate english eventEncuentraTrabajo_6755280c:

    # protaPensa "Buscando en Coocle..."
    protaPensa "Searching on Coocle..."

# game/Code/Eventos/micasa/miHabitacion/eventEncuentraTrabajo/eventEncuentraTrabajo.rpy:14
translate english eventEncuentraTrabajo_e2d41e0e:

    # protaPensa "Buscando empleos..."
    protaPensa "Looking for jobs..."

# game/Code/Eventos/micasa/miHabitacion/eventEncuentraTrabajo/eventEncuentraTrabajo.rpy:16
translate english eventEncuentraTrabajo_528ae1b6:

    # protaPensa "Aquí hay uno de pasear perros, pero voy a buscar un poco más. Paso de recoger mierdas."
    protaPensa "Here's a dog-walking one, but I'm going to look some more. I'll pass on picking up shit."

# game/Code/Eventos/micasa/miHabitacion/eventEncuentraTrabajo/eventEncuentraTrabajo.rpy:19
translate english eventEncuentraTrabajo_e9fe7485:

    # protaPensa "mmmm... Repartidor de paquetes."
    protaPensa "mmmm.... Package delivery guy."

# game/Code/Eventos/micasa/miHabitacion/eventEncuentraTrabajo/eventEncuentraTrabajo.rpy:20
translate english eventEncuentraTrabajo_d68bf1c8:

    # protaPensa "Este creo que me podría ir bien."
    protaPensa "This one I think I could use."

# game/Code/Eventos/micasa/miHabitacion/eventEncuentraTrabajo/eventEncuentraTrabajo.rpy:21
translate english eventEncuentraTrabajo_d5adcf60:

    # "Petición enviada"
    "Request sent."

# game/Code/Eventos/micasa/miHabitacion/eventEncuentraTrabajo/eventEncuentraTrabajo.rpy:22
translate english eventEncuentraTrabajo_f2fb33c5:

    # protaPensa "Vale, ahora a esperar a que me digan algo..."
    protaPensa "Okay, now I'll wait to hear from them..."

# game/Code/Eventos/micasa/miHabitacion/eventEncuentraTrabajo/eventEncuentraTrabajo.rpy:24
translate english eventEncuentraTrabajo_758dc893:

    # protaPensa "¿Esto que es?"
    protaPensa "What is this?"

# game/Code/Eventos/micasa/miHabitacion/eventEncuentraTrabajo/eventEncuentraTrabajo.rpy:26
translate english eventEncuentraTrabajo_3f6a10f6:

    # protaPensa "Parece una promoción para un curso de masajes."
    protaPensa "Looks like a promotion for a massage course."

# game/Code/Eventos/micasa/miHabitacion/eventEncuentraTrabajo/eventEncuentraTrabajo.rpy:28
translate english eventEncuentraTrabajo_45d4b8ac:

    # "Click"
    "Click"

# game/Code/Eventos/micasa/miHabitacion/eventEncuentraTrabajo/eventEncuentraTrabajo.rpy:33
translate english eventEncuentraTrabajo_2982e038:

    # mandin "Soy Mandin el chino mandarín."
    mandin "I am Mandin the Mandarin Chinese."

# game/Code/Eventos/micasa/miHabitacion/eventEncuentraTrabajo/eventEncuentraTrabajo.rpy:35
translate english eventEncuentraTrabajo_321e046b:

    # mandin "Experto en masajes."
    mandin "Massage expert."

# game/Code/Eventos/micasa/miHabitacion/eventEncuentraTrabajo/eventEncuentraTrabajo.rpy:37
translate english eventEncuentraTrabajo_849f21db:

    # mandin "Mis manos son un arte"
    mandin "My hands are art."

# game/Code/Eventos/micasa/miHabitacion/eventEncuentraTrabajo/eventEncuentraTrabajo.rpy:39
translate english eventEncuentraTrabajo_8db43cbc:

    # mandin "Si tu mujeres querer"
    mandin "If you women want"

# game/Code/Eventos/micasa/miHabitacion/eventEncuentraTrabajo/eventEncuentraTrabajo.rpy:43
translate english eventEncuentraTrabajo_e5f738b1:

    # mandin "Haz mis cursos, no te los podrás perder."
    mandin "Do my courses, you can't miss them."

# game/Code/Eventos/micasa/miHabitacion/eventEncuentraTrabajo/eventEncuentraTrabajo.rpy:48
translate english eventEncuentraTrabajo_2041e03f:

    # protaPensa "Ohhh, primer curso gratis."
    protaPensa "Ohhh, first free course."

# game/Code/Eventos/micasa/miHabitacion/eventEncuentraTrabajo/eventEncuentraTrabajo.rpy:50
translate english eventEncuentraTrabajo_dbe66be5:

    # protaPensa "Espera que me lo pienso..."
    protaPensa "Wait I'm thinking about it..."

# game/Code/Eventos/micasa/miHabitacion/eventEncuentraTrabajo/eventEncuentraTrabajo.rpy:52
translate english eventEncuentraTrabajo_3ba43241:

    # protaPensa "¡¡¡Apuntado!!!"
    protaPensa "Aim!!!"

# game/Code/Eventos/micasa/miHabitacion/eventEncuentraTrabajo/eventEncuentraTrabajo.rpy:54
translate english eventEncuentraTrabajo_ebd067db:

    # protaPensa "Ahora puedo hacer el primer curso desde el ordenador."
    protaPensa "Now I can do the first course from the computer."

# game/Code/Eventos/micasa/miHabitacion/eventEncuentraTrabajo/eventEncuentraTrabajo.rpy:58
translate english eventEncuentraTrabajo_959b5876:

    # protaPensa "Acabo de recibir un mail..."
    protaPensa "I just received an email..."

# game/Code/Eventos/micasa/miHabitacion/eventEncuentraTrabajo/eventEncuentraTrabajo.rpy:60
translate english eventEncuentraTrabajo_c42e031d:

    # "Hola, somos Amarzond, acabamos de ver su petición para trabajar con nosotros. Puede venir a la dirección adjuntada en el mail y hablaremos de ello."
    "Hi, we are Amarzond, we just saw your request to work with us. You can come to the address attached in the mail and we will talk about it."

# game/Code/Eventos/micasa/miHabitacion/eventEncuentraTrabajo/eventEncuentraTrabajo.rpy:61
translate english eventEncuentraTrabajo_e399d766:

    # "Te esperamos, un cordial saludo."
    "We are waiting for you, best regards."

# game/Code/Eventos/micasa/miHabitacion/eventEncuentraTrabajo/eventEncuentraTrabajo.rpy:64
translate english eventEncuentraTrabajo_3b020a3d:

    # narrador "Ahora tienes una nueva zona en el mapa."
    narrador "Now you have a new area on the map."

# game/Code/Eventos/micasa/miHabitacion/eventEncuentraTrabajo/eventEncuentraTrabajo.rpy:67
translate english eventEncuentraTrabajo_0eaddcaf:

    # protaPensa "¡Genial! parece que el destino me sonrie."
    protaPensa "Great! It looks like fate is smiling on me."

# game/Code/Eventos/micasa/miHabitacion/eventEncuentraTrabajo/eventEncuentraTrabajo.rpy:70
translate english eventEncuentraTrabajo_f4e3e9b0:

    # narrador "Ahora tienes disponible un nuevo curso."
    narrador "You now have a new course available."
