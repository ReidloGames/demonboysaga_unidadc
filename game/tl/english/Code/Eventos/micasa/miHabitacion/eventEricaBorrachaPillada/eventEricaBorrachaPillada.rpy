﻿# TODO: Translation updated at 2021-12-12 13:06

# game/Code/Eventos/micasa/miHabitacion/eventEricaBorrachaPillada/eventEricaBorrachaPillada.rpy:11
translate english eventEricaBorrachaPillada_c41ee98d:

    # "Información"
    "Information."

# game/Code/Eventos/micasa/miHabitacion/eventEricaBorrachaPillada/eventEricaBorrachaPillada.rpy:13
translate english eventEricaBorrachaPillada_0ff33e7e:

    # "El NTR está desactivado."
    "The NTR is disabled."

# game/Code/Eventos/micasa/miHabitacion/eventEricaBorrachaPillada/eventEricaBorrachaPillada.rpy:14
translate english eventEricaBorrachaPillada_86599ec5:

    # "Quieres Activar el NTR?"
    "Do you want to Activate NTR?"

# game/Code/Eventos/micasa/miHabitacion/eventEricaBorrachaPillada/eventEricaBorrachaPillada.rpy:18
translate english eventEricaBorrachaPillada_87e42c30:

    # "NTR activado"
    "NTR activated"

# game/Code/Eventos/micasa/miHabitacion/eventEricaBorrachaPillada/eventEricaBorrachaPillada.rpy:21
translate english eventEricaBorrachaPillada_cb9c6842:

    # "NTR desactivado"
    "NTR deactivated"

# game/Code/Eventos/micasa/miHabitacion/eventEricaBorrachaPillada/eventEricaBorrachaPillada.rpy:23
translate english eventEricaBorrachaPillada_93d6b529:

    # "El NTR está activado."
    "NTR is activated."

# game/Code/Eventos/micasa/miHabitacion/eventEricaBorrachaPillada/eventEricaBorrachaPillada.rpy:24
translate english eventEricaBorrachaPillada_3f99edf3:

    # "Quieres Desactivar el NTR?"
    "Do you want to Disable NTR?"

# game/Code/Eventos/micasa/miHabitacion/eventEricaBorrachaPillada/eventEricaBorrachaPillada.rpy:28
translate english eventEricaBorrachaPillada_cb9c6842_1:

    # "NTR desactivado"
    "NTR deactivated."

# game/Code/Eventos/micasa/miHabitacion/eventEricaBorrachaPillada/eventEricaBorrachaPillada.rpy:31
translate english eventEricaBorrachaPillada_87e42c30_1:

    # "NTR activado"
    "NTR enabled."

# game/Code/Eventos/micasa/miHabitacion/eventEricaBorrachaPillada/eventEricaBorrachaPillada.rpy:37
translate english eventEricaBorrachaPillada_538e1541:

    # "¡¡Pum!!"
    "Pum!!!"

# game/Code/Eventos/micasa/miHabitacion/eventEricaBorrachaPillada/eventEricaBorrachaPillada.rpy:41
translate english eventEricaBorrachaPillada_a62e3a5b:

    # protaPensa "¿Que ha sido ese ruido?"
    protaPensa "What was that noise?"

# game/Code/Eventos/micasa/miHabitacion/eventEricaBorrachaPillada/eventEricaBorrachaPillada.rpy:62
translate english eventEricaBorrachaPilladaInvestiga_1519e8f6:

    # "¡Click!"
    "Click!"

# game/Code/Eventos/micasa/miHabitacion/eventEricaBorrachaPillada/eventEricaBorrachaPillada.rpy:65
translate english eventEricaBorrachaPilladaInvestiga_18d3cb86:

    # protaPensa "¡¡Pero si es [nombreMed]!! ¡Se le ven las bragas!"
    protaPensa "But it's [nombreMed]!!! You can see her panties!"

# game/Code/Eventos/micasa/miHabitacion/eventEricaBorrachaPillada/eventEricaBorrachaPillada.rpy:67
translate english eventEricaBorrachaPilladaInvestiga_c78f25c9:

    # protaPensa "¿Que hace en el suelo? ¿Esta borracha?"
    protaPensa "What's she doing on the floor? Is she drunk?"

# game/Code/Eventos/micasa/miHabitacion/eventEricaBorrachaPillada/eventEricaBorrachaPillada.rpy:81
translate english eventEricaBorrachaPilladaInvestiga_c9323d37:

    # hermanMed "¡Ey tonto! que miiiras."
    hermanMed "Hey dummy! What are you looking at."

# game/Code/Eventos/micasa/miHabitacion/eventEricaBorrachaPillada/eventEricaBorrachaPillada.rpy:83
translate english eventEricaBorrachaPilladaInvestiga_18e20435:

    # hermanMed "Ayudaaame a leeevantarmeee... hip"
    hermanMed "Help me get up..... hip"

# game/Code/Eventos/micasa/miHabitacion/eventEricaBorrachaPillada/eventEricaBorrachaPillada.rpy:85
translate english eventEricaBorrachaPilladaInvestiga_d61ab2df:

    # hermanMed "Todo da vueltas..."
    hermanMed "Everything is spinning..."

# game/Code/Eventos/micasa/miHabitacion/eventEricaBorrachaPillada/eventEricaBorrachaPillada.rpy:87
translate english eventEricaBorrachaPilladaInvestiga_b0fb3fda:

    # protaPensa "¡¡Oh Dios!! no puedo aguantar más, me lo está sirviendo en bandeja."
    protaPensa "Oh God!!! I can't take it anymore, he's serving it to me on a platter."

# game/Code/Eventos/micasa/miHabitacion/eventEricaBorrachaPillada/eventEricaBorrachaPillada.rpy:90
translate english eventEricaBorrachaPilladaInvestiga_d6602c0b:

    # prota "SHHH..."
    prota "SHHH..."

# game/Code/Eventos/micasa/miHabitacion/eventEricaBorrachaPillada/eventEricaBorrachaPillada.rpy:92
translate english eventEricaBorrachaPilladaInvestiga_2ea6f415:

    # prota "Te ayudo, pero no hagas ruido o se despertaran todos."
    prota "I'll help you, but don't make any noise or everyone will wake up."

# game/Code/Eventos/micasa/miHabitacion/eventEricaBorrachaPillada/eventEricaBorrachaPillada.rpy:96
translate english eventEricaBorrachaPilladaInvestiga_00f67207:

    # prota "Vamos al salón, para que se te pase el mareo."
    prota "Let's go to the living room, so your dizziness will pass."

# game/Code/Eventos/micasa/miHabitacion/eventEricaBorrachaPillada/eventEricaBorrachaPillada.rpy:100
translate english eventEricaBorrachaPilladaInvestiga_7a010a29:

    # prota "Túmbate en el sofá a ver si te mejoras."
    prota "Lie down on the couch and see if you get better."

# game/Code/Eventos/micasa/miHabitacion/eventEricaBorrachaPillada/eventEricaBorrachaPillada.rpy:104
translate english eventEricaBorrachaPilladaInvestiga_0e814318:

    # hermanMed "eeeee si..."
    hermanMed "eeeee yeah..."

# game/Code/Eventos/micasa/miHabitacion/eventEricaBorrachaPillada/eventEricaBorrachaPillada.rpy:108
translate english eventEricaBorrachaPilladaInvestiga_72d1ea70:

    # hermanMed "Tengo sueño..."
    hermanMed "I'm sleepy..."

# game/Code/Eventos/micasa/miHabitacion/eventEricaBorrachaPillada/eventEricaBorrachaPillada.rpy:112
translate english eventEricaBorrachaPilladaInvestiga_38f17300:

    # protaPensa "Oh dios mío... mira como esta..."
    protaPensa "Oh my god... look how he is..."

# game/Code/Eventos/micasa/miHabitacion/eventEricaBorrachaPillada/eventEricaBorrachaPillada.rpy:118
translate english eventEricaBorrachaPilladaInvestiga_41e12b04:

    # prota "Estira las piernas para que descanses bien..."
    prota "Stretch your legs so you can get a good rest..."

# game/Code/Eventos/micasa/miHabitacion/eventEricaBorrachaPillada/eventEricaBorrachaPillada.rpy:135
translate english eventEricaBorrachaPilladaInvestiga_3ee261d0:

    # protaPensa "¿Esta dormida? Con lo borracha que esta dudo mucho que se despierte."
    protaPensa "Is she asleep? As drunk as she is I doubt she'll wake up."

# game/Code/Eventos/micasa/miHabitacion/eventEricaBorrachaPillada/eventEricaBorrachaPillada.rpy:139
translate english eventEricaBorrachaPilladaInvestiga_b38fd87b:

    # prota "¿Erica, estas despierta?"
    prota "Erica, are you awake?"

# game/Code/Eventos/micasa/miHabitacion/eventEricaBorrachaPillada/eventEricaBorrachaPillada.rpy:145
translate english eventEricaBorrachaPilladaInvestiga_e764e788:

    # protaPensa "Oh joder! Esta es mi oportunidad..."
    protaPensa "Oh fuck! This is my chance..."

# game/Code/Eventos/micasa/miHabitacion/eventEricaBorrachaPillada/eventEricaBorrachaPillada.rpy:165
translate english eventEricaBorrachaPilladaPechoDerecho_6bd211d6:

    # protaPensa "No te despiertes, por favor... "
    protaPensa "Don't wake up, please...."

# game/Code/Eventos/micasa/miHabitacion/eventEricaBorrachaPillada/eventEricaBorrachaPillada.rpy:174
translate english eventEricaBorrachaPilladaPechoDerecho_765f5dd3:

    # protaPensa "Que tetas más increíbles."
    protaPensa "What amazing tits."

# game/Code/Eventos/micasa/miHabitacion/eventEricaBorrachaPillada/eventEricaBorrachaPillada.rpy:176
translate english eventEricaBorrachaPilladaPechoDerecho_cbe14f24:

    # protaPensa "Está totalmente dormida."
    protaPensa "She's totally asleep."

# game/Code/Eventos/micasa/miHabitacion/eventEricaBorrachaPillada/eventEricaBorrachaPillada.rpy:178
translate english eventEricaBorrachaPilladaPechoDerecho_f5cb56f2:

    # protaPensa "No me puedo creer que le esté tocando los pechos a mi [nombreMed3]. Podría estar toda la noche así."
    protaPensa "I can't believe I'm touching my [nombreMed3]'s breasts. I could go all night like this."

# game/Code/Eventos/micasa/miHabitacion/eventEricaBorrachaPillada/eventEricaBorrachaPillada.rpy:180
translate english eventEricaBorrachaPilladaPechoDerecho_b6d5ce42:

    # protaPensa "Quiero ver estas tetas bien. Es improbable que se despierte tal y como esta."
    protaPensa "I want to get a good look at these boobs. She' s unlikely to wake up the way she is."

# game/Code/Eventos/micasa/miHabitacion/eventEricaBorrachaPillada/eventEricaBorrachaPillada.rpy:185
translate english eventEricaBorrachaPilladaPechoDerecho_c43aa9f1:

    # protaPensa "Impresionante."
    protaPensa "Awesome."

# game/Code/Eventos/micasa/miHabitacion/eventEricaBorrachaPillada/eventEricaBorrachaPillada.rpy:227
translate english eventEricaBorrachaPilladaPechoDerechojugarPechoDerecho1_f00e0187:

    # protaPensa "Esta dormida pero pone cara de placer.{p=1.5}{nw}"
    protaPensa "She's asleep but she's making a face of pleasure.{p=1.5}{nw}"

# game/Code/Eventos/micasa/miHabitacion/eventEricaBorrachaPillada/eventEricaBorrachaPillada.rpy:229
translate english eventEricaBorrachaPilladaPechoDerechojugarPechoDerecho1_8c93bf8b:

    # protaPensa "No puedo parar, me la follaría aquí mismo.{p=1.5}{nw}"
    protaPensa "I can't stop, I'd fuck her right here.{p=1.5}{nw}"

# game/Code/Eventos/micasa/miHabitacion/eventEricaBorrachaPillada/eventEricaBorrachaPillada.rpy:264
translate english eventEricaBorrachaPilladaPechoDerechojugarPechoDerecho2_751133d3:

    # protaPensa "Me encanta apretarlos {p=1.5}{nw}"
    protaPensa "I love to squeeze them {p=1.5}{nw}"

# game/Code/Eventos/micasa/miHabitacion/eventEricaBorrachaPillada/eventEricaBorrachaPillada.rpy:267
translate english eventEricaBorrachaPilladaPechoDerechojugarPechoDerecho2_252edaec:

    # protaPensa "Mira como se le ponen los pezones de hinchados. {p=1.5}{nw}"
    protaPensa "Look how perky her nipples get. {p=1.5}{nw}"

# game/Code/Eventos/micasa/miHabitacion/eventEricaBorrachaPillada/eventEricaBorrachaPillada.rpy:287
translate english eventEricaBorrachaPilladaPechoDerechojugarPechoDerecho3_19bb17c1:

    # "Que tetas tan redondas y grandes y estos pezones. Increíble...{p=1.5}{nw}"
    "What big round boobs and these nipples. Increíble...{p=1.5}{nw}"

# game/Code/Eventos/micasa/miHabitacion/eventEricaBorrachaPillada/eventEricaBorrachaPillada.rpy:289
translate english eventEricaBorrachaPilladaPechoDerechojugarPechoDerecho3_2671fdd1:

    # "Me encantan y parece que lo disfruta.{p=1.5}{nw}"
    "I love them and she seems to enjoy it.{p=1.5}{nw}"

# game/Code/Eventos/micasa/miHabitacion/eventEricaBorrachaPillada/eventEricaBorrachaPillada.rpy:304
translate english manosearPechoEricaBorrachaContinuar_5dcef7a4:

    # protaPensa "Tiene unos pechos perfectos."
    protaPensa "She has perfect breasts."

# game/Code/Eventos/micasa/miHabitacion/eventEricaBorrachaPillada/eventEricaBorrachaPillada.rpy:306
translate english manosearPechoEricaBorrachaContinuar_1116c997:

    # protaPensa "Se le han hinchado los pezones, seguro que está en su mejor sueño y lo ha disfrutado."
    protaPensa "Her nipples have swollen, I'm sure she's in her best dream and enjoyed it."

# game/Code/Eventos/micasa/miHabitacion/eventEricaBorrachaPillada/eventEricaBorrachaPillada.rpy:308
translate english manosearPechoEricaBorrachaContinuar_a8a42416:

    # protaPensa "¿Me muero de ganas de verle el coño, como lo tendrá? estará mojada? es mi gran oportunidad..."
    protaPensa "I can't wait to see her pussy, how will it look, will it be wet, it's my big chance..."

# game/Code/Eventos/micasa/miHabitacion/eventEricaBorrachaPillada/eventEricaBorrachaPillada.rpy:310
translate english manosearPechoEricaBorrachaContinuar_4c61bb0d:

    # protaPensa "Le pondré la parte de arriba del vestido, por si acaso..."
    protaPensa "I'll put the top of her dress on, just in case..."

# game/Code/Eventos/micasa/miHabitacion/eventEricaBorrachaPillada/eventEricaBorrachaPillada.rpy:313
translate english manosearPechoEricaBorrachaContinuar_89467c5c:

    # protaPensa "No puedo aguantar más..."
    protaPensa "I can't hnew on any longer..."

# game/Code/Eventos/micasa/miHabitacion/eventEricaBorrachaPillada/eventEricaBorrachaPillada.rpy:325
translate english manosearPechoEricaBorrachaContinuar_b3aa6300:

    # protaPensa "Así, no te despiertes."
    protaPensa "Like this, don't wake up."

# game/Code/Eventos/micasa/miHabitacion/eventEricaBorrachaPillada/eventEricaBorrachaPillada.rpy:328
translate english manosearPechoEricaBorrachaContinuar_88362bde:

    # protaPensa "Ábrete de piernas..."
    protaPensa "Spread your legs..."

# game/Code/Eventos/micasa/miHabitacion/eventEricaBorrachaPillada/eventEricaBorrachaPillada.rpy:336
translate english manosearPechoEricaBorrachaContinuar_c03cc25e:

    # protaPensa "Vamos a ver que escondes debajo de estas braguitas."
    protaPensa "Let's see what you're hiding under these panties."

# game/Code/Eventos/micasa/miHabitacion/eventEricaBorrachaPillada/eventEricaBorrachaPillada.rpy:343
translate english manosearPechoEricaBorrachaContinuar_09b48492:

    # protaPensa "Ya puedo verle los labios del coño. Es como si quisiera mostrarse."
    protaPensa "I can already see her pussy lips. It's like she wants to show off."

# game/Code/Eventos/micasa/miHabitacion/eventEricaBorrachaPillada/eventEricaBorrachaPillada.rpy:347
translate english manosearPechoEricaBorrachaContinuar_a08667cc:

    # protaPensa "Así... tranquila."
    protaPensa "Like that... easy."

# game/Code/Eventos/micasa/miHabitacion/eventEricaBorrachaPillada/eventEricaBorrachaPillada.rpy:352
translate english manosearPechoEricaBorrachaContinuar_89d6f8a9:

    # protaPensa "A ver este coñito..."
    protaPensa "Let's see this pussy..."

# game/Code/Eventos/micasa/miHabitacion/eventEricaBorrachaPillada/eventEricaBorrachaPillada.rpy:360
translate english manosearPechoEricaBorrachaContinuar_1f24b8e9:

    # protaPensa "Argh!!! ¿Qué es esto?"
    protaPensa "Argh!!! What is this?"

# game/Code/Eventos/micasa/miHabitacion/eventEricaBorrachaPillada/eventEricaBorrachaPillada.rpy:362
translate english manosearPechoEricaBorrachaContinuar_db573f72:

    # protaPensa "¡Esto es semen que puto asco!"
    protaPensa "This is semen how fucking gross!"

# game/Code/Eventos/micasa/miHabitacion/eventEricaBorrachaPillada/eventEricaBorrachaPillada.rpy:364
translate english manosearPechoEricaBorrachaContinuar_6185b3d8:

    # protaPensa "¡Sera zorra! ¡Se la han follado y tiene el coño que le esta goteando el semen!"
    protaPensa "She's a slut! She's been fucked and her pussy is dripping with cum!"

# game/Code/Eventos/micasa/miHabitacion/eventEricaBorrachaPillada/eventEricaBorrachaPillada.rpy:372
translate english manosearPechoEricaBorrachaContinuar_b823dcd2:

    # protaPensa "¡Joder, se está despertando!"
    protaPensa "Fuck, she's waking up!"

# game/Code/Eventos/micasa/miHabitacion/eventEricaBorrachaPillada/eventEricaBorrachaPillada.rpy:374
translate english manosearPechoEricaBorrachaContinuar_c2d7062f:

    # hermanMed "¿Que hago aquí?"
    hermanMed "What am I doing here?"

# game/Code/Eventos/micasa/miHabitacion/eventEricaBorrachaPillada/eventEricaBorrachaPillada.rpy:376
translate english manosearPechoEricaBorrachaContinuar_35157726:

    # prota "Estabas mareada y te tumbaste aquí para descansar."
    prota "You were dizzy and laid down here to rest."

# game/Code/Eventos/micasa/miHabitacion/eventEricaBorrachaPillada/eventEricaBorrachaPillada.rpy:378
translate english manosearPechoEricaBorrachaContinuar_61d9aaf0:

    # hermanMed "Me duele la cabeza."
    hermanMed "I have a headache."

# game/Code/Eventos/micasa/miHabitacion/eventEricaBorrachaPillada/eventEricaBorrachaPillada.rpy:380
translate english manosearPechoEricaBorrachaContinuar_9cbe12b4:

    # hermanMed "Me voy a dormir..."
    hermanMed "I'm going to sleep..."

# game/Code/Eventos/micasa/miHabitacion/eventEricaBorrachaPillada/eventEricaBorrachaPillada.rpy:385
translate english manosearPechoEricaBorrachaContinuar_0caccfad:

    # protaPensa "Estoy seguro de que alguien se ha follado a mi [nombreMed]. ¿Quién habrá sido?"
    protaPensa "I'm sure someone fucked my [nombreMed]. Who could it have been?"

# game/Code/Eventos/micasa/miHabitacion/eventEricaBorrachaPillada/eventEricaBorrachaPillada.rpy:393
translate english manosearPechoEricaBorrachaContinuar_b823dcd2_1:

    # protaPensa "¡Joder, se está despertando!"
    protaPensa "Fuck, she's waking up!"

# game/Code/Eventos/micasa/miHabitacion/eventEricaBorrachaPillada/eventEricaBorrachaPillada.rpy:401
translate english manosearPechoEricaBorrachaContinuar_c2d7062f_1:

    # hermanMed "¿Que hago aquí?"
    hermanMed "What am I doing here?"

# game/Code/Eventos/micasa/miHabitacion/eventEricaBorrachaPillada/eventEricaBorrachaPillada.rpy:403
translate english manosearPechoEricaBorrachaContinuar_35157726_1:

    # prota "Estabas mareada y te tumbaste aquí para descansar."
    prota "You were dizzy and laid down here to rest."

# game/Code/Eventos/micasa/miHabitacion/eventEricaBorrachaPillada/eventEricaBorrachaPillada.rpy:405
translate english manosearPechoEricaBorrachaContinuar_61d9aaf0_1:

    # hermanMed "Me duele la cabeza."
    hermanMed "I have a headache."

# game/Code/Eventos/micasa/miHabitacion/eventEricaBorrachaPillada/eventEricaBorrachaPillada.rpy:407
translate english manosearPechoEricaBorrachaContinuar_9cbe12b4_1:

    # hermanMed "Me voy a dormir..."
    hermanMed "I'm going to sleep..."

# game/Code/Eventos/micasa/miHabitacion/eventEricaBorrachaPillada/eventEricaBorrachaPillada.rpy:412
translate english manosearPechoEricaBorrachaContinuar_382dd82b:

    # protaPensa "Me ha faltado muy poco..."
    protaPensa "I was so close..."

# game/Code/Eventos/micasa/miHabitacion/eventEricaBorrachaPillada/eventEricaBorrachaPillada.rpy:415
translate english manosearPechoEricaBorrachaContinuar_393678d6:

    # protaPensa "Me quedado con las ganas de jugar con su coñito, pero sus tetas han sido impresionantes."
    protaPensa "I was left with a desire to play with her pussy, but her tits were awesome."

# game/Code/Eventos/micasa/miHabitacion/eventEricaBorrachaPillada/eventEricaBorrachaPillada.rpy:417
translate english manosearPechoEricaBorrachaContinuar_a0b28637:

    # protaPensa "Quiero más..."
    protaPensa "I want more..."

# game/Code/Eventos/micasa/miHabitacion/eventEricaBorrachaPillada/eventEricaBorrachaPillada.rpy:420
translate english manosearPechoEricaBorrachaContinuar_b399f744:

    # "Unas horas mas tarde..."
    "A few hours later..."

# game/Code/Eventos/micasa/miHabitacion/eventEricaBorrachaPillada/eventEricaBorrachaPillada.rpy:422
translate english manosearPechoEricaBorrachaContinuar_2c5a4b9b:

    # hermanMed "¡Despierta!"
    hermanMed "Wake up!"

# game/Code/Eventos/micasa/miHabitacion/eventEricaBorrachaPillada/eventEricaBorrachaPillada.rpy:426
translate english manosearPechoEricaBorrachaContinuar_dae9d573:

    # protaPensa "Pensé que era un sueño..."
    protaPensa "I thought it was a dream..."

# game/Code/Eventos/micasa/miHabitacion/eventEricaBorrachaPillada/eventEricaBorrachaPillada.rpy:428
translate english manosearPechoEricaBorrachaContinuar_70db6a23:

    # hermanMed "¿Qué paso ayer? Recuerdo que te vi y nada más..."
    hermanMed "What happened yesterday? I remember seeing you and nothing else..."

# game/Code/Eventos/micasa/miHabitacion/eventEricaBorrachaPillada/eventEricaBorrachaPillada.rpy:431
translate english manosearPechoEricaBorrachaContinuar_c2d9a815:

    # prota "AAaaaammmmmm...."
    prota "AAaaaammmmmmmm...."

# game/Code/Eventos/micasa/miHabitacion/eventEricaBorrachaPillada/eventEricaBorrachaPillada.rpy:433
translate english manosearPechoEricaBorrachaContinuar_eda0567f:

    # prota "Te encontré tirada en el suelo, estabas más que borracha. ¿Qué hiciste ayer? ¿Te lo pasaste bien?"
    prota "I found you lying on the floor, you were more than drunk, what did you do yesterday, did you have a good time?"

# game/Code/Eventos/micasa/miHabitacion/eventEricaBorrachaPillada/eventEricaBorrachaPillada.rpy:435
translate english manosearPechoEricaBorrachaContinuar_1f37e30d:

    # hermanMed "¡Cállate!"
    hermanMed "Shut up!"

# game/Code/Eventos/micasa/miHabitacion/eventEricaBorrachaPillada/eventEricaBorrachaPillada.rpy:437
translate english manosearPechoEricaBorrachaContinuar_27df1c7f:

    # hermanMed "¡Ni se te ocurra decirle nada a [nombreMad3]!"
    hermanMed "Don't even think about saying anything to [nombreMad3]!"

translate english strings:

    # game/Code/Eventos/micasa/miHabitacion/eventEricaBorrachaPillada/eventEricaBorrachaPillada.rpy:15
    old "{size=30}{b} Si {/b}{/size}"
    new "{size=30}{b} Yes {/b}{/size}"

    # game/Code/Eventos/micasa/miHabitacion/eventEricaBorrachaPillada/eventEricaBorrachaPillada.rpy:15
    old "{size=30}{b} No {/b}{/size}"
    new "{size=30}{b} No {/b}{/size}"

    # game/Code/Eventos/micasa/miHabitacion/eventEricaBorrachaPillada/eventEricaBorrachaPillada.rpy:43
    old "Investigar"
    new "Investigate"

    # game/Code/Eventos/micasa/miHabitacion/eventEricaBorrachaPillada/eventEricaBorrachaPillada.rpy:93
    old "Ayudarla"
    new "Help her"

    # game/Code/Eventos/micasa/miHabitacion/eventEricaBorrachaPillada/eventEricaBorrachaPillada.rpy:120
    old "Cogerle las piernas"
    new "Hold their legs."

    # game/Code/Eventos/micasa/miHabitacion/eventEricaBorrachaPillada/eventEricaBorrachaPillada.rpy:151
    old "Mostrar pecho derecho"
    new "Show right breast"

    # game/Code/Eventos/micasa/miHabitacion/eventEricaBorrachaPillada/eventEricaBorrachaPillada.rpy:181
    old "Bajarle la parte superior del vestido"
    new "Pulling down the top of her dress"

    # game/Code/Eventos/micasa/miHabitacion/eventEricaBorrachaPillada/eventEricaBorrachaPillada.rpy:197
    old "Manosear pecho"
    new "Grope her breast"

    # game/Code/Eventos/micasa/miHabitacion/eventEricaBorrachaPillada/eventEricaBorrachaPillada.rpy:197
    old "Manosear pecho 2"
    new "Grope her breast 2"

    # game/Code/Eventos/micasa/miHabitacion/eventEricaBorrachaPillada/eventEricaBorrachaPillada.rpy:197
    old "Manosear un poco mas"
    new "Grope a little bit more"
# TODO: Translation updated at 2022-02-07 21:19

# game/Code/Eventos/micasa/miHabitacion/eventEricaBorrachaPillada/eventEricaBorrachaPillada.rpy:450
translate english ericaBorrachaEscenaReproductor_538e1541:

    # "¡¡Pum!!"
    "Boom!!"

# game/Code/Eventos/micasa/miHabitacion/eventEricaBorrachaPillada/eventEricaBorrachaPillada.rpy:454
translate english ericaBorrachaEscenaReproductor_a62e3a5b:

    # protaPensa "¿Que ha sido ese ruido?"
    protaPensa "What was that noise?"#What has that noise been?
