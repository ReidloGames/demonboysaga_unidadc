﻿# TODO: Translation updated at 2021-07-08 13:36

translate english strings:

    # game/Code/Eventos/micasa/miHabitacion/eventMadRecogeRopa/eventMadRecogeRopa.rpy:42
    old "Nada"
    new "Nothing"
# TODO: Translation updated at 2021-12-12 13:06

# game/Code/Eventos/micasa/miHabitacion/eventMadRecogeRopa/eventMadRecogeRopa.rpy:68
translate english accionDetrasMadRecogeRopa_c52e0ae7:

    # prota "Ups, hemos chocado."
    prota "Whoops, we crashed."

# game/Code/Eventos/micasa/miHabitacion/eventMadRecogeRopa/eventMadRecogeRopa.rpy:69
translate english accionDetrasMadRecogeRopa_af9202a1:

    # prota "Solo quería darte los buenos días [nombreMad3]."
    prota "I just wanted to say good morning to you [nombreMad3]."

# game/Code/Eventos/micasa/miHabitacion/eventMadRecogeRopa/eventMadRecogeRopa.rpy:71
translate english accionDetrasMadRecogeRopa_1e1a1563:

    # mad "Oh! no te vi, me asustaste. Buenos días."
    mad "Oh! I didn't see you, you scared me. Good morning."

# game/Code/Eventos/micasa/miHabitacion/eventMadRecogeRopa/eventMadRecogeRopa.rpy:81
translate english accionDetrasMadRecogeRopa_8a3bdc6c:

    # prota "Ups, hemos chocado.Buenos días [nombreMad3]."
    prota "Oops, we've bumped into each other. Good morning [nombreMad3]."

# game/Code/Eventos/micasa/miHabitacion/eventMadRecogeRopa/eventMadRecogeRopa.rpy:83
translate english accionDetrasMadRecogeRopa_67475995:

    # mad "Tienes que ir con mas cuidado. Buenos días."
    mad "You have to be more careful. Good morning."

translate english strings:

    # game/Code/Eventos/micasa/miHabitacion/eventMadRecogeRopa/eventMadRecogeRopa.rpy:41
    old "Acercarte por detras"
    new "Approach from behind."
