﻿# TODO: Translation updated at 2021-10-13 15:38

# game/Code/Eventos/micasa/miHabitacion/eventPensaCursoMad/eventPensaCursoMad.rpy:7
translate english eventPensaCursoMad_d99c694b:

    # protaPensa "El curso de Mandin me da buenas vibraciones."
    protaPensa "Mandin's course gives me good vibes."

# game/Code/Eventos/micasa/miHabitacion/eventPensaCursoMad/eventPensaCursoMad.rpy:9
translate english eventPensaCursoMad_9ba519ae:

    # protaPensa "Y [nombreMad3] ya me ha dicho que me ayudara."
    protaPensa "And [nombreMad3] has already told me he will help me."

# game/Code/Eventos/micasa/miHabitacion/eventPensaCursoMad/eventPensaCursoMad.rpy:11
translate english eventPensaCursoMad_549a867b:

    # protaPensa "Es una gran oportunidad, pero tengo que ir con cuidado o me descubrira las intenciones."
    protaPensa "It's a great opportunity, but I have to be careful or he'll find out my intentions."

# game/Code/Eventos/micasa/miHabitacion/eventPensaCursoMad/eventPensaCursoMad.rpy:13
translate english eventPensaCursoMad_1ead9eaa:

    # protaPensa "Aún no sé cómo se ha podido creer semejante trola, pero he conseguido que aceptara..."
    protaPensa "I still don't know how he could have believed such a hoax, but I got him to agree..."

# game/Code/Eventos/micasa/miHabitacion/eventPensaCursoMad/eventPensaCursoMad.rpy:15
translate english eventPensaCursoMad_70891099:

    # protaPensa "Supongo que ahora, toda ayuda económica sera bienvenida."
    protaPensa "I guess now, any financial help will be welcome."
