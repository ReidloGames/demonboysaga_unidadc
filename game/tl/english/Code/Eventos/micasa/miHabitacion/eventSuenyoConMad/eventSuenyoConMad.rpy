﻿# TODO: Translation updated at 2021-08-27 11:11

# game/Code/Eventos/micasa/miHabitacion/eventSuenyoConMad/eventSuenyoConMad.rpy:15
translate english eventSuenyoConMad_face6900:

    # protaPensa "¿Que pasara a partir de ahora?"
    protaPensa "What happens from now on?"

# game/Code/Eventos/micasa/miHabitacion/eventSuenyoConMad/eventSuenyoConMad.rpy:16
translate english eventSuenyoConMad_f6943c2c:

    # protaPensa "La situación es preocupante."
    protaPensa "The situation is worrying."

# game/Code/Eventos/micasa/miHabitacion/eventSuenyoConMad/eventSuenyoConMad.rpy:20
translate english eventSuenyoConMad_f5fc3493:

    # protaPensa "Mejor me voy a dormir, tampoco puedo solucionar nada calentándome la cabeza."
    protaPensa "I'd better go to sleep, I can't solve anything by warming my head either."

# game/Code/Eventos/micasa/miHabitacion/eventSuenyoConMad/eventSuenyoConMad.rpy:28
translate english eventSuenyoConMad_ab1346ef:

    # mad "¡Me has hecho daño!"
    mad "You hurt me!"

# game/Code/Eventos/micasa/miHabitacion/eventSuenyoConMad/eventSuenyoConMad.rpy:30
translate english eventSuenyoConMad_f6800d21:

    # mad "Como me has podido hacer esto..."
    mad "How could you do this to me..."

# game/Code/Eventos/micasa/miHabitacion/eventSuenyoConMad/eventSuenyoConMad.rpy:38
translate english eventSuenyoConMad_8aa75f39:

    # mad "¿Lo quieres?"
    mad "Do you want it?"

# game/Code/Eventos/micasa/miHabitacion/eventSuenyoConMad/eventSuenyoConMad.rpy:42
translate english eventSuenyoConMad_0861d042:

    # mad "Cógelo..."
    mad "Take it..."

# game/Code/Eventos/micasa/miHabitacion/eventSuenyoConMad/eventSuenyoConMad.rpy:44
translate english eventSuenyoConMad_8aa75f39_1:

    # mad "¿Lo quieres?"
    mad "Do you want it?"

# game/Code/Eventos/micasa/miHabitacion/eventSuenyoConMad/eventSuenyoConMad.rpy:50
translate english eventSuenyoConMad_0861d042_1:

    # mad "Cógelo..."
    mad "Take it..."

# game/Code/Eventos/micasa/miHabitacion/eventSuenyoConMad/eventSuenyoConMad.rpy:54
translate english eventSuenyoConMad_3f41a125:

    # prota "Lo quiero."
    prota "I want it."

# game/Code/Eventos/micasa/miHabitacion/eventSuenyoConMad/eventSuenyoConMad.rpy:58
translate english eventSuenyoConMad_ef39c018:

    # mad "Ven aquí..."
    mad "Come here..."

# game/Code/Eventos/micasa/miHabitacion/eventSuenyoConMad/eventSuenyoConMad.rpy:62
translate english eventSuenyoConMad_f8963caf:

    # mad "¿Qué quieres hacerme?"
    mad "What do you want to do to me?"

# game/Code/Eventos/micasa/miHabitacion/eventSuenyoConMad/eventSuenyoConMad.rpy:66
translate english eventSuenyoConMad_0ce99636:

    # prota "Yo te cuidare."
    prota "I'll take care of you."

# game/Code/Eventos/micasa/miHabitacion/eventSuenyoConMad/eventSuenyoConMad.rpy:67
translate english eventSuenyoConMad_ca15cae7:

    # prota "Te hare de todo."
    prota "I'll do all sorts of things to you."

# game/Code/Eventos/micasa/miHabitacion/eventSuenyoConMad/eventSuenyoConMad.rpy:77
translate english eventSuenyoConMad_4fdda2f8:

    # prota "Al fin eres mía."
    prota "You're mine at last."

# game/Code/Eventos/micasa/miHabitacion/eventSuenyoConMad/eventSuenyoConMad.rpy:78
translate english eventSuenyoConMad_80be48c3:

    # prota "Te deseaba tanto..."
    prota "I wanted you so bad..."

# game/Code/Eventos/micasa/miHabitacion/eventSuenyoConMad/eventSuenyoConMad.rpy:80
translate english eventSuenyoConMad_ebae959f:

    # prota "Fuera bragas."
    prota "Panties off."

# game/Code/Eventos/micasa/miHabitacion/eventSuenyoConMad/eventSuenyoConMad.rpy:84
translate english eventSuenyoConMad_6e631e3e:

    # prota "¿Qué?"
    prota "What?"

# game/Code/Eventos/micasa/miHabitacion/eventSuenyoConMad/eventSuenyoConMad.rpy:86
translate english eventSuenyoConMad_851f71d6:

    # protaPensa "¿Dónde está?"
    protaPensa "Where is it?"

# game/Code/Eventos/micasa/miHabitacion/eventSuenyoConMad/eventSuenyoConMad.rpy:90
translate english eventSuenyoConMad_177795cc:

    # prota "mmmmmmm"
    prota "mmmmmmm"

# game/Code/Eventos/micasa/miHabitacion/eventSuenyoConMad/eventSuenyoConMad.rpy:92
translate english eventSuenyoConMad_9af15563:

    # prota "¿Dónde estás?"
    prota "Where are you?"

# game/Code/Eventos/micasa/miHabitacion/eventSuenyoConMad/eventSuenyoConMad.rpy:94
translate english eventSuenyoConMad_be80b1c1:

    # hermanPeq "Despierta dormilón."
    hermanPeq "Wake up sleepyhead."

# game/Code/Eventos/micasa/miHabitacion/eventSuenyoConMad/eventSuenyoConMad.rpy:100
translate english eventSuenyoConMad_23c94e17:

    # hermanPeq "Hay reunión familiar en el comedor. Vamos, levántate..."
    hermanPeq "There's a family meeting in the dining room. Come on, get up..."

# game/Code/Eventos/micasa/miHabitacion/eventSuenyoConMad/eventSuenyoConMad.rpy:102
translate english eventSuenyoConMad_bf38611d:

    # protaPensa "¡¡Oh dios!! Era un sueño."
    protaPensa "Oh my God!!!! It was a dream."

# game/Code/Eventos/micasa/miHabitacion/eventSuenyoConMad/eventSuenyoConMad.rpy:104
translate english eventSuenyoConMad_a65b3340:

    # protaPensa "Todo ha sido un maldito sueño."
    protaPensa "It was all a fucking dream."

# game/Code/Eventos/micasa/miHabitacion/eventSuenyoConMad/eventSuenyoConMad.rpy:106
translate english eventSuenyoConMad_72ca1c62:

    # protaPensa "Parecía tan real..."
    protaPensa "It seemed so real..."

# game/Code/Eventos/micasa/miHabitacion/eventSuenyoConMad/eventSuenyoConMad.rpy:108
translate english eventSuenyoConMad_c6f1f798:

    # protaPensa "Como he podido tener esa clase de sueño con [nombreMad3]?"
    protaPensa "How could I have had that kind of dream about [nombreMad3]?"

# game/Code/Eventos/micasa/miHabitacion/eventSuenyoConMad/eventSuenyoConMad.rpy:109
translate english eventSuenyoConMad_6b8de565:

    # protaPensa "Y lo he disfrutado..."
    protaPensa "And I enjoyed it..."

# game/Code/Eventos/micasa/miHabitacion/eventSuenyoConMad/eventSuenyoConMad.rpy:113
translate english eventSuenyoConMad_a83977f6:

    # protaPensa "Tengo que darme prisa, me están esperando y seguro que es importante después de lo que paso ayer."
    protaPensa "I have to hurry, they're waiting for me and it must be important after what happened yesterday."
# TODO: Translation updated at 2022-02-07 21:19

# game/Code/Eventos/micasa/miHabitacion/eventSuenyoConMad/eventSuenyoConMad.rpy:142
translate english suenyoConMadEscenaReproductor_face6900:

    # protaPensa "¿Que pasara a partir de ahora?"
    protaPensa "What happens from now on?"

# game/Code/Eventos/micasa/miHabitacion/eventSuenyoConMad/eventSuenyoConMad.rpy:143
translate english suenyoConMadEscenaReproductor_f6943c2c:

    # protaPensa "La situación es preocupante."
    protaPensa "The situation is worrying."

# game/Code/Eventos/micasa/miHabitacion/eventSuenyoConMad/eventSuenyoConMad.rpy:147
translate english suenyoConMadEscenaReproductor_f5fc3493:

    # protaPensa "Mejor me voy a dormir, tampoco puedo solucionar nada calentándome la cabeza."
    protaPensa "I'd better go to sleep, I can't solve anything by warming my head either."

# game/Code/Eventos/micasa/miHabitacion/eventSuenyoConMad/eventSuenyoConMad.rpy:156
translate english suenyoConMadEscenaReproductor_ab1346ef:

    # mad "¡Me has hecho daño!"
    mad "You hurt me!"

# game/Code/Eventos/micasa/miHabitacion/eventSuenyoConMad/eventSuenyoConMad.rpy:158
translate english suenyoConMadEscenaReproductor_f6800d21:

    # mad "Como me has podido hacer esto..."
    mad "How could you do this to me..."

# game/Code/Eventos/micasa/miHabitacion/eventSuenyoConMad/eventSuenyoConMad.rpy:168
translate english suenyoConMadEscenaReproductor_8aa75f39:

    # mad "¿Lo quieres?"
    mad "Do you want it?"

# game/Code/Eventos/micasa/miHabitacion/eventSuenyoConMad/eventSuenyoConMad.rpy:173
translate english suenyoConMadEscenaReproductor_0861d042:

    # mad "Cógelo..."
    mad "Take it..."

# game/Code/Eventos/micasa/miHabitacion/eventSuenyoConMad/eventSuenyoConMad.rpy:175
translate english suenyoConMadEscenaReproductor_8aa75f39_1:

    # mad "¿Lo quieres?"
    mad "Do you want it?"

# game/Code/Eventos/micasa/miHabitacion/eventSuenyoConMad/eventSuenyoConMad.rpy:184
translate english suenyoConMadEscenaReproductor_0861d042_1:

    # mad "Cógelo..."
    mad "Take it..."

# game/Code/Eventos/micasa/miHabitacion/eventSuenyoConMad/eventSuenyoConMad.rpy:188
translate english suenyoConMadEscenaReproductor_3f41a125:

    # prota "Lo quiero."
    prota "I want it."

# game/Code/Eventos/micasa/miHabitacion/eventSuenyoConMad/eventSuenyoConMad.rpy:192
translate english suenyoConMadEscenaReproductor_ef39c018:

    # mad "Ven aquí..."
    mad "Come here..."

# game/Code/Eventos/micasa/miHabitacion/eventSuenyoConMad/eventSuenyoConMad.rpy:196
translate english suenyoConMadEscenaReproductor_f8963caf:

    # mad "¿Qué quieres hacerme?"
    mad "What do you want to do to me?"

# game/Code/Eventos/micasa/miHabitacion/eventSuenyoConMad/eventSuenyoConMad.rpy:200
translate english suenyoConMadEscenaReproductor_0ce99636:

    # prota "Yo te cuidare."
    prota "I'll take care of you."

# game/Code/Eventos/micasa/miHabitacion/eventSuenyoConMad/eventSuenyoConMad.rpy:201
translate english suenyoConMadEscenaReproductor_ca15cae7:

    # prota "Te hare de todo."
    prota "I'll do all sorts of things to you."

# game/Code/Eventos/micasa/miHabitacion/eventSuenyoConMad/eventSuenyoConMad.rpy:213
translate english suenyoConMadEscenaReproductor_4fdda2f8:

    # prota "Al fin eres mía."
    prota "You're mine at last."

# game/Code/Eventos/micasa/miHabitacion/eventSuenyoConMad/eventSuenyoConMad.rpy:214
translate english suenyoConMadEscenaReproductor_80be48c3:

    # prota "Te deseaba tanto..."
    prota "I wanted you so bad..."

# game/Code/Eventos/micasa/miHabitacion/eventSuenyoConMad/eventSuenyoConMad.rpy:216
translate english suenyoConMadEscenaReproductor_ebae959f:

    # prota "Fuera bragas."
    prota "Panties off."

# game/Code/Eventos/micasa/miHabitacion/eventSuenyoConMad/eventSuenyoConMad.rpy:223
translate english suenyoConMadEscenaReproductor_6e631e3e:

    # prota "¿Qué?"
    prota "What?"

# game/Code/Eventos/micasa/miHabitacion/eventSuenyoConMad/eventSuenyoConMad.rpy:225
translate english suenyoConMadEscenaReproductor_851f71d6:

    # protaPensa "¿Dónde está?"
    protaPensa "Where is it?"

# game/Code/Eventos/micasa/miHabitacion/eventSuenyoConMad/eventSuenyoConMad.rpy:229
translate english suenyoConMadEscenaReproductor_177795cc:

    # prota "mmmmmmm"
    prota "mmmmmmm"

# game/Code/Eventos/micasa/miHabitacion/eventSuenyoConMad/eventSuenyoConMad.rpy:231
translate english suenyoConMadEscenaReproductor_9af15563:

    # prota "¿Dónde estás?"
    prota "Where are you?"

# game/Code/Eventos/micasa/miHabitacion/eventSuenyoConMad/eventSuenyoConMad.rpy:233
translate english suenyoConMadEscenaReproductor_be80b1c1:

    # hermanPeq "Despierta dormilón."
    hermanPeq "Wake up sleepyhead."

# game/Code/Eventos/micasa/miHabitacion/eventSuenyoConMad/eventSuenyoConMad.rpy:239
translate english suenyoConMadEscenaReproductor_23c94e17:

    # hermanPeq "Hay reunión familiar en el comedor. Vamos, levántate..."
    hermanPeq "There's a family meeting in the dining room. Come on, get up..."

# game/Code/Eventos/micasa/miHabitacion/eventSuenyoConMad/eventSuenyoConMad.rpy:241
translate english suenyoConMadEscenaReproductor_bf38611d:

    # protaPensa "¡¡Oh dios!! Era un sueño."
    protaPensa "Oh my God!!!! It was a dream."

# game/Code/Eventos/micasa/miHabitacion/eventSuenyoConMad/eventSuenyoConMad.rpy:243
translate english suenyoConMadEscenaReproductor_a65b3340:

    # protaPensa "Todo ha sido un maldito sueño."
    protaPensa "It was all a fucking dream."

# game/Code/Eventos/micasa/miHabitacion/eventSuenyoConMad/eventSuenyoConMad.rpy:245
translate english suenyoConMadEscenaReproductor_72ca1c62:

    # protaPensa "Parecía tan real..."
    protaPensa "It seemed so real..."

# game/Code/Eventos/micasa/miHabitacion/eventSuenyoConMad/eventSuenyoConMad.rpy:247
translate english suenyoConMadEscenaReproductor_c6f1f798:

    # protaPensa "Como he podido tener esa clase de sueño con [nombreMad3]?"
    protaPensa "How could I have had that kind of dream about [nombreMad3]?"

# game/Code/Eventos/micasa/miHabitacion/eventSuenyoConMad/eventSuenyoConMad.rpy:248
translate english suenyoConMadEscenaReproductor_6b8de565:

    # protaPensa "Y lo he disfrutado..."
    protaPensa "And I enjoyed it..."

# game/Code/Eventos/micasa/miHabitacion/eventSuenyoConMad/eventSuenyoConMad.rpy:252
translate english suenyoConMadEscenaReproductor_a83977f6:

    # protaPensa "Tengo que darme prisa, me están esperando y seguro que es importante después de lo que paso ayer."
    protaPensa "I have to hurry, they're waiting for me and it must be important after what happened yesterday."
