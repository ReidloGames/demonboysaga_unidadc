﻿# TODO: Translation updated at 2021-08-27 11:11

# game/Code/Eventos/micasa/miHabitacion/eventPensarSiPadTieneProblemas/eventPensarSiPadTieneProblemas.rpy:6
translate english eventPensarSiPadTieneProblemas_9312c1f2:

    # protaPensa "Me pregunto qué habrá sido esa llamada que recibió [situPad2] por la mañana."
    protaPensa "I wonder what that call [situPad2] got in the morning was all about."

# game/Code/Eventos/micasa/miHabitacion/eventPensarSiPadTieneProblemas/eventPensarSiPadTieneProblemas.rpy:8
translate english eventPensarSiPadTieneProblemas_77475e04:

    # protaPensa "Tenía una cara de preocupación y se le veía muy pensativo. No sé si se habrá metido en algún lio, aunque por otro lado que yo sepa nunca ha pasado nada extraño."
    protaPensa "He had a worried look on his face and looked very thoughtful. I don't know if he got into any trouble, although otherwise as far as I know nothing strange has ever happened."

# game/Code/Eventos/micasa/miHabitacion/eventPensarSiPadTieneProblemas/eventPensarSiPadTieneProblemas.rpy:10
translate english eventPensarSiPadTieneProblemas_be3d9d21:

    # protaPensa "A parte de eso, [nombreMad3] estaba muy contenta sobre lo bien que le iban las cosas a [situPad2]. Hay algo que no acaba de encajar."
    protaPensa "Other than that, [nombreMad3] was very happy about how well things were going for [situPad2]. Something just doesn't quite add up."

# game/Code/Eventos/micasa/miHabitacion/eventPensarSiPadTieneProblemas/eventPensarSiPadTieneProblemas.rpy:11
translate english eventPensarSiPadTieneProblemas_fb10b7fa:

    # protaPensa "Igual le estoy dando demasiadas vueltas y todo va como la seda."
    protaPensa "Maybe I'm giving it too much thought and everything is going smoothly."

# game/Code/Eventos/micasa/miHabitacion/eventPensarSiPadTieneProblemas/eventPensarSiPadTieneProblemas.rpy:13
translate english eventPensarSiPadTieneProblemas_3fab492d:

    # "Dormir..."
    "Sleep..."
# TODO: Translation updated at 2022-02-16 11:36

# game/Code/Eventos/micasa/miHabitacion/eventPensarSiPadTieneProblemas/eventPensarSiPadTieneProblemas.rpy:7
translate english eventPensarSiPadTieneProblemas_a6829910:

    # protaPensa "Me pregunto cuál fue esa llamada que [situPad2] recibió anteriormente."
    protaPensa "I wonder what that call [situPad2] got earlier was all about."
