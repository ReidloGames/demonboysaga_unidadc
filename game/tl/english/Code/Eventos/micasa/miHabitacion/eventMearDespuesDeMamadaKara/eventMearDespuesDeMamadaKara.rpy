﻿# TODO: Translation updated at 2022-03-27 17:09

# game/Code/Eventos/micasa/miHabitacion/eventMearDespuesDeMamadaKara/eventMearDespuesDeMamadaKara.rpy:6
translate english eventMearDespuesDeMamadaKara_04fa9fa9:

    # protaPensa "Esas pastillas funcionaron... Buff menuda mamada."
    protaPensa "Those pills worked... Ooff, what a huge success."

# game/Code/Eventos/micasa/miHabitacion/eventMearDespuesDeMamadaKara/eventMearDespuesDeMamadaKara.rpy:8
translate english eventMearDespuesDeMamadaKara_9dab0e4a:

    # protaPensa "De pensar se me esta poniendo dura otra vez."
    protaPensa "Thinking about it is making me hard again."

# game/Code/Eventos/micasa/miHabitacion/eventMearDespuesDeMamadaKara/eventMearDespuesDeMamadaKara.rpy:10
translate english eventMearDespuesDeMamadaKara_1dc06f46:

    # protaPensa "Voy al baño a mear."
    protaPensa "I'll go to the bathroom to piss."

# game/Code/Eventos/micasa/miHabitacion/eventMearDespuesDeMamadaKara/eventMearDespuesDeMamadaKara.rpy:26
translate english eventMearDespuesDeMamadaKara_9079b5fd:

    # protaPensa "Que a gusto..."
    protaPensa "What a relief..."

# game/Code/Eventos/micasa/miHabitacion/eventMearDespuesDeMamadaKara/eventMearDespuesDeMamadaKara.rpy:34
translate english eventMearDespuesDeMamadaKara_dce18f31:

    # hermanMay "Perdón no sabía que había alguien."
    hermanMay "Sorry, I didn't know there was anyone here."

# game/Code/Eventos/micasa/miHabitacion/eventMearDespuesDeMamadaKara/eventMearDespuesDeMamadaKara.rpy:37
translate english eventMearDespuesDeMamadaKara_3483f3c7:

    # prota "¿Eres una pervertida?"
    prota "Are you a pervert?"

# game/Code/Eventos/micasa/miHabitacion/eventMearDespuesDeMamadaKara/eventMearDespuesDeMamadaKara.rpy:39
translate english eventMearDespuesDeMamadaKara_557911ff:

    # hermanMay "¿Como tú? Que entras en mi habitación sin llamar a la puerta."
    hermanMay "Like you? When you enter my room without knocking on the door."

# game/Code/Eventos/micasa/miHabitacion/eventMearDespuesDeMamadaKara/eventMearDespuesDeMamadaKara.rpy:41
translate english eventMearDespuesDeMamadaKara_80a26a7e:

    # hermanMay "Solo quería coger mi cepillo de dientes."
    hermanMay "I just wanted to pick up my toothbrush."

# game/Code/Eventos/micasa/miHabitacion/eventMearDespuesDeMamadaKara/eventMearDespuesDeMamadaKara.rpy:42
translate english eventMearDespuesDeMamadaKara_73d19725:

    # hermanMay "Me levantado con un sabor de boca como a queso..."
    hermanMay "I woke up with a taste in my mouth like cheese..."

# game/Code/Eventos/micasa/miHabitacion/eventMearDespuesDeMamadaKara/eventMearDespuesDeMamadaKara.rpy:47
translate english eventMearDespuesDeMamadaKara_d23b0fb4:

    # prota "A saber, que comiste ayer..."
    prota "Probably something you ate yesterday..."

# game/Code/Eventos/micasa/miHabitacion/eventMearDespuesDeMamadaKara/eventMearDespuesDeMamadaKara.rpy:49
translate english eventMearDespuesDeMamadaKara_502fda6c:

    # hermanMay "No sé..."
    hermanMay "I do not know..."

# game/Code/Eventos/micasa/miHabitacion/eventMearDespuesDeMamadaKara/eventMearDespuesDeMamadaKara.rpy:51
translate english eventMearDespuesDeMamadaKara_7d13f245:

    # prota "Por suerte te gusta el queso."
    prota "Good thing you like cheese."

# game/Code/Eventos/micasa/miHabitacion/eventMearDespuesDeMamadaKara/eventMearDespuesDeMamadaKara.rpy:53
translate english eventMearDespuesDeMamadaKara_75b5a969:

    # hermanMay "Te dejo con tus asuntos."
    hermanMay "I leave you to your business."

# game/Code/Eventos/micasa/miHabitacion/eventMearDespuesDeMamadaKara/eventMearDespuesDeMamadaKara.rpy:55
translate english eventMearDespuesDeMamadaKara_2a5aa871:

    # prota "Y recuerda llamar a la puerta o podrías ver a mi Godzilla."
    prota "And remember knocking on the door or you might see my Godzilla."

# game/Code/Eventos/micasa/miHabitacion/eventMearDespuesDeMamadaKara/eventMearDespuesDeMamadaKara.rpy:57
translate english eventMearDespuesDeMamadaKara_cbf5dbed:

    # hermanMay "Hahaha. Que fantasma."
    hermanMay "Hahaha. What a liar."

# game/Code/Eventos/micasa/miHabitacion/eventMearDespuesDeMamadaKara/eventMearDespuesDeMamadaKara.rpy:58
translate english eventMearDespuesDeMamadaKara_6eeb23e1:

    # hermanMay "Todos los hombres siempre presumís de lo mismo y siempre la tenéis así de pequeña."
    hermanMay "All men always boast the same thing and it is always turns out to be small."

# game/Code/Eventos/micasa/miHabitacion/eventMearDespuesDeMamadaKara/eventMearDespuesDeMamadaKara.rpy:60
translate english eventMearDespuesDeMamadaKara_2a70af08:

    # hermanMay "No estoy interesada en cosas tan minúsculas."
    hermanMay "I'm not interested in such minuscule things."

# game/Code/Eventos/micasa/miHabitacion/eventMearDespuesDeMamadaKara/eventMearDespuesDeMamadaKara.rpy:62
translate english eventMearDespuesDeMamadaKara_03511be2:

    # protaPensa "Si tu supieras..."
    protaPensa "If only you knew..."
