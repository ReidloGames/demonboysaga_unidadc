﻿# TODO: Translation updated at 2022-03-27 17:09

# game/Code/Eventos/micasa/hermanMay/eventEscenaKaraPreparaPastillas/eventEscenaKaraPreparaPastillas.rpy:9
translate english eventEscenaKaraPreparaPastillas_312a1bc4:

    # protaPensa "Unas pastillitas para dormir bien..."
    protaPensa "A few pills to sleep well..."

# game/Code/Eventos/micasa/hermanMay/eventEscenaKaraPreparaPastillas/eventEscenaKaraPreparaPastillas.rpy:21
translate english eventEscenaKaraPreparaPastillas_bb3f32f2:

    # hermanMay "¿Qué haces en mi habitación?"
    hermanMay "What are you doing in my room?"

# game/Code/Eventos/micasa/hermanMay/eventEscenaKaraPreparaPastillas/eventEscenaKaraPreparaPastillas.rpy:23
translate english eventEscenaKaraPreparaPastillas_d8e62dae:

    # prota "Oh yo..."
    prota "Oh my..."

# game/Code/Eventos/micasa/hermanMay/eventEscenaKaraPreparaPastillas/eventEscenaKaraPreparaPastillas.rpy:24
translate english eventEscenaKaraPreparaPastillas_7f4f3e89:

    # prota "Perdona solo entre para saludarte, pero no había nadie."
    prota "Apologies. Just come in to say hi, but there was no one here."

# game/Code/Eventos/micasa/hermanMay/eventEscenaKaraPreparaPastillas/eventEscenaKaraPreparaPastillas.rpy:25
translate english eventEscenaKaraPreparaPastillas_e0c7cd67:

    # prota "Ya me iba..."
    prota "I was leaving..."

# game/Code/Eventos/micasa/hermanMay/eventEscenaKaraPreparaPastillas/eventEscenaKaraPreparaPastillas.rpy:27
translate english eventEscenaKaraPreparaPastillas_f15c0855:

    # hermanMay "Estas muy raro últimamente..."
    hermanMay "You're so weird lately..."
