﻿# TODO: Translation updated at 2021-07-08 13:36

# game/Code/Eventos/micasa/pasillo2/eventPeqSeVaUni/eventPeqSeVaUni.rpy:20
translate english eventPeqSeVaUni_e41c805c:

    # hermanPeq "Eyy, dormilon!!"
    hermanPeq "Hey, sleepyhead!!!"

# game/Code/Eventos/micasa/pasillo2/eventPeqSeVaUni/eventPeqSeVaUni.rpy:25
translate english eventPeqSeVaUni_a9329357:

    # hermanPeq "Me voy a la Universidad si no te das prisa llegaras tarde!"
    hermanPeq "I'm off to college if you don't hurry you'll be late!"

# game/Code/Eventos/micasa/pasillo2/eventPeqSeVaUni/eventPeqSeVaUni.rpy:34
translate english eventPeqSeVaUni_96cb12cc:

    # prota "Si, ahora me iba a vestir"
    prota "Yeah, I was just going to get dressed."

# game/Code/Eventos/micasa/pasillo2/eventPeqSeVaUni/eventPeqSeVaUni.rpy:37
translate english eventPeqSeVaUni_cb81df15:

    # hermanPeq "Ya sabes que la profesora Alicia no te tiene mucho cariño"
    hermanPeq "You know that Professor Alicia is not very fond of you."

# game/Code/Eventos/micasa/pasillo2/eventPeqSeVaUni/eventPeqSeVaUni.rpy:39
translate english eventPeqSeVaUni_602ab25a:

    # hermanPeq "Nos vemos luego."
    hermanPeq "See you later"
