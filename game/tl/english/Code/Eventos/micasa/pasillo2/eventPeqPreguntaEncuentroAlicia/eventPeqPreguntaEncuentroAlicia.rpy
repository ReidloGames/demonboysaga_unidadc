﻿# TODO: Translation updated at 2021-08-27 11:11

# game/Code/Eventos/micasa/pasillo2/eventPeqPreguntaEncuentroAlicia/eventPeqPreguntaEncuentroAlicia.rpy:7
translate english eventPeqPreguntaEncuentroAlicia_35328c68:

    # hermanPeq "Ey!"
    hermanPeq "Hey!"

# game/Code/Eventos/micasa/pasillo2/eventPeqPreguntaEncuentroAlicia/eventPeqPreguntaEncuentroAlicia.rpy:11
translate english eventPeqPreguntaEncuentroAlicia_6477fdca:

    # hermanPeq "Ya me contaron que llegaste tarde a clase y que Alicia fue dura contigo."
    hermanPeq "They already told me that you were late for class and that Alicia was hard on you."

# game/Code/Eventos/micasa/pasillo2/eventPeqPreguntaEncuentroAlicia/eventPeqPreguntaEncuentroAlicia.rpy:15
translate english eventPeqPreguntaEncuentroAlicia_1bc730d7:

    # prota "Estoy harto de aguantar a esa perra. Siempre busca algo para joderme y dejarme mal delante de toda la clase."
    prota "I'm sick of putting up with that bitch. She's always looking for something to fuck me up and make me look bad in front of the whole class."

# game/Code/Eventos/micasa/pasillo2/eventPeqPreguntaEncuentroAlicia/eventPeqPreguntaEncuentroAlicia.rpy:17
translate english eventPeqPreguntaEncuentroAlicia_69f94a0f:

    # hermanPeq "¿Si sabes que no os lleváis bien porque no llegaste puntual a clase?"
    hermanPeq "If you know you two don't get along why didn't you come to class on time?"

# game/Code/Eventos/micasa/pasillo2/eventPeqPreguntaEncuentroAlicia/eventPeqPreguntaEncuentroAlicia.rpy:19
translate english eventPeqPreguntaEncuentroAlicia_05fe7131:

    # prota "Porque me salió un imprevisto en el último momento."
    prota "Because something came up at the last minute."

# game/Code/Eventos/micasa/pasillo2/eventPeqPreguntaEncuentroAlicia/eventPeqPreguntaEncuentroAlicia.rpy:21
translate english eventPeqPreguntaEncuentroAlicia_9152cc2d:

    # prota "Juro que me vengare de esa perra."
    prota "I swear I'll get even with that bitch."

# game/Code/Eventos/micasa/pasillo2/eventPeqPreguntaEncuentroAlicia/eventPeqPreguntaEncuentroAlicia.rpy:23
translate english eventPeqPreguntaEncuentroAlicia_2e414491:

    # hermanPeq "hahahahaha"
    hermanPeq "hahahahahaha"

# game/Code/Eventos/micasa/pasillo2/eventPeqPreguntaEncuentroAlicia/eventPeqPreguntaEncuentroAlicia.rpy:25
translate english eventPeqPreguntaEncuentroAlicia_226bd0ed:

    # hermanPeq "Has de ser realista ella es tu profesora y está por encima de ti. Es imposible que puedas hacer nada al respecto."
    hermanPeq "You have to be realistic she's your teacher and she's above you. There's no way you can do anything about it."

# game/Code/Eventos/micasa/pasillo2/eventPeqPreguntaEncuentroAlicia/eventPeqPreguntaEncuentroAlicia.rpy:27
translate english eventPeqPreguntaEncuentroAlicia_abf18e04:

    # hermanPeq "Yo de ti no la haría enfadar, igual se le acaba pasando."
    hermanPeq "If I were you I wouldn't make her mad, maybe she'll get over it."

# game/Code/Eventos/micasa/pasillo2/eventPeqPreguntaEncuentroAlicia/eventPeqPreguntaEncuentroAlicia.rpy:29
translate english eventPeqPreguntaEncuentroAlicia_4e16c531:

    # prota "Ya veremos, ya sabes que cuando se me mete algo en la cabeza no puedo parar."
    prota "We'll see, you know when I get something in my head I can't stop."

# game/Code/Eventos/micasa/pasillo2/eventPeqPreguntaEncuentroAlicia/eventPeqPreguntaEncuentroAlicia.rpy:31
translate english eventPeqPreguntaEncuentroAlicia_a0104a02:

    # hermanPeq "Dices eso porque estas enfadado, pero ya se te pasara."
    hermanPeq "You say that because you're angry, but it'll pass."

# game/Code/Eventos/micasa/pasillo2/eventPeqPreguntaEncuentroAlicia/eventPeqPreguntaEncuentroAlicia.rpy:33
translate english eventPeqPreguntaEncuentroAlicia_7579524f:

    # hermanPeq "Adiós gruñón"
    hermanPeq "Bye bye grumpy."

# game/Code/Eventos/micasa/pasillo2/eventPeqPreguntaEncuentroAlicia/eventPeqPreguntaEncuentroAlicia.rpy:37
translate english eventPeqPreguntaEncuentroAlicia_eb1f9662:

    # hermanPeq "hahahaha"
    hermanPeq "hahahaha"
# TODO: Translation updated at 2022-02-16 11:36

# game/Code/Eventos/micasa/pasillo2/eventPeqPreguntaEncuentroAlicia/eventPeqPreguntaEncuentroAlicia.rpy:43
translate english eventPeqPreguntaEncuentroAlicia_af67bcb8:

    # protaPensa "Creo que iré a ver un poco de televisión para relajarme."
    protaPensa "I think I'll go watch some TV to relax."
