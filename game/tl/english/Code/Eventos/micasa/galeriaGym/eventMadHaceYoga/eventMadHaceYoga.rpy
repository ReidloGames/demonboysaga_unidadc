﻿# TODO: Translation updated at 2022-03-27 17:09

# game/Code/Eventos/micasa/galeriaGym/eventMadHaceYoga/eventMadHaceYoga.rpy:14
translate english eventMadHaceYoga_12b04eec:

    # protaPensa "Que culo tiene..."
    protaPensa "What an ass she has..."

# game/Code/Eventos/micasa/galeriaGym/eventMadHaceYoga/eventMadHaceYoga.rpy:32
translate english eventMadHaceYoga_a909af46:

    # protaPensa "Me dijo que debería hacer ejercicio..."
    protaPensa "She told me I should exercise..."

# game/Code/Eventos/micasa/galeriaGym/eventMadHaceYoga/eventMadHaceYoga.rpy:33
translate english eventMadHaceYoga_98083589:

    # protaPensa "Podría intentar convencerla y hacer ejercicio con ella. Seguro que se puede hacer yoga en pareja. "
    protaPensa "You could try to convince her and exercise with her. Surely you can do yoga as a couple."

# game/Code/Eventos/micasa/galeriaGym/eventMadHaceYoga/eventMadHaceYoga.rpy:45
translate english comprarEquipoDeGym_aa62c0dc:

    # protaPensa "Debería pasarme por la tienda de Kara y comprarme un equipo de gimnasia."
    protaPensa "I should stop by Kara's shop and buy us some gym equipment."
