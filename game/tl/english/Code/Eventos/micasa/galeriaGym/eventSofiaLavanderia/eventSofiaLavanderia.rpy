﻿# TODO: Translation updated at 2022-04-05 11:15

# game/Code/Eventos/micasa/galeriaGym/eventSofiaLavanderia/eventSofiaLavanderia.rpy:3
translate english eventSofiaLavanderia_ed9647b1:

    # protaPensa "Oh dios en que posición esta."
    protaPensa "Oh God, what is she doing like that."

# game/Code/Eventos/micasa/galeriaGym/eventSofiaLavanderia/eventSofiaLavanderia.rpy:13
translate english eventSofiaLavanderia_0d454305:

    # mad "[nombreProta2], estas ahí?"
    mad "[nombreProta2], are you there?"

# game/Code/Eventos/micasa/galeriaGym/eventSofiaLavanderia/eventSofiaLavanderia.rpy:15
translate english eventSofiaLavanderia_f52f24e7:

    # prota "Oh si, acabo de entrar. ¿Qué pasa?"
    prota "Oh yes, I just came in. What's going on?"

# game/Code/Eventos/micasa/galeriaGym/eventSofiaLavanderia/eventSofiaLavanderia.rpy:19
translate english eventSofiaLavanderia_776a4ee3:

    # mad "No te había visto. Tenemos un problema con la lavadora. Parece que no hace buen contacto."
    mad "I didn't notice you. We have a problem with the washing machine. It seems that it does not make a good connection."

# game/Code/Eventos/micasa/galeriaGym/eventSofiaLavanderia/eventSofiaLavanderia.rpy:21
translate english eventSofiaLavanderia_529ab873:

    # prota "Te ayudo a resolver el problema?"
    prota "Should I help you fix the problem?"

# game/Code/Eventos/micasa/galeriaGym/eventSofiaLavanderia/eventSofiaLavanderia.rpy:23
translate english eventSofiaLavanderia_f6f03e91:

    # mad "Gracias por la ayuda, pero ya lo mirare en otro momento."
    mad "Thanks for the help, but I'll look at it another time."

# game/Code/Eventos/micasa/galeriaGym/eventSofiaLavanderia/eventSofiaLavanderia.rpy:24
translate english eventSofiaLavanderia_8052d614:

    # mad "Aprovecha y recoge tu cesta."
    mad "Take advantage and collect your basket."

# game/Code/Eventos/micasa/galeriaGym/eventSofiaLavanderia/eventSofiaLavanderia.rpy:29
translate english eventSofiaLavanderia_22f3b08e:

    # mad "La lavadora está dando problemas otra vez."
    mad "The washing machine is giving us problems again."

# game/Code/Eventos/micasa/galeriaGym/eventSofiaLavanderia/eventSofiaLavanderia.rpy:31
translate english eventSofiaLavanderia_e7151b32:

    # prota "Le doy al botón a ver si se enciende."
    prota "I will try the button to see if it turns on."

# game/Code/Eventos/micasa/galeriaGym/eventSofiaLavanderia/eventSofiaLavanderia.rpy:33
translate english eventSofiaLavanderia_bd5c34c3:

    # prota "El botón esta encendido, sigue intentándolo..."
    prota "The button is pressed, keep trying..."

# game/Code/Eventos/micasa/galeriaGym/eventSofiaLavanderia/eventSofiaLavanderia.rpy:35
translate english eventSofiaLavanderia_71212b07:

    # prota "Ves moviendo el cable..."
    prota "You keep moving the cable..."

# game/Code/Eventos/micasa/galeriaGym/eventSofiaLavanderia/eventSofiaLavanderia.rpy:41
translate english eventSofiaLavanderia_7da5e6d3:

    # mad "¿Y ahora funciona?"
    mad "And now is it working?"

# game/Code/Eventos/micasa/galeriaGym/eventSofiaLavanderia/eventSofiaLavanderia.rpy:42
translate english eventSofiaLavanderia_777e6176:

    # prota "Prueba a mover el cable otra vez..."
    prota "Try moving the cable again..."

# game/Code/Eventos/micasa/galeriaGym/eventSofiaLavanderia/eventSofiaLavanderia.rpy:52
translate english eventSofiaLavanderia_7e7e6b27:

    # prota "Estoy en un sueño. El coño de [nombreMad3]."
    prota "I'm in a dream. The pussy of [nombreMad3]."

# game/Code/Eventos/micasa/galeriaGym/eventSofiaLavanderia/eventSofiaLavanderia.rpy:54
translate english eventSofiaLavanderia_ab546b83:

    # mad "Parece que esto no es."
    mad "It seems that this is no good."

# game/Code/Eventos/micasa/galeriaGym/eventSofiaLavanderia/eventSofiaLavanderia.rpy:58
translate english eventSofiaLavanderia_c657d21c:

    # prota "Es posible que este obstruido el desagüe y no se encienda por seguridad."
    prota "It is possible that the drain is clogged and does not turn on for safety."

# game/Code/Eventos/micasa/galeriaGym/eventSofiaLavanderia/eventSofiaLavanderia.rpy:60
translate english eventSofiaLavanderia_452b9e8e:

    # mad "Arghh solo faltaba que se empiecen a estropear las cosas."
    mad "Arghh, this is all I needed to start spoiling things."

# game/Code/Eventos/micasa/galeriaGym/eventSofiaLavanderia/eventSofiaLavanderia.rpy:62
translate english eventSofiaLavanderia_7227a0bb:

    # prota "Tranquila intentaremos arreglarlo."
    prota "Calm down, we will try to fix it."

translate english strings:

    # game/Code/Eventos/micasa/galeriaGym/eventSofiaLavanderia/eventSofiaLavanderia.rpy:36
    old "levantarle la falda"
    new "lift her skirt"
