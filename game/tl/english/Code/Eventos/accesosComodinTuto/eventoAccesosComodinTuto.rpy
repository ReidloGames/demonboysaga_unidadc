﻿# TODO: Translation updated at 2021-10-13 15:38

# game/Code/Eventos/accesosComodinTuto/eventoAccesosComodinTuto.rpy:6
translate english eventoAccesosComodinTuto_741f6911:

    # narrador "A partir de ahora se activarán los accesos comodín."
    narrador "From now on wildcard access will be activated."

# game/Code/Eventos/accesosComodinTuto/eventoAccesosComodinTuto.rpy:8
translate english eventoAccesosComodinTuto_ae966297:

    # narrador "Estos accesos saldrán en el mapa indicados con la letra C y fuera de las habitaciones de las chicas."
    narrador "These access will appear on the map indicated by the letter C and outside the girls' rooms."

# game/Code/Eventos/accesosComodinTuto/eventoAccesosComodinTuto.rpy:18
translate english eventoAccesosComodinTuto_bd28a2c4:

    # narrador "Estos accesos comodín permitirán acceder a una interacción sin tener que esperar una semana entera."
    narrador "These wildcard access will allow you to access an interaction without having to wait a whole week."

# game/Code/Eventos/accesosComodinTuto/eventoAccesosComodinTuto.rpy:19
translate english eventoAccesosComodinTuto_a0327e79:

    # narrador "Por cada interacción hay 1 acceso comodín que se activara dependiendo el día y la hora."
    narrador "For each interaction there is 1 wildcard access that will be activated depending on the day and time."
