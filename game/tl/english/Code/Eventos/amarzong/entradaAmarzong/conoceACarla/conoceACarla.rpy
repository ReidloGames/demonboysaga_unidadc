﻿# TODO: Translation updated at 2021-10-13 15:38

# game/Code/Eventos/amarzong/entradaAmarzong/conoceACarla/conoceACarla.rpy:9
translate english eventConoceACarla_fbceaddf:

    # protaPensa "¡¡¡Ohh dios!!! por ahora las condiciones son perfectas."
    protaPensa "Ohh gosh!!! so far the conditions are perfect."

# game/Code/Eventos/amarzong/entradaAmarzong/conoceACarla/conoceACarla.rpy:20
translate english eventConoceACarla_db5500aa:

    # prota "Hola"
    prota "Hi!"

# game/Code/Eventos/amarzong/entradaAmarzong/conoceACarla/conoceACarla.rpy:23
translate english eventConoceACarla_59970593:

    # carla "¡Uy!"
    carla "Oops!"

# game/Code/Eventos/amarzong/entradaAmarzong/conoceACarla/conoceACarla.rpy:24
translate english eventConoceACarla_a7708557:

    # carla "Perdona, no sabía que había alguien. Se me cayó un sobre detrás de la estantería."
    carla "Sorry, I didn't know anyone was there. I dropped an envelope behind the bookshelf."

# game/Code/Eventos/amarzong/entradaAmarzong/conoceACarla/conoceACarla.rpy:26
translate english eventConoceACarla_918327cb:

    # protaPensa "Me reafirmo, las condiciones son más que perfectas. Esta chica es preciosa."
    protaPensa "I reaffirm, the conditions are beyond perfect. This girl is beautiful."

# game/Code/Eventos/amarzong/entradaAmarzong/conoceACarla/conoceACarla.rpy:28
translate english eventConoceACarla_0fbb799e:

    # protaPensa "Hola, acabo de hablar con Louie. Soy [nombreProta2] vengo por la petición del empleo."
    protaPensa "Hi, I just got off the phone with Louie. I'm [nombreProta2] I'm here about the job request."

# game/Code/Eventos/amarzong/entradaAmarzong/conoceACarla/conoceACarla.rpy:31
translate english eventConoceACarla_d85557a3:

    # carla "Hola encantada, soy Carla la mujer de Louie."
    carla "Hi nice to meet you, I'm Carla Louie's wife."

# game/Code/Eventos/amarzong/entradaAmarzong/conoceACarla/conoceACarla.rpy:38
translate english eventConoceACarla_0e20af0c:

    # carla "El empleo consiste en enviar los paquetes a los domicilios."
    carla "The job is to send packages to people's homes."

# game/Code/Eventos/amarzong/entradaAmarzong/conoceACarla/conoceACarla.rpy:40
translate english eventConoceACarla_49e8d301:

    # carla "Lo que necesitaras es un transporte."
    carla "What you'll need is transportation."

# game/Code/Eventos/amarzong/entradaAmarzong/conoceACarla/conoceACarla.rpy:42
translate english eventConoceACarla_d635f48b:

    # carla "¿Tienes coche?"
    carla "Do you have a car?"

# game/Code/Eventos/amarzong/entradaAmarzong/conoceACarla/conoceACarla.rpy:46
translate english eventConoceACarla_45c375a9:

    # prota "No."
    prota "No."

# game/Code/Eventos/amarzong/entradaAmarzong/conoceACarla/conoceACarla.rpy:48
translate english eventConoceACarla_b33bf627:

    # carla "¿Moto?"
    carla "Motorcycle?"

# game/Code/Eventos/amarzong/entradaAmarzong/conoceACarla/conoceACarla.rpy:50
translate english eventConoceACarla_e31ccfa7:

    # prota "Me temo que no..."
    prota "I'm afraid not..."

# game/Code/Eventos/amarzong/entradaAmarzong/conoceACarla/conoceACarla.rpy:52
translate english eventConoceACarla_ef394956:

    # carla "Ohhh..."
    carla "Ohhh..."

# game/Code/Eventos/amarzong/entradaAmarzong/conoceACarla/conoceACarla.rpy:54
translate english eventConoceACarla_d6072089:

    # carla "¿Y bicicleta?"
    carla "And bike?"

# game/Code/Eventos/amarzong/entradaAmarzong/conoceACarla/conoceACarla.rpy:56
translate english eventConoceACarla_f46e70b8:

    # protaPensa "Creo que en casa hay una bici, pero no recuerdo donde estaba... Tendré que buscarla."
    protaPensa "I think there's a bike at home, but I don't remember where it was.... I'll have to look for it."

# game/Code/Eventos/amarzong/entradaAmarzong/conoceACarla/conoceACarla.rpy:58
translate english eventConoceACarla_e53f779e:

    # prota "Si, creo que en casa tengo una."
    prota "Yes, I think I have one at home."

# game/Code/Eventos/amarzong/entradaAmarzong/conoceACarla/conoceACarla.rpy:60
translate english eventConoceACarla_f0edcf17:

    # carla "¡Genial!"
    carla "Great!"

# game/Code/Eventos/amarzong/entradaAmarzong/conoceACarla/conoceACarla.rpy:62
translate english eventConoceACarla_c7674e70:

    # carla "Cuando tengas la bicicleta habla con Louie y él te dirá donde tienes que enviar los paquetes."
    carla "When you get the bike talk to Louie and he'll tell you where to send the packages."

# game/Code/Eventos/amarzong/entradaAmarzong/conoceACarla/conoceACarla.rpy:66
translate english eventConoceACarla_d204d3c6:

    # prota "Muchas gracias."
    prota "Thank you so much."

# game/Code/Eventos/amarzong/entradaAmarzong/conoceACarla/conoceACarla.rpy:68
translate english eventConoceACarla_80f1fd6e:

    # carla "A ti. Espero que te sientas a gusto con nosotros."
    carla "To you. I hope you feel comfortable with us."

# game/Code/Eventos/amarzong/entradaAmarzong/conoceACarla/conoceACarla.rpy:78
translate english eventConoceACarla_df70d09c:

    # protaPensa "Que buena que esta y que culo tiene..."
    protaPensa "What a hottie she is and what an ass she has..."
