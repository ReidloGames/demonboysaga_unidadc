﻿# TODO: Translation updated at 2021-07-08 13:36

# game/Code/Tiendas/tiendaComestibles.rpy:112
translate english optionCompra_4e236c4c:

    # hombreConsum "Muchas gracias, desea algo mas?"
    hombreConsum "Thank you very much, do you need anything else?"

# game/Code/Tiendas/tiendaComestibles.rpy:121
translate english textosEmpleado_cede86b6:

    # hombreConsum "Muchas gracias"
    hombreConsum "Thank you very much"

translate english strings:

    # game/Code/Tiendas/tiendaComestibles.rpy:12
    old "{b}Tienda [nom]{/b}"
    new "{b}Store [nom]{/b}"

    # game/Code/Tiendas/tiendaComestibles.rpy:54
    old "[player_gold]€"
    new "[player_gold]€"

    # game/Code/Tiendas/tiendaComestibles.rpy:74
    old "Deseas comprar [itemObj.nameEsp] por el precio de [itemObj.cost]€ ?"
    new "Would you like to buy [itemObj.nameEng] for the price of [itemObj.cost]€"

    # game/Code/Tiendas/tiendaComestibles.rpy:79
    old "Deseas comprar [itemObj.nameEng] por el precio de [itemObj.cost]€ ?"
    new "Would you like to buy [itemObj.nameEng] for the price of [itemObj.cost]€"

    # game/Code/Tiendas/tiendaComestibles.rpy:95
    old "Aceptar"
    new "Accept"

    # game/Code/Tiendas/tiendaComestibles.rpy:100
    old "Cancelar"
    new "Cancel"

    # game/Code/Tiendas/tiendaComestibles.rpy:113
    old "Comestibles"
    new "Grocery"

    # game/Code/Tiendas/tiendaComestibles.rpy:131
    old "No tienes dinero suficiente"
    new "You don't have enough money"
