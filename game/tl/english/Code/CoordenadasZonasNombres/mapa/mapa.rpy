﻿# TODO: Translation updated at 2021-07-08 13:36

# game/Code/CoordenadasZonasNombres/mapa/mapa.rpy:53
translate english controlTiempoEntrada_c4ffd9fb:

    # protaPensa "La Universidad esta cerrada el fin de semana"
    protaPensa "The University is closed for the weekend"

# game/Code/CoordenadasZonasNombres/mapa/mapa.rpy:55
translate english controlTiempoEntrada_ce53220d:

    # protaPensa "A estas horas la Universidad esta cerrada"
    protaPensa "At this time the University is closed"

# game/Code/CoordenadasZonasNombres/mapa/mapa.rpy:57
translate english controlTiempoEntrada_5fdf63cc:

    # protaPensa "La tienda cierra los domingos."
    protaPensa "The store is closed on Sundays."

# game/Code/CoordenadasZonasNombres/mapa/mapa.rpy:59
translate english controlTiempoEntrada_f4910b51:

    # protaPensa "Ya es tarde, la tienda esta cerrada"
    protaPensa "It is already late, the store is closed "

translate english strings:

    # game/Code/CoordenadasZonasNombres/mapa/mapa.rpy:14
    old "Mi Casa"
    new "My House"

    # game/Code/CoordenadasZonasNombres/mapa/mapa.rpy:23
    old "Universidad"
    new "University"

    # game/Code/CoordenadasZonasNombres/mapa/mapa.rpy:38
    old "Tienda comestibles"
    new "Grocery store"
# TODO: Translation updated at 2021-09-09 19:44

translate english strings:

    # game/Code/CoordenadasZonasNombres/mapa/mapa.rpy:50
    old "Amarzong"
    new "Amarzong"

# TODO: Translation updated at 2021-12-12 13:06

# game/Code/CoordenadasZonasNombres/mapa/mapa.rpy:90
translate english controlTiempoEntrada_8e157f2e:

    # protaPensa "La tienda cierra los sabados y domingos."
    protaPensa "The store is closed on Saturdays and Sundays."

# game/Code/CoordenadasZonasNombres/mapa/mapa.rpy:92
translate english controlTiempoEntrada_9139c792:

    # protaPensa "La tienda esta cerrada."
    protaPensa "The store is closed."

translate english strings:

    # game/Code/CoordenadasZonasNombres/mapa/mapa.rpy:67
    old "Tienda ropa de Kara"
    new "Kara's clothing store"
# TODO: Translation updated at 2022-03-27 17:09

# game/Code/CoordenadasZonasNombres/mapa/mapa.rpy:112
translate english controlTiempoEntrada_d809ee06:

    # protaPensa "Ya es tarde, la biblioteca esta cerrada."
    protaPensa "It's too late, the library is closed."

# game/Code/CoordenadasZonasNombres/mapa/mapa.rpy:114
translate english controlTiempoEntrada_7b757d23:

    # protaPensa "La biblioteca cierra los sábados y domingos."
    protaPensa "The library is closed on Saturdays and Sundays."

# game/Code/CoordenadasZonasNombres/mapa/mapa.rpy:116
translate english controlTiempoEntrada_ddcd2062:

    # protaPensa "La biblioteca esta cerrada."
    protaPensa "The library is closed."

translate english strings:

    # game/Code/CoordenadasZonasNombres/mapa/mapa.rpy:85
    old "Biblioteca"
    new "Library"
