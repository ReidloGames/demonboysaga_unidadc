﻿# TODO: Translation updated at 2021-07-08 13:36

translate english strings:

    # game/Code/CoordenadasZonasNombres/miCasa/PASILLO2/pasillo2.rpy:26
    old "Habitación [nombrePeq]"
    new "Room [nombrePeq]"

    # game/Code/CoordenadasZonasNombres/miCasa/PASILLO2/pasillo2.rpy:33
    old "Habitación [nombreMed]"
    new "Room [nombreMed]"

    # game/Code/CoordenadasZonasNombres/miCasa/PASILLO2/pasillo2.rpy:40
    old "Habitación [nombreMay]"
    new "Room [nombreMay]"
# TODO: Translation updated at 2022-02-16 11:36

# game/Code/CoordenadasZonasNombres/miCasa/PASILLO2/pasillo2.rpy:72
translate english noHayTiempoParaEso_f3101538:

    # protaPensa "[nombrePeq] ya se fue a Uni. Yo debería hacer lo mismo."
    protaPensa "[namePeq] has already gone to Uni. I should do the same."

# game/Code/CoordenadasZonasNombres/miCasa/PASILLO2/pasillo2.rpy:75
translate english noHayTiempoParaEso_2df61dff:

    # protaPensa "No tengo tiempo para eso. Necesito llegar a uni."
    protaPensa "I don't have time for that. I need to get to uni."

# game/Code/CoordenadasZonasNombres/miCasa/PASILLO2/pasillo2.rpy:78
translate english noHayTiempoParaEso_2df61dff_1:

    # protaPensa "No tengo tiempo para eso. Necesito llegar a uni."
    protaPensa "I don't have time for that. I need to get to uni."

# game/Code/CoordenadasZonasNombres/miCasa/PASILLO2/pasillo2.rpy:81
translate english noHayTiempoParaEso_87af4b4d:

    # protaPensa "No hay tiempo para ducharse ahora. Ya llego tarde a Uni."
    protaPensa "There is no time to shower now. I'm already late for Uni."

# game/Code/CoordenadasZonasNombres/miCasa/PASILLO2/pasillo2.rpy:85
translate english noHayTiempoParaEso_ad5f6c05:

    # protaPensa "Quiero ver qué se está cocinando."
    protaPensa "I want to see what's cooking."

# game/Code/CoordenadasZonasNombres/miCasa/PASILLO2/pasillo2.rpy:88
translate english noHayTiempoParaEso_e147ed1e:

    # protaPensa "Tengo ganas de ver la gran televisión en el salón."
    protaPensa "I feel like watching the big TV in the living room."

# game/Code/CoordenadasZonasNombres/miCasa/PASILLO2/pasillo2.rpy:92
translate english noHayTiempoParaEso_8bb962e2:

    # protaPensa "[nombreMed] se ha fijado en mí a pesar de que actúa como si no lo hiciera. Debería saludar."
    protaPensa "[nombreMed] has noticed me even though she acts like she didn't. I should say hello."

# game/Code/CoordenadasZonasNombres/miCasa/PASILLO2/pasillo2.rpy:94
translate english noHayTiempoParaEso_8c2ea1ba:

    # protaPensa "Tengo la oportunidad de hablar con papá a solas en este momento."
    protaPensa "I have the chance to talk to [nombrePad] alone right now."

# game/Code/CoordenadasZonasNombres/miCasa/PASILLO2/pasillo2.rpy:96
translate english noHayTiempoParaEso_1f7efd8f:

    # protaPensa "Creo que escucho a [nombreMad] en la cocina."
    protaPensa "I think I hear [nombreMad] in the kitchen."

# game/Code/CoordenadasZonasNombres/miCasa/PASILLO2/pasillo2.rpy:100
translate english noHayTiempoParaEso_d7ac1e80:

    # protaPensa "Quiero ver a John sobre mi asignación de dinero de bolsillo primero."
    protaPensa "I want to see [nombrePad] about my pocket money allowance first."

# game/Code/CoordenadasZonasNombres/miCasa/PASILLO2/pasillo2.rpy:102
translate english noHayTiempoParaEso_9b326dc9:

    # protaPensa "Debería beber un poco de leche de la nevera antes de salir."
    protaPensa "I should drink some milk from the fridge before I head out."
# TODO: Translation updated at 2022-03-27 21:47

# game/Code/CoordenadasZonasNombres/miCasa/PASILLO2/pasillo2.rpy:76
translate english noHayTiempoParaEso_64f8eb3d:

    # protaPensa "No tengo tiempo para eso. Necesito llegar a la Uni."
    protaPensa "I don't have time for that. I need to get to the Uni."

# game/Code/CoordenadasZonasNombres/miCasa/PASILLO2/pasillo2.rpy:79
translate english noHayTiempoParaEso_64f8eb3d_1:

    # protaPensa "No tengo tiempo para eso. Necesito llegar a la Uni."
    protaPensa "I don't have time for that. I need to get to the Uni."

# game/Code/CoordenadasZonasNombres/miCasa/PASILLO2/pasillo2.rpy:82
translate english noHayTiempoParaEso_fc6d3532:

    # protaPensa "No hay tiempo para ducharse ahora. Ya llego tarde a la Uni."
    protaPensa "There is no time to shower now. I'm already late for Uni."

# game/Code/CoordenadasZonasNombres/miCasa/PASILLO2/pasillo2.rpy:89
translate english noHayTiempoParaEso_f6ac8174:

    # protaPensa "Tengo ganas de ver la televisión en el salón."
    protaPensa "I feel like watching TV in the living room."

# game/Code/CoordenadasZonasNombres/miCasa/PASILLO2/pasillo2.rpy:95
translate english noHayTiempoParaEso_5d9a9b56:

    # protaPensa "Ahora es un buen momento de hablar con [situPad2]."
    protaPensa "Now is a good time to talk to [situPad2]."

# game/Code/CoordenadasZonasNombres/miCasa/PASILLO2/pasillo2.rpy:101
translate english noHayTiempoParaEso_a9093393:

    # protaPensa "Quiero ver a John, a ver si me da dinero."
    protaPensa "I want to see John, see if he gives me money."

# game/Code/CoordenadasZonasNombres/miCasa/PASILLO2/pasillo2.rpy:103
translate english noHayTiempoParaEso_fe958e74:

    # protaPensa "Debería ir a la cocina y beber un poco de leche antes de salir."
    protaPensa "I should go to the kitchen and drink some milk before heading go out."
# TODO: Translation updated at 2022-04-04 15:35

# game/Code/CoordenadasZonasNombres/miCasa/PASILLO2/pasillo2.rpy:99
#translate english noHayTiempoParaEso_9f7bde83:

    # protaPensa "Estoy tan erecto. Necesito sentarme en la televisión y masturbarme."
#    protaPensa "I'm so hard. I need to sit at the TV and masturbate."

# game/Code/CoordenadasZonasNombres/miCasa/PASILLO2/pasillo2.rpy:107
#translate english noHayTiempoParaEso_9f7bde83_1:

    # protaPensa "Estoy tan erecto. Necesito sentarme en la televisión y masturbarme."
#    protaPensa "I'm so hard. I need to sit at the TV and masturbate."
# TODO: Translation updated at 2022-04-05 11:15

# game/Code/CoordenadasZonasNombres/miCasa/PASILLO2/pasillo2.rpy:99
translate english noHayTiempoParaEso_d405e80e:

    # protaPensa "Estoy empalmado de ver el culo de Kara. Necesito ir a ver la televisión y hacerme una paja."
    protaPensa "I'm so horny seeing Kara's ass. I need to go watch TV and deal with this."

# game/Code/CoordenadasZonasNombres/miCasa/PASILLO2/pasillo2.rpy:107
translate english noHayTiempoParaEso_d405e80e_1:

    # protaPensa "Estoy empalmado de ver el culo de Kara. Necesito ir a ver la televisión y hacerme una paja."
    protaPensa "I'm so horny seeing Kara's ass. I need to go watch TV and deal with this."
