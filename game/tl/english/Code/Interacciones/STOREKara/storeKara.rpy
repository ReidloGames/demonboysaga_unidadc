﻿# TODO: Translation updated at 2022-03-27 17:09

# game/Code/Interacciones/STOREKara/storeKara.rpy:21
translate english comprarEquipoDeGymTiendaKara_e3ea68c4:

    # prota "Hola Kara."
    prota "Hello Kara."

# game/Code/Interacciones/STOREKara/storeKara.rpy:23
translate english comprarEquipoDeGymTiendaKara_dac990f3:

    # hermanMay "Que sorpresa, ¿que haces por aquí?"
    hermanMay "What a surprise. What are you doing around here?"

# game/Code/Interacciones/STOREKara/storeKara.rpy:25
translate english comprarEquipoDeGymTiendaKara_16411f82:

    # prota "¿Te has oscurecido el pelo?"
    prota "Has your hair darkened?"

# game/Code/Interacciones/STOREKara/storeKara.rpy:27
translate english comprarEquipoDeGymTiendaKara_d2e34cab:

    # hermanMay "Solo es un baño de color."
    hermanMay "It's just a color rinse."

# game/Code/Interacciones/STOREKara/storeKara.rpy:29
translate english comprarEquipoDeGymTiendaKara_ae86e4da:

    # prota "Te sienta bien."
    prota "It suits you well."

# game/Code/Interacciones/STOREKara/storeKara.rpy:32
translate english comprarEquipoDeGymTiendaKara_447471af:

    # prota "He venido porque quiero empezar a hacer un poco de ejercicio y necesito un traje de gimnasia. Y que mejor que comprarlo en tu tienda."
    prota "I've come because I want to start doing some exercise and I need a gym suit. And what better than to buy it in your store."

# game/Code/Interacciones/STOREKara/storeKara.rpy:36
translate english comprarEquipoDeGymTiendaKara_582785f8:

    # hermanMay "Hahaha, eso está muy bien, así me gusta."
    hermanMay "Hahaha, that's great. That's how I like it."

# game/Code/Interacciones/STOREKara/storeKara.rpy:37
translate english comprarEquipoDeGymTiendaKara_10196ab3:

    # hermanMay "Déjame que saque el material..."
    hermanMay "Let me bring out the material..."

# game/Code/Interacciones/STOREKara/storeKara.rpy:40
translate english comprarEquipoDeGymTiendaKara_a131487d:

    # "Unos minutos mas tarde..."
    "A few minutes later..."

# game/Code/Interacciones/STOREKara/storeKara.rpy:42
translate english comprarEquipoDeGymTiendaKara_3e0cb70b:

    # hermanMay "Para chico tengo este."
    hermanMay "For men, I have this one."

# game/Code/Interacciones/STOREKara/storeKara.rpy:44
translate english comprarEquipoDeGymTiendaKara_319b131c:

    # hermanMay "Si fuera para chica podría enseñarte mas variedad."
    hermanMay "If it were for a girl, I could offer you more variety."

# game/Code/Interacciones/STOREKara/storeKara.rpy:46
translate english comprarEquipoDeGymTiendaKara_9b352747:

    # prota "Ese me gusta. Ya está bien."
    prota "I like that one. That's okay."

# game/Code/Interacciones/STOREKara/storeKara.rpy:48
translate english comprarEquipoDeGymTiendaKara_eb0aa1fa:

    # prota "¿Y para chica que tienes?"
    prota "And for girls, you have?"

# game/Code/Interacciones/STOREKara/storeKara.rpy:50
translate english comprarEquipoDeGymTiendaKara_cf96052f:

    # hermanMay "¿Que tienes alguna novia? Hahaha"
    hermanMay "Telling me you have a girlfriend? Hahaha!"

# game/Code/Interacciones/STOREKara/storeKara.rpy:54
translate english comprarEquipoDeGymTiendaKara_0e4d38d9:

    # hermanMay "Este me ha venido esta semana."
    hermanMay "This one has just come in this week."

# game/Code/Interacciones/STOREKara/storeKara.rpy:56
translate english comprarEquipoDeGymTiendaKara_07bc7f77:

    # prota "¿Te importaría probártelo para hacerme una idea?"
    prota "Would you mind trying it on, just to get an idea?"

# game/Code/Interacciones/STOREKara/storeKara.rpy:58
translate english comprarEquipoDeGymTiendaKara_349d275c:

    # hermanMay "¿Ahora? Lo que hay que hacer para vender algo... Hahaha."
    hermanMay "Now? The things you have to do to sell something... Hahaha."

# game/Code/Interacciones/STOREKara/storeKara.rpy:60
translate english comprarEquipoDeGymTiendaKara_3c22d0da:

    # hermanMay "Vigílame un momento la tienda."
    hermanMay "Keep an eye on the store for a moment."

# game/Code/Interacciones/STOREKara/storeKara.rpy:66
translate english comprarEquipoDeGymTiendaKara_07928917:

    # protaPensa "Cerro la puerta..."
    protaPensa "I'll close the door..."

# game/Code/Interacciones/STOREKara/storeKara.rpy:68
translate english comprarEquipoDeGymTiendaKara_a131487d_1:

    # "Unos minutos mas tarde..."
    "A few minutes later..."

# game/Code/Interacciones/STOREKara/storeKara.rpy:70
translate english comprarEquipoDeGymTiendaKara_3806fcbf:

    # prota "Uauuu, te queda genial."
    prota "Wauuu, it looks great."

# game/Code/Interacciones/STOREKara/storeKara.rpy:72
translate english comprarEquipoDeGymTiendaKara_81d52a57:

    # hermanMay "¿A caso lo dudabas?"
    hermanMay "Did you ever doubt it would?"

# game/Code/Interacciones/STOREKara/storeKara.rpy:74
translate english comprarEquipoDeGymTiendaKara_06779dd7:

    # hermanMay "Hahaha"
    hermanMay "Hahaha!"

# game/Code/Interacciones/STOREKara/storeKara.rpy:78
translate english comprarEquipoDeGymTiendaKara_836d462a:

    # prota "Oye, hacemos una cosa. ¿Qué tal si compro estos trajes de deporte y este se lo regalas a [nombreMad3]? Seguro que le tranquilizara ver que te van bien las cosas en la tienda. Y de paso se alegrará, recibir un regalo de tu parte."
    prota "Hey, about we do one thing. How about I buy these sports suits and you give one to [nombreMad3]? Surely it will reassure her to see that things are going well in the store. And by the way, she will be happy, to receive a gift from you."

# game/Code/Interacciones/STOREKara/storeKara.rpy:81
translate english comprarEquipoDeGymTiendaKara_7500b7eb:

    # hermanMay "¿De verdad? Eso es una gran idea."
    hermanMay "Really? That's a great idea."

# game/Code/Interacciones/STOREKara/storeKara.rpy:83
translate english comprarEquipoDeGymTiendaKara_5f6cb9ae:

    # prota "Y cógete uno para ti también."
    prota "And take one for yourself too."

# game/Code/Interacciones/STOREKara/storeKara.rpy:85
translate english comprarEquipoDeGymTiendaKara_26483223:

    # hermanMay "¿En serio? Hoy estas muy generoso. No sabía que tenías tanto dinero."
    hermanMay "Seriously? Today you are very generous. I didn't know you had so much money."

# game/Code/Interacciones/STOREKara/storeKara.rpy:87
translate english comprarEquipoDeGymTiendaKara_c1d1ed04:

    # prota "Ehh... ¿cuánto costara?"
    prota "Ehh... how much will it cost?"

# game/Code/Interacciones/STOREKara/storeKara.rpy:94
translate english comprarEquipoDeGymTiendaKara_49f6e0fa:

    # senyorMay "Disculpe..."
    senyorMay "Excuse me..."

# game/Code/Interacciones/STOREKara/storeKara.rpy:96
translate english comprarEquipoDeGymTiendaKara_03c65b22:

    # senyorMay "Estoy buscando trajes de baño para mi mujer."
    senyorMay "I'm looking for swimsuits for my wife."

# game/Code/Interacciones/STOREKara/storeKara.rpy:98
translate english comprarEquipoDeGymTiendaKara_b80fd645:

    # protaPensa "La dejare trabajar."
    protaPensa "I'll let her work."

# game/Code/Interacciones/STOREKara/storeKara.rpy:111
translate english comprarEquipoGymTiendaKara_2ecbe5e2:

    # "Comprado"
    "Bought"

# game/Code/Interacciones/STOREKara/storeKara.rpy:118
translate english comprarEquipoGymTiendaKara_09e60320:

    # "No tienes suficiente dinero"
    "You don't have enough money"

# game/Code/Interacciones/STOREKara/storeKara.rpy:119
translate english comprarEquipoGymTiendaKara_b9489804:

    # prota "Voy a sacar dinero, luego me paso."
    prota "I'm going to get more money, then I I'll come back."

translate english strings:

    # game/Code/Interacciones/STOREKara/storeKara.rpy:4
    old "Equipo de gimnasia"
    new "Gymnastics equipment"

    # game/Code/Interacciones/STOREKara/storeKara.rpy:108
    old "Comprar equipos de deporte. 60€"
    new "Buy sports equipment. 60€"
