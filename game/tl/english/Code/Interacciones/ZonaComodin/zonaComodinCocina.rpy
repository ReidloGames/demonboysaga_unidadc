﻿# TODO: Translation updated at 2021-09-14 14:34

translate english strings:

    # game/Code/Interacciones/ZonaComodin/zonaComodinCocina.rpy:7
    old "Esperar a [nombreMad3] para hacer la comida"
    new "Waiting for [nombreMad3] to make the meal"

    # game/Code/Interacciones/ZonaComodin/zonaComodinCocina.rpy:7
    old "Esperar a [nombreMad3] para hacer limpieza de casa"
    new "Waiting for [nombreMad3] to clean house"

    # game/Code/Interacciones/ZonaComodin/zonaComodinCocina.rpy:7
    old "Esperar a [nombreMad3] para que de teta a Max"
    new "Waiting for [nombreMad3] to breastfeed Max"

    # game/Code/Interacciones/ZonaComodin/zonaComodinCocina.rpy:7
    old "Esperar a [nombreMad3] para que friege los platos"
    new "Waiting for [nombreMad3] to wash the dishes"

    # game/Code/Interacciones/ZonaComodin/zonaComodinCocina.rpy:7
    old "Esperar a [nombrePeq] para comer juntos"
    new "Waiting for [nombrePeq] to have lunch together"

    # game/Code/Interacciones/ZonaComodin/zonaComodinCocina.rpy:7
    old "Esperar a que venga [nombreMay]"
    new "Wait for [nombreMay] to come."
