﻿# TODO: Translation updated at 2021-09-14 14:34

translate english strings:

    # game/Code/Interacciones/ZonaComodin/zonaComodinSalon.rpy:8
    old "Esperar a [nombreMad3] a que mire su revista"
    new "Waiting for [nombreMad3] to look at your magazine"

    # game/Code/Interacciones/ZonaComodin/zonaComodinSalon.rpy:8
    old "Esperar a [nombreMad3] para mirar Tv"
    new "Waiting for [nombreMad3] to watch TV"

    # game/Code/Interacciones/ZonaComodin/zonaComodinSalon.rpy:8
    old "Esperar a [nombrePeq]"
    new "Wait for [nombrePeq]"

    # game/Code/Interacciones/ZonaComodin/zonaComodinSalon.rpy:8
    old "Esperar a [nombreMed]"
    new "Wait for [nombreMed]"

    # game/Code/Interacciones/ZonaComodin/zonaComodinSalon.rpy:8
    old "Esperar a [nombreMay]"
    new "Wait for [nombreMay]"
