﻿# TODO: Translation updated at 2021-09-14 14:34

translate english strings:

    # game/Code/Interacciones/ZonaComodin/zonaComodinJardinPis.rpy:7
    old "Esperar a [nombreMay] a que venga a tomar el sol"
    new "Waiting for [nombreMay] to come and sunbathe"

    # game/Code/Interacciones/ZonaComodin/zonaComodinJardinPis.rpy:7
    old "Esperar a [nombreMad3] a que venga a tomar el sol"
    new "Waiting for [nombreMad3] to come and sunbathe"

    # game/Code/Interacciones/ZonaComodin/zonaComodinJardinPis.rpy:7
    old "Esperar a [nombreMed] a que venga a tomar el sol"
    new "Waiting for [nombreMed] to come and sunbathe"
