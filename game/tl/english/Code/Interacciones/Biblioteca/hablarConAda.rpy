﻿# TODO: Translation updated at 2022-03-27 17:09

# game/Code/Interacciones/Biblioteca/hablarConAda.rpy:4
translate english hablarConAda_a147dec4:

    # protaPensa "Ella se llama Ada. Casi todos los chicos de la Uni vienen a esta biblioteca para verle las tetas."
    protaPensa "Her name is Ada. Almost all the guys from Uni come to this library to see her tits."

# game/Code/Interacciones/Biblioteca/hablarConAda.rpy:30
translate english preguntaLibroYogaBiblioteca_86395dc2:

    # protaPensa "Hola, estoy buscando un libro. ¿me puedes ayudar?"
    protaPensa "Hello, I am looking for a book. Can you help me?"

# game/Code/Interacciones/Biblioteca/hablarConAda.rpy:32
translate english preguntaLibroYogaBiblioteca_dcc59c80:

    # protaPensa "Que tetas tiene. Son incluso más grandes que las de [nombreMad3]."
    protaPensa "What tits she has. They are even larger than those of [nombreMad3]."

# game/Code/Interacciones/Biblioteca/hablarConAda.rpy:34
translate english preguntaLibroYogaBiblioteca_1e2a6161:

    # ada "Hola, por supuesto. ¿Qué clase de libro estas buscando?"
    ada "Hello, of course. What kind of book are you looking for?"

# game/Code/Interacciones/Biblioteca/hablarConAda.rpy:36
translate english preguntaLibroYogaBiblioteca_9298e1c7:

    # prota "Estoy buscando un libro de yoga con ejercicios, para hacer en pareja."
    prota "I am looking for a yoga book with exercises, to do as a couple."

# game/Code/Interacciones/Biblioteca/hablarConAda.rpy:38
translate english preguntaLibroYogaBiblioteca_dd769915:

    # ada "Un chico deportista..."
    ada "A fitness boy..."

# game/Code/Interacciones/Biblioteca/hablarConAda.rpy:40
translate english preguntaLibroYogaBiblioteca_13f19765:

    # ada "Sin problemas, tenemos varios al final del pasillo en la última estantería."
    ada "No problem, we have several at the end of the aisle on the last shelf."

# game/Code/Interacciones/Biblioteca/hablarConAda.rpy:42
translate english preguntaLibroYogaBiblioteca_d3f82ca4:

    # ada "A mi también me encanta el yoga y el ejercicio en general."
    ada "I also love yoga and exercise in general."

# game/Code/Interacciones/Biblioteca/hablarConAda.rpy:46
translate english preguntaLibroYogaBiblioteca_4f6197ab:

    # ada "Si no fuera por el ejercicio estaría como una ballena ya que tengo que estar todo el día sentada en la silla. Pero adoro los libros y las novelas..."
    ada "If it weren't for the exercise I would be like a whale, since I have to sit in the chair all day. But I love books and novels..."

# game/Code/Interacciones/Biblioteca/hablarConAda.rpy:48
translate english preguntaLibroYogaBiblioteca_8220222e:

    # ada "Oh, perdona. Te estoy contando mi vida."
    ada "Oh, forgive me. I'm telling you my life story."

# game/Code/Interacciones/Biblioteca/hablarConAda.rpy:50
translate english preguntaLibroYogaBiblioteca_5764f387:

    # prota "Oh no te preocupes, eres muy amable."
    prota "Oh, don't worry, you are very kind."

# game/Code/Interacciones/Biblioteca/hablarConAda.rpy:52
translate english preguntaLibroYogaBiblioteca_9799235e:

    # ada "Si necesitas cualquier libro, avísame y te ayudare encantada."
    ada "If you need any book, let me know and I will be delighted to help you."

# game/Code/Interacciones/Biblioteca/hablarConAda.rpy:54
translate english preguntaLibroYogaBiblioteca_d204d3c6:

    # prota "Muchas gracias."
    prota "Thanks a lot."

# game/Code/Interacciones/Biblioteca/hablarConAda.rpy:56
translate english preguntaLibroYogaBiblioteca_365d4107:

    # protaPensa "No me canso de ver esas tetas con esa camisa tan apretada y a punto de explotar."
    protaPensa "I never tire of seeing those tits in that shirt, so tight and about to explode."

# game/Code/Interacciones/Biblioteca/hablarConAda.rpy:58
translate english preguntaLibroYogaBiblioteca_305c30ae:

    # protaPensa "A buscar el libro..."
    protaPensa "Let's look for the book..."

# game/Code/Interacciones/Biblioteca/hablarConAda.rpy:60
translate english preguntaLibroYogaBiblioteca_fe1dc05f:

    # protaPensa "Aquí están..."
    protaPensa "Here they are..."

# game/Code/Interacciones/Biblioteca/hablarConAda.rpy:62
translate english preguntaLibroYogaBiblioteca_5c5a9ac2:

    # protaPensa "Ejercicios de yoga nivel principiante para hacer en grupo."
    protaPensa "Beginner level yoga exercises to do in a group."

# game/Code/Interacciones/Biblioteca/hablarConAda.rpy:63
translate english preguntaLibroYogaBiblioteca_ca14f33c:

    # protaPensa "Aquí hay otro..."
    protaPensa "Here's another..."

# game/Code/Interacciones/Biblioteca/hablarConAda.rpy:65
translate english preguntaLibroYogaBiblioteca_5fdece47:

    # protaPensa "Sube la libido con estos ejercicios de yoga. Ideal para hacer con tu pareja."
    protaPensa "Raise libido with these yoga exercises. Ideal to do with your partner."

# game/Code/Interacciones/Biblioteca/hablarConAda.rpy:66
translate english preguntaLibroYogaBiblioteca_5494b1aa:

    # protaPensa "Este sería perfecto, pero [nombreMad3] lo rechazaría y yo quedaría como un deprabado."
    protaPensa "This one would be perfect, but [nombreMad3] would reject it and I would be seen as depraved."

# game/Code/Interacciones/Biblioteca/hablarConAda.rpy:68
translate english preguntaLibroYogaBiblioteca_7b5ac4a5:

    # protaPensa "A no ser..."
    protaPensa "Unless..."

# game/Code/Interacciones/Biblioteca/hablarConAda.rpy:69
translate english preguntaLibroYogaBiblioteca_f51a07a2:

    # protaPensa "Me llevo los dos e intercambio las portadas. Y así será un libro de ejercicios normal..."
    protaPensa "I take both of them and exchange the covers. And so it will be like a normal workbook..."

# game/Code/Interacciones/Biblioteca/hablarConAda.rpy:71
translate english preguntaLibroYogaBiblioteca_9d0d198e:

    # protaPensa "¡Fantástico!"
    protaPensa "Fantastic!"

# game/Code/Interacciones/Biblioteca/hablarConAda.rpy:72
translate english preguntaLibroYogaBiblioteca_ba954eaa:

    # protaPensa "Ya tengo el libro adecuado."
    protaPensa "I already have the right book."

translate english strings:

    # game/Code/Interacciones/Biblioteca/hablarConAda.rpy:9
    old "Preguntar por libro de yoga"
    new "Ask for yoga book"
