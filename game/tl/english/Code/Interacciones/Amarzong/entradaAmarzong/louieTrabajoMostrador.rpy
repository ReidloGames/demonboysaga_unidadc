﻿# TODO: Translation updated at 2021-10-13 15:38

# game/Code/Interacciones/Amarzong/entradaAmarzong/louieTrabajoMostrador.rpy:5
translate english conoceLouieTrabajoInterac_d06b6ae1:

    # protaPensa "Tengo que hablar con Carla"
    protaPensa "I need to talk to Carla."

# game/Code/Interacciones/Amarzong/entradaAmarzong/louieTrabajoMostrador.rpy:9
translate english conoceLouieTrabajoInterac_f4e5e158:

    # louie "Para empezar a trabajar necesitas un transporte"
    louie "To get started you need transportation."

# game/Code/Interacciones/Amarzong/entradaAmarzong/louieTrabajoMostrador.rpy:21
translate english conoceLouieTrabajoInterac_0bc66210:

    # prota "Hola, soy [nombreProta2]."
    prota "Hi, I'm [nombreProta2]."

# game/Code/Interacciones/Amarzong/entradaAmarzong/louieTrabajoMostrador.rpy:23
translate english conoceLouieTrabajoInterac_412d2200:

    # prota "Envie una solicitud para trabajar con vosotros."
    prota "I sent an application to work with you."

# game/Code/Interacciones/Amarzong/entradaAmarzong/louieTrabajoMostrador.rpy:25
translate english conoceLouieTrabajoInterac_6dd44dfc:

    # louie "Hola [nombreProta2]. Te estabamos esperando,me llamo Louie."
    louie "Hello [nombreProta2]. We've been waiting for you, my name is Louie."

# game/Code/Interacciones/Amarzong/entradaAmarzong/louieTrabajoMostrador.rpy:27
translate english conoceLouieTrabajoInterac_a1016a46:

    # louie "Ves esa puerta a mi izquierda? Allí esta mi mujer, se llama Carla."
    louie "See that door on my left? There's my wife, her name is Carla."

# game/Code/Interacciones/Amarzong/entradaAmarzong/louieTrabajoMostrador.rpy:29
translate english conoceLouieTrabajoInterac_69dd5b05:

    # louie "Ella te lo explicara todo para que puedas empezar a trabajar con nosotros."
    louie "She'll explain everything so you can start working with us."

# game/Code/Interacciones/Amarzong/entradaAmarzong/louieTrabajoMostrador.rpy:31
translate english conoceLouieTrabajoInterac_dfda3a02:

    # louie "Espero que te gusten las condiciones del trabajo y te sientas muy cómodo."
    louie "I hope you like the working conditions and feel very comfortable."

# game/Code/Interacciones/Amarzong/entradaAmarzong/louieTrabajoMostrador.rpy:35
translate english conoceLouieTrabajoInterac_d204d3c6:

    # prota "Muchas gracias."
    prota "Thank you very much."
