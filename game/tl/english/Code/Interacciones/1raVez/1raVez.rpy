﻿# TODO: Translation updated at 2021-07-08 13:36

# game/Code/Interacciones/1raVez/1raVez.rpy:8
translate english primeraVezMad_df6b7535:

    # protaPensa "Ella es mi [situMad]. Se llama [nombreMad]"
    protaPensa "She is my [situMad]. Her name is [nombreMad]"

# game/Code/Interacciones/1raVez/1raVez.rpy:9
translate english primeraVezMad_ac58f5ee:

    # protaPensa "Es muy atractiva, cuando era más joven trabajaba para una revista de moda, pero por alguna razón dejo el empleo.\n{i}{size=15}(Imagino que para poder llevar las tareas de la casa, pero nunca me lo dijo){/size}{/i}"
    protaPensa "She's very attractive, when she was younger she worked for a fashion magazine, but for some reason she quit the job.\n{i}{size=15}(I imagine so she could take care of chores around the house, but she never told me){/size}{/i}"

# game/Code/Interacciones/1raVez/1raVez.rpy:10
translate english primeraVezMad_52fa0ad3:

    # protaPensa "Es alta y joven y eso que ha tenido varios hijos, pero sigue teniendo un cuerpo increíble y aparenta menos años de los que tiene."
    protaPensa "She's tall and young and that she's had several children, but she still has an incredible body and looks younger than her years."

# game/Code/Interacciones/1raVez/1raVez.rpy:18
translate english primeraVezPad_b8b07b66:

    # protaPensa "El es mi [situPad]. Se llama [nombrePad]."
    protaPensa "He is my [situPad]. His name is [nombrePad]."

# game/Code/Interacciones/1raVez/1raVez.rpy:19
translate english primeraVezPad_7019d9a0:

    # protaPensa "Trabaja de director en un banco."
    protaPensa "He works as a director in a bank."

# game/Code/Interacciones/1raVez/1raVez.rpy:20
translate english primeraVezPad_ffe29ac7:

    # protaPensa "Como el gana mucho dinero, [nombreMad3] dejo su carrera de modelo para ocuparse de la casa."
    protaPensa "Since he earns a lot of money, [nombreMad3] gave up her modeling career to take care of the house."

# game/Code/Interacciones/1raVez/1raVez.rpy:21
translate english primeraVezPad_ec640b35:

    # protaPensa "Gracias a el, vivimos aquí y no tenemos ningún tipo de problema económico"
    protaPensa "Thanks to him, we live here and we don't have any kind of economic problem."

# game/Code/Interacciones/1raVez/1raVez.rpy:30
translate english primeraVezPeq_c2ddd954:

    # protaPensa "Ella es mi [situPeq2]. Se llama [nombrePeq] y tiene 18 años"
    protaPensa "She is my [situPeq2]. Her name is [nombrePeq] and she is 18 years old"

# game/Code/Interacciones/1raVez/1raVez.rpy:32
translate english primeraVezPeq_43fb20eb:

    # protaPensa "Siempre me llevado genial con ella. Es muy dulce y la quiero mucho."
    protaPensa "I always get along great with her. She's very sweet and I love her very much."

# game/Code/Interacciones/1raVez/1raVez.rpy:33
translate english primeraVezPeq_5d20ba22:

    # protaPensa "Aunque es hermosa, siempre ha tenido muchos complejos e inseguridades ya que sus hermanas tienen mucho pecho y son mas altas que ella."
    protaPensa "Although she is beautiful, she has always had many complexes and insecurities because her sisters have large breasts and are taller than her."
# TODO: Translation updated at 2021-08-06 14:00

# game/Code/Interacciones/1raVez/1raVez.rpy:42
translate english primeraVezMed_8773bb68:

    # protaPensa "Ella se llama [nombreMed] y es mi [situMed2]."
    protaPensa "She's called [nombreMed] and she's my [situMed2]."

# game/Code/Interacciones/1raVez/1raVez.rpy:44
translate english primeraVezMed_9811d820:

    # protaPensa "Nunca nos hemos llevado bien."
    protaPensa "We've never gotten along."

# game/Code/Interacciones/1raVez/1raVez.rpy:45
translate english primeraVezMed_77192c7c:

    # protaPensa "No entiendo el por qué, pero la relación siempre ha sido muy distante."
    protaPensa "I don't understand why, but the relationship has always been very distant."

# game/Code/Interacciones/1raVez/1raVez.rpy:46
translate english primeraVezMed_c5244840:

    # protaPensa "No me soporta y apenas he tenido relación más allá de la justa, para poder convivir en paz."
    protaPensa "He can't stand me and I've barely had any relationship beyond just so we can live together peacefully."

# game/Code/Interacciones/1raVez/1raVez.rpy:47
translate english primeraVezMed_65815092:

    # protaPensa "Es como si yo fuera el ratón y ella el gato."
    protaPensa "It's like I'm the mouse and she's the cat."

# game/Code/Interacciones/1raVez/1raVez.rpy:48
translate english primeraVezMed_6b754dd3:

    # protaPensa "Es una persona que le gusta mucho la fiesta y es bastante extrovertida."
    protaPensa "She is a person who likes to party a lot and is quite extroverted."

# game/Code/Interacciones/1raVez/1raVez.rpy:49
translate english primeraVezMed_d891af2e:

    # protaPensa "Actualmente esta estudiando un master en psicología."
    protaPensa "She is currently studying for a master's degree in psychology."

# game/Code/Interacciones/1raVez/1raVez.rpy:50
translate english primeraVezMed_25ac76ed:

    # protaPensa "No es buena estudiante, pero el dinero de [nombrePad3] ayuda mucho."
    protaPensa "She's not a good student, but [nombrePad3]'s money helps a lot."

# game/Code/Interacciones/1raVez/1raVez.rpy:60
translate english primeraVezMay_da4fc689:

    # protaPensa "Ella se llama [nombreMay] y es mi [situMay2]."
    protaPensa "Her name is [nombreMay] and she's my [situMay2]."

# game/Code/Interacciones/1raVez/1raVez.rpy:62
translate english primeraVezMay_8b067d08:

    # protaPensa "[nombreMay] es preciosa y tiene un físico impresionante. Creo que ha cogido los mejores genes de [nombreMad3] y la [nombreAbu]."
    protaPensa "[nombreMay] is beautiful and has an impressive physique. I think she has taken the best genes from [nombreMad3] and [nombreAbu]."

# game/Code/Interacciones/1raVez/1raVez.rpy:63
translate english primeraVezMay_75024a45:

    # protaPensa "Es una persona responsable muy dulce y me llevo muy bien con ella."
    protaPensa "She is a very sweet responsible person and I get along very well with her."

# game/Code/Interacciones/1raVez/1raVez.rpy:64
translate english primeraVezMay_f6bd18d7:

    # protaPensa "Hace relativamente poco abrió su propio negocio (con la ayuda económica de [nombrePad3]). Una tienda de ropa con prendas muy modernas y a la última moda."
    protaPensa "She recently opened her own business (with the financial help of [nombrePad3]). A clothing store with very trendy and fashionable clothes."

# game/Code/Interacciones/1raVez/1raVez.rpy:65
translate english primeraVezMay_e523b9c0:

    # protaPensa "Es novata en el negocio, pero tiene mucha ilusión en que su tienda salga adelante y de sus frutos."
    protaPensa "She is a rookie in the business, but she is very excited that her store will succeed and bear fruit."
# TODO: Translation updated at 2021-08-27 11:11

# game/Code/Interacciones/1raVez/1raVez.rpy:73
translate english primeraVezLucia_5437ed0d:

    # protaPensa "Ella se llama [nombreLucia]."
    protaPensa "Her name is [nombreLucia]."

# game/Code/Interacciones/1raVez/1raVez.rpy:75
translate english primeraVezLucia_afebb1f9:

    # protaPensa "Es la madre de Mike y es la mujer de la limpieza de la escuela."
    protaPensa "She is Mike's mother and is the cleaning lady at the school."

# game/Code/Interacciones/1raVez/1raVez.rpy:76
translate english primeraVezLucia_a3619726:

    # protaPensa "Mike no lleva nada bien que su madre sea la chacha de la escuela e intenta evitarla todo lo que puede."
    protaPensa "Mike doesn't take kindly to his mother being the school cleaning lady and tries to avoid her as much as he can."

# game/Code/Interacciones/1raVez/1raVez.rpy:77
translate english primeraVezLucia_ef414cfd:

    # protaPensa "[nombreLucia] es una mujer que tiene su encanto. No es tan atractiva como [nombreMad3] pero es muy presumida"
    protaPensa "[nombreLucia] is a woman who has her charm. She's not as attractive as [nombreMad3] but she's very snooty."

# game/Code/Interacciones/1raVez/1raVez.rpy:78
translate english primeraVezLucia_4fb2a7bb:

    # protaPensa "Solo hay que ver como viste. No debe ser nada cómodo limpiar con esa vestimenta pero a los alumnos ya nos va bien."
    protaPensa "Just look at the way she dresses. It must not be comfortable at all to clean in that outfit but we students do just fine."

# game/Code/Interacciones/1raVez/1raVez.rpy:79
translate english primeraVezLucia_a786b8f2:

    # protaPensa "[nombreLucia] no puede permitirse perder el empleo. En casa de Mike las cosas no van muy bien económicamente."
    protaPensa "[nombreLucia] can't afford to lose her job. Things aren't going so well financially at Mike's house."
# TODO: Translation updated at 2021-09-09 19:44

# game/Code/Interacciones/1raVez/1raVez.rpy:62
translate english primeraVezMay_a7b0268c:

    # protaPensa "[nombreMay] es preciosa y tiene un físico impresionante. Creo que ha cogido los mejores genes de [nombreMad3] y la [nombreAbu2]."
    protaPensa "[nombreMay] is beautiful and has an impressive physique. I think she has taken the best genes from [nombreMad3] and [nombreAbu2]."
