﻿# TODO: Translation updated at 2021-12-12 13:06

# game/Code/Interacciones/miCasa/SALON/mad/martes/Ethan/madSeVaConEthan.rpy:6
translate english madSeVaConEthan_00afdd6b:

    # mad "Esta bien, un masaje en los pies no me ira nada mal."
    mad "Okay, a foot massage wouldn't hurt."

# game/Code/Interacciones/miCasa/SALON/mad/martes/Ethan/madSeVaConEthan.rpy:8
translate english madSeVaConEthan_a6cdd581:

    # prota "¡Genial!"
    prota "Great!"

# game/Code/Interacciones/miCasa/SALON/mad/martes/Ethan/madSeVaConEthan.rpy:16
translate english madSeVaConEthan_6bacca09:

    # "¡Ding Dong!"
    "Ding Dong!"

# game/Code/Interacciones/miCasa/SALON/mad/martes/Ethan/madSeVaConEthan.rpy:18
translate english madSeVaConEthan_5aa59a0b:

    # mad "Ya abro yo la puerta."
    mad "I'll get the door."

# game/Code/Interacciones/miCasa/SALON/mad/martes/Ethan/madSeVaConEthan.rpy:20
translate english madSeVaConEthan_d16b749f:

    # protaPensa "¿Joder, quien será ahora?"
    protaPensa "Fuck, who will it be now?"

# game/Code/Interacciones/miCasa/SALON/mad/martes/Ethan/madSeVaConEthan.rpy:24
translate english madSeVaConEthan_1d195b55:

    # protaPensa "Ese es Ethan, el vecino y amigo de [situPad2]. Suele venir a ver los partidos con el."
    protaPensa "That's Ethan, [situPad2]'s neighbor and friend. He usually comes over to watch the games with him."

# game/Code/Interacciones/miCasa/SALON/mad/martes/Ethan/madSeVaConEthan.rpy:25
translate english madSeVaConEthan_8fc3fd16:

    # protaPensa "Lo he pillado varias veces mirando a [nombreMad3] de una manera poco sana. No me fio de el."
    protaPensa "I've caught him several times looking at [nombreMad3] in an unhealthy way. I don't trust him."

# game/Code/Interacciones/miCasa/SALON/mad/martes/Ethan/madSeVaConEthan.rpy:27
translate english madSeVaConEthan_212a6afd:

    # ethan "Hola Sofia."
    ethan "Hi Sofia."

# game/Code/Interacciones/miCasa/SALON/mad/martes/Ethan/madSeVaConEthan.rpy:29
translate english madSeVaConEthan_b83ed737:

    # mad "Oh Ethan, que sorpresa."
    mad "Oh Ethan, what a surprise."

# game/Code/Interacciones/miCasa/SALON/mad/martes/Ethan/madSeVaConEthan.rpy:33
translate english madSeVaConEthan_accf435a:

    # mad "Perdona que estoy con el pijama."
    mad "Sorry I'm in my pajamas."

# game/Code/Interacciones/miCasa/SALON/mad/martes/Ethan/madSeVaConEthan.rpy:37
translate english madSeVaConEthan_291a2957:

    # ethan "Oh no te preocupes, siento la molestia."
    ethan "Oh don't worry, I'm sorry for the bother."

# game/Code/Interacciones/miCasa/SALON/mad/martes/Ethan/madSeVaConEthan.rpy:39
translate english madSeVaConEthan_e8f88708:

    # mad "No eres una molestia Ethan."
    mad "You're not a bother Ethan."

# game/Code/Interacciones/miCasa/SALON/mad/martes/Ethan/madSeVaConEthan.rpy:41
translate english madSeVaConEthan_5fb728ef:

    # ethan "Gracias, siempre tan amable Sofia. He venido porque me quedado sin sal, y me preguntaba si me podías dar un poco."
    ethan "Thank you, always so kind Sofia. I came by because I'm out of salt, and I was wondering if you could give me some."

# game/Code/Interacciones/miCasa/SALON/mad/martes/Ethan/madSeVaConEthan.rpy:43
translate english madSeVaConEthan_b3f528e6:

    # mad "Oh claro no hay problema, pasa dame 1 min."
    mad "Oh sure no problem, come on in give me 1 min."

# game/Code/Interacciones/miCasa/SALON/mad/martes/Ethan/madSeVaConEthan.rpy:45
translate english madSeVaConEthan_6b6c85fc:

    # protaPensa "¿Sal? menuda mierda de excusa..."
    protaPensa "Salt? What a shitty excuse..."

# game/Code/Interacciones/miCasa/SALON/mad/martes/Ethan/madSeVaConEthan.rpy:47
translate english madSeVaConEthan_8644439c:

    # "Unos segundos más tarde..."
    "A few seconds later..."

# game/Code/Interacciones/miCasa/SALON/mad/martes/Ethan/madSeVaConEthan.rpy:49
translate english madSeVaConEthan_ae3f63ee:

    # mad "Toma Ethan coge la que necesites."
    mad "Here Ethan, take the one you need."

# game/Code/Interacciones/miCasa/SALON/mad/martes/Ethan/madSeVaConEthan.rpy:51
translate english madSeVaConEthan_1f2e39bb:

    # ethan "Genial, muchas gracias. ¿Y dónde está John? hace días que no le veo y tampoco me ha llamado"
    ethan "Great, thanks a lot. And where's John? I haven't seen him for days and he hasn't called me either."

# game/Code/Interacciones/miCasa/SALON/mad/martes/Ethan/madSeVaConEthan.rpy:53
translate english madSeVaConEthan_a691e402:

    # mad "Oh bueno, ha habido algún pequeño problema y está trabajando fuera, pero todo bien."
    mad "Oh well, there's been some little problem and he's working away, but everything's fine."

# game/Code/Interacciones/miCasa/SALON/mad/martes/Ethan/madSeVaConEthan.rpy:55
translate english madSeVaConEthan_050a3b80:

    # ethan "Que raro no me dijo nada... Por cierto, me comprado un violoncello, por favor avisarme si molesto al tocarlo."
    ethan "Strange, he didn't say anything.... By the way, he bought me a cello, please let me know if I bother to play it."

# game/Code/Interacciones/miCasa/SALON/mad/martes/Ethan/madSeVaConEthan.rpy:57
translate english madSeVaConEthan_89d84ad8:

    # mad "Ohh que sorpresa, no sabía que tocabas instrumentos. Yo cuando era pequeña di un par de clases con el violoncello y me encantaba, pero ya hace mucho tiempo de eso y ya no me acuerdo de nada."
    mad "Ohh what a surprise, I didn't know you played instruments. I had a couple of lessons with the cello when I was little and I loved it, but it's been a long time since that and I don't remember anything anymore."

# game/Code/Interacciones/miCasa/SALON/mad/martes/Ethan/madSeVaConEthan.rpy:63
translate english madSeVaConEthan_7b22e703:

    # ethan "¿Oye y porque no te vienes un rato y te lo muestro? podría darte una clase rápida, es muy bonito tocar instrumentos."
    ethan "Hey and why don't you come over for a while and I'll show you, I could give you a quick lesson, it's really nice to play instruments."

# game/Code/Interacciones/miCasa/SALON/mad/martes/Ethan/madSeVaConEthan.rpy:65
translate english madSeVaConEthan_15844ae1:

    # mad "Ohhh seguro que lo haría fatal y tengo cosas que hacer."
    mad "Ohhh sure I'd be terrible at it and I've got things to do."

# game/Code/Interacciones/miCasa/SALON/mad/martes/Ethan/madSeVaConEthan.rpy:67
translate english madSeVaConEthan_00150df6:

    # ethan "Vamos anímate no tienes nada que perder seguro que te diviertes."
    ethan "Come on cheer up you have nothing to lose I'm sure you'll have fun."

# game/Code/Interacciones/miCasa/SALON/mad/martes/Ethan/madSeVaConEthan.rpy:69
translate english madSeVaConEthan_48793c74:

    # mad "hahaha tú lo que quieres es reírte de lo mal que lo haría..."
    mad "hahaha what you want is to laugh at how bad I would do it..."

# game/Code/Interacciones/miCasa/SALON/mad/martes/Ethan/madSeVaConEthan.rpy:71
translate english madSeVaConEthan_108d0c0a:

    # ethan "Que no, venga anímate seguro que lo haces genial y lo pasaremos bien."
    ethan "What no, come on cheer up I'm sure you'll do great and we'll have a good time."

# game/Code/Interacciones/miCasa/SALON/mad/martes/Ethan/madSeVaConEthan.rpy:73
translate english madSeVaConEthan_bc0159c2:

    # mad "Esta bien, pero solo un rato, deja que me cambie de ropa."
    mad "Okay, but just for a while, let me change my clothes."

# game/Code/Interacciones/miCasa/SALON/mad/martes/Ethan/madSeVaConEthan.rpy:75
translate english madSeVaConEthan_ba77e1e3:

    # ethan "Claro, sin problemas."
    ethan "Sure, no problem."

# game/Code/Interacciones/miCasa/SALON/mad/martes/Ethan/madSeVaConEthan.rpy:77
translate english madSeVaConEthan_5cd897de:

    # "Unos minutos más tarde..."
    "A few minutes later..."

# game/Code/Interacciones/miCasa/SALON/mad/martes/Ethan/madSeVaConEthan.rpy:79
translate english madSeVaConEthan_c8d32021:

    # mad "Ya estoy lista."
    mad "I'm ready now."

# game/Code/Interacciones/miCasa/SALON/mad/martes/Ethan/madSeVaConEthan.rpy:81
translate english madSeVaConEthan_1fd49f80:

    # ethan "¡Perfecto!, tu primera."
    ethan "Perfect, your first."

# game/Code/Interacciones/miCasa/SALON/mad/martes/Ethan/madSeVaConEthan.rpy:89
translate english madSeVaConEthan_c3628fb4:

    # protaPensa "Hijo de puta..."
    protaPensa "Son of a bitch..."

# game/Code/Interacciones/miCasa/SALON/mad/martes/Ethan/madSeVaConEthan.rpy:95
translate english madSeVaConEthan_5cd897de_1:

    # "Unos minutos más tarde..."
    "A few minutes later..."

# game/Code/Interacciones/miCasa/SALON/mad/martes/Ethan/madSeVaConEthan.rpy:100
translate english madSeVaConEthan_0d4e2721:

    # protaPensa "Ya me colado en su jardín."
    protaPensa "I've already snuck into your garden."

# game/Code/Interacciones/miCasa/SALON/mad/martes/Ethan/madSeVaConEthan.rpy:102
translate english madSeVaConEthan_e74df1c6:

    # protaPensa "Tienen que estar por aquí..."
    protaPensa "They've got to be here somewhere..."

# game/Code/Interacciones/miCasa/SALON/mad/martes/Ethan/madSeVaConEthan.rpy:104
translate english madSeVaConEthan_945fadde:

    # protaPensa "Ahí están..."
    protaPensa "There they are..."

# game/Code/Interacciones/miCasa/SALON/mad/martes/Ethan/madSeVaConEthan.rpy:108
translate english madSeVaConEthan_aa56a28c:

    # mad "¡Caramba! que habitación tan acondicionada. Tienes muchos instrumentos y cosas para hacer gimnasia."
    mad "Wow, what a nicely appointed room. You've got lots of instruments and things to do exercises with."

# game/Code/Interacciones/miCasa/SALON/mad/martes/Ethan/madSeVaConEthan.rpy:110
translate english madSeVaConEthan_7f80958e:

    # mad "John nunca me dijo que tocabas instrumentos de música."
    mad "John never told me that you played musical instruments."

# game/Code/Interacciones/miCasa/SALON/mad/martes/Ethan/madSeVaConEthan.rpy:116
translate english madSeVaConEthan_4069a0fe:

    # ethan "Si desde pequeño que me gusta la música, es algo que me hace desconectar y relajarme."
    ethan "Yeah ever since I was little I've loved music, it's something that makes me unwind and relax."

# game/Code/Interacciones/miCasa/SALON/mad/martes/Ethan/madSeVaConEthan.rpy:118
translate english madSeVaConEthan_4434bb05:

    # ethan "Seguramente John no te dijo nada porque debe estar muy ocupado con su trabajo..."
    ethan "Surely John didn't tell you anything because he must be very busy with his work..."

# game/Code/Interacciones/miCasa/SALON/mad/martes/Ethan/madSeVaConEthan.rpy:120
translate english madSeVaConEthan_315b4afb:

    # mad "..."
    mad "..."

# game/Code/Interacciones/miCasa/SALON/mad/martes/Ethan/madSeVaConEthan.rpy:122
translate english madSeVaConEthan_5054eb37:

    # ethan "Oh disculpa por no ofrecerte nada de beber, ahora vengo y traigo algo..."
    ethan "Oh excuse me for not offering you anything to drink, I'll come and get you something..."

# game/Code/Interacciones/miCasa/SALON/mad/martes/Ethan/madSeVaConEthan.rpy:124
translate english madSeVaConEthan_9744ca2a:

    # mad "Oh no te preocupes, no es necesario, no me quedare mucho rato..."
    mad "Oh don't worry, it's not necessary, I won't stay long..."

# game/Code/Interacciones/miCasa/SALON/mad/martes/Ethan/madSeVaConEthan.rpy:126
translate english madSeVaConEthan_1fb10c4a:

    # ethan "Por supuesto que sí, ahora vengo. Como si estuvieras en tu casa..."
    ethan "Of course I will, I'll be right back. As if you were at home..."

# game/Code/Interacciones/miCasa/SALON/mad/martes/Ethan/madSeVaConEthan.rpy:128
translate english madSeVaConEthan_8bc76501:

    # mad "Eres muy amable."
    mad "You're very kind."

# game/Code/Interacciones/miCasa/SALON/mad/martes/Ethan/madSeVaConEthan.rpy:132
translate english madSeVaConEthan_9fff1437:

    # madPensa "Ahí está el violoncello..."
    madPensa "There's the cello..."

# game/Code/Interacciones/miCasa/SALON/mad/martes/Ethan/madSeVaConEthan.rpy:134
translate english madSeVaConEthan_e763e581:

    # madPensa "Es una habitación muy agradable..."
    madPensa "It's a very nice room..."

# game/Code/Interacciones/miCasa/SALON/mad/martes/Ethan/madSeVaConEthan.rpy:150
translate english madSeVaConEthan_d70fe418:

    # ethan "Lo haces bien."
    ethan "You're doing well."

# game/Code/Interacciones/miCasa/SALON/mad/martes/Ethan/madSeVaConEthan.rpy:156
translate english madSeVaConEthan_c0629ac7:

    # mad "Uhh me has asustado hahaha."
    mad "Uhh you scared me hahaha."

# game/Code/Interacciones/miCasa/SALON/mad/martes/Ethan/madSeVaConEthan.rpy:158
translate english madSeVaConEthan_441df2b4:

    # ethan "Sofia, te traído un poco de vino, no tenía nada más fresco."
    ethan "Sofia, I brought you some wine, I didn't have anything cooler."

# game/Code/Interacciones/miCasa/SALON/mad/martes/Ethan/madSeVaConEthan.rpy:160
translate english madSeVaConEthan_662cea1d:

    # mad "Oh bueno, está bien. Con un simple vaso de agua era suficiente."
    mad "Oh well, that's okay. Just a glass of water was enough."

# game/Code/Interacciones/miCasa/SALON/mad/martes/Ethan/madSeVaConEthan.rpy:166
translate english madSeVaConEthan_cf2b859f:

    # mad "Esta muy rico gracias."
    mad "It's very nice thank you."

# game/Code/Interacciones/miCasa/SALON/mad/martes/Ethan/madSeVaConEthan.rpy:168
translate english madSeVaConEthan_c044bc49:

    # ethan "Si, es un vino que me gusta mucho. Me alegro que a ti también te guste."
    ethan "Yes, it's a wine I like very much. I'm glad you like it too."

# game/Code/Interacciones/miCasa/SALON/mad/martes/Ethan/madSeVaConEthan.rpy:170
translate english madSeVaConEthan_95fa6dc2:

    # ethan "Parece que se te ha desabrochado un botón de la camisa."
    ethan "You look like a button has come undone on your shirt."

# game/Code/Interacciones/miCasa/SALON/mad/martes/Ethan/madSeVaConEthan.rpy:174
translate english madSeVaConEthan_14c0edf6:

    # mad "Uh!"
    mad "Uh!"

# game/Code/Interacciones/miCasa/SALON/mad/martes/Ethan/madSeVaConEthan.rpy:176
translate english madSeVaConEthan_6fdf3013:

    # mad "Que situación más incomoda, lo siento mucho que vergüenza, gracias por avisarme."
    mad "What an awkward situation, I'm so sorry how embarrassing, thanks for letting me know."

# game/Code/Interacciones/miCasa/SALON/mad/martes/Ethan/madSeVaConEthan.rpy:178
translate english madSeVaConEthan_8385d1a5:

    # ethan "Seguramente golpeaste muy fuerte el saco y se te desabrocho el botón de la camisa. ¿A caso quieres matar a alguien? hahaha."
    ethan "You probably hit the bag too hard and your shirt button came undone. are you trying to kill someone? hahaha."

# game/Code/Interacciones/miCasa/SALON/mad/martes/Ethan/madSeVaConEthan.rpy:180
translate english madSeVaConEthan_42ce5c6c:

    # mad "¡Oh dios! que vergüenza, gracias."
    mad "Oh god! what a shame, thank you."

# game/Code/Interacciones/miCasa/SALON/mad/martes/Ethan/madSeVaConEthan.rpy:182
translate english madSeVaConEthan_44f36555:

    # ethan "Ese saco es la mejor manera de desestresarse... Es la mejor terapia."
    ethan "That bag is the best way to de-stress.... It's the best therapy."

# game/Code/Interacciones/miCasa/SALON/mad/martes/Ethan/madSeVaConEthan.rpy:184
translate english madSeVaConEthan_5cc2623f:

    # mad "Hahaha"
    mad "Hahaha"

# game/Code/Interacciones/miCasa/SALON/mad/martes/Ethan/madSeVaConEthan.rpy:186
translate english madSeVaConEthan_96199b2e:

    # mad "Voy a probar con el violoncelo o se hará tarde."
    mad "I'm going to try the cello or it's going to get late."

# game/Code/Interacciones/miCasa/SALON/mad/martes/Ethan/madSeVaConEthan.rpy:188
translate english madSeVaConEthan_c9e756a2:

    # ethan "Claro todo tuyo."
    ethan "Sure all yours."

# game/Code/Interacciones/miCasa/SALON/mad/martes/Ethan/madSeVaConEthan.rpy:190
translate english madSeVaConEthan_2b2bffd0:

    # mad "Creo que era así..."
    mad "I think it was like this..."

# game/Code/Interacciones/miCasa/SALON/mad/martes/Ethan/madSeVaConEthan.rpy:192
translate english madSeVaConEthan_b32073a7:

    # mad "Ohh que horror..."
    mad "Ohh what a horror..."

# game/Code/Interacciones/miCasa/SALON/mad/martes/Ethan/madSeVaConEthan.rpy:194
translate english madSeVaConEthan_ef43273b:

    # mad "Han pasado tantos años que ni me acuerdo."
    mad "It's been so many years that I don't even remember."

# game/Code/Interacciones/miCasa/SALON/mad/martes/Ethan/madSeVaConEthan.rpy:196
translate english madSeVaConEthan_af10f0bd:

    # ethan "Con la música no se puede correr, hay que sentirla."
    ethan "With music you can't run, you have to feel it."

# game/Code/Interacciones/miCasa/SALON/mad/martes/Ethan/madSeVaConEthan.rpy:198
translate english madSeVaConEthan_1b55c4b9:

    # ethan "Te has de relajar Sofia y te saldrá mejor..."
    ethan "You have to relax Sofia and you'll do better..."

# game/Code/Interacciones/miCasa/SALON/mad/martes/Ethan/madSeVaConEthan.rpy:202
translate english madSeVaConEthan_ed2e7885:

    # protaPensa "Como intente algo ese cabrón, le voy a parar los pies."
    protaPensa "If that bastard tries anything, I'm going to stop his feet."

# game/Code/Interacciones/miCasa/SALON/mad/martes/Ethan/madSeVaConEthan.rpy:206
translate english madSeVaConEthan_142ab887:

    # ethan "Así... Prueba ahora."
    ethan "Like this... Try it now."

# game/Code/Interacciones/miCasa/SALON/mad/martes/Ethan/madSeVaConEthan.rpy:210
translate english madSeVaConEthan_797b59d3:

    # ethan "Es cosa de paciencia perseverancia y de estar en sintonía y relajada."
    ethan "It's a matter of patience, perseverance and being in tune and relaxed."

# game/Code/Interacciones/miCasa/SALON/mad/martes/Ethan/madSeVaConEthan.rpy:212
translate english madSeVaConEthan_5fd169b9:

    # ethan "Así te saldrá mejor."
    ethan "That's how you'll do it better."

# game/Code/Interacciones/miCasa/SALON/mad/martes/Ethan/madSeVaConEthan.rpy:214
translate english madSeVaConEthan_dcffcca8:

    # mad "Esta bien..."
    mad "Okay..."

# game/Code/Interacciones/miCasa/SALON/mad/martes/Ethan/madSeVaConEthan.rpy:225
translate english madSeVaConEthan_3a6b9139:

    # "Despues de unos minutos practicando..."
    "After a few minutes of practice..."

# game/Code/Interacciones/miCasa/SALON/mad/martes/Ethan/madSeVaConEthan.rpy:227
translate english madSeVaConEthan_57954a42:

    # ethan "Mucho mejor Sofia. Con unas pocas clases más seguro que te volverías una profesional."
    ethan "Much better Sofia. With a few more classes I'm sure you'd become a pro."

# game/Code/Interacciones/miCasa/SALON/mad/martes/Ethan/madSeVaConEthan.rpy:229
translate english madSeVaConEthan_6e72cee9:

    # mad "Hahaha no te rías de mí."
    mad "Hahaha don't laugh at me."

# game/Code/Interacciones/miCasa/SALON/mad/martes/Ethan/madSeVaConEthan.rpy:231
translate english madSeVaConEthan_390ec833:

    # ethan "Podemos practicar con las notas principales."
    ethan "We can practice with the main notes."

# game/Code/Interacciones/miCasa/SALON/mad/martes/Ethan/madSeVaConEthan.rpy:233
translate english madSeVaConEthan_cf51c900:

    # mad "Oh creo que ya es hora de que me vaya, ha estado divertido."
    mad "Oh I think it's time for me to go, it's been fun."

# game/Code/Interacciones/miCasa/SALON/mad/martes/Ethan/madSeVaConEthan.rpy:235
translate english madSeVaConEthan_264ac6cc:

    # ethan "¿Tan pronto?"
    ethan "So soon?"

# game/Code/Interacciones/miCasa/SALON/mad/martes/Ethan/madSeVaConEthan.rpy:237
translate english madSeVaConEthan_05a8bd7c:

    # ethan "¿Está bien porque no te vienes otro día y seguimos?"
    ethan "Okay, why don't you come over another day and we'll continue?"

# game/Code/Interacciones/miCasa/SALON/mad/martes/Ethan/madSeVaConEthan.rpy:239
translate english madSeVaConEthan_7524af5a:

    # mad "Oh bueno ya veremos a ver si saco un poco de tiempo."
    mad "Oh well we'll see if I can find some time."

# game/Code/Interacciones/miCasa/SALON/mad/martes/Ethan/madSeVaConEthan.rpy:241
translate english madSeVaConEthan_f32a3e1f:

    # ethan "Por supuesto, cuídate mucho."
    ethan "Of course, take care of yourself."

# game/Code/Interacciones/miCasa/SALON/mad/martes/Ethan/madSeVaConEthan.rpy:245
translate english madSeVaConEthan_e3e709a4:

    # protaPensa "¡Maldito bastardo! ya veo donde van sus intenciones..."
    protaPensa "Damn bastard! I see where his intentions are going..."

# game/Code/Interacciones/miCasa/SALON/mad/martes/Ethan/madSeVaConEthan.rpy:247
translate english madSeVaConEthan_ca6f3cd0:

    # protaPensa "Sera mejor que vuelva a casa antes que [nombreMad3]."
    protaPensa "I'd better get home before [nombreMad3]."

translate english strings:

    # game/Code/Interacciones/miCasa/SALON/mad/martes/Ethan/madSeVaConEthan.rpy:92
    old "Seguirles"
    new "Follow them."
