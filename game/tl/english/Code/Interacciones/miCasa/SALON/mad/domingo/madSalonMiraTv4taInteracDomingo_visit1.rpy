﻿# TODO: Translation updated at 2021-07-08 13:36

# game/Code/Interacciones/miCasa/SALON/mad/domingo/madSalonMiraTv4taInteracDomingo_visit1.rpy:10
translate english madSalonMiraTv4taInteracDomingo_visit1_2d490a9b:

    # prota "Hola [nombreMad3]"
    prota "Hello [nombreMad3]"

# game/Code/Interacciones/miCasa/SALON/mad/domingo/madSalonMiraTv4taInteracDomingo_visit1.rpy:14
translate english madSalonMiraTv4taInteracDomingo_visit1_b0f88cdb:

    # prota "estas mirando la tele?"
    prota "Are you watching TV?"

# game/Code/Interacciones/miCasa/SALON/mad/domingo/madSalonMiraTv4taInteracDomingo_visit1.rpy:24
translate english madSalonMiraTv4taInteracDomingo_visit1_fc2d2393:

    # mad "Hola [nombreProta2]"
    mad "Hello [nombreProta2]"

# game/Code/Interacciones/miCasa/SALON/mad/domingo/madSalonMiraTv4taInteracDomingo_visit1.rpy:30
translate english madSalonMiraTv4taInteracDomingo_visit1_4c93645a:

    # mad "Iba a ver la tele con tu [situPad]"
    mad "I was going to watch TV with your [situPad]"

# game/Code/Interacciones/miCasa/SALON/mad/domingo/madSalonMiraTv4taInteracDomingo_visit1.rpy:34
translate english madSalonMiraTv4taInteracDomingo_visit1_30e53c6d:

    # mad "Pero se ha tenido que quedar trabajando"
    mad "But he had to stay at work."

# game/Code/Interacciones/miCasa/SALON/mad/domingo/madSalonMiraTv4taInteracDomingo_visit1.rpy:39
translate english madSalonMiraTv4taInteracDomingo_visit1_7e8101f6:

    # mad "Quieres ver la tele conmigo?"
    mad "Do you want to watch TV with me?"

# game/Code/Interacciones/miCasa/SALON/mad/domingo/madSalonMiraTv4taInteracDomingo_visit1.rpy:74
translate english madSalonMiraTv4taInteracDomingo_visit1_c275e66c:

    # prota "En el canal 2 empieza una peli. ¿Quieres que la veamos?"
    prota "There's a movie starting on channel 2. Do you want to watch it?"

# game/Code/Interacciones/miCasa/SALON/mad/domingo/madSalonMiraTv4taInteracDomingo_visit1.rpy:83
translate english madSalonMiraTv4taInteracDomingo_visit1_8d91418f:

    # mad "Seguro que es mas interesante que lo que estoy viendo ahora"
    mad "I'm sure it's more interesting than what I'm watching now"

# game/Code/Interacciones/miCasa/SALON/mad/domingo/madSalonMiraTv4taInteracDomingo_visit1.rpy:92
translate english madSalonMiraTv4taInteracDomingo_visit1_aa231434:

    # prota "Ahora te la pongo"
    prota "I'll put it on for you"
# TODO: Translation updated at 2021-08-27 11:11

# game/Code/Interacciones/miCasa/SALON/mad/domingo/madSalonMiraTv4taInteracDomingo_visit1.rpy:55
translate english madSalonMiraTv4taInteracDomingo_visit1_9fbb95d3:

    # prota "Puedo unirme y miramos una peli?"
    prota "Can I join you and watch a movie?"

# game/Code/Interacciones/miCasa/SALON/mad/domingo/madSalonMiraTv4taInteracDomingo_visit1.rpy:59
translate english madSalonMiraTv4taInteracDomingo_visit1_e23f05f6:

    # mad "Si, claro"
    mad "Yes, of course"
