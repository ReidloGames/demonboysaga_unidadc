﻿# TODO: Translation updated at 2021-07-08 13:36

# game/Code/Interacciones/miCasa/SALON/Tv/verTvSalon.rpy:7
translate english verTvSalon_e7b3486e:

    # protaPensa "Es muy tarde, no tengo ganas de ver la Tv."
    protaPensa "It's very late, I don't feel like watching TV."

# TODO: Translation updated at 2021-07-19 09:51

# game/Code/Interacciones/miCasa/SALON/Tv/verTvSalon.rpy:5
translate english verTvSalon_50126e22:

    # protaPensa "Ahora no es momento de ver la Tv."
    protaPensa "Now is not the time to watch TV."
# TODO: Translation updated at 2021-07-21 19:11

translate english strings:

    # game/Code/Interacciones/miCasa/SALON/Tv/verTvSalon.rpy:9
    old "Ver Tv"
    new "Watch Tv"

    # game/Code/Interacciones/miCasa/SALON/Tv/verTvSalon.rpy:9
    old "No ver Tv"
    new "Do not watch Tv"
# TODO: Translation updated at 2021-08-27 11:11

# game/Code/Interacciones/miCasa/SALON/Tv/verTvSalon.rpy:24
translate english verTvSalon_56390ec2:

    # protaPensa "Ahora no puedo ver la Tv. Tengo que ir a la Universidad"
    protaPensa "I can't watch TV now. I have to go to the University"
# TODO: Translation updated at 2022-02-16 11:36

# game/Code/Interacciones/miCasa/SALON/Tv/verTvSalon.rpy:29
translate english verTvSalonPrimerDia_041dac7b:

    # protaPensa "No puedo ver la televisión en este momento."
    protaPensa "I can't watch TV right now."
