﻿# TODO: Translation updated at 2021-08-06 14:00

# game/Code/Interacciones/miCasa/SALON/family/viernes/familyViendoTv4taInteracViernes_visit1.rpy:17
translate english viendoTvLos4_9ef3d575:

    # "Te estábamos esperando."
    "We've been waiting for you."

# game/Code/Interacciones/miCasa/SALON/family/viernes/familyViendoTv4taInteracViernes_visit1.rpy:18
translate english viendoTvLos4_b25c94db:

    # "Vamos a ver una peli."
    "Let's go see a movie."

# game/Code/Interacciones/miCasa/SALON/family/viernes/familyViendoTv4taInteracViernes_visit1.rpy:76
translate english viendoTvLos5_9ef3d575:

    # "Te estábamos esperando."
    "We've been waiting for you."

# game/Code/Interacciones/miCasa/SALON/family/viernes/familyViendoTv4taInteracViernes_visit1.rpy:77
translate english viendoTvLos5_b25c94db:

    # "Vamos a ver una peli."
    "Let's go see a movie."

translate english strings:

    # game/Code/Interacciones/miCasa/SALON/family/viernes/familyViendoTv4taInteracViernes_visit1.rpy:22
    old "Sentarte con [nombreMay]"
    new "Sit with [nombreMay]."

    # game/Code/Interacciones/miCasa/SALON/family/viernes/familyViendoTv4taInteracViernes_visit1.rpy:22
    old "Sentarte con [nombreMed]"
    new "Sit with [nombreMed]"

    # game/Code/Interacciones/miCasa/SALON/family/viernes/familyViendoTv4taInteracViernes_visit1.rpy:22
    old "Sentarte con [nombreMad3]"
    new "Sit with [nombreMad3]"

    # game/Code/Interacciones/miCasa/SALON/family/viernes/familyViendoTv4taInteracViernes_visit1.rpy:22
    old "Sentarte con [nombrePeq]"
    new "Sit with [nombrePeq]."
