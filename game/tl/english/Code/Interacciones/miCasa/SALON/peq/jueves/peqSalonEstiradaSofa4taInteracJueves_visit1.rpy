﻿# TODO: Translation updated at 2021-08-06 14:00

# game/Code/Interacciones/miCasa/SALON/peq/jueves/peqSalonEstiradaSofa4taInteracJueves_visit1.rpy:8
translate english peqSalonEstiradaSofa4taInteracJueves_visit1_9bc577d2:

    # prota "Hola [nombrePeq] ¿Puedo sentarme?"
    prota "Hi [nombrePeq] Can I sit down?"

# game/Code/Interacciones/miCasa/SALON/peq/jueves/peqSalonEstiradaSofa4taInteracJueves_visit1.rpy:12
translate english peqSalonEstiradaSofa4taInteracJueves_visit1_8d1c526c:

    # hermanPeq "Por supuesto, siéntate."
    hermanPeq "Of course, sit down."

# game/Code/Interacciones/miCasa/SALON/peq/jueves/peqSalonEstiradaSofa4taInteracJueves_visit1.rpy:43
translate english noCosquillas_69b3a5f1:

    # protaPensa "No puedo, [nombreMed] está a mi lado y me observa."
    protaPensa "I can't, [nombreMed] is next to me and watching me."

# game/Code/Interacciones/miCasa/SALON/peq/jueves/peqSalonEstiradaSofa4taInteracJueves_visit1.rpy:63
translate english hacerCosquillas_b08deae9:

    # hermanPeq "jajajaja"
    hermanPeq "hahahahahaha"

# game/Code/Interacciones/miCasa/SALON/peq/jueves/peqSalonEstiradaSofa4taInteracJueves_visit1.rpy:66
translate english hacerCosquillas_45266186:

    # hermanPeq "¡¡¡Me haces cosquillas malo!!! ajajaja"
    hermanPeq "You're tickling me!!!! hahahaha"

# game/Code/Interacciones/miCasa/SALON/peq/jueves/peqSalonEstiradaSofa4taInteracJueves_visit1.rpy:78
translate english hacerCosquillas_b08deae9_1:

    # hermanPeq "jajajaja"
    hermanPeq "hahahahaha"

# game/Code/Interacciones/miCasa/SALON/peq/jueves/peqSalonEstiradaSofa4taInteracJueves_visit1.rpy:79
translate english hacerCosquillas_ec48d851:

    # hermanPeq "Pórtate bien o tendrás guerra"
    hermanPeq "Behave yourself or you will have a war"

# game/Code/Interacciones/miCasa/SALON/peq/jueves/peqSalonEstiradaSofa4taInteracJueves_visit1.rpy:97
translate english golpeaMed_81839abf:

    # hermanMed "Serás idiota."
    hermanMed "You´re such an idiot."

translate english strings:

    # game/Code/Interacciones/miCasa/SALON/peq/jueves/peqSalonEstiradaSofa4taInteracJueves_visit1.rpy:22
    old "Hacer cosquillas en los pies de [nombrePeq]"
    new "Tickle [nombrePeq]'s feet."

    # game/Code/Interacciones/miCasa/SALON/peq/jueves/peqSalonEstiradaSofa4taInteracJueves_visit1.rpy:22
    old "Golpear [nombreMed]"
    new "Hitting [nombreMed]"
