﻿# TODO: Translation updated at 2021-08-06 14:00

# game/Code/Interacciones/miCasa/HABHERMANMED/med/martes/medEnSuHabitacionDepieArmario4taInteracMartes_visit1.rpy:9
translate english medEnSuHabitacionDepieArmario4taInteracMartes_visit1_fec38118:

    # hermanMed "Oye no entres en mi habitación."
    hermanMed "Hey don't come into my room."

# game/Code/Interacciones/miCasa/HABHERMANMED/med/martes/medEnSuHabitacionDepieArmario4taInteracMartes_visit1.rpy:11
translate english medEnSuHabitacionDepieArmario4taInteracMartes_visit1_4eb937ec:

    # hermanMed "Eres muy pesado. ¡Fuera!"
    hermanMed "You're a pain in the ass. get out!"

# game/Code/Interacciones/miCasa/HABHERMANMED/med/martes/medEnSuHabitacionDepieArmario4taInteracMartes_visit1.rpy:13
translate english medEnSuHabitacionDepieArmario4taInteracMartes_visit1_4e4cbf44:

    # prota "Pero..."
    prota "But..."

# game/Code/Interacciones/miCasa/HABHERMANMED/med/martes/medEnSuHabitacionDepieArmario4taInteracMartes_visit1.rpy:15
translate english medEnSuHabitacionDepieArmario4taInteracMartes_visit1_7c1086df:

    # hermanMed "¡Adiós!"
    hermanMed "Goodbye!"

# game/Code/Interacciones/miCasa/HABHERMANMED/med/martes/medEnSuHabitacionDepieArmario4taInteracMartes_visit1.rpy:17
translate english medEnSuHabitacionDepieArmario4taInteracMartes_visit1_b6493c77:

    # prota "No me quiere ni ver."
    prota "She doesn't even want to see me."
