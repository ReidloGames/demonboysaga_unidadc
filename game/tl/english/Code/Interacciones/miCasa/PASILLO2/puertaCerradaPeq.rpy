﻿# TODO: Translation updated at 2021-07-08 13:36

# game/Code/Interacciones/miCasa/PASILLO2/puertaCerradaPeq.rpy:5
translate english puertaCerradaPeq_cc3df183:

    # protaPensa "La puerta de la habitación esta cerrada"
    protaPensa "The door to the room is closed."

# game/Code/Interacciones/miCasa/PASILLO2/puertaCerradaPeq.rpy:9
translate english puertaCerradaPeq_51b6d3ba:

    # protaPensa "La puerta esta cerrada..."
    protaPensa "The door is closed..."

# game/Code/Interacciones/miCasa/PASILLO2/puertaCerradaPeq.rpy:34
translate english noIrHabPeqOtraVez_8710c503:

    # protaPensa "Ya he estado aquí"
    protaPensa "I've already been here."

# game/Code/Interacciones/miCasa/PASILLO2/puertaCerradaPeq.rpy:43
translate english noEntrarEscenaPeq_5a5ccbbc:

    # protaPensa "Mejor no.."
    protaPensa "Better not..."
# TODO: Translation updated at 2021-10-13 15:38

# game/Code/Interacciones/miCasa/PASILLO2/puertaCerradaPeq.rpy:8
translate english puertaCerradaPeq_4405f1a9:

    # protaPensa "Tengo que encontrar la manera de poder entrar por la noche"
    protaPensa "I have to find a way to get in at night."
