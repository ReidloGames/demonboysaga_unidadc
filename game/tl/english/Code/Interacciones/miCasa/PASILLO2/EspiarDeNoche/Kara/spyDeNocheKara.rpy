﻿# TODO: Translation updated at 2022-03-27 17:09

# game/Code/Interacciones/miCasa/PASILLO2/EspiarDeNoche/Kara/spyDeNocheKara.rpy:9
translate english spyDeNocheKara_e351f705:

    # protaPensa "Parece que Kara no esta..."
    protaPensa "It seems that Kara is not here..."

# game/Code/Interacciones/miCasa/PASILLO2/EspiarDeNoche/Kara/spyDeNocheKara.rpy:23
translate english protaEntraHabMayDeNoche_62951016:

    # protaPensa "Lo conseguí."
    protaPensa "I made it."

# game/Code/Interacciones/miCasa/PASILLO2/EspiarDeNoche/Kara/spyDeNocheKara.rpy:27
translate english protaEntraHabMayDeNoche_ba49e239:

    # protaPensa "Parece que duerme plácidamente."
    protaPensa "She seems to be sleeping peacefully."

# game/Code/Interacciones/miCasa/PASILLO2/EspiarDeNoche/Kara/spyDeNocheKara.rpy:29
translate english protaEntraHabMayDeNoche_5ba0dfed:

    # protaPensa "Pero Kara se despierta con facilidad."
    protaPensa "But Kara wakes up easily."

# game/Code/Interacciones/miCasa/PASILLO2/EspiarDeNoche/Kara/spyDeNocheKara.rpy:31
translate english protaEntraHabMayDeNoche_3c8b994c:

    # protaPensa "Kara siempre bebe agua antes de ir a dormir. "
    protaPensa "Kara always drinks water before going to sleep."

# game/Code/Interacciones/miCasa/PASILLO2/EspiarDeNoche/Kara/spyDeNocheKara.rpy:33
translate english protaEntraHabMayDeNoche_4e07843f:

    # protaPensa "Si se despierta me mataría. Debería comprar algún tipo de valeriana en la tienda."
    protaPensa "If she wakes up she would kill me. I should buy some kind of valerian root in the store."

# game/Code/Interacciones/miCasa/PASILLO2/EspiarDeNoche/Kara/spyDeNocheKara.rpy:41
translate english protaEntraHabMayDeNoche_c1d8b95f:

    # protaPensa "Tengo que poner las pastillas en el vaso por la mañana."
    protaPensa "I have to put the pills in the glass in the morning."

# game/Code/Interacciones/miCasa/PASILLO2/EspiarDeNoche/Kara/spyDeNocheKara.rpy:108
translate english mamadaBocaKaraHabMaySpy_2c226dcf:

    # protaPensa "No te despiertes..."
    protaPensa "Don't wake up..."

# game/Code/Interacciones/miCasa/PASILLO2/EspiarDeNoche/Kara/spyDeNocheKara.rpy:113
translate english mamadaBocaKaraHabMaySpy_821b83d9:

    # "Oh dios mio... Las pastillas funcionan. Increíble...{p=1.5}{nw}"
    "Oh, my God... The pills work. Incredible...{p=1.5}{nw}"

# game/Code/Interacciones/miCasa/PASILLO2/EspiarDeNoche/Kara/spyDeNocheKara.rpy:115
translate english mamadaBocaKaraHabMaySpy_c3f28a9c:

    # "A este paso no podre aguantar mucho.{p=1.5}{nw}"
    "At this rate I will not be able to endure much."

# game/Code/Interacciones/miCasa/PASILLO2/EspiarDeNoche/Kara/spyDeNocheKara.rpy:121
translate english mamadaBocaKaraHabMaySpy_a42b2a1a:

    # protaPensa "Me voy a correr..."
    protaPensa "I'm going to cum..."

# game/Code/Interacciones/miCasa/PASILLO2/EspiarDeNoche/Kara/spyDeNocheKara.rpy:135
translate english mamadaBocaKaraHabMaySpy_3d0dee7d:

    # protaPensa "Le llenado toda la boca de semen. Espero que mañana no se de cuenta."
    protaPensa "I filled her whole mouth with semen. I hope she doesn't realize it tomorrow."

# game/Code/Interacciones/miCasa/PASILLO2/EspiarDeNoche/Kara/spyDeNocheKara.rpy:137
translate english mamadaBocaKaraHabMaySpy_992875d3:

    # protaPensa "Me voy antes de que se despierte..."
    protaPensa "I'm leaving before she wakes up..."

# game/Code/Interacciones/miCasa/PASILLO2/EspiarDeNoche/Kara/spyDeNocheKara.rpy:139
translate english mamadaBocaKaraHabMaySpy_4e528dc4:

    # protaPensa "Que se despierta!"
    protaPensa "Who is now waking up!"

# game/Code/Interacciones/miCasa/PASILLO2/EspiarDeNoche/Kara/spyDeNocheKara.rpy:164
translate english tetasKaraHabMaySpy_10e3e902:

    # protaPensa "Oh dios mio..."
    protaPensa "Oh, my God..."

# game/Code/Interacciones/miCasa/PASILLO2/EspiarDeNoche/Kara/spyDeNocheKara.rpy:167
translate english tetasKaraHabMaySpy_4847f6e0:

    # "Ojala pudiera apretarlas fuerte...{p=1.5}{nw}"
    "I wish I could squeeze them hard..."

# game/Code/Interacciones/miCasa/PASILLO2/EspiarDeNoche/Kara/spyDeNocheKara.rpy:171
translate english tetasKaraHabMaySpy_0c38b7c3:

    # protaPensa "¡Me corro!"
    protaPensa "I'm cumming!"

# game/Code/Interacciones/miCasa/PASILLO2/EspiarDeNoche/Kara/spyDeNocheKara.rpy:179
translate english tetasKaraHabMaySpy_252b57c0:

    # protaPensa "Menuda corrida... La limpiare un poco o se podría dar cuenta."
    protaPensa "What a cumshot... I'll clean it up a bit, or she might notice."

# game/Code/Interacciones/miCasa/PASILLO2/EspiarDeNoche/Kara/spyDeNocheKara.rpy:196
translate english conyoKaraHabMaySpy_94725aec:

    # protaPensa "A ver qué escondes..."
    protaPensa "Let's see what you are hiding..."

# game/Code/Interacciones/miCasa/PASILLO2/EspiarDeNoche/Kara/spyDeNocheKara.rpy:205
translate english conyoKaraHabMaySpy_eb0ec4a7:

    # protaPensa "Tiene los pantalones muy apretados."
    protaPensa "She has very tight pants."

# game/Code/Interacciones/miCasa/PASILLO2/EspiarDeNoche/Kara/spyDeNocheKara.rpy:207
translate english conyoKaraHabMaySpy_f212a64f:

    # protaPensa "Muestrame el coñito Kara..."
    protaPensa "Show me it, Kara..."

# game/Code/Interacciones/miCasa/PASILLO2/EspiarDeNoche/Kara/spyDeNocheKara.rpy:220
translate english conyoKaraHabMaySpy_4927963e:

    # protaPensa "Casi se despierta..."
    protaPensa "She almost woke up..."

# game/Code/Interacciones/miCasa/PASILLO2/EspiarDeNoche/Kara/spyDeNocheKara.rpy:222
translate english conyoKaraHabMaySpy_4700cb86:

    # protaPensa "¿No se habrá tomado el vaso de agua? o tiene la zona muy sensible?"
    protaPensa "Hasn't the glass of water been taken? Or do you just have a very sensitive area?"

translate english strings:

    # game/Code/Interacciones/miCasa/PASILLO2/EspiarDeNoche/Kara/spyDeNocheKara.rpy:61
    old "Quitar manta"
    new "Remove blanket"

    # game/Code/Interacciones/miCasa/PASILLO2/EspiarDeNoche/Kara/spyDeNocheKara.rpy:67
    old "Boca"
    new "Mouth"

    # game/Code/Interacciones/miCasa/PASILLO2/EspiarDeNoche/Kara/spyDeNocheKara.rpy:72
    old "Tetas"
    new "Tits"

    # game/Code/Interacciones/miCasa/PASILLO2/EspiarDeNoche/Kara/spyDeNocheKara.rpy:72
    old "Coño"
    new "Pussy"

    # game/Code/Interacciones/miCasa/PASILLO2/EspiarDeNoche/Kara/spyDeNocheKara.rpy:153
    old "Mostrar pechos"
    new "Show breasts"

    # game/Code/Interacciones/miCasa/PASILLO2/EspiarDeNoche/Kara/spyDeNocheKara.rpy:191
    old "Abrir piernas"
    new "Open legs"

    # game/Code/Interacciones/miCasa/PASILLO2/EspiarDeNoche/Kara/spyDeNocheKara.rpy:200
    old "Abrir pantalón"
    new "Remove panties"
