﻿# TODO: Translation updated at 2021-07-08 13:36

# game/Code/Interacciones/miCasa/PASILLO2/puertaCerradaMay.rpy:6
translate english puertaCerradaMay_cc3df183:

    # protaPensa "La puerta de la habitación esta cerrada"
    protaPensa "The door to the room is closed."

# game/Code/Interacciones/miCasa/PASILLO2/puertaCerradaMay.rpy:10
translate english puertaCerradaMay_51b6d3ba:

    # protaPensa "La puerta esta cerrada..."
    protaPensa "The door is closed..."

# game/Code/Interacciones/miCasa/PASILLO2/puertaCerradaMay.rpy:35
translate english noIrHabMayOtraVez_8710c503:

    # protaPensa "Ya he estado aquí"
    protaPensa "I've already been here."

# game/Code/Interacciones/miCasa/PASILLO2/puertaCerradaMay.rpy:44
translate english noEntrarEscenaMay_5a5ccbbc:

    # protaPensa "Mejor no.."
    protaPensa "I've already been here."
# TODO: Translation updated at 2021-10-13 15:38

# game/Code/Interacciones/miCasa/PASILLO2/puertaCerradaMay.rpy:8
translate english puertaCerradaMay_4405f1a9:

    # protaPensa "Tengo que encontrar la manera de poder entrar por la noche"
    protaPensa "I have to find a way to get in at night."
# TODO: Translation updated at 2021-12-12 13:06

# game/Code/Interacciones/miCasa/PASILLO2/puertaCerradaMay.rpy:11
translate english puertaCerradaMay_07ececce:

    # "Fin ruta de Kara Version 04"
    "End of Kara's route Version 04"
# TODO: Translation updated at 2022-03-27 17:09

# game/Code/Interacciones/miCasa/PASILLO2/puertaCerradaMay.rpy:14
translate english puertaCerradaMay_5a830124:

    # protaPensa "Mejor no volver a entrar."
    protaPensa "Better not to go back in."
