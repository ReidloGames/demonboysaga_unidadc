﻿# TODO: Translation updated at 2021-08-06 14:00

# game/Code/Interacciones/miCasa/GALERIAGYM/peq/lunes/peqHaceGym3raInteracLunes.rpy:14
translate english peqHaceGym3raInteracLunes_bc5f983d:

    # protaPensa "No tengo nada mas que decirle"
    protaPensa "I have nothing more to say to her"

# game/Code/Interacciones/miCasa/GALERIAGYM/peq/lunes/peqHaceGym3raInteracLunes.rpy:16
translate english peqHaceGym3raInteracLunes_0e9e3663:

    # protaPensa "Ya he hablado con ella"
    protaPensa "I've already talked to her."
# TODO: Translation updated at 2021-12-12 13:06

# game/Code/Interacciones/miCasa/GALERIAGYM/peq/lunes/peqHaceGym3raInteracLunes.rpy:35
translate english hablarConPeqEnGym_92d6bd86:

    # "ruta Dana fin version 0.4"
    "route Dana fin version 0.4"

# game/Code/Interacciones/miCasa/GALERIAGYM/peq/lunes/peqHaceGym3raInteracLunes.rpy:44
translate english hablarConPeqEnGym_03e69cd0:

    # hermanPeq "Me encantaría, pero aún no he acabado las tareas de clase."
    hermanPeq "I'd love to, but I haven't finished my class assignments yet."

# game/Code/Interacciones/miCasa/GALERIAGYM/peq/lunes/peqHaceGym3raInteracLunes.rpy:46
translate english hablarConPeqEnGym_c59f40de:

    # prota "Que pena, en otra ocasión entonces..."
    prota "Too bad, another time then..."

translate english strings:

    # game/Code/Interacciones/miCasa/GALERIAGYM/peq/lunes/peqHaceGym3raInteracLunes.rpy:32
    old "¿Te vienes a jugar a la consola?"
    new "Are you coming to play console games?"
# TODO: Translation updated at 2022-02-07 21:19

# game/Code/Interacciones/miCasa/GALERIAGYM/peq/lunes/peqHaceGym3raInteracLunes.rpy:36
translate english hablarConPeqEnGym_70472a24:

    # hermanPeq "Claro!"
    hermanPeq "Sure!"
