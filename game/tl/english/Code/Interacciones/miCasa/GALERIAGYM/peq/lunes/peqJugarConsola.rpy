﻿# TODO: Translation updated at 2022-02-07 21:19

# game/Code/Interacciones/miCasa/GALERIAGYM/peq/lunes/peqJugarConsola.rpy:16
translate english peqJugarConsola_50c30c33:

    # hermanPeq "¡Hahaha que malo eres!"
    hermanPeq "Hahaha! You suck!"#Hahaha how bad you are!

# game/Code/Interacciones/miCasa/GALERIAGYM/peq/lunes/peqJugarConsola.rpy:18
translate english peqJugarConsola_c4163e3a:

    # hermanPeq "Maldita sea..."
    hermanPeq "Dammit..."

# game/Code/Interacciones/miCasa/GALERIAGYM/peq/lunes/peqJugarConsola.rpy:35
translate english peqJugarConsola_53d8ea42:

    # prota "Admítelo soy mejor que tu"
    prota "Admit it, I'm better than you."#Admit it I'm better than you

# game/Code/Interacciones/miCasa/GALERIAGYM/peq/lunes/peqJugarConsola.rpy:38
translate english peqJugarConsola_f3ed8a7a:

    # hermanPeq "¡Calla!"
    hermanPeq "Shut up!"

# game/Code/Interacciones/miCasa/GALERIAGYM/peq/lunes/peqJugarConsola.rpy:46
translate english peqJugarConsola_d0f87c35:

    # prota "¡¡Oh!! quieres guerra!"
    prota "Oh! You want a fight!"#oh! you want war!

# game/Code/Interacciones/miCasa/GALERIAGYM/peq/lunes/peqJugarConsola.rpy:51
translate english peqJugarConsola_ed68a7e4:

    # hermanPeq "¿Tienes miedo?"
    hermanPeq "Are you scared?"#Are you afraid?

# game/Code/Interacciones/miCasa/GALERIAGYM/peq/lunes/peqJugarConsola.rpy:53
translate english peqJugarConsola_ace4cb38:

    # prota "¡Te vas a enterar!"
    prota "You're about to find out!"#You're going to find out!

# game/Code/Interacciones/miCasa/GALERIAGYM/peq/lunes/peqJugarConsola.rpy:58
translate english peqJugarConsola_7ec4a20f:

    # prota "¡No puedes conmigo!"
    prota "You can't beat me!"#You can't with me!

# game/Code/Interacciones/miCasa/GALERIAGYM/peq/lunes/peqJugarConsola.rpy:73
translate english peqJugarConsola_b1158627:

    # hermanPeq "¡Toma! ¡Te ganado!"
    hermanPeq "Take that! I'm going to beat you!"#Taking! I won you!

# game/Code/Interacciones/miCasa/GALERIAGYM/peq/lunes/peqJugarConsola.rpy:75
translate english peqJugarConsola_3c9ffa1e:

    # prota "Ni lo sueñes..."
    prota "Dream on..."#Do not even dream about it...

# game/Code/Interacciones/miCasa/GALERIAGYM/peq/lunes/peqJugarConsola.rpy:77
translate english peqJugarConsola_25f9630b:

    # hermanPeq "¡Te vas a enterar!"
    hermanPeq "I'll show you!"#You're going to find out!

# game/Code/Interacciones/miCasa/GALERIAGYM/peq/lunes/peqJugarConsola.rpy:87
translate english peqJugarConsola_e5aa10a2:

    # prota "¡Yo soy más fuerte!"
    prota "I am better!"#I am stronger!

# game/Code/Interacciones/miCasa/GALERIAGYM/peq/lunes/peqJugarConsola.rpy:93
translate english peqJugarConsola_85ceb40b:

    # hermanPeq "¡Vale, me rindo!"
    hermanPeq "OK, I give up!"#Ok, I give up!

# game/Code/Interacciones/miCasa/GALERIAGYM/peq/lunes/peqJugarConsola.rpy:95
translate english peqJugarConsola_f78feb37:

    # prota "¿Seguro?"
    prota "You sure?"#Sure?

# game/Code/Interacciones/miCasa/GALERIAGYM/peq/lunes/peqJugarConsola.rpy:101
translate english peqJugarConsola_1f78a1c4:

    # hermanPeq "¡Sal, que pesas mucho!"
    hermanPeq "Get off, you're too heavy!"#Come out, you weigh a lot!

# game/Code/Interacciones/miCasa/GALERIAGYM/peq/lunes/peqJugarConsola.rpy:103
translate english peqJugarConsola_62a77f92:

    # prota "¡Di que soy el mejor y el más fuerte!"
    prota "Say I'm the best and the strongest!"#Say that I am the best and the strongest!

# game/Code/Interacciones/miCasa/GALERIAGYM/peq/lunes/peqJugarConsola.rpy:105
translate english peqJugarConsola_830a22a4:

    # hermanPeq "No..."
    hermanPeq "No..."

# game/Code/Interacciones/miCasa/GALERIAGYM/peq/lunes/peqJugarConsola.rpy:109
translate english peqJugarConsola_7c2501ae:

    # prota "Entonces te ahogare con el cojín."
    prota "Then I'll smother you with this cushion."#Then I'll smother you with the cushion.

# game/Code/Interacciones/miCasa/GALERIAGYM/peq/lunes/peqJugarConsola.rpy:119
translate english peqJugarConsola_c7ea6274:

    # prota "¡Toma!"
    prota "Take this!"#

# game/Code/Interacciones/miCasa/GALERIAGYM/peq/lunes/peqJugarConsola.rpy:121
translate english peqJugarConsola_c368a9ca:

    # hermanPeq "Vale, vale."
    hermanPeq "OK OK."

# game/Code/Interacciones/miCasa/GALERIAGYM/peq/lunes/peqJugarConsola.rpy:122
translate english peqJugarConsola_18ade72c:

    # hermanPeq "Eres el mejor..."
    hermanPeq "You're the best..."

# game/Code/Interacciones/miCasa/GALERIAGYM/peq/lunes/peqJugarConsola.rpy:124
translate english peqJugarConsola_1c5a62ac:

    # prota "¿Y qué más?"
    prota "And what else?"

# game/Code/Interacciones/miCasa/GALERIAGYM/peq/lunes/peqJugarConsola.rpy:126
translate english peqJugarConsola_1c3cfa85:

    # hermanPeq "Y el más fuerte..."
    hermanPeq "And the strongest..."

# game/Code/Interacciones/miCasa/GALERIAGYM/peq/lunes/peqJugarConsola.rpy:128
translate english peqJugarConsola_f3d63b50:

    # prota "Hahahaha te ganado."
    prota "Hahahaha! I'm the winner.."#Hahahaha I won you.

# game/Code/Interacciones/miCasa/GALERIAGYM/peq/lunes/peqJugarConsola.rpy:131
translate english peqJugarConsola_619d634e:

    # hermanPeq "Me dejado ganar, para que luego no te sientas mal hahahahaha"
    hermanPeq "I just let you win so you don't cry about it later!"#He let me win, so you don't feel bad later hahahahaha

# game/Code/Interacciones/miCasa/GALERIAGYM/peq/lunes/peqJugarConsola.rpy:133
translate english peqJugarConsola_3c4a4d17:

    # prota "¿Entonces habrá revancha?"
    prota "Then there will be a rematch?"#So will there be revenge? Then there will be revenge?

# game/Code/Interacciones/miCasa/GALERIAGYM/peq/lunes/peqJugarConsola.rpy:136
translate english peqJugarConsola_5462449f:

    # "FIN DANA VERSION 0.5"
    "End version 0.4 Dana"

# game/Code/Interacciones/miCasa/GALERIAGYM/peq/lunes/peqJugarConsola.rpy:147
translate english peqJugarConsolaPerdido_50c30c33:

    # hermanPeq "¡Hahaha que malo eres!"
    hermanPeq "Hahaha! You suck!"#Hahaha how bad you are!

# game/Code/Interacciones/miCasa/GALERIAGYM/peq/lunes/peqJugarConsola.rpy:149
translate english peqJugarConsolaPerdido_c4163e3a:

    # hermanPeq "Maldita sea..."
    hermanPeq "Dammit..."

# game/Code/Interacciones/miCasa/GALERIAGYM/peq/lunes/peqJugarConsola.rpy:157
translate english peqJugarConsolaVictoria_2b7f7504:

    # prota "¡Soy el mejor!"
    prota "I'm the best!"

# game/Code/Interacciones/miCasa/GALERIAGYM/peq/lunes/peqJugarConsola.rpy:159
translate english peqJugarConsolaVictoria_8a6bba2e:

    # prota "¡Te dado una paliza!"
    prota "I beat your ass!"#I beat you up!

# game/Code/Interacciones/miCasa/GALERIAGYM/peq/lunes/peqJugarConsola.rpy:161
translate english peqJugarConsolaVictoria_8d396af6:

    # prota "Has tenido suerte..."
    prota "You just got lucky..."#You have had lucky...

# game/Code/Interacciones/miCasa/GALERIAGYM/peq/lunes/peqJugarConsola.rpy:165
translate english peqJugarConsolaVictoria_2b7f7504_1:

    # prota "¡Soy el mejor!"
    prota "I'm the best!"

# game/Code/Interacciones/miCasa/GALERIAGYM/peq/lunes/peqJugarConsola.rpy:167
translate english peqJugarConsolaVictoria_f6624943:

    # prota "¡Te vuelto a ganar y de paliza otra vez!"
    prota "I keep beating you again and again!"#I beat you again and beat you up again!
