﻿# TODO: Translation updated at 2021-08-06 14:00

# game/Code/Interacciones/miCasa/GALERIAGYM/peq/lunes/peqGaleriaHaciendoGym3raInteracLunes_visit1.rpy:11
translate english peqGaleriaHaciendoGym3raInteracLunes_visit1_f8fc2323:

    # protaPensa "{size=15}{i}Es una suerte que en esta casa todos hagan tanto ejercicio.{/size}{/i}"
    protaPensa "{size=15}{i}It is fortunate that everyone in this house gets so much exercise.{/size}{/i}"

# game/Code/Interacciones/miCasa/GALERIAGYM/peq/lunes/peqGaleriaHaciendoGym3raInteracLunes_visit1.rpy:14
translate english peqGaleriaHaciendoGym3raInteracLunes_visit1_966de2c8:

    # prota "¡¡Hola!!"
    prota "Hello!!!"

# game/Code/Interacciones/miCasa/GALERIAGYM/peq/lunes/peqGaleriaHaciendoGym3raInteracLunes_visit1.rpy:17
translate english peqGaleriaHaciendoGym3raInteracLunes_visit1_49e17d57:

    # hermanPeq "¡Hey!"
    hermanPeq "Hey!"

# game/Code/Interacciones/miCasa/GALERIAGYM/peq/lunes/peqGaleriaHaciendoGym3raInteracLunes_visit1.rpy:20
translate english peqGaleriaHaciendoGym3raInteracLunes_visit1_8cdab974:

    # prota "¿Trabajando el físico?"
    prota "Working on your body?"

# game/Code/Interacciones/miCasa/GALERIAGYM/peq/lunes/peqGaleriaHaciendoGym3raInteracLunes_visit1.rpy:27
translate english peqGaleriaHaciendoGym3raInteracLunes_visit1_9b7367ab:

    # hermanPeq "Si, no como tu..."
    hermanPeq "Yeah, not like you..."

# game/Code/Interacciones/miCasa/GALERIAGYM/peq/lunes/peqGaleriaHaciendoGym3raInteracLunes_visit1.rpy:38
translate english peqGaleriaHaciendoGym3raInteracLunes_visit1_49291fd4:

    # prota "Espera haces mal el ejercicio tienes que levantar más la pierna."
    prota "Wait you're doing the exercise wrong you have to lift your leg more."

# game/Code/Interacciones/miCasa/GALERIAGYM/peq/lunes/peqGaleriaHaciendoGym3raInteracLunes_visit1.rpy:41
translate english peqGaleriaHaciendoGym3raInteracLunes_visit1_2db04da1:

    # prota "Te ayudo."
    prota "Let me help you."

# game/Code/Interacciones/miCasa/GALERIAGYM/peq/lunes/peqGaleriaHaciendoGym3raInteracLunes_visit1.rpy:44
translate english peqGaleriaHaciendoGym3raInteracLunes_visit1_1423355c:

    # hermanPeq "No es necesario"
    hermanPeq "It's not necessary."

# game/Code/Interacciones/miCasa/GALERIAGYM/peq/lunes/peqGaleriaHaciendoGym3raInteracLunes_visit1.rpy:46
translate english peqGaleriaHaciendoGym3raInteracLunes_visit1_221741f5:

    # prota "Solo quiero ayudarte."
    prota "I just want to help you."

# game/Code/Interacciones/miCasa/GALERIAGYM/peq/lunes/peqGaleriaHaciendoGym3raInteracLunes_visit1.rpy:49
translate english peqGaleriaHaciendoGym3raInteracLunes_visit1_fa986744:

    # hermanPeq "Bueno, esta bien."
    hermanPeq "Well, that's okay."

# game/Code/Interacciones/miCasa/GALERIAGYM/peq/lunes/peqGaleriaHaciendoGym3raInteracLunes_visit1.rpy:86
translate english seguirPierna1_4bfac31e:

    # hermanPeq "¡¡Hay, que duele!!"
    hermanPeq "Ouch, that hurts!!!"

# game/Code/Interacciones/miCasa/GALERIAGYM/peq/lunes/peqGaleriaHaciendoGym3raInteracLunes_visit1.rpy:89
translate english seguirPierna1_5bac7e32:

    # prota "El dolor es parte del entrenamiento."
    prota "Pain is part of training."

# game/Code/Interacciones/miCasa/GALERIAGYM/peq/lunes/peqGaleriaHaciendoGym3raInteracLunes_visit1.rpy:106
translate english seguirPierna2_451d9abe:

    # hermanPeq "¡No tan fuerte bestia! ¡No puedo más!"
    hermanPeq "Not so hard beast! I can't take it anymore!"

# game/Code/Interacciones/miCasa/GALERIAGYM/peq/lunes/peqGaleriaHaciendoGym3raInteracLunes_visit1.rpy:109
translate english seguirPierna2_2776d9d9:

    # hermanPeq "¡Ay que me desmontas!"
    hermanPeq "Oh, you're tearing me apart!"

# game/Code/Interacciones/miCasa/GALERIAGYM/peq/lunes/peqGaleriaHaciendoGym3raInteracLunes_visit1.rpy:181
translate english seguirPierna2_713625aa:

    # hermanPeq "!!!Eres un bruto!!! no podía aguantar más."
    hermanPeq "You're a brute! I couldn't take it anymore."

# game/Code/Interacciones/miCasa/GALERIAGYM/peq/lunes/peqGaleriaHaciendoGym3raInteracLunes_visit1.rpy:186
translate english seguirPierna2_21559a94:

    # prota "Perdona, solo quería ayudar. ¿Estas bien?"
    prota "Sorry, I just wanted to help. are you ok?"

# game/Code/Interacciones/miCasa/GALERIAGYM/peq/lunes/peqGaleriaHaciendoGym3raInteracLunes_visit1.rpy:190
translate english seguirPierna2_0e9e3cba:

    # hermanPeqPensa "{size=15}{i}Eso es... ¿tan grande? {/size}{/i}"
    hermanPeqPensa "{size=15}{i}That's.... That big? {/size}{/i}"

# game/Code/Interacciones/miCasa/GALERIAGYM/peq/lunes/peqGaleriaHaciendoGym3raInteracLunes_visit1.rpy:193
translate english seguirPierna2_5879904f:

    # hermanPeqPensa "{size=15}{i}No puede ser. No es posible. {/size}{/i}"
    hermanPeqPensa "{size=15}{i}That can't be. It's not possible. {/size}{/i}"

# game/Code/Interacciones/miCasa/GALERIAGYM/peq/lunes/peqGaleriaHaciendoGym3raInteracLunes_visit1.rpy:197
translate english seguirPierna2_9bade934:

    # hermanPeq "jajaja"
    hermanPeq "hahaha"

# game/Code/Interacciones/miCasa/GALERIAGYM/peq/lunes/peqGaleriaHaciendoGym3raInteracLunes_visit1.rpy:199
translate english seguirPierna2_0a8c3360:

    # hermanPeq "Eres un patoso y un pervertido."
    hermanPeq "You're a dork and a pervert."

# game/Code/Interacciones/miCasa/GALERIAGYM/peq/lunes/peqGaleriaHaciendoGym3raInteracLunes_visit1.rpy:200
translate english seguirPierna2_3d921fd3:

    # hermanPeq "¿Que clase de ejercicio es este?"
    hermanPeq "What kind of exercise is this?"

# game/Code/Interacciones/miCasa/GALERIAGYM/peq/lunes/peqGaleriaHaciendoGym3raInteracLunes_visit1.rpy:202
translate english seguirPierna2_c7b67d90:

    # prota "¿La carretilla?"
    prota "The cartwheel?"

# game/Code/Interacciones/miCasa/GALERIAGYM/peq/lunes/peqGaleriaHaciendoGym3raInteracLunes_visit1.rpy:204
translate english seguirPierna2_61b5e2be:

    # hermanPeq "¿Pero has visto cómo estamos?"
    hermanPeq "But have you seen how we are?"

# game/Code/Interacciones/miCasa/GALERIAGYM/peq/lunes/peqGaleriaHaciendoGym3raInteracLunes_visit1.rpy:218
translate english seguirPierna2_e2047d10:

    # hermanPeqPensa "{size=15}{i}Oh Dios... no te muevas tanto. {/size}{/i}"
    hermanPeqPensa "{size=15}{i}Oh God... don't move so much. {/size}{/i}"

# game/Code/Interacciones/miCasa/GALERIAGYM/peq/lunes/peqGaleriaHaciendoGym3raInteracLunes_visit1.rpy:220
translate english seguirPierna2_c42f7251:

    # hermanPeq "Venga sal y deja de hacer el tonto que esta pose es un poco rara."
    hermanPeq "Come on and stop fooling around, this pose is a little weird."

# game/Code/Interacciones/miCasa/GALERIAGYM/peq/lunes/peqGaleriaHaciendoGym3raInteracLunes_visit1.rpy:222
translate english seguirPierna2_41edde38:

    # prota "¿Seguro? jajaja"
    prota "Are you sure? hahaha."

# game/Code/Interacciones/miCasa/GALERIAGYM/peq/lunes/peqGaleriaHaciendoGym3raInteracLunes_visit1.rpy:224
translate english seguirPierna2_34303a2a:

    # hermanPeq "¡¡¡Splash!!!"
    hermanPeq "Splash!!!"

# game/Code/Interacciones/miCasa/GALERIAGYM/peq/lunes/peqGaleriaHaciendoGym3raInteracLunes_visit1.rpy:225
translate english seguirPierna2_9786a260:

    # hermanPeq "Te la has ganado"
    hermanPeq "You've earned it."

# game/Code/Interacciones/miCasa/GALERIAGYM/peq/lunes/peqGaleriaHaciendoGym3raInteracLunes_visit1.rpy:229
translate english seguirPierna2_be840e01:

    # prota "Perdona jajaja igual sí que estoy bajo de forma y tenga que entrenar como tú."
    prota "Sorry hahaha maybe I really am out of shape and have to train like you."

# game/Code/Interacciones/miCasa/GALERIAGYM/peq/lunes/peqGaleriaHaciendoGym3raInteracLunes_visit1.rpy:231
translate english seguirPierna2_e3d11713:

    # prota "Te dejo entrenar."
    prota "I'll let you train."

# game/Code/Interacciones/miCasa/GALERIAGYM/peq/lunes/peqGaleriaHaciendoGym3raInteracLunes_visit1.rpy:233
translate english seguirPierna2_c3801d49:

    # hermanPeq "¡¡Adios tonto!!"
    hermanPeq "Goodbye fool!!!"

# game/Code/Interacciones/miCasa/GALERIAGYM/peq/lunes/peqGaleriaHaciendoGym3raInteracLunes_visit1.rpy:235
translate english seguirPierna2_dc581f59:

    # hermanPeqPensa "{size=15}{i}Que me ha pasado...{/size}{/i}"
    hermanPeqPensa "{size=15}{i}What happened to me...{/size}{/i}"

# game/Code/Interacciones/miCasa/GALERIAGYM/peq/lunes/peqGaleriaHaciendoGym3raInteracLunes_visit1.rpy:239
translate english seguirPierna2_f666017c:

    # prota "¿Que ha sido eso? Buff... mi amiguito de abajo quería saludar."
    prota "What was that? Buff...my little friend downstairs wanted to say hi."

# game/Code/Interacciones/miCasa/GALERIAGYM/peq/lunes/peqGaleriaHaciendoGym3raInteracLunes_visit1.rpy:259
translate english dejarPierna_5258d93b:

    # hermanPeqPensa "{size=15}{i}Eso es... ¿Tan grande?{/size}{/i}"
    hermanPeqPensa "{size=15}{i}That's.... That big? {/size}{/i}"

# game/Code/Interacciones/miCasa/GALERIAGYM/peq/lunes/peqGaleriaHaciendoGym3raInteracLunes_visit1.rpy:261
translate english dejarPierna_6e7773bf:

    # hermanPeqPensa "{size=15}{i}No puede ser. No es posible.{/size}{/i}"
    hermanPeqPensa "{size=15}{i}That can't be. It's not possible.{/size}{/i}"

# game/Code/Interacciones/miCasa/GALERIAGYM/peq/lunes/peqGaleriaHaciendoGym3raInteracLunes_visit1.rpy:267
translate english dejarPierna_598daa06:

    # prota "¿Todo bien?"
    prota "Everything ok?"

# game/Code/Interacciones/miCasa/GALERIAGYM/peq/lunes/peqGaleriaHaciendoGym3raInteracLunes_visit1.rpy:277
translate english dejarPierna_50874b1f:

    # hermanPeq "Si claro."
    hermanPeq "Yeah sure."

# game/Code/Interacciones/miCasa/GALERIAGYM/peq/lunes/peqGaleriaHaciendoGym3raInteracLunes_visit1.rpy:279
translate english dejarPierna_09f63909:

    # hermanPeq "Gracias"
    hermanPeq "Thank you."

# game/Code/Interacciones/miCasa/GALERIAGYM/peq/lunes/peqGaleriaHaciendoGym3raInteracLunes_visit1.rpy:281
translate english dejarPierna_c3801d49:

    # hermanPeq "¡¡Adios tonto!!"
    hermanPeq "Bye fool!!!"

translate english strings:

    # game/Code/Interacciones/miCasa/GALERIAGYM/peq/lunes/peqGaleriaHaciendoGym3raInteracLunes_visit1.rpy:61
    old "Subir mas"
    new "Raise more"

    # game/Code/Interacciones/miCasa/GALERIAGYM/peq/lunes/peqGaleriaHaciendoGym3raInteracLunes_visit1.rpy:71
    old "Seguir"
    new "Continue"

    # game/Code/Interacciones/miCasa/GALERIAGYM/peq/lunes/peqGaleriaHaciendoGym3raInteracLunes_visit1.rpy:71
    old "Dejar pierna"
    new "Leave leg"
