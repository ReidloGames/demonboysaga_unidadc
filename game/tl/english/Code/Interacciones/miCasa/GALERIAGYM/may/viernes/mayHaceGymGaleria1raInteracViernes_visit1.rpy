﻿# TODO: Translation updated at 2021-08-06 14:00

# game/Code/Interacciones/miCasa/GALERIAGYM/may/viernes/mayHaceGymGaleria1raInteracViernes_visit1.rpy:13
translate english mayHaceGymGaleria1raInteracViernes_visit1_2d06e6e8:

    # prota "¿Trabajando duro?"
    prota "Hard at work?"

# game/Code/Interacciones/miCasa/GALERIAGYM/may/viernes/mayHaceGymGaleria1raInteracViernes_visit1.rpy:17
translate english mayHaceGymGaleria1raInteracViernes_visit1_e7f1caa8:

    # hermanMay "Hay que mantenerse en forma"
    hermanMay "You have to keep in shape"

# game/Code/Interacciones/miCasa/GALERIAGYM/may/viernes/mayHaceGymGaleria1raInteracViernes_visit1.rpy:23
translate english mayHaceGymGaleria1raInteracViernes_visit1_2df39037:

    # prota "Pero si tú tienes un cuerpo de modelo."
    prota "But you have a model's body."

# game/Code/Interacciones/miCasa/GALERIAGYM/may/viernes/mayHaceGymGaleria1raInteracViernes_visit1.rpy:27
translate english mayHaceGymGaleria1raInteracViernes_visit1_44d95254:

    # hermanMay "jajajaja"
    hermanMay "hahahaha"

# game/Code/Interacciones/miCasa/GALERIAGYM/may/viernes/mayHaceGymGaleria1raInteracViernes_visit1.rpy:29
translate english mayHaceGymGaleria1raInteracViernes_visit1_daa97078:

    # hermanMay "Que exagerado. Tú que me ves con buenos ojos."
    hermanMay "How exaggerated. You see me in a good light."

# game/Code/Interacciones/miCasa/GALERIAGYM/may/viernes/mayHaceGymGaleria1raInteracViernes_visit1.rpy:31
translate english mayHaceGymGaleria1raInteracViernes_visit1_367d08aa:

    # hermanMay "Me tengo que cuidar ya que cuando compro nuevas prendas para la tienda, me las pruebo para ver cómo me quedan. Me hago una idea de si les gustara a los clientes. A parte que me gusta estar en buena forma."
    hermanMay "I have to take care of myself because when I buy new clothes for the store, I try them on to see how they fit. I get an idea of whether the customers will like it. I also like to be in good shape."

# game/Code/Interacciones/miCasa/GALERIAGYM/may/viernes/mayHaceGymGaleria1raInteracViernes_visit1.rpy:33
translate english mayHaceGymGaleria1raInteracViernes_visit1_548a5342:

    # hermanMay "No puedo permitirme tener un cuerpo en baja forma porque la ropa no me quedaría bien y en una tienda de moda vende la imagen."
    hermanMay "I can't afford to have an out of shape body because the clothes wouldn't fit me and in a fashion store it sells the image."

# game/Code/Interacciones/miCasa/GALERIAGYM/may/viernes/mayHaceGymGaleria1raInteracViernes_visit1.rpy:39
translate english mayHaceGymGaleria1raInteracViernes_visit1_ac44867d:

    # prota "En eso tienes toda la razón. Si necesitas que te dé un golpe de mano házmelo saber."
    prota "You're absolutely right about that. If you need me to give you a hand slap let me know."

# game/Code/Interacciones/miCasa/GALERIAGYM/may/viernes/mayHaceGymGaleria1raInteracViernes_visit1.rpy:41
translate english mayHaceGymGaleria1raInteracViernes_visit1_2069c138:

    # hermanMay "Muchas gracias"
    hermanMay "Thank you very much"
