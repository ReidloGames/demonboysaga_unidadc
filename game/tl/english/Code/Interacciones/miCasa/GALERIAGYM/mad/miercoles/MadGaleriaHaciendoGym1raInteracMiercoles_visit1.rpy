﻿# TODO: Translation updated at 2021-07-08 13:36

# game/Code/Interacciones/miCasa/GALERIAGYM/mad/miercoles/MadGaleriaHaciendoGym1raInteracMiercoles_visit1.rpy:5
translate english MadGaleriaHaciendoGym1raInteracMiercoles_visit1_c37359b7:

    # protaPensa "{size=15}{i}Oh diooos!!{/size}{/i}"
    protaPensa "{size=15}{i}Oh my gosh!!{/size}{/i}"

# game/Code/Interacciones/miCasa/GALERIAGYM/mad/miercoles/MadGaleriaHaciendoGym1raInteracMiercoles_visit1.rpy:6
translate english MadGaleriaHaciendoGym1raInteracMiercoles_visit1_df15e4dc:

    # protaPensa "{size=15}{i}Hay que reconocer que mi [situMad2] tiene un cuerpazo.{/size}{/i}"
    protaPensa "{size=15}{i}You have to hand it to my [situMad2], she has a great body.{/size}{/i}"

# game/Code/Interacciones/miCasa/GALERIAGYM/mad/miercoles/MadGaleriaHaciendoGym1raInteracMiercoles_visit1.rpy:7
translate english MadGaleriaHaciendoGym1raInteracMiercoles_visit1_cc40b88e:

    # protaPensa "{size=15}{i}Ese culo respingon y ese pecho firme...{/size}{/i}"
    protaPensa "{size=15}{i}That perky ass and firm chest...{/size}{/i}"

# game/Code/Interacciones/miCasa/GALERIAGYM/mad/miercoles/MadGaleriaHaciendoGym1raInteracMiercoles_visit1.rpy:9
translate english MadGaleriaHaciendoGym1raInteracMiercoles_visit1_1cec812a:

    # prota "Hola [situMad2]"
    prota "Hello [situMad2]."

# game/Code/Interacciones/miCasa/GALERIAGYM/mad/miercoles/MadGaleriaHaciendoGym1raInteracMiercoles_visit1.rpy:11
translate english MadGaleriaHaciendoGym1raInteracMiercoles_visit1_39556b34:

    # mad "Hola"
    mad "Hi"

# game/Code/Interacciones/miCasa/GALERIAGYM/mad/miercoles/MadGaleriaHaciendoGym1raInteracMiercoles_visit1.rpy:12
translate english MadGaleriaHaciendoGym1raInteracMiercoles_visit1_3ccafec1:

    # prota "A ti no te hace falta hacer ejercicio. Estas muy bien"
    prota "You don't need to exercise. You look great. "

# game/Code/Interacciones/miCasa/GALERIAGYM/mad/miercoles/MadGaleriaHaciendoGym1raInteracMiercoles_visit1.rpy:14
translate english MadGaleriaHaciendoGym1raInteracMiercoles_visit1_68d80d94:

    # mad "Gracias [nombreProta2], pero si no me cuidara un poco seguro que aumentaría de peso y me vería mal. Hay que tener un poco de fuerza de voluntad"
    mad "Thank you [nombreProta2], but if I didn't take care of myself a little bit I would surely gain weight and look bad. You have to have a little willpower "

# game/Code/Interacciones/miCasa/GALERIAGYM/mad/miercoles/MadGaleriaHaciendoGym1raInteracMiercoles_visit1.rpy:16
translate english MadGaleriaHaciendoGym1raInteracMiercoles_visit1_8faff9c7:

    # mad "Tu tambien deberías cuidarte ya que haces poco deporte y estas mucho con el ordenador"
    mad "You should also take care of yourself since you do little sport and spend a lot of time at the computer."

# game/Code/Interacciones/miCasa/GALERIAGYM/mad/miercoles/MadGaleriaHaciendoGym1raInteracMiercoles_visit1.rpy:24
translate english MadGaleriaHaciendoGym1raInteracMiercoles_visit1_666820b1:

    # prota "Bueno igual si debería hacer un poco de gimnasia."
    prota "Well, maybe I should do some gymnastics."

# game/Code/Interacciones/miCasa/GALERIAGYM/mad/miercoles/MadGaleriaHaciendoGym1raInteracMiercoles_visit1.rpy:31
translate english MadGaleriaHaciendoGym1raInteracMiercoles_visit1_2b22c89a:

    # prota "Pero lo que digo es verdad. Tienes un cuerpo espectacular. Seguro que cuando modelabas te salían muchos pretendientes y te los tenías que sacar de encima"
    prota "But what I say is true. You have a spectacular body. I'm sure when you were modeling you had a lot of suitors and you had to get them off your back"

# game/Code/Interacciones/miCasa/GALERIAGYM/mad/miercoles/MadGaleriaHaciendoGym1raInteracMiercoles_visit1.rpy:35
translate english MadGaleriaHaciendoGym1raInteracMiercoles_visit1_774ce081:

    # mad "Ehh.. no tampoco tanto.."
    mad "Ehh... not so much..."

# game/Code/Interacciones/miCasa/GALERIAGYM/mad/miercoles/MadGaleriaHaciendoGym1raInteracMiercoles_visit1.rpy:37
translate english MadGaleriaHaciendoGym1raInteracMiercoles_visit1_2b84bae1:

    # mad "Es muy amable por tu parte, pero no creo que esa sea una conversación muy apropiada para hablar contigo."
    mad "That's very nice of you, but I don't think that's a very appropriate conversation to be having with you."

# game/Code/Interacciones/miCasa/GALERIAGYM/mad/miercoles/MadGaleriaHaciendoGym1raInteracMiercoles_visit1.rpy:39
translate english MadGaleriaHaciendoGym1raInteracMiercoles_visit1_46346007:

    # mad "A parte de que esa época ya paso"
    mad "Other than that that time is long gone."

# game/Code/Interacciones/miCasa/GALERIAGYM/mad/miercoles/MadGaleriaHaciendoGym1raInteracMiercoles_visit1.rpy:43
translate english MadGaleriaHaciendoGym1raInteracMiercoles_visit1_8343c15f:

    # prota "Pero porque dejaste tu trabajo? nunca me lo dijiste. Y tampoco pasa nada por decir que mi [situMad] es muy atractiva"
    prota "But why did you quit your job? You never told me. And it's also okay to say that my [situMad] is very attractive"

# game/Code/Interacciones/miCasa/GALERIAGYM/mad/miercoles/MadGaleriaHaciendoGym1raInteracMiercoles_visit1.rpy:45
translate english MadGaleriaHaciendoGym1raInteracMiercoles_visit1_3bd97876:

    # mad "Esta bien, esta bien.Me gustaba mi trabajo pero la vida cambia y aparecen nuevas obligaciones como cuidar de la casa y todo lo que conlleva. Y ahora podemos cambiar de tema?"
    mad "Okay, okay. I liked my job but life changes and new obligations come along like taking care of the house and all that goes with it. And now can we change the subject?"

# game/Code/Interacciones/miCasa/GALERIAGYM/mad/miercoles/MadGaleriaHaciendoGym1raInteracMiercoles_visit1.rpy:47
translate english MadGaleriaHaciendoGym1raInteracMiercoles_visit1_48e6d50e:

    # madPensa "{size=15}{i}Hoy esta rarísimo...{/i}"
    madPensa "{size=15}{i}He is very weird today...{/i}"

# game/Code/Interacciones/miCasa/GALERIAGYM/mad/miercoles/MadGaleriaHaciendoGym1raInteracMiercoles_visit1.rpy:49
translate english MadGaleriaHaciendoGym1raInteracMiercoles_visit1_31a8a166:

    # protaPensa "{size=15}{i}Nunca le gusta hablar de su pasado{/size}{/i}"
    protaPensa "{size=15}{i}Never likes to talk about his past{/size}{/i}"

# game/Code/Interacciones/miCasa/GALERIAGYM/mad/miercoles/MadGaleriaHaciendoGym1raInteracMiercoles_visit1.rpy:51
translate english MadGaleriaHaciendoGym1raInteracMiercoles_visit1_e264bad4:

    # prota "Algun dia me enseñaras algun trabajo de cuando modelabas? Solo es curiosidad, para saber que hacías antes."
    prota "Someday will you show me some work from when you used to model? I'm just curious, to know what you used to do."

# game/Code/Interacciones/miCasa/GALERIAGYM/mad/miercoles/MadGaleriaHaciendoGym1raInteracMiercoles_visit1.rpy:55
translate english MadGaleriaHaciendoGym1raInteracMiercoles_visit1_8f117740:

    # mad "Si.. bueno, supongo que los guarde en algún lugar. Solo son fotografías."
    mad "Yeah... well, I guess I saved them somewhere. They're just photographs."

# game/Code/Interacciones/miCasa/GALERIAGYM/mad/miercoles/MadGaleriaHaciendoGym1raInteracMiercoles_visit1.rpy:57
translate english MadGaleriaHaciendoGym1raInteracMiercoles_visit1_c973a75e:

    # mad "Dejemos el tema, ahora quiero terminar la sesión de yoga"
    mad "Let's drop the subject, now I want to finish the yoga session."

# game/Code/Interacciones/miCasa/GALERIAGYM/mad/miercoles/MadGaleriaHaciendoGym1raInteracMiercoles_visit1.rpy:59
translate english MadGaleriaHaciendoGym1raInteracMiercoles_visit1_c41be119:

    # prota "Esta bien nos vemos [situMad2]"
    prota "Alright see you [situMad2]"

# game/Code/Interacciones/miCasa/GALERIAGYM/mad/miercoles/MadGaleriaHaciendoGym1raInteracMiercoles_visit1.rpy:62
translate english MadGaleriaHaciendoGym1raInteracMiercoles_visit1_d2c96299:

    # protaPensa "{size=15}{i}Es inutil, no suelta prenda...Pero supongo que al fin podre ver alguna imagen de ella ejerciendo de modelo{/size}{/i}"
    protaPensa "{size=15}{i}It's useless, she's not letting go...But I guess I'll finally get to see some images of her modeling...{/size}{/i}"
