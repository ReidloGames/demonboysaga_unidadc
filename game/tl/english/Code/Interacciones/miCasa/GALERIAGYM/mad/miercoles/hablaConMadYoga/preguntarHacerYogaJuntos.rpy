﻿# TODO: Translation updated at 2022-03-27 17:09

# game/Code/Interacciones/miCasa/GALERIAGYM/mad/miercoles/hablaConMadYoga/preguntarHacerYogaJuntos.rpy:5
translate english preguntarHacerYogaJuntos_3b490ac3:

    # prota "Estaba pensando lo que me dijiste el otro día."
    prota "I was thinking about what you told me the other day."

# game/Code/Interacciones/miCasa/GALERIAGYM/mad/miercoles/hablaConMadYoga/preguntarHacerYogaJuntos.rpy:7
translate english preguntarHacerYogaJuntos_8d398dc4:

    # mad "¿Qué te dije?"
    mad "What did I tell you?"

# game/Code/Interacciones/miCasa/GALERIAGYM/mad/miercoles/hablaConMadYoga/preguntarHacerYogaJuntos.rpy:10
translate english preguntarHacerYogaJuntos_3c93eeec:

    # prota "Que podría hacer un poco de ejercicio..."
    prota "That I could do a little exercise..."

# game/Code/Interacciones/miCasa/GALERIAGYM/mad/miercoles/hablaConMadYoga/preguntarHacerYogaJuntos.rpy:12
translate english preguntarHacerYogaJuntos_856528eb:

    # mad "Ahhh si, deberías empezar a cuidarte. Siempre estás en la consola o mirando la tele."
    mad "Ahhh yes, you should start taking care of yourself. You're always on the console or watching TV."

# game/Code/Interacciones/miCasa/GALERIAGYM/mad/miercoles/hablaConMadYoga/preguntarHacerYogaJuntos.rpy:14
translate english preguntarHacerYogaJuntos_625c8480:

    # prota "¿Y porque no hacemos un poco de yoga juntos? Leí que hay ejercicios de yoga en pareja y podría ser más divertido."
    prota "And why don't we do a little yoga together? I read that there are yoga exercises as a couple and it could be more fun."

# game/Code/Interacciones/miCasa/GALERIAGYM/mad/miercoles/hablaConMadYoga/preguntarHacerYogaJuntos.rpy:16
translate english preguntarHacerYogaJuntos_175354f2:

    # prota "Hacer ejercicio solo es muy aburrido..."
    prota "Exercising alone is very boring..."

# game/Code/Interacciones/miCasa/GALERIAGYM/mad/miercoles/hablaConMadYoga/preguntarHacerYogaJuntos.rpy:18
translate english preguntarHacerYogaJuntos_92752f78:

    # mad "Oh... Entiendo que se necesita constancia y puede ser aburrido, pero eso no es posible, ya que no conozco una rutina de ejercicios para dos."
    mad "Oh... I understand that it takes consistency and can be boring, but that's not possible, as I don't know of an exercise routine for two."

# game/Code/Interacciones/miCasa/GALERIAGYM/mad/miercoles/hablaConMadYoga/preguntarHacerYogaJuntos.rpy:20
translate english preguntarHacerYogaJuntos_8772f93e:

    # mad "Lo siento..."
    mad "I am sorry..."

# game/Code/Interacciones/miCasa/GALERIAGYM/mad/miercoles/hablaConMadYoga/preguntarHacerYogaJuntos.rpy:22
translate english preguntarHacerYogaJuntos_e24da53f:

    # prota "Vaya..."
    prota "Well..."
