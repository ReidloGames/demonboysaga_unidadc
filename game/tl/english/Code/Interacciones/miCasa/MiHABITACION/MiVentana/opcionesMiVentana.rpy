﻿# TODO: Translation updated at 2021-07-08 13:36

# game/Code/Interacciones/miCasa/MiHABITACION/MiVentana/opcionesMiVentana.rpy:9
translate english opcionesMiVentana_ed9f0c0f:

    # protaPensa "Tengo prismaticos"
    protaPensa "I've got binoculars."

# game/Code/Interacciones/miCasa/MiHABITACION/MiVentana/opcionesMiVentana.rpy:11
translate english opcionesMiVentana_63142363:

    # protaPensa "No tengo prismáticos"
    protaPensa "I don't have binoculars."

# game/Code/Interacciones/miCasa/MiHABITACION/MiVentana/opcionesMiVentana.rpy:12
translate english opcionesMiVentana_43d20592:

    # protaPensa "Si tuviera unos prismáticos podría mirar los edificios del horizonte"
    protaPensa "If had binoculars I could look at the buildings on the skyline."
# TODO: Translation updated at 2021-12-12 13:06

# game/Code/Interacciones/miCasa/MiHABITACION/MiVentana/opcionesMiVentana.rpy:9
translate english opcionesMiVentana_4e303173:

    # "Fin version 0.4 Lucia"
    "End version 0.4 Lucia"
# TODO: Translation updated at 2022-04-03 09:13

# game/Code/Interacciones/miCasa/MiHABITACION/MiVentana/opcionesMiVentana.rpy:52
translate english espiarLuciaPrismaticos_a15533fc:

    # protaPensa "No hay nadie."
    protaPensa "There is no one there."

# game/Code/Interacciones/miCasa/MiHABITACION/MiVentana/opcionesMiVentana.rpy:55
translate english espiarLuciaPrismaticos_a15533fc_1:

    # protaPensa "No hay nadie."
    protaPensa "There is no one there."

# game/Code/Interacciones/miCasa/MiHABITACION/MiVentana/opcionesMiVentana.rpy:66
translate english espiarLuciaPrismaticos_a15533fc_2:

    # protaPensa "No hay nadie."
    protaPensa "There is no one there."

# game/Code/Interacciones/miCasa/MiHABITACION/MiVentana/opcionesMiVentana.rpy:69
translate english espiarLuciaPrismaticos_a15533fc_3:

    # protaPensa "No hay nadie."
    protaPensa "There is no one there."

# game/Code/Interacciones/miCasa/MiHABITACION/MiVentana/opcionesMiVentana.rpy:81
translate english espiarLuciaPrismaticos_a15533fc_4:

    # protaPensa "No hay nadie."
    protaPensa "There is no one there."

# game/Code/Interacciones/miCasa/MiHABITACION/MiVentana/opcionesMiVentana.rpy:84
translate english espiarLuciaPrismaticos_a15533fc_5:

    # protaPensa "No hay nadie."
    protaPensa "There is no one there."

# game/Code/Interacciones/miCasa/MiHABITACION/MiVentana/opcionesMiVentana.rpy:91
translate english espiarLuciaPrismaticos_d9e63476:

    # protaPensa "Esta durmiendo."
    protaPensa "She is sleeping."

# game/Code/Interacciones/miCasa/MiHABITACION/MiVentana/opcionesMiVentana.rpy:95
translate english espiarLuciaPrismaticos_a15533fc_6:

    # protaPensa "No hay nadie."
    protaPensa "There is no one there."

# game/Code/Interacciones/miCasa/MiHABITACION/MiVentana/opcionesMiVentana.rpy:97
translate english espiarLuciaPrismaticos_68174d12:

    # protaPensa "Por ahora ya he tenido suficiente."
    protaPensa "That's enough for now."

translate english strings:

    # game/Code/Interacciones/miCasa/MiHABITACION/MiVentana/opcionesMiVentana.rpy:24
    old "Espiar Lucia"
    new "Spy on Lucia"
