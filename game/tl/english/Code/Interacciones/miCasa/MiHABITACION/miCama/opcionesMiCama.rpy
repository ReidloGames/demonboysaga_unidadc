﻿# TODO: Translation updated at 2021-07-20 13:59

translate english strings:

    # game/Code/Interacciones/miCasa/MiHABITACION/miCama/opcionesMiCama.rpy:6
    old "Dormir hasta el dia siguiente"
    new "Sleep until the next day"

    # game/Code/Interacciones/miCasa/MiHABITACION/miCama/opcionesMiCama.rpy:6
    old "Hacer una siesta"
    new "Take a nap"

    # game/Code/Interacciones/miCasa/MiHABITACION/miCama/opcionesMiCama.rpy:21
    old "Dormir hasta el dia siguiente (en calzoncillos)"
    new "Sleep until the next day (in underwear)."

    # game/Code/Interacciones/miCasa/MiHABITACION/miCama/opcionesMiCama.rpy:21
    old "Dormir hasta el dia siguiente (desnudo)"
    new "Sleeping until the next day (naked)"
# TODO: Translation updated at 2021-08-27 11:11

# game/Code/Interacciones/miCasa/MiHABITACION/miCama/opcionesMiCama.rpy:35
translate english opcionesMiCama_3e9895e9:

    # protaPensa "No puedo dormir, tengo que ir a la Universidad"
    protaPensa "I can't sleep, I have to go to the University."

# game/Code/Interacciones/miCasa/MiHABITACION/miCama/opcionesMiCama.rpy:42
translate english opcionesMiCama_89eb97a0:

    # protaPensa "No es momento de ir a la cama"
    protaPensa "It's not time to go to bed"
# TODO: Translation updated at 2022-02-16 11:36

# game/Code/Interacciones/miCasa/MiHABITACION/miCama/opcionesMiCama.rpy:44
translate english opcionesMiCama_bcd9b8e0:

    # protaPensa "Necesito preguntarle a [situMad] y [situPad] sobre los problemas de dinero."
    protaPensa "I need to ask [situMad] and [situPad] about the money issues."
# TODO: Translation updated at 2022-03-27 17:09

translate english strings:

    # game/Code/Interacciones/miCasa/MiHABITACION/miCama/opcionesMiCama.rpy:7
    old "Pensar en como hacer yoga con [nombreMad3] y dormir"
    new "Think about how to do yoga with [nombreMad3] and sleep"
