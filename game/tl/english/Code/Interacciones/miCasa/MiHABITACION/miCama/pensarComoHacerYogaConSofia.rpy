﻿# TODO: Translation updated at 2022-03-27 17:09

# game/Code/Interacciones/miCasa/MiHABITACION/miCama/pensarComoHacerYogaConSofia.rpy:10
translate english piensaComoHacerYogaConSofia_52ea8568:

    # protaPensa "¿Como podría forzar la situación y hacer yoga con [nombreMad3]?"
    protaPensa "How could I force the situation and do yoga with [nombreMad3]?"

# game/Code/Interacciones/miCasa/MiHABITACION/miCama/pensarComoHacerYogaConSofia.rpy:27
translate english piensaComoHacerYogaConSofia_74f6ef60:

    # protaPensa "¿Y si voy a la biblioteca? seguro que allí encontrare un montón de libros sobre el tema."
    protaPensa "What if I go to the library? I'm sure I'll find a lot of books on the subject there."

# game/Code/Interacciones/miCasa/MiHABITACION/miCama/pensarComoHacerYogaConSofia.rpy:29
translate english piensaComoHacerYogaConSofia_114c950c:

    # protaPensa "Y así, no se podra negar..."
    protaPensa "And then, she can't refuse it..."

# game/Code/Interacciones/miCasa/MiHABITACION/miCama/pensarComoHacerYogaConSofia.rpy:32
translate english piensaComoHacerYogaConSofia_f612502b:

    # narrador "Ahora tienes una nueva zona en el mapa.(Biblioteca)"
    narrador "Now you have a new area on the map. (Library)"

# game/Code/Interacciones/miCasa/MiHABITACION/miCama/pensarComoHacerYogaConSofia.rpy:35
translate english piensaComoHacerYogaConSofia_3fab492d:

    # "Dormir..."
    "Sleep..."
