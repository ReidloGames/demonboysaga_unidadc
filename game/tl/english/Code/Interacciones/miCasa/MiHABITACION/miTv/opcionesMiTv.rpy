﻿# TODO: Translation updated at 2021-07-20 13:59

# game/Code/Interacciones/miCasa/MiHABITACION/miTv/opcionesMiTv.rpy:34
translate english jugarConsola_9689db88:

    # protaPensa "Es muy tarde, no tengo ganas de jugar a la consola."
    protaPensa "It's very late, I don't feel like playing the console."

translate english strings:

    # game/Code/Interacciones/miCasa/MiHABITACION/miTv/opcionesMiTv.rpy:6
    old "Jugar a la consola "
    new "Playing the console"
# TODO: Translation updated at 2021-08-27 11:11

# game/Code/Interacciones/miCasa/MiHABITACION/miTv/opcionesMiTv.rpy:17
translate english opcionesMiTv_b6741849:

    # protaPensa "No puedo jugar a la consola, tengo que ir a la Universidad"
    protaPensa "I can't play video games, I have to go to the university."

# game/Code/Interacciones/miCasa/MiHABITACION/miTv/opcionesMiTv.rpy:24
translate english opcionesMiTv_c3fd84fe:

    # protaPensa "No es momento de jugar a la consola"
    protaPensa "This is not the time to play video games"
