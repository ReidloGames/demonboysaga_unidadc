﻿# TODO: Translation updated at 2021-10-13 15:38

# game/Code/Interacciones/miCasa/MiHABITACION/ordenador/cursos/cursoMassage/leccionMassage1.rpy:10
translate english leccionMassage1_4b58c917:

    # mandin "Hola, como ya sabéis soy Chei Mandin."
    mandin "Hello, as you know I am Chei Mandin."

# game/Code/Interacciones/miCasa/MiHABITACION/ordenador/cursos/cursoMassage/leccionMassage1.rpy:12
translate english leccionMassage1_28fd2f02:

    # mandin "Gracias por apuntaros a estas clases. Os aseguro que no os decepcionaran y que os daran una serie de habilidades únicas."
    mandin "Thank you for signing up for these classes. I assure you that you will not be disappointed and that they will give you a unique set of skills."

# game/Code/Interacciones/miCasa/MiHABITACION/ordenador/cursos/cursoMassage/leccionMassage1.rpy:14
translate english leccionMassage1_8b48cd38:

    # mandin "Soy experto en todas las clases y tipos de masajes que existen en el mundo."
    mandin "I am an expert in all kinds and types of massage that exist in the world."

# game/Code/Interacciones/miCasa/MiHABITACION/ordenador/cursos/cursoMassage/leccionMassage1.rpy:16
translate english leccionMassage1_d3a16d57:

    # mandin "Pero lo que más quiero es que triunféis en la vida..."
    mandin "But what I want most of all is for you to succeed in life..."

# game/Code/Interacciones/miCasa/MiHABITACION/ordenador/cursos/cursoMassage/leccionMassage1.rpy:18
translate english leccionMassage1_cb00472d:

    # mandin "Ven aquí Mai"
    mandin "Come here Mai"

# game/Code/Interacciones/miCasa/MiHABITACION/ordenador/cursos/cursoMassage/leccionMassage1.rpy:37
translate english leccionMassage1_5a8baf7e:

    # mandin "Ya me entendéis..."
    mandin "You know what I mean..."

# game/Code/Interacciones/miCasa/MiHABITACION/ordenador/cursos/cursoMassage/leccionMassage1.rpy:44
translate english leccionMassage1_f9a4ff66:

    # mandin "Gracias Mai ya te puedes ir, solo quería que te conocieran mis estudiantes."
    mandin "Thank you Mai you can go now, I just wanted my students to meet you."

# game/Code/Interacciones/miCasa/MiHABITACION/ordenador/cursos/cursoMassage/leccionMassage1.rpy:48
translate english leccionMassage1_f5dedac7:

    # mandin "Seguimos"
    mandin "We go on."

# game/Code/Interacciones/miCasa/MiHABITACION/ordenador/cursos/cursoMassage/leccionMassage1.rpy:50
translate english leccionMassage1_d998ed0f:

    # mandin "Conozco todas las técnicas ancestrales de mi familia que han pasado de generación en generación."
    mandin "I know all my family's ancestral techniques that have been passed down from generation to generation."

# game/Code/Interacciones/miCasa/MiHABITACION/ordenador/cursos/cursoMassage/leccionMassage1.rpy:52
translate english leccionMassage1_d169c45f:

    # mandin "Mi tatarabuelo era masajista, mi bisabuelo era masajista, mi abuelo era masajista y mi padre era masajista."
    mandin "My great-great-grandfather was a masseur, my great-grandfather was a masseur, my grandfather was a masseur and my father was a masseur."

# game/Code/Interacciones/miCasa/MiHABITACION/ordenador/cursos/cursoMassage/leccionMassage1.rpy:54
translate english leccionMassage1_8af24e41:

    # mandin "Durante todas estas generaciones hemos ido perfeccionando las técnicas."
    mandin "Over all these generations we have been perfecting the techniques."

# game/Code/Interacciones/miCasa/MiHABITACION/ordenador/cursos/cursoMassage/leccionMassage1.rpy:57
translate english leccionMassage1_8fe1b349:

    # mandin "Yo las llamo 'las técnicas Mandin'."
    mandin "I call them 'the Mandin techniques'."

# game/Code/Interacciones/miCasa/MiHABITACION/ordenador/cursos/cursoMassage/leccionMassage1.rpy:59
translate english leccionMassage1_9e61cf17:

    # mandin "Estas técnicas consisten en abrir los chacras del cuerpo femenino y hacer que la energía fluya."
    mandin "These techniques consist of opening the chakras of the female body and getting the energy flowing."

# game/Code/Interacciones/miCasa/MiHABITACION/ordenador/cursos/cursoMassage/leccionMassage1.rpy:62
translate english leccionMassage1_09b2601c:

    # mandin "Cuando consigáis un buen nivel, os aseguro que ninguna mujer podrá resistir la técnica MANDIN."
    mandin "When you get a good level, I assure you that no woman will be able to resist the MANDIN technique."

# game/Code/Interacciones/miCasa/MiHABITACION/ordenador/cursos/cursoMassage/leccionMassage1.rpy:65
translate english leccionMassage1_f763fe88:

    # mandin "Empezamos la primera clase."
    mandin "We are starting the first class."

# game/Code/Interacciones/miCasa/MiHABITACION/ordenador/cursos/cursoMassage/leccionMassage1.rpy:67
translate english leccionMassage1_d5eed8db:

    # mandin "Ven aquí Yun."
    mandin "Come here Yun."

# game/Code/Interacciones/miCasa/MiHABITACION/ordenador/cursos/cursoMassage/leccionMassage1.rpy:71
translate english leccionMassage1_f9f91382:

    # mandin "Túmbate en la camilla por favor."
    mandin "Lie down on the stretcher please."

# game/Code/Interacciones/miCasa/MiHABITACION/ordenador/cursos/cursoMassage/leccionMassage1.rpy:90
translate english leccionMassage1_08d9d345:

    # mandin "Oh espera Yun, mejor boca arriba."
    mandin "Oh wait Yun, better lie on your back."

# game/Code/Interacciones/miCasa/MiHABITACION/ordenador/cursos/cursoMassage/leccionMassage1.rpy:92
translate english leccionMassage1_9aaa24b8:

    # mandin "Espera que te ayudo."
    mandin "Wait I'll help you."

# game/Code/Interacciones/miCasa/MiHABITACION/ordenador/cursos/cursoMassage/leccionMassage1.rpy:108
translate english leccionMassage1_78837cf3:

    # mandin "¿Todo bien Yun?"
    mandin "Everything okay Yun?"

# game/Code/Interacciones/miCasa/MiHABITACION/ordenador/cursos/cursoMassage/leccionMassage1.rpy:110
translate english leccionMassage1_f125b4a2:

    # yun "Si, todo bien."
    yun "Yes, everything's fine."

# game/Code/Interacciones/miCasa/MiHABITACION/ordenador/cursos/cursoMassage/leccionMassage1.rpy:112
translate english leccionMassage1_c06e5fd9:

    # mandin "Perfecto Yun, continuamos."
    mandin "Perfect Yun, let's continue."

# game/Code/Interacciones/miCasa/MiHABITACION/ordenador/cursos/cursoMassage/leccionMassage1.rpy:122
translate english leccionMassage1_26af2ec7:

    # mandin "Primera clase, los pies."
    mandin "First class, the feet."

# game/Code/Interacciones/miCasa/MiHABITACION/ordenador/cursos/cursoMassage/leccionMassage1.rpy:130
translate english leccionMassage1_e2104066:

    # mandin "Hay que transmitir que tú tienes el control de la situación. No hay que dudar y siempre con confianza."
    mandin "You have to convey that you are in control of the situation. No hesitation and always with confidence."

# game/Code/Interacciones/miCasa/MiHABITACION/ordenador/cursos/cursoMassage/leccionMassage1.rpy:137
translate english leccionMassage1_b5a70082:

    # mandin "Los pies tienen muchas terminaciones, tenemos que presionar estos puntos."
    mandin "The feet have many endings, we have to press these points."

# game/Code/Interacciones/miCasa/MiHABITACION/ordenador/cursos/cursoMassage/leccionMassage1.rpy:147
translate english leccionMassage1_85bbeaf8:

    # mandin "Estos son las zonas y poco a poco, la tensión ira desapareciendo y se ira abriendo y relajando."
    mandin "These are the zones and little by little, the tension will disappear and it will open up and relax."

# game/Code/Interacciones/miCasa/MiHABITACION/ordenador/cursos/cursoMassage/leccionMassage1.rpy:153
translate english leccionMassage1_71ae611d:

    # mandin "Mirar lo relajada que esta, y como sus músculos no ofrecen resistencia ni tensión."
    mandin "See how relaxed she is, and how her muscles offer no resistance or tension."

# game/Code/Interacciones/miCasa/MiHABITACION/ordenador/cursos/cursoMassage/leccionMassage1.rpy:163
translate english leccionMassage1_b998ee40:

    # mandin "Esta es la técnica Mandin pero no es fácil alcanzarla."
    mandin "This is the Mandin technique but it is not easy to achieve."

# game/Code/Interacciones/miCasa/MiHABITACION/ordenador/cursos/cursoMassage/leccionMassage1.rpy:175
translate english leccionMassage1_8409f4ab:

    # mandin "Como has podido ver, Yun alcanzo un nivel de relajación absoluto."
    mandin "As you can see, Yun reached an absolute level of relaxation."

# game/Code/Interacciones/miCasa/MiHABITACION/ordenador/cursos/cursoMassage/leccionMassage1.rpy:177
translate english leccionMassage1_77cb6682:

    # mandin "Practica mucho y esfuérzate."
    mandin "Practice a lot and try hard."

# game/Code/Interacciones/miCasa/MiHABITACION/ordenador/cursos/cursoMassage/leccionMassage1.rpy:182
translate english leccionMassage1_e84870da:

    # protaPensa "Interesante, tendré que poner en práctica las técnicas Mandin."
    protaPensa "Interesting, I'll have to put the Mandin techniques into practice."

# game/Code/Interacciones/miCasa/MiHABITACION/ordenador/cursos/cursoMassage/leccionMassage1.rpy:184
translate english leccionMassage1_3947277e:

    # protaPensa "Podría usar a [nombreMad3] para empezar a practicar..."
    protaPensa "I could use [nombreMad3] to start practicing..."

# game/Code/Interacciones/miCasa/MiHABITACION/ordenador/cursos/cursoMassage/leccionMassage1.rpy:186
translate english leccionMassage1_76834744:

    # protaPensa "Me gusta la idea."
    protaPensa "I like the idea."
