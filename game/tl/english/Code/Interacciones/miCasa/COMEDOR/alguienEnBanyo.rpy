﻿# TODO: Translation updated at 2021-07-20 13:59

# game/Code/Interacciones/miCasa/COMEDOR/alguienEnBanyo.rpy:8
translate english hayAlguienEnBanyo_55f100a7:

    # protaPensa "Hay alguien dentro"
    protaPensa "There is someone inside"

# game/Code/Interacciones/miCasa/COMEDOR/alguienEnBanyo.rpy:11
translate english hayAlguienEnBanyo_55f100a7_1:

    # protaPensa "Hay alguien dentro"
    protaPensa "There is someone inside"

# game/Code/Interacciones/miCasa/COMEDOR/alguienEnBanyo.rpy:14
translate english hayAlguienEnBanyo_55f100a7_2:

    # protaPensa "Hay alguien dentro"
    protaPensa "There is someone inside"

# game/Code/Interacciones/miCasa/COMEDOR/alguienEnBanyo.rpy:17
translate english hayAlguienEnBanyo_55f100a7_3:

    # protaPensa "Hay alguien dentro"
    protaPensa "There is someone inside"

# game/Code/Interacciones/miCasa/COMEDOR/alguienEnBanyo.rpy:20
translate english hayAlguienEnBanyo_55f100a7_4:

    # protaPensa "Hay alguien dentro"
    protaPensa "There is someone inside"

# game/Code/Interacciones/miCasa/COMEDOR/alguienEnBanyo.rpy:23
translate english hayAlguienEnBanyo_c3b49883:

    # protaPensa "No puedo volver a entrar"
    protaPensa "I can't get back in."

translate english strings:

    # game/Code/Interacciones/miCasa/COMEDOR/alguienEnBanyo.rpy:33
    old "Entrar en baño"
    new "Enter the bathroom"
