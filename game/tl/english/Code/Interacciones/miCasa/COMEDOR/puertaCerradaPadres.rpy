﻿# TODO: Translation updated at 2021-07-08 13:36

# game/Code/Interacciones/miCasa/COMEDOR/puertaCerradaPadres.rpy:5
translate english puertaCerradaPadres_cc3df183:

    # protaPensa "La puerta de la habitación esta cerrada"
    protaPensa "The door to the room is closed"

# game/Code/Interacciones/miCasa/COMEDOR/puertaCerradaPadres.rpy:33
translate english noIrHabPadrOtraVez_8710c503:

    # protaPensa "Ya he estado aquí"
    protaPensa "Already been here"

# game/Code/Interacciones/miCasa/COMEDOR/puertaCerradaPadres.rpy:42
translate english noEntrarEscenaMad_5a5ccbbc:

    # protaPensa "Mejor no.."
    protaPensa "Better not..."

translate english strings:

    # game/Code/Interacciones/miCasa/COMEDOR/puertaCerradaPadres.rpy:19
    old "No entrar"
    new "Do not enter"

    # game/Code/Interacciones/miCasa/COMEDOR/puertaCerradaPadres.rpy:19
    old "Llamar a la puerta"
    new "Knock on the door"

    # game/Code/Interacciones/miCasa/COMEDOR/puertaCerradaPadres.rpy:19
    old "Entrar en la habitación"
    new "Enter the room"
# TODO: Translation updated at 2021-10-13 15:38

# game/Code/Interacciones/miCasa/COMEDOR/puertaCerradaPadres.rpy:12
translate english puertaCerradaPadres_4405f1a9:

    # protaPensa "Tengo que encontrar la manera de poder entrar por la noche"
    protaPensa "I have to find a way to get in at night."
