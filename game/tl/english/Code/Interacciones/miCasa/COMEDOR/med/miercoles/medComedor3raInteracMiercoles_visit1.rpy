﻿# TODO: Translation updated at 2021-08-06 14:00

# game/Code/Interacciones/miCasa/COMEDOR/med/miercoles/medComedor3raInteracMiercoles_visit1.rpy:11
translate english medComedor3raInteracMiercoles_visit1_79ec4f35:

    # prota "¿Estas estudiando?"
    prota "Are you studying?"

# game/Code/Interacciones/miCasa/COMEDOR/med/miercoles/medComedor3raInteracMiercoles_visit1.rpy:13
translate english medComedor3raInteracMiercoles_visit1_b237af0d:

    # hermanMed "¿A caso no es evidente?"
    hermanMed "Isn't it obvious?"

# game/Code/Interacciones/miCasa/COMEDOR/med/miercoles/medComedor3raInteracMiercoles_visit1.rpy:15
translate english medComedor3raInteracMiercoles_visit1_6b7795dc:

    # protaPensa "{size=15}{i}Está estudiando, pero tiene la seguridad que si suspende, papaíto le pagara el curso todas las veces que haga falta hasta que apruebe el master.{/size}{/i}"
    protaPensa "{size=15}{i}He is studying, but he is sure that if he fails, daddy will pay for the course as many times as it takes until he passes the master's degree.{/size}{/i}"

# game/Code/Interacciones/miCasa/COMEDOR/med/miercoles/medComedor3raInteracMiercoles_visit1.rpy:16
translate english medComedor3raInteracMiercoles_visit1_bb8de863:

    # protaPensa "{size=15}{i}Así cualquiera.{/size}{/i}"
    protaPensa "{size=15}{i}Just like that.{/size}{/i}"

# game/Code/Interacciones/miCasa/COMEDOR/med/miercoles/medComedor3raInteracMiercoles_visit1.rpy:18
translate english medComedor3raInteracMiercoles_visit1_c09378b1:

    # prota "Bueno, pues ánimos con los estudios."
    prota "Well, I wish you good luck with your studies."

# game/Code/Interacciones/miCasa/COMEDOR/med/miercoles/medComedor3raInteracMiercoles_visit1.rpy:20
translate english medComedor3raInteracMiercoles_visit1_364432fe:

    # hermanMed "No necesito tu ayuda ni tus ánimos..."
    hermanMed "I don't need your help or encouragement...."

# game/Code/Interacciones/miCasa/COMEDOR/med/miercoles/medComedor3raInteracMiercoles_visit1.rpy:22
translate english medComedor3raInteracMiercoles_visit1_b6d5a32d:

    # protaPensa "{size=15}{i}Que estúpida. Parece un ángel, pero es una bruja...{/size}{/i}"
    protaPensa "{size=15}{i}How stupid. She looks like an angel, but she's a witch...{/size}{/i}"

# game/Code/Interacciones/miCasa/COMEDOR/med/miercoles/medComedor3raInteracMiercoles_visit1.rpy:23
translate english medComedor3raInteracMiercoles_visit1_0019da52:

    # protaPensa "{size=15}{i}Aunque está muy buena, pero no se puede tener todo en la vida.{/size}{/i}"
    protaPensa "{size=15}{i}Although she's very hot, but you can't have everything in life.{/size}{/i}"
