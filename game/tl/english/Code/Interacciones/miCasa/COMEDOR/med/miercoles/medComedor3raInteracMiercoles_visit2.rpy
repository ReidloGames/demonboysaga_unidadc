﻿# TODO: Translation updated at 2021-10-13 15:38

# game/Code/Interacciones/miCasa/COMEDOR/med/miercoles/medComedor3raInteracMiercoles_visit2.rpy:9
translate english medComedor3raInteracMiercoles_visit2_08d9e997:

    # hermanMed "¿Otra vez tú por aquí?"
    hermanMed "You here again?"

# game/Code/Interacciones/miCasa/COMEDOR/med/miercoles/medComedor3raInteracMiercoles_visit2.rpy:11
translate english medComedor3raInteracMiercoles_visit2_54e7c0cf:

    # hermanMed "Que pesado eres."
    hermanMed "What a pain in the ass you are."

# game/Code/Interacciones/miCasa/COMEDOR/med/miercoles/medComedor3raInteracMiercoles_visit2.rpy:18
translate english medComedor3raInteracMiercoles_visit2_5fa1c60b:

    # prota "Solo quería saber si te encontrabas bien después de la noticia de [nombrePad3]."
    prota "I just wanted to know if you were feeling okay after [nombrePad3]'s news."

# game/Code/Interacciones/miCasa/COMEDOR/med/miercoles/medComedor3raInteracMiercoles_visit2.rpy:22
translate english medComedor3raInteracMiercoles_visit2_d1b6cd77:

    # hermanMed "Déjame en paz, no necesito a nadie. Ni a papa ni a ti."
    hermanMed "Leave me alone, I don't need anyone. Not dad or you."

# game/Code/Interacciones/miCasa/COMEDOR/med/miercoles/medComedor3raInteracMiercoles_visit2.rpy:24
translate english medComedor3raInteracMiercoles_visit2_143231a9:

    # protaPensa "Como de costumbre sigue con sus malas pulgas."
    protaPensa "As usual he continues with his bad temper."

# game/Code/Interacciones/miCasa/COMEDOR/med/miercoles/medComedor3raInteracMiercoles_visit2.rpy:26
translate english medComedor3raInteracMiercoles_visit2_ec18f3ca:

    # prota "No deberías tratarme así, para empezar solo me preocupo por ti, aparte de que podría ayudarte con tus estudios."
    prota "You shouldn't treat me like that, I only care about you to begin with, besides I could help you with your"

# game/Code/Interacciones/miCasa/COMEDOR/med/miercoles/medComedor3raInteracMiercoles_visit2.rpy:28
translate english medComedor3raInteracMiercoles_visit2_5eff459f:

    # hermanMed "No me hagas reír."
    hermanMed "Don't make me laugh."

# game/Code/Interacciones/miCasa/COMEDOR/med/miercoles/medComedor3raInteracMiercoles_visit2.rpy:30
translate english medComedor3raInteracMiercoles_visit2_474abfe7:

    # hermanMed "Tu ayudarme? ¿en qué?"
    hermanMed "You help me...with what?"

# game/Code/Interacciones/miCasa/COMEDOR/med/miercoles/medComedor3raInteracMiercoles_visit2.rpy:32
translate english medComedor3raInteracMiercoles_visit2_67ddba29:

    # prota "Yo no soy mal estudiante, igual necesitas otras formas de ver las cosas."
    prota "I'm not a bad student, maybe you need other ways of looking at things."

# game/Code/Interacciones/miCasa/COMEDOR/med/miercoles/medComedor3raInteracMiercoles_visit2.rpy:34
translate english medComedor3raInteracMiercoles_visit2_c8da06b1:

    # hermanMed "Vale, deja de contarme cuentos chinos."
    hermanMed "Okay, stop telling me tall tales."

# game/Code/Interacciones/miCasa/COMEDOR/med/miercoles/medComedor3raInteracMiercoles_visit2.rpy:36
translate english medComedor3raInteracMiercoles_visit2_9784e7a6:

    # hermanMed "Adiós."
    hermanMed "Goodbye."

# game/Code/Interacciones/miCasa/COMEDOR/med/miercoles/medComedor3raInteracMiercoles_visit2.rpy:41
translate english medComedor3raInteracMiercoles_visit2_fc113ec8:

    # prota "Nada ,dejalo..."
    prota "Nothing ,leave it..."

# game/Code/Interacciones/miCasa/COMEDOR/med/miercoles/medComedor3raInteracMiercoles_visit2.rpy:43
translate english medComedor3raInteracMiercoles_visit2_d2f35c11:

    # hermanMed "Siempre molestando..."
    hermanMed "Always bothering..."

translate english strings:

    # game/Code/Interacciones/miCasa/COMEDOR/med/miercoles/medComedor3raInteracMiercoles_visit2.rpy:14
    old "Preguntar por [nombrePad3]"
    new "Ask for [nombrePad3]."
