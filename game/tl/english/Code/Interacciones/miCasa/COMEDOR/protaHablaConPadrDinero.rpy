﻿# TODO: Translation updated at 2021-08-27 11:11

# game/Code/Interacciones/miCasa/COMEDOR/protaHablaConPadrDinero.rpy:24
translate english protaHablaConPadrDineroHabPadr_80dc3b21:

    # "Adelante, puedes pasar"
    "Go ahead, you can come in."

# game/Code/Interacciones/miCasa/COMEDOR/protaHablaConPadrDinero.rpy:30
translate english protaHablaConPadrDineroHabPadr_4747d6c3:

    # prota "Hola, siento molestar."
    prota "Hi, sorry to bother you."

# game/Code/Interacciones/miCasa/COMEDOR/protaHablaConPadrDinero.rpy:32
translate english protaHablaConPadrDineroHabPadr_4bb203e9:

    # mad "¿Que pasa [nombreProta2]? es raro verte aquí a estas horas."
    mad "What's up [nombreProta2]? It's weird to see you here at this hour."

# game/Code/Interacciones/miCasa/COMEDOR/protaHablaConPadrDinero.rpy:34
translate english protaHablaConPadrDineroHabPadr_18b9f76d:

    # prota "Bueno quería veros a los dos porque ha pasado algo extraño en la universidad."
    prota "Well I wanted to see you both because something strange happened at the university."

# game/Code/Interacciones/miCasa/COMEDOR/protaHablaConPadrDinero.rpy:36
translate english protaHablaConPadrDineroHabPadr_1cdb1c27:

    # prota "Igual no es nada, pero quería informaros por si pasaba algo extraño."
    prota "Maybe it's nothing, but I wanted to inform you in case something strange happened."

# game/Code/Interacciones/miCasa/COMEDOR/protaHablaConPadrDinero.rpy:38
translate english protaHablaConPadrDineroHabPadr_9c5831cc:

    # prota "Me dijo el director que no se ha realizado el pago de la cuota y no parecía muy contento."
    prota "I was told by the director that the fee payment has not been made and he didn't seem very happy."

# game/Code/Interacciones/miCasa/COMEDOR/protaHablaConPadrDinero.rpy:40
translate english protaHablaConPadrDineroHabPadr_c476d4eb:

    # mad "¿¿¿Como???"
    mad "What????"

# game/Code/Interacciones/miCasa/COMEDOR/protaHablaConPadrDinero.rpy:42
translate english protaHablaConPadrDineroHabPadr_5bb771da:

    # mad "¿John sabes algo de esto?"
    mad "John do you know anything about this?"

# game/Code/Interacciones/miCasa/COMEDOR/protaHablaConPadrDinero.rpy:44
translate english protaHablaConPadrDineroHabPadr_32bea5e4:

    # pad "Bueno... yo..."
    pad "Well... I..."

# game/Code/Interacciones/miCasa/COMEDOR/protaHablaConPadrDinero.rpy:48
translate english protaHablaConPadrDineroHabPadr_e9fa8444:

    # mad "¿Que ha pasado?"
    mad "What happened?"

# game/Code/Interacciones/miCasa/COMEDOR/protaHablaConPadrDinero.rpy:50
translate english protaHablaConPadrDineroHabPadr_8e62de83:

    # pad "No sabía cómo contarte esto, pero tuve un problema en el trabajo..."
    pad "I didn't know how to tell you this, but I had a problem at work..."

# game/Code/Interacciones/miCasa/COMEDOR/protaHablaConPadrDinero.rpy:52
translate english protaHablaConPadrDineroHabPadr_fb3e37e4:

    # mad "¿Que clase de problema?"
    mad "What kind of problem?"

# game/Code/Interacciones/miCasa/COMEDOR/protaHablaConPadrDinero.rpy:54
translate english protaHablaConPadrDineroHabPadr_a9c9c759:

    # pad "Perdóname por favor..."
    pad "Forgive me please..."

# game/Code/Interacciones/miCasa/COMEDOR/protaHablaConPadrDinero.rpy:56
translate english protaHablaConPadrDineroHabPadr_10a12a73:

    # mad "Me estas asustando..."
    mad "You're scaring me..."

# game/Code/Interacciones/miCasa/COMEDOR/protaHablaConPadrDinero.rpy:60
translate english protaHablaConPadrDineroHabPadr_454b9d23:

    # pad "No sé cómo decirte esto... pero el otro día tuve una cena de empresa y me emborraché demasiado."
    pad "I don't know how to tell you this... but I had a company dinner the other day and I got too drunk."

# game/Code/Interacciones/miCasa/COMEDOR/protaHablaConPadrDinero.rpy:62
translate english protaHablaConPadrDineroHabPadr_585d2986:

    # pad "La cuestión es que una cosa llevo a la otra y empezamos a jugar al póker."
    pad "The thing is, one thing led to another and we started playing poker."

# game/Code/Interacciones/miCasa/COMEDOR/protaHablaConPadrDinero.rpy:64
translate english protaHablaConPadrDineroHabPadr_ec2b8b91:

    # mad "Oh dios... que no sea lo que me estoy imaginando."
    mad "Oh god... that's not what I'm imagining."

# game/Code/Interacciones/miCasa/COMEDOR/protaHablaConPadrDinero.rpy:66
translate english protaHablaConPadrDineroHabPadr_77fc1d7b:

    # pad "Empezamos a jugar, yo borracho y..."
    pad "We started to play, I was drunk and..."

# game/Code/Interacciones/miCasa/COMEDOR/protaHablaConPadrDinero.rpy:68
translate english protaHablaConPadrDineroHabPadr_321936af:

    # mad "¡¡¡Y que!!! dilo ya!!"
    mad "And what!!! say it already!!!"

# game/Code/Interacciones/miCasa/COMEDOR/protaHablaConPadrDinero.rpy:70
translate english protaHablaConPadrDineroHabPadr_96c96e53:

    # pad "Me aposté la casa y perdí..."
    pad "I bet the house and lost..."

# game/Code/Interacciones/miCasa/COMEDOR/protaHablaConPadrDinero.rpy:72
translate english protaHablaConPadrDineroHabPadr_3d232087:

    # mad "¡¡¡NO!!!"
    mad "NO!!!"

# game/Code/Interacciones/miCasa/COMEDOR/protaHablaConPadrDinero.rpy:76
translate english protaHablaConPadrDineroHabPadr_9ece82e1:

    # mad "¡¡¡Como me has podido hacer esto!!!"
    mad "How could you do this to me!!!!"

# game/Code/Interacciones/miCasa/COMEDOR/protaHablaConPadrDinero.rpy:80
translate english protaHablaConPadrDineroHabPadr_f0befa24:

    # protaPensa "Esto pinta muy mal sabía que pasaba algo malo..."
    protaPensa "This looks really bad I knew something bad was going on..."

# game/Code/Interacciones/miCasa/COMEDOR/protaHablaConPadrDinero.rpy:86
translate english protaHablaConPadrDineroHabPadr_42344cc3:

    # protaPensa "Oh dios santo, con todo lo que está pasando y yo mirando el culo de mi [situMad2]. Demasiadas cosas por procesar."
    protaPensa "Oh my goodness, with everything going on and me looking at my [situMad2]'s ass. Too much stuff to process."

# game/Code/Interacciones/miCasa/COMEDOR/protaHablaConPadrDinero.rpy:90
translate english protaHablaConPadrDineroHabPadr_036427b9:

    # pad "No solo eso, el banco se enteró de mi estado y de que estaba jugando al póker y apostando..."
    pad "Not only that, the bank found out about my status and that I was playing poker and gambling..."

# game/Code/Interacciones/miCasa/COMEDOR/protaHablaConPadrDinero.rpy:92
translate english protaHablaConPadrDineroHabPadr_0c8e2ebe:

    # pad "Y me despidió, porque considero que una persona con mi cargo no puede ser confiable y no puede dirigir las gestiones del banco si hace esta clase de cosas..."
    pad "And fired me, because I consider that a person with my position can't be trusted and can't run the bank's business if he does this kind of thing..."

# game/Code/Interacciones/miCasa/COMEDOR/protaHablaConPadrDinero.rpy:93
translate english protaHablaConPadrDineroHabPadr_c35ee5a5:

    # pad "Estoy muy arrepentido, me deje llevar…"
    pad "I am very sorry, I got carried away..."

# game/Code/Interacciones/miCasa/COMEDOR/protaHablaConPadrDinero.rpy:95
translate english protaHablaConPadrDineroHabPadr_166e31a5:

    # mad "Me va a coger un ataque. ¡¡¡Eres un irresponsable!!!"
    mad "I'm going to have a stroke. You're irresponsible!!!"

# game/Code/Interacciones/miCasa/COMEDOR/protaHablaConPadrDinero.rpy:97
translate english protaHablaConPadrDineroHabPadr_60590ff0:

    # mad "Me has estado engañando, pensando que se podía confiar en ti. Me has hecho daño."
    mad "You've been deceiving me, thinking you could be trusted. You've been hurting me."

# game/Code/Interacciones/miCasa/COMEDOR/protaHablaConPadrDinero.rpy:99
translate english protaHablaConPadrDineroHabPadr_9fb940f3:

    # pad "Yo no quería... pero el alcohol. Me deje llevar."
    pad "I didn't mean to... but the alcohol. I got carried away."

# game/Code/Interacciones/miCasa/COMEDOR/protaHablaConPadrDinero.rpy:101
translate english protaHablaConPadrDineroHabPadr_c9941a40:

    # pad "Pero lo arreglare..."
    pad "But I'll fix it..."

# game/Code/Interacciones/miCasa/COMEDOR/protaHablaConPadrDinero.rpy:103
translate english protaHablaConPadrDineroHabPadr_8f939588:

    # pad "Hare que no nos quiten la casa..."
    pad "I'll make it so they don't take our house..."

# game/Code/Interacciones/miCasa/COMEDOR/protaHablaConPadrDinero.rpy:105
translate english protaHablaConPadrDineroHabPadr_8108891d:

    # pad "Mi primo George me ha ofrecido un empleo..."
    pad "My cousin George has offered me a job..."

# game/Code/Interacciones/miCasa/COMEDOR/protaHablaConPadrDinero.rpy:107
translate english protaHablaConPadrDineroHabPadr_f27f4782:

    # mad "¡¡¡Pero si tu primo es camionero!!! estás loco!!!"
    mad "But your cousin is a truck driver!!!! you're crazy!!!!"

# game/Code/Interacciones/miCasa/COMEDOR/protaHablaConPadrDinero.rpy:109
translate english protaHablaConPadrDineroHabPadr_b2a76b9c:

    # mad "Estoy con un demente, no me lo puedo creer."
    mad "I'm with a madman, I can't believe it."

# game/Code/Interacciones/miCasa/COMEDOR/protaHablaConPadrDinero.rpy:111
translate english protaHablaConPadrDineroHabPadr_141e4c3c:

    # mad "Me va a dar algo..."
    mad "I'm feeling sick..."

# game/Code/Interacciones/miCasa/COMEDOR/protaHablaConPadrDinero.rpy:113
translate english protaHablaConPadrDineroHabPadr_b5d75ad8:

    # pad "Lo siento..."
    pad "I'm sorry..."

# game/Code/Interacciones/miCasa/COMEDOR/protaHablaConPadrDinero.rpy:115
translate english protaHablaConPadrDineroHabPadr_4100d72c:

    # mad "¡¡¡No quiero verte!!!"
    mad "I don't want to see you!!!"

# game/Code/Interacciones/miCasa/COMEDOR/protaHablaConPadrDinero.rpy:117
translate english protaHablaConPadrDineroHabPadr_7942aa6b:

    # mad "Ahora tendré que volver a trabajar. Esto hará que cambie todas nuestras vidas."
    mad "Now I'll have to go back to work. This is going to change all our lives."

# game/Code/Interacciones/miCasa/COMEDOR/protaHablaConPadrDinero.rpy:119
translate english protaHablaConPadrDineroHabPadr_ab59a0a0:

    # mad "Oh dios que has hecho..."
    mad "Oh god what have you done..."

# game/Code/Interacciones/miCasa/COMEDOR/protaHablaConPadrDinero.rpy:121
translate english protaHablaConPadrDineroHabPadr_c75a10fd:

    # mad "Lo siento cielo, por haber tenido que vivir esta situación yo tampoco me lo esperaba, no te preocupes lo hablaremos mañana, lo arreglaremos..."
    mad "I'm sorry honey, for having to live this situation I didn't expect it either, don't worry we'll talk about it tomorrow, we'll fix it..."

# game/Code/Interacciones/miCasa/COMEDOR/protaHablaConPadrDinero.rpy:122
translate english protaHablaConPadrDineroHabPadr_b5d75ad8_1:

    # pad "Lo siento..."
    pad "I'm sorry..."

# game/Code/Interacciones/miCasa/COMEDOR/protaHablaConPadrDinero.rpy:124
translate english protaHablaConPadrDineroHabPadr_714832f4:

    # prota "Me voy, os dejo hablar de esto."
    prota "I'm leaving, I'll let you talk about it."

# game/Code/Interacciones/miCasa/COMEDOR/protaHablaConPadrDinero.rpy:126
translate english protaHablaConPadrDineroHabPadr_ba693ffd:

    # protaPensa "No me creo que este pasando esto… ¿Y ahora que pasara con todo? es muy fuerte..."
    protaPensa "I can't believe this is happening... And now what will happen to everything?"

translate english strings:

    # game/Code/Interacciones/miCasa/COMEDOR/protaHablaConPadrDinero.rpy:2
    old "Llamar a la puerta y preguntar por el tema del dinero de la Universidad"
    new "Knock on the door and ask about the University money issue."
