﻿# TODO: Translation updated at 2021-08-06 14:00

# game/Code/Interacciones/miCasa/COCINA/peq/miercoles/peqCocinaHaceComida3raInteracMiercoles_visit1.rpy:15
translate english peqCocinaHaceComida3raInteracMiercoles_visit1_0900e5a7:

    # prota "Hola, iba a prepararme la comida."
    prota "Hi, I was just going to prepare my lunch."

# game/Code/Interacciones/miCasa/COCINA/peq/miercoles/peqCocinaHaceComida3raInteracMiercoles_visit1.rpy:16
translate english peqCocinaHaceComida3raInteracMiercoles_visit1_5f7f5cc3:

    # prota "¿Comemos juntos?"
    prota "Shall we eat together?"

# game/Code/Interacciones/miCasa/COCINA/peq/miercoles/peqCocinaHaceComida3raInteracMiercoles_visit1.rpy:19
translate english peqCocinaHaceComida3raInteracMiercoles_visit1_77d2b846:

    # hermanPeq "¡Hola, Claro!"
    hermanPeq "Hi, sure!"

# game/Code/Interacciones/miCasa/COCINA/peq/miercoles/peqCocinaHaceComida3raInteracMiercoles_visit1.rpy:20
translate english peqCocinaHaceComida3raInteracMiercoles_visit1_65110b40:

    # hermanPeq "Ayúdame a preparar la comida."
    hermanPeq "Help me prepare the food."

# game/Code/Interacciones/miCasa/COCINA/peq/miercoles/peqCocinaHaceComida3raInteracMiercoles_visit1.rpy:24
translate english peqCocinaHaceComida3raInteracMiercoles_visit1_a131487d:

    # "Unos minutos mas tarde..."
    "A few minutes later..."

# game/Code/Interacciones/miCasa/COCINA/peq/miercoles/peqCocinaHaceComida3raInteracMiercoles_visit1.rpy:38
translate english preguntasPeqCocinaHaceComida3raMiercoles_a868a1b3:

    # hermanPeq "Muy agobiada con muchos trabajos de clase."
    hermanPeq "Very overwhelmed with a lot of classwork."

# game/Code/Interacciones/miCasa/COCINA/peq/miercoles/peqCocinaHaceComida3raInteracMiercoles_visit1.rpy:40
translate english preguntasPeqCocinaHaceComida3raMiercoles_8ad8972f:

    # hermanPeq "El profesor Eduard no para de ponernos tareas..."
    hermanPeq "Professor Eduard keeps giving us homework..."

# game/Code/Interacciones/miCasa/COCINA/peq/miercoles/peqCocinaHaceComida3raInteracMiercoles_visit1.rpy:42
translate english preguntasPeqCocinaHaceComida3raMiercoles_2c63743d:

    # prota "Sabes que si necesitas ayuda puedes contar conmigo."
    prota "You know if you need help you can count on me."

# game/Code/Interacciones/miCasa/COCINA/peq/miercoles/peqCocinaHaceComida3raInteracMiercoles_visit1.rpy:46
translate english preguntasPeqCocinaHaceComida3raMiercoles_c9db9718:

    # hermanPeq "Claro gracias :)"
    hermanPeq "Sure thanks :)"

# game/Code/Interacciones/miCasa/COCINA/peq/miercoles/peqCocinaHaceComida3raInteracMiercoles_visit1.rpy:52
translate english preguntasPeqCocinaHaceComida3raMiercoles_a868a1b3_1:

    # hermanPeq "Muy agobiada con muchos trabajos de clase."
    hermanPeq "Very overwhelmed with lots of classwork."

# game/Code/Interacciones/miCasa/COCINA/peq/miercoles/peqCocinaHaceComida3raInteracMiercoles_visit1.rpy:54
translate english preguntasPeqCocinaHaceComida3raMiercoles_41a2bbd2:

    # prota "Deberíamos hacer algo para que te diviertas y salgas de la rutina de la Universidad."
    prota "We should do something to get you to have fun and get out of the college rut."

translate english strings:

    # game/Code/Interacciones/miCasa/COCINA/peq/miercoles/peqCocinaHaceComida3raInteracMiercoles_visit1.rpy:35
    old "¿Como te va la Universidad?"
    new "How is college going?"

    # game/Code/Interacciones/miCasa/COCINA/peq/miercoles/peqCocinaHaceComida3raInteracMiercoles_visit1.rpy:35
    old "¿Y tu como estas?"
    new "How are you doing?"

    # game/Code/Interacciones/miCasa/COCINA/peq/miercoles/peqCocinaHaceComida3raInteracMiercoles_visit1.rpy:35
    old "Fin conversación"
    new "End conversation"
# TODO: Translation updated at 2021-12-12 13:06

# game/Code/Interacciones/miCasa/COCINA/peq/miercoles/peqCocinaHaceComida3raInteracMiercoles_visit1.rpy:68
translate english preguntasPeqCocinaHaceComida3raMiercoles_b98d0727:

    # prota "¿Y cómo estas? por todo lo que ha sucedido estos días en casa"
    prota "And how are you? because of everything that has happened these days at home?"

# game/Code/Interacciones/miCasa/COCINA/peq/miercoles/peqCocinaHaceComida3raInteracMiercoles_visit1.rpy:70
translate english preguntasPeqCocinaHaceComida3raMiercoles_a3da6e7b:

    # prota "Sabes que puedes contarme todo lo que necesites y que me preocupa que estes bien."
    prota "You know you can tell me anything you need to and that I care that you're okay."

# game/Code/Interacciones/miCasa/COCINA/peq/miercoles/peqCocinaHaceComida3raInteracMiercoles_visit1.rpy:72
translate english preguntasPeqCocinaHaceComida3raMiercoles_6e4d340c:

    # hermanPeq "Todo ha sido tan raro..."
    hermanPeq "It's all been so weird..."

# game/Code/Interacciones/miCasa/COCINA/peq/miercoles/peqCocinaHaceComida3raInteracMiercoles_visit1.rpy:74
translate english preguntasPeqCocinaHaceComida3raMiercoles_3d469a0a:

    # hermanPeq "Ha sido un shock, aún me cuesta creerme todo esto."
    hermanPeq "It's been a shock, I'm still having a hard time believing all this."

# game/Code/Interacciones/miCasa/COCINA/peq/miercoles/peqCocinaHaceComida3raInteracMiercoles_visit1.rpy:76
translate english preguntasPeqCocinaHaceComida3raMiercoles_f3528182:

    # hermanPeq "Cualquier cosa que necesites cuenta conmigo. Estoy para ayudarte."
    hermanPeq "Anything you need count on me. I'm here to help you."

# game/Code/Interacciones/miCasa/COCINA/peq/miercoles/peqCocinaHaceComida3raInteracMiercoles_visit1.rpy:80
translate english preguntasPeqCocinaHaceComida3raMiercoles_7f7a3853:

    # "Despiértala el domingo y animarla."
    "Wake her up on Sunday and cheer her up."

translate english strings:

    # game/Code/Interacciones/miCasa/COCINA/peq/miercoles/peqCocinaHaceComida3raInteracMiercoles_visit1.rpy:42
    old "Preguntar por como estan las cosas de casa"
    new "Ask how things are at home."
