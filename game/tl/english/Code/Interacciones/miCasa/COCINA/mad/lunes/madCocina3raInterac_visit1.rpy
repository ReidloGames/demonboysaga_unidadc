﻿# TODO: Translation updated at 2021-07-08 13:36

# game/Code/Interacciones/miCasa/COCINA/mad/lunes/madCocina3raInterac_visit1.rpy:3
translate english madCocina3raInteracLunes_visit1_2d490a9b:

    # prota "Hola [nombreMad3]"
    prota "Hello [nombreMad3]"

# game/Code/Interacciones/miCasa/COCINA/mad/lunes/madCocina3raInterac_visit1.rpy:4
translate english madCocina3raInteracLunes_visit1_96869b80:

    # prota "Que estas haciendo?"
    prota "What are you doing?"

# game/Code/Interacciones/miCasa/COCINA/mad/lunes/madCocina3raInterac_visit1.rpy:6
translate english madCocina3raInteracLunes_visit1_fc2d2393:

    # mad "Hola [nombreProta2]"
    mad "Hello [nombreProta2]"

# game/Code/Interacciones/miCasa/COCINA/mad/lunes/madCocina3raInterac_visit1.rpy:8
translate english madCocina3raInteracLunes_visit1_53b21339:

    # mad "estoy preparando la comida, es un plato que le gusta mucho a tu [situPad], risoto con setas"
    mad "I'm preparing lunch, it's a dish that your [situPad] likes a lot, risotto with mushrooms"

# game/Code/Interacciones/miCasa/COCINA/mad/lunes/madCocina3raInterac_visit1.rpy:10
translate english madCocina3raInteracLunes_visit1_cc0f5e61:

    # protaPensa "{i}{size=15}Se la ve contenta, le deben ir bien las cosas a mi [situPad]{/size}{/i}"
    protaPensa "{i}{size=15}She looks happy, things must be going well for my [situPad]{/size}{/i}"

# game/Code/Interacciones/miCasa/COCINA/mad/lunes/madCocina3raInterac_visit1.rpy:11
translate english madCocina3raInteracLunes_visit1_10c5e42a:

    # prota "Huele muy bien, se te ve feliz ¿hay algo que haya que celebrar?"
    prota "Smells great, you look happy is there something to celebrate?"

# game/Code/Interacciones/miCasa/COCINA/mad/lunes/madCocina3raInterac_visit1.rpy:13
translate english madCocina3raInteracLunes_visit1_60a44b12:

    # mad "Bueno me ha contado tu [situPad] que esta tarde iba a cerrar un contrato importante en el banco y se llevaría una comisión generosa."
    mad "Well, your [situPad] told me that this afternoon he was going to close an important contract at the bank and take a generous commission."

# game/Code/Interacciones/miCasa/COCINA/mad/lunes/madCocina3raInterac_visit1.rpy:15
translate english madCocina3raInteracLunes_visit1_4cf33e79:

    # prota "Eso es increíble"
    prota "That's incredible"

# game/Code/Interacciones/miCasa/COCINA/mad/lunes/madCocina3raInterac_visit1.rpy:17
translate english madCocina3raInteracLunes_visit1_79e2a6f0:

    # mad "Si, las cosas van muy bien en casa"
    mad "Yes, things are going very well at home."

# game/Code/Interacciones/miCasa/COCINA/mad/lunes/madCocina3raInterac_visit1.rpy:18
translate english madCocina3raInteracLunes_visit1_72dcdcde:

    # prota "Me alegro mucho, te dejo terminar el trabajo no sea que se te vaya a quemar"
    prota "I'm so glad, I'll let you finish the job lest you get burnt"

# game/Code/Interacciones/miCasa/COCINA/mad/lunes/madCocina3raInterac_visit1.rpy:20
translate english madCocina3raInteracLunes_visit1_092838b1:

    # mad "gracias [nombreProta2]"
    mad "thank you [nombreProta2]"

# TODO: Translation updated at 2021-07-19 09:51

# game/Code/Interacciones/miCasa/COCINA/mad/lunes/madCocina3raInterac_visit1.rpy:8
translate english madCocina3raInteracLunes_visit1_378b3b71:

    # mad "estoy preparando la comida, es un plato que le gusta mucho a tu [situPad], risotto con setas"
    mad "I'm preparing lunch, it's a dish that your [situPad] likes a lot, risotto with mushrooms"
