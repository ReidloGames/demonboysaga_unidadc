﻿# TODO: Translation updated at 2021-07-20 13:59

# game/Code/Interacciones/miCasa/COCINA/mad/martes/madCocinaLimpiaInterac_visit1.rpy:38
translate english madCocinaLimpiaInterac_visit1_e6fe33c2:

    # protaPensa "Dios mio... aquí no se puede vivir..."
    protaPensa "Oh my god... It is not possible to live here..."

# game/Code/Interacciones/miCasa/COCINA/mad/martes/madCocinaLimpiaInterac_visit1.rpy:39
translate english madCocinaLimpiaInterac_visit1_e16e9871:

    # protaPensa "Sera mejor que me vaya."
    protaPensa "I'd better go."
# TODO: Translation updated at 2022-02-16 11:36

# game/Code/Interacciones/miCasa/COCINA/mad/martes/madCocinaLimpiaInterac_visit1.rpy:40
translate english madCocinaLimpiaInterac_visit1_c55f2a9f:

    # protaPensa "Jugaré algunos videojuegos en mi habitación y hablaré con ambos esta noche."
    protaPensa "I'll play some video games in my room and talk to both of them tonight."
