﻿# TODO: Translation updated at 2021-07-08 13:36

# game/Code/Interacciones/miCasa/COCINA/mad/jueves/madCocinaDarMamar3raInteracJueves_visit1.rpy:4
translate english madCocinaDarMamar3raInteracJueves_visit1_ed483844:

    # protaPensa "{size=15}{i}Y ahí esta Max. Ahora mismo el niño mas afortunado de la Tierra{/size}{/i}"
    protaPensa "{size=15}{i}And there's Max. Right now the luckiest kid on Earth{/size}{/i}"

# game/Code/Interacciones/miCasa/COCINA/mad/jueves/madCocinaDarMamar3raInteracJueves_visit1.rpy:5
translate english madCocinaDarMamar3raInteracJueves_visit1_ac5943a9:

    # protaPensa "{size=15}{i}A mi tambien me han entrado ganas de leche.{/size}{/i}"
    protaPensa "{size=15}{i}I've been craving milk, too.{/size}{/i}"

# game/Code/Interacciones/miCasa/COCINA/mad/jueves/madCocinaDarMamar3raInteracJueves_visit1.rpy:22
translate english madCocinaDarMamar3raInteracJueves_visit1_9fb200be:

    # protaPensa "{size=15}{i}Igual podría hechar un vistazo...{/size}{/i}"
    protaPensa "{size=15}{i}Maybe I could take a look...{/size}{/i}"

# game/Code/Interacciones/miCasa/COCINA/mad/jueves/madCocinaDarMamar3raInteracJueves_visit1.rpy:43
translate english madCocinaDarMamar3raInteracJueves_visit1_8d73fe5a:

    # protaPensa "{size=15}{i}En mi situación, hasta al papa de Roma le saldrían cuernos{/size}{/i}"
    protaPensa "{size=15}{i}In my situation, even the pope of Rome would grow horns{/size}{/i}"

translate english strings:

    # game/Code/Interacciones/miCasa/COCINA/mad/jueves/madCocinaDarMamar3raInteracJueves_visit1.rpy:54
    old "Retroceder"
    new "Step backward"
# TODO: Translation updated at 2022-02-16 11:36

# game/Code/Interacciones/miCasa/COCINA/mad/jueves/madCocinaDarMamar3raInteracJueves_visit1.rpy:24
translate english madCocinaDarMamar3raInteracJueves_visit1_fd901522:

    # protaPensa "{size=15}{i}no, es demasiado arriesgado.{/size}{/i}"
    protaPensa "No, it's too risky."
