﻿# TODO: Translation updated at 2021-07-08 13:36

# game/Code/Interacciones/miCasa/COCINA/mad/domingo/madCocinaPideFregar1raInteracDomingo_visit1.rpy:6
translate english madCocinaPideFregar1raInteracDomingo_visit1_fc2d2393:

    # mad "Hola [nombreProta2]"
    mad "Hello [nombreProta2]"

# game/Code/Interacciones/miCasa/COCINA/mad/domingo/madCocinaPideFregar1raInteracDomingo_visit1.rpy:15
translate english madCocinaPideFregar1raInteracDomingo_visit1_c39897a8:

    # mad "Te importaría limpiar los platos que hay en el fregadero?"
    mad "Would you mind cleaning the dishes in the sink?"

# game/Code/Interacciones/miCasa/COCINA/mad/domingo/madCocinaPideFregar1raInteracDomingo_visit1.rpy:24
translate english madCocinaPideFregar1raInteracDomingo_visit1_2ae747c8:

    # prota "Oh, claro"
    prota "Oh, right"

# game/Code/Interacciones/miCasa/COCINA/mad/domingo/madCocinaPideFregar1raInteracDomingo_visit1.rpy:30
translate english madCocinaPideFregar1raInteracDomingo_visit1_123963b8:

    # prota "Lo que necesites [nombreMad3]"
    prota "Whatever you need [nombreMad3]."
