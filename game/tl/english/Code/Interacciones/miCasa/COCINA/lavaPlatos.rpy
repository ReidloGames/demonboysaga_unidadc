﻿# TODO: Translation updated at 2021-07-08 13:36

# game/Code/Interacciones/miCasa/COCINA/lavaPlatos.rpy:5
translate english lavaPlatos_edb23b2a:

    # protaPensa "Los platos ya estan fregados por hoy"
    protaPensa "He dishes are already done"

# TODO: Translation updated at 2021-07-19 09:51

# game/Code/Interacciones/miCasa/COCINA/lavaPlatos.rpy:8
translate english lavaPlatos_ab66becc:

    # protaPensa "Los lavaré en otro momento"
    protaPensa "I will wash them another time"
# TODO: Translation updated at 2021-12-12 13:06

# game/Code/Interacciones/miCasa/COCINA/lavaPlatos.rpy:32
translate english lavaPlatos_1efc7c61:

    # protaPensa "Esto ya está limpio."
    protaPensa "This is already clean."

# game/Code/Interacciones/miCasa/COCINA/lavaPlatos.rpy:34
translate english lavaPlatos_3d72a452:

    # mad "Ohhhh muchas gracias [nombreProta2], por ayudarme en las tareas de la casa."
    mad "Ohhhh thank you so much [nombreProta2], for helping me with the housework."

# game/Code/Interacciones/miCasa/COCINA/lavaPlatos.rpy:38
translate english lavaPlatos_63f0829e:

    # prota "De nada [nombreMad3], para lo que necesites estoy aquí."
    prota "You're welcome [nombreMad3], whatever you need I'm here."
