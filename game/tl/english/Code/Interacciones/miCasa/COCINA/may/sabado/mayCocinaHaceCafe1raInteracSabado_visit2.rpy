﻿# TODO: Translation updated at 2021-12-12 13:06

# game/Code/Interacciones/miCasa/COCINA/may/sabado/mayCocinaHaceCafe1raInteracSabado_visit2.rpy:12
translate english mayCocinaHaceCafe1raInteracSabado_visit2_b087884e:

    # hermanMay "Estoy super estresada. Me tomo el café rápido y me voy a la tienda."
    hermanMay "I'm super stressed. I drink my coffee quickly and go to the store."

# game/Code/Interacciones/miCasa/COCINA/may/sabado/mayCocinaHaceCafe1raInteracSabado_visit2.rpy:14
translate english mayCocinaHaceCafe1raInteracSabado_visit2_fe54d486:

    # hermanMay "En los próximos días ha de venir el camión con el nuevo género que compre y aún no sé cómo hare para pagarlo."
    hermanMay "In the next few days the truck is due to come with the new stuff I bought and I still don't know how I'm going to pay for it."

# game/Code/Interacciones/miCasa/COCINA/may/sabado/mayCocinaHaceCafe1raInteracSabado_visit2.rpy:16
translate english mayCocinaHaceCafe1raInteracSabado_visit2_653152cf:

    # hermanMay "Seguro que encuentras la manera. Esto solo es un bache. Ya verás que todo ira a mejor."
    hermanMay "I'm sure you'll find a way. This is just a bump in the road. You'll see that everything will get better."

# game/Code/Interacciones/miCasa/COCINA/may/sabado/mayCocinaHaceCafe1raInteracSabado_visit2.rpy:18
translate english mayCocinaHaceCafe1raInteracSabado_visit2_a64ad92a:

    # hermanMay "Eso espero..."
    hermanMay "I hope so..."
