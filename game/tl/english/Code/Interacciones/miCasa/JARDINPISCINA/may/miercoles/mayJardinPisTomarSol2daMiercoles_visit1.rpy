﻿# TODO: Translation updated at 2021-08-06 14:00

# game/Code/Interacciones/miCasa/JARDINPISCINA/may/miercoles/mayJardinPisTomarSol2daMiercoles_visit1.rpy:13
translate english mayJardinPisTomarSol2daMiercoles_visit1_11da6573:

    # protaPensa "{size=15}{i} Parece que no me ha visto. Esta relajada.{/size}{/i}"
    protaPensa "{size=15}{i} She doesn't seem to have seen me. She's relaxed.{/size}{/i}"

# game/Code/Interacciones/miCasa/JARDINPISCINA/may/miercoles/mayJardinPisTomarSol2daMiercoles_visit1.rpy:23
translate english mayJardinPisTomarSol2daMiercoles_visit1_5cd9b47b:

    # protaPensa "{size=15}{i} Tiene un cuerpo increíble...{/size}{/i}"
    protaPensa "{size=15}{i} She has an amazing body...{/size}{/i}"

# game/Code/Interacciones/miCasa/JARDINPISCINA/may/miercoles/mayJardinPisTomarSol2daMiercoles_visit1.rpy:27
translate english mayJardinPisTomarSol2daMiercoles_visit1_d2729f3c:

    # protaPensa "{size=15}{i} Me estoy poniendo enfermo. Dios mío.{/size}{/i}"
    protaPensa "{size=15}{i} I'm getting sick. Oh, my God.{/size}{/i}"

# game/Code/Interacciones/miCasa/JARDINPISCINA/may/miercoles/mayJardinPisTomarSol2daMiercoles_visit1.rpy:31
translate english mayJardinPisTomarSol2daMiercoles_visit1_2fbf087d:

    # protaPensa "{size=15}{i} Lo que daría por ver lo que hay allí debajo.{/size}{/i}"
    protaPensa "{size=15}{i} What I wouldn't give to see what's under there.{/size}{/i}"

# game/Code/Interacciones/miCasa/JARDINPISCINA/may/miercoles/mayJardinPisTomarSol2daMiercoles_visit1.rpy:39
translate english mayJardinPisTomarSol2daMiercoles_visit1_3ad79bbd:

    # "¡¡¡CRAAAACK!!!"
    "CRAAAACK!!!"

# game/Code/Interacciones/miCasa/JARDINPISCINA/may/miercoles/mayJardinPisTomarSol2daMiercoles_visit1.rpy:51
translate english mayJardinPisTomarSol2daMiercoles_visit1_c98ce140:

    # hermanMay "Me has asustado. No sabía que estabas aquí."
    hermanMay "You scared me. I didn't know you were here."

# game/Code/Interacciones/miCasa/JARDINPISCINA/may/miercoles/mayJardinPisTomarSol2daMiercoles_visit1.rpy:55
translate english mayJardinPisTomarSol2daMiercoles_visit1_9533a290:

    # prota "Perdóname, no sabía si dormías y no quería molestarte."
    prota "Forgive me, I didn't know if you were sleeping and I didn't want to disturb you."

# game/Code/Interacciones/miCasa/JARDINPISCINA/may/miercoles/mayJardinPisTomarSol2daMiercoles_visit1.rpy:61
translate english mayJardinPisTomarSol2daMiercoles_visit1_0c6e91f1:

    # hermanMay "Solo quería relajarme y tomar un poco el sol."
    hermanMay "I just wanted to relax and sunbathe a little."

# game/Code/Interacciones/miCasa/JARDINPISCINA/may/miercoles/mayJardinPisTomarSol2daMiercoles_visit1.rpy:72
translate english mayJardinPisTomarSol2daMiercoles_visit1_92de2728:

    # prota "¿Te has puesto protector solar? No me gustaría ver a mi hermana transformada en una gamba"
    prota "Did you put on sunscreen? I'd hate to see my sister turned into a shrimp."

# game/Code/Interacciones/miCasa/JARDINPISCINA/may/miercoles/mayJardinPisTomarSol2daMiercoles_visit1.rpy:76
translate english mayJardinPisTomarSol2daMiercoles_visit1_5ca8ff34:

    # hermanMay "jajaja"
    hermanMay "hahaha"

# game/Code/Interacciones/miCasa/JARDINPISCINA/may/miercoles/mayJardinPisTomarSol2daMiercoles_visit1.rpy:78
translate english mayJardinPisTomarSol2daMiercoles_visit1_3c7f39c2:

    # hermanMay "Si, ya casi no quedaba, pero me pude poner suficiente protector solar."
    hermanMay "Yeah, there was hardly any left, but I was able to put on enough sunscreen."

# game/Code/Interacciones/miCasa/JARDINPISCINA/may/miercoles/mayJardinPisTomarSol2daMiercoles_visit1.rpy:80
translate english mayJardinPisTomarSol2daMiercoles_visit1_268230eb:

    # protaPensa "He llegado tarde. ¡Maldición!"
    protaPensa "I was late. damn!"

# game/Code/Interacciones/miCasa/JARDINPISCINA/may/miercoles/mayJardinPisTomarSol2daMiercoles_visit1.rpy:82
translate english mayJardinPisTomarSol2daMiercoles_visit1_a638cd45:

    # prota "Bien te dejo que sigas relajándote."
    prota "Well I'll let you continue to relax."

# game/Code/Interacciones/miCasa/JARDINPISCINA/may/miercoles/mayJardinPisTomarSol2daMiercoles_visit1.rpy:86
translate english mayJardinPisTomarSol2daMiercoles_visit1_0a39f871:

    # hermanMay "Gracias"
    hermanMay "Thanks."
