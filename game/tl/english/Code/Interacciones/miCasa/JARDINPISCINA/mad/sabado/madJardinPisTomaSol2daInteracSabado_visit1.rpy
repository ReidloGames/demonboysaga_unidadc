﻿# TODO: Translation updated at 2021-07-08 13:36

# game/Code/Interacciones/miCasa/JARDINPISCINA/mad/sabado/madJardinPisTomaSol2daInteracSabado_visit1.rpy:18
translate english madJardinPisTomaSol2daInteracSabado_visit1_ea55892b:

    # mad "Ey tambien viniste a tomar el sol?"
    mad "Hey did you come to sunbathe too?"

# game/Code/Interacciones/miCasa/JARDINPISCINA/mad/sabado/madJardinPisTomaSol2daInteracSabado_visit1.rpy:25
translate english madJardinPisTomaSol2daInteracSabado_visit1_d624b461:

    # prota "No [nombreMad3], vine para preguntar si necesitabas que te ayudara en algo."
    prota "No [nombreMad3], I came to ask if you needed me to help you with anything."

# game/Code/Interacciones/miCasa/JARDINPISCINA/mad/sabado/madJardinPisTomaSol2daInteracSabado_visit1.rpy:30
translate english madJardinPisTomaSol2daInteracSabado_visit1_ef66f6a6:

    # mad "Ohhh, hoy estas muy servicial te preocupas por tu [situMad]. Gracias cariño"
    mad "Ohhh, you are very helpful today you care about your [situMad]. Thank you sweetie."

# game/Code/Interacciones/miCasa/JARDINPISCINA/mad/sabado/madJardinPisTomaSol2daInteracSabado_visit1.rpy:42
translate english madJardinPisTomaSol2daInteracSabado_visit1_9baafb8d:

    # mad "No te preocupes, hoy tu [situPad] traera la comida, así que me puedo relajar un poco"
    mad "Don't worry, today your [situPad] will bring the food, so I can relax a bit."

# game/Code/Interacciones/miCasa/JARDINPISCINA/mad/sabado/madJardinPisTomaSol2daInteracSabado_visit1.rpy:44
translate english madJardinPisTomaSol2daInteracSabado_visit1_a0900d3c:

    # prota "Genial entonces"
    prota "Great then"

# game/Code/Interacciones/miCasa/JARDINPISCINA/mad/sabado/madJardinPisTomaSol2daInteracSabado_visit1.rpy:51
translate english madJardinPisTomaSol2daInteracSabado_visit1_3465e696:

    # prota "El sol esta muy fuerte, te has puesto protector solar?"
    prota "The sun is really strong, have you put on sunscreen?"

# game/Code/Interacciones/miCasa/JARDINPISCINA/mad/sabado/madJardinPisTomaSol2daInteracSabado_visit1.rpy:55
translate english madJardinPisTomaSol2daInteracSabado_visit1_fcbaeca5:

    # mad "Oh, si me puse gracias cielo"
    mad "Oh, yes I did thank you sweetie"
# TODO: Translation updated at 2021-08-29 19:08

# game/Code/Interacciones/miCasa/JARDINPISCINA/mad/sabado/madJardinPisTomaSol2daInteracSabado_visit1.rpy:42
translate english madJardinPisTomaSol2daInteracSabado_visit1_74a18e56:

    # mad "No te preocupes, solo voy a relajarme un poco"
    mad "Don't worry, I'm just going to relax a bit."
