﻿# TODO: Translation updated at 2021-08-06 14:00

# game/Code/Interacciones/miCasa/HabHERMANMAY/may/martes/mayHabMayEnPc4taInteracMartes_visit1.rpy:13
translate english mayHabMayEnPc4taInteracMartes_visit1_5ce7d57c:

    # prota "Hola, ¿estas trabajado?"
    prota "Hi, are you working?"

# game/Code/Interacciones/miCasa/HabHERMANMAY/may/martes/mayHabMayEnPc4taInteracMartes_visit1.rpy:17
translate english mayHabMayEnPc4taInteracMartes_visit1_ecc56331:

    # hermanMay "Si, estoy seleccionando los pedidos para la tienda."
    hermanMay "Yes, I'm picking orders for the store."

# game/Code/Interacciones/miCasa/HabHERMANMAY/may/martes/mayHabMayEnPc4taInteracMartes_visit1.rpy:21
translate english mayHabMayEnPc4taInteracMartes_visit1_8151c907:

    # prota "Cuando los tengas ya me los enseñaras."
    prota "When you get them, you'll show them to me."

# game/Code/Interacciones/miCasa/HabHERMANMAY/may/martes/mayHabMayEnPc4taInteracMartes_visit1.rpy:27
translate english mayHabMayEnPc4taInteracMartes_visit1_906b9a1b:

    # hermanMay "¿Desde cuando tienes interés por la moda?"
    hermanMay "Since when have you had an interest in fashion?"

# game/Code/Interacciones/miCasa/HabHERMANMAY/may/martes/mayHabMayEnPc4taInteracMartes_visit1.rpy:29
translate english mayHabMayEnPc4taInteracMartes_visit1_44d95254:

    # hermanMay "jajajaja"
    hermanMay "hahahaha"

# game/Code/Interacciones/miCasa/HabHERMANMAY/may/martes/mayHabMayEnPc4taInteracMartes_visit1.rpy:33
translate english mayHabMayEnPc4taInteracMartes_visit1_c2c934ea:

    # prota "Solo me preocupo por las cosas de mi [situPeq2] favorita."
    prota "I just care about my favorite [situPeq2] stuff."

# game/Code/Interacciones/miCasa/HabHERMANMAY/may/martes/mayHabMayEnPc4taInteracMartes_visit1.rpy:37
translate english mayHabMayEnPc4taInteracMartes_visit1_656017cd:

    # hermanMay "Ya claro…"
    hermanMay "Ya sure..."

# game/Code/Interacciones/miCasa/HabHERMANMAY/may/martes/mayHabMayEnPc4taInteracMartes_visit1.rpy:39
translate english mayHabMayEnPc4taInteracMartes_visit1_7c192b9d:

    # prota "Te dejo trabajar."
    prota "I´ll let you work."
