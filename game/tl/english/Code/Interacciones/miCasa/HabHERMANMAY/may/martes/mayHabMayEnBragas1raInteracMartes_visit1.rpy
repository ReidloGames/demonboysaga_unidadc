﻿# TODO: Translation updated at 2021-08-06 14:00

# game/Code/Interacciones/miCasa/HabHERMANMAY/may/martes/mayHabMayEnBragas1raInteracMartes_visit1.rpy:17
translate english mayHabMayEnBragas1raInteracMartes_visit1_715efb1a:

    # hermanMay "¿Vaya [nombreProta2], no sabes llamar a la puerta?"
    hermanMay "Wow [nombreProta2], don't you know how to knock?"

# game/Code/Interacciones/miCasa/HabHERMANMAY/may/martes/mayHabMayEnBragas1raInteracMartes_visit1.rpy:23
translate english mayHabMayEnBragas1raInteracMartes_visit1_3feaf419:

    # prota "Perdón la puerta no estaba cerrada y..."
    prota "Sorry the door wasn't locked and..."

# game/Code/Interacciones/miCasa/HabHERMANMAY/may/martes/mayHabMayEnBragas1raInteracMartes_visit1.rpy:33
translate english mayHabMayEnBragas1raInteracMartes_visit1_b4807006:

    # hermanMay "A lo mejor [nombreMed] tiene razón y eres un pervertido."
    hermanMay "Maybe [nombreMed] is right and you're a pervert."

# game/Code/Interacciones/miCasa/HabHERMANMAY/may/martes/mayHabMayEnBragas1raInteracMartes_visit1.rpy:43
translate english mayHabMayEnBragas1raInteracMartes_visit1_848094cc:

    # hermanMay "Tranquilo estaba bromeando... Tampoco te vas asustar por ver a tu [situMay] en bragas. Pero no te acostumbres."
    hermanMay "Relax I was just kidding.... You're not going to freak out about seeing your [situMay] in her panties either. But don't get used to it."

# game/Code/Interacciones/miCasa/HabHERMANMAY/may/martes/mayHabMayEnBragas1raInteracMartes_visit1.rpy:53
translate english mayHabMayEnBragas1raInteracMartes_visit1_cf13386d:

    # protaPensa "{size=15}{i}Todo lo contrario {/size}{/i}"
    protaPensa "{size=15}{i}quite the opposite {/size}{/i}"

# game/Code/Interacciones/miCasa/HabHERMANMAY/may/martes/mayHabMayEnBragas1raInteracMartes_visit1.rpy:55
translate english mayHabMayEnBragas1raInteracMartes_visit1_45d5b748:

    # protaPensa "{size=15}{i}Que gran vista {/size}{/i}"
    protaPensa "{size=15}{i}What a great view {/size}{/i}"

# game/Code/Interacciones/miCasa/HabHERMANMAY/may/martes/mayHabMayEnBragas1raInteracMartes_visit1.rpy:59
translate english mayHabMayEnBragas1raInteracMartes_visit1_2a2cb5a4:

    # prota "Solo pasaba a saludar y saber cómo te iba todo."
    prota "Just stopping by to say hi and see how you were doing."

# game/Code/Interacciones/miCasa/HabHERMANMAY/may/martes/mayHabMayEnBragas1raInteracMartes_visit1.rpy:67
translate english mayHabMayEnBragas1raInteracMartes_visit1_68b9c70d:

    # hermanMay "Muy bien, estaba ordenando un poco la ropa del armario y aparte de eso muy contenta porque voy a realizar unos pedidos para la tienda de ropa."
    hermanMay "Very well, I was just sorting out some clothes from the closet and apart from that very happy because I'm going to be placing some orders for the store."

# game/Code/Interacciones/miCasa/HabHERMANMAY/may/martes/mayHabMayEnBragas1raInteracMartes_visit1.rpy:71
translate english mayHabMayEnBragas1raInteracMartes_visit1_d765fc48:

    # hermanMay "Hable con [nombrePad3] hace unos días y me lo financiara todo."
    hermanMay "I talked to [nombrePad3] a few days ago and he will finance everything."

# game/Code/Interacciones/miCasa/HabHERMANMAY/may/martes/mayHabMayEnBragas1raInteracMartes_visit1.rpy:73
translate english mayHabMayEnBragas1raInteracMartes_visit1_39f6312f:

    # hermanMay "Estoy muy feliz de tener mi propio negocio y creo que estos conjuntos de ropa le darán un plus diferente a la tienda."
    hermanMay "I'm very happy to have my own business and I think these clothing outfits will add a plus to the store."

# game/Code/Interacciones/miCasa/HabHERMANMAY/may/martes/mayHabMayEnBragas1raInteracMartes_visit1.rpy:79
translate english mayHabMayEnBragas1raInteracMartes_visit1_1e242973:

    # protaPensa "En esta casa todos dependen de [nombrePad3] es increíble."
    protaPensa "In this house everyone depends on [nombrePad3] is unbelievable."

# game/Code/Interacciones/miCasa/HabHERMANMAY/may/martes/mayHabMayEnBragas1raInteracMartes_visit1.rpy:81
translate english mayHabMayEnBragas1raInteracMartes_visit1_04bfd996:

    # prota "Me alegro que te vaya todo bien"
    prota "I'm glad everything is going well for you."

# game/Code/Interacciones/miCasa/HabHERMANMAY/may/martes/mayHabMayEnBragas1raInteracMartes_visit1.rpy:83
translate english mayHabMayEnBragas1raInteracMartes_visit1_b2e46e19:

    # hermanMay "¡Si! Gracias. "
    hermanMay "Yes! Thank you. "
