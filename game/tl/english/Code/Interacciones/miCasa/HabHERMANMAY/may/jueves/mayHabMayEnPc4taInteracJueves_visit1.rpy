﻿# TODO: Translation updated at 2021-08-06 14:00

# game/Code/Interacciones/miCasa/HabHERMANMAY/may/jueves/mayHabMayEnPc4taInteracJueves_visit1.rpy:11
translate english mayHabMayEnPc4taInteracJueves_visit1_9a83018c:

    # prota "¿Hola, que haces?"
    prota "Hello, what are you doing?"

# game/Code/Interacciones/miCasa/HabHERMANMAY/may/jueves/mayHabMayEnPc4taInteracJueves_visit1.rpy:17
translate english mayHabMayEnPc4taInteracJueves_visit1_53fabc3c:

    # hermanMay "Estoy mirando como se podría hacer una web de pedidos online"
    hermanMay "I'm looking at how I could design an online ordering website"

# game/Code/Interacciones/miCasa/HabHERMANMAY/may/jueves/mayHabMayEnPc4taInteracJueves_visit1.rpy:21
translate english mayHabMayEnPc4taInteracJueves_visit1_f28dd62e:

    # hermanMay "Mejor hablamos luego. No acabo de entender cómo hacer todo esto y estoy muy agobiada."
    hermanMay "We'd better talk later. I don't quite understand how to do all this and I'm really overwhelmed."
