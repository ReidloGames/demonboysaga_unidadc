﻿# TODO: Translation updated at 2021-07-08 13:36

# game/Code/Interacciones/miCasa/HABPADR/pad/lunes/padSentadoCamaHabPadr1raInteracLunes_visit1.rpy:5
translate english padSentadoCamaHabPadr1raInteracLunes_visit1_23fd85bf:

    # prota "Hola [situPad2]"
    prota "Hello [situPad2]"

# game/Code/Interacciones/miCasa/HABPADR/pad/lunes/padSentadoCamaHabPadr1raInteracLunes_visit1.rpy:11
translate english padSentadoCamaHabPadr1raInteracLunes_visit1_3ede97dc:

    # pad "Hola [nombreProta2]"
    pad "Hi [nombreProta2]"

# game/Code/Interacciones/miCasa/HABPADR/pad/lunes/padSentadoCamaHabPadr1raInteracLunes_visit1.rpy:14
translate english padSentadoCamaHabPadr1raInteracLunes_visit1_4c7db2fa:

    # prota "Que haces aquí tan solo? "
    prota "What are you doing here all alone?"

# game/Code/Interacciones/miCasa/HABPADR/pad/lunes/padSentadoCamaHabPadr1raInteracLunes_visit1.rpy:15
translate english padSentadoCamaHabPadr1raInteracLunes_visit1_b23cf33c:

    # protaPensa "{size=15}{i}Esta pensativo...{/size}{/i}"
    protaPensa "{size=15}{i}He´s pensive...{/size}{/i}"

# game/Code/Interacciones/miCasa/HABPADR/pad/lunes/padSentadoCamaHabPadr1raInteracLunes_visit1.rpy:17
translate english padSentadoCamaHabPadr1raInteracLunes_visit1_61b0ca46:

    # pad "Oh, nada. Solo necesitaba desconectar un poco la mente."
    pad "Oh, nothing. I just needed to take my mind off things for a bit"

# game/Code/Interacciones/miCasa/HABPADR/pad/lunes/padSentadoCamaHabPadr1raInteracLunes_visit1.rpy:25
translate english padSentadoCamaHabPadr1raInteracLunes_visit1_297a4a9b:

    # "!RIIING!"
    "RIIING!"

# game/Code/Interacciones/miCasa/HABPADR/pad/lunes/padSentadoCamaHabPadr1raInteracLunes_visit1.rpy:37
translate english padSentadoCamaHabPadr1raInteracLunes_visit1_12a8ac5b:

    # pad "Si no te importa hablamos mas tarde"
    pad "If you don't mind I'll talk to you later."

# game/Code/Interacciones/miCasa/HABPADR/pad/lunes/padSentadoCamaHabPadr1raInteracLunes_visit1.rpy:44
translate english padSentadoCamaHabPadr1raInteracLunes_visit1_166838ac:

    # prota "Si, ningún problema"
    prota "Yes, no problem"

# game/Code/Interacciones/miCasa/HABPADR/pad/lunes/padSentadoCamaHabPadr1raInteracLunes_visit1.rpy:55
translate english padSentadoCamaHabPadr1raInteracLunes_visit1_2bf40979:

    # protaPensa "{size=15}{i}Tiene cara de preocupación. No me da buena espina.{/size}{/i}"
    protaPensa "{size=15}{i}He's got a worried look on his face. I don't have a good feeling about him {/size}{/i}"

# game/Code/Interacciones/miCasa/HABPADR/pad/lunes/padSentadoCamaHabPadr1raInteracLunes_visit1.rpy:72
translate english espiarPadLlamadaTelefono_906429f6:

    # pad "Si, te pagare la proxima semana."
    pad "Yes, I'll pay you next week."

# game/Code/Interacciones/miCasa/HABPADR/pad/lunes/padSentadoCamaHabPadr1raInteracLunes_visit1.rpy:77
translate english espiarPadLlamadaTelefono_8d377e95:

    # pad "Así sera..."
    pad "So be it..."

# game/Code/Interacciones/miCasa/HABPADR/pad/lunes/padSentadoCamaHabPadr1raInteracLunes_visit1.rpy:80
translate english espiarPadLlamadaTelefono_cd68060c:

    # protaPensa "{size=15}{i}Me voy antes de que me vea.{/size}{/i}"
    protaPensa "{size=15}{i}I'm leaving before he sees me."

translate english strings:

    # game/Code/Interacciones/miCasa/HABPADR/pad/lunes/padSentadoCamaHabPadr1raInteracLunes_visit1.rpy:64
    old "Espiar"
    new "Spy"
