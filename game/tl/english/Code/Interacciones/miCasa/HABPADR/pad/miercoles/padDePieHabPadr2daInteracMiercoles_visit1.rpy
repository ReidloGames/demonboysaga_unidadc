﻿# TODO: Translation updated at 2021-07-08 13:36

# game/Code/Interacciones/miCasa/HABPADR/pad/miercoles/padDePieHabPadr2daInteracMiercoles_visit1.rpy:9
translate english padDePieHabPadr2daInteracMiercoles_visit1_4f98bf56:

    # prota "Hola [nombrePad3], va todo bien?"
    prota "Hello [nombrePad3], is everything okay?"

# game/Code/Interacciones/miCasa/HABPADR/pad/miercoles/padDePieHabPadr2daInteracMiercoles_visit1.rpy:17
translate english padDePieHabPadr2daInteracMiercoles_visit1_f59beb51:

    # pad "si, todo va perfecto."
    pad "yes, everything is going perfect."

# game/Code/Interacciones/miCasa/HABPADR/pad/miercoles/padDePieHabPadr2daInteracMiercoles_visit1.rpy:28
translate english padDePieHabPadr2daInteracMiercoles_visit1_1d25e1d7:

    # prota "Podrías darme algo de dinero?"
    prota "Could you give me some money?"

# game/Code/Interacciones/miCasa/HABPADR/pad/miercoles/padDePieHabPadr2daInteracMiercoles_visit1.rpy:36
translate english padDePieHabPadr2daInteracMiercoles_visit1_fa64e0b7:

    # pad "Necesitas empezar a responsabilizarte y dejar de depender tanto de mi"
    pad "You need to start taking responsibility and stop depending on me so much."

# game/Code/Interacciones/miCasa/HABPADR/pad/miercoles/padDePieHabPadr2daInteracMiercoles_visit1.rpy:42
translate english padDePieHabPadr2daInteracMiercoles_visit1_7b50add3:

    # pad "Te doy 1$"
    pad "I'll give you $1"

# game/Code/Interacciones/miCasa/HABPADR/pad/miercoles/padDePieHabPadr2daInteracMiercoles_visit1.rpy:49
translate english padDePieHabPadr2daInteracMiercoles_visit1_72d233be:

    # prota "Que?? pero si con eso no tengo ni para cacahuetes!!"
    prota "What? I can't even afford peanuts with that!!!"

# game/Code/Interacciones/miCasa/HABPADR/pad/miercoles/padDePieHabPadr2daInteracMiercoles_visit1.rpy:53
translate english padDePieHabPadr2daInteracMiercoles_visit1_9921098e:

    # pad "Seguro que te apañas,tengo cosas que hacer."
    pad "I'm sure you can manage, I have things to do."

# game/Code/Interacciones/miCasa/HABPADR/pad/miercoles/padDePieHabPadr2daInteracMiercoles_visit1.rpy:58
translate english padDePieHabPadr2daInteracMiercoles_visit1_1aad462f:

    # protaPensa "{size=15}{i}Si fuera para Dana, Erika o Kara seguro que la cantidad tendría tres ceros detras.{/size}{/i}"
    protaPensa "{size=15}{i}If it were for Dana, Erika or Kara I'm sure the amount would have three zeros behind it.{/size}{/i}"
# TODO: Translation updated at 2022-02-16 11:36

# game/Code/Interacciones/miCasa/HABPADR/pad/miercoles/padDePieHabPadr2daInteracMiercoles_visit1.rpy:50
translate english padDePieHabPadr2daInteracMiercoles_visit1_38388044:

    # protaPensa "{size=15}{i}Qué patín tan barato. Debería beber un poco de leche antes de irme a Uni.{/size}{/i}"
    protaPensa "What a cheap skate. I should drink some milk before I go to Uni."
# TODO: Translation updated at 2022-03-27 17:09

# game/Code/Interacciones/miCasa/HABPADR/pad/miercoles/padDePieHabPadr2daInteracMiercoles_visit1.rpy:50
translate english padDePieHabPadr2daInteracMiercoles_visit1_36bc001c:

    # protaPensa "{size=15}{i}Conmigo es un tacaño. Debería beber un poco de leche antes de irme a la Uni.{/size}{/i}"
    protaPensa "{size=15}{i}With me he is so stingy. I should drink some milk before I go to Uni.{/size}{/i}"
