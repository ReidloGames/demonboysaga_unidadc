﻿# TODO: Translation updated at 2021-10-13 15:38

# game/Code/Interacciones/miCasa/HABPADR/mad/miercoles/madHabPadrEnPijama4taInteracMiercoles_visit2.rpy:6
translate english madHabPadrEnPijama4taInteracMiercoles_visit2_27b9e079:

    # mad "Me has asustado"
    mad "You scared me"

# game/Code/Interacciones/miCasa/HABPADR/mad/miercoles/madHabPadrEnPijama4taInteracMiercoles_visit2.rpy:7
translate english madHabPadrEnPijama4taInteracMiercoles_visit2_1d0cb668:

    # mad "Tienes que llamar a la puerta"
    mad "You have to knock on the door."

# game/Code/Interacciones/miCasa/HABPADR/mad/miercoles/madHabPadrEnPijama4taInteracMiercoles_visit2.rpy:9
translate english madHabPadrEnPijama4taInteracMiercoles_visit2_cfa3d543:

    # prota "Perdón, pero necesitaba hablar contigo y con las prisas..."
    prota "Sorry, but I needed to talk to you and with the rush..."

# game/Code/Interacciones/miCasa/HABPADR/mad/miercoles/madHabPadrEnPijama4taInteracMiercoles_visit2.rpy:11
translate english madHabPadrEnPijama4taInteracMiercoles_visit2_1d7f2c24:

    # mad "Dime..."
    mad "Tell me..."

# game/Code/Interacciones/miCasa/HABPADR/mad/miercoles/madHabPadrEnPijama4taInteracMiercoles_visit2.rpy:18
translate english madHabPadrEnPijama4taInteracMiercoles_visit2_16b5add7:

    # prota "Solo quería darte las buenas noches."
    prota "I just wanted to say good night to you."

# game/Code/Interacciones/miCasa/HABPADR/mad/miercoles/madHabPadrEnPijama4taInteracMiercoles_visit2.rpy:20
translate english madHabPadrEnPijama4taInteracMiercoles_visit2_42636344:

    # mad "Ohh, gracias igualmente."
    mad "Ohh, thanks anyway."

# game/Code/Interacciones/miCasa/HABPADR/mad/miercoles/madHabPadrEnPijama4taInteracMiercoles_visit2.rpy:25
translate english madHabPadrEnPijama4taInteracMiercoles_visit2_182506f9:

    # mad "Todo esto ha sido muy duro, de la noche a la mañana todo ha cambiado."
    mad "This has all been very hard, overnight everything has changed."

# game/Code/Interacciones/miCasa/HABPADR/mad/miercoles/madHabPadrEnPijama4taInteracMiercoles_visit2.rpy:27
translate english madHabPadrEnPijama4taInteracMiercoles_visit2_67f73d30:

    # mad "[situPad2] no esta y ahora tengo toda la responsabilidad de la casa."
    mad "[situPad2] is gone and now I have all the responsibility for the house."

# game/Code/Interacciones/miCasa/HABPADR/mad/miercoles/madHabPadrEnPijama4taInteracMiercoles_visit2.rpy:29
translate english madHabPadrEnPijama4taInteracMiercoles_visit2_4e739129:

    # mad "Estoy mirando empleos."
    mad "I'm looking at jobs."

# game/Code/Interacciones/miCasa/HABPADR/mad/miercoles/madHabPadrEnPijama4taInteracMiercoles_visit2.rpy:31
translate english madHabPadrEnPijama4taInteracMiercoles_visit2_ffe72a7b:

    # mad "Creo que he podido encontrar uno. Aún no estoy del todo segura."
    mad "I think I might have found one. I'm not quite sure yet."

# game/Code/Interacciones/miCasa/HABPADR/mad/miercoles/madHabPadrEnPijama4taInteracMiercoles_visit2.rpy:33
translate english madHabPadrEnPijama4taInteracMiercoles_visit2_c9f106ed:

    # mad "Eso es fantástico."
    mad "That's fantastic."

# game/Code/Interacciones/miCasa/HABPADR/mad/miercoles/madHabPadrEnPijama4taInteracMiercoles_visit2.rpy:35
translate english madHabPadrEnPijama4taInteracMiercoles_visit2_e0761bc7:

    # prota "Yo me esforzare al máximo."
    prota "I'll try my best."

# game/Code/Interacciones/miCasa/HABPADR/mad/miercoles/madHabPadrEnPijama4taInteracMiercoles_visit2.rpy:37
translate english madHabPadrEnPijama4taInteracMiercoles_visit2_4a9ed919:

    # prota "No te preocupes."
    prota "Don't worry."

# game/Code/Interacciones/miCasa/HABPADR/mad/miercoles/madHabPadrEnPijama4taInteracMiercoles_visit2.rpy:39
translate english madHabPadrEnPijama4taInteracMiercoles_visit2_c340dd8a:

    # prota "Buenas noches."
    prota "Good night."

# game/Code/Interacciones/miCasa/HABPADR/mad/miercoles/madHabPadrEnPijama4taInteracMiercoles_visit2.rpy:41
translate english madHabPadrEnPijama4taInteracMiercoles_visit2_c04032e8:

    # mad "Gracias, buenas noches."
    mad "Thank you, good night."

# game/Code/Interacciones/miCasa/HABPADR/mad/miercoles/madHabPadrEnPijama4taInteracMiercoles_visit2.rpy:48
translate english madHabPadrEnPijama4taInteracMiercoles_visit2_6636c33a:

    # prota "Tengo una noticia muy buena que contarte."
    prota "I have some very good news to tell you."

# game/Code/Interacciones/miCasa/HABPADR/mad/miercoles/madHabPadrEnPijama4taInteracMiercoles_visit2.rpy:50
translate english madHabPadrEnPijama4taInteracMiercoles_visit2_e9fa8444:

    # mad "¿Que ha pasado?"
    mad "What happened?"

# game/Code/Interacciones/miCasa/HABPADR/mad/miercoles/madHabPadrEnPijama4taInteracMiercoles_visit2.rpy:52
translate english madHabPadrEnPijama4taInteracMiercoles_visit2_75ef5be4:

    # prota "Ya encontré trabajo."
    prota "I've already found a job."

# game/Code/Interacciones/miCasa/HABPADR/mad/miercoles/madHabPadrEnPijama4taInteracMiercoles_visit2.rpy:59
translate english madHabPadrEnPijama4taInteracMiercoles_visit2_67aad4dd:

    # prota "Estoy haciendo un curso para ser masajista. Me lo está enseñando un profesional en la materia."
    prota "I'm taking a course to become a masseur. I'm being taught by a professional in the field."

# game/Code/Interacciones/miCasa/HABPADR/mad/miercoles/madHabPadrEnPijama4taInteracMiercoles_visit2.rpy:61
translate english madHabPadrEnPijama4taInteracMiercoles_visit2_9d734638:

    # prota "Ellos me enseñan la teoría y a cambio algunos días voy para ayudarles."
    prota "They teach me the theory and in return some days I go to help them."

# game/Code/Interacciones/miCasa/HABPADR/mad/miercoles/madHabPadrEnPijama4taInteracMiercoles_visit2.rpy:63
translate english madHabPadrEnPijama4taInteracMiercoles_visit2_69786660:

    # prota "Hay gente deportista y gente mayor que necesitan cuidados y ellos me pagan."
    prota "There are sports people and elderly people who need care and they pay me."

# game/Code/Interacciones/miCasa/HABPADR/mad/miercoles/madHabPadrEnPijama4taInteracMiercoles_visit2.rpy:65
translate english madHabPadrEnPijama4taInteracMiercoles_visit2_1d47cbc9:

    # prota "Los masajes les ayudan mucho. También va gente que necesita un reconfortante masaje."
    prota "The massages help them a lot. People also go who need a comforting massage."

# game/Code/Interacciones/miCasa/HABPADR/mad/miercoles/madHabPadrEnPijama4taInteracMiercoles_visit2.rpy:67
translate english madHabPadrEnPijama4taInteracMiercoles_visit2_89ec261b:

    # prota "Son gente muy profesional."
    prota "They are very professional people."

# game/Code/Interacciones/miCasa/HABPADR/mad/miercoles/madHabPadrEnPijama4taInteracMiercoles_visit2.rpy:69
translate english madHabPadrEnPijama4taInteracMiercoles_visit2_7bc956a6:

    # protaPensa "Menuda mentira, lo primero que se me ocurrió..."
    protaPensa "What a lie, the first thing that came to my mind..."

# game/Code/Interacciones/miCasa/HABPADR/mad/miercoles/madHabPadrEnPijama4taInteracMiercoles_visit2.rpy:71
translate english madHabPadrEnPijama4taInteracMiercoles_visit2_2cba4fe4:

    # mad "¡¡Oh!! eso está muy bien. Que gran sorpresa."
    mad "Oh!!! that's very good. What a great surprise."

# game/Code/Interacciones/miCasa/HABPADR/mad/miercoles/madHabPadrEnPijama4taInteracMiercoles_visit2.rpy:73
translate english madHabPadrEnPijama4taInteracMiercoles_visit2_8dcac7f1:

    # prota "Si, lo único que me han dicho que como están saturados de gente no pueden dedicarme el tiempo necesario para poner en práctica las teorías."
    prota "Yes, the only thing I've been told is that since they are saturated with people they can't give me the time I need to put the theories into practice."

# game/Code/Interacciones/miCasa/HABPADR/mad/miercoles/madHabPadrEnPijama4taInteracMiercoles_visit2.rpy:75
translate english madHabPadrEnPijama4taInteracMiercoles_visit2_ea5b7a6b:

    # mad "¿Como? ¿Y cómo aprendes entonces?"
    mad "How? And how do you learn then?"

# game/Code/Interacciones/miCasa/HABPADR/mad/miercoles/madHabPadrEnPijama4taInteracMiercoles_visit2.rpy:77
translate english madHabPadrEnPijama4taInteracMiercoles_visit2_247396f2:

    # prota "Claro, ahí está el problema, me dijeron que practicara con algún conocido y como me estan pagando creo que no hay nada que perder. A parte me está gustando mucho, ya que puedo ayudar a los demás."
    prota "Sure, that's the problem, they told me to practice with someone I know and since they're paying me I think there's nothing to lose. Besides I'm really enjoying it, since I can help others."

# game/Code/Interacciones/miCasa/HABPADR/mad/miercoles/madHabPadrEnPijama4taInteracMiercoles_visit2.rpy:79
translate english madHabPadrEnPijama4taInteracMiercoles_visit2_f9bcf21d:

    # prota "He pensado que podría practicar contigo."
    prota "I thought I could practice with you."

# game/Code/Interacciones/miCasa/HABPADR/mad/miercoles/madHabPadrEnPijama4taInteracMiercoles_visit2.rpy:82
translate english madHabPadrEnPijama4taInteracMiercoles_visit2_2f3afc34:

    # mad "¿Conmigo? Pero [nombreProta2], estoy muy liada ahora mismo. Tengo muchas cosas en la cabeza."
    mad "With me? But [nombreProta2], I'm really busy right now. I have a lot on my mind."

# game/Code/Interacciones/miCasa/HABPADR/mad/miercoles/madHabPadrEnPijama4taInteracMiercoles_visit2.rpy:84
translate english madHabPadrEnPijama4taInteracMiercoles_visit2_8a1f5b5d:

    # prota "Pero el otro día dijiste que juntos saldríamos de este problema. Estoy ganando dinero, pensé que me ayudarías..."
    prota "But the other day you said that together we'd get out of this problem. I'm making money, I thought you would help me..."

# game/Code/Interacciones/miCasa/HABPADR/mad/miercoles/madHabPadrEnPijama4taInteracMiercoles_visit2.rpy:87
translate english madHabPadrEnPijama4taInteracMiercoles_visit2_6e34c0d5:

    # prota "Mira aquí está el primer pago."
    prota "Look here's the first payment."

# game/Code/Interacciones/miCasa/HABPADR/mad/miercoles/madHabPadrEnPijama4taInteracMiercoles_visit2.rpy:90
translate english madHabPadrEnPijama4taInteracMiercoles_visit2_3e39d4cb:

    # mad "Ehh...Es muy amable de tu parte."
    mad "Ehh...That's very kind of you."

# game/Code/Interacciones/miCasa/HABPADR/mad/miercoles/madHabPadrEnPijama4taInteracMiercoles_visit2.rpy:92
translate english madHabPadrEnPijama4taInteracMiercoles_visit2_ea22e25b:

    # mad "¿Pero en que consiste esto?"
    mad "But what's this about?"

# game/Code/Interacciones/miCasa/HABPADR/mad/miercoles/madHabPadrEnPijama4taInteracMiercoles_visit2.rpy:94
translate english madHabPadrEnPijama4taInteracMiercoles_visit2_8992efd7:

    # prota "Nada, solo son masajes relajantes. A parte creo que te podrían ir bien."
    prota "Nothing, it's just relaxing massages. Besides, I think it might be good for you."

# game/Code/Interacciones/miCasa/HABPADR/mad/miercoles/madHabPadrEnPijama4taInteracMiercoles_visit2.rpy:96
translate english madHabPadrEnPijama4taInteracMiercoles_visit2_aa5050fc:

    # mad "¿Y no tienes a nadie más?"
    mad "And you don't have anyone else?"

# game/Code/Interacciones/miCasa/HABPADR/mad/miercoles/madHabPadrEnPijama4taInteracMiercoles_visit2.rpy:98
translate english madHabPadrEnPijama4taInteracMiercoles_visit2_078854f8:

    # prota "Necesito alguien de confianza, pensaba que tú me ayudarías..."
    prota "I need someone I can trust, I thought you could help me..."

# game/Code/Interacciones/miCasa/HABPADR/mad/miercoles/madHabPadrEnPijama4taInteracMiercoles_visit2.rpy:100
translate english madHabPadrEnPijama4taInteracMiercoles_visit2_dcffcca8:

    # mad "Esta bien..."
    mad "Okay..."

# game/Code/Interacciones/miCasa/HABPADR/mad/miercoles/madHabPadrEnPijama4taInteracMiercoles_visit2.rpy:102
translate english madHabPadrEnPijama4taInteracMiercoles_visit2_d895b4b0:

    # prota "Genial, ya verás que todo ira mejorando."
    prota "Great, you'll see that everything will get better."

# game/Code/Interacciones/miCasa/HABPADR/mad/miercoles/madHabPadrEnPijama4taInteracMiercoles_visit2.rpy:104
translate english madHabPadrEnPijama4taInteracMiercoles_visit2_53182315:

    # mad "Es bueno ver cómo te esfuerzas."
    mad "It's good to see you making an effort."

# game/Code/Interacciones/miCasa/HABPADR/mad/miercoles/madHabPadrEnPijama4taInteracMiercoles_visit2.rpy:106
translate english madHabPadrEnPijama4taInteracMiercoles_visit2_fb6d6923:

    # prota "Por ti lo que sea [nombreMad3]."
    prota "Anything for you [nombreMad3]."

# game/Code/Interacciones/miCasa/HABPADR/mad/miercoles/madHabPadrEnPijama4taInteracMiercoles_visit2.rpy:108
translate english madHabPadrEnPijama4taInteracMiercoles_visit2_eb94f456:

    # protaPensa "No me lo puedo creer, se ha tragado la mentira."
    protaPensa "I can't believe it, he swallowed the lie."

# game/Code/Interacciones/miCasa/HABPADR/mad/miercoles/madHabPadrEnPijama4taInteracMiercoles_visit2.rpy:110
translate english madHabPadrEnPijama4taInteracMiercoles_visit2_c340dd8a_1:

    # prota "Buenas noches."
    prota "Good night."

# game/Code/Interacciones/miCasa/HABPADR/mad/miercoles/madHabPadrEnPijama4taInteracMiercoles_visit2.rpy:116
translate english madHabPadrEnPijama4taInteracMiercoles_visit2_5d3db926:

    # protaPensa "Necesito 10€ para poderle dar a [nombreMad3] y que confie en mi."
    protaPensa "I need 10€ so I can give [nombreMad3] so she trusts me."

# game/Code/Interacciones/miCasa/HABPADR/mad/miercoles/madHabPadrEnPijama4taInteracMiercoles_visit2.rpy:118
translate english madHabPadrEnPijama4taInteracMiercoles_visit2_c340dd8a_2:

    # prota "Buenas noches."
    prota "Good night."

# game/Code/Interacciones/miCasa/HABPADR/mad/miercoles/madHabPadrEnPijama4taInteracMiercoles_visit2.rpy:120
translate english madHabPadrEnPijama4taInteracMiercoles_visit2_c04032e8_1:

    # mad "Gracias, buenas noches."
    mad "Thank you, good night."

translate english strings:

    # game/Code/Interacciones/miCasa/HABPADR/mad/miercoles/madHabPadrEnPijama4taInteracMiercoles_visit2.rpy:15
    old "Dar las buenas noches"
    new "Say good night."

    # game/Code/Interacciones/miCasa/HABPADR/mad/miercoles/madHabPadrEnPijama4taInteracMiercoles_visit2.rpy:15
    old "Preguntar como esta"
    new "Ask how he is."

    # game/Code/Interacciones/miCasa/HABPADR/mad/miercoles/madHabPadrEnPijama4taInteracMiercoles_visit2.rpy:15
    old "Informar de tu nuevo trabajo"
    new "Report your new job"
# TODO: Translation updated at 2021-12-12 13:06

# game/Code/Interacciones/miCasa/HABPADR/mad/miercoles/madHabPadrEnPijama4taInteracMiercoles_visit2.rpy:49
translate english preguntarComoEstaMad_182506f9:

    # mad "Todo esto ha sido muy duro, de la noche a la mañana todo ha cambiado."
    mad "This has all been very hard, overnight everything has changed."

# game/Code/Interacciones/miCasa/HABPADR/mad/miercoles/madHabPadrEnPijama4taInteracMiercoles_visit2.rpy:51
translate english preguntarComoEstaMad_67f73d30:

    # mad "[situPad2] no esta y ahora tengo toda la responsabilidad de la casa."
    mad "[situPad2] is gone and now I have all the responsibility for the house."

# game/Code/Interacciones/miCasa/HABPADR/mad/miercoles/madHabPadrEnPijama4taInteracMiercoles_visit2.rpy:53
translate english preguntarComoEstaMad_4e739129:

    # mad "Estoy mirando empleos."
    mad "I'm looking at jobs."

# game/Code/Interacciones/miCasa/HABPADR/mad/miercoles/madHabPadrEnPijama4taInteracMiercoles_visit2.rpy:55
translate english preguntarComoEstaMad_ffe72a7b:

    # mad "Creo que he podido encontrar uno. Aún no estoy del todo segura."
    mad "I think I might have found one. I'm not quite sure yet."

# game/Code/Interacciones/miCasa/HABPADR/mad/miercoles/madHabPadrEnPijama4taInteracMiercoles_visit2.rpy:57
translate english preguntarComoEstaMad_c9f106ed:

    # mad "Eso es fantástico."
    mad "That's fantastic."

# game/Code/Interacciones/miCasa/HABPADR/mad/miercoles/madHabPadrEnPijama4taInteracMiercoles_visit2.rpy:59
translate english preguntarComoEstaMad_e0761bc7:

    # prota "Yo me esforzare al máximo."
    prota "I'll try my best."

# game/Code/Interacciones/miCasa/HABPADR/mad/miercoles/madHabPadrEnPijama4taInteracMiercoles_visit2.rpy:61
translate english preguntarComoEstaMad_4a9ed919:

    # prota "No te preocupes."
    prota "Don't worry."

# game/Code/Interacciones/miCasa/HABPADR/mad/miercoles/madHabPadrEnPijama4taInteracMiercoles_visit2.rpy:63
translate english preguntarComoEstaMad_c340dd8a:

    # prota "Buenas noches."
    prota "Good night."

# game/Code/Interacciones/miCasa/HABPADR/mad/miercoles/madHabPadrEnPijama4taInteracMiercoles_visit2.rpy:65
translate english preguntarComoEstaMad_c04032e8:

    # mad "Gracias, buenas noches."
    mad "Thank you, good night."

# game/Code/Interacciones/miCasa/HABPADR/mad/miercoles/madHabPadrEnPijama4taInteracMiercoles_visit2.rpy:72
translate english informarDeTuNuevoTrabajo_6636c33a:

    # prota "Tengo una noticia muy buena que contarte."
    prota "I have some very good news to tell you."

# game/Code/Interacciones/miCasa/HABPADR/mad/miercoles/madHabPadrEnPijama4taInteracMiercoles_visit2.rpy:74
translate english informarDeTuNuevoTrabajo_e9fa8444:

    # mad "¿Que ha pasado?"
    mad "What happened?"

# game/Code/Interacciones/miCasa/HABPADR/mad/miercoles/madHabPadrEnPijama4taInteracMiercoles_visit2.rpy:76
translate english informarDeTuNuevoTrabajo_75ef5be4:

    # prota "Ya encontré trabajo."
    prota "I've already found a job."

# game/Code/Interacciones/miCasa/HABPADR/mad/miercoles/madHabPadrEnPijama4taInteracMiercoles_visit2.rpy:83
translate english informarDeTuNuevoTrabajo_67aad4dd:

    # prota "Estoy haciendo un curso para ser masajista. Me lo está enseñando un profesional en la materia."
    prota "I'm taking a course to become a masseur. I'm being taught by a professional in the field."

# game/Code/Interacciones/miCasa/HABPADR/mad/miercoles/madHabPadrEnPijama4taInteracMiercoles_visit2.rpy:85
translate english informarDeTuNuevoTrabajo_9d734638:

    # prota "Ellos me enseñan la teoría y a cambio algunos días voy para ayudarles."
    prota "They teach me the theory and in return some days I go to help them."

# game/Code/Interacciones/miCasa/HABPADR/mad/miercoles/madHabPadrEnPijama4taInteracMiercoles_visit2.rpy:87
translate english informarDeTuNuevoTrabajo_69786660:

    # prota "Hay gente deportista y gente mayor que necesitan cuidados y ellos me pagan."
    prota "There are sports people and elderly people who need care and they pay me."

# game/Code/Interacciones/miCasa/HABPADR/mad/miercoles/madHabPadrEnPijama4taInteracMiercoles_visit2.rpy:89
translate english informarDeTuNuevoTrabajo_1d47cbc9:

    # prota "Los masajes les ayudan mucho. También va gente que necesita un reconfortante masaje."
    prota "The massages help them a lot. People also go who need a comforting massage."

# game/Code/Interacciones/miCasa/HABPADR/mad/miercoles/madHabPadrEnPijama4taInteracMiercoles_visit2.rpy:91
translate english informarDeTuNuevoTrabajo_89ec261b:

    # prota "Son gente muy profesional."
    prota "They are very professional people."

# game/Code/Interacciones/miCasa/HABPADR/mad/miercoles/madHabPadrEnPijama4taInteracMiercoles_visit2.rpy:93
translate english informarDeTuNuevoTrabajo_7bc956a6:

    # protaPensa "Menuda mentira, lo primero que se me ocurrió..."
    protaPensa "What a lie, the first thing that came to my mind..."

# game/Code/Interacciones/miCasa/HABPADR/mad/miercoles/madHabPadrEnPijama4taInteracMiercoles_visit2.rpy:95
translate english informarDeTuNuevoTrabajo_2cba4fe4:

    # mad "¡¡Oh!! eso está muy bien. Que gran sorpresa."
    mad "Oh!!! that's very good. What a great surprise."

# game/Code/Interacciones/miCasa/HABPADR/mad/miercoles/madHabPadrEnPijama4taInteracMiercoles_visit2.rpy:97
translate english informarDeTuNuevoTrabajo_8dcac7f1:

    # prota "Si, lo único que me han dicho que como están saturados de gente no pueden dedicarme el tiempo necesario para poner en práctica las teorías."
    prota "Yes, the only thing I've been told is that since they are saturated with people they can't give me the time I need to put the theories into practice."

# game/Code/Interacciones/miCasa/HABPADR/mad/miercoles/madHabPadrEnPijama4taInteracMiercoles_visit2.rpy:99
translate english informarDeTuNuevoTrabajo_ea5b7a6b:

    # mad "¿Como? ¿Y cómo aprendes entonces?"
    mad "How? And how do you learn then?"

# game/Code/Interacciones/miCasa/HABPADR/mad/miercoles/madHabPadrEnPijama4taInteracMiercoles_visit2.rpy:101
translate english informarDeTuNuevoTrabajo_247396f2:

    # prota "Claro, ahí está el problema, me dijeron que practicara con algún conocido y como me estan pagando creo que no hay nada que perder. A parte me está gustando mucho, ya que puedo ayudar a los demás."
    prota "Sure, that's the problem, they told me to practice with someone I know and since they're paying me I think there's nothing to lose. Besides I'm really enjoying it, since I can help others."

# game/Code/Interacciones/miCasa/HABPADR/mad/miercoles/madHabPadrEnPijama4taInteracMiercoles_visit2.rpy:103
translate english informarDeTuNuevoTrabajo_f9bcf21d:

    # prota "He pensado que podría practicar contigo."
    prota "I thought I could practice with you."

# game/Code/Interacciones/miCasa/HABPADR/mad/miercoles/madHabPadrEnPijama4taInteracMiercoles_visit2.rpy:106
translate english informarDeTuNuevoTrabajo_2f3afc34:

    # mad "¿Conmigo? Pero [nombreProta2], estoy muy liada ahora mismo. Tengo muchas cosas en la cabeza."
    mad "With me? But [nombreProta2], I'm really busy right now. I have a lot on my mind."

# game/Code/Interacciones/miCasa/HABPADR/mad/miercoles/madHabPadrEnPijama4taInteracMiercoles_visit2.rpy:108
translate english informarDeTuNuevoTrabajo_8a1f5b5d:

    # prota "Pero el otro día dijiste que juntos saldríamos de este problema. Estoy ganando dinero, pensé que me ayudarías..."
    prota "But the other day you said that together we'd get out of this problem. I'm making money, I thought you would help me..."

# game/Code/Interacciones/miCasa/HABPADR/mad/miercoles/madHabPadrEnPijama4taInteracMiercoles_visit2.rpy:111
translate english informarDeTuNuevoTrabajo_6e34c0d5:

    # prota "Mira aquí está el primer pago."
    prota "Look here's the first payment."

# game/Code/Interacciones/miCasa/HABPADR/mad/miercoles/madHabPadrEnPijama4taInteracMiercoles_visit2.rpy:114
translate english informarDeTuNuevoTrabajo_3e39d4cb:

    # mad "Ehh...Es muy amable de tu parte."
    mad "Ehh...That's very kind of you."

# game/Code/Interacciones/miCasa/HABPADR/mad/miercoles/madHabPadrEnPijama4taInteracMiercoles_visit2.rpy:116
translate english informarDeTuNuevoTrabajo_ea22e25b:

    # mad "¿Pero en que consiste esto?"
    mad "But what's this about?"

# game/Code/Interacciones/miCasa/HABPADR/mad/miercoles/madHabPadrEnPijama4taInteracMiercoles_visit2.rpy:118
translate english informarDeTuNuevoTrabajo_8992efd7:

    # prota "Nada, solo son masajes relajantes. A parte creo que te podrían ir bien."
    prota "Nothing, it's just relaxing massages. Besides, I think it might be good for you."

# game/Code/Interacciones/miCasa/HABPADR/mad/miercoles/madHabPadrEnPijama4taInteracMiercoles_visit2.rpy:120
translate english informarDeTuNuevoTrabajo_aa5050fc:

    # mad "¿Y no tienes a nadie más?"
    mad "And you don't have anyone else?"

# game/Code/Interacciones/miCasa/HABPADR/mad/miercoles/madHabPadrEnPijama4taInteracMiercoles_visit2.rpy:122
translate english informarDeTuNuevoTrabajo_078854f8:

    # prota "Necesito alguien de confianza, pensaba que tú me ayudarías..."
    prota "I need someone I can trust, I thought you could help me..."

# game/Code/Interacciones/miCasa/HABPADR/mad/miercoles/madHabPadrEnPijama4taInteracMiercoles_visit2.rpy:124
translate english informarDeTuNuevoTrabajo_dcffcca8:

    # mad "Esta bien..."
    mad "Okay..."

# game/Code/Interacciones/miCasa/HABPADR/mad/miercoles/madHabPadrEnPijama4taInteracMiercoles_visit2.rpy:126
translate english informarDeTuNuevoTrabajo_d895b4b0:

    # prota "Genial, ya verás que todo ira mejorando."
    prota "Great, you'll see that everything will get better."

# game/Code/Interacciones/miCasa/HABPADR/mad/miercoles/madHabPadrEnPijama4taInteracMiercoles_visit2.rpy:128
translate english informarDeTuNuevoTrabajo_53182315:

    # mad "Es bueno ver cómo te esfuerzas."
    mad "It's good to see you making an effort."

# game/Code/Interacciones/miCasa/HABPADR/mad/miercoles/madHabPadrEnPijama4taInteracMiercoles_visit2.rpy:130
translate english informarDeTuNuevoTrabajo_fb6d6923:

    # prota "Por ti lo que sea [nombreMad3]."
    prota "Anything for you [nombreMad3]"

# game/Code/Interacciones/miCasa/HABPADR/mad/miercoles/madHabPadrEnPijama4taInteracMiercoles_visit2.rpy:132
translate english informarDeTuNuevoTrabajo_eb94f456:

    # protaPensa "No me lo puedo creer, se ha tragado la mentira."
    protaPensa "I can't believe it, he swallowed the lie."

# game/Code/Interacciones/miCasa/HABPADR/mad/miercoles/madHabPadrEnPijama4taInteracMiercoles_visit2.rpy:134
translate english informarDeTuNuevoTrabajo_c340dd8a:

    # prota "Buenas noches."
    prota "Good night."

# game/Code/Interacciones/miCasa/HABPADR/mad/miercoles/madHabPadrEnPijama4taInteracMiercoles_visit2.rpy:141
translate english decirAMadNoTeGustaEthan_89983d4a:

    # prota "[nombreMad3], tengo que decirte que no me gusta Ethan."
    prota "[nombreMad3], I have to tell you that I don't like Ethan."

# game/Code/Interacciones/miCasa/HABPADR/mad/miercoles/madHabPadrEnPijama4taInteracMiercoles_visit2.rpy:143
translate english decirAMadNoTeGustaEthan_c0cf07b2:

    # prota "No creo que sea muy adecuado que vayas a su casa."
    prota "I don't think it's very appropriate for you to go to his house."

# game/Code/Interacciones/miCasa/HABPADR/mad/miercoles/madHabPadrEnPijama4taInteracMiercoles_visit2.rpy:145
translate english decirAMadNoTeGustaEthan_ebb5c216:

    # mad "¿Porque dices eso? Ethan es amigo de [nombrePad3] desde hace mucho tiempo. Me esta ayudando a tocar el violoncello y me lo paso bien."
    mad "Why do you say that? Ethan has been [nombrePad3]'s friend for a long time. He's helping me play the cello and I'm having a good time."

# game/Code/Interacciones/miCasa/HABPADR/mad/miercoles/madHabPadrEnPijama4taInteracMiercoles_visit2.rpy:147
translate english decirAMadNoTeGustaEthan_52822352:

    # mad "Ha venido varias veces a casa. ¿A qué viene esto?"
    mad "He's been home several times. Why is that?"

# game/Code/Interacciones/miCasa/HABPADR/mad/miercoles/madHabPadrEnPijama4taInteracMiercoles_visit2.rpy:149
translate english decirAMadNoTeGustaEthan_2ad856e4:

    # prota "..."
    prota "..."

# game/Code/Interacciones/miCasa/HABPADR/mad/miercoles/madHabPadrEnPijama4taInteracMiercoles_visit2.rpy:151
translate english decirAMadNoTeGustaEthan_a81ebf34:

    # prota "Nada, dejalo..."
    prota "Nothing, let it go..."

# game/Code/Interacciones/miCasa/HABPADR/mad/miercoles/madHabPadrEnPijama4taInteracMiercoles_visit2.rpy:153
translate english decirAMadNoTeGustaEthan_c340dd8a:

    # prota "Buenas noches."
    prota "Good night."

translate english strings:

    # game/Code/Interacciones/miCasa/HABPADR/mad/miercoles/madHabPadrEnPijama4taInteracMiercoles_visit2.rpy:15
    old "Decirle que no te gusta Ethan"
    new "Tell her you don't like Ethan."
# TODO: Translation updated at 2022-03-27 17:09

# game/Code/Interacciones/miCasa/HABPADR/mad/miercoles/madHabPadrEnPijama4taInteracMiercoles_visit2.rpy:166
translate english hablaLibroYogaSofia_538ac6b3:

    # prota "El otro día mientras estaba en la biblioteca me encontré de casualidad con este libro."
    prota "The other day while I was in the library I happened to come across this book."

# game/Code/Interacciones/miCasa/HABPADR/mad/miercoles/madHabPadrEnPijama4taInteracMiercoles_visit2.rpy:168
translate english hablaLibroYogaSofia_ab03259b:

    # prota "Es un libro para aprender hacer yoga con ejercicios para dos."
    prota "It is a book to learn to do yoga with exercises for two."

# game/Code/Interacciones/miCasa/HABPADR/mad/miercoles/madHabPadrEnPijama4taInteracMiercoles_visit2.rpy:170
translate english hablaLibroYogaSofia_50818822:

    # prota "Ahora si que podemos hacer ejercicio juntos."
    prota "Now we can exercise together."

# game/Code/Interacciones/miCasa/HABPADR/mad/miercoles/madHabPadrEnPijama4taInteracMiercoles_visit2.rpy:172
translate english hablaLibroYogaSofia_b3633b61:

    # mad "Oh... A ver."
    mad "Oh... Show me."

# game/Code/Interacciones/miCasa/HABPADR/mad/miercoles/madHabPadrEnPijama4taInteracMiercoles_visit2.rpy:177
translate english hablaLibroYogaSofia_4555f60c:

    # mad "Pero estos ejercicios son un poco..."
    mad "But these exercises are a bit..."

# game/Code/Interacciones/miCasa/HABPADR/mad/miercoles/madHabPadrEnPijama4taInteracMiercoles_visit2.rpy:179
translate english hablaLibroYogaSofia_6096881d:

    # prota "Oh, no te preocupes por la dificultad es como todo. Al principio será un poco difícil y luego iremos mejorando."
    prota "Oh, don't worry about the difficulty, it's like everything. At first it will be a bit difficult and then we will improve."

# game/Code/Interacciones/miCasa/HABPADR/mad/miercoles/madHabPadrEnPijama4taInteracMiercoles_visit2.rpy:181
translate english hablaLibroYogaSofia_357a04a1:

    # mad "{size=14} Pero...{/size}"
    mad "{size=14}But...{/size}"

# game/Code/Interacciones/miCasa/HABPADR/mad/miercoles/madHabPadrEnPijama4taInteracMiercoles_visit2.rpy:183
translate english hablaLibroYogaSofia_d2ba1b91:

    # prota "Estoy muy contento de que podamos hacer ejercicio juntos."
    prota "I'm so glad we can exercise together."

# game/Code/Interacciones/miCasa/HABPADR/mad/miercoles/madHabPadrEnPijama4taInteracMiercoles_visit2.rpy:185
translate english hablaLibroYogaSofia_e458dfee:

    # prota "Gracias [nombreMad3]. Me voy a trabajar."
    prota "Thank you, [nombreMad3]. I'm going to work."

translate english strings:

    # game/Code/Interacciones/miCasa/HABPADR/mad/miercoles/madHabPadrEnPijama4taInteracMiercoles_visit2.rpy:15
    old "Hablar del libro de yoga"
    new "Talk about the yoga book"
