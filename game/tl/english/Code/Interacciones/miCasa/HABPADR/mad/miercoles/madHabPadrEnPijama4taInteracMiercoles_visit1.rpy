﻿# TODO: Translation updated at 2021-07-08 13:36

# game/Code/Interacciones/miCasa/HABPADR/mad/miercoles/madHabPadrEnPijama4taInteracMiercoles_visit1.rpy:5
translate english madHabPadrEnPijama4taInteracMiercoles_visit1_db5500aa:

    # prota "Hola"
    prota "Hello"

# game/Code/Interacciones/miCasa/HABPADR/mad/miercoles/madHabPadrEnPijama4taInteracMiercoles_visit1.rpy:12
translate english madHabPadrEnPijama4taInteracMiercoles_visit1_324c6e7a:

    # protaPensa "{size=15}{i}Dios mio!! Se estaba poniendo el pijama. {/size}{/i}"
    protaPensa "{size=15}{i}My God!!! He was putting on his pajamas. {/size}{/i}"

# game/Code/Interacciones/miCasa/HABPADR/mad/miercoles/madHabPadrEnPijama4taInteracMiercoles_visit1.rpy:14
translate english madHabPadrEnPijama4taInteracMiercoles_visit1_f6bad134:

    # mad "Deberías llamar a la puerta."
    mad "You should knock on the door"

# game/Code/Interacciones/miCasa/HABPADR/mad/miercoles/madHabPadrEnPijama4taInteracMiercoles_visit1.rpy:16
translate english madHabPadrEnPijama4taInteracMiercoles_visit1_712cf489:

    # mad "Me estaba cambiando de ropa..."
    mad "You should knock on the door"

# game/Code/Interacciones/miCasa/HABPADR/mad/miercoles/madHabPadrEnPijama4taInteracMiercoles_visit1.rpy:18
translate english madHabPadrEnPijama4taInteracMiercoles_visit1_8e4037af:

    # prota "Pensé que estaba abierta, no me di cuenta."
    prota "I thought it was open, I didn't notice."

# game/Code/Interacciones/miCasa/HABPADR/mad/miercoles/madHabPadrEnPijama4taInteracMiercoles_visit1.rpy:21
translate english madHabPadrEnPijama4taInteracMiercoles_visit1_b190b190:

    # mad "Esta bien, no pasa nada. Pero fijate para la proxima vez."
    mad "It's okay, it's okay. But be careful next time."

# game/Code/Interacciones/miCasa/HABPADR/mad/miercoles/madHabPadrEnPijama4taInteracMiercoles_visit1.rpy:28
translate english madHabPadrEnPijama4taInteracMiercoles_visit1_1575d316:

    # prota "Te queda muy bien este conjunto de noche"
    prota "You look great in this evening outfit."

# game/Code/Interacciones/miCasa/HABPADR/mad/miercoles/madHabPadrEnPijama4taInteracMiercoles_visit1.rpy:38
translate english madHabPadrEnPijama4taInteracMiercoles_visit1_e4400b29:

    # prota "Adios [nombreMad2]"
    prota "Goodbye [nombreMad2]"
