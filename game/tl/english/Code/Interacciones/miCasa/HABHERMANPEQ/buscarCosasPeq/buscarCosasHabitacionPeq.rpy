﻿# TODO: Translation updated at 2022-02-07 21:19

# game/Code/Interacciones/miCasa/HABHERMANPEQ/buscarCosasPeq/buscarCosasHabitacionPeq.rpy:5
translate english buscarCosasHabitacionPeq_1b188b46:

    # protaPensa "No puedo mirar los cajones en presencia de Dana."
    protaPensa "I can't look in the drawers when Dana's around."#I can't look at the drawers in Dana's presence.

# game/Code/Interacciones/miCasa/HABHERMANPEQ/buscarCosasPeq/buscarCosasHabitacionPeq.rpy:22
translate english verDiarioPersonalPeq_773a8748:

    # protaPensa "Aquí esta."
    protaPensa "Here it is."

translate english strings:

    # game/Code/Interacciones/miCasa/HABHERMANPEQ/buscarCosasPeq/buscarCosasHabitacionPeq.rpy:8
    old "Buscar diario personal"
    new "Search for personal diary"#Search personal diary
