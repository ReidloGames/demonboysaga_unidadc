﻿# TODO: Translation updated at 2021-08-06 14:00

# game/Code/Interacciones/miCasa/HABHERMANPEQ/peq/domingo/peqSuHabitacionSentadaCama1raInteracDomingo_visit1.rpy:9
translate english peqSuHabitacionSentadaCama1raInteracDomingo_visit1_769ce4d3:

    # prota "¡¡Buenos días!!"
    prota "Good morning!!!"

# game/Code/Interacciones/miCasa/HABHERMANPEQ/peq/domingo/peqSuHabitacionSentadaCama1raInteracDomingo_visit1.rpy:13
translate english peqSuHabitacionSentadaCama1raInteracDomingo_visit1_9c3184c6:

    # hermanPeq "¡¡Tengo sueño!!"
    hermanPeq "I'm sleepy!!!"

# game/Code/Interacciones/miCasa/HABHERMANPEQ/peq/domingo/peqSuHabitacionSentadaCama1raInteracDomingo_visit1.rpy:15
translate english peqSuHabitacionSentadaCama1raInteracDomingo_visit1_215d1d4d:

    # hermanPeq "Tengo muchas tareas que hacer para la Universidad."
    hermanPeq "I have a lot of homework to do for college."

# game/Code/Interacciones/miCasa/HABHERMANPEQ/peq/domingo/peqSuHabitacionSentadaCama1raInteracDomingo_visit1.rpy:17
translate english peqSuHabitacionSentadaCama1raInteracDomingo_visit1_e14eef35:

    # prota "Está bien, entonces no te duermas."
    prota "Okay, then don't go to sleep."

# game/Code/Interacciones/miCasa/HABHERMANPEQ/peq/domingo/peqSuHabitacionSentadaCama1raInteracDomingo_visit1.rpy:19
translate english peqSuHabitacionSentadaCama1raInteracDomingo_visit1_06dca753:

    # prota "No quiero que [nombreMad3] me regañe por entretenerte."
    prota "I don't want [nombreMad3] to scold me for holding you up."

# game/Code/Interacciones/miCasa/HABHERMANPEQ/peq/domingo/peqSuHabitacionSentadaCama1raInteracDomingo_visit1.rpy:21
translate english peqSuHabitacionSentadaCama1raInteracDomingo_visit1_603c339c:

    # hermanPeqPensa "Que pereza..."
    hermanPeqPensa "What laziness..."
# TODO: Translation updated at 2021-12-12 13:06

# game/Code/Interacciones/miCasa/HABHERMANPEQ/peq/domingo/peqSuHabitacionSentadaCama1raInteracDomingo_visit1.rpy:20
translate english peqSuHabitacionSentadaCama1raInteracDomingo_visit1_0861b174:

    # prota "Tengo un regalo para ti"
    prota "I've got a present for you."

# game/Code/Interacciones/miCasa/HABHERMANPEQ/peq/domingo/peqSuHabitacionSentadaCama1raInteracDomingo_visit1.rpy:22
translate english peqSuHabitacionSentadaCama1raInteracDomingo_visit1_5bb07d7c:

    # hermanPeq "¿De verdad?"
    hermanPeq "Really?"

# game/Code/Interacciones/miCasa/HABHERMANPEQ/peq/domingo/peqSuHabitacionSentadaCama1raInteracDomingo_visit1.rpy:24
translate english peqSuHabitacionSentadaCama1raInteracDomingo_visit1_a99212c0:

    # hermanPeq "Ohhh el chocolate que más me gusta. ¡Muchas gracias!"
    hermanPeq "Ohhh the chocolate I like the most. thank you so much!"

# game/Code/Interacciones/miCasa/HABHERMANPEQ/peq/domingo/peqSuHabitacionSentadaCama1raInteracDomingo_visit1.rpy:26
translate english peqSuHabitacionSentadaCama1raInteracDomingo_visit1_33a1c2fe:

    # prota "Para que te animes un poco."
    prota "To cheer you up a little."

# game/Code/Interacciones/miCasa/HABHERMANPEQ/peq/domingo/peqSuHabitacionSentadaCama1raInteracDomingo_visit1.rpy:38
translate english peqSuHabitacionSentadaCama1raInteracDomingo_visit1_88f2b13b:

    # "¿Como podrías animarla? tal vez con un regalo..."
    "How could you cheer her up? Maybe with a gift..."

# game/Code/Interacciones/miCasa/HABHERMANPEQ/peq/domingo/peqSuHabitacionSentadaCama1raInteracDomingo_visit1.rpy:52
translate english peqNoSeAnimaEnCama_9c3184c6:

    # hermanPeq "¡¡Tengo sueño!!"
    hermanPeq "I'm sleepy!!!"

# game/Code/Interacciones/miCasa/HABHERMANPEQ/peq/domingo/peqSuHabitacionSentadaCama1raInteracDomingo_visit1.rpy:54
translate english peqNoSeAnimaEnCama_215d1d4d:

    # hermanPeq "Tengo muchas tareas que hacer para la Universidad."
    hermanPeq "I have a lot of homework to do for college."

# game/Code/Interacciones/miCasa/HABHERMANPEQ/peq/domingo/peqSuHabitacionSentadaCama1raInteracDomingo_visit1.rpy:56
translate english peqNoSeAnimaEnCama_e14eef35:

    # prota "Está bien, entonces no te duermas."
    prota "Okay, then don't go to sleep."

# game/Code/Interacciones/miCasa/HABHERMANPEQ/peq/domingo/peqSuHabitacionSentadaCama1raInteracDomingo_visit1.rpy:58
translate english peqNoSeAnimaEnCama_06dca753:

    # prota "No quiero que [nombreMad3] me regañe por entretenerte."
    prota "I don't want [nombreMad3] to scold me for entertaining you."

# game/Code/Interacciones/miCasa/HABHERMANPEQ/peq/domingo/peqSuHabitacionSentadaCama1raInteracDomingo_visit1.rpy:60
translate english peqNoSeAnimaEnCama_603c339c:

    # hermanPeqPensa "Que pereza..."
    hermanPeqPensa "How lazy..."

translate english strings:

    # game/Code/Interacciones/miCasa/HABHERMANPEQ/peq/domingo/peqSuHabitacionSentadaCama1raInteracDomingo_visit1.rpy:15
    old "Decirle que se anime"
    new "Tell her to cheer up."
