﻿# TODO: Translation updated at 2021-08-06 14:00

# game/Code/Interacciones/miCasa/HABHERMANPEQ/peq/martes/peqSuHabitacionMiraPc1raInteracMartes_visit1.rpy:7
translate english peqSuHabitacionMiraPc1raInteracMartes_visit1_1b80aacb:

    # prota "¡Hey!! aún en pijama?"
    prota "Hey!! still in pajamas?"

# game/Code/Interacciones/miCasa/HABHERMANPEQ/peq/martes/peqSuHabitacionMiraPc1raInteracMartes_visit1.rpy:11
translate english peqSuHabitacionMiraPc1raInteracMartes_visit1_c3aea9d8:

    # hermanPeq "Si, tengo que hacer unos trabajos de clase. No puedo hablar ahora."
    hermanPeq "Yeah, I have some classwork to do. I can't talk right now."

# game/Code/Interacciones/miCasa/HABHERMANPEQ/peq/martes/peqSuHabitacionMiraPc1raInteracMartes_visit1.rpy:15
translate english peqSuHabitacionMiraPc1raInteracMartes_visit1_d18915f5:

    # prota "Esta bien. Te dejo trabajar."
    prota "That's okay. I'll let you work."

# game/Code/Interacciones/miCasa/HABHERMANPEQ/peq/martes/peqSuHabitacionMiraPc1raInteracMartes_visit1.rpy:17
translate english peqSuHabitacionMiraPc1raInteracMartes_visit1_03e8c623:

    # prota "Adiós"
    prota "Bye."

# game/Code/Interacciones/miCasa/HABHERMANPEQ/peq/martes/peqSuHabitacionMiraPc1raInteracMartes_visit1.rpy:19
translate english peqSuHabitacionMiraPc1raInteracMartes_visit1_b110eab5:

    # hermanPeq "Gracias,! adiós!"
    hermanPeq "Thank you,! bye!"
