﻿# TODO: Translation updated at 2021-10-13 15:38

# game/Code/Interacciones/escuela/Clase/aliciaEnClaseInterac_visit1.rpy:15
translate english aliciaEnClaseInterac_visit1_06d28153:

    # protaPensa "¿Que estará mirando en el teléfono que se ha puesto tan contenta?"
    protaPensa "What is she looking at on the phone that made her so happy?"
# TODO: Translation updated at 2021-12-12 13:06

# game/Code/Interacciones/escuela/Clase/aliciaEnClaseInterac_visit1.rpy:35
translate english protaPiensaCrearDistracionAlicia_c19d7ad5:

    # protaPensa "Necesito acceder a ese teléfono"
    protaPensa "I need access to that phone."

# game/Code/Interacciones/escuela/Clase/aliciaEnClaseInterac_visit1.rpy:37
translate english protaPiensaCrearDistracionAlicia_4662c2a1:

    # protaPensa "Podría crear una distracción..."
    protaPensa "I could create a distraction..."

# game/Code/Interacciones/escuela/Clase/aliciaEnClaseInterac_visit1.rpy:44
translate english protaIntentaObtenerCodigoMovilAlicia_00263d26:

    # protaPensa "Tengo que obtener el código..."
    protaPensa "I need to get the code..."

# game/Code/Interacciones/escuela/Clase/aliciaEnClaseInterac_visit1.rpy:56
translate english protaIntentaObtenerCodigoMovilAlicia_8a147daa:

    # protaPensa "¡Genial! Ya tengo un número..."
    protaPensa "Great! I already have a number..."

# game/Code/Interacciones/escuela/Clase/aliciaEnClaseInterac_visit1.rpy:57
translate english protaIntentaObtenerCodigoMovilAlicia_bc22fd0e:

    # protaPensa "Me voy antes de que sospeche de mi."
    protaPensa "I'm leaving before he suspects me."

# game/Code/Interacciones/escuela/Clase/aliciaEnClaseInterac_visit1.rpy:60
translate english protaIntentaObtenerCodigoMovilAlicia_dc0cbd60:

    # protaPensa "¡Ya tengo el código!"
    protaPensa "I already have the code!"

# game/Code/Interacciones/escuela/Clase/aliciaEnClaseInterac_visit1.rpy:64
translate english protaIntentaObtenerCodigoMovilAlicia_cc60678b:

    # alicia "Que estás haciendo ¿tonterías como siempre?"
    alicia "What are you doing, fooling around as usual?"

# game/Code/Interacciones/escuela/Clase/aliciaEnClaseInterac_visit1.rpy:66
translate english protaIntentaObtenerCodigoMovilAlicia_3a2a05ae:

    # prota "Oh, el zapato lo tengo desabrochado..."
    prota "Oh, my shoe is unbuttoned..."

# game/Code/Interacciones/escuela/Clase/aliciaEnClaseInterac_visit1.rpy:68
translate english protaIntentaObtenerCodigoMovilAlicia_9ec9846e:

    # protaPensa "Me voy antes de que sospeche de mí."
    protaPensa "I'm leaving before he suspects me."

translate english strings:

    # game/Code/Interacciones/escuela/Clase/aliciaEnClaseInterac_visit1.rpy:20
    old "Intentar obtener el código de acceso"
    new "Try to get the access code."
