﻿# TODO: Translation updated at 2021-08-27 11:11

# game/Code/Interacciones/escuela/Clase/hacerClase.rpy:6
translate english hacerClase_f7e665fd:

    # protaPensa "Las clases empiezan por la mañana"
    protaPensa "Classes start in the morning."

# game/Code/Interacciones/escuela/Clase/hacerClase.rpy:14
translate english hacerClase_4d27770f:

    # protaPensa "Por hoy, se han terminado las clases"
    protaPensa "For today, classes are over."

# game/Code/Interacciones/escuela/Clase/hacerClase.rpy:23
translate english hacerClaseAlicia_b0e6c0e3:

    # alicia "Chicos y chicas empieza la clase, ir sacando los libros"
    alicia "Boys and girls, class is starting, get your books out."

# game/Code/Interacciones/escuela/Clase/hacerClase.rpy:25
translate english hacerClaseAlicia_5ea885d6:

    # alicia "........."
    alicia "........."

# game/Code/Interacciones/escuela/Clase/hacerClase.rpy:27
translate english hacerClaseAlicia_850ad35a:

    # alicia "Bien, por hoy hemos terminado la clase."
    alicia "Okay, we're done with class for today."
