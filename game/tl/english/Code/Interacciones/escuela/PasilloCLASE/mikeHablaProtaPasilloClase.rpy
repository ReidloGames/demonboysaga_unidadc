﻿# TODO: Translation updated at 2021-12-12 13:06

# game/Code/Interacciones/escuela/PasilloCLASE/mikeHablaProtaPasilloClase.rpy:4
translate english mikeHablaProtaPasilloClase_6e8c1d1b:

    # prota "¡Ey Mike!"
    prota "Hey Mike!"

# game/Code/Interacciones/escuela/PasilloCLASE/mikeHablaProtaPasilloClase.rpy:22
translate english hacerTareasDeCLaseJuntosMike_e7edcad7:

    # prota "Tenemos muchos trabajos de clase. ¿Qué te parece si hacemos juntos las tareas?"
    prota "We've got a lot of classwork. How about we do the homework together?"

# game/Code/Interacciones/escuela/PasilloCLASE/mikeHablaProtaPasilloClase.rpy:24
translate english hacerTareasDeCLaseJuntosMike_f25f287d:

    # mike "¡Por mi perfecto! al menos será más divertido."
    mike "Fine with me! At least it'll be more fun."

# game/Code/Interacciones/escuela/PasilloCLASE/mikeHablaProtaPasilloClase.rpy:26
translate english hacerTareasDeCLaseJuntosMike_9229bc38:

    # mike "¿Vamos a mi casa? allí estaremos tranquilos y si sobra un poco de tiempo podríamos jugar al Call of tuty."
    mike "Let's go to my house? we'll be quiet there and if there's a little time left over we could play Call of tuty."

# game/Code/Interacciones/escuela/PasilloCLASE/mikeHablaProtaPasilloClase.rpy:28
translate english hacerTareasDeCLaseJuntosMike_bf315081:

    # prota "¡Me parece perfecto!"
    prota "Sounds perfect to me!"

# game/Code/Interacciones/escuela/PasilloCLASE/mikeHablaProtaPasilloClase.rpy:32
translate english hacerTareasDeCLaseJuntosMike_5cd897de:

    # "Unos minutos más tarde..."
    "A few minutes later..."

# game/Code/Interacciones/escuela/PasilloCLASE/mikeHablaProtaPasilloClase.rpy:51
translate english hacerTareasDeCLaseJuntosMikeVez1_152b20b9:

    # prota "Esto es muy aburrido..."
    prota "This is so boring..."

# game/Code/Interacciones/escuela/PasilloCLASE/mikeHablaProtaPasilloClase.rpy:53
translate english hacerTareasDeCLaseJuntosMikeVez1_1e952e52:

    # mike "Un poco más a ver si podemos acabar los trabajos."
    mike "A little more time to see if we can finish our work."

# game/Code/Interacciones/escuela/PasilloCLASE/mikeHablaProtaPasilloClase.rpy:55
translate english hacerTareasDeCLaseJuntosMikeVez1_156c924d:

    # prota "Que remedio..."
    prota "What a drag..."

# game/Code/Interacciones/escuela/PasilloCLASE/mikeHablaProtaPasilloClase.rpy:59
translate english hacerTareasDeCLaseJuntosMikeVez1_101bb56d:

    # prota "Se ha hecho tarde y tengo que irme. ¡Nos vemos y gracias por todo!"
    prota "It's getting late and I have to go. See you and thanks for everything!"

# game/Code/Interacciones/escuela/PasilloCLASE/mikeHablaProtaPasilloClase.rpy:60
translate english hacerTareasDeCLaseJuntosMikeVez1_01395fdb:

    # mike "Una lástima, jugaremos en otra ocasión. ¡Hasta mañana!"
    mike "Too bad, we'll play another time. See you tomorrow!"

# game/Code/Interacciones/escuela/PasilloCLASE/mikeHablaProtaPasilloClase.rpy:68
translate english hacerTareasDeCLaseJuntosMikeVez2_aaac3589:

    # prota "Necesito ir al baño."
    prota "I need to go to the bathroom."

# game/Code/Interacciones/escuela/PasilloCLASE/mikeHablaProtaPasilloClase.rpy:69
translate english hacerTareasDeCLaseJuntosMikeVez2_ae859abc:

    # mike "Claro, ya sabes arriba la última puerta."
    mike "Sure, you know up the last door."

# game/Code/Interacciones/escuela/PasilloCLASE/mikeHablaProtaPasilloClase.rpy:75
translate english hacerTareasDeCLaseJuntosMikeVez2_77bc6607:

    # protaPensa "Esa puerta del fondo es el baño."
    protaPensa "That door at the back is the bathroom."

# game/Code/Interacciones/escuela/PasilloCLASE/mikeHablaProtaPasilloClase.rpy:77
translate english hacerTareasDeCLaseJuntosMikeVez2_60ab5a6e:

    # protaPensa "La primera puerta es la habitación de Lucia. Nunca entre en ella y la del medio es la de Mike."
    protaPensa "The first door is Lucia's room. Never enter it and the middle one is Mike's."

# game/Code/Interacciones/escuela/PasilloCLASE/mikeHablaProtaPasilloClase.rpy:81
translate english hacerTareasDeCLaseJuntosMikeVez2_e919ccdc:

    # "Mear..."
    "Piss..."

# game/Code/Interacciones/escuela/PasilloCLASE/mikeHablaProtaPasilloClase.rpy:83
translate english hacerTareasDeCLaseJuntosMikeVez2_101bb56d:

    # prota "Se ha hecho tarde y tengo que irme. ¡Nos vemos y gracias por todo!"
    prota "It's getting late and I have to go. See you and thanks for everything!"

# game/Code/Interacciones/escuela/PasilloCLASE/mikeHablaProtaPasilloClase.rpy:84
translate english hacerTareasDeCLaseJuntosMikeVez2_01395fdb:

    # mike "Una lástima, jugaremos en otra ocasión. ¡Hasta mañana!"
    mike "Too bad, we'll play another time. See you tomorrow!"

# game/Code/Interacciones/escuela/PasilloCLASE/mikeHablaProtaPasilloClase.rpy:92
translate english hacerTareasDeCLaseJuntosMikeVez3_aaac3589:

    # prota "Necesito ir al baño."
    prota "I need to go to the bathroom."

# game/Code/Interacciones/escuela/PasilloCLASE/mikeHablaProtaPasilloClase.rpy:100
translate english hacerTareasDeCLaseJuntosMikeVez3_7a695746:

    # protaPensa "Lucia no está en casa..."
    protaPensa "Lucia's not home..."

# game/Code/Interacciones/escuela/PasilloCLASE/mikeHablaProtaPasilloClase.rpy:104
translate english hacerTareasDeCLaseJuntosMikeVez3_cfdc5d79:

    # protaPensa "Tengo curiosidad, un vistazo rápido..."
    protaPensa "Lucia's not home..."

# game/Code/Interacciones/escuela/PasilloCLASE/mikeHablaProtaPasilloClase.rpy:106
translate english hacerTareasDeCLaseJuntosMikeVez3_01ada206:

    # protaPensa "Esta es la habitación de Lucia..."
    protaPensa "This is Lucia's room..."

# game/Code/Interacciones/escuela/PasilloCLASE/mikeHablaProtaPasilloClase.rpy:108
translate english hacerTareasDeCLaseJuntosMikeVez3_6a0745ea:

    # protaPensa "Tiene una gran cama, y un gran ventanal."
    protaPensa "It has a big bed, and a big window."

# game/Code/Interacciones/escuela/PasilloCLASE/mikeHablaProtaPasilloClase.rpy:112
translate english hacerTareasDeCLaseJuntosMikeVez3_265f6392:

    # protaPensa "¡No me lo puedo creer! Se ve muy lejos, pero desde aquí puedo ver mi casa y la ventana de mi habitación."
    protaPensa "I can't believe it! It looks so far away, but from here I can see my house and my bedroom window."

# game/Code/Interacciones/escuela/PasilloCLASE/mikeHablaProtaPasilloClase.rpy:114
translate english hacerTareasDeCLaseJuntosMikeVez3_6c537da2:

    # protaPensa "Se me está ocurriendo una gran idea... Me voy antes de que me vean."
    protaPensa "I'm coming up with a great idea.... I'm leaving before they see me."

# game/Code/Interacciones/escuela/PasilloCLASE/mikeHablaProtaPasilloClase.rpy:116
translate english hacerTareasDeCLaseJuntosMikeVez3_a20cefa7:

    # "..."
    "..."

# game/Code/Interacciones/escuela/PasilloCLASE/mikeHablaProtaPasilloClase.rpy:119
translate english hacerTareasDeCLaseJuntosMikeVez3_101bb56d:

    # prota "Se ha hecho tarde y tengo que irme. ¡Nos vemos y gracias por todo!"
    prota "It's getting late and I have to go. see you and thanks for everything!"

# game/Code/Interacciones/escuela/PasilloCLASE/mikeHablaProtaPasilloClase.rpy:120
translate english hacerTareasDeCLaseJuntosMikeVez3_01395fdb:

    # mike "Una lástima, jugaremos en otra ocasión. ¡Hasta mañana!"
    mike "Too bad, we'll play another time. see you tomorrow!"

translate english strings:

    # game/Code/Interacciones/escuela/PasilloCLASE/mikeHablaProtaPasilloClase.rpy:6
    old "Hacer tareas de clase juntos"
    new "Doing homework together"

    # game/Code/Interacciones/escuela/PasilloCLASE/mikeHablaProtaPasilloClase.rpy:101
    old "Entrar en la habitación de Lucia"
    new "Going into Lucia's room"
