﻿# TODO: Translation updated at 2021-12-12 13:06

# game/Code/Interacciones/escuela/EntradaEscuela1/peqPasilloEscuelaSentadaHablaProta.rpy:5
translate english peqPasilloEscuelaSentadaHablaProta_f217c7f5:

    # prota "¡Ey Dana!"
    prota "Hey Dana!"

# game/Code/Interacciones/escuela/EntradaEscuela1/peqPasilloEscuelaSentadaHablaProta.rpy:7
translate english peqPasilloEscuelaSentadaHablaProta_c65a4fdb:

    # prota "¿Qué haces ahí?"
    prota "What are you doing there?"

# game/Code/Interacciones/escuela/EntradaEscuela1/peqPasilloEscuelaSentadaHablaProta.rpy:9
translate english peqPasilloEscuelaSentadaHablaProta_a4723b86:

    # hermanPeq "Hola [nombreProta2]"
    hermanPeq "Hi [nombreProta2]"

# game/Code/Interacciones/escuela/EntradaEscuela1/peqPasilloEscuelaSentadaHablaProta.rpy:11
translate english peqPasilloEscuelaSentadaHablaProta_fec2903a:

    # hermanPeq "Acabo de entregar unas tareas importantes de clase y me estaba dando un respiro."
    hermanPeq "I just turned in some important class assignments and was giving myself a break."

# game/Code/Interacciones/escuela/EntradaEscuela1/peqPasilloEscuelaSentadaHablaProta.rpy:13
translate english peqPasilloEscuelaSentadaHablaProta_fd436267:

    # hermanPeq "Esos trabajos me estaban estresando mucho. Me quitado un peso de encima."
    hermanPeq "Those assignments were really stressing me out. It's a weight off my shoulders."

# game/Code/Interacciones/escuela/EntradaEscuela1/peqPasilloEscuelaSentadaHablaProta.rpy:15
translate english peqPasilloEscuelaSentadaHablaProta_d7f719fd:

    # prota "Me alegro mucho. Ahora tendras mas tiempo para hacer otras cosas. Siempre estas con trabajos de la universidad..."
    prota "I'm so glad. Now you'll have more time to do other things. You're always busy with college work..."

# game/Code/Interacciones/escuela/EntradaEscuela1/peqPasilloEscuelaSentadaHablaProta.rpy:17
translate english peqPasilloEscuelaSentadaHablaProta_3cca115b:

    # hermanPeq "¡Si! Voy tirando a clase o se me hará tarde."
    hermanPeq "Yes! I'm going to get to class or I'll be late."

# game/Code/Interacciones/escuela/EntradaEscuela1/peqPasilloEscuelaSentadaHablaProta.rpy:19
translate english peqPasilloEscuelaSentadaHablaProta_ec74082b:

    # prota "Nos vemos luego."
    prota "See you later."
