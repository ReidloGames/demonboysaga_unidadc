﻿# TODO: Translation updated at 2021-08-27 11:11

# game/Code/Interacciones/escuela/PasilloWC/luciaPasilloWcLimpiaInterac_visit1.rpy:11
translate english luciaPasilloWcLimpiaInterac_visit1_29caa844:

    # prota "Buenos días Lucia."
    prota "Good morning Lucia."

# game/Code/Interacciones/escuela/PasilloWC/luciaPasilloWcLimpiaInterac_visit1.rpy:13
translate english luciaPasilloWcLimpiaInterac_visit1_ca621e23:

    # lucia "Buenos días cielo."
    lucia "Good morning sweetheart."

# game/Code/Interacciones/escuela/PasilloWC/luciaPasilloWcLimpiaInterac_visit1.rpy:15
translate english luciaPasilloWcLimpiaInterac_visit1_36ce8ec3:

    # lucia "Si ves a mi Mike dale un beso de mi parte."
    lucia "If you see my Mike give him a kiss for me."

# game/Code/Interacciones/escuela/PasilloWC/luciaPasilloWcLimpiaInterac_visit1.rpy:17
translate english luciaPasilloWcLimpiaInterac_visit1_9cef61af:

    # lucia "Nunca lo veo por la escuela."
    lucia "I never see him around school."

# game/Code/Interacciones/escuela/PasilloWC/luciaPasilloWcLimpiaInterac_visit1.rpy:19
translate english luciaPasilloWcLimpiaInterac_visit1_df32d775:

    # prota "Claro, sin problema."
    prota "Sure, no problem."
