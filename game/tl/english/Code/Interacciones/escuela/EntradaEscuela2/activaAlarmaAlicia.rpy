﻿# TODO: Translation updated at 2021-12-12 13:06

# game/Code/Interacciones/escuela/EntradaEscuela2/activaAlarmaAlicia.rpy:7
translate english activaAlarmaAlicia_91e26f29:

    # protaPensa "Si activo la alarma la gente tendrá que salir y con un poco de suerte el móvil de Alicia estará en la mesa"
    protaPensa "If I activate the alarm people will have to leave and with any luck Alicia's cell phone will be on the table."

# game/Code/Interacciones/escuela/EntradaEscuela2/activaAlarmaAlicia.rpy:17
translate english activaAlarmaAlicia_6dfd6ee7:

    # protaPensa "El plan está saliendo a las mil maravillas..."
    protaPensa "The plan is going swimmingly..."

# game/Code/Interacciones/escuela/EntradaEscuela2/activaAlarmaAlicia.rpy:19
translate english activaAlarmaAlicia_0623552b:

    # protaPensa "Es un plan perfecto sin fisuras..."
    protaPensa "It's a seamless perfect plan..."

# game/Code/Interacciones/escuela/EntradaEscuela2/activaAlarmaAlicia.rpy:26
translate english activaAlarmaAlicia_25373de3:

    # jack "¡¡Tu, tonto!!"
    jack "You fool!!!"

# game/Code/Interacciones/escuela/EntradaEscuela2/activaAlarmaAlicia.rpy:28
translate english activaAlarmaAlicia_1ed07293:

    # jack "¿A caso te quieres quemar como un pollo?"
    jack "Do you want to burn like a chicken?"

# game/Code/Interacciones/escuela/EntradaEscuela2/activaAlarmaAlicia.rpy:30
translate english activaAlarmaAlicia_42336694:

    # protaPensa "Otra vez el retrasado este. Parece que nació y se golpeó la cabeza."
    protaPensa "There's that retard again. Looks like he was born and hit his head."

# game/Code/Interacciones/escuela/EntradaEscuela2/activaAlarmaAlicia.rpy:32
translate english activaAlarmaAlicia_e3e8a3f9:

    # jack "¡Sal de aquí!"
    jack "Get out of here!"

# game/Code/Interacciones/escuela/EntradaEscuela2/activaAlarmaAlicia.rpy:37
translate english activaAlarmaAlicia_6b69bb46:

    # protaPensa "¡Ahí está el teléfono!"
    protaPensa "There's the phone!"

# game/Code/Interacciones/escuela/EntradaEscuela2/activaAlarmaAlicia.rpy:39
translate english activaAlarmaAlicia_d99a9ba7:

    # protaPensa "A ver que tenemos aquí..."
    protaPensa "Let's see what we have here..."

# game/Code/Interacciones/escuela/EntradaEscuela2/activaAlarmaAlicia.rpy:41
translate english activaAlarmaAlicia_cda11882:

    # protaPensa "Maldición está bloqueado! Tengo que averiguar el código de acceso..."
    protaPensa "Damn it's locked! I have to find out the access code..."
