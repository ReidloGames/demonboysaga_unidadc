﻿# TODO: Translation updated at 2021-07-08 13:36

translate english strings:

    # game/Code/hud/functionsDay.rpy:47
    old "Lunes"
    new "Monday"

    # game/Code/hud/functionsDay.rpy:50
    old "Martes"
    new "Tuesday"

    # game/Code/hud/functionsDay.rpy:53
    old "Miercoles"
    new "Wednesday"

    # game/Code/hud/functionsDay.rpy:56
    old "Jueves"
    new "Thursday"

    # game/Code/hud/functionsDay.rpy:59
    old "Viernes"
    new "Friday"

    # game/Code/hud/functionsDay.rpy:62
    old "Sabado"
    new "Saturday"

    # game/Code/hud/functionsDay.rpy:65
    old "Domingo"
    new "Sunday"

    # game/Code/hud/functionsDay.rpy:79
    old "Amanecer"
    new "Sunrise"

    # game/Code/hud/functionsDay.rpy:86
    old "Mañana"
    new "Morning"

    # game/Code/hud/functionsDay.rpy:93
    old "Mediodia"
    new "Midday"

    # game/Code/hud/functionsDay.rpy:100
    old "Tarde"
    new "Afternoon"

    # game/Code/hud/functionsDay.rpy:107
    old "Noche"
    new "Night"

    # game/Code/hud/functionsDay.rpy:133
    old "Es tarde, hay que ir a la cama"
    new "It's late, it's time to go to bed"
# TODO: Translation updated at 2021-07-20 13:59

translate english strings:

    # game/Code/hud/functionsDay.rpy:124
    old "[tiempoDiaStr]"
    new "[tiempoDiaStr]"
# TODO: Translation updated at 2021-08-27 11:11

translate english strings:

    # game/Code/hud/functionsDay.rpy:174
    old "No es momento de hacer el vago"
    new "This is no time to be lazy"
