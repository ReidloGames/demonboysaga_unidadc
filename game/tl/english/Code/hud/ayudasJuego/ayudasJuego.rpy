﻿# TODO: Translation updated at 2021-08-29 22:07

translate english strings:

    # game/Code/hud/ayudasJuego/ayudasJuego.rpy:25
    old "{color=#000000}{size=12}{b}[nombrePad]:{/b}{/size}{size=11} Saludalo antes de ir a la Universidad{/size}{/color}"
    new "{color=#000000}{size=12}{b}[nombrePad]:{/b}{/size}{size=11} Greet him before going to college{/size}{/color}"

# TODO: Translation updated at 2021-08-29 22:08

translate english strings:

    # game/Code/hud/ayudasJuego/ayudasJuego.rpy:43
    old "{color=#000000}{size=12}{b}[nombreMad]:{/b}{/size}{size=11} Habla con ella en la cocina, el lunes al mediodia{/size}{/color}"
    new "{color=#000000}{size=12}{b}[nombreMad]:{/b}{/size}{size=11} Talk to her in the kitchen, Monday at noon.{/size}{/color}"

    # game/Code/hud/ayudasJuego/ayudasJuego.rpy:45
    old "{color=#000000}{size=12}{b}[nombreMad]:{/b}{/size}{size=11} Habla con [nombreMad3] y con [nombrePad3] por la noche en su habitación sobre el problema del pago de la Universidad{/size}{/color}"
    new "{color=#000000}{size=12}{b}[nombreMad]:{/b}{/size}{size=11} Talk to [nombreMad3] and [nombrePad3] at night in their room about the University payment problem.{/size}{/color}"

    # game/Code/hud/ayudasJuego/ayudasJuego.rpy:60
    old "{color=#000000}{size=12}{b}[nombrePeq]:{/b}{/size}{size=11} Visitala en la sala del gimnasio de casa, el lunes a tercera hora.{/size}{/color}"
    new "{color=#000000}{size=12}{b}[nombrePeq]:{/b}{/size}{size=11} Visit her in the gym room at home, on Monday at the third hour.{/size}{/color}"

    # game/Code/hud/ayudasJuego/ayudasJuego.rpy:75
    old "{color=#000000}{size=12}{b}[nombreMed]:{/b}{/size}{size=11} Visitala en el comedor, el miercoles a tercera hora.{/size}{/color}"
    new "{color=#000000}{size=12}{b}[nombreMed]:{/b}{/size}{size=11} Visit her in the dining room on Wednesday at the third hour.{/size}{/color}"

    # game/Code/hud/ayudasJuego/ayudasJuego.rpy:90
    old "{color=#000000}{size=12}{b}[nombreMay]:{/b}{/size}{size=11} Visitala en su habitación el martes a primera hora.{/size}{/color}"
    new "{color=#000000}{size=12}{b}[nombreMay]:{/b}{/size}{size=11} Visit her in her room first thing Tuesday morning.{/size}{/color}"

    # game/Code/hud/ayudasJuego/ayudasJuego.rpy:102
    old "{color=#000000}{size=11}{b}[nombreLucia]:{/b} [ayudaMad1] {/size}{/color}"
    new ""

    # game/Code/hud/ayudasJuego/ayudasJuego.rpy:118
    old "{color=#000000}{size=11}{b}[nombreClaire]:{/b} [ayudaMad1] {/size}{/color}"
    new ""

    # game/Code/hud/ayudasJuego/ayudasJuego.rpy:133
    old "{color=#000000}{size=12}{b}[nombreAlicia]:{/b}{/size}{size=11} No llegues tarde a clase.{/size}{/color}"
    new "{color=#000000}{size=12}{b}[nombreAlicia]:{/b}{/size}{size=11} Don't be late for class.{/size}{/color}"

    # game/Code/hud/ayudasJuego/ayudasJuego.rpy:136
    old "{color=#000000}{size=12}{b}[nombreAlicia]:{/b}{/size}{size=11} Haz clase con Alicia el martes, miercoles, jueves o viernes.{/size}{/color}"
    new "{color=#000000}{size=12}{b}[nombreAlicia]:{/b}{/size}{size=11} Take a class with Alicia on Tuesday, Wednesday, Thursday or Friday.{/size}{/color}"
# TODO: Translation updated at 2021-10-13 15:38

translate english strings:

    # game/Code/hud/ayudasJuego/ayudasJuego.rpy:48
    old "{color=#000000}{size=12}{b}[nombreMad]:{/b}{/size}{size=11} Visita a [nombreMad3] en su habitación, el miercoles o sábado por la tarde {/size}{/color}"
    new ""

