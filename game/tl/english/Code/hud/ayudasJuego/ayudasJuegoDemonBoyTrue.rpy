﻿# TODO: Translation updated at 2021-10-13 15:38

translate english strings:

    # game/Code/hud/ayudasJuego/ayudasJuegoDemonBoyTrue.rpy:20
    old " {image=madcarnetMini} {color=#000000}{size=12}{b}[nombreMad]:{/b}{/size}{size=11} Visita a [nombreMad3] en la {b} Cocina{/b}   {i} (el Martes o Sábado al Mediodía) {/i} {/size}{/color} "
    new " {image=madcarnetMini} {color=#000000}{size=12}{b}[nombreMad]:{/b}{/size}{size=11} Visit [nombreMad3] in the {b} Kitchen{/b}   {i} (on Tuesday or Saturday at Midday) {/i} {/size}{/color} "

    # game/Code/hud/ayudasJuego/ayudasJuegoDemonBoyTrue.rpy:23
    old " {image=madcarnetMini} {color=#000000}{size=12}{b}[nombreMad]:{/b}{/size}{size=11} Observa como [nombreMad3] hace yoga en el {b} Almacen gimnasio {/b}  {i} (el Miercoles o Sábado al Amanecer) {/i}  {/size}{/color} "
    new " {image=madcarnetMini} {color=#000000}{size=12}{b}[nombreMad]:{/b}{/size}{size=11} Watch [nombreMad3] do yoga in the {b} Gym Storage {/b} {i}  (on Wednesday or Saturday at Sunrise) {/i} {/size}{/color} "

    # game/Code/hud/ayudasJuego/ayudasJuegoDemonBoyTrue.rpy:26
    old " {image=madcarnetMini} {color=#000000}{size=12}{b}[nombreMad]:{/b}{/size}{size=11} Visita a [nombreMad3] en {b} Su Habitación {/b}  {i} (el Miércoles o Sábado por la tarde) {/i} {/size}{/color} "
    new " {image=madcarnetMini} {color=#000000}{size=12}{b}[nombreMad]:{/b}{/size}{size=11} Visits [nombreMad3] in {b} His Room {/b} {i} (on Wednesday or Saturday afternoon) {/i} {/size}{/color} "

    # game/Code/hud/ayudasJuego/ayudasJuegoDemonBoyTrue.rpy:29
    old " {image=madcarnetMini} {color=#000000}{size=12}{b}[nombreMad]:{/b}{/size}{size=11} Preguntale a [nombreMad3] como se encuentra, {b} en Su Habitación {/b} {i} (el Miércoles o Sábado por la tarde) {/i} {/size}{/color} "
    new "{image=madcarnetMini} {color=#000000}{size=12}{b}[nombreMad]:{/b}{/size}{size=11} Ask [nombreMad3] how he is, {b} in His Room {/b} {i}  (on Wednesday or Saturday afternoon) {/i} {/size}{/color} "

    # game/Code/hud/ayudasJuego/ayudasJuegoDemonBoyTrue.rpy:33
    old " {image=madcarnetMini} {color=#000000}{size=12}{b}[nombreMad]:{/b}{/size}{size=11} Investiga que hace [nombreMad3] en la {b} Cocina  {/b} {i}  (el Jueves o el Domingo al Mediodía) {/i} {/size}{/color} "
    new " {image=madcarnetMini} {color=#000000}{size=12}{b}[nombreMad]:{/b}{/size}{size=11} Investigates what [nombreMad3] does in the {b} Kitchen {/b} {i}  (on Thursday or Sunday Midday) {/i} {/size}{/color} "

    # game/Code/hud/ayudasJuego/ayudasJuegoDemonBoyTrue.rpy:36
    old " {image=madcarnetMini} {color=#000000}{size=12}{b}[nombreMad]:{/b}{/size}{size=11} Esperar acontecimientos... {/size}{/color} "
    new " {image=madcarnetMini} {color=#000000}{size=12}{b}[nombreMad]:{/b}{/size}{size=11} Wait for developments.... {/size}{/color} "

    # game/Code/hud/ayudasJuego/ayudasJuegoDemonBoyTrue.rpy:39
    old " {image=madcarnetMini} {color=#000000}{size=12}{b}[nombreMad]:{/b}{/size}{size=11} Infórmale de tu nuevo trabajo, en {b} Su Habitación {/b}  {i}(el Miercoles o Sábado por la tarde) {/i}{/size}{/color} "
    new " {image=madcarnetMini} {color=#000000}{size=12}{b}[nombreMad]:{/b}{/size}{size=11} Inform him of your new job, in {b} His Room {/b}  {i}(on Wednesday or Saturday afternoon) {/i}{/size}{/color} "

    # game/Code/hud/ayudasJuego/ayudasJuegoDemonBoyTrue.rpy:68
    old " {image=medcarnetMini} {color=#000000}{size=12}{b}[nombreMed]:{/b}{/size}{size=11} Visítala en la {b} Piscina {/b}  {i}(el Miércoles al Amanecer o el Sábado por la Mañana){/i} {/size}{/color} "
    new " {image=medcarnetMini} {color=#000000}{size=12}{b}[nombreMed]:{/b}{/size}{size=11} Visit her at {b} Pool {/b}  {i}(on Wednesday Sunrise or Saturday Morning){/i} {/size}{/color} "

    # game/Code/hud/ayudasJuego/ayudasJuegoDemonBoyTrue.rpy:70
    old " {image=medcarnetMini} {color=#000000}{size=12}{b}[nombreMed]:{/b}{/size}{size=11} Visítala en la {b} Piscina {/b}  {i}(el Miércoles al Amanecer o el Sábado por la Mañana){/i}  {/size}{/color} "
    new " {image=medcarnetMini} {color=#000000}{size=12}{b}[nombreMed]:{/b}{/size}{size=11} Visit her at the {b} Pool {/b}  {i}(on Wednesday at Sunrise or Saturday Morning){/i} {/size}{/color}"

    # game/Code/hud/ayudasJuego/ayudasJuegoDemonBoyTrue.rpy:73
    old " {image=medcarnetMini} {color=#000000}{size=12}{b}[nombreMed]:{/b}{/size}{size=11} Habla con ella en el {b} Comedor {/b}  {i} (el Miércoles o Viernes al Mediodía) {/i} {/size}{/color} "
    new " {image=medcarnetMini} {color=#000000}{size=12}{b}[nombreMed]:{/b}{/size}{size=11} Talk to her in the {b} Dining Room {/b}  {i}(on Wednesday or Friday at Midday){/i} {/size}{/color} "

    # game/Code/hud/ayudasJuego/ayudasJuegoDemonBoyTrue.rpy:86
    old " {image=maycarnetMini} {color=#000000}{size=12}{b}[nombreMay]:{/b}{/size}{size=11} Mirar televisión en el {b} Salón {/b} {/size}{/color} "
    new " {image=maycarnetMini} {color=#000000}{size=12}{b}[nombreMay]:{/b}{/size}{size=11} Watch TV in the {b} Lounge {/b} {/size}{/color} "
# TODO: Translation updated at 2021-10-15 11:12

translate english strings:

    # game/Code/hud/ayudasJuego/ayudasJuegoDemonBoyTrue.rpy:33
    old " {image=madcarnetMini} {color=#000000}{size=12}{b}[nombreMad]:{/b}{/size}{size=11} Investiga que hace [nombreMad3] en la {b} Cocina  {/b} {i} (el Jueves o el Domingo al Mediodía) {/i} {/size}{/color} "
    new " {image=madcarnetMini} {color=#000000}{size=12}{b}[nombreMad]:{/b}{/size}{size=11} Investigates what [nombreMad3] does in the {b} Kitchen {/b} {i}  (on Thursday or Sunday Midday) {/i} {/size}{/color} "
# TODO: Translation updated at 2021-12-12 13:06

translate english strings:

    # game/Code/hud/ayudasJuego/ayudasJuegoDemonBoyTrue.rpy:49
    old " {image=madcarnetMini} {color=#000000}{size=12}{b}[nombreMad]:{/b}{/size}{size=11} Dar un masaje de pies a [nombreMad3]. {b} Salón {/b}  {i} (Lunes o Martes por la Tarde o Jueves al Mediodía) {/i}  {/size}{/color} "
    new " {image=madcarnetMini} {color=#000000}{size=12}{b}[nombreMad]:{/b}{/size}{size=11} Give a foot massage to [nombreMad3]. {b} Living room {/b} {i}  (Monday or Tuesday Afternoon or Thursday Noon) {/i} {/size}{/color} "

    # game/Code/hud/ayudasJuego/ayudasJuegoDemonBoyTrue.rpy:62
    old " {image=peqcarnetMini} {color=#000000}{size=12}{b}[nombrePeq]:{/b}{/size}{size=11} Habla con ella sobre la situación de casa. {b} Cocina {/b}  {i} (Miercoles al mediodía o Domingo por la mañana) {/i} {/size}{/color}"
    new " {image=peqcarnetMini} {color=#000000}{size=12}{b}[nombrePeq]:{/b}{/size}{size=11} Talk to her about the home situation. {b} Kitchen {/b}  {i}  (Wednesday noon or Sunday morning) {/i} {/size}{/color}"

    # game/Code/hud/ayudasJuego/ayudasJuegoDemonBoyTrue.rpy:65
    old " {image=peqcarnetMini} {color=#000000}{size=12}{b}[nombrePeq]:{/b}{/size}{size=11} Habla con ella cuando esté haciendo gimnasia. {b} Gym {/b}  {i} (Lunes o Jueves al mediodía) {/i} {/size}{/color}"
    new " {image=peqcarnetMini} {color=#000000}{size=12}{b}[nombrePeq]:{/b}{/size}{size=11} Talk to her when she is doing gym. {b} Gym {/b} {i}   (Monday or Thursday noon) {/i} {/size}{/color}"

    # game/Code/hud/ayudasJuego/ayudasJuegoDemonBoyTrue.rpy:86
    old " {image=medcarnetMini} {color=#000000}{size=12}{b}[nombreMed]:{/b}{/size}{size=11} Erica se va? {b} Comedor {/b}  {i} (el Viernes por la Tarde) {/i} {/size}{/color} "
    new " {image=medcarnetMini} {color=#000000}{size=12}{b}[nombreMed]:{/b}{/size}{size=11} Erica is going? {b} Dining room {/b} {i}  (Friday Afternoon) {/i} {/size}{/color} "

    # game/Code/hud/ayudasJuego/ayudasJuegoDemonBoyTrue.rpy:102
    old " {image=maycarnetMini} {color=#000000}{size=12}{b}[nombreMay]:{/b}{/size}{size=11} Habla con ella {b} Cocina {/b}  {i}(el Miércoles o Sábado al Amanecer){/i} {/size}{/color} "
    new " {image=maycarnetMini} {color=#000000}{size=12}{b}[nombreMay]:{/b}{/size}{size=11} Talk to her {b} Kitchen {/b}  {i}(on Wednesday or Saturday at Sunrise){/i} {/size}{/color}"

    # game/Code/hud/ayudasJuego/ayudasJuegoDemonBoyTrue.rpy:105
    old " {image=maycarnetMini} {color=#000000}{size=12}{b}[nombreMay]:{/b}{/size}{size=11} Esperar acontecimientos... {/size}{/color} "
    new " {image=maycarnetMini} {color=#000000}{size=12}{b}[nombreMay]:{/b}{/size}{size=11} Wait for developments.... {/size}{/color} "
# TODO: Translation updated at 2022-02-07 21:19

translate english strings:

    # game/Code/hud/ayudasJuego/ayudasJuegoDemonBoyTrue.rpy:32
    old " {image=madcarnetMini} {color=#000000}{size=12}{b}[nombreMad]:{/b}{/size}{size=11} Infórmale de tu nuevo trabajo, en {b} en Su Habitación {/b}  {i}(el Miercoles o Sábado por la tarde) {/i}{/size}{/color} "
    new " {image=madcarnetMini} {color=#000000}{size=12}{b}[nombreMad]:{/b}{/size}{size=11} Inform her of your new work {b} in Her Room {/b} {i}(Wednesday or Saturday afternoon) {/i}{/size}{/color} "

    # game/Code/hud/ayudasJuego/ayudasJuegoDemonBoyTrue.rpy:68
    old " {image=peqcarnetMini} {color=#000000}{size=12}{b}[nombrePeq]:{/b}{/size}{size=11} Observa que hace. {b} Su Habitación {/b}  {i} {/i} {/size}{/color}"
    new " {image=peqcarnetMini} {color=#000000}{size=12}{b}[nombrePeq]:{/b}{/size}{size=11} See what happens. {b} Her Room {/b}  {i} {/i} {/size}{/color}"

    # game/Code/hud/ayudasJuego/ayudasJuegoDemonBoyTrue.rpy:72
    old " {image=peqcarnetMini} {color=#000000}{size=12}{b}[nombrePeq]:{/b}{/size}{size=11} ¿Que hace? {b} Su Habitación {/b}  {i} (Sabado o Domingo a 2nda hora) {/i} {/size}{/color}"
    new " {image=peqcarnetMini} {color=#000000}{size=12}{b}[nombrePeq]:{/b}{/size}{size=11} What's she doing? {b} Her Room {/b}  {i} (Saturday or Sunday Morning) {/i} {/size}{/color}"
# TODO: Translation updated at 2022-03-27 17:09

translate english strings:

    # game/Code/hud/ayudasJuego/ayudasJuegoDemonBoyTrue.rpy:114
    old " {image=maycarnetMini} {color=#000000}{size=12}{b}[nombreMay]:{/b}{/size}{size=11} Comprar pastillas para dormir {/size}{/color} "
    new " {image=maycarnetMini} {color=#000000}{size=12}{b}[nombreMay]:{/b}{/size}{size=11} Buy sleeping pills {/size}{/color} "
