﻿# TODO: Translation updated at 2021-04-05 12:03

translate english strings:

    # game/screens.rpy:256
    old "Atrás"
    new "Back"

    # game/screens.rpy:257
    old "Historial"
    new "History"

    # game/screens.rpy:258
    old "Saltar"
    new "Skip"

    # game/screens.rpy:259
    old "Auto"
    new "Auto"

    # game/screens.rpy:260
    old "Guardar"
    new "Save"

    # game/screens.rpy:261
    old "Guardar R."
    new "Q.Save"

    # game/screens.rpy:262
    old "Cargar R."
    new "Q.Load"

    # game/screens.rpy:263
    old "Prefs."
    new "Prefs."

    # game/screens.rpy:304
    old "Comenzar"
    new "Start"

    # game/screens.rpy:312
    old "Cargar"
    new "Load"

    # game/screens.rpy:314
    old "Opciones"
    new "Preferences"

    # game/screens.rpy:318
    old "Finaliza repetición"
    new "End Replay"

    # game/screens.rpy:322
    old "Menú principal"
    new "Main menu"

    # game/screens.rpy:324
    old "Acerca de"
    new "About"

    # game/screens.rpy:329
    old "Ayuda"
    new "Help"

    # game/screens.rpy:335
    old "Salir"
    new "Quit"

    # game/screens.rpy:476
    old "Volver"
    new "Return"

    # game/screens.rpy:561
    old "Versión [config.version!t]\n"
    new "Version [config.version!t]\n"

    # game/screens.rpy:567
    old "Hecho con {a=https://www.renpy.org/}Ren'Py{/a} [renpy.version_only].\n\n[renpy.license!t]"
    new "Made with {a=https://www.renpy.org/}Ren'Py{/a} [renpy.version_only].\n\n[renpy.license!t]"

    # game/screens.rpy:608
    old "Página {}"
    new "Page {}"

    # game/screens.rpy:608
    old "Grabación automática"
    new "Automatic saves"

    # game/screens.rpy:608
    old "Grabación rápida"
    new "Quick saves"

    # game/screens.rpy:651
    old "{#file_time}%A, %d de %B %Y, %H:%M"
    new "{#file_time}%A, %d de %B %Y, %H:%M"

    # game/screens.rpy:651
    old "vacío"
    new "empty slot"

    # game/screens.rpy:668
    old "<"
    new "<"

    # game/screens.rpy:671
    old "{#auto_page}A"
    new "{#auto_page}A"

    # game/screens.rpy:674
    old "{#quick_page}R"
    new "{#quick_page}R"

    # game/screens.rpy:680
    old ">"
    new ">"

    # game/screens.rpy:737
    old "Pantalla"
    new "Display"

    # game/screens.rpy:738
    old "Ventana"
    new "Window"

    # game/screens.rpy:739
    old "Pant. completa"
    new "Fullscreen"

    # game/screens.rpy:743
    old "Lado de retroceso"
    new "Rollback Side"

    # game/screens.rpy:744
    old "Desactivar"
    new "Disable"
    # game/screens.rpy:745
    old "Izquierda"
    new "Left"

    # game/screens.rpy:746
    old "Derecha"
    new "Right"

    # game/screens.rpy:751
    old "Texto no visto"
    new "Unseen Text"

    # game/screens.rpy:752
    old "Tras opciones"
    new "After Choices"

    # game/screens.rpy:753
    old "Transiciones"
    new "Transitions"

    # game/screens.rpy:766
    old "Veloc. texto"
    new "Text Speed"

    # game/screens.rpy:770
    old "Veloc. auto-avance"
    new "Auto-Forward Time"

    # game/screens.rpy:777
    old "Volumen música"
    new "Music Volume"

    # game/screens.rpy:784
    old "Volumen sonido"
    new "Sound Volume"

    # game/screens.rpy:790
    old "Prueba"
    new "Test"

    # game/screens.rpy:794
    old "Volumen voz"
    new "Voice Volume"

    # game/screens.rpy:805
    old "Silencia todo"
    new "Mute All"

    # game/screens.rpy:924
    old "El historial está vacío."
    new "The dialogue history is empty."

    # game/screens.rpy:994
    old "Teclado"
    new "Keyboard"

    # game/screens.rpy:995
    old "Ratón"
    new "Mouse"

    # game/screens.rpy:998
    old "Mando"
    new "Gamepad"

    # game/screens.rpy:1011
    old "Enter"
    new "Enter"

    # game/screens.rpy:1012
    old "Avanza el diálogo y activa la interfaz."
    new "Advances dialogue and activates the interface."

    # game/screens.rpy:1015
    old "Espacio"
    new "Space"

    # game/screens.rpy:1016
    old "Avanza el dilogo sin seleccionar opciones."
    new "Advances dialogue without selecting choices."

    # game/screens.rpy:1019
    old "Teclas de flecha"
    new "Arrow Keys"

    # game/screens.rpy:1020
    old "Navega la interfaz."
    new "Navigate the interface."

    # game/screens.rpy:1023
    old "Escape"
    new "Escape"

    # game/screens.rpy:1024
    old "Accede al menú del juego."
    new "Accesses the game menu."

    # game/screens.rpy:1027
    old "Ctrl"
    new "Ctrl"

    # game/screens.rpy:1028
    old "Salta el diálogo mientras se presiona."
    new "Skips dialogue while held down."

    # game/screens.rpy:1031
    old "Tabulador"
    new "Tab"

    # game/screens.rpy:1032
    old "Activa/desactiva el salto de diálogo."
    new "Toggles dialogue skipping."

    # game/screens.rpy:1035
    old "Av. pág."
    new "Page Up"

    # game/screens.rpy:1036
    old "Retrocede al diálogo anterior."
    new "Rolls back to earlier dialogue."

    # game/screens.rpy:1039
    old "Re. pág."
    new "Page Down"

    # game/screens.rpy:1040
    old "Avanza hacia el diálogo siguiente."
    new "Rolls forward to later dialogue."

    # game/screens.rpy:1044
    old "Oculta la interfaz."
    new "Hides the user interface."

    # game/screens.rpy:1048
    old "Captura la pantalla."
    new "Takes a screenshot."

    # game/screens.rpy:1052
    old "Activa/desactiva la asistencia por {a=https://www.renpy.org/l/voicing}voz-automática{/a}."
    new "Toggles assistive {a=https://www.renpy.org/l/voicing}self-voicing{/a}."

    # game/screens.rpy:1058
    old "Clic izquierdo"
    new "Left Click"

    # game/screens.rpy:1062
    old "Clic medio"
    new "Middle Click"

    # game/screens.rpy:1066
    old "Clic derecho"
    new "Right Click"

    # game/screens.rpy:1070
    old "Rueda del ratón arriba\nClic en lado de retroceso"
    new "Mouse Wheel Up\nClick Rollback Side"

    # game/screens.rpy:1074
    old "Rueda del ratón abajo"
    new "Mouse Wheel Down"

    # game/screens.rpy:1081
    old "Gatillo derecho\nA/Botón inferior"
    new "Right Trigger\nA/Bottom Button"

    # game/screens.rpy:1085
    old "Gatillo izquierdo\nBotón sup. frontal izq."
    new "Left Trigger\nLeft Shoulder"

    # game/screens.rpy:1089
    old "Botón sup. frontal der."
    new "Right Shoulder"

    # game/screens.rpy:1094
    old "D-Pad, Sticks"
    new "D-Pad, Sticks"

    # game/screens.rpy:1098
    old "Comenzar, Guía"
    new "Start, Guide"

    # game/screens.rpy:1102
    old "Y/Botón superior"
    new "Y/Top Button"

    # game/screens.rpy:1105
    old "Calibrar"
    new "Calibrate"

    # game/screens.rpy:1171
    old "Sí"
    new "Yes"

    # game/screens.rpy:1172
    old "No"
    new "No"

    # game/screens.rpy:1218
    old "Saltando"
    new "Skipping"

    # game/screens.rpy:1442
    old "Menú"
    new "Menu"
# TODO: Translation updated at 2021-04-05 13:09

translate english strings:

    # game/screens.rpy:759
    old "Idioma"
    new "Language"
# TODO: Translation updated at 2021-07-08 13:36

translate english strings:

    # game/screens.rpy:325
    old "Creditos"
    new "Credits"

    # game/screens.rpy:593
    old "{b}Juego:{/b}"
    new "{b}Game:{/b}"

    # game/screens.rpy:596
    old "{b}Música:{/b}"
    new "{b}Music:{/b}"

    # game/screens.rpy:600
    old "{b}Gracias a todos mis Patreons y Subscribestar.\n Sin vosotros no sería posible.{/b}"
    new "{b}Thanks to all my Patreons and Subscribestar.\n Without you it would not be possible.{/b}"
# TODO: Translation updated at 2022-02-07 21:19

translate english strings:

    # game/screens.rpy:682
    old "[save_name]"
    new "[save_name]"

    # game/screens.rpy:688
    old "Terminar"
    new "Finish"
# TODO: Translation updated at 2022-03-27 17:09

translate english strings:

    # game/screens.rpy:663
    old "Guardar nombre"
    new "Save name"

    # game/screens.rpy:680
    old "Escriba un nombre para guardar"
    new "Type a save name"

    # game/screens.rpy:693
    old "Back"
    new "Back"
