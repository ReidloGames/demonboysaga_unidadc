
label configGameVisual:

    define gui.text_font = "font/Aneliza-Medium.ttf"
    #define gui.interface_text_font = "font/Aneliza-Medium.ttf"
    define gui.interface_text_font = "font/Aneliza-Black.ttf"
    define gui.interface_text_size = 15
    #color text Opciones
    define gui.accent_color = '#B0240F'

    define gui.hover_color = '#C05050'

    #Barra Scroll horizontal
    #define gui.bar_size = 25
    #define gui.scrollbar_size = 12
    #define gui.slider_size = 25
    define gui.bar_size = 10
    define gui.scrollbar_size = 12
    define gui.slider_size = 10



    style say_dialogue:
        outlines [ (absolute(2), "#000", absolute(1), absolute(1)) ]
        color "#FFFFFF"



    style say_label:
        outlines [ (absolute(2), "#000", absolute(1), absolute(1)) ]



    style input_prompt:
        color "#FFFFFF"
        outlines [ (absolute(2), "#000", absolute(1), absolute(1)) ]



    style input:
        color "#FFFFFF"
        outlines [ (absolute(2), "#000", absolute(1), absolute(1)) ]



    style choice_button_text:
        idle_color "#FFFFFF"
        hover_color "#0080FF"
        insensitive_color "#808080"
        outlines [ (absolute(2), "#000", absolute(1), absolute(1)) ]
        size 18

        #font "font/Aneliza-Medium.ttf"



    style quick_button_text:
        font "font/Aneliza-Medium.ttf"
        size 14
        idle_color "#8885"
        hover_color "#0F05"
        insensitive_color "#4445"
        outlines [ (absolute(1), "#000A", absolute(0), absolute(0)) ]
        hover_outlines [ (absolute(1), "#050F", absolute(0), absolute(0)) ]



    style window:
        background None



    style namebox:
        background None



    style choice_button:
        background None


    python:

        if renpy.variant("pc"):
            suppress_overlay = True
        else:
            suppress_overlay = False

    $ quick_menu = True


    define gui.choice_button_width = 1185
    define gui.choice_button_height = None
    define gui.choice_button_tile = False
    define gui.choice_button_borders = Borders(150, 8, 150, 8)
    define gui.choice_button_text_font = gui.text_font
    define gui.choice_button_text_size = gui.text_size
    define gui.choice_button_text_xalign = 0.5
    define gui.choice_button_text_idle_color = "#cccccc"
    define gui.choice_button_text_hover_color = "#ffffff"
