label eventConoceACarla():
    if conocesCarla == False:
        $ conocesCarla = True
        scene habitacionAmarzondConoceCarlaEvent_0
        stop music fadeout 2.0
        pause
        play sound "audio/effects/sorpresaHombre.ogg"
        scene habitacionAmarzondConoceCarlaEvent_1 with dissolve
        protaPensa "¡¡¡Ohh dios!!! por ahora las condiciones son perfectas."
        scene habitacionAmarzondConoceCarlaEvent_2 with dissolve
        pause
        scene habitacionAmarzondConoceCarlaEvent_3 with dissolve
        pause
        play sound "audio/effects/golpecitoSuelo.ogg"
        scene habitacionAmarzondConoceCarlaEvent_4 with dissolve
        pause
        scene habitacionAmarzondConoceCarlaEvent_5 with dissolve
        pause
        scene habitacionAmarzondConoceCarlaEvent_6 with dissolve
        prota "Hola"
        play sound "audio/effects/risaChica.ogg"
        scene habitacionAmarzondConoceCarlaEvent_7 with dissolve
        carla "¡Uy!"
        carla "Perdona, no sabía que había alguien. Se me cayó un sobre detrás de la estantería."
        scene habitacionAmarzondConoceCarlaEvent_8 with dissolve
        protaPensa "Me reafirmo, las condiciones son más que perfectas. Esta chica es preciosa."
        scene habitacionAmarzondConoceCarlaEvent_9 with dissolve
        protaPensa "Hola, acabo de hablar con Louie. Soy [nombreProta2] vengo por la petición del empleo."
        scene habitacionAmarzondConoceCarlaEvent_10 with dissolve
        play sound "audio/effects/risaChica3.ogg"
        carla "Hola encantada, soy Carla la mujer de Louie."

        ##newPersonaje agregar a lista de chicas
        call avisosStatsSms("newContact") from _call_avisosStatsSms_11
        $ chicaCarla = True
        scene habitacionAmarzondConoceCarlaEvent_11 with dissolve
        play music "audio/music/fretless-by-kevin-macleod.ogg" fadein 2.0
        carla "El empleo consiste en enviar los paquetes a los domicilios."
        scene habitacionAmarzondConoceCarlaEvent_12 with dissolve
        carla "Lo que necesitaras es un transporte."
        scene habitacionAmarzondConoceCarlaEvent_13 with dissolve
        carla "¿Tienes coche?"
        scene habitacionAmarzondConoceCarlaEvent_23 with dissolve
        pause
        scene habitacionAmarzondConoceCarlaEvent_14 with dissolve
        prota "No."
        scene habitacionAmarzondConoceCarlaEvent_15 with dissolve
        carla "¿Moto?"
        scene habitacionAmarzondConoceCarlaEvent_16 with dissolve
        prota "Me temo que no..."
        scene habitacionAmarzondConoceCarlaEvent_17 with dissolve
        carla "Ohhh..."
        scene habitacionAmarzondConoceCarlaEvent_18 with dissolve
        carla "¿Y bicicleta?"
        scene habitacionAmarzondConoceCarlaEvent_19 with dissolve
        protaPensa "Creo que en casa hay una bici, pero no recuerdo donde estaba... Tendré que buscarla."
        scene habitacionAmarzondConoceCarlaEvent_20 with dissolve
        prota "Si, creo que en casa tengo una."
        scene habitacionAmarzondConoceCarlaEvent_21 with dissolve
        carla "¡Genial!"
        scene habitacionAmarzondConoceCarlaEvent_22 with dissolve
        carla "Cuando tengas la bicicleta habla con Louie y él te dirá donde tienes que enviar los paquetes."
        scene habitacionAmarzondConoceCarlaEvent_23 with dissolve
        pause
        scene habitacionAmarzondConoceCarlaEvent_24 with dissolve
        prota "Muchas gracias."
        scene habitacionAmarzondConoceCarlaEvent_22 with dissolve
        carla "A ti. Espero que te sientas a gusto con nosotros."
        scene habitacionAmarzondConoceCarlaEvent_23 with dissolve
        pause
        scene habitacionAmarzondConoceCarlaEvent_24 with dissolve
        pause
        scene habitacionAmarzondConoceCarlaEvent_25 with dissolve
        pause
        scene habitacionAmarzondConoceCarlaEvent_26 with dissolve
        pause
        scene habitacionAmarzondConoceCarlaEvent_27 with dissolve
        protaPensa "Que buena que esta y que culo tiene..."
        #VAR COGER BICI
        $ puedesCogerBici = True
        #AL trabajar Cobraras 10€ por trabajo.

    return
