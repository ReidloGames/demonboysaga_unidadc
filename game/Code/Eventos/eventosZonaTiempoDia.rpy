label eventosZonaTiempoDia():
    call esconderHud() from _call_esconderHud

    #miCasa
    if zona == "micasa":
        call eventosMiCasaFas0() from _call_eventosMiCasaFas0
    if zona == "escuela":
        call eventosEscuela() from _call_eventosEscuela
    if zona == "miMapa":
        call eventosMapa() from _call_eventosMapa
    if zona == "amarzong":
        call eventosAmarzong from _call_eventosAmarzong
    #if zona == "tiendaKara":
    #    call eventosTiendaKara


    call mostrarHud() from _call_mostrarHud

    return
