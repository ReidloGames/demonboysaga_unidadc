
label openingIntro:
    scene intro_01
    pause
    #scene parque with fade
    scene intro_02 with dissolve
    pause
    scene intro_03 with dissolve
    "Esta grabando..."
    play sound "audio/effects/moverCam.ogg"
    scene intro_02 with hpunch
    pause .5
    scene intro_02_REC
    "Ahora si."
    scene intro_04 with dissolve
    "Hola me llamo"
    $ nombreProta = "Lucas"
    python:
        nombreProta = renpy.input(_("Tu nombre?"))
        nombreProta = nombreProta.strip() or "Lucas"
    $ nombreProta2 = nombreProta
    play music "audio/music/fearless.ogg" fadein 5.0
    prota "Y tengo 21 años"
    scene intro_05 with dissolve
    prota "Os voy a contar como todo puede dar un giro de 180 grados y cambiarte la vida de la noche a la mañana."

    if renpy.loadable("parche.rpy"):
        call historiaOpening_p from _call_historiaOpening_p #patx
    else:
        call historiaOpening() from _call_historiaOpening

    #Version ANDROID comentar if else de arriba
    #call historiaOpening_p


    stop music fadeout 2.0
    scene despierta1 with hpunch
    $ renpy.sound.play("audio/effects/alarmReloj.ogg", loop = True,relative_volume=0.3)
    "!!RIIIIIINNNGGGG!!"
    scene despierta2 with hpunch
    pause
    scene despierta3 with dissolve
    pause
    stop sound
    scene despierta4 with dissolve
    prota "ARGGHH, que pereza..."
    prota "Otro dia aburrido mas, en el que no pasara nada interesante y que encima toca levantarse para ir a la universidad... "
    scene despierta5 with dissolve
    return


label historiaOpening:
    scene intro_06 with dissolve
    prota "Hace 5 años mis padres fallecieron en un accidente de tráfico.Pero la familia Spencer, amigos intimos de mis padres difuntos me adoptaron y ahora son mi familia."
    scene intro_05 with dissolve
    prota "Después de esto todo iba genial, una gran casa, familia adinerada, sin preocupaciones…"
    scene intro_07 with dissolve
    prota "Pero todo cambio. [nombrePad] que es mi [situPad], fue despedido de su trabajo y la familia empezó a desmoronarse."
    scene intro_06 with dissolve
    prota "No se cual fue la razón pero algo raro pasó."
    scene intro_07 with dissolve
    prota "A partir de ahí vinieron los problemas."
    scene intro_08 with dissolve
    prota "De tener dinero y ser una familia privilegiada vinieron las deudas y las discusiones familiares."
    scene intro_07 with dissolve
    prota "[nombreMad] que es mi [situMad] tuvo que buscarse un empleo para tapar agujeros..."

    return
