label eventClaireDirectorVisita2():
    if eventClaireDirectorVisita2 == False:
        $ eventClaireDirectorVisita2 = True
        scene claireDirector2ndaVisita_0
        pause
        scene claireDirector2ndaVisita_1 with dissolve
        pause
        scene claireDirector2ndaVisita_2 with dissolve
        pause
        scene claireDirector2ndaVisita_3 with hpunch
        play sound "audio/effects/golpeAu1.ogg"
        "¡¡¡Pam!!!"
        stop music fadeout 3.0
        scene claireDirector2ndaVisita_4 with dissolve
        claire "¡¡Ay!! que daño!"
        scene claireDirector2ndaVisita_5 with dissolve
        protaPensa "Oh dios es Claire!, ¡joder sus bragas!"
        scene claireDirector2ndaVisita_6 with dissolve
        pause
        scene claireDirector2ndaVisita_7 with dissolve
        claire "Que haces tonto! tenías que ser tú, siempre en medio."
        scene claireDirector2ndaVisita_8 with dissolve
        prota "Perdón no te vi. ¿Estas bien?"
        scene claireDirector2ndaVisita_9 with dissolve
        claire "Sal de mi camino."
        scene claireDirector2ndaVisita_10 with dissolve
        protaPensa "¿A dónde ira?"
        play music "audio/music/Marty_Gots_a_Plan.ogg" fadein 2.0

        menu:
            "Seguirla":
                call eventClaireDirectorVisita2_Part2() from _call_eventClaireDirectorVisita2_Part2
    return


label eventClaireDirectorVisita2_Part2():
    scene claireDirector2ndaVisita_11 with dissolve
    pause
    scene claireDirector2ndaVisita_12 with dissolve
    pause
    scene claireDirector2ndaVisita_13 with dissolve
    pause
    scene claireDirector2ndaVisita_14 with dissolve
    protaPensa "¿Se va a ver al director?"
    scene claireDirector2ndaVisita_15 with dissolve
    protaPensa "Apenas puedo ver nada..."
    scene claireDirector2ndaVisita_16 with dissolve
    protaPensa "Ahora veo un poco, que hacen?"
    scene claireDirector2ndaVisita_28 with dissolve
    pause
    "Atención escena NTR, quieres activarla?"
    menu:
        "SI":
            $ ntrGame = True
        "NO":
            $ ntrGame = False
    if ntrGame == True:
        scene claireDirector2ndaVisita_29 with dissolve
        stop music fadeout 3.0
        directorUni "Bien así me gusta, que seas puntual."
        scene claireDirector2ndaVisita_30 with dissolve
        directorUni "¿Has venido sin sujetador como te pedí?"
        scene claireDirector2ndaVisita_31 with dissolve
        directorUni "No te oigo..."
        scene claireDirector2ndaVisita_32 with dissolve
        play music "audio/music/DangerStorm_kevinMacLeod.ogg" fadein 2.0
        claire "Si..."
        scene claireDirector2ndaVisita_33 with dissolve
        directorUni "Perfecto."
        #play sound "audio/effects/tiraraMesa.ogg"
        play sound "audio/effects/susto.ogg"
        scene claireDirector2ndaVisita_34 with hpunch
        directorUni "Pero tengo que comprobarlo..."
        scene claireDirector2ndaVisita_35 with dissolve
        claire "Esta bien... Pero esta vez sin pasarse de los límites..."
        scene claireDirector2ndaVisita_36 with dissolve
        directorUni "Claro, seré bueno..."
        scene claireDirector2ndaVisita_37 with dissolve
        pause
        play sound "audio/effects/roze1.ogg"
        scene claireDirector2ndaVisita_38 with dissolve
        pause
        scene claireDirector2ndaVisita_39 with dissolve
        pause
        play sound "audio/effects/roze3.ogg"
        scene claireDirector2ndaVisita_40 with dissolve
        directorUni "¿Y estos pezones? ¿Ya estas excitada?"
        directorUni "Necesitabas venir..."
        scene claireDirector2ndaVisita_41 with dissolve
        pause
        scene claireDirector2ndaVisita_18 with dissolve
        stop music fadeout 2.0
        protaPensa "¡¡¡Le ha levantado la camisa y se le ven las tetas!!!"
        scene claireDirector2ndaVisita_41 with dissolve
        pause
        scene claireDirector2ndaVisita_42 with dissolve
        play music "audio/music/ChillWave_kevinMacLeod.ogg" fadein 2.0
        directorUni "Ahora vamos a jugar un poco..."
        scene claireDirector2ndaVisita_43 with dissolve
        directorUni "No te oigo..."
        scene claireDirector2ndaVisita_44 with dissolve
        claire "Si..."
        scene claireDirector2ndaVisita_45 with dissolve
        pause
        scene claireDirector2ndaVisita_46 with dissolve
        pause
        scene claireDirector2ndaVisita_20 with dissolve
        protaPensa "¡Se está dejando manosear las tetas por el director!! ¡Menuda zorra!"
        scene claireDirector2ndaVisita_46 with dissolve
        pause
        $ manosearPechosClaireDirector2 = False
        $ manosearPechosClaireDirector2Continuar = False
        call menuManoseoPechosClaireDirectorVisita2() from _call_menuManoseoPechosClaireDirectorVisita2

    else:
        scene claireDirector2ndaVisita_16 with dissolve
        "El director le esta tocando la espalda?"
        scene claireDirector2ndaVisita_21 with dissolve
        stop music fadeout 3.0
        jack "¿Tu payaso, te has enamorado del director? Eres un voyeur."
        play sound "audio/effects/discorayado.ogg"
        scene claireDirector2ndaVisita_22 with dissolve
        protaPensa "¡¡Joder esta su novio aquí!!"
        scene claireDirector2ndaVisita_23 with dissolve
        jack "Mosquito te hice una pregunta. ¿Has visto a mi novia?"
        scene claireDirector2ndaVisita_24 with dissolve
        pause
        play sound "audio/effects/puertaCambiandose.ogg"
        scene claireDirector2ndaVisita_25_NONTR with dissolve
        pause
        scene claireDirector2ndaVisita_26_NONTR with dissolve
        claire "Que es este escandalo?"
        pause
        scene claireDirector2ndaVisita_27_NONTR with dissolve
        jack "Te estaba buscando."
        scene claireDirector2ndaVisita_28_NONTR with dissolve
        claire "Estaba resolviendo unos asuntos de la Universidad."

        call menuEventClaireDirectorVisita2PREMIUM from _call_menuEventClaireDirectorVisita2PREMIUM_2

    return

label menuManoseoPechosClaireDirectorVisita2():
    menu:
        "Manosear pechos 1":
            call manoseo1PechosClaireDirectorVisita2Bucle from _call_manoseo1PechosClaireDirectorVisita2Bucle
        "Manosear pechos 2" if manosearPechosClaireDirector2 == True:
            call manoseo2PechosClaireDirectorVisita2Bucle from _call_manoseo2PechosClaireDirectorVisita2Bucle
        "Continuar" if manosearPechosClaireDirector2Continuar == True:
            call manoseo2PechosClaireDirectorVisita2Continuar from _call_manoseo2PechosClaireDirectorVisita2Continuar

    return

label manoseo1PechosClaireDirectorVisita2Bucle():
    $ manosearPechosClaireDirector2 = True
    $ numManoseo1PechosClaireDirectorVisita2Bucle = 0
    while numManoseo1PechosClaireDirectorVisita2Bucle < 2:
        scene claireDirector2ndaVisita_46 with dissolve
        pause 0.1
        scene claireDirector2ndaVisita_47 with dissolve
        if numManoseo1PechosClaireDirectorVisita2Bucle == 0:
            play sound "audio/effects/roze3.ogg"
            directorUni "Me encantan estas tetas y mas me gusta que seas mi putita.{p=1.8}{nw}"
            pause 0.5
        pause 0.1
        scene claireDirector2ndaVisita_48 with dissolve
        pause 0.1
        scene claireDirector2ndaVisita_49 with dissolve
        pause 0.2
        scene claireDirector2ndaVisita_50 with dissolve
        pause 0.1
        scene claireDirector2ndaVisita_57 with dissolve
        pause 0.1
        scene claireDirector2ndaVisita_58 with dissolve
        play sound "audio/effects/dolor2.ogg"
        pause 0.4
        scene claireDirector2ndaVisita_57 with dissolve
        pause 0.1
        if numManoseo1PechosClaireDirectorVisita2Bucle == 1:
            directorUni "Y estos pezones los voy a chupar enteros.{p=1.5}{nw}"
        scene claireDirector2ndaVisita_58 with dissolve
        play sound "audio/effects/orgasm2.ogg"
        pause 0.1
        if numManoseo1PechosClaireDirectorVisita2Bucle == 1:
            play sound "audio/effects/dolor1.ogg"
            scene claireDirector2ndaVisita_59 with dissolve
            pause 0.1
            scene claireDirector2ndaVisita_47 with dissolve
            pause 0.3
        else:
            play sound "audio/effects/dolor1.ogg"
            scene claireDirector2ndaVisita_59 with dissolve
            pause 0.1
            scene claireDirector2ndaVisita_46 with dissolve
            pause 0.1
            play sound "audio/effects/orgasm4.ogg"
            scene claireDirector2ndaVisita_59 with dissolve
            pause 0.1
        $ numManoseo1PechosClaireDirectorVisita2Bucle = numManoseo1PechosClaireDirectorVisita2Bucle +1
    call menuManoseoPechosClaireDirectorVisita2() from _call_menuManoseoPechosClaireDirectorVisita2_1
    return

label manoseo2PechosClaireDirectorVisita2Bucle():
    $ numManoseo2PechosClaireDirectorVisita2Bucle = 0
    while numManoseo2PechosClaireDirectorVisita2Bucle < 2:
        scene claireDirector2ndaVisita_46_ with dissolve
        pause 0.1
        scene claireDirector2ndaVisita_47_ with dissolve
        if numManoseo2PechosClaireDirectorVisita2Bucle == 0:
            scene claireDirector2ndaVisita_51 with dissolve
            pause 0.7
            scene claireDirector2ndaVisita_54 with dissolve
            directorUni "¿Te gusta que te los apriete fuerte verdad?.{p=1.8}{nw}"
            pause 0.5
        scene claireDirector2ndaVisita_47_ with dissolve
        pause 0.1
        scene claireDirector2ndaVisita_48_ with dissolve
        pause 0.1
        scene claireDirector2ndaVisita_49_ with dissolve
        pause 0.1
        scene claireDirector2ndaVisita_50_ with dissolve
        pause 0.1
        if numManoseo2PechosClaireDirectorVisita2Bucle == 1:
            scene claireDirector2ndaVisita_57_ with dissolve
            pause 0.1
            play sound "audio/effects/orgasm2.ogg"
            scene claireDirector2ndaVisita_58_ with dissolve
            pause 0.5
            scene claireDirector2ndaVisita_57_ with dissolve
            pause 0.1
            scene claireDirector2ndaVisita_50_ with dissolve
            pause 0.1
            play sound "audio/effects/orgasm3.ogg"
            scene claireDirector2ndaVisita_59_ with dissolve
            pause 1
            scene claireDirector2ndaVisita_56 with dissolve
            directorUni "¿Así de fuerte o quieres mas?{p=1.2}{nw}"
        scene claireDirector2ndaVisita_57_ with dissolve
        pause 0.1
        scene claireDirector2ndaVisita_58_ with dissolve
        play sound "audio/effects/orgasm5.ogg"
        pause 0.1
        scene claireDirector2ndaVisita_57_ with dissolve
        pause 0.1
        $ numManoseo2PechosClaireDirectorVisita2Bucle = numManoseo2PechosClaireDirectorVisita2Bucle +1
    $ manosearPechosClaireDirector2Continuar = True
    call menuManoseoPechosClaireDirectorVisita2 from _call_menuManoseoPechosClaireDirectorVisita2_2
    return

label manoseo2PechosClaireDirectorVisita2Continuar():
    scene claireDirector2ndaVisita_60 with dissolve
    pause
    scene claireDirector2ndaVisita_61 with dissolve
    directorUni "Vamos a mirar un poco por abajo..."
    scene claireDirector2ndaVisita_62 with dissolve
    claire "¿Qué?"
    play sound "audio/effects/quitarRopa.ogg"
    scene claireDirector2ndaVisita_63 with dissolve
    pause
    scene claireDirector2ndaVisita_64 with dissolve
    pause
    scene claireDirector2ndaVisita_65 with dissolve
    pause
    scene claireDirector2ndaVisita_66 with dissolve
    claire "Dijimos que no pasaríamos de la raya."
    scene claireDirector2ndaVisita_67 with dissolve
    directorUni "Solo mirar un poco y ya está."
    scene claireDirector2ndaVisita_68 with dissolve
    pause
    scene claireDirector2ndaVisita_69 with dissolve
    directorUni "Sube la pierna"
    play sound "audio/effects/tiraraMesa.ogg"
    scene claireDirector2ndaVisita_70 with dissolve
    claire "¿Porque me pones así?"
    scene claireDirector2ndaVisita_71 with dissolve
    pause
    scene claireDirector2ndaVisita_72 with dissolve
    pause
    play sound "audio/effects/golpecitoGemido1.ogg"
    scene claireDirector2ndaVisita_73 with dissolve
    pause
    scene claireDirector2ndaVisita_74 with dissolve
    pause
    play sound "audio/effects/roze3.ogg"
    scene claireDirector2ndaVisita_75 with dissolve
    pause
    scene claireDirector2ndaVisita_76 with dissolve
    clairePensa "Otra vez..."
    scene claireDirector2ndaVisita_77 with dissolve
    pause
    scene claireDirector2ndaVisita_78 with dissolve
    pause
    play sound "audio/effects/abrirCony4.ogg"
    scene claireDirector2ndaVisita_79 with dissolve
    pause
    play sound "audio/effects/dedoConyo.ogg"
    scene claireDirector2ndaVisita_80 with dissolve
    pause
    scene claireDirector2ndaVisita_19 with dissolve
    protaPensa "¡¡¡Oh dios mio!!!"
    scene claireDirector2ndaVisita_21 with dissolve
    stop music fadeout 3.0
    jack "¿Tu payaso, te has enamorado del director? Eres un voyeur."
    play sound "audio/effects/discorayado.ogg"
    scene claireDirector2ndaVisita_22 with dissolve
    protaPensa "¡¡Joder esta su novio aquí!!"
    scene claireDirector2ndaVisita_23 with dissolve
    jack "Mosquito te hice una pregunta. ¿Has visto a mi novia?"
    scene claireDirector2ndaVisita_24 with dissolve
    protaPensa "¡Joder! su novia la está manoseando el director y este tonto buscándola."
    scene claireDirector2ndaVisita_25 with dissolve
    prota "No, la he visto."
    play sound "audio/effects/golpe1.ogg"
    scene claireDirector2ndaVisita_26 with dissolve
    jack "¡Pues deja de ser un voyeur y ven conmigo a buscarla, enano!"
    scene claireDirector2ndaVisita_27 with dissolve
    protaPensa "No he podido ver más una lástima... Me guardare el secreto por ahora..."
    protaPensa "Este tio es muy tonto hahaha"
    call menuEventClaireDirectorVisita2PREMIUM from _call_menuEventClaireDirectorVisita2PREMIUM
    return

label menuEventClaireDirectorVisita2PREMIUM:
    menu:
        "Saber que pasa en el despacho del director (PREMIUM)" if ntrGame == True:
            if versionPremium == True:
                call eventClaireDirectorVisita2PREMIUM from _call_eventClaireDirectorVisita2PREMIUM
            else:
                "Versión premium solo para suscriptores."
                call menuEventClaireDirectorVisita2PREMIUM from _call_menuEventClaireDirectorVisita2PREMIUM_1

        "Continuar":
            scene entradaEscuela2SG
            call nextTiempoDay(0) from _call_nextTiempoDay_24


    return

label eventClaireDirectorVisita2PREMIUM():
    play music "audio/music/ChillWave_kevinMacLeod.ogg" fadein 2.0
    scene claireDirector2ndaVisita_81
    pause
    scene claireDirector2ndaVisita_82 with dissolve
    pause
    scene claireDirector2ndaVisita_83 with dissolve
    pause
    play sound "audio/effects/juegaBragas2.ogg"
    scene claireDirector2ndaVisita_84 with dissolve
    pause
    scene claireDirector2ndaVisita_85 with dissolve
    pause
    scene claireDirector2ndaVisita_86 with dissolve
    directorUni "¡No te muevas!"
    play sound "audio/effects/golpecitoGemido1.ogg"
    scene claireDirector2ndaVisita_87 with dissolve
    "¡¡Splash!!"
    scene claireDirector2ndaVisita_88 with dissolve
    pause
    scene claireDirector2ndaVisita_89 with dissolve
    pause
    scene claireDirector2ndaVisita_90 with dissolve
    directorUni "Ven aquí."
    play sound "audio/effects/roze1.ogg"
    scene claireDirector2ndaVisita_91 with dissolve
    pause
    play sound "audio/effects/roze3.ogg"
    scene claireDirector2ndaVisita_92 with dissolve
    claire "¿Que haces?"
    scene claireDirector2ndaVisita_93 with dissolve
    pause
    play sound "audio/effects/orgasm5.ogg"
    scene claireDirector2ndaVisita_94 with dissolve
    directorUni "¡Silencio!"
    scene claireDirector2ndaVisita_95 with dissolve
    pause
    scene claireDirector2ndaVisita_96 with dissolve
    claire "Espera..."
    play sound "audio/effects/golpeAu1.ogg"
    scene claireDirector2ndaVisita_97 with hpunch
    directorUni "Ahora te voy a enseñar lo que necesitas."
    scene claireDirector2ndaVisita_98 with dissolve
    claire "¡Me voy a caer!"
    scene claireDirector2ndaVisita_99 with dissolve
    directorUni "No te preocupes yo te voy a coger bien."
    scene claireDirector2ndaVisita_100 with dissolve
    directorUni "Ahora colócate como te diga, que te voy abrir las piernas."
    play sound "audio/effects/roze3.ogg"
    scene claireDirector2ndaVisita_101 with dissolve
    pause
    play sound "audio/effects/golpecitoGemido2.ogg"
    scene claireDirector2ndaVisita_102 with dissolve
    pause
    scene claireDirector2ndaVisita_103 with dissolve
    pause
    scene claireDirector2ndaVisita_104 with dissolve
    directorUni "Voy a enseñarte lo que necesita tu coñito. Siempre vienes con la excusa, pero sabes muy bien que te encanta esto y acabas chorreando."
    play sound "audio/effects/asentir2golpe.ogg"
    scene claireDirector2ndaVisita_105 with dissolve
    claire "{size=14} no... {/size=14}"
    play sound "audio/effects/roze3.ogg"
    scene claireDirector2ndaVisita_106 with dissolve
    pause
    play sound "audio/effects/susto1.ogg"
    scene claireDirector2ndaVisita_107 with dissolve
    claire "¡Que me caigo!"
    scene claireDirector2ndaVisita_108 with dissolve
    directorUni "Tienes el clitoris hinchadísimo de lo cachonda que estas!"
    scene claireDirector2ndaVisita_109 with dissolve
    pause
    $ numEventClaireDirectorVisita2MeterleDedo1PREMIUM = False
    $ numEventClaireDirectorVisita2MeterleDedo3PREMIUM = False
    $ numEventClaireDirectorVisita2MeterleDedo4PREMIUM = False
    call menuMeterDedos() from _call_menuMeterDedos

    return

label menuMeterDedos():
    menu:
        "Meterle el dedo":
            call eventClaireDirectorVisita2MeterleDedo1PREMIUM from _call_eventClaireDirectorVisita2MeterleDedo1PREMIUM
            return

        "Meterle 2 dedos" if numEventClaireDirectorVisita2MeterleDedo1PREMIUM == True:
            call eventClaireDirectorVisita2Meterle2DedosPREMIUM from _call_eventClaireDirectorVisita2Meterle2DedosPREMIUM
            return
        "Meterle 3 dedos" if numEventClaireDirectorVisita2MeterleDedo3PREMIUM == True:
            call eventClaireDirectorVisita2Meterle3DedosPREMIUM from _call_eventClaireDirectorVisita2Meterle3DedosPREMIUM
            return
        "¡Mas fuerte y que se corra! "if numEventClaireDirectorVisita2MeterleDedo4PREMIUM == True:
            call eventClaireDirectorVisita2Meterle4DedosPREMIUM from _call_eventClaireDirectorVisita2Meterle4DedosPREMIUM
            return

label eventClaireDirectorVisita2MeterleDedo1PREMIUM():
    $ numEventClaireDirectorVisita2MeterleDedo1PREMIUM = 0
    scene claireDirector2ndaVisita_110 with dissolve
    pause
    while numEventClaireDirectorVisita2MeterleDedo1PREMIUM < 2:
        scene claireDirector2ndaVisita_111 with dissolve
        pause 0.01
        play sound "audio/effects/juegaBragas4.ogg"
        scene claireDirector2ndaVisita_112 with dissolve
        pause 0.05
        play sound "audio/effects/juegaBragas3.ogg"
        scene claireDirector2ndaVisita_113 with dissolve
        pause 0.02
        play sound "audio/effects/juegaBragasOrgasm.ogg"
        scene claireDirector2ndaVisita_114 with dissolve
        #pause 0.05
        if numEventClaireDirectorVisita2MeterleDedo1PREMIUM == 0:
            claire "oooohhhhhh...{p=0.8}{nw}"
            play sound "audio/effects/juegaBragasOrgasm2.ogg"
        if numEventClaireDirectorVisita2MeterleDedo1PREMIUM == 1:
            claire "aahhhhhh...{p=0.6}{nw}"
        scene claireDirector2ndaVisita_115 with dissolve
        pause 0.01
        play sound "audio/effects/juegaBragas4.ogg"
        scene claireDirector2ndaVisita_112 with dissolve

        pause 0.05
        play sound "audio/effects/juegaBragas5.ogg"
        scene claireDirector2ndaVisita_114 with dissolve
        if numEventClaireDirectorVisita2MeterleDedo1PREMIUM == 0:
            directorUni "Vas a ver como te dejo el coñito {p=1.1}{nw}"
        scene claireDirector2ndaVisita_115 with dissolve
        pause 0.02
        play sound "audio/effects/juegaBragas3.ogg"
        scene claireDirector2ndaVisita_112 with dissolve
        pause 0.05
        if numEventClaireDirectorVisita2MeterleDedo1PREMIUM == 1:
            scene claireDirector2ndaVisita_113 with dissolve
            pause 0.02
            play sound "audio/effects/juegaBragasOrgasm2.ogg"
            scene claireDirector2ndaVisita_115 with dissolve
            pause 0.03
        play sound "audio/effects/juegaBragas4.ogg"
        scene claireDirector2ndaVisita_114 with dissolve
        if numEventClaireDirectorVisita2MeterleDedo1PREMIUM == 1:
            directorUni "¿Ya sabe tu novio que te gusta venir aqui? {p=1.3}{nw}"

        $ numEventClaireDirectorVisita2MeterleDedo1PREMIUM = numEventClaireDirectorVisita2MeterleDedo1PREMIUM +1
    $ numEventClaireDirectorVisita2MeterleDedo1PREMIUM = True
    call menuMeterDedos from _call_menuMeterDedos_1

    return

label eventClaireDirectorVisita2Meterle2DedosPREMIUM():
    $ numEventClaireDirectorVisita2MeterleDedo2PREMIUM = 0
    $ numEventClaireDirectorVisita2MeterleDedo3PREMIUM = True
    play sound "audio/effects/juegaBragas3.ogg"
    scene claireDirector2ndaVisita_113 with dissolve
    pause 0.02
    play sound "audio/effects/juegaBragas4.ogg"
    scene claireDirector2ndaVisita_112 with dissolve
    pause 0.02
    play sound "audio/effects/juegaBragas3.ogg"
    scene claireDirector2ndaVisita_111 with dissolve
    pause 0.02
    scene claireDirector2ndaVisita_110 with dissolve
    pause 0.02

    scene claireDirector2ndaVisita_130 with dissolve
    pause 0.4
    scene claireDirector2ndaVisita_130_ with dissolve
    pause 0.1
    scene claireDirector2ndaVisita_130__ with dissolve
    pause 0.1
    scene claireDirector2ndaVisita_131_ with dissolve
    pause 0.02
    while numEventClaireDirectorVisita2MeterleDedo2PREMIUM < 3:
        scene claireDirector2ndaVisita_131_ with dissolve
        pause 0.03
        play sound "audio/effects/juegaBragas4.ogg"
        scene claireDirector2ndaVisita_131 with dissolve
        pause 0.03
        scene claireDirector2ndaVisita_132 with dissolve
        pause 0.03
        play sound "audio/effects/juegaBragas6.ogg"
        scene claireDirector2ndaVisita_133 with dissolve
        if numEventClaireDirectorVisita2MeterleDedo2PREMIUM == 0:
            claire "oooouuhhhhhh...{p=0.6}{nw}"

        scene claireDirector2ndaVisita_134 with dissolve
        pause 0.4
        play sound "audio/effects/juegaBragasOrgasm3.ogg"
        scene claireDirector2ndaVisita_135 with dissolve
        if  numEventClaireDirectorVisita2MeterleDedo2PREMIUM == 0:
            claire "¡¡¡Ooooohhhhhh!!! {p=0.4}{nw}"
        if  numEventClaireDirectorVisita2MeterleDedo2PREMIUM == 2:
            play sound "audio/effects/juegaBragasOrgasm4.ogg"
            claire "¡¡¡Aaaahhhhhh!!! {p=0.4}{nw}"
        if numEventClaireDirectorVisita2MeterleDedo2PREMIUM == 1:
            directorUni "¿Tu novio no te deja satisfecha verdad? {p=1.3}{nw}"
        pause 0.05
        if numEventClaireDirectorVisita2MeterleDedo2PREMIUM == 0:
            scene claireDirector2ndaVisita_134 with dissolve
            pause 0.04
            play sound "audio/effects/juegaBragasOrgasm5.ogg"
            scene claireDirector2ndaVisita_135 with dissolve
            directorUni "¡Todo dentro puta! {p=1.0}{nw}"

        scene claireDirector2ndaVisita_132 with dissolve
        pause 0.4
        if numEventClaireDirectorVisita2MeterleDedo2PREMIUM == 2:
            scene claireDirector2ndaVisita_117 with dissolve
            pause 0.02
            play sound "audio/effects/orgasm3.ogg"
            scene claireDirector2ndaVisita_118 with dissolve
            pause 0.04
            scene claireDirector2ndaVisita_117 with dissolve
            pause 0.05
            play sound "audio/effects/orgasm4.ogg"
            scene claireDirector2ndaVisita_119 with dissolve
            claire "¡¡¡Ohhhhhh!!! {p=0.4}{nw}"
            pause 0.02
            scene claireDirector2ndaVisita_117 with dissolve
            pause 0.03
            play sound "audio/effects/orgasm5.ogg"
            scene claireDirector2ndaVisita_120 with dissolve
            pause 0.02
            scene claireDirector2ndaVisita_117 with dissolve
            pause 0.03
            scene claireDirector2ndaVisita_121 with dissolve
            clairePensa "No paro de correrme... {p=0.4}{nw}"


        $ numEventClaireDirectorVisita2MeterleDedo2PREMIUM = numEventClaireDirectorVisita2MeterleDedo2PREMIUM +1

    call menuMeterDedos from _call_menuMeterDedos_2
    return

label eventClaireDirectorVisita2Meterle3DedosPREMIUM():
    $ numEventClaireDirectorVisita2MeterleDedo3PREMIUM = 0
    scene claireDirector2ndaVisita_136 with dissolve
    directorUni "Ya estas mojadísima. ¡Baja la pierna!"
    play sound "audio/effects/golpeGritoPeque.ogg"
    scene claireDirector2ndaVisita_137 with dissolve
    directorUni "Acabaras suplicando que te folle."
    scene claireDirector2ndaVisita_138 with dissolve
    directorUni "Ya estas como una perra chorreando otra vez."
    directorUni "Tu necesitas mas."
    pause
    while numEventClaireDirectorVisita2MeterleDedo3PREMIUM < 3:
        if numEventClaireDirectorVisita2MeterleDedo3PREMIUM == 0:
            directorUni "A las putitas como tu hay que abrirles bien el coño. {p=1.9}{nw}"
            play sound "audio/effects/juegaBragasOrgasm6.ogg"
        scene claireDirector2ndaVisita_139 with dissolve
        if numEventClaireDirectorVisita2MeterleDedo3PREMIUM == 0:
            claire "¡¡¡Oooohhhhhh!!! {p=0.3}{nw}"
        play sound "audio/effects/juegaBragasOrgasm7.ogg"
        scene claireDirector2ndaVisita_140 with dissolve
        pause 0.3
        play sound "audio/effects/juegaBragasOrgasm8.ogg"
        scene claireDirector2ndaVisita_141 with dissolve
        if numEventClaireDirectorVisita2MeterleDedo3PREMIUM == 0:
            claire "¡¡¡No tan adentro Aaaaaaauuuhhhhhh!!! {p=1.0}{nw}"
        if numEventClaireDirectorVisita2MeterleDedo3PREMIUM == 1:
            claire "¡¡¡Aaaaaaahhhhhh!!! {p=0.4}{nw}"
        play sound "audio/effects/juegaBragasOrgasm9_.ogg"
        scene claireDirector2ndaVisita_142 with dissolve
        if numEventClaireDirectorVisita2MeterleDedo3PREMIUM == 0:
            directorUni "¡Tienes el coño ardiendo! {p=0.7}{nw}"
        if numEventClaireDirectorVisita2MeterleDedo3PREMIUM == 1:
            directorUni "¡Lo estas disfrutando mucho! {p=0.7}{nw}"
        pause 0.3
        play sound "audio/effects/juegaBragas10.ogg"
        scene claireDirector2ndaVisita_143 with dissolve
        if numEventClaireDirectorVisita2MeterleDedo3PREMIUM == 2:
            claire "¡¡¡Uhhhhhh!!! {p=0.3}{nw}"
        #pause 0.2
        play sound "audio/effects/juegaBragas6.ogg"
        scene claireDirector2ndaVisita_144 with dissolve
        pause 0.3
        if numEventClaireDirectorVisita2MeterleDedo3PREMIUM == 0:
            play sound "audio/effects/juegaBragasOrgasm8.ogg"
        else:
            play sound "audio/effects/juegaBragas7.ogg"

        scene claireDirector2ndaVisita_142 with dissolve
        pause 0.3
        play sound "audio/effects/juegaBragas6.ogg"
        if numEventClaireDirectorVisita2MeterleDedo3PREMIUM == 1:
            #play sound "audio/effects/juegaBragasOrgasm10.ogg"
            pause 0.7
        scene claireDirector2ndaVisita_141 with dissolve
        pause 0.2
        play sound "audio/effects/juegaBragasOrgasm5.ogg"
        scene claireDirector2ndaVisita_140 with dissolve
        pause 0.2

        if numEventClaireDirectorVisita2MeterleDedo3PREMIUM == 2:
            play sound "audio/effects/juegaBragasOrgasm8.ogg"
            claire "¡¡¡Aaaaaaahhhhhh!!! {p=0.4}{nw}"
        scene claireDirector2ndaVisita_142 with dissolve

        pause 0.3
        play sound "audio/effects/juegaBragas5.ogg"
        scene claireDirector2ndaVisita_144 with dissolve
        pause 0.1
        if numEventClaireDirectorVisita2MeterleDedo3PREMIUM == 2:
            scene claireDirector2ndaVisita_122 with dissolve
            pause 0.2
            play sound "audio/effects/golpecitoGemido3.ogg"
            scene claireDirector2ndaVisita_123 with dissolve
            pause 0.05
            scene claireDirector2ndaVisita_124 with dissolve
            pause 0.1
            play sound "audio/effects/golpecitoGemido5.ogg"
            scene claireDirector2ndaVisita_125 with dissolve
            claire "¡¡¡Uuuuuhhhhhh!!! {p=0.2}{nw}"
            scene claireDirector2ndaVisita_122 with dissolve
            pause 0.2
            play sound "audio/effects/golpecitoGemido3.ogg"
            scene claireDirector2ndaVisita_126 with dissolve
            claire "¡¡¡Auuuuhhhhhh!!! {p=0.1}{nw}"
            scene claireDirector2ndaVisita_122 with dissolve
            pause 0.2
            play sound "audio/effects/golpeMesa2.ogg"
            scene claireDirector2ndaVisita_129 with dissolve
            pause 0.02
            scene claireDirector2ndaVisita_122 with dissolve
            pause 0.2
            play sound "audio/effects/golpecitoGemido5.ogg"
            scene claireDirector2ndaVisita_123 with dissolve
            pause 0.02
            scene claireDirector2ndaVisita_124 with dissolve
            pause 0.1
            play sound "audio/effects/golpecitoGemido4.ogg"
            scene claireDirector2ndaVisita_125 with dissolve
            claire "¡¡¡Ouuuuhhhhhh!!! {p=0.4}{nw}"

        $ numEventClaireDirectorVisita2MeterleDedo3PREMIUM = numEventClaireDirectorVisita2MeterleDedo3PREMIUM +1
    $ numEventClaireDirectorVisita2MeterleDedo4PREMIUM = True
    call menuMeterDedos from _call_menuMeterDedos_3

    return

label eventClaireDirectorVisita2Meterle4DedosPREMIUM():
    scene claireDirector2ndaVisita_139 with dissolve
    #claire "¡¡¡Oooouuhhhhhh!!! {p=0.4}{nw}"
    play sound "audio/effects/juegaBragasOrgasm6.ogg"
    scene claireDirector2ndaVisita_140 with dissolve
    pause 0.1
    scene claireDirector2ndaVisita_141 with dissolve
    #claire "¡¡¡Aaaaaaahhhhhh!!! {p=0.4}{nw}"
    play sound "audio/effects/juegaBragasOrgasm8.ogg"
    scene claireDirector2ndaVisita_142 with dissolve
    #if numEventClaireDirectorVisita2MeterleDedo3PREMIUM == 0:
    #    directorUni "¡Tienes el coño ardiendo! {p=0.9}{nw}"
    #if numEventClaireDirectorVisita2MeterleDedo3PREMIUM == 1:
    #    directorUni "¡Lo estas disfrutando mucho! {p=0.9}{nw}"
    pause 0.1
    scene claireDirector2ndaVisita_143 with dissolve
    #claire "¡¡¡Uuuuhhhhhh!!! {p=0.3}{nw}"
    #pause 0.2
    play sound "audio/effects/juegaBragas5.ogg"
    scene claireDirector2ndaVisita_144 with dissolve
    pause 0.1
    scene claireDirector2ndaVisita_145 with dissolve
    directorUni "Eres una puta que necesita ser follada, mira cuantos chorros sueltas. No paras de correrte, esto te encanta."
    directorUni "Estas toda abierta de piernas sin quejarte y tu coño pide mas."
    play sound "audio/effects/llanto.ogg"
    scene claireDirector2ndaVisita_146 with dissolve
    claire "Por favor ya no puedo más..."
    scene claireDirector2ndaVisita_147 with dissolve
    directorUni "Si que puedes, mira esto..."
    play sound "audio/effects/juegaBragasOrgasm8.ogg"
    scene claireDirector2ndaVisita_148 with dissolve
    pause 0.1
    play sound "audio/effects/juegaBragas8.ogg"
    claire "¡¡¡OOOOOOHhhhhhh!!!! {p=0.7}{nw}"

    scene claireDirector2ndaVisita_149 with dissolve
    claire "{size=14}Me corroooooooooooo... {/size}"

    $ numEventClaireDirectorVisita2MeterleDedo4PREMIUM = 0
    while numEventClaireDirectorVisita2MeterleDedo4PREMIUM < 3:
        scene claireDirector2ndaVisita_150 with dissolve
        pause 0.4
        play sound "audio/effects/juegaBragasOrgasm11.ogg"
        scene claireDirector2ndaVisita_151 with dissolve
        pause 0.04
        play sound "audio/effects/juegaBragasOrgasm12.ogg"
        scene claireDirector2ndaVisita_152 with dissolve
        if numEventClaireDirectorVisita2MeterleDedo4PREMIUM == 0:
            claire "¡¡¡AAAAAAaahhhhhhh!!!! {p=0.3}{nw}"
        play sound "audio/effects/juegaBragasOrgasm14.ogg"
        scene claireDirector2ndaVisita_153 with dissolve
        if numEventClaireDirectorVisita2MeterleDedo4PREMIUM == 0:
            directorUni "¡Así! toda abiertaaa! {p=0.9}{nw}"
        play sound "audio/effects/juegaBragas7.ogg"
        scene claireDirector2ndaVisita_154 with dissolve
        pause 0.04
        if numEventClaireDirectorVisita2MeterleDedo4PREMIUM == 0:
            play sound "audio/effects/juegaBragasOrgasm15.ogg"
        else:
            play sound "audio/effects/juegaBragasOrgasm20.ogg"
        scene claireDirector2ndaVisita_155 with dissolve
        pause 0.05
        scene claireDirector2ndaVisita_156 with dissolve
        play sound "audio/effects/juegaBragasOrgasm22.ogg"
        scene claireDirector2ndaVisita_157 with dissolve
        pause 0.01
        if numEventClaireDirectorVisita2MeterleDedo4PREMIUM == 0:
            play sound "audio/effects/juegaBragasOrgasm21.ogg"
        else:
            play sound "audio/effects/juegaBragasOrgasm15.ogg"
        scene claireDirector2ndaVisita_158 with dissolve
        pause 0.3
        play sound "audio/effects/juegaBragasOrgasm19.ogg"
        if numEventClaireDirectorVisita2MeterleDedo4PREMIUM == 2:
            scene claireDirector2ndaVisita_159 with dissolve
            pause 0.3
            play sound "audio/effects/squirt.ogg"
            scene claireDirector2ndaVisita_160 with dissolve
            directorUni "¡Todo adentrooooo! {p=1.5}{nw}"
            scene claireDirector2ndaVisita_161 with dissolve
            claire "{size=25} ¡¡¡OOOOOOooooooooouuHhhhhhh me corrooooo!!!! {p=1.2}{nw}{/size=25}"
            scene claireDirector2ndaVisita_162 with dissolve
            play sound "audio/effects/juegaBragasOrgasm3.ogg"
            directorUni "¡Me has mojado toda la mesa!"
            play sound "audio/effects/juegaBragasOrgasm.ogg"
            scene claireDirector2ndaVisita_163 with dissolve
            pause 0.1
            claire "{size=14} No puedo mas... {/size}"
            play sound "audio/effects/sacarDedos.ogg"
            pause 0.1
            scene claireDirector2ndaVisita_164 with dissolve
            directorUni "Este es el coño que te mereces."
            play sound "audio/effects/orgasm4.ogg"
            scene claireDirector2ndaVisita_165 with dissolve
            claire "ooohhhhhh"
            scene claireDirector2ndaVisita_166 with dissolve
            pause
            scene claireDirector2ndaVisita_167 with dissolve
            directorUni "Te dije que esto es lo que necesitabas, hahahaha"
            pause
            narrador "Volvemos con el protagonista..."
        $ numEventClaireDirectorVisita2MeterleDedo4PREMIUM = numEventClaireDirectorVisita2MeterleDedo4PREMIUM +1
    call nextTiempoDay(0) from _call_nextTiempoDay_29
    return


#REPRODUCTOR


label eventClaireDirectorVisita2EscenaReproductor():
    call deshabilitaObjetosPersonas
    stop music
    scene claireDirector2ndaVisita_0
    pause
    scene claireDirector2ndaVisita_1 with dissolve
    pause
    scene claireDirector2ndaVisita_2 with dissolve
    pause
    scene claireDirector2ndaVisita_3 with hpunch
    play sound "audio/effects/golpeAu1.ogg"
    "¡¡¡Pam!!!"
    stop music fadeout 3.0
    scene claireDirector2ndaVisita_4 with dissolve
    claire "¡¡Ay!! que daño!"
    scene claireDirector2ndaVisita_5 with dissolve
    protaPensa "Oh dios es Claire!, ¡joder sus bragas!"
    scene claireDirector2ndaVisita_6 with dissolve
    pause
    scene claireDirector2ndaVisita_7 with dissolve
    claire "Que haces tonto! tenías que ser tú, siempre en medio."
    scene claireDirector2ndaVisita_8 with dissolve
    prota "Perdón no te vi. ¿Estas bien?"
    scene claireDirector2ndaVisita_9 with dissolve
    claire "Sal de mi camino."
    scene claireDirector2ndaVisita_10 with dissolve
    protaPensa "¿A dónde ira?"
    play music "audio/music/Marty_Gots_a_Plan.ogg" fadein 2.0

    scene claireDirector2ndaVisita_11 with dissolve
    pause
    scene claireDirector2ndaVisita_12 with dissolve
    pause
    scene claireDirector2ndaVisita_13 with dissolve
    pause
    scene claireDirector2ndaVisita_14 with dissolve
    protaPensa "¿Se va a ver al director?"
    scene claireDirector2ndaVisita_15 with dissolve
    protaPensa "Apenas puedo ver nada..."
    scene claireDirector2ndaVisita_16 with dissolve
    protaPensa "Ahora veo un poco, que hacen?"
    scene claireDirector2ndaVisita_28 with dissolve
    pause
    "Atención escena NTR, quieres activarla?"
    menu:
        "SI":
            $ ntrGame = True
        "NO":
            $ ntrGame = False
    if ntrGame == True:
        scene claireDirector2ndaVisita_29 with dissolve
        stop music fadeout 3.0
        directorUni "Bien así me gusta, que seas puntual."
        scene claireDirector2ndaVisita_30 with dissolve
        directorUni "¿Has venido sin sujetador como te pedí?"
        scene claireDirector2ndaVisita_31 with dissolve
        directorUni "No te oigo..."
        scene claireDirector2ndaVisita_32 with dissolve
        play music "audio/music/DangerStorm_kevinMacLeod.ogg" fadein 2.0
        claire "Si..."
        scene claireDirector2ndaVisita_33 with dissolve
        directorUni "Perfecto."
        #play sound "audio/effects/tiraraMesa.ogg"
        play sound "audio/effects/susto.ogg"
        scene claireDirector2ndaVisita_34 with hpunch
        directorUni "Pero tengo que comprobarlo..."
        scene claireDirector2ndaVisita_35 with dissolve
        claire "Esta bien... Pero esta vez sin pasarse de los límites..."
        scene claireDirector2ndaVisita_36 with dissolve
        directorUni "Claro, seré bueno..."
        scene claireDirector2ndaVisita_37 with dissolve
        pause
        play sound "audio/effects/roze1.ogg"
        scene claireDirector2ndaVisita_38 with dissolve
        pause
        scene claireDirector2ndaVisita_39 with dissolve
        pause
        play sound "audio/effects/roze3.ogg"
        scene claireDirector2ndaVisita_40 with dissolve
        directorUni "¿Y estos pezones? ¿Ya estas excitada?"
        directorUni "Necesitabas venir..."
        scene claireDirector2ndaVisita_41 with dissolve
        pause
        scene claireDirector2ndaVisita_18 with dissolve
        stop music fadeout 2.0
        protaPensa "¡¡¡Le ha levantado la camisa y se le ven las tetas!!!"
        scene claireDirector2ndaVisita_41 with dissolve
        pause
        scene claireDirector2ndaVisita_42 with dissolve
        play music "audio/music/ChillWave_kevinMacLeod.ogg" fadein 2.0
        directorUni "Ahora vamos a jugar un poco..."
        scene claireDirector2ndaVisita_43 with dissolve
        directorUni "No te oigo..."
        scene claireDirector2ndaVisita_44 with dissolve
        claire "Si..."
        scene claireDirector2ndaVisita_45 with dissolve
        pause
        scene claireDirector2ndaVisita_46 with dissolve
        pause
        scene claireDirector2ndaVisita_20 with dissolve
        protaPensa "¡Se está dejando manosear las tetas por el director!! ¡Menuda zorra!"
        scene claireDirector2ndaVisita_46 with dissolve
        pause
        $ manosearPechosClaireDirector2 = False
        $ manosearPechosClaireDirector2Continuar = False
        call menuManoseoPechosClaireDirectorVisita2EscenaReproductor()

    else:
        scene claireDirector2ndaVisita_16 with dissolve
        "El director le esta tocando la espalda?"
        scene claireDirector2ndaVisita_21 with dissolve
        stop music fadeout 3.0
        jack "¿Tu payaso, te has enamorado del director? Eres un voyeur."
        play sound "audio/effects/discorayado.ogg"
        scene claireDirector2ndaVisita_22 with dissolve
        protaPensa "¡¡Joder esta su novio aquí!!"
        scene claireDirector2ndaVisita_23 with dissolve
        jack "Mosquito te hice una pregunta. ¿Has visto a mi novia?"
        scene claireDirector2ndaVisita_24 with dissolve
        pause
        play sound "audio/effects/puertaCambiandose.ogg"
        scene claireDirector2ndaVisita_25_NONTR with dissolve
        pause
        scene claireDirector2ndaVisita_26_NONTR with dissolve
        claire "Que es este escandalo?"
        pause
        scene claireDirector2ndaVisita_27_NONTR with dissolve
        jack "Te estaba buscando."
        scene claireDirector2ndaVisita_28_NONTR with dissolve
        claire "Estaba resolviendo unos asuntos de la Universidad."

        show screen escenasClaire("Claire")

    return


label menuManoseoPechosClaireDirectorVisita2EscenaReproductor():
    menu:
        "Manosear pechos 1":
            call manoseo1PechosClaireDirectorVisita2BucleEscenaReproductor
        "Manosear pechos 2" if manosearPechosClaireDirector2 == True:
            call manoseo2PechosClaireDirectorVisita2BucleEscenaReproductor
        "Continuar" if manosearPechosClaireDirector2Continuar == True:
            call manoseo2PechosClaireDirectorVisita2ContinuarEscenaReproductor

    return

label manoseo1PechosClaireDirectorVisita2BucleEscenaReproductor():
    $ manosearPechosClaireDirector2 = True
    $ numManoseo1PechosClaireDirectorVisita2Bucle = 0
    while numManoseo1PechosClaireDirectorVisita2Bucle < 2:
        scene claireDirector2ndaVisita_46 with dissolve
        pause 0.1
        scene claireDirector2ndaVisita_47 with dissolve
        if numManoseo1PechosClaireDirectorVisita2Bucle == 0:
            play sound "audio/effects/roze3.ogg"
            directorUni "Me encantan estas tetas y mas me gusta que seas mi putita.{p=1.8}{nw}"
            pause 0.5
        pause 0.1
        scene claireDirector2ndaVisita_48 with dissolve
        pause 0.1
        scene claireDirector2ndaVisita_49 with dissolve
        pause 0.2
        scene claireDirector2ndaVisita_50 with dissolve
        pause 0.1
        scene claireDirector2ndaVisita_57 with dissolve
        pause 0.1
        scene claireDirector2ndaVisita_58 with dissolve
        play sound "audio/effects/dolor2.ogg"
        pause 0.4
        scene claireDirector2ndaVisita_57 with dissolve
        pause 0.1
        if numManoseo1PechosClaireDirectorVisita2Bucle == 1:
            directorUni "Y estos pezones los voy a chupar enteros.{p=1.5}{nw}"
        scene claireDirector2ndaVisita_58 with dissolve
        play sound "audio/effects/orgasm2.ogg"
        pause 0.1
        if numManoseo1PechosClaireDirectorVisita2Bucle == 1:
            play sound "audio/effects/dolor1.ogg"
            scene claireDirector2ndaVisita_59 with dissolve
            pause 0.1
            scene claireDirector2ndaVisita_47 with dissolve
            pause 0.3
        else:
            play sound "audio/effects/dolor1.ogg"
            scene claireDirector2ndaVisita_59 with dissolve
            pause 0.1
            scene claireDirector2ndaVisita_46 with dissolve
            pause 0.1
            play sound "audio/effects/orgasm4.ogg"
            scene claireDirector2ndaVisita_59 with dissolve
            pause 0.1
        $ numManoseo1PechosClaireDirectorVisita2Bucle = numManoseo1PechosClaireDirectorVisita2Bucle +1
    call menuManoseoPechosClaireDirectorVisita2EscenaReproductor()
    return


label manoseo2PechosClaireDirectorVisita2BucleEscenaReproductor():
    $ numManoseo2PechosClaireDirectorVisita2Bucle = 0
    while numManoseo2PechosClaireDirectorVisita2Bucle < 2:
        scene claireDirector2ndaVisita_46_ with dissolve
        pause 0.1
        scene claireDirector2ndaVisita_47_ with dissolve
        if numManoseo2PechosClaireDirectorVisita2Bucle == 0:
            scene claireDirector2ndaVisita_51 with dissolve
            pause 0.7
            scene claireDirector2ndaVisita_54 with dissolve
            directorUni "¿Te gusta que te los apriete fuerte verdad?.{p=1.8}{nw}"
            pause 0.5
        scene claireDirector2ndaVisita_47_ with dissolve
        pause 0.1
        scene claireDirector2ndaVisita_48_ with dissolve
        pause 0.1
        scene claireDirector2ndaVisita_49_ with dissolve
        pause 0.1
        scene claireDirector2ndaVisita_50_ with dissolve
        pause 0.1
        if numManoseo2PechosClaireDirectorVisita2Bucle == 1:
            scene claireDirector2ndaVisita_57_ with dissolve
            pause 0.1
            play sound "audio/effects/orgasm2.ogg"
            scene claireDirector2ndaVisita_58_ with dissolve
            pause 0.5
            scene claireDirector2ndaVisita_57_ with dissolve
            pause 0.1
            scene claireDirector2ndaVisita_50_ with dissolve
            pause 0.1
            play sound "audio/effects/orgasm3.ogg"
            scene claireDirector2ndaVisita_59_ with dissolve
            pause 1
            scene claireDirector2ndaVisita_56 with dissolve
            directorUni "¿Así de fuerte o quieres mas?{p=1.2}{nw}"
        scene claireDirector2ndaVisita_57_ with dissolve
        pause 0.1
        scene claireDirector2ndaVisita_58_ with dissolve
        play sound "audio/effects/orgasm5.ogg"
        pause 0.1
        scene claireDirector2ndaVisita_57_ with dissolve
        pause 0.1
        $ numManoseo2PechosClaireDirectorVisita2Bucle = numManoseo2PechosClaireDirectorVisita2Bucle +1
    $ manosearPechosClaireDirector2Continuar = True
    call menuManoseoPechosClaireDirectorVisita2EscenaReproductor
    return

label manoseo2PechosClaireDirectorVisita2ContinuarEscenaReproductor():
    scene claireDirector2ndaVisita_60 with dissolve
    pause
    scene claireDirector2ndaVisita_61 with dissolve
    directorUni "Vamos a mirar un poco por abajo..."
    scene claireDirector2ndaVisita_62 with dissolve
    claire "¿Qué?"
    play sound "audio/effects/quitarRopa.ogg"
    scene claireDirector2ndaVisita_63 with dissolve
    pause
    scene claireDirector2ndaVisita_64 with dissolve
    pause
    scene claireDirector2ndaVisita_65 with dissolve
    pause
    scene claireDirector2ndaVisita_66 with dissolve
    claire "Dijimos que no pasaríamos de la raya."
    scene claireDirector2ndaVisita_67 with dissolve
    directorUni "Solo mirar un poco y ya está."
    scene claireDirector2ndaVisita_68 with dissolve
    pause
    scene claireDirector2ndaVisita_69 with dissolve
    directorUni "Sube la pierna"
    play sound "audio/effects/tiraraMesa.ogg"
    scene claireDirector2ndaVisita_70 with dissolve
    claire "¿Porque me pones así?"
    scene claireDirector2ndaVisita_71 with dissolve
    pause
    scene claireDirector2ndaVisita_72 with dissolve
    pause
    play sound "audio/effects/golpecitoGemido1.ogg"
    scene claireDirector2ndaVisita_73 with dissolve
    pause
    scene claireDirector2ndaVisita_74 with dissolve
    pause
    play sound "audio/effects/roze3.ogg"
    scene claireDirector2ndaVisita_75 with dissolve
    pause
    scene claireDirector2ndaVisita_76 with dissolve
    clairePensa "Otra vez..."
    scene claireDirector2ndaVisita_77 with dissolve
    pause
    scene claireDirector2ndaVisita_78 with dissolve
    pause
    play sound "audio/effects/abrirCony4.ogg"
    scene claireDirector2ndaVisita_79 with dissolve
    pause
    play sound "audio/effects/dedoConyo.ogg"
    scene claireDirector2ndaVisita_80 with dissolve
    pause
    scene claireDirector2ndaVisita_19 with dissolve
    protaPensa "¡¡¡Oh dios mio!!!"
    scene claireDirector2ndaVisita_21 with dissolve
    stop music fadeout 3.0
    jack "¿Tu payaso, te has enamorado del director? Eres un voyeur."
    play sound "audio/effects/discorayado.ogg"
    scene claireDirector2ndaVisita_22 with dissolve
    protaPensa "¡¡Joder esta su novio aquí!!"
    scene claireDirector2ndaVisita_23 with dissolve
    jack "Mosquito te hice una pregunta. ¿Has visto a mi novia?"
    scene claireDirector2ndaVisita_24 with dissolve
    protaPensa "¡Joder! su novia la está manoseando el director y este tonto buscándola."
    scene claireDirector2ndaVisita_25 with dissolve
    prota "No, la he visto."
    play sound "audio/effects/golpe1.ogg"
    scene claireDirector2ndaVisita_26 with dissolve
    jack "¡Pues deja de ser un voyeur y ven conmigo a buscarla, enano!"
    scene claireDirector2ndaVisita_27 with dissolve
    protaPensa "No he podido ver más una lástima... Me guardare el secreto por ahora..."
    protaPensa "Este tio es muy tonto hahaha"
    call eventClaireDirectorVisita2PREMIUMEscenaReproductor
    return


label eventClaireDirectorVisita2PREMIUMEscenaReproductor():
    play music "audio/music/ChillWave_kevinMacLeod.ogg" fadein 2.0
    scene claireDirector2ndaVisita_81
    pause
    scene claireDirector2ndaVisita_82 with dissolve
    pause
    scene claireDirector2ndaVisita_83 with dissolve
    pause
    play sound "audio/effects/juegaBragas2.ogg"
    scene claireDirector2ndaVisita_84 with dissolve
    pause
    scene claireDirector2ndaVisita_85 with dissolve
    pause
    scene claireDirector2ndaVisita_86 with dissolve
    directorUni "¡No te muevas!"
    play sound "audio/effects/golpecitoGemido1.ogg"
    scene claireDirector2ndaVisita_87 with dissolve
    "¡¡Splash!!"
    scene claireDirector2ndaVisita_88 with dissolve
    pause
    scene claireDirector2ndaVisita_89 with dissolve
    pause
    scene claireDirector2ndaVisita_90 with dissolve
    directorUni "Ven aquí."
    play sound "audio/effects/roze1.ogg"
    scene claireDirector2ndaVisita_91 with dissolve
    pause
    play sound "audio/effects/roze3.ogg"
    scene claireDirector2ndaVisita_92 with dissolve
    claire "¿Que haces?"
    scene claireDirector2ndaVisita_93 with dissolve
    pause
    play sound "audio/effects/orgasm5.ogg"
    scene claireDirector2ndaVisita_94 with dissolve
    directorUni "¡Silencio!"
    scene claireDirector2ndaVisita_95 with dissolve
    pause
    scene claireDirector2ndaVisita_96 with dissolve
    claire "Espera..."
    play sound "audio/effects/golpeAu1.ogg"
    scene claireDirector2ndaVisita_97 with hpunch
    directorUni "Ahora te voy a enseñar lo que necesitas."
    scene claireDirector2ndaVisita_98 with dissolve
    claire "¡Me voy a caer!"
    scene claireDirector2ndaVisita_99 with dissolve
    directorUni "No te preocupes yo te voy a coger bien."
    scene claireDirector2ndaVisita_100 with dissolve
    directorUni "Ahora colócate como te diga, que te voy abrir las piernas."
    play sound "audio/effects/roze3.ogg"
    scene claireDirector2ndaVisita_101 with dissolve
    pause
    play sound "audio/effects/golpecitoGemido2.ogg"
    scene claireDirector2ndaVisita_102 with dissolve
    pause
    scene claireDirector2ndaVisita_103 with dissolve
    pause
    scene claireDirector2ndaVisita_104 with dissolve
    directorUni "Voy a enseñarte lo que necesita tu coñito. Siempre vienes con la excusa, pero sabes muy bien que te encanta esto y acabas chorreando."
    play sound "audio/effects/asentir2golpe.ogg"
    scene claireDirector2ndaVisita_105 with dissolve
    claire "{size=14} no... {/size=14}"
    play sound "audio/effects/roze3.ogg"
    scene claireDirector2ndaVisita_106 with dissolve
    pause
    play sound "audio/effects/susto1.ogg"
    scene claireDirector2ndaVisita_107 with dissolve
    claire "¡Que me caigo!"
    scene claireDirector2ndaVisita_108 with dissolve
    directorUni "Tienes el clitoris hinchadísimo de lo cachonda que estas!"
    scene claireDirector2ndaVisita_109 with dissolve
    pause
    $ numEventClaireDirectorVisita2MeterleDedo1PREMIUM = False
    $ numEventClaireDirectorVisita2MeterleDedo3PREMIUM = False
    $ numEventClaireDirectorVisita2MeterleDedo4PREMIUM = False
    call menuMeterDedosEscenaReproductor()

    return

label menuMeterDedosEscenaReproductor():
    menu:
        "Meterle el dedo":
            call eventClaireDirectorVisita2MeterleDedo1PREMIUMEscenaReproductor
            return

        "Meterle 2 dedos" if numEventClaireDirectorVisita2MeterleDedo1PREMIUM == True:
            call eventClaireDirectorVisita2Meterle2DedosPREMIUMEscenaReproductor
            return
        "Meterle 3 dedos" if numEventClaireDirectorVisita2MeterleDedo3PREMIUM == True:
            call eventClaireDirectorVisita2Meterle3DedosPREMIUMEscenaReproductor
            return
        "¡Mas fuerte y que se corra! "if numEventClaireDirectorVisita2MeterleDedo4PREMIUM == True:
            call eventClaireDirectorVisita2Meterle4DedosPREMIUMEscenaReproductor
            return

label eventClaireDirectorVisita2MeterleDedo1PREMIUMEscenaReproductor():
    $ numEventClaireDirectorVisita2MeterleDedo1PREMIUM = 0
    scene claireDirector2ndaVisita_110 with dissolve
    pause
    while numEventClaireDirectorVisita2MeterleDedo1PREMIUM < 2:
        scene claireDirector2ndaVisita_111 with dissolve
        pause 0.01
        play sound "audio/effects/juegaBragas4.ogg"
        scene claireDirector2ndaVisita_112 with dissolve
        pause 0.05
        play sound "audio/effects/juegaBragas3.ogg"
        scene claireDirector2ndaVisita_113 with dissolve
        pause 0.02
        play sound "audio/effects/juegaBragasOrgasm.ogg"
        scene claireDirector2ndaVisita_114 with dissolve
        #pause 0.05
        if numEventClaireDirectorVisita2MeterleDedo1PREMIUM == 0:
            claire "oooohhhhhh...{p=0.8}{nw}"
            play sound "audio/effects/juegaBragasOrgasm2.ogg"
        if numEventClaireDirectorVisita2MeterleDedo1PREMIUM == 1:
            claire "aahhhhhh...{p=0.6}{nw}"
        scene claireDirector2ndaVisita_115 with dissolve
        pause 0.01
        play sound "audio/effects/juegaBragas4.ogg"
        scene claireDirector2ndaVisita_112 with dissolve

        pause 0.05
        play sound "audio/effects/juegaBragas5.ogg"
        scene claireDirector2ndaVisita_114 with dissolve
        if numEventClaireDirectorVisita2MeterleDedo1PREMIUM == 0:
            directorUni "Vas a ver como te dejo el coñito {p=1.1}{nw}"
        scene claireDirector2ndaVisita_115 with dissolve
        pause 0.02
        play sound "audio/effects/juegaBragas3.ogg"
        scene claireDirector2ndaVisita_112 with dissolve
        pause 0.05
        if numEventClaireDirectorVisita2MeterleDedo1PREMIUM == 1:
            scene claireDirector2ndaVisita_113 with dissolve
            pause 0.02
            play sound "audio/effects/juegaBragasOrgasm2.ogg"
            scene claireDirector2ndaVisita_115 with dissolve
            pause 0.03
        play sound "audio/effects/juegaBragas4.ogg"
        scene claireDirector2ndaVisita_114 with dissolve
        if numEventClaireDirectorVisita2MeterleDedo1PREMIUM == 1:
            directorUni "¿Ya sabe tu novio que te gusta venir aqui? {p=1.3}{nw}"

        $ numEventClaireDirectorVisita2MeterleDedo1PREMIUM = numEventClaireDirectorVisita2MeterleDedo1PREMIUM +1
    $ numEventClaireDirectorVisita2MeterleDedo1PREMIUM = True
    call menuMeterDedosEscenaReproductor

    return

label eventClaireDirectorVisita2Meterle2DedosPREMIUMEscenaReproductor():
    $ numEventClaireDirectorVisita2MeterleDedo2PREMIUM = 0
    $ numEventClaireDirectorVisita2MeterleDedo3PREMIUM = True
    play sound "audio/effects/juegaBragas3.ogg"
    scene claireDirector2ndaVisita_113 with dissolve
    pause 0.02
    play sound "audio/effects/juegaBragas4.ogg"
    scene claireDirector2ndaVisita_112 with dissolve
    pause 0.02
    play sound "audio/effects/juegaBragas3.ogg"
    scene claireDirector2ndaVisita_111 with dissolve
    pause 0.02
    scene claireDirector2ndaVisita_110 with dissolve
    pause 0.02

    scene claireDirector2ndaVisita_130 with dissolve
    pause 0.4
    scene claireDirector2ndaVisita_130_ with dissolve
    pause 0.1
    scene claireDirector2ndaVisita_130__ with dissolve
    pause 0.1
    scene claireDirector2ndaVisita_131_ with dissolve
    pause 0.02
    while numEventClaireDirectorVisita2MeterleDedo2PREMIUM < 3:
        scene claireDirector2ndaVisita_131_ with dissolve
        pause 0.03
        play sound "audio/effects/juegaBragas4.ogg"
        scene claireDirector2ndaVisita_131 with dissolve
        pause 0.03
        scene claireDirector2ndaVisita_132 with dissolve
        pause 0.03
        play sound "audio/effects/juegaBragas6.ogg"
        scene claireDirector2ndaVisita_133 with dissolve
        if numEventClaireDirectorVisita2MeterleDedo2PREMIUM == 0:
            claire "oooouuhhhhhh...{p=0.6}{nw}"

        scene claireDirector2ndaVisita_134 with dissolve
        pause 0.4
        play sound "audio/effects/juegaBragasOrgasm3.ogg"
        scene claireDirector2ndaVisita_135 with dissolve
        if  numEventClaireDirectorVisita2MeterleDedo2PREMIUM == 0:
            claire "¡¡¡Ooooohhhhhh!!! {p=0.4}{nw}"
        if  numEventClaireDirectorVisita2MeterleDedo2PREMIUM == 2:
            play sound "audio/effects/juegaBragasOrgasm4.ogg"
            claire "¡¡¡Aaaahhhhhh!!! {p=0.4}{nw}"
        if numEventClaireDirectorVisita2MeterleDedo2PREMIUM == 1:
            directorUni "¿Tu novio no te deja satisfecha verdad? {p=1.3}{nw}"
        pause 0.05
        if numEventClaireDirectorVisita2MeterleDedo2PREMIUM == 0:
            scene claireDirector2ndaVisita_134 with dissolve
            pause 0.04
            play sound "audio/effects/juegaBragasOrgasm5.ogg"
            scene claireDirector2ndaVisita_135 with dissolve
            directorUni "¡Todo dentro puta! {p=1.0}{nw}"

        scene claireDirector2ndaVisita_132 with dissolve
        pause 0.4
        if numEventClaireDirectorVisita2MeterleDedo2PREMIUM == 2:
            scene claireDirector2ndaVisita_117 with dissolve
            pause 0.02
            play sound "audio/effects/orgasm3.ogg"
            scene claireDirector2ndaVisita_118 with dissolve
            pause 0.04
            scene claireDirector2ndaVisita_117 with dissolve
            pause 0.05
            play sound "audio/effects/orgasm4.ogg"
            scene claireDirector2ndaVisita_119 with dissolve
            claire "¡¡¡Ohhhhhh!!! {p=0.4}{nw}"
            pause 0.02
            scene claireDirector2ndaVisita_117 with dissolve
            pause 0.03
            play sound "audio/effects/orgasm5.ogg"
            scene claireDirector2ndaVisita_120 with dissolve
            pause 0.02
            scene claireDirector2ndaVisita_117 with dissolve
            pause 0.03
            scene claireDirector2ndaVisita_121 with dissolve
            clairePensa "No paro de correrme... {p=0.4}{nw}"


        $ numEventClaireDirectorVisita2MeterleDedo2PREMIUM = numEventClaireDirectorVisita2MeterleDedo2PREMIUM +1

    call menuMeterDedosEscenaReproductor
    return

label eventClaireDirectorVisita2Meterle3DedosPREMIUMEscenaReproductor():
    $ numEventClaireDirectorVisita2MeterleDedo3PREMIUM = 0
    scene claireDirector2ndaVisita_136 with dissolve
    directorUni "Ya estas mojadísima. ¡Baja la pierna!"
    play sound "audio/effects/golpeGritoPeque.ogg"
    scene claireDirector2ndaVisita_137 with dissolve
    directorUni "Acabaras suplicando que te folle."
    scene claireDirector2ndaVisita_138 with dissolve
    directorUni "Ya estas como una perra chorreando otra vez."
    directorUni "Tu necesitas mas."
    pause
    while numEventClaireDirectorVisita2MeterleDedo3PREMIUM < 3:
        if numEventClaireDirectorVisita2MeterleDedo3PREMIUM == 0:
            directorUni "A las putitas como tu hay que abrirles bien el coño. {p=1.9}{nw}"
            play sound "audio/effects/juegaBragasOrgasm6.ogg"
        scene claireDirector2ndaVisita_139 with dissolve
        if numEventClaireDirectorVisita2MeterleDedo3PREMIUM == 0:
            claire "¡¡¡Oooohhhhhh!!! {p=0.3}{nw}"
        play sound "audio/effects/juegaBragasOrgasm7.ogg"
        scene claireDirector2ndaVisita_140 with dissolve
        pause 0.3
        play sound "audio/effects/juegaBragasOrgasm8.ogg"
        scene claireDirector2ndaVisita_141 with dissolve
        if numEventClaireDirectorVisita2MeterleDedo3PREMIUM == 0:
            claire "¡¡¡No tan adentro Aaaaaaauuuhhhhhh!!! {p=1.0}{nw}"
        if numEventClaireDirectorVisita2MeterleDedo3PREMIUM == 1:
            claire "¡¡¡Aaaaaaahhhhhh!!! {p=0.4}{nw}"
        play sound "audio/effects/juegaBragasOrgasm9_.ogg"
        scene claireDirector2ndaVisita_142 with dissolve
        if numEventClaireDirectorVisita2MeterleDedo3PREMIUM == 0:
            directorUni "¡Tienes el coño ardiendo! {p=0.7}{nw}"
        if numEventClaireDirectorVisita2MeterleDedo3PREMIUM == 1:
            directorUni "¡Lo estas disfrutando mucho! {p=0.7}{nw}"
        pause 0.3
        play sound "audio/effects/juegaBragas10.ogg"
        scene claireDirector2ndaVisita_143 with dissolve
        if numEventClaireDirectorVisita2MeterleDedo3PREMIUM == 2:
            claire "¡¡¡Uhhhhhh!!! {p=0.3}{nw}"
        #pause 0.2
        play sound "audio/effects/juegaBragas6.ogg"
        scene claireDirector2ndaVisita_144 with dissolve
        pause 0.3
        if numEventClaireDirectorVisita2MeterleDedo3PREMIUM == 0:
            play sound "audio/effects/juegaBragasOrgasm8.ogg"
        else:
            play sound "audio/effects/juegaBragas7.ogg"

        scene claireDirector2ndaVisita_142 with dissolve
        pause 0.3
        play sound "audio/effects/juegaBragas6.ogg"
        if numEventClaireDirectorVisita2MeterleDedo3PREMIUM == 1:
            #play sound "audio/effects/juegaBragasOrgasm10.ogg"
            pause 0.7
        scene claireDirector2ndaVisita_141 with dissolve
        pause 0.2
        play sound "audio/effects/juegaBragasOrgasm5.ogg"
        scene claireDirector2ndaVisita_140 with dissolve
        pause 0.2

        if numEventClaireDirectorVisita2MeterleDedo3PREMIUM == 2:
            play sound "audio/effects/juegaBragasOrgasm8.ogg"
            claire "¡¡¡Aaaaaaahhhhhh!!! {p=0.4}{nw}"
        scene claireDirector2ndaVisita_142 with dissolve

        pause 0.3
        play sound "audio/effects/juegaBragas5.ogg"
        scene claireDirector2ndaVisita_144 with dissolve
        pause 0.1
        if numEventClaireDirectorVisita2MeterleDedo3PREMIUM == 2:
            scene claireDirector2ndaVisita_122 with dissolve
            pause 0.2
            play sound "audio/effects/golpecitoGemido3.ogg"
            scene claireDirector2ndaVisita_123 with dissolve
            pause 0.05
            scene claireDirector2ndaVisita_124 with dissolve
            pause 0.1
            play sound "audio/effects/golpecitoGemido5.ogg"
            scene claireDirector2ndaVisita_125 with dissolve
            claire "¡¡¡Uuuuuhhhhhh!!! {p=0.2}{nw}"
            scene claireDirector2ndaVisita_122 with dissolve
            pause 0.2
            play sound "audio/effects/golpecitoGemido3.ogg"
            scene claireDirector2ndaVisita_126 with dissolve
            claire "¡¡¡Auuuuhhhhhh!!! {p=0.1}{nw}"
            scene claireDirector2ndaVisita_122 with dissolve
            pause 0.2
            play sound "audio/effects/golpeMesa2.ogg"
            scene claireDirector2ndaVisita_129 with dissolve
            pause 0.02
            scene claireDirector2ndaVisita_122 with dissolve
            pause 0.2
            play sound "audio/effects/golpecitoGemido5.ogg"
            scene claireDirector2ndaVisita_123 with dissolve
            pause 0.02
            scene claireDirector2ndaVisita_124 with dissolve
            pause 0.1
            play sound "audio/effects/golpecitoGemido4.ogg"
            scene claireDirector2ndaVisita_125 with dissolve
            claire "¡¡¡Ouuuuhhhhhh!!! {p=0.4}{nw}"

        $ numEventClaireDirectorVisita2MeterleDedo3PREMIUM = numEventClaireDirectorVisita2MeterleDedo3PREMIUM +1
    $ numEventClaireDirectorVisita2MeterleDedo4PREMIUM = True
    call menuMeterDedosEscenaReproductor

    return

label eventClaireDirectorVisita2Meterle4DedosPREMIUMEscenaReproductor():
    scene claireDirector2ndaVisita_139 with dissolve
    #claire "¡¡¡Oooouuhhhhhh!!! {p=0.4}{nw}"
    play sound "audio/effects/juegaBragasOrgasm6.ogg"
    scene claireDirector2ndaVisita_140 with dissolve
    pause 0.1
    scene claireDirector2ndaVisita_141 with dissolve
    #claire "¡¡¡Aaaaaaahhhhhh!!! {p=0.4}{nw}"
    play sound "audio/effects/juegaBragasOrgasm8.ogg"
    scene claireDirector2ndaVisita_142 with dissolve
    #if numEventClaireDirectorVisita2MeterleDedo3PREMIUM == 0:
    #    directorUni "¡Tienes el coño ardiendo! {p=0.9}{nw}"
    #if numEventClaireDirectorVisita2MeterleDedo3PREMIUM == 1:
    #    directorUni "¡Lo estas disfrutando mucho! {p=0.9}{nw}"
    pause 0.1
    scene claireDirector2ndaVisita_143 with dissolve
    #claire "¡¡¡Uuuuhhhhhh!!! {p=0.3}{nw}"
    #pause 0.2
    play sound "audio/effects/juegaBragas5.ogg"
    scene claireDirector2ndaVisita_144 with dissolve
    pause 0.1
    scene claireDirector2ndaVisita_145 with dissolve
    directorUni "Eres una puta que necesita ser follada, mira cuantos chorros sueltas. No paras de correrte, esto te encanta."
    directorUni "Estas toda abierta de piernas sin quejarte y tu coño pide mas."
    play sound "audio/effects/llanto.ogg"
    scene claireDirector2ndaVisita_146 with dissolve
    claire "Por favor ya no puedo más..."
    scene claireDirector2ndaVisita_147 with dissolve
    directorUni "Si que puedes, mira esto..."
    play sound "audio/effects/juegaBragasOrgasm8.ogg"
    scene claireDirector2ndaVisita_148 with dissolve
    pause 0.1
    play sound "audio/effects/juegaBragas8.ogg"
    claire "¡¡¡OOOOOOHhhhhhh!!!! {p=0.7}{nw}"

    scene claireDirector2ndaVisita_149 with dissolve
    claire "{size=14}Me corroooooooooooo... {/size}"

    $ numEventClaireDirectorVisita2MeterleDedo4PREMIUM = 0
    while numEventClaireDirectorVisita2MeterleDedo4PREMIUM < 3:
        scene claireDirector2ndaVisita_150 with dissolve
        pause 0.4
        play sound "audio/effects/juegaBragasOrgasm11.ogg"
        scene claireDirector2ndaVisita_151 with dissolve
        pause 0.04
        play sound "audio/effects/juegaBragasOrgasm12.ogg"
        scene claireDirector2ndaVisita_152 with dissolve
        if numEventClaireDirectorVisita2MeterleDedo4PREMIUM == 0:
            claire "¡¡¡AAAAAAaahhhhhhh!!!! {p=0.3}{nw}"
        play sound "audio/effects/juegaBragasOrgasm14.ogg"
        scene claireDirector2ndaVisita_153 with dissolve
        if numEventClaireDirectorVisita2MeterleDedo4PREMIUM == 0:
            directorUni "¡Así! toda abiertaaa! {p=0.9}{nw}"
        play sound "audio/effects/juegaBragas7.ogg"
        scene claireDirector2ndaVisita_154 with dissolve
        pause 0.04
        if numEventClaireDirectorVisita2MeterleDedo4PREMIUM == 0:
            play sound "audio/effects/juegaBragasOrgasm15.ogg"
        else:
            play sound "audio/effects/juegaBragasOrgasm20.ogg"
        scene claireDirector2ndaVisita_155 with dissolve
        pause 0.05
        scene claireDirector2ndaVisita_156 with dissolve
        play sound "audio/effects/juegaBragasOrgasm22.ogg"
        scene claireDirector2ndaVisita_157 with dissolve
        pause 0.01
        if numEventClaireDirectorVisita2MeterleDedo4PREMIUM == 0:
            play sound "audio/effects/juegaBragasOrgasm21.ogg"
        else:
            play sound "audio/effects/juegaBragasOrgasm15.ogg"
        scene claireDirector2ndaVisita_158 with dissolve
        pause 0.3
        play sound "audio/effects/juegaBragasOrgasm19.ogg"
        if numEventClaireDirectorVisita2MeterleDedo4PREMIUM == 2:
            scene claireDirector2ndaVisita_159 with dissolve
            pause 0.3
            play sound "audio/effects/squirt.ogg"
            scene claireDirector2ndaVisita_160 with dissolve
            directorUni "¡Todo adentrooooo! {p=1.5}{nw}"
            scene claireDirector2ndaVisita_161 with dissolve
            claire "{size=25} ¡¡¡OOOOOOooooooooouuHhhhhhh me corrooooo!!!! {p=1.2}{nw}{/size=25}"
            scene claireDirector2ndaVisita_162 with dissolve
            play sound "audio/effects/juegaBragasOrgasm3.ogg"
            directorUni "¡Me has mojado toda la mesa!"
            play sound "audio/effects/juegaBragasOrgasm.ogg"
            scene claireDirector2ndaVisita_163 with dissolve
            pause 0.1
            claire "{size=14} No puedo mas... {/size}"
            play sound "audio/effects/sacarDedos.ogg"
            pause 0.1
            scene claireDirector2ndaVisita_164 with dissolve
            directorUni "Este es el coño que te mereces."
            play sound "audio/effects/orgasm4.ogg"
            scene claireDirector2ndaVisita_165 with dissolve
            claire "ooohhhhhh"
            scene claireDirector2ndaVisita_166 with dissolve
            pause
            scene claireDirector2ndaVisita_167 with dissolve
            directorUni "Te dije que esto es lo que necesitabas, hahahaha"
            pause
            narrador "Volvemos con el protagonista..."
        $ numEventClaireDirectorVisita2MeterleDedo4PREMIUM = numEventClaireDirectorVisita2MeterleDedo4PREMIUM +1
    show screen escenasClaire("Claire")
    return
