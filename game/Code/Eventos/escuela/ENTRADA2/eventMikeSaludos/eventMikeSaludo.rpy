label eventMikeSaludo():
    if eventMikeSaludoUniversidad == False:
        $ eventMikeSaludoUniversidad = True
        $ noPuedesIrteDeEscuela = True
        scene mikeEscuelaEntradaEvent_0
        protaPensa "Este es Mike"
        scene mikeEscuelaEntradaEvent_1 with dissolve
        protaPensa "Es mi mejor amigo, nos conocemos desde hace muchos años."
        scene mikeEscuelaEntradaEvent_2 with dissolve
        protaPensa "El vive cerca de casa, así que podemos decir que somos casi vecinos."
        play music "audio/music/funkorama-by-kevin-macleod.ogg" fadein 3.0
        scene mikeEscuelaEntradaEvent_3 with dissolve
        prota "¡Que tal hermano!"
        scene mikeEscuelaEntradaEvent_4 with dissolve
        pause
        play sound "audio/effects/choca5.ogg"

        scene mikeEscuelaEntradaEvent_5 with dissolve
        pause
        scene mikeEscuelaEntradaEvent_6 with dissolve
        mike "Menos mal [nombreProta2], ya pense que no llegarías a tiempo."
        scene mikeEscuelaEntradaEvent_7 with dissolve
        mike "Venga vamos a clase, no tenemos mucho tiempo o llegaremos tarde."
        scene mikeEscuelaEntradaEvent_8 with dissolve
        pause
        scene mikeEscuelaEntradaEvent_9 with dissolve
        prota "Solo necesito 1 minuto, tengo que ir a mear."
        scene mikeEscuelaEntradaEvent_10 with dissolve
        mike "Ostia tio,yo voy tirando, no quiero tener problemas. Date prisa o Alicia cargara contra ti otra vez."
        scene mikeEscuelaEntradaEvent_11 with dissolve
        prota "Si, no te preocupes, no quiero escuchar a esa bruja. En nada estoy allí."
        stop music fadeout 2.0
        scene mikeEscuelaEntradaEvent_12 with dissolve
        mike "Nos vemos en clase."



    return
