label eventLuciaClaireJack():
    if eventLuciaClaireJackTiraBasura == False:
        $ eventLuciaClaireJackTiraBasura = True
        stop music fadeout 2.0
        play sound "audio/effects/closeDoor.ogg"
        play music "audio/music/big-rock-by-kevin-macleod.ogg" fadein 3.0
        scene LuciaJackClaireEncuentro_0
        pause
        scene LuciaJackClaireEncuentro_1 with dissolve
        pause
        scene LuciaJackClaireEncuentro_2 with dissolve
        pause
        scene LuciaJackClaireEncuentro_3 with dissolve
        pause
        protaPensa "Y ahí están Jack y Claire"
        scene LuciaJackClaireEncuentro_4 with dissolve
        pause
        scene LuciaJackClaireEncuentro_5 with dissolve
        $ chicaClaire = True
        call avisosStatsSms("newContact") from _call_avisosStatsSms_6
        protaPensa "Claire es la chica más popular de la escuela"
        protaPensa "Es una tía egocéntrica y problemática. Habré hablado con ella 3 veces en total como mucho. Ella está en otra liga."
        protaPensa "Todos los chicos hablan de ella y a todos nos gustaría follárnosla"
        scene LuciaJackClaireEncuentro_6 with dissolve
        pause
        scene LuciaJackClaireEncuentro_7 with dissolve
        pause
        scene LuciaJackClaireEncuentro_8 with dissolve
        protaPensa "Desgraciadamente está saliendo con el tonto de Jack."
        protaPensa "Jack es el novio de Claire. El típico hombre musculitos y sin cerebro."
        scene LuciaJackClaireEncuentro_9 with dissolve
        protaPensa "Es un chulo y se lo tiene muy creído. Tengo que vigilar con él o puede darme problemas."
        scene LuciaJackClaireEncuentro_10 with dissolve
        pause
        scene LuciaJackClaireEncuentro_11 with dissolve
        jack "Venga chacha a limpiar bien!"
        play sound "audio/effects/nalgada.ogg"
        scene LuciaJackClaireEncuentro_12 with dissolve
        "ZAAASS!!!"
        scene LuciaJackClaireEncuentro_14 with dissolve
        pause
        scene LuciaJackClaireEncuentro_15 with dissolve
        pause
        scene LuciaJackClaireEncuentro_16 with dissolve
        pause
        scene LuciaJackClaireEncuentro_17 with dissolve
        pause
        scene LuciaJackClaireEncuentro_18 with dissolve
        jack "Así, muy bien."
        scene LuciaJackClaireEncuentro_19 with dissolve
        pause
        scene LuciaJackClaireEncuentro_20 with dissolve
        pause
        scene LuciaJackClaireEncuentro_21 with dissolve
        pause
        scene LuciaJackClaireEncuentro_22 with dissolve
        pause
        scene LuciaJackClaireEncuentro_23 with dissolve
        jack "Sal de mi camino enano!"
        $ config.rollback_enabled = False
        call miniGameClick("esquivarGolpeJackPasilloWc",5) from _call_miniGameClick
        scene LuciaJackClaireEncuentro_27 with dissolve
        stop music fadeout 3.0
        $ config.rollback_enabled = True
        claire "Adiós perdedor!"
        scene LuciaJackClaireEncuentro_28 with dissolve
        pause
        scene LuciaJackClaireEncuentro_29 with dissolve
        prota "No te preocupes son idiotas."
        scene LuciaJackClaireEncuentro_30 with dissolve
        lucia "No me respetan y me tratan mal porque soy la de la limpieza. Con razón mi hijo no viene a saludarme."
        scene LuciaJackClaireEncuentro_31 with dissolve
        lucia "Seguro que le da vergüenza ver a su madre limpiar la escuela."
        scene LuciaJackClaireEncuentro_32 with dissolve
        prota "No digas tonterías, eres una persona fuerte y muy bonita para decir esas cosas."
        scene LuciaJackClaireEncuentro_33 with dissolve
        pause
        scene LuciaJackClaireEncuentro_34 with dissolve
        lucia "Gracias cielo. Me alegra que Mike tenga como amigo una persona tan buena y que se preocupe por los demás."
        scene LuciaJackClaireEncuentro_35 with dissolve
        prota "Mierda, llegare tarde a clase. Lo siento me tengo que ir!"


    return

    label esquivarGolpeJackPasilloWc():
        scene LuciaJackClaireEncuentro_25 with dissolve
        pause
        scene LuciaJackClaireEncuentro_26 with dissolve
        jack "Enanooo!!"

        return

    label noEsquivarGolpeJackPasilloWc():
        play sound "audio/effects/golpe.ogg"
        scene LuciaJackClaireEncuentro_24 with dissolve
        pause
        scene LuciaJackClaireEncuentro_26 with dissolve
        jack "Eres un pringado!!!"

        return
