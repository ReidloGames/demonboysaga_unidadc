label eventClaseAliciaLlegoTarde():
    if llegoTardeClaseAlicia == False:
        $ llegoTardeClaseAlicia = True
        $ noPuedesIrteDeEscuela = False
        $ puedoVolverMiHabitacion = False
        scene aliciaDandoClaseEntraProtaEvent_0
        stop music fadeout 1.0
        pause
        scene aliciaDandoClaseEntraProtaEvent_1 with dissolve
        pause
        scene aliciaDandoClaseEntraProtaEvent_2 with dissolve
        pause
        scene aliciaDandoClaseEntraProtaEvent_3 with dissolve
        protaPensa "Oh joder, he llegado tarde."
        scene aliciaDandoClaseEntraProtaEvent_4 with dissolve
        protaPensa "Y esta es Alicia, la profesora que tanto me quiere..."
        call avisosStatsSms("newContact") from _call_avisosStatsSms_10
        $ chicaAlicia = True
        protaPensa "Como mujer esta cañón, pero su carácter es el de una serpiente."
        scene aliciaDandoClaseEntraProtaEvent_5 with dissolve
        protaPensa "Aunque creo que me tiene manía... Siempre me ataca gratuitamente"
        play music "audio/music/holiday-weasel-by-kevin-macleod.ogg" fadein 2.0
        scene aliciaDandoClaseEntraProtaEvent_6 with dissolve
        prota "Perdón."
        scene aliciaDandoClaseEntraProtaEvent_7 with dissolve
        alicia "Hombre quien tenemos aquí..."
        alicia "Es una falta de respeto hacia mi y tus compañeros el llegar tarde e interrumpir las clases."
        scene aliciaDandoClaseEntraProtaEvent_8 with dissolve
        alicia "Siéntate ya! y empieza a tomar apuntes"
        scene aliciaDandoClaseEntraProtaEvent_9 with dissolve
        pause
        scene aliciaDandoClaseEntraProtaEvent_10 with dissolve
        call getObjectInventario("bolis") from _call_getObjectInventario_2
        if resultObject == False:
            protaPensa "Mierda, lo que me faltaba!! Me dejado los bolis en la taquilla"
            scene aliciaDandoClaseEntraProtaEvent_11 with dissolve
            pause
            scene aliciaDandoClaseEntraProtaEvent_14 with dissolve
            alicia "Que pasa [nombreProta2], no sabes llegar puntual. ¿Tampoco sabes escribir?"
            scene aliciaDandoClaseEntraProtaEvent_10 with dissolve
            pause
            scene aliciaDandoClaseEntraProtaEvent_12 with dissolve
            prota "Me dejado los bolis en mi taquilla."
            scene aliciaDandoClaseEntraProtaEvent_14 with dissolve
            pause
            scene aliciaDandoClaseEntraProtaEvent_15 with dissolve
            alicia "Muy bien un aplauso para [nombreProta2], que viene a clase tarde y encima sin el material necesario."
            play sound "audio/effects/risasClase.ogg"
            scene aliciaDandoClaseEntraProtaEvent_17 with dissolve
            protaPensa "Joder!!"
        else:
            scene aliciaDandoClaseEntraProtaEvent_18 with dissolve
            protaPensa "Uf, suerte que me acorde de coger los bolis"

        scene aliciaDandoClaseEntraProtaEvent_19 with dissolve
        pause
        scene aliciaDandoClaseEntraProtaEvent_20 with dissolve
        alicia "Y recordar alumnos lo que no hay que hacer es lo que hace [nombreProta2]. Doy por finalizada la clase."
        scene aliciaDandoClaseEntraProtaEvent_21 with dissolve
        protaPensa "Juro que un día de estos me vengare, por hacerme pasar todo esto."
        $ escondeFrameAyudasAlicia = True
        $ hasDadoClaseAlicia = True
        $ adelantarHora = False
        call nextTiempoDay(0) from _call_nextTiempoDay_22


    return
