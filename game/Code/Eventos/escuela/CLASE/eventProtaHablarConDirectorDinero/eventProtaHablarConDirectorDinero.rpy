label eventProtaHablarConDirectorDinero():
    if protaHablarConDirectorDinero == False:
        $ protaHablarConDirectorDinero = True
        $ escondeFrameAyudasAlicia = True
        $ escondeFrameAyudasMad = False
        scene protaIrHablarConDirector_0
        pause
        scene protaIrHablarConDirector_1 with dissolve
        alicia "Un momento."
        scene protaIrHablarConDirector_2 with dissolve
        alicia "El director quiere verte."
        scene protaIrHablarConDirector_4 with dissolve
        pause
        scene protaIrHablarConDirector_5 with dissolve
        protaPensa "A mí me quiere ver el director? ¿qué es lo que querrá?"
        scene protaIrHablarConDirector_3 with dissolve
        alicia "A saber, que has hecho ya..."
        alicia "El director está en su despacho en el segundo piso"
        scene protaIrHablarConDirector_6 with dissolve
        prota "Ok, ahora iré a verle."
        scene protaIrHablarConDirector_7 with dissolve
        pause
        scene protaIrHablarConDirector_8 with dissolve
        pause
        scene protaIrHablarConDirector_9 with dissolve
        pause
        scene protaIrHablarConDirector_10 with dissolve
        pause
        scene protaIrHablarConDirector_11 with dissolve
        pause
        play sound "audio/effects/toctoc.ogg"
        scene protaIrHablarConDirector_12 with dissolve
        "TOC TOC"
        scene protaIrHablarConDirector_27 with dissolve
        "Unos minutos antes..."
        scene protaIrHablarConDirector_13 with dissolve
        pause
        scene protaIrHablarConDirector_14 with dissolve
        pause

        "Atención escena NTR, quieres activarla?"
        menu:
            "SI":
                $ ntrGame = True
            "NO":
                $ ntrGame = False



        if ntrGame == True:
            stop music fadeout 2.0
            scene protaIrHablarConDirector_15 with dissolve
            play music "audio/music/DangerStorm_kevinMacLeod.ogg" fadein 2.0
            directorUni "Si quieres que limpie todo tu expediente y que apruebe todas tus asignaturas..."
            scene protaIrHablarConDirector_16 with dissolve
            directorUni "Ya sabes lo que tienes que hacer."
            scene protaIrHablarConDirector_17 with dissolve
            claire "Por favor... ¿No puede haber otra manera? No quiero hacer esto..."
            scene protaIrHablarConDirector_18 with dissolve
            directorUni "Es esto o ya te digo que tu carrera habrá terminado para siempre."
            stop music fadeout 2.0
            scene protaIrHablarConDirector_19 with dissolve

            directorUni "Arrodíllate y empieza..."
            play music "audio/music/ChillWave_kevinMacLeod.ogg" fadein 2.0
            scene protaIrHablarConDirector_20 with dissolve
            pause
            scene protaIrHablarConDirector_21 with dissolve
            claire "Por favor... no me hagas esto..."
            directorUni "Sácala y empieza a mamar."
            scene protaIrHablarConDirector_22 with dissolve
            claire "yo..."
            scene protaIrHablarConDirector_23 with dissolve
            directorUni "¡¡¡Ahora!!!"
            play sound "audio/effects/golpeMesa.ogg"
            scene protaIrHablarConDirector_24 with dissolve
            "¡¡¡PAM!!!"
            scene protaIrHablarConDirector_25 with dissolve
            pause
            scene protaIrHablarConDirector_26 with dissolve
            pause
            scene protaIrHablarConDirector_27 with dissolve
            play sound "audio/effects/quitarRopa.ogg"
            narrador "Le quita los pantalones"
            scene protaIrHablarConDirector_28 with dissolve
            pause
            scene protaIrHablarConDirector_29 with dissolve
            pause
            scene protaIrHablarConDirector_30 with dissolve
            pause
            scene protaIrHablarConDirector_31 with dissolve
            pause
            scene protaIrHablarConDirector_32 with dissolve
            pause
            scene protaIrHablarConDirector_33 with dissolve
            clairePensa "Le está creciendo..."
            scene protaIrHablarConDirector_34 with dissolve
            clairePensa "La tiene bastante grande. Espero que con esto se corra rápido y termine esta pesadilla."
            scene protaIrHablarConDirector_35 with dissolve
            pause
            scene protaIrHablarConDirector_36 with dissolve
            pause
            scene protaIrHablarConDirector_37 with dissolve
            pause
            scene protaIrHablarConDirector_38 with dissolve
            pause
            call bucle1PajaDirector

        else:
            scene protaIrHablarConDirector_15_NONTR with dissolve
            directorUni "Claire estas teniendo muy malas notas y sabes que tienes que mejorar.Tu futuro esta en juego"
            scene protaIrHablarConDirector_16_NONTR with dissolve
            claire "Intentare mejorar..."
            scene protaIrHablarConDirector_17_NONTR with dissolve
            claire "gracias."
            scene protaIrHablarConDirector_12 with dissolve
            "TOC TOC"
            scene protaIrHablarConDirector_18_NONTR with dissolve
            pause
            scene protaIrHablarConDirector_19_NONTR with dissolve
            protaPensa "El director tambien ha llamado a Claire?"
            scene protaIrHablarConDirector_20_NONTR with dissolve
            pause
            scene protaIrHablarConDirector_81 with dissolve
            prota "Buenos días."
            scene protaIrHablarConDirector_82 with dissolve
            pause
            scene protaIrHablarConDirector_81 with dissolve
            directorUni "Buenos días. No hace falta que te sientes, será breve."
            scene protaIrHablarConDirector_83 with dissolve
            prota "Alicia me ha dicho que querías hablar conmigo."
            scene protaIrHablarConDirector_84 with dissolve
            directorUni "Así es..."
            directorUni "Quería hablar contigo porque este mes no hemos recibido el pago de la Universidad."
            scene protaIrHablarConDirector_85 with dissolve
            prota "Pero eso no es posible..."
            scene protaIrHablarConDirector_84 with dissolve
            directorUni "Sabemos que nunca hemos tenido problemas de impago contigo, pero este mes no hemos recibido el ingreso."
            scene protaIrHablarConDirector_84 with dissolve
            directorUni "Esta Universidad es de las mejores del país con un personal muy profesional y muy respetado. Solo acude gente importante. Por ahora haremos la vista gorda, pero espero que soluciones el problema lo antes posible, o tomaremos cartas en el asunto."
            scene protaIrHablarConDirector_86 with dissolve
            prota "Yo... no sabía nada... Lo hablare en casa."
            scene protaIrHablarConDirector_84 with dissolve
            directorUni "Bien, solo era eso. Por favor cierra la puerta al salir."
            directorUni "Gracias."
            scene protaIrHablarConDirector_82 with dissolve
            prota "Esta bien, adiós."
            play sound "audio/effects/open_door.ogg"
            scene protaIrHablarConDirector_88_ with dissolve
            pause
            scene protaIrHablarConDirector_89_ with dissolve
            protaPensa "Oh mierda... esto no me está gustando... Tendré que hablar con [nombreMad3] y [nombrePad3]. Espero que solo sea un mal entendido."
            call menu4ProtaHablarConDirectorDinero from _call_menu4ProtaHablarConDirectorDinero_1


    return

label menu1ProtaHablarConDirectorDinero:
    menu:
        "Seguir":
            call eventProtaHablarConDirectorDinero2 from _call_eventProtaHablarConDirectorDinero2

        "Hacer otra Paja":
            scene protaIrHablarConDirector_38 with dissolve
            pause
            call bucle1PajaDirector from _call_bucle1PajaDirector_1
    return


label bucle1PajaDirector():
    $ numPajaDirector1 = 0
    while numPajaDirector1 < 5:
        scene protaIrHablarConDirector_39 with dissolve
        play sound "audio/effects/flip.ogg"
        pause 0.1
        play sound "audio/effects/flap.ogg"
        scene protaIrHablarConDirector_40 with dissolve
        pause 0.1
        $ numPajaDirector1 = numPajaDirector1 +1
    call menu1ProtaHablarConDirectorDinero from _call_menu1ProtaHablarConDirectorDinero
    return

label eventProtaHablarConDirectorDinero2:
    scene protaIrHablarConDirector_41 with dissolve
    directorUni "Métetela en la boca."
    scene protaIrHablarConDirector_42 with dissolve
    claire "¿Qué? Pero pensé que con esto ya era suficiente. Eso es demasiado…"
    scene protaIrHablarConDirector_43 with dissolve
    directorUni "¡Silencio! Aquí las ordenes las doy yo. Esto solo acaba de empezar."
    scene protaIrHablarConDirector_44 with dissolve
    directorUni "O haces lo que te digo o tu futuro será el de fregar suelos."
    directorUni "Abre la boca y empieza a chupar."
    scene protaIrHablarConDirector_45 with dissolve
    play sound "audio/effects/llanto.ogg"
    "sniff..."
    scene protaIrHablarConDirector_46 with dissolve
    pause
    scene protaIrHablarConDirector_47 with dissolve
    clairePensa "Dios mío que estoy haciendo... Tengo que aprobar este curso no me queda otra."
    clairePensa "Sabe a salado."
    scene protaIrHablarConDirector_48 with dissolve
    pause
    scene protaIrHablarConDirector_49 with dissolve
    pause
    play sound "audio/effects/flip.ogg"
    scene protaIrHablarConDirector_50 with dissolve

    pause
    play sound "audio/effects/flap.ogg"
    scene protaIrHablarConDirector_51 with dissolve
    pause
    play sound "audio/effects/flap.ogg"
    scene protaIrHablarConDirector_52 with dissolve
    directorUni "Así me gusta calladita y obediente. Empieza a moverte y succiona bien."
    call bucleMamada1Director from _call_bucleMamada1Director

    return

label menu2ProtaHablarConDirectorDinero:
    menu:
        "Seguir":
            call eventProtaHablarConDirectorDinero3 from _call_eventProtaHablarConDirectorDinero3

        "Hacer otra Mamada":
            call bucleMamada1Director from _call_bucleMamada1Director_1
    return

label bucleMamada1Director():
    show ClaireChupandoAnim100
    pause(8)
    call menu2ProtaHablarConDirectorDinero from _call_menu2ProtaHablarConDirectorDinero
    return

label eventProtaHablarConDirectorDinero3():
    scene protaIrHablarConDirector_53 with dissolve
    directorUni "Esta bien pero seguro que puedes hacerlo mejor... Métela más."
    scene protaIrHablarConDirector_54 with dissolve
    claire "Por favor ya está, hice todo lo que me pediste."
    scene protaIrHablarConDirector_53 with dissolve
    directorUni "¡¡Abre la boca bien y métela toda!!"
    scene protaIrHablarConDirector_54 with dissolve
    play sound "audio/effects/llanto2.ogg"
    claire "Ya no puedo más por favor… "
    scene protaIrHablarConDirector_55 with dissolve
    pause
    scene protaIrHablarConDirector_56 with dissolve
    directorUni "Quita las manos."
    scene protaIrHablarConDirector_57 with dissolve
    claire "Por favor..."
    directorUni "Quita las manos, no lo volveré a repetir."
    scene protaIrHablarConDirector_58 with dissolve
    pause
    call bucle1MamadaProfundaDirector from _call_bucle1MamadaProfundaDirector
    return

label menu3ProtaHablarConDirectorDinero():
    stop sound
    menu:
        "Seguir":
            call eventProtaHablarConDirectorDinero4 from _call_eventProtaHablarConDirectorDinero4

        "Hacer otra garganta profunda camara1":
            scene protaIrHablarConDirector_58 with dissolve
            pause
            call bucle1MamadaProfundaDirector from _call_bucle1MamadaProfundaDirector_1
        "Hacer otra garganta profunda camara2":
            scene protaIrHablarConDirector_74 with dissolve
            pause
            call bucle2MamadaProfundaDirector from _call_bucle2MamadaProfundaDirector
            return
    return

label bucle1MamadaProfundaDirector():
    $ numMamadaProfundaClaireDirector1 = 0
    while numMamadaProfundaClaireDirector1 < 2:
        play sound "audio/effects/gargantaprofunda1.ogg"
        scene protaIrHablarConDirector_59 with dissolve
        pause 0.1
        scene protaIrHablarConDirector_60 with dissolve
        if numMamadaProfundaClaireDirector1 == 0:
            directorUni "Así muy bien.{p=0.5}{nw}"

        pause 0.1
        scene protaIrHablarConDirector_62 with dissolve
        pause 0.1
        scene protaIrHablarConDirector_65 with dissolve
        if numMamadaProfundaClaireDirector1 == 1:
            directorUni "Te voy a rellenar todos los agujeros. Ya veras...{p=1}{nw}"
        pause 0.1
        scene protaIrHablarConDirector_64 with dissolve
        pause 0.1
        scene protaIrHablarConDirector_61 with dissolve
        pause 0.1
        scene protaIrHablarConDirector_59 with dissolve
        pause 0.1
        scene protaIrHablarConDirector_61 with dissolve
        pause 0.1
        scene protaIrHablarConDirector_62 with dissolve
        pause 0.1
        scene protaIrHablarConDirector_63 with dissolve
        pause 0.1
        scene protaIrHablarConDirector_64 with dissolve
        pause 0.1
        scene protaIrHablarConDirector_65 with dissolve
        pause 0.05
        scene protaIrHablarConDirector_68 with dissolve
        if numMamadaProfundaClaireDirector1 == 0:
            directorUni "Serás mi putita y te hare disfrutar...{p=1}{nw}"
        pause 0.1
        scene protaIrHablarConDirector_67 with dissolve
        pause 0.1
        scene protaIrHablarConDirector_66 with dissolve
        pause 0.1
        scene protaIrHablarConDirector_68 with dissolve
        pause 0.1
        scene protaIrHablarConDirector_64 with dissolve
        pause 0.1
        scene protaIrHablarConDirector_61 with dissolve
        pause 0.1
        $ numMamadaProfundaClaireDirector1 = numMamadaProfundaClaireDirector1 +1
    call menu3ProtaHablarConDirectorDinero from _call_menu3ProtaHablarConDirectorDinero
    return

label bucle2MamadaProfundaDirector():
    $ numMamadaProfundaClaireDirector2 = 0
    while numMamadaProfundaClaireDirector2 < 2:
        play sound "audio/effects/gargantaprofunda1.ogg"
        scene protaIrHablarConDirector_74 with dissolve
        pause 0.1
        scene protaIrHablarConDirector_73 with dissolve
        pause 0.1
        scene protaIrHablarConDirector_75 with dissolve
        pause 0.1
        scene protaIrHablarConDirector_72 with dissolve
        pause 0.1
        scene protaIrHablarConDirector_70 with dissolve
        pause 0.1
        scene protaIrHablarConDirector_69 with dissolve
        if numMamadaProfundaClaireDirector2 == 0:
            directorUni "¿Te gusta cómo te follo la boca? Se que lo estas disfrutando.{p=1.8}{nw}"
        pause 0.1
        scene protaIrHablarConDirector_70 with dissolve
        pause 0.1
        scene protaIrHablarConDirector_71 with dissolve
        pause 0.1
        scene protaIrHablarConDirector_70 with dissolve
        pause 0.1
        scene protaIrHablarConDirector_72 with dissolve
        pause 0.1
        scene protaIrHablarConDirector_73 with dissolve
        pause 0.1
        if numMamadaProfundaClaireDirector2 == 1:
            directorUni "Te dicho que abras bien la boca.{p=1}{nw}"
        scene protaIrHablarConDirector_72 with dissolve
        pause 0.1
        scene protaIrHablarConDirector_74 with dissolve
        pause 0.1
        scene protaIrHablarConDirector_70 with dissolve
        pause 0.1
        $ numMamadaProfundaClaireDirector2 = numMamadaProfundaClaireDirector2 +1
    call menu3ProtaHablarConDirectorDinero from _call_menu3ProtaHablarConDirectorDinero_1
    return

label eventProtaHablarConDirectorDinero4():
    scene protaIrHablarConDirector_76 with dissolve
    directorUni "Toda adentro. Así me gusta, que te portes bien."
    play sound "audio/effects/toctoc.ogg"
    scene protaIrHablarConDirector_77 with dissolve
    stop music
    "Toc Toc!!!"
    scene protaIrHablarConDirector_78 with dissolve
    directorUni "Joder metete debajo de la mesa y no hagas ruido."

    scene protaIrHablarConDirector_79 with dissolve

    directorUni "Adelante."
    play sound "audio/effects/open_door.ogg"
    scene protaIrHablarConDirector_80 with dissolve
    play music "audio/music/sneaky-adventure-by-kevin-macleod.ogg" fadein 3.0
    pause
    scene protaIrHablarConDirector_81 with dissolve
    prota "Buenos días."
    scene protaIrHablarConDirector_82 with dissolve
    protaPensa "Juraría que estaba hablando con alguien..."
    scene protaIrHablarConDirector_81 with dissolve
    directorUni "Buenos días. No hace falta que te sientes, será breve."
    scene protaIrHablarConDirector_83 with dissolve
    prota "Alicia me ha dicho que querías hablar conmigo."
    scene protaIrHablarConDirector_84 with dissolve
    directorUni "Así es..."
    directorUni "Quería hablar contigo porque este mes no hemos recibido el pago de la Universidad."
    scene protaIrHablarConDirector_85 with dissolve
    prota "Pero eso no es posible..."
    scene protaIrHablarConDirector_84 with dissolve
    directorUni "Sabemos que nunca hemos tenido problemas de impago contigo, pero este mes no hemos recibido el ingreso."
    scene protaIrHablarConDirector_84 with dissolve
    directorUni "Esta Universidad es de las mejores del país con un personal muy profesional y muy respetado. Solo acude gente importante. Por ahora haremos la vista gorda, pero espero que soluciones el problema lo antes posible, o tomaremos cartas en el asunto."
    scene protaIrHablarConDirector_86 with dissolve
    prota "Yo... no sabía nada... Lo hablare en casa."
    scene protaIrHablarConDirector_87 with dissolve
    directorUni "Bien, solo era eso. Por favor cierra la puerta al salir."
    directorUni "Gracias."
    scene protaIrHablarConDirector_82 with dissolve
    prota "Esta bien, adiós."
    play sound "audio/effects/open_door.ogg"
    scene protaIrHablarConDirector_88_ with dissolve
    pause
    scene protaIrHablarConDirector_89_ with dissolve
    protaPensa "Oh mierda... esto no me está gustando... Tendré que hablar con [nombreMad3] y [nombrePad3]. Espero que solo sea un mal entendido."
    call menu4ProtaHablarConDirectorDinero from _call_menu4ProtaHablarConDirectorDinero

    return


label menu4ProtaHablarConDirectorDinero():
    if ntrGame == True:
        menu:
            "Seguir con [nombreProta2]":
                call nextZona("pasilloDirector") from _call_nextZona_11
                return
            "Seguir con el Director":
                call eventProtaHablarConDirectorDinero_Part2() from _call_eventProtaHablarConDirectorDinero_Part2
                return
    else:
        call nextZona("pasilloDirector") from _call_nextZona_9
        return

label eventProtaHablarConDirectorDinero_Part2():
    scene protaIrHablarConDirector_88 with dissolve
    directorUni "Nos han interrumpido, pero no pasa nada, levántate."
    scene protaIrHablarConDirector_89 with dissolve
    claire "Yo ya me tengo que ir."
    scene protaIrHablarConDirector_90 with dissolve
    directorUni "Tranquila, solo un poco mas y ya estara..."
    play music "audio/music/ChillWave_kevinMacLeod.ogg" fadein 2.0
    play sound "audio/effects/tiraraMesa.ogg"
    scene protaIrHablarConDirector_91 with hpunch
    directorUni "A ver que tienes por aquí."
    scene protaIrHablarConDirector_92 with dissolve
    claire "No por favor todo menos eso... ¡¡¡Por favor!!!"
    play sound "audio/effects/llanto.ogg"
    scene protaIrHablarConDirector_93 with dissolve
    directorUni "¡¡Silencio!! bien que hace un rato me la estabas chupando entera y sin rechistar. Se muy bien que esto te gusta."
    scene protaIrHablarConDirector_94 with dissolve
    claire "Espera no..."
    scene protaIrHablarConDirector_95 with dissolve
    pause
    scene protaIrHablarConDirector_96 with dissolve
    pause
    scene protaIrHablarConDirector_97 with dissolve
    directorUni "Voy a disfrutar esto y quiero que estés calladita o tendrás problemas."
    scene protaIrHablarConDirector_98 with dissolve
    play sound "audio/effects/llanto2.ogg"
    pause
    scene protaIrHablarConDirector_99 with dissolve
    pause
    scene protaIrHablarConDirector_100 with dissolve
    directorUni "Pero si estas chorreando... Lo estabas deseando."
    play sound "audio/effects/roze1.ogg"
    scene protaIrHablarConDirector_101 with dissolve

    pause
    scene protaIrHablarConDirector_100 with dissolve
    pause
    play sound "audio/effects/roze1.ogg"
    scene protaIrHablarConDirector_101 with dissolve
    pause
    scene protaIrHablarConDirector_100 with dissolve
    pause
    play sound "audio/effects/roze3.ogg"
    scene protaIrHablarConDirector_102 with dissolve
    #play sound "audio/effects/roze1.ogg"
    pause
    play sound "audio/effects/juegaBragas1.ogg"
    scene protaIrHablarConDirector_103 with dissolve
    pause
    directorUni "Vienes a clase con esta ropa de puta y encima decías que no querías."
    scene protaIrHablarConDirector_104 with dissolve
    play sound "audio/effects/juegaBragas2.ogg"
    directorUni "Tremenda puta estas hecha... No paras de chorrear y tienes todo el coño abierto."
    scene protaIrHablarConDirector_105 with dissolve
    play sound "audio/effects/llanto.ogg"
    claire "Por favor ya no mas..."
    scene protaIrHablarConDirector_106 with dissolve
    play sound "audio/effects/roze4.ogg"
    directorUni "Tu coño no dice lo mismo, mira lo abierto que lo tienes y como gotea..."
    scene protaIrHablarConDirector_105 with dissolve
    play sound "audio/effects/rozeOrgasm.ogg"
    directorUni "¿Y esos gemidos?"
    scene protaIrHablarConDirector_106 with dissolve
    pause
    play sound "audio/effects/rozeOrgasm2.ogg"
    scene protaIrHablarConDirector_107 with dissolve
    pause
    play sound "audio/effects/rozeOrgasm.ogg"
    scene protaIrHablarConDirector_108 with dissolve
    pause
    scene protaIrHablarConDirector_109 with dissolve
    directorUni "¿Porque tienes el coño tan abierto y chorreando?"
    directorUni "hahahaha"
    scene protaIrHablarConDirector_110 with dissolve
    pause
    scene protaIrHablarConDirector_111 with dissolve
    clairePensa "{size=15}{i}Como siga así voy a perder la conciencia. Este desgraciado sabe lo que hace, me está volviendo loca con lo que hace y no puedo evitarlo...{/size}{/i}"
    scene protaIrHablarConDirector_112 with dissolve
    play sound "audio/effects/rozeOrgasm3.ogg"
    directorUni "Prepárate porque ahora te lo abriré más."

    clairePensa "..."
    scene protaIrHablarConDirector_111 with dissolve
    clairePensa "{size=15}{i}Dios no puedo moverme estoy paralizada, este cerdo me va a follar. No para de frotarme el clítoris y abrirme el coño con mis bragas.{/size}{/i}"
    scene protaIrHablarConDirector_112 with dissolve
    play sound "audio/effects/rozeOrgasm3.ogg"
    claire "haaaaaa haaay"
    scene protaIrHablarConDirector_113 with dissolve

    pause
    play sound "audio/effects/abrirCony.ogg"
    scene protaIrHablarConDirector_114 with dissolve
    pause 0.2
    play sound "audio/effects/juegaBragas2.ogg"

    directorUni "Tremenda puta estas hecha, mira cómo te abres de piernas tu sola y te colocas en buena posición. Y tu coño no para de gotear."
    scene protaIrHablarConDirector_115 with dissolve
    play sound "audio/effects/juegaBragas2.ogg"

    pause

    scene protaIrHablarConDirector_114 with dissolve
    play sound "audio/effects/abrirCony2.ogg"
    pause
    scene protaIrHablarConDirector_117 with dissolve
    play sound "audio/effects/abrirCony3.ogg"
    directorUni "Te estas corriendo y no dices nada. Te está encantando."

    scene protaIrHablarConDirector_116 with dissolve
    pause
    play sound "audio/effects/abrirCony.ogg"
    scene protaIrHablarConDirector_118 with dissolve
    pause
    play sound "audio/effects/juegaBragas2.ogg"
    scene protaIrHablarConDirector_116 with dissolve
    pause
    scene protaIrHablarConDirector_115 with dissolve
    pause
    play sound "audio/effects/abrirCony3.ogg"
    scene protaIrHablarConDirector_117 with dissolve
    directorUni "Cada vez chorreas más, se acabaron los juegos..."
    scene protaIrHablarConDirector_119 with dissolve
    clairePensa "{size=15}{i}No puedo más, me tiene bajo su control, necesito hacer esto para pasar el semestre.{/size}{/i}"
    scene protaIrHablarConDirector_119 with dissolve
    claire "aaaaaaaay"
    scene protaIrHablarConDirector_121 with dissolve
    pause
    scene protaIrHablarConDirector_122 with dissolve
    play sound "audio/effects/closeCony.ogg"
    directorUni "Te rellenare todo el coño, solo te falta suplicar por ello."
    scene protaIrHablarConDirector_123 with dissolve
    pause
    scene protaIrHablarConDirector_124 with dissolve
    pause
    scene protaIrHablarConDirector_125 with dissolve
    pause
    play sound "audio/effects/grito.ogg"
    scene protaIrHablarConDirector_126 with dissolve

    claire "¡¡No!! eso no!!"
    play sound "audio/effects/pollaentra.ogg"
    scene protaIrHablarConDirector_127 with dissolve

    directorUni "¡¡Calla!! pero si sabes que lo estas suplicando."
    narrador "¡Un momento!"
    narrador "Siento interrumpir, pero…"
    narrador "Estas jugando al juego Demon Boy Saga, no al Demon Director Saga."
    narrador "Ya hemos visto demasiado del director..."
    narrador "Volvamos donde dejamos la aventura de nuestro protagonista..."
    play sound "audio/effects/risaDemon.ogg"
    narrador "XD"
    call nextZona("pasilloDirector") from _call_nextZona_12
    return




#REPRODUCTOR

label eventProtaHablarConDirectorDineroEscenaReproductor():
    call deshabilitaObjetosPersonas
    stop music
    scene protaIrHablarConDirector_0
    pause
    scene protaIrHablarConDirector_1 with dissolve
    alicia "Un momento."
    scene protaIrHablarConDirector_2 with dissolve
    alicia "El director quiere verte."
    scene protaIrHablarConDirector_4 with dissolve
    pause
    scene protaIrHablarConDirector_5 with dissolve
    protaPensa "A mí me quiere ver el director? ¿qué es lo que querrá?"
    scene protaIrHablarConDirector_3 with dissolve
    alicia "A saber, que has hecho ya..."
    alicia "El director está en su despacho en el segundo piso"
    scene protaIrHablarConDirector_6 with dissolve
    prota "Ok, ahora iré a verle."
    scene protaIrHablarConDirector_7 with dissolve
    pause
    scene protaIrHablarConDirector_8 with dissolve
    pause
    scene protaIrHablarConDirector_9 with dissolve
    pause
    scene protaIrHablarConDirector_10 with dissolve
    pause
    scene protaIrHablarConDirector_11 with dissolve
    pause
    play sound "audio/effects/toctoc.ogg"
    scene protaIrHablarConDirector_12 with dissolve
    "TOC TOC"
    scene protaIrHablarConDirector_27 with dissolve
    "Unos minutos antes..."
    scene protaIrHablarConDirector_13 with dissolve
    pause
    scene protaIrHablarConDirector_14 with dissolve
    pause

    "Atención escena NTR, quieres activarla?"
    menu:
        "SI":
            $ ntrGame = True
        "NO":
            $ ntrGame = False



    if ntrGame == True:
        stop music fadeout 2.0
        scene protaIrHablarConDirector_15 with dissolve
        play music "audio/music/DangerStorm_kevinMacLeod.ogg" fadein 2.0
        directorUni "Si quieres que limpie todo tu expediente y que apruebe todas tus asignaturas..."
        scene protaIrHablarConDirector_16 with dissolve
        directorUni "Ya sabes lo que tienes que hacer."
        scene protaIrHablarConDirector_17 with dissolve
        claire "Por favor... ¿No puede haber otra manera? No quiero hacer esto..."
        scene protaIrHablarConDirector_18 with dissolve
        directorUni "Es esto o ya te digo que tu carrera habrá terminado para siempre."
        stop music fadeout 2.0
        scene protaIrHablarConDirector_19 with dissolve

        directorUni "Arrodíllate y empieza..."
        play music "audio/music/ChillWave_kevinMacLeod.ogg" fadein 2.0
        scene protaIrHablarConDirector_20 with dissolve
        pause
        scene protaIrHablarConDirector_21 with dissolve
        claire "Por favor... no me hagas esto..."
        directorUni "Sácala y empieza a mamar."
        scene protaIrHablarConDirector_22 with dissolve
        claire "yo..."
        scene protaIrHablarConDirector_23 with dissolve
        directorUni "¡¡¡Ahora!!!"
        play sound "audio/effects/golpeMesa.ogg"
        scene protaIrHablarConDirector_24 with dissolve
        "¡¡¡PAM!!!"
        scene protaIrHablarConDirector_25 with dissolve
        pause
        scene protaIrHablarConDirector_26 with dissolve
        pause
        scene protaIrHablarConDirector_27 with dissolve
        play sound "audio/effects/quitarRopa.ogg"
        narrador "Le quita los pantalones"
        scene protaIrHablarConDirector_28 with dissolve
        pause
        scene protaIrHablarConDirector_29 with dissolve
        pause
        scene protaIrHablarConDirector_30 with dissolve
        pause
        scene protaIrHablarConDirector_31 with dissolve
        pause
        scene protaIrHablarConDirector_32 with dissolve
        pause
        scene protaIrHablarConDirector_33 with dissolve
        clairePensa "Le está creciendo..."
        scene protaIrHablarConDirector_34 with dissolve
        clairePensa "La tiene bastante grande. Espero que con esto se corra rápido y termine esta pesadilla."
        scene protaIrHablarConDirector_35 with dissolve
        pause
        scene protaIrHablarConDirector_36 with dissolve
        pause
        scene protaIrHablarConDirector_37 with dissolve
        pause
        scene protaIrHablarConDirector_38 with dissolve
        pause
        call bucle1PajaDirectorEscenaReproductor



    else:
        scene protaIrHablarConDirector_15_NONTR with dissolve
        directorUni "Claire estas teniendo muy malas notas y sabes que tienes que mejorar.Tu futuro esta en juego"
        scene protaIrHablarConDirector_16_NONTR with dissolve
        claire "Intentare mejorar..."
        scene protaIrHablarConDirector_17_NONTR with dissolve
        claire "gracias."
        scene protaIrHablarConDirector_12 with dissolve
        "TOC TOC"
        scene protaIrHablarConDirector_18_NONTR with dissolve
        pause
        scene protaIrHablarConDirector_19_NONTR with dissolve
        protaPensa "El director tambien ha llamado a Claire?"
        scene protaIrHablarConDirector_20_NONTR with dissolve
        pause
        scene protaIrHablarConDirector_81 with dissolve
        prota "Buenos días."
        scene protaIrHablarConDirector_82 with dissolve
        pause
        scene protaIrHablarConDirector_81 with dissolve
        directorUni "Buenos días. No hace falta que te sientes, será breve."
        scene protaIrHablarConDirector_83 with dissolve
        prota "Alicia me ha dicho que querías hablar conmigo."
        scene protaIrHablarConDirector_84 with dissolve
        directorUni "Así es..."
        directorUni "Quería hablar contigo porque este mes no hemos recibido el pago de la Universidad."
        scene protaIrHablarConDirector_85 with dissolve
        prota "Pero eso no es posible..."
        scene protaIrHablarConDirector_84 with dissolve
        directorUni "Sabemos que nunca hemos tenido problemas de impago contigo, pero este mes no hemos recibido el ingreso."
        scene protaIrHablarConDirector_84 with dissolve
        directorUni "Esta Universidad es de las mejores del país con un personal muy profesional y muy respetado. Solo acude gente importante. Por ahora haremos la vista gorda, pero espero que soluciones el problema lo antes posible, o tomaremos cartas en el asunto."
        scene protaIrHablarConDirector_86 with dissolve
        prota "Yo... no sabía nada... Lo hablare en casa."
        scene protaIrHablarConDirector_84 with dissolve
        directorUni "Bien, solo era eso. Por favor cierra la puerta al salir."
        directorUni "Gracias."
        scene protaIrHablarConDirector_82 with dissolve
        prota "Esta bien, adiós."
        play sound "audio/effects/open_door.ogg"
        scene protaIrHablarConDirector_88_ with dissolve
        pause
        scene protaIrHablarConDirector_89_ with dissolve
        protaPensa "Oh mierda... esto no me está gustando... Tendré que hablar con [nombreMad3] y [nombrePad3]. Espero que solo sea un mal entendido."


    show screen escenasClaire("Claire")

    return

label menu1ProtaHablarConDirectorDineroEscenaReproductor:
    menu:
        "Seguir":
            call eventProtaHablarConDirectorDinero2EscenaReproductor

        "Hacer otra Paja":
            scene protaIrHablarConDirector_38 with dissolve
            pause
            call bucle1PajaDirectorEscenaReproductor
    return


label bucle1PajaDirectorEscenaReproductor():
    $ numPajaDirector1 = 0
    while numPajaDirector1 < 5:
        scene protaIrHablarConDirector_39 with dissolve
        play sound "audio/effects/flip.ogg"
        pause 0.1
        play sound "audio/effects/flap.ogg"
        scene protaIrHablarConDirector_40 with dissolve
        pause 0.1
        $ numPajaDirector1 = numPajaDirector1 +1
    call menu1ProtaHablarConDirectorDineroEscenaReproductor
    return

label eventProtaHablarConDirectorDinero2EscenaReproductor:
    scene protaIrHablarConDirector_41 with dissolve
    directorUni "Métetela en la boca."
    scene protaIrHablarConDirector_42 with dissolve
    claire "¿Qué? Pero pensé que con esto ya era suficiente. Eso es demasiado…"
    scene protaIrHablarConDirector_43 with dissolve
    directorUni "¡Silencio! Aquí las ordenes las doy yo. Esto solo acaba de empezar."
    scene protaIrHablarConDirector_44 with dissolve
    directorUni "O haces lo que te digo o tu futuro será el de fregar suelos."
    directorUni "Abre la boca y empieza a chupar."
    scene protaIrHablarConDirector_45 with dissolve
    play sound "audio/effects/llanto.ogg"
    "sniff..."
    scene protaIrHablarConDirector_46 with dissolve
    pause
    scene protaIrHablarConDirector_47 with dissolve
    clairePensa "Dios mío que estoy haciendo... Tengo que aprobar este curso no me queda otra."
    clairePensa "Sabe a salado."
    scene protaIrHablarConDirector_48 with dissolve
    pause
    scene protaIrHablarConDirector_49 with dissolve
    pause
    play sound "audio/effects/flip.ogg"
    scene protaIrHablarConDirector_50 with dissolve

    pause
    play sound "audio/effects/flap.ogg"
    scene protaIrHablarConDirector_51 with dissolve
    pause
    play sound "audio/effects/flap.ogg"
    scene protaIrHablarConDirector_52 with dissolve
    directorUni "Así me gusta calladita y obediente. Empieza a moverte y succiona bien."
    call bucleMamada1DirectorEscenaReproductor

    return

label menu2ProtaHablarConDirectorDineroEscenaReproductor:
    menu:
        "Seguir":
            call eventProtaHablarConDirectorDinero3EscenaReproductor

        "Hacer otra Mamada":
            call bucleMamada1DirectorEscenaReproductor
    return

label bucleMamada1DirectorEscenaReproductor():
    show ClaireChupandoAnim100
    pause(8)
    call menu2ProtaHablarConDirectorDineroEscenaReproductor
    return


label eventProtaHablarConDirectorDinero3EscenaReproductor():
    scene protaIrHablarConDirector_53 with dissolve
    directorUni "Esta bien pero seguro que puedes hacerlo mejor... Métela más."
    scene protaIrHablarConDirector_54 with dissolve
    claire "Por favor ya está, hice todo lo que me pediste."
    scene protaIrHablarConDirector_53 with dissolve
    directorUni "¡¡Abre la boca bien y métela toda!!"
    scene protaIrHablarConDirector_54 with dissolve
    play sound "audio/effects/llanto2.ogg"
    claire "Ya no puedo más por favor… "
    scene protaIrHablarConDirector_55 with dissolve
    pause
    scene protaIrHablarConDirector_56 with dissolve
    directorUni "Quita las manos."
    scene protaIrHablarConDirector_57 with dissolve
    claire "Por favor..."
    directorUni "Quita las manos, no lo volveré a repetir."
    scene protaIrHablarConDirector_58 with dissolve
    pause
    call bucle1MamadaProfundaDirectorEscenaReproductor
    return

label menu3ProtaHablarConDirectorDineroEscenaReproductor():
    stop sound
    menu:
        "Seguir":
            call eventProtaHablarConDirectorDinero4EscenaReproductor

        "Hacer otra garganta profunda camara1":
            scene protaIrHablarConDirector_58 with dissolve
            pause
            call bucle1MamadaProfundaDirectorEscenaReproductor
        "Hacer otra garganta profunda camara2":
            scene protaIrHablarConDirector_74 with dissolve
            pause
            call bucle2MamadaProfundaDirectorEscenaReproductor
            return
    return




label bucle1MamadaProfundaDirectorEscenaReproductor():
    $ numMamadaProfundaClaireDirector1 = 0
    while numMamadaProfundaClaireDirector1 < 2:
        play sound "audio/effects/gargantaprofunda1.ogg"
        scene protaIrHablarConDirector_59 with dissolve
        pause 0.1
        scene protaIrHablarConDirector_60 with dissolve
        if numMamadaProfundaClaireDirector1 == 0:
            directorUni "Así muy bien.{p=0.5}{nw}"

        pause 0.1
        scene protaIrHablarConDirector_62 with dissolve
        pause 0.1
        scene protaIrHablarConDirector_65 with dissolve
        if numMamadaProfundaClaireDirector1 == 1:
            directorUni "Te voy a rellenar todos los agujeros. Ya veras...{p=1}{nw}"
        pause 0.1
        scene protaIrHablarConDirector_64 with dissolve
        pause 0.1
        scene protaIrHablarConDirector_61 with dissolve
        pause 0.1
        scene protaIrHablarConDirector_59 with dissolve
        pause 0.1
        scene protaIrHablarConDirector_61 with dissolve
        pause 0.1
        scene protaIrHablarConDirector_62 with dissolve
        pause 0.1
        scene protaIrHablarConDirector_63 with dissolve
        pause 0.1
        scene protaIrHablarConDirector_64 with dissolve
        pause 0.1
        scene protaIrHablarConDirector_65 with dissolve
        pause 0.05
        scene protaIrHablarConDirector_68 with dissolve
        if numMamadaProfundaClaireDirector1 == 0:
            directorUni "Serás mi putita y te hare disfrutar...{p=1}{nw}"
        pause 0.1
        scene protaIrHablarConDirector_67 with dissolve
        pause 0.1
        scene protaIrHablarConDirector_66 with dissolve
        pause 0.1
        scene protaIrHablarConDirector_68 with dissolve
        pause 0.1
        scene protaIrHablarConDirector_64 with dissolve
        pause 0.1
        scene protaIrHablarConDirector_61 with dissolve
        pause 0.1
        $ numMamadaProfundaClaireDirector1 = numMamadaProfundaClaireDirector1 +1
    call menu3ProtaHablarConDirectorDineroEscenaReproductor
    return

label bucle2MamadaProfundaDirectorEscenaReproductor():
    $ numMamadaProfundaClaireDirector2 = 0
    while numMamadaProfundaClaireDirector2 < 2:
        play sound "audio/effects/gargantaprofunda1.ogg"
        scene protaIrHablarConDirector_74 with dissolve
        pause 0.1
        scene protaIrHablarConDirector_73 with dissolve
        pause 0.1
        scene protaIrHablarConDirector_75 with dissolve
        pause 0.1
        scene protaIrHablarConDirector_72 with dissolve
        pause 0.1
        scene protaIrHablarConDirector_70 with dissolve
        pause 0.1
        scene protaIrHablarConDirector_69 with dissolve
        if numMamadaProfundaClaireDirector2 == 0:
            directorUni "¿Te gusta cómo te follo la boca? Se que lo estas disfrutando.{p=1.8}{nw}"
        pause 0.1
        scene protaIrHablarConDirector_70 with dissolve
        pause 0.1
        scene protaIrHablarConDirector_71 with dissolve
        pause 0.1
        scene protaIrHablarConDirector_70 with dissolve
        pause 0.1
        scene protaIrHablarConDirector_72 with dissolve
        pause 0.1
        scene protaIrHablarConDirector_73 with dissolve
        pause 0.1
        if numMamadaProfundaClaireDirector2 == 1:
            directorUni "Te dicho que abras bien la boca.{p=1}{nw}"
        scene protaIrHablarConDirector_72 with dissolve
        pause 0.1
        scene protaIrHablarConDirector_74 with dissolve
        pause 0.1
        scene protaIrHablarConDirector_70 with dissolve
        pause 0.1
        $ numMamadaProfundaClaireDirector2 = numMamadaProfundaClaireDirector2 +1
    call menu3ProtaHablarConDirectorDineroEscenaReproductor
    return

label eventProtaHablarConDirectorDinero4EscenaReproductor():
    scene protaIrHablarConDirector_76 with dissolve
    directorUni "Toda adentro. Así me gusta, que te portes bien."
    play sound "audio/effects/toctoc.ogg"
    scene protaIrHablarConDirector_77 with dissolve
    stop music
    "Toc Toc!!!"
    scene protaIrHablarConDirector_78 with dissolve
    directorUni "Joder metete debajo de la mesa y no hagas ruido."

    scene protaIrHablarConDirector_79 with dissolve

    directorUni "Adelante."
    play sound "audio/effects/open_door.ogg"
    scene protaIrHablarConDirector_80 with dissolve
    play music "audio/music/sneaky-adventure-by-kevin-macleod.ogg" fadein 3.0
    pause
    scene protaIrHablarConDirector_81 with dissolve
    prota "Buenos días."
    scene protaIrHablarConDirector_82 with dissolve
    protaPensa "Juraría que estaba hablando con alguien..."
    scene protaIrHablarConDirector_81 with dissolve
    directorUni "Buenos días. No hace falta que te sientes, será breve."
    scene protaIrHablarConDirector_83 with dissolve
    prota "Alicia me ha dicho que querías hablar conmigo."
    scene protaIrHablarConDirector_84 with dissolve
    directorUni "Así es..."
    directorUni "Quería hablar contigo porque este mes no hemos recibido el pago de la Universidad."
    scene protaIrHablarConDirector_85 with dissolve
    prota "Pero eso no es posible..."
    scene protaIrHablarConDirector_84 with dissolve
    directorUni "Sabemos que nunca hemos tenido problemas de impago contigo, pero este mes no hemos recibido el ingreso."
    scene protaIrHablarConDirector_84 with dissolve
    directorUni "Esta Universidad es de las mejores del país con un personal muy profesional y muy respetado. Solo acude gente importante. Por ahora haremos la vista gorda, pero espero que soluciones el problema lo antes posible, o tomaremos cartas en el asunto."
    scene protaIrHablarConDirector_86 with dissolve
    prota "Yo... no sabía nada... Lo hablare en casa."
    scene protaIrHablarConDirector_87 with dissolve
    directorUni "Bien, solo era eso. Por favor cierra la puerta al salir."
    directorUni "Gracias."
    scene protaIrHablarConDirector_82 with dissolve
    prota "Esta bien, adiós."
    play sound "audio/effects/open_door.ogg"
    scene protaIrHablarConDirector_88_ with dissolve
    pause
    scene protaIrHablarConDirector_89_ with dissolve
    protaPensa "Oh mierda... esto no me está gustando... Tendré que hablar con [nombreMad3] y [nombrePad3]. Espero que solo sea un mal entendido."



    scene protaIrHablarConDirector_88 with dissolve
    directorUni "Nos han interrumpido, pero no pasa nada, levántate."
    scene protaIrHablarConDirector_89 with dissolve
    claire "Yo ya me tengo que ir."
    scene protaIrHablarConDirector_90 with dissolve
    directorUni "Tranquila, solo un poco mas y ya estara..."
    play music "audio/music/ChillWave_kevinMacLeod.ogg" fadein 2.0
    play sound "audio/effects/tiraraMesa.ogg"
    scene protaIrHablarConDirector_91 with hpunch
    directorUni "A ver que tienes por aquí."
    scene protaIrHablarConDirector_92 with dissolve
    claire "No por favor todo menos eso... ¡¡¡Por favor!!!"
    play sound "audio/effects/llanto.ogg"
    scene protaIrHablarConDirector_93 with dissolve
    directorUni "¡¡Silencio!! bien que hace un rato me la estabas chupando entera y sin rechistar. Se muy bien que esto te gusta."
    scene protaIrHablarConDirector_94 with dissolve
    claire "Espera no..."
    scene protaIrHablarConDirector_95 with dissolve
    pause
    scene protaIrHablarConDirector_96 with dissolve
    pause
    scene protaIrHablarConDirector_97 with dissolve
    directorUni "Voy a disfrutar esto y quiero que estés calladita o tendrás problemas."
    scene protaIrHablarConDirector_98 with dissolve
    play sound "audio/effects/llanto2.ogg"
    pause
    scene protaIrHablarConDirector_99 with dissolve
    pause
    scene protaIrHablarConDirector_100 with dissolve
    directorUni "Pero si estas chorreando... Lo estabas deseando."
    play sound "audio/effects/roze1.ogg"
    scene protaIrHablarConDirector_101 with dissolve

    pause
    scene protaIrHablarConDirector_100 with dissolve
    pause
    play sound "audio/effects/roze1.ogg"
    scene protaIrHablarConDirector_101 with dissolve
    pause
    scene protaIrHablarConDirector_100 with dissolve
    pause
    play sound "audio/effects/roze3.ogg"
    scene protaIrHablarConDirector_102 with dissolve
    #play sound "audio/effects/roze1.ogg"
    pause
    play sound "audio/effects/juegaBragas1.ogg"
    scene protaIrHablarConDirector_103 with dissolve
    pause
    directorUni "Vienes a clase con esta ropa de puta y encima decías que no querías."
    scene protaIrHablarConDirector_104 with dissolve
    play sound "audio/effects/juegaBragas2.ogg"
    directorUni "Tremenda puta estas hecha... No paras de chorrear y tienes todo el coño abierto."
    scene protaIrHablarConDirector_105 with dissolve
    play sound "audio/effects/llanto.ogg"
    claire "Por favor ya no mas..."
    scene protaIrHablarConDirector_106 with dissolve
    play sound "audio/effects/roze4.ogg"
    directorUni "Tu coño no dice lo mismo, mira lo abierto que lo tienes y como gotea..."
    scene protaIrHablarConDirector_105 with dissolve
    play sound "audio/effects/rozeOrgasm.ogg"
    directorUni "¿Y esos gemidos?"
    scene protaIrHablarConDirector_106 with dissolve
    pause
    play sound "audio/effects/rozeOrgasm2.ogg"
    scene protaIrHablarConDirector_107 with dissolve
    pause
    play sound "audio/effects/rozeOrgasm.ogg"
    scene protaIrHablarConDirector_108 with dissolve
    pause
    scene protaIrHablarConDirector_109 with dissolve
    directorUni "¿Porque tienes el coño tan abierto y chorreando?"
    directorUni "hahahaha"
    scene protaIrHablarConDirector_110 with dissolve
    pause
    scene protaIrHablarConDirector_111 with dissolve
    clairePensa "{size=15}{i}Como siga así voy a perder la conciencia. Este desgraciado sabe lo que hace, me está volviendo loca con lo que hace y no puedo evitarlo...{/size}{/i}"
    scene protaIrHablarConDirector_112 with dissolve
    play sound "audio/effects/rozeOrgasm3.ogg"
    directorUni "Prepárate porque ahora te lo abriré más."

    clairePensa "..."
    scene protaIrHablarConDirector_111 with dissolve
    clairePensa "{size=15}{i}Dios no puedo moverme estoy paralizada, este cerdo me va a follar. No para de frotarme el clítoris y abrirme el coño con mis bragas.{/size}{/i}"
    scene protaIrHablarConDirector_112 with dissolve
    play sound "audio/effects/rozeOrgasm3.ogg"
    claire "haaaaaa haaay"
    scene protaIrHablarConDirector_113 with dissolve

    pause
    play sound "audio/effects/abrirCony.ogg"
    scene protaIrHablarConDirector_114 with dissolve
    pause 0.2
    play sound "audio/effects/juegaBragas2.ogg"

    directorUni "Tremenda puta estas hecha, mira cómo te abres de piernas tu sola y te colocas en buena posición. Y tu coño no para de gotear."
    scene protaIrHablarConDirector_115 with dissolve
    play sound "audio/effects/juegaBragas2.ogg"

    pause

    scene protaIrHablarConDirector_114 with dissolve
    play sound "audio/effects/abrirCony2.ogg"
    pause
    scene protaIrHablarConDirector_117 with dissolve
    play sound "audio/effects/abrirCony3.ogg"
    directorUni "Te estas corriendo y no dices nada. Te está encantando."

    scene protaIrHablarConDirector_116 with dissolve
    pause
    play sound "audio/effects/abrirCony.ogg"
    scene protaIrHablarConDirector_118 with dissolve
    pause
    play sound "audio/effects/juegaBragas2.ogg"
    scene protaIrHablarConDirector_116 with dissolve
    pause
    scene protaIrHablarConDirector_115 with dissolve
    pause
    play sound "audio/effects/abrirCony3.ogg"
    scene protaIrHablarConDirector_117 with dissolve
    directorUni "Cada vez chorreas más, se acabaron los juegos..."
    scene protaIrHablarConDirector_119 with dissolve
    clairePensa "{size=15}{i}No puedo más, me tiene bajo su control, necesito hacer esto para pasar el semestre.{/size}{/i}"
    scene protaIrHablarConDirector_119 with dissolve
    claire "aaaaaaaay"
    scene protaIrHablarConDirector_121 with dissolve
    pause
    scene protaIrHablarConDirector_122 with dissolve
    play sound "audio/effects/closeCony.ogg"
    directorUni "Te rellenare todo el coño, solo te falta suplicar por ello."
    scene protaIrHablarConDirector_123 with dissolve
    pause
    scene protaIrHablarConDirector_124 with dissolve
    pause
    scene protaIrHablarConDirector_125 with dissolve
    pause
    play sound "audio/effects/grito.ogg"
    scene protaIrHablarConDirector_126 with dissolve

    claire "¡¡No!! eso no!!"
    play sound "audio/effects/pollaentra.ogg"
    scene protaIrHablarConDirector_127 with dissolve

    directorUni "¡¡Calla!! pero si sabes que lo estas suplicando."
    narrador "¡Un momento!"
    narrador "Siento interrumpir, pero…"
    narrador "Estas jugando al juego Demon Boy Saga, no al Demon Director Saga."
    narrador "Ya hemos visto demasiado del director..."
    narrador "Volvamos donde dejamos la aventura de nuestro protagonista..."
    play sound "audio/effects/risaDemon.ogg"
    narrador "XD"

    show screen escenasClaire("Claire")

    return
