label eventClaireNotasClaseAlicia():
    if eventClaireNotasClaseAlicia == False:
        $ eventClaireNotasClaseAlicia = True
        stop music fadeout 3.0

        scene eventClaireNotasClaseAlicia_0
        alicia "Dejar de hablar que empezamos la clase."
        play music "audio/music/loopster-by-kevin-macleod.ogg" fadein 3.0

        scene eventClaireNotasClaseAlicia_1 with dissolve
        alicia "Antes de nada, comentar que hay gente que como siga así no aprobara las asignaturas."
        scene eventClaireNotasClaseAlicia_2 with dissolve
        alicia "Claire he detectado un error en tu expediente y lo he rectificado. Por algún error tenías un aprobado, pero ya lo he corregido."
        scene eventClaireNotasClaseAlicia_3 with dissolve
        alicia "Lo siento pero por ahora estas suspendida."

        play sound "audio/effects/sorpresa3.ogg"
        scene eventClaireNotasClaseAlicia_4 with dissolve
        pause 0.5
        scene eventClaireNotasClaseAlicia_5 with dissolve
        claire "¿Que?"
        scene eventClaireNotasClaseAlicia_6 with dissolve
        claire "¡Eso no es posible!"
        scene eventClaireNotasClaseAlicia_7 with dissolve
        alicia "Yo no tengo la culpa de que no te esfuerces en clase y no estudies para los exámenes."
        scene eventClaireNotasClaseAlicia_8 with dissolve
        claire "Pero yo estaba aprobada."
        scene eventClaireNotasClaseAlicia_9 with dissolve
        alicia "Ya te dije que las notas estaban equivocadas y no me repliques más."
        scene eventClaireNotasClaseAlicia_10 with dissolve
        claire "Eso no es justo, no puede ser."
        scene eventClaireNotasClaseAlicia_11 with dissolve
        alicia "Vete al pasillo y te aireas un poco y así nos dejas hacer clase."
        scene eventClaireNotasClaseAlicia_12 with dissolve
        play sound "audio/effects/tacones.ogg"
        claire "¡Esto es un asco!"
        scene eventClaireNotasClaseAlicia_13 with dissolve
        alicia "Continuemos con la clase."
        scene eventClaireNotasClaseAlicia_14 with dissolve
        alicia "¿Te hace gracia?"
        scene eventClaireNotasClaseAlicia_15 with dissolve
        prota "¡Pero si no hice nada!"
        scene eventClaireNotasClaseAlicia_16 with dissolve
        alicia "Vete tu también al pasillo que te ira bien."
        scene eventClaireNotasClaseAlicia_17 with dissolve
        prota "¡Maldita sea!"
        play sound "audio/effects/puertaEntrada.ogg"
        scene eventClaireNotasClaseAlicia_18 with dissolve
        pause
        scene eventClaireNotasClaseAlicia_19 with dissolve
        claire "¿Qué haces tu aquí? Lo que me faltaba..."
        scene eventClaireNotasClaseAlicia_20 with dissolve
        prota "A mí no me digas, se enfadó contigo y también lo pago conmigo."
        scene eventClaireNotasClaseAlicia_21 with dissolve
        claire "Arghh.. no me molestes."
        scene eventClaireNotasClaseAlicia_22 with dissolve
        pause
        scene eventClaireNotasClaseAlicia_23 with dissolve
        protaPensa "Es la primera vez que estoy junto a Claire sin que nadie moleste."
        menu:
            "Preguntar por los estudios":
                scene eventClaireNotasClaseAlicia_24 with dissolve
                prota "No sabía que te importaban tanto los estudios."
                scene eventClaireNotasClaseAlicia_25 with dissolve
                claire "Es evidente que me importan. Metete en tus asuntos perdedor."
                if ntrGame == True:
                    scene eventClaireNotasClaseAlicia_26 with dissolve
                    protaPensa "Está claro que los tocamientos del director están relacionados con sus estudios. Una lástima que no tenga pruebas. Aunque meterme con el director me daría muchos problemas."
                scene eventClaireNotasClaseAlicia_27 with dissolve
                pause
                play sound "audio/effects/timbreColegio.ogg"
                scene eventClaireNotasClaseAlicia_28 with dissolve
                pause
                play sound "audio/effects/tacones.ogg"
                scene eventClaireNotasClaseAlicia_29 with dissolve
                prota "Adiós."
                scene eventClaireNotasClaseAlicia_30 with dissolve
                pause
                scene eventClaireNotasClaseAlicia_31 with dissolve
                pause
                call nextTiempoDay(0)


    return
