label openingTuto:
    #scene prueba at pandown1
    #$ renpy.pause(4)
    scene op_1
    #scene parque with fade
    narrador "Antes de empezar, este juego es de elección libre.(Sandbox)"
    scene op_2 with dissolve
    narrador "Dispones de una barra de tiempo con sus días y horas correspondientes."
    narrador "El reloj de arena hara que pase el tiempo. (Ej: de tarde a noche)"
    scene op_1 with dissolve
    narrador "Podrás desplazarte libremente y escoger la ruta deseada."
    scene tuto_1 with dissolve
    narrador "Para desplazarte tienes que seleccionar una puerta o el final del camino."
    scene tuto_2 with dissolve
    pause
    scene tuto_3 with dissolve
    narrador "El HUD mostrara el nombre de la zona seleccionada."
    scene tuto_4 with dissolve
    pause
    narrador "Hay zonas que, para volver a la zona anterior hay que darle click a la flecha roja."
    scene tuto_5 with dissolve
    pause
    scene tuto_1 with dissolve
    pause
    scene tuto_6 with dissolve
    narrador "Este icono muestra donde se encuentran las personas que viven en casa en ese momento."
    scene tuto_7 with dissolve
    pause
    scene tuto_7_ with dissolve
    narrador "Puedes desplazarte a la zona indicada haciendo click"
    scene tuto_7__ with dissolve
    narrador "En el mapa dispones de un acceso directo para volver a tu habitación"
    scene tuto_7___ with dissolve
    pause
    scene op_1 with dissolve
    narrador "El juego tiene un sistema de inventario donde podrás recoger o comprar objetos siempre que dispongas de dinero."

    scene tuto_8 with dissolve
    narrador "El juego dispone de un sistema de ayudas para poder avanzar en la aventura"
    scene tuto_9 with dissolve
    pause
    scene tuto_10 with dissolve
    pause
    scene op_1 with dissolve
    narrador "Durante el juego habrá avisos como estos para advertirte de cambios en los stats de los personajes."

    call avisosStatsSms("sms") from _call_avisosStatsSms
    call avisosStatsSms("afecto") from _call_avisosStatsSms_1
    call avisosStatsSms("sumision") from _call_avisosStatsSms_2
    call avisosStatsSms("newContact") from _call_avisosStatsSms_3
    scene op_1
    narrador "La aventura dispone de animaciones como esta"
    show hermanPeqBajaPantaPijam #codec xvid
    pause(5)
    #e "ejemplo ñe"
    #window hide
    pause
    scene op_1
    scene op_4 with dissolve
    narrador "El juego se encuentra en desarrollo.\nSi te gusta, apoya el proyecto."

    #narrador "Aviso, el uso de trucos puede corromper el juego y crear bugs inesperados"
    narrador "disfruta del juego"
    return
