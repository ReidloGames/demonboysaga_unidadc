label eventCambiateDeRopa():
    if eventCambiateDeRopa == False:
        $ eventCambiateDeRopa = True
        scene salirPrimeraVez_0
        pause
        scene salirPrimeraVez_1 with dissolve
        protaPensa "No puedo salir con esta ropa."
        play music "audio/music/funkorama-by-kevin-macleod.ogg" fadein 2.0
        protaPensa "Tengo que ir a cambiarme."
        scene salirPrimeraVez_2 with dissolve
        pause
        scene salirPrimeraVez_3 with dissolve
        "Unos minutos mas tarde..."
        scene salirPrimeraVez_4 with dissolve
        protaPensa "Ahora sí que ya estoy listo."


    return
