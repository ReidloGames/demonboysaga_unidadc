label eventoAccesosComodinTuto():
    if eventoAccesosComodinTuto == False and eventComunicadoPadCuentaProblema == True:
        $ eventoAccesosComodinTuto = True
        $ numDiaVienenLosDelBanco = numDiaTotal
        scene comodin1
        narrador "A partir de ahora se activarán los accesos comodín."
        scene comodin1_ with dissolve
        narrador "Estos accesos saldrán en el mapa indicados con la letra C y fuera de las habitaciones de las chicas."
        scene comodin2 with dissolve
        pause
        scene comodin3 with dissolve
        pause
        scene comodin4 with dissolve
        pause
        scene comodin5 with dissolve
        pause
        scene comodin6 with dissolve
        narrador "Estos accesos comodín permitirán acceder a una interacción sin tener que esperar una semana entera."
        narrador "Por cada interacción hay 1 acceso comodín que se activara dependiendo el día y la hora."
    return
