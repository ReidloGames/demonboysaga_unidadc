label eventMaySeHaDejadoLLavesTienda():
    $ eventMaySeHaDejadoLLavesTienda = True
    $ tengoLlaveHabitacionKara = True
    stop music fadeout 2.0
    call esconderHud
    play sound "audio/effects/tonoMovil.ogg"
    if tiempoDia == 0:
        call nextTiempoDay(0)
    scene mayLlamaFaltanLlavesTienda_00
    "¡RIIIING!"
    scene mayLlamaFaltanLlavesTienda_01 with dissolve
    stop sound
    hermanMay "Hola [nombreProta], soy Kara."
    scene mayLlamaFaltanLlavesTienda_02 with dissolve
    prota "Hola Kara"
    hermanMay "Necesito que me hagas un favor. Creo que me dejado el bolso con las llaves en mi habitación y no puedo abrir la tienda."
    scene mayLlamaFaltanLlavesTienda_03 with dissolve
    hermanMay "Puedes comprobarlo?"
    scene mayLlamaFaltanLlavesTienda_02 with dissolve
    prota "¡Claro!"
    scene mayLlamaFaltanLlavesTienda_04 with dissolve
    pause
    scene mayLlamaFaltanLlavesTienda_05 with dissolve
    protaPensa "Aquí está el bolso."
    scene mayLlamaFaltanLlavesTienda_06 with dissolve
    play sound "audio/effects/roze3.ogg"
    protaPensa "A ver que tenemos por aquí..."
    play sound "audio/effects/roze1.ogg"
    scene mayLlamaFaltanLlavesTienda_07 with dissolve
    pause
    play sound "audio/effects/roze3.ogg"
    scene mayLlamaFaltanLlavesTienda_08 with dissolve
    protaPensa "Un támpax... jejeje"
    play sound "audio/effects/roze1.ogg"
    scene mayLlamaFaltanLlavesTienda_07 with dissolve
    protaPensa "Un spray pimienta..."
    play sound "audio/effects/roze3.ogg"
    scene mayLlamaFaltanLlavesTienda_06 with dissolve
    pause
    scene mayLlamaFaltanLlavesTienda_08 with dissolve
    protaPensa "¡Aquí están las llaves!"
    scene mayLlamaFaltanLlavesTienda_09 with dissolve
    play music "audio/music/Disco_Medusae-kevin_macleod.ogg" fadein 2.0
    protaPensa "¡Y la llave de su habitación!"
    protaPensa "Me voy a llevarle el bolso, pero antes pasare por el cerrajero para hacer una copia de la llave."
    scene mayLlamaFaltanLlavesTienda_10 with dissolve
    protaPensa "Hoy es mi día de suerte..."
    call avisosStatsSms("newUbication") from _call_avisosStatsSms_14
    narrador "Ahora tienes una nueva zona en el mapa."
    $ puedesIrTiendaKara = True
    scene minutosMasTarde with dissolve
    "Unos minutos mas tarde..."
    #stop music fadeout 3.0
    play music "audio/music/fretless-by-kevin-macleod.ogg" fadein 2.0
    scene mayLlamaFaltanLlavesTienda_11 with dissolve
    pause
    scene mayLlamaFaltanLlavesTienda_12 with dissolve
    hermanMay "Muchas gracias por hacerme el favor."
    scene mayLlamaFaltanLlavesTienda_13 with dissolve
    pause
    scene mayLlamaFaltanLlavesTienda_14 with dissolve
    pause
    scene mayLlamaFaltanLlavesTienda_15 with dissolve
    prota "Ya sabes que me encanta ayudarte..."

    $ zona = "tiendaKara"
    call nextZona("tiendaKara1") from _call_nextZona_14
    #call mostrarHud()

    return
