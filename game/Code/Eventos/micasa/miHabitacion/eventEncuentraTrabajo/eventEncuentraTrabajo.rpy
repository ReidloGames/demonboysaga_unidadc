label eventEncuentraTrabajo():
    if eventInformarHipotecaBanco == True and eventEncuentraTrabajo == False:
        hide screen protaEnCama
        $ eventEncuentraTrabajo = True
        stop music fadeout 3.0
        scene buscaEmpleoYChinoMandinEvent_0
        protaPensa "Voy a buscar en internet algún empleo"
        scene buscaEmpleoYChinoMandinEvent_1 with dissolve
        play sound "audio/effects/teclear.ogg"
        protaPensa "Buscando en Coocle..."
        scene buscaEmpleoYChinoMandinEvent_2 with dissolve
        pause
        scene buscaEmpleoYChinoMandinEvent_3 with dissolve
        protaPensa "Buscando empleos..."
        scene buscaEmpleoYChinoMandinEvent_3 with dissolve
        protaPensa "Aquí hay uno de pasear perros, pero voy a buscar un poco más. Paso de recoger mierdas."
        scene buscaEmpleoYChinoMandinEvent_4 with dissolve
        play sound "audio/effects/teclear.ogg"
        protaPensa "mmmm... Repartidor de paquetes."
        protaPensa "Este creo que me podría ir bien."
        "Petición enviada"
        protaPensa "Vale, ahora a esperar a que me digan algo..."
        scene buscaEmpleoYChinoMandinEvent_5 with dissolve
        protaPensa "¿Esto que es?"
        scene buscaEmpleoYChinoMandinEvent_6 with dissolve
        protaPensa "Parece una promoción para un curso de masajes."
        play sound "audio/effects/clickMouse.ogg"
        "Click"
        scene buscaEmpleoYChinoMandinEvent_7 with dissolve
        play music "audio/music/loopster-by-kevin-macleod.ogg" fadein 2.0
        pause
        scene buscaEmpleoYChinoMandinEvent_8 with dissolve
        mandin "Soy Mandin el chino mandarín."
        scene buscaEmpleoYChinoMandinEvent_9 with dissolve
        mandin "Experto en masajes."
        scene buscaEmpleoYChinoMandinEvent_10 with dissolve
        mandin "Mis manos son un arte"
        scene buscaEmpleoYChinoMandinEvent_11 with dissolve
        mandin "Si tu mujeres querer"
        #Haz mis cursos, que seguro los disfrutaras.
        #Do my courses, you will enjoy them for sure..
        scene buscaEmpleoYChinoMandinEvent_12 with dissolve
        mandin "Haz mis cursos, no te los podrás perder."
        play sound "audio/effects/guinyo.ogg"
        scene buscaEmpleoYChinoMandinEvent_13 with dissolve
        pause
        scene buscaEmpleoYChinoMandinEvent_14 with dissolve
        protaPensa "Ohhh, primer curso gratis."
        scene buscaEmpleoYChinoMandinEvent_15 with dissolve
        protaPensa "Espera que me lo pienso..."
        scene buscaEmpleoYChinoMandinEvent_16 with dissolve
        protaPensa "¡¡¡Apuntado!!!"
        scene buscaEmpleoYChinoMandinEvent_17 with dissolve
        protaPensa "Ahora puedo hacer el primer curso desde el ordenador."
        #sonidoEmail
        play sound "audio/effects/email.ogg"
        scene buscaEmpleoYChinoMandinEvent_18 with dissolve
        protaPensa "Acabo de recibir un mail..."
        scene buscaEmpleoYChinoMandinEvent_19 with dissolve
        "Hola, somos Amarzond, acabamos de ver su petición para trabajar con nosotros. Puede venir a la dirección adjuntada en el mail y hablaremos de ello."
        "Te esperamos, un cordial saludo."
        #Ubicacion Nueva
        call avisosStatsSms("newUbication") from _call_avisosStatsSms_12
        narrador "Ahora tienes una nueva zona en el mapa."
        $ puedesIrAmarzond = True
        scene buscaEmpleoYChinoMandinEvent_20 with dissolve
        protaPensa "¡Genial! parece que el destino me sonrie."
        #ActivarCurso
        $ verCursos = True
        narrador "Ahora tienes disponible un nuevo curso."

    return
