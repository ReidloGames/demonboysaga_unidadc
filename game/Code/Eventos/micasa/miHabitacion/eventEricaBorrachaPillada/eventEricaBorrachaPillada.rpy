label eventEricaBorrachaPillada():
    if eventEricaBorrachaPillada == False:
        hide screen protaEnCama
        $ eventEricaBorrachaPillada = True
        stop music fadeout 2.0
        scene protaPillaBorrachaAMed_0
        pause
        scene protaPillaBorrachaAMed_1 with dissolve
        pause
        scene Ntr_Info  with dissolve
        "Información"
        if ntrGame == False:
            "El NTR está desactivado."
            "Quieres Activar el NTR?"
            menu:
                "{size=30}{b} Si {/b}{/size}":
                    $ ntrGame = True
                    "NTR activado"
                "{size=30}{b} No {/b}{/size}":
                    $ ntrGame = False
                    "NTR desactivado"
        elif ntrGame == True:
            "El NTR está activado."
            "Quieres Desactivar el NTR?"
            menu:
                "{size=30}{b} Si {/b}{/size}":
                    $ ntrGame = False
                    "NTR desactivado"
                "{size=30}{b} No {/b}{/size}":
                    $ ntrGame = True
                    "NTR activado"

        scene protaPillaBorrachaAMed_1 with dissolve
        pause
        scene protaPillaBorrachaAMed_2 with dissolve
        play sound "audio/effects/golpeMesa.ogg"
        "¡¡Pum!!"
        scene protaPillaBorrachaAMed_3 with dissolve
        pause
        scene protaPillaBorrachaAMed_4 with dissolve
        protaPensa "¿Que ha sido ese ruido?"

        menu:
            "Investigar":
                call eventEricaBorrachaPilladaInvestiga from _call_eventEricaBorrachaPilladaInvestiga



    return

label eventEricaBorrachaPilladaInvestiga():
    scene protaPillaBorrachaAMed_5 with dissolve
    pause
    scene protaPillaBorrachaAMed_6 with dissolve
    pause
    scene protaPillaBorrachaAMed_7 with dissolve
    pause
    scene protaPillaBorrachaAMed_8 with dissolve
    pause
    play sound "audio/effects/clickLuz.ogg"
    scene protaPillaBorrachaAMed_9 with dissolve
    "¡Click!"
    scene protaPillaBorrachaAMed_10 with dissolve
    play sound "audio/effects/sorpres.ogg"
    protaPensa "¡¡Pero si es [nombreMed]!! ¡Se le ven las bragas!"
    scene protaPillaBorrachaAMed_11 with dissolve
    protaPensa "¿Que hace en el suelo? ¿Esta borracha?"
    scene protaPillaBorrachaAMed_12 with dissolve
    pause
    scene protaPillaBorrachaAMed_13 with dissolve
    pause
    scene protaPillaBorrachaAMed_14 with dissolve
    pause
    scene protaPillaBorrachaAMed_15 with dissolve
    pause
    scene protaPillaBorrachaAMed_16 with dissolve
    pause
    scene protaPillaBorrachaAMed_17 with dissolve
    pause
    scene protaPillaBorrachaAMed_18 with dissolve
    hermanMed "¡Ey tonto! que miiiras."
    scene protaPillaBorrachaAMed_19 with dissolve
    hermanMed "Ayudaaame a leeevantarmeee... hip"
    scene protaPillaBorrachaAMed_20 with dissolve
    hermanMed "Todo da vueltas..."
    scene protaPillaBorrachaAMed_21 with dissolve
    protaPensa "¡¡Oh Dios!! no puedo aguantar más, me lo está sirviendo en bandeja."
    scene protaPillaBorrachaAMed_22 with dissolve
    play sound "audio/effects/silencio.ogg"
    prota "SHHH..."
    scene protaPillaBorrachaAMed_23 with dissolve
    prota "Te ayudo, pero no hagas ruido o se despertaran todos."
    menu:
        "Ayudarla":
            scene protaPillaBorrachaAMed_24 with dissolve
            prota "Vamos al salón, para que se te pase el mareo."
            scene protaPillaBorrachaAMed_25 with dissolve
            pause
            scene protaPillaBorrachaAMed_26 with dissolve
            prota "Túmbate en el sofá a ver si te mejoras."
            scene protaPillaBorrachaAMed_27 with dissolve
            pause
            scene protaPillaBorrachaAMed_28 with dissolve
            hermanMed "eeeee si..."
            scene protaPillaBorrachaAMed_29 with dissolve
            pause
            scene protaPillaBorrachaAMed_30 with dissolve
            hermanMed "Tengo sueño..."
            scene protaPillaBorrachaAMed_31 with dissolve
            pause
            scene protaPillaBorrachaAMed_32 with dissolve
            protaPensa "Oh dios mío... mira como esta..."
            scene protaPillaBorrachaAMed_33 with dissolve
            pause
            scene protaPillaBorrachaAMed_34 with dissolve
            pause
            scene protaPillaBorrachaAMed_35 with dissolve
            prota "Estira las piernas para que descanses bien..."

            menu:
                "Cogerle las piernas":
                    scene protaPillaBorrachaAMed_36 with dissolve
                    pause
                    scene protaPillaBorrachaAMed_37 with dissolve
                    pause
                    play sound "audio/effects/roze3.ogg"
                    scene protaPillaBorrachaAMed_38 with dissolve
                    pause
                    scene protaPillaBorrachaAMed_39 with dissolve
                    pause
                    play sound "audio/effects/golpecitoSuelo.ogg"
                    scene protaPillaBorrachaAMed_40 with dissolve
                    pause
                    scene protaPillaBorrachaAMed_41 with dissolve
                    protaPensa "¿Esta dormida? Con lo borracha que esta dudo mucho que se despierte."
                    scene protaPillaBorrachaAMed_42 with dissolve
                    pause
                    scene protaPillaBorrachaAMed_43 with dissolve
                    prota "¿Erica, estas despierta?"
                    scene protaPillaBorrachaAMed_44 with dissolve
                    pause
                    scene protaPillaBorrachaAMed_45 with dissolve
                    play music "audio/music/ChillWave_kevinMacLeod.ogg" fadein 2.0

                    protaPensa "Oh joder! Esta es mi oportunidad..."
                    call eventEricaBorrachaPilladaPechos from _call_eventEricaBorrachaPilladaPechos

    return

label eventEricaBorrachaPilladaPechos:
    menu:
        "Mostrar pecho derecho":
            call eventEricaBorrachaPilladaPechoDerecho from _call_eventEricaBorrachaPilladaPechoDerecho

    return

label eventEricaBorrachaPilladaPechoDerecho():
    scene protaPillaBorrachaAMed_45 with dissolve
    pause
    scene protaPillaBorrachaAMed_46 with dissolve
    pause
    scene protaPillaBorrachaAMed_47 with dissolve
    pause
    scene protaPillaBorrachaAMed_48 with dissolve
    protaPensa "No te despiertes, por favor... "
    scene protaPillaBorrachaAMed_49 with dissolve
    pause
    play sound "audio/effects/rozeRopaFuerte.ogg"
    scene protaPillaBorrachaAMed_50 with dissolve
    pause
    scene protaPillaBorrachaAMed_51 with dissolve
    pause
    scene protaPillaBorrachaAMed_52 with dissolve
    protaPensa "Que tetas más increíbles."
    scene protaPillaBorrachaAMed_53 with dissolve
    protaPensa "Está totalmente dormida."
    scene protaPillaBorrachaAMed_54 with dissolve
    protaPensa "No me puedo creer que le esté tocando los pechos a mi [nombreMed3]. Podría estar toda la noche así."
    scene protaPillaBorrachaAMed_55 with dissolve
    protaPensa "Quiero ver estas tetas bien. Es improbable que se despierte tal y como esta."
    menu:
        "Bajarle la parte superior del vestido":
            play sound "audio/effects/quitarRopa.ogg"
            scene protaPillaBorrachaAMed_56 with dissolve
            protaPensa "Impresionante."
            $ manosearPecho2EricaBorracha = False
            $ manosearPecho3EricaBorracha = False
            $ manosearPechoEricaBorrachaContinuar = False
            call eventEricaBorrachaPilladaPechoDerechojugarConPechos from _call_eventEricaBorrachaPilladaPechoDerechojugarConPechos



    return

label eventEricaBorrachaPilladaPechoDerechojugarConPechos():
    scene protaPillaBorrachaAMed_57 with dissolve
    menu:
        "Manosear pecho":
            call eventEricaBorrachaPilladaPechoDerechojugarPechoDerecho1 from _call_eventEricaBorrachaPilladaPechoDerechojugarPechoDerecho1
        "Manosear pecho 2" if manosearPecho2EricaBorracha == True:
            call eventEricaBorrachaPilladaPechoDerechojugarPechoDerecho2 from _call_eventEricaBorrachaPilladaPechoDerechojugarPechoDerecho2
        "Manosear un poco mas" if manosearPecho3EricaBorracha == True:
            call eventEricaBorrachaPilladaPechoDerechojugarPechoDerecho3 from _call_eventEricaBorrachaPilladaPechoDerechojugarPechoDerecho3
        "Continuar" if manosearPechoEricaBorrachaContinuar == True:
            call manosearPechoEricaBorrachaContinuar from _call_manosearPechoEricaBorrachaContinuar
    return

label eventEricaBorrachaPilladaPechoDerechojugarPechoDerecho1():
    $ numEricaBorrachaJugarPechoDerecho1 = 0
    scene protaPillaBorrachaAMed_58 with dissolve
    pause 0.1
    scene protaPillaBorrachaAMed_59 with dissolve
    pause 0.3
    while numEricaBorrachaJugarPechoDerecho1 < 2:
        scene protaPillaBorrachaAMed_60 with dissolve
        pause 0.4
        play sound "audio/effects/roze1.ogg"
        scene protaPillaBorrachaAMed_61 with dissolve
        pause 0.3
        scene protaPillaBorrachaAMed_62 with dissolve
        pause 0.3
        play sound "audio/effects/roze3.ogg"
        scene protaPillaBorrachaAMed_63 with dissolve
        pause 0.7
        scene protaPillaBorrachaAMed_64 with dissolve
        if numEricaBorrachaJugarPechoDerecho1 == 0:
            protaPensa "Esta dormida pero pone cara de placer.{p=1.5}{nw}"
        if numEricaBorrachaJugarPechoDerecho1 == 1:
            protaPensa "No puedo parar, me la follaría aquí mismo.{p=1.5}{nw}"
        if numEricaBorrachaJugarPechoDerecho1 == 1:
            scene protaPillaBorrachaAMed_62 with dissolve
            pause 0.6
            play sound "audio/effects/roze3.ogg"
            scene protaPillaBorrachaAMed_60 with dissolve
            pause 0.1
            scene protaPillaBorrachaAMed_59 with dissolve
            pause 0.3
        $ numEricaBorrachaJugarPechoDerecho1 = numEricaBorrachaJugarPechoDerecho1 +1
    $ manosearPecho2EricaBorracha = True
    call eventEricaBorrachaPilladaPechoDerechojugarConPechos from _call_eventEricaBorrachaPilladaPechoDerechojugarConPechos_1

    return

label eventEricaBorrachaPilladaPechoDerechojugarPechoDerecho2():
    $ numEricaBorrachaJugarPechoDerecho2 = 0
    scene protaPillaBorrachaAMed_58_ with dissolve
    pause 0.1
    scene protaPillaBorrachaAMed_59_ with dissolve
    pause 0.3
    scene protaPillaBorrachaAMed_60_ with dissolve
    pause 0.4

    while numEricaBorrachaJugarPechoDerecho2 < 2:
        scene protaPillaBorrachaAMed_61_ with dissolve
        pause 0.3
        play sound "audio/effects/roze3.ogg"
        scene protaPillaBorrachaAMed_62_ with dissolve
        pause 0.3
        scene protaPillaBorrachaAMed_63_ with dissolve
        pause 0.6
        play sound "audio/effects/orgasm2.ogg"
        scene protaPillaBorrachaAMed_64_ with dissolve
        if numEricaBorrachaJugarPechoDerecho2 == 0:
            protaPensa "Me encanta apretarlos {p=1.5}{nw}"
            play sound "audio/effects/roze3.ogg"
        if numEricaBorrachaJugarPechoDerecho2 == 1:
            protaPensa "Mira como se le ponen los pezones de hinchados. {p=1.5}{nw}"
        if numEricaBorrachaJugarPechoDerecho2 == 1:
            scene protaPillaBorrachaAMed_62_ with dissolve
            pause 0.7
            play sound "audio/effects/roze1.ogg"
            scene protaPillaBorrachaAMed_60_ with dissolve
            pause 0.4
            scene protaPillaBorrachaAMed_59_ with dissolve
            pause 0.3
        $ numEricaBorrachaJugarPechoDerecho2 = numEricaBorrachaJugarPechoDerecho2 +1
    $ manosearPecho3EricaBorracha = True
    call eventEricaBorrachaPilladaPechoDerechojugarConPechos from _call_eventEricaBorrachaPilladaPechoDerechojugarConPechos_2

    return

label eventEricaBorrachaPilladaPechoDerechojugarPechoDerecho3():
    #$ numEricaBorrachaJugarPechoDerecho3 = 0
    #while numEricaBorrachaJugarPechoDerecho3 < 2:
    show protaPillaBorrachaAMed_PREMIUM00
    pause 0.5
    "Que tetas tan redondas y grandes y estos pezones. Increíble...{p=1.5}{nw}"
    pause 0.5
    "Me encantan y parece que lo disfruta.{p=1.5}{nw}"
    pause
        #$ numEricaBorrachaJugarPechoDerecho3 = numEricaBorrachaJugarPechoDerecho3 +1
    $ manosearPechoEricaBorrachaContinuar = True
    call eventEricaBorrachaPilladaPechoDerechojugarConPechos from _call_eventEricaBorrachaPilladaPechoDerechojugarConPechos_3
    return

label manosearPechoEricaBorrachaContinuar():
    scene protaPillaBorrachaAMed_65 with dissolve
    pause
    scene protaPillaBorrachaAMed_66 with dissolve
    pause 0.02
    scene protaPillaBorrachaAMed_67 with dissolve
    pause
    scene protaPillaBorrachaAMed_68 with dissolve
    protaPensa "Tiene unos pechos perfectos."
    scene protaPillaBorrachaAMed_69 with dissolve
    protaPensa "Se le han hinchado los pezones, seguro que está en su mejor sueño y lo ha disfrutado."
    scene protaPillaBorrachaAMed_70 with dissolve
    protaPensa "¿Me muero de ganas de verle el coño, como lo tendrá? estará mojada? es mi gran oportunidad..."
    scene protaPillaBorrachaAMed_71 with dissolve
    protaPensa "Le pondré la parte de arriba del vestido, por si acaso..."
    scene protaPillaBorrachaAMed_72 with dissolve
    play sound "audio/effects/quitarRopa.ogg"
    protaPensa "No puedo aguantar más..."
    scene protaPillaBorrachaAMed_73 with dissolve
    pause
    scene protaPillaBorrachaAMed_74 with dissolve
    pause
    scene protaPillaBorrachaAMed_75 with dissolve
    pause
    scene protaPillaBorrachaAMed_76 with dissolve
    pause
    scene protaPillaBorrachaAMed_77 with dissolve
    pause
    scene protaPillaBorrachaAMed_78 with dissolve
    protaPensa "Así, no te despiertes."
    play sound "audio/effects/rozeRopa2.ogg"
    scene protaPillaBorrachaAMed_79 with dissolve
    protaPensa "Ábrete de piernas..."
    play sound "audio/effects/rozeRopaFuerte.ogg"
    scene protaPillaBorrachaAMed_80 with dissolve
    pause
    scene protaPillaBorrachaAMed_81 with dissolve
    pause
    play sound "audio/effects/massagePie1.ogg"
    scene protaPillaBorrachaAMed_82 with dissolve
    protaPensa "Vamos a ver que escondes debajo de estas braguitas."
    scene protaPillaBorrachaAMed_83 with dissolve
    pause
    play sound "audio/effects/orgasm5.ogg"
    scene protaPillaBorrachaAMed_84 with dissolve
    pause
    scene protaPillaBorrachaAMed_85 with dissolve
    protaPensa "Ya puedo verle los labios del coño. Es como si quisiera mostrarse."
    scene protaPillaBorrachaAMed_86 with dissolve
    pause
    scene protaPillaBorrachaAMed_87 with dissolve
    protaPensa "Así... tranquila."
    scene protaPillaBorrachaAMed_88 with dissolve
    pause
    play sound "audio/effects/massagePie2.ogg"
    scene protaPillaBorrachaAMed_89 with dissolve
    protaPensa "A ver este coñito..."
    play sound "audio/effects/massagePie1.ogg"
    scene protaPillaBorrachaAMed_90 with dissolve
    pause
    if ntrGame == True:
        stop music
        play sound "audio/effects/discorayado.ogg"
        scene protaPillaBorrachaAMed_91 with dissolve
        protaPensa "Argh!!! ¿Qué es esto?"
        scene protaPillaBorrachaAMed_92 with dissolve
        protaPensa "¡Esto es semen que puto asco!"
        scene protaPillaBorrachaAMed_93 with dissolve
        protaPensa "¡Sera zorra! ¡Se la han follado y tiene el coño que le esta goteando el semen!"
        scene protaPillaBorrachaAMed_94 with dissolve
        pause
        scene protaPillaBorrachaAMed_95 with dissolve
        pause
        scene protaPillaBorrachaAMed_96 with dissolve
        pause
        scene protaPillaBorrachaAMed_97 with dissolve
        protaPensa "¡Joder, se está despertando!"
        scene protaPillaBorrachaAMed_98 with dissolve
        hermanMed "¿Que hago aquí?"
        scene protaPillaBorrachaAMed_99 with dissolve
        prota "Estabas mareada y te tumbaste aquí para descansar."
        scene protaPillaBorrachaAMed_100 with dissolve
        hermanMed "Me duele la cabeza."
        scene protaPillaBorrachaAMed_101 with dissolve
        hermanMed "Me voy a dormir..."
        play sound "audio/effects/tacones.ogg"
        scene protaPillaBorrachaAMed_102 with dissolve
        pause
        scene protaPillaBorrachaAMed_103 with dissolve
        protaPensa "Estoy seguro de que alguien se ha follado a mi [nombreMed]. ¿Quién habrá sido?"
        scene protaPillaBorrachaAMed_104 with dissolve
    else:
        scene protaPillaBorrachaAMed_96 with dissolve
        pause
        scene protaPillaBorrachaAMed_97 with dissolve
        stop music
        play sound "audio/effects/discorayado.ogg"
        protaPensa "¡Joder, se está despertando!"
        scene protaPillaBorrachaAMed_93_ with dissolve
        pause
        scene protaPillaBorrachaAMed_94_ with dissolve
        pause
        scene protaPillaBorrachaAMed_96 with dissolve
        pause
        scene protaPillaBorrachaAMed_98 with dissolve
        hermanMed "¿Que hago aquí?"
        scene protaPillaBorrachaAMed_99 with dissolve
        prota "Estabas mareada y te tumbaste aquí para descansar."
        scene protaPillaBorrachaAMed_100 with dissolve
        hermanMed "Me duele la cabeza."
        scene protaPillaBorrachaAMed_101 with dissolve
        hermanMed "Me voy a dormir..."
        play sound "audio/effects/tacones.ogg"
        scene protaPillaBorrachaAMed_102 with dissolve
        pause
        scene protaPillaBorrachaAMed_103 with dissolve
        protaPensa "Me ha faltado muy poco..."
        scene protaPillaBorrachaAMed_104 with dissolve

    protaPensa "Me quedado con las ganas de jugar con su coñito, pero sus tetas han sido impresionantes."
    scene protaPillaBorrachaAMed_105 with dissolve
    protaPensa "Quiero más..."

    scene fondoNegro with dissolve
    "Unas horas mas tarde..."
    scene protaPillaBorrachaAMed_106 with dissolve
    hermanMed "¡Despierta!"
    scene protaPillaBorrachaAMed_107 with dissolve
    pause
    scene protaPillaBorrachaAMed_108 with dissolve
    protaPensa "Pensé que era un sueño..."
    scene protaPillaBorrachaAMed_109 with dissolve
    hermanMed "¿Qué paso ayer? Recuerdo que te vi y nada más..."
    scene protaPillaBorrachaAMed_110 with dissolve
    play music "audio/music/Marty_Gots_a_Plan.ogg" fadein 2.0
    prota "AAaaaammmmmm...."
    scene protaPillaBorrachaAMed_111 with dissolve
    prota "Te encontré tirada en el suelo, estabas más que borracha. ¿Qué hiciste ayer? ¿Te lo pasaste bien?"
    scene protaPillaBorrachaAMed_112 with dissolve
    hermanMed "¡Cállate!"
    scene protaPillaBorrachaAMed_113 with dissolve
    hermanMed "¡Ni se te ocurra decirle nada a [nombreMad3]!"
    scene protaPillaBorrachaAMed_114 with dissolve
    pause


    return


label ericaBorrachaEscenaReproductor():
    call deshabilitaObjetosPersonas
    scene protaPillaBorrachaAMed_1
    pause
    scene protaPillaBorrachaAMed_2 with dissolve
    play sound "audio/effects/golpeMesa.ogg"
    "¡¡Pum!!"
    scene protaPillaBorrachaAMed_3 with dissolve
    pause
    scene protaPillaBorrachaAMed_4 with dissolve
    protaPensa "¿Que ha sido ese ruido?"

    menu:
        "Investigar":
            call eventEricaBorrachaPilladaInvestiga

    show screen escenasErica("Erica")

    return
