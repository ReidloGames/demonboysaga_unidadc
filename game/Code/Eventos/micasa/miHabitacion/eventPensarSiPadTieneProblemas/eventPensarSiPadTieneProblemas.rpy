label eventPensarSiPadTieneProblemas():
    if eventPensarSiPadTieneProblemas == False:
        hide screen protaEnCama
        $ eventPensarSiPadTieneProblemas = True
        scene protaPensativo_1
        #protaPensa "Me pregunto qué habrá sido esa llamada que recibió [situPad2] por la mañana."
        protaPensa "Me pregunto cuál fue esa llamada que [situPad2] recibió anteriormente."
        scene protaPensativo_2 with dissolve
        protaPensa "Tenía una cara de preocupación y se le veía muy pensativo. No sé si se habrá metido en algún lio, aunque por otro lado que yo sepa nunca ha pasado nada extraño."
        scene protaPensativo_1 with dissolve
        protaPensa "A parte de eso, [nombreMad3] estaba muy contenta sobre lo bien que le iban las cosas a [situPad2]. Hay algo que no acaba de encajar."
        protaPensa "Igual le estoy dando demasiadas vueltas y todo va como la seda."
        scene protaPensativo_3 with dissolve
        "Dormir..."
        $ puedesIrHabPeq = False
        $ puedesIrHabMed = False
        $ puedesIrHabMay = False

    return
