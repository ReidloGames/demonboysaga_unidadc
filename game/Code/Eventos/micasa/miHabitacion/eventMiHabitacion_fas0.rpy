label eventMiHabitacion_fas0:
    if room == "mihabitacion":
        #LUNES
        #if numDia == 0:
        #    if tiempoDia == 0:
                #scene pasillo1
                #e "ÑEE Es mediodia y miercoles. Prueba de evento"
            #mañana
            #mediodia
            #tarde
            #noche

        #MARTES
        if numDia == 1:
            #$ renpy.sound.play("audio/effects/alarmReloj.ogg", loop = True,relative_volume=0.3)
            #amanecer
            if tiempoDia == 0:
                if demonBoy:
                    call eventMadRecogeRopa() from _call_eventMadRecogeRopa
                else:
                    $ adelantarHora = False
                    $ puedesDesplazarteLibreCasa = False
                    $ puedoIrADormir = False
                    call eventPensarSiPadTieneProblemas from _call_eventPensarSiPadTieneProblemas


        #PROTA pensar si pad tiene problemas en el trabajo
        #if numDia == 0 or numDia == 2 or numDia == 3 or numDia == 4 or numDia == 5 or numDia == 6:
            #if tiempoDia == 0:
            #    if madCocina3raInteracLunes_visit1 == True:
            #        call eventPensarSiPadTieneProblemas from _call_eventPensarSiPadTieneProblemas

        #Comunicado de Pad de que se va
        if numDia == 0 or numDia == 2 or numDia == 3 or numDia == 4 or numDia == 5 or numDia == 6:
            if tiempoDia == 0:
                if protaHablaConPadrDinero == True:
                    call eventSuenyoConMad() from _call_eventSuenyoConMad

        #BuscaTrabajo
        if numDia == 0 or numDia == 1 or numDia == 2 or numDia == 3 or numDia == 4 or numDia == 5 or numDia == 6:
            if tiempoDia == 0 or tiempoDia == 1 or tiempoDia == 2 or tiempoDia == 3:
                if eventInformarHipotecaBanco == True:
                    call eventEncuentraTrabajo from _call_eventEncuentraTrabajo
        #PiensaNocheCursoMandin
        if numDia == 0 or numDia == 2 or numDia == 3 or numDia == 4 or numDia == 5 or numDia == 6:
            if tiempoDia == 0:
                if informarNuevoTrabajoMad == True:
                    call eventPensaCursoMad from _call_eventPensaCursoMad

        #EventErikaPilladaBorrachaSola
        if numDia == 5 and numErikaSeVaDeFiesta >=2 and numEricaSeVaDeFiestaActivarBorracha < numDiaTotal:
            if tiempoDia == 0:
                call eventEricaBorrachaPillada from _call_eventEricaBorrachaPillada

        #Kara despues de mamada quiere mear
        #eventMadRecogeRopaHabProta_visit
        if tiempoDia == 0:
            if mamadaBocaKaraSpyNoche == True:
                call eventMearDespuesDeMamadaKara

    return
