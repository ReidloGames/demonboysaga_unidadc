label eventSuenyoConMad():
    if eventSuenyoConMad == False:
        hide screen protaEnCama
        $ eventSuenyoConMad = True
        $ puedoVerTv = False
        $ puedoIrADormir = False
        $ adelantarHora = False
        $ puedesIrAGaleria = False
        $ puedesIrHabPeq = False
        $ puedesIrHabMed = False
        $ puedesIrHabMay = False

        scene protaSuenyoConMad_0
        pause
        scene protaSuenyoConMad_1 with dissolve
        stop music fadeout 2.0
        protaPensa "¿Que pasara a partir de ahora?"
        protaPensa "La situación es preocupante."
        scene protaSuenyoConMad_2 with dissolve
        pause
        scene protaSuenyoConMad_3 with dissolve
        protaPensa "Mejor me voy a dormir, tampoco puedo solucionar nada calentándome la cabeza."
        scene protaSuenyoConMad_4 with dissolve
        pause
        scene protaSuenyoConMad_5 with dissolve
        pause
        scene protaSuenyoConMad_6 with dissolve
        pause
        play music "audio/music/One_Sly_Move.ogg" fadein 3.0
        scene protaSuenyoConMad_7 with dissolve
        mad "¡Me has hecho daño!"
        scene protaSuenyoConMad_8 with dissolve
        mad "Como me has podido hacer esto..."
        scene protaSuenyoConMad_9 with dissolve
        pause
        scene protaSuenyoConMad_10 with dissolve
        pause
        scene protaSuenyoConMad_11 with dissolve
        stop music fadeout 2.0
        pause

        scene protaSuenyoConMad_12 with dissolve
        mad "¿Lo quieres?"
        scene protaSuenyoConMad_13 with dissolve
        play music "audio/music/ChillWave_kevinMacLeod.ogg" fadein 2.0
        pause
        scene protaSuenyoConMad_14 with dissolve
        mad "Cógelo..."
        scene protaSuenyoConMad_15 with dissolve
        mad "¿Lo quieres?"
        play sound "audio/effects/roze4.ogg"
        scene protaSuenyoConMad_16 with dissolve
        pause
        play sound "audio/effects/roze1.ogg"
        scene protaSuenyoConMad_15 with dissolve
        pause
        scene protaSuenyoConMad_16 with dissolve
        play sound "audio/effects/roze4.ogg"
        mad "Cógelo..."
        scene protaSuenyoConMad_17 with dissolve
        pause
        scene protaSuenyoConMad_18 with dissolve
        prota "Lo quiero."
        scene protaSuenyoConMad_19 with dissolve
        pause
        scene protaSuenyoConMad_20 with dissolve
        mad "Ven aquí..."
        scene protaSuenyoConMad_21 with dissolve
        pause
        scene protaSuenyoConMad_22 with dissolve
        mad "¿Qué quieres hacerme?"
        scene protaSuenyoConMad_23 with dissolve
        pause
        scene protaSuenyoConMad_24 with dissolve
        prota "Yo te cuidare."
        prota "Te hare de todo."
        scene protaSuenyoConMad_25 with dissolve

        pause
        play sound "audio/effects/dedoConyo.ogg"
        scene protaSuenyoConMad_26 with dissolve
        pause
        scene protaSuenyoConMad_27 with dissolve
        pause
        scene protaSuenyoConMad_28 with dissolve
        pause
        scene protaSuenyoConMad_29 with dissolve
        prota "Al fin eres mía."
        prota "Te deseaba tanto..."
        scene protaSuenyoConMad_30 with dissolve
        prota "Fuera bragas."
        scene protaSuenyoConMad_31 with dissolve

        pause
        scene protaSuenyoConMad_32 with dissolve
        play sound "audio/effects/discorayado.ogg"
        stop music
        prota "¿Qué?"
        scene protaSuenyoConMad_33 with dissolve
        protaPensa "¿Dónde está?"
        scene protaSuenyoConMad_34 with dissolve
        pause
        scene protaSuenyoConMad_35 with dissolve
        prota "mmmmmmm"
        scene protaSuenyoConMad_36 with dissolve
        prota "¿Dónde estás?"
        scene protaSuenyoConMad_37 with dissolve
        hermanPeq "Despierta dormilón."
        scene protaSuenyoConMad_38 with dissolve
        pause
        scene protaSuenyoConMad_39 with dissolve
        pause
        scene protaSuenyoConMad_40 with dissolve
        hermanPeq "Hay reunión familiar en el comedor. Vamos, levántate..."
        scene protaSuenyoConMad_41 with dissolve
        protaPensa "¡¡Oh dios!! Era un sueño."
        scene protaSuenyoConMad_42 with dissolve
        protaPensa "Todo ha sido un maldito sueño."
        scene protaSuenyoConMad_43 with dissolve
        protaPensa "Parecía tan real..."
        scene protaSuenyoConMad_44 with dissolve
        protaPensa "Como he podido tener esa clase de sueño con [nombreMad3]?"
        protaPensa "Y lo he disfrutado..."
        scene protaSuenyoConMad_45 with dissolve
        pause
        scene protaSuenyoConMad_46 with dissolve
        protaPensa "Tengo que darme prisa, me están esperando y seguro que es importante después de lo que paso ayer."




    return

#REPRODUCTOR
label suenyoConMadEscenaReproductor():
    call deshabilitaObjetosPersonas
    stop music
    hide screen protaEnCama
    scene protaSuenyoConMad_0
    pause
    scene protaSuenyoConMad_1 with dissolve
    stop music fadeout 2.0
    protaPensa "¿Que pasara a partir de ahora?"
    protaPensa "La situación es preocupante."
    scene protaSuenyoConMad_2 with dissolve
    pause
    scene protaSuenyoConMad_3 with dissolve
    protaPensa "Mejor me voy a dormir, tampoco puedo solucionar nada calentándome la cabeza."
    scene protaSuenyoConMad_4 with dissolve
    pause
    scene protaSuenyoConMad_5 with dissolve
    pause
    scene protaSuenyoConMad_6 with dissolve
    pause
    play music "audio/music/One_Sly_Move.ogg" fadein 3.0
    scene protaSuenyoConMad_7 with dissolve
    mad "¡Me has hecho daño!"
    scene protaSuenyoConMad_8 with dissolve
    mad "Como me has podido hacer esto..."
    scene protaSuenyoConMad_9 with dissolve
    pause
    scene protaSuenyoConMad_10 with dissolve
    pause
    scene protaSuenyoConMad_11 with dissolve
    stop music fadeout 2.0
    pause

    scene protaSuenyoConMad_12 with dissolve
    mad "¿Lo quieres?"
    scene protaSuenyoConMad_13 with dissolve
    play music "audio/music/ChillWave_kevinMacLeod.ogg" fadein 2.0
    pause
    scene protaSuenyoConMad_14 with dissolve
    mad "Cógelo..."
    scene protaSuenyoConMad_15 with dissolve
    mad "¿Lo quieres?"
    play sound "audio/effects/roze4.ogg"
    scene protaSuenyoConMad_16 with dissolve
    pause
    play sound "audio/effects/roze1.ogg"
    scene protaSuenyoConMad_15 with dissolve
    pause
    scene protaSuenyoConMad_16 with dissolve
    play sound "audio/effects/roze4.ogg"
    mad "Cógelo..."
    scene protaSuenyoConMad_17 with dissolve
    pause
    scene protaSuenyoConMad_18 with dissolve
    prota "Lo quiero."
    scene protaSuenyoConMad_19 with dissolve
    pause
    scene protaSuenyoConMad_20 with dissolve
    mad "Ven aquí..."
    scene protaSuenyoConMad_21 with dissolve
    pause
    scene protaSuenyoConMad_22 with dissolve
    mad "¿Qué quieres hacerme?"
    scene protaSuenyoConMad_23 with dissolve
    pause
    scene protaSuenyoConMad_24 with dissolve
    prota "Yo te cuidare."
    prota "Te hare de todo."
    scene protaSuenyoConMad_25 with dissolve

    pause
    play sound "audio/effects/dedoConyo.ogg"
    scene protaSuenyoConMad_26 with dissolve
    pause
    scene protaSuenyoConMad_27 with dissolve
    pause
    scene protaSuenyoConMad_28 with dissolve
    pause
    scene protaSuenyoConMad_29 with dissolve
    prota "Al fin eres mía."
    prota "Te deseaba tanto..."
    scene protaSuenyoConMad_30 with dissolve
    prota "Fuera bragas."
    scene protaSuenyoConMad_31 with dissolve

    pause
    scene protaSuenyoConMad_32 with dissolve
    play sound "audio/effects/discorayado.ogg"
    stop music
    prota "¿Qué?"
    scene protaSuenyoConMad_33 with dissolve
    protaPensa "¿Dónde está?"
    scene protaSuenyoConMad_34 with dissolve
    pause
    scene protaSuenyoConMad_35 with dissolve
    prota "mmmmmmm"
    scene protaSuenyoConMad_36 with dissolve
    prota "¿Dónde estás?"
    scene protaSuenyoConMad_37 with dissolve
    hermanPeq "Despierta dormilón."
    scene protaSuenyoConMad_38 with dissolve
    pause
    scene protaSuenyoConMad_39 with dissolve
    pause
    scene protaSuenyoConMad_40 with dissolve
    hermanPeq "Hay reunión familiar en el comedor. Vamos, levántate..."
    scene protaSuenyoConMad_41 with dissolve
    protaPensa "¡¡Oh dios!! Era un sueño."
    scene protaSuenyoConMad_42 with dissolve
    protaPensa "Todo ha sido un maldito sueño."
    scene protaSuenyoConMad_43 with dissolve
    protaPensa "Parecía tan real..."
    scene protaSuenyoConMad_44 with dissolve
    protaPensa "Como he podido tener esa clase de sueño con [nombreMad3]?"
    protaPensa "Y lo he disfrutado..."
    scene protaSuenyoConMad_45 with dissolve
    pause
    scene protaSuenyoConMad_46 with dissolve
    protaPensa "Tengo que darme prisa, me están esperando y seguro que es importante después de lo que paso ayer."


    show screen escenasSofia("Sofia")
    return
