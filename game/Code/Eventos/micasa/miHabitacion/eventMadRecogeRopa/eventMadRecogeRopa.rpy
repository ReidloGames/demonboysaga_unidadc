label eventMadRecogeRopa():
    if eventMadRecogeRopaHabProta_visit == False:
        $ eventMadRecogeRopaHabProta_visit = True
        hide screen protaEnCama
        stop music
        #pause
        #scene parque with fade
        play sound "audio/effects/open_door.ogg"

        scene madEntroHabRopaSucia1_ with dissolve
        pause
        scene madEntroHabRopaSucia2_ with dissolve
        pause
        scene madEntroHabRopaSucia2_1 with dissolve
        pause
        scene madEntroHabRopaSucia5 with dissolve
        pause
        scene madEntroHabRopaSucia3 with dissolve
        pause
        scene madEntroHabRopaSucia4 with dissolve
        pause
        scene madEntroHabRopaSucia6 with dissolve
        #pause
        call accionesMadRecogeRopa() from _call_accionesMadRecogeRopa

        scene madEntroHabRopaSucia8 with dissolve
        pause
        scene madEntroHabRopaSucia9 with dissolve
        pause
        scene madEntroHabRopaSucia10 with dissolve
        pause
        play sound "audio/effects/open_door.ogg"
        scene madEntroHabRopaSucia11 with dissolve

    return



label accionesMadRecogeRopa():
    play music "audio/music/Sneaky_Snitch.ogg" fadein 5.0
    menu:
        "Acercarte por detras" if haVenidoEthan1 == True:
            call accionDetrasMadRecogeRopa() from _call_accionDetrasMadRecogeRopa

        "Nada":
            scene madEntroHabRopaSucia7 with dissolve
            pause
    return



label accionDetrasMadRecogeRopa:
    $ numVecesChocqueCuloMadRecogeRopa = numVecesChocqueCuloMadRecogeRopa +1
    if numVecesChocqueCuloMadRecogeRopa == 1:
        scene protaChoqueCuloMadRecogeRopa_0 with dissolve
        pause
        scene protaChoqueCuloMadRecogeRopa_1 with dissolve
        pause
        scene protaChoqueCuloMadRecogeRopa_2 with dissolve
        pause
        scene protaChoqueCuloMadRecogeRopa_3 with dissolve
        pause
        scene protaChoqueCuloMadRecogeRopa_4 with dissolve
        pause
        play sound "audio/effects/sorpresa3.ogg"
        scene protaChoqueCuloMadRecogeRopa_5 with dissolve
        pause
        scene protaChoqueCuloMadRecogeRopa_6 with dissolve
        prota "Ups, hemos chocado."
        prota "Solo quería darte los buenos días [nombreMad3]."
        scene protaChoqueCuloMadRecogeRopa_7 with dissolve
        mad "Oh! no te vi, me asustaste. Buenos días."

    elif numVecesChocqueCuloMadRecogeRopa > 1:
        scene protaChoqueCuloMadRecogeRopa_3 with dissolve
        pause
        scene protaChoqueCuloMadRecogeRopa_4 with dissolve
        pause
        play sound "audio/effects/sorpresa3.ogg"
        scene protaChoqueCuloMadRecogeRopa_5 with dissolve
        pause
        scene protaChoqueCuloMadRecogeRopa_6 with dissolve
        prota "Ups, hemos chocado.Buenos días [nombreMad3]."
        scene protaChoqueCuloMadRecogeRopa_8 with dissolve
        mad "Tienes que ir con mas cuidado. Buenos días."
    return
