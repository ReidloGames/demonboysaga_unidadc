label eventPensaCursoMad():
    play music "audio/music/sneaky-adventure-by-kevin-macleod.ogg" fadein 2.0
    if eventPensaCursoMad == False:
        hide screen protaEnCama
        $ eventPensaCursoMad = True
        scene eventPensandoCursoConMad_0
        protaPensa "El curso de Mandin me da buenas vibraciones."
        scene eventPensandoCursoConMad_1 with dissolve
        protaPensa "Y [nombreMad3] ya me ha dicho que me ayudara."
        scene eventPensandoCursoConMad_2 with dissolve
        protaPensa "Es una gran oportunidad, pero tengo que ir con cuidado o me descubrira las intenciones."
        scene eventPensandoCursoConMad_3 with dissolve
        protaPensa "Aún no sé cómo se ha podido creer semejante trola, pero he conseguido que aceptara..."
        scene eventPensandoCursoConMad_4 with dissolve
        protaPensa "Supongo que ahora, toda ayuda económica sera bienvenida."
        scene eventPensandoCursoConMad_5 with dissolve
        pause
        scene fondoNegro with dissolve
        pause

    return
