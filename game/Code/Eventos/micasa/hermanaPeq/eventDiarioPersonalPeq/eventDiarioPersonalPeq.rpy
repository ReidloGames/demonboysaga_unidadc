label eventDiarioPersonalPeq():
    if eventDiarioPersonalPeq == False:
        $ eventDiarioPersonalPeq = True
        ##hide screen protaEnCama
        $ numDiaDiarioEventPeq = numDiaTotal + 1
        play music "audio/music/holiday-weasel-by-kevin-macleod.ogg" fadein 2.0
        scene peqEscribeDiarioPersonal_0
        protaPensa "¿Qué hace Dana?"
        scene peqEscribeDiarioPersonal_1 with dissolve
        pause
        scene peqEscribeDiarioPersonal_2 with dissolve
        pause
        scene peqEscribeDiarioPersonal_3 with dissolve
        protaPensa "¿Es su diario personal?"
        scene peqEscribeDiarioPersonal_4 with dissolve
        pause
        scene peqEscribeDiarioPersonal_5 with dissolve
        protaPensa "Necesito ver ese diario..."

        call nextTiempoDay(0)
        call nextZona("pasillo2")
        scene pasillo2
        pause
        call esconderHud()




    return
