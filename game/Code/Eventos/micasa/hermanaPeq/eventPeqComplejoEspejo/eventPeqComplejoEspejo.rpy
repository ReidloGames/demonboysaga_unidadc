label eventPeqComplejoEspejo():
    if luchaAlmohadasPeq == True and eventPeqComplejoEspejo == False:
        if numDiaDiarioEventPeq < numDiaTotal:

            $ eventPeqComplejoEspejo = True
            play music "audio/music/holiday-weasel-by-kevin-macleod.ogg" fadein 3.0
            scene eventPeqSuHabitacionMirandoEspejo_0
            protaPensa "Otra vez..."
            scene eventPeqSuHabitacionMirandoEspejo_1 with dissolve
            protaPensa "Ahora en ropa interior..."
            scene eventPeqSuHabitacionMirandoEspejo_2 with dissolve
            pause
            scene eventPeqSuHabitacionMirandoEspejo_3 with dissolve
            pause
            scene eventPeqSuHabitacionMirandoEspejo_4 with dissolve
            pause
            scene eventPeqSuHabitacionMirandoEspejo_5 with dissolve
            protaPensa "Parece que Dana esta insatisfecha, ¿Qué le pasa?"
            scene eventPeqSuHabitacionMirandoEspejo_6 with dissolve
            pause
            scene eventPeqSuHabitacionMirandoEspejo_7 with dissolve
            pause
            scene eventPeqSuHabitacionMirandoEspejo_8 with dissolve
            pause
            scene eventPeqSuHabitacionMirandoEspejo_9 with dissolve
            pause
            scene eventPeqSuHabitacionMirandoEspejo_10 with dissolve
            pause 0.2
            scene eventPeqSuHabitacionMirandoEspejo_11 with dissolve
            pause
            scene eventPeqSuHabitacionMirandoEspejo_12 with dissolve
            play sound "audio/effects/asentir.ogg"
            protaPensa "Oh dios, que buena que esta."
            protaPensa "Me voy antes de que me vea."

    return
