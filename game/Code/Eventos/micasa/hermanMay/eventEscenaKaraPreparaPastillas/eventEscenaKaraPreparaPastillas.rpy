label eventEscenaKaraPreparaPastillas():
    if eventEscenaKaraPreparaPastillas == False:
        $ eventEscenaKaraPreparaPastillas = True
        $ prepararPastillasKara = True
        play music "audio/music/Scheming_Weasel_faster.ogg" fadein 3.0
        scene valerianasEnVasoKara_0
        pause
        scene valerianasEnVasoKara_1 with dissolve
        protaPensa "Unas pastillitas para dormir bien..."
        play sound "audio/effects/agua1.ogg"
        scene valerianasEnVasoKara_2 with dissolve
        pause
        scene valerianasEnVasoKara_3 with dissolve
        pause
        play sound "audio/effects/abrePuertaSorpresa.ogg"
        scene valerianasEnVasoKara_4 with dissolve
        stop music

        pause
        scene valerianasEnVasoKara_5 with dissolve
        hermanMay "¿Qué haces en mi habitación?"
        scene valerianasEnVasoKara_6 with dissolve
        prota "Oh yo..."
        prota "Perdona solo entre para saludarte, pero no había nadie."
        prota "Ya me iba..."
        scene valerianasEnVasoKara_7 with dissolve
        hermanMay "Estas muy raro últimamente..."
        play sound "audio/effects/puertaEntrada.ogg"



    return
