label eventosMiCasaFas0():
    #banyo
    call eventBanyo_fas0() from _call_eventBanyo_fas0
    #cocina
    call eventCocina_fas0() from _call_eventCocina_fas0
    #comedor
    call eventComedor_fas0() from _call_eventComedor_fas0
    #entradaCasa
    call eventEntradaCasa_fas0() from _call_eventEntradaCasa_fas0
    #galeriaGym
    call eventGaleriaGym_fas0() from _call_eventGaleriaGym_fas0
    #garage
    call eventGarage_fas0() from _call_eventGarage_fas0
    #habPadres
    call eventHabPadres_fas0() from _call_eventHabPadres_fas0
    #hermanMay
    call eventHermanMay_fas0() from _call_eventHermanMay_fas0
    #hermanMed
    call eventHermanMed_fas0() from _call_eventHermanMed_fas0
    #hermanPeq
    call eventHermanPeq_fas0() from _call_eventHermanPeq_fas0
    #jardinPis
    call eventJardinPis_fas0() from _call_eventJardinPis_fas0
    #miHabitacion
    call eventMiHabitacion_fas0() from _call_eventMiHabitacion_fas0
    #pasillo1
    call eventPasillo1_fas0() from _call_eventPasillo1_fas0
    #pasillo2
    call eventPasillo2_fas0() from _call_eventPasillo2_fas0
    #salon
    call eventSalon_fas0() from _call_eventSalon_fas0

    #eventoAccesosComodinTuto
    call eventoAccesosComodinTuto from _call_eventoAccesosComodinTuto
    return
