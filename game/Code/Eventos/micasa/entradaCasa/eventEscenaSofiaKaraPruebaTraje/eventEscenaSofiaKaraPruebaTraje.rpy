label eventEscenaSofiaKaraPruebaTraje():
    if eventEscenaSofiaKaraPruebaTraje == False:
        $ eventEscenaSofiaKaraPruebaTraje = True
        scene eventMadSofiaHablanTrajeGym_0 with dissolve
        pause
        scene eventMadSofiaHablanTrajeGym_1 with dissolve
        protaPensa "Oigo voces en el comedor de [nombreMad3] y Kara."
        stop music fadeout 2.0

        menu:
            "Investigar":
                scene eventMadSofiaHablanTrajeGym_2 with dissolve
                play music "audio/music/spy-glass-by-kevin-macleod-from-filmmusic-io.ogg" fadein 2.0
                hermanMay "Venga vamos a tu habitación y pruébatelo."

                scene eventMadSofiaHablanTrajeGym_3 with dissolve

                menu:
                    "Espiar":
                        play sound "audio/effects/abrirPuertaSigilo.ogg"
                        scene eventMadSofiaHablanTrajeGym_4 with dissolve
                        pause
                        scene eventMadSofiaHablanTrajeGym_5 with dissolve
                        mad "No deberías haberte gastado dinero. Soy consciente que acabas de abrir el negocio y que papa se fue sin ayudarte con los pagos como te prometió."
                        scene eventMadSofiaHablanTrajeGym_6 with dissolve
                        hermanMay "Mejor no hablemos de eso. A parte tenías los pantalones de yoga un poco gastados y no me importa hacerle un regalo a mi madre."
                        scene eventMadSofiaHablanTrajeGym_7 with dissolve
                        pause
                        scene eventMadSofiaHablanTrajeGym_8 with dissolve
                        pause
                        scene eventMadSofiaHablanTrajeGym_9 with dissolve
                        hermanMay "Mama, sigues teniendo un cuerpo impresionante. Tienes los pechos de una adolescente y eso que Max siempre esta enganchado a tu teta. Si quisieras podrías volver a modelar."
                        scene eventMadSofiaHablanTrajeGym_10 with dissolve
                        mad "No digas tonterías."
                        scene eventMadSofiaHablanTrajeGym_11 with dissolve
                        hermanMay "Déjamelas tocar."
                        scene eventMadSofiaHablanTrajeGym_12 with dissolve
                        pause
                        scene eventMadSofiaHablanTrajeGym_13 with dissolve
                        hermanMay "Como se nota que están hinchadas y llenas de leche. Max estará contento."
                        scene eventMadSofiaHablanTrajeGym_14 with dissolve
                        protaPensa "..."
                        scene eventMadSofiaHablanTrajeGym_15 with dissolve
                        mad "Venga deja de hacer tonterías y tráeme el equipo de gimnasia."
                        scene eventMadSofiaHablanTrajeGym_16 with dissolve
                        hermanMay "Que suerte tienes mama de tener este cuerpo. Ahora que no está papa tendrás que ir con cuidado. Hahaha"
                        scene eventMadSofiaHablanTrajeGym_17 with dissolve
                        hermanMay "Se quedo la puerta medio abierta."
                        scene eventMadSofiaHablanTrajeGym_18 with dissolve
                        play sound "audio/effects/sorpresaHombre.ogg"
                        pause
                        scene eventMadSofiaHablanTrajeGym_19 with dissolve
                        play sound "audio/effects/cajon.ogg"
                        protaPensa "Uff! Por Poco..."
                        "FIN VERSION 0.5 SOFIA"
                        call nextZona("comedor")


    return
