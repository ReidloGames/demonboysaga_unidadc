label eventSofiaLavanderia:
    scene sofiaLavanderiaPart1_0
    protaPensa "Oh dios en que posición esta."
    play music "audio/music/hiddenAgenda_by_kevin_macleod.ogg" fadein 2.0

    scene sofiaLavanderiaPart1_1 with dissolve
    $ renpy.pause(2.0)

    scene sofiaLavanderiaPart1_2 with dissolve
    $ renpy.pause(2.0)


    scene sofiaLavanderiaPart1_3 with dissolve

    mad "[nombreProta2], estas ahí?"
    stop music
    play sound "audio/effects/sorpresaHombre.ogg"
    scene sofiaLavanderiaPart1_4 with dissolve
    prota "Oh si, acabo de entrar. ¿Qué pasa?"
    #Part1
    if not haVistoMadLavanderia:
        scene sofiaLavanderiaPart1_5 with dissolve
        mad "No te había visto. Tenemos un problema con la lavadora. Parece que no hace buen contacto."
        scene sofiaLavanderiaPart1_6 with dissolve
        prota "Te ayudo a resolver el problema?"
        scene sofiaLavanderiaPart1_7 with dissolve
        mad "Gracias por la ayuda, pero ya lo mirare en otro momento."
        mad "Aprovecha y recoge tu cesta."
        $ haVistoMadLavanderia = True
    #Part2
    else:
        scene sofiaLavanderiaPart2_5 with dissolve
        mad "La lavadora está dando problemas otra vez."
        scene sofiaLavanderiaPart2_6 with dissolve
        prota "Le doy al botón a ver si se enciende."
        scene sofiaLavanderiaPart2_7 with dissolve
        prota "El botón esta encendido, sigue intentándolo..."
        scene sofiaLavanderiaPart2_8 with dissolve
        prota "Ves moviendo el cable..."
        play music "audio/music/hiddenAgenda_by_kevin_macleod.ogg" fadein 1.0
        menu:
            "levantarle la falda":
                scene sofiaLavanderiaPart2_9 with dissolve
                $ renpy.pause(2.0)
                scene sofiaLavanderiaPart2_10 with dissolve
                mad "¿Y ahora funciona?"
                prota "Prueba a mover el cable otra vez..."
                scene sofiaLavanderiaPart2_11 with dissolve
                $ renpy.pause(2.0)

                scene sofiaLavanderiaPart2_12 with dissolve
                $ renpy.pause(2.0)

                scene sofiaLavanderiaPart2_13 with dissolve
                $ renpy.pause(2.0)
                scene sofiaLavanderiaPart2_14 with dissolve
                prota "Estoy en un sueño. El coño de [nombreMad3]."
                scene sofiaLavanderiaPart2_15 with dissolve
                mad "Parece que esto no es."
                scene sofiaLavanderiaPart2_16 with dissolve
                $ renpy.pause(2.0)
                scene sofiaLavanderiaPart2_17 with dissolve
                prota "Es posible que este obstruido el desagüe y no se encienda por seguridad."
                scene sofiaLavanderiaPart2_18 with dissolve
                mad "Arghh solo faltaba que se empiecen a estropear las cosas."
                scene sofiaLavanderiaPart2_19 with dissolve
                prota "Tranquila intentaremos arreglarlo."
    call nextTiempoDay(0) from _call_nextTiempoDay_37
    return
