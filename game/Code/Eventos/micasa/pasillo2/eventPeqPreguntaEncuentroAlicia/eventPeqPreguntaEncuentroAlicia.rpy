label eventPeqPreguntaEncuentroAlicia():
    if chicaPeq == True and llegoTardeClaseAlicia == True and eventPeqPreguntaEncuentroAlicia == False and chicaMad == True:
        $ eventPeqPreguntaEncuentroAlicia = True
        scene peqPasilloEncuentroPasilloAlicia_0
        play sound "audio/effects/tacones.ogg"
        pause 0.5
        stop music fadeout 3.0
        scene peqPasilloEncuentroPasilloAlicia_1 with dissolve
        hermanPeq "Ey!"
        scene peqPasilloEncuentroPasilloAlicia_2 with dissolve
        stop sound fadeout 3.0
        pause
        scene peqPasilloEncuentroPasilloAlicia_3 with dissolve
        hermanPeq "Ya me contaron que llegaste tarde a clase y que Alicia fue dura contigo."
        play music "audio/music/sneaky-adventure-by-kevin-macleod.ogg" fadein 2.0

        scene peqPasilloEncuentroPasilloAlicia_4 with dissolve
        pause
        scene peqPasilloEncuentroPasilloAlicia_5 with dissolve
        prota "Estoy harto de aguantar a esa perra. Siempre busca algo para joderme y dejarme mal delante de toda la clase."
        scene peqPasilloEncuentroPasilloAlicia_6 with dissolve
        hermanPeq "¿Si sabes que no os lleváis bien porque no llegaste puntual a clase?"
        scene peqPasilloEncuentroPasilloAlicia_7 with dissolve
        prota "Porque me salió un imprevisto en el último momento."
        scene peqPasilloEncuentroPasilloAlicia_8 with dissolve
        prota "Juro que me vengare de esa perra."
        scene peqPasilloEncuentroPasilloAlicia_9 with dissolve
        hermanPeq "hahahahaha"
        scene peqPasilloEncuentroPasilloAlicia_10 with dissolve
        hermanPeq "Has de ser realista ella es tu profesora y está por encima de ti. Es imposible que puedas hacer nada al respecto."
        scene peqPasilloEncuentroPasilloAlicia_11 with dissolve
        hermanPeq "Yo de ti no la haría enfadar, igual se le acaba pasando."
        scene peqPasilloEncuentroPasilloAlicia_12 with dissolve
        prota "Ya veremos, ya sabes que cuando se me mete algo en la cabeza no puedo parar."
        scene peqPasilloEncuentroPasilloAlicia_13 with dissolve
        hermanPeq "Dices eso porque estas enfadado, pero ya se te pasara."
        scene peqPasilloEncuentroPasilloAlicia_14 with dissolve
        hermanPeq "Adiós gruñón"
        scene peqPasilloEncuentroPasilloAlicia_15 with dissolve
        pause
        scene peqPasilloEncuentroPasilloAlicia_16 with dissolve
        hermanPeq "hahahaha"
        protaPensa "Creo que iré a ver un poco de televisión para relajarme."
        call nextTiempoDay(0)
    return
