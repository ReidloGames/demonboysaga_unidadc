label eventPeqSeVaUni():
    if eventPeqSeVaUniPasillo2_visit == False and llegoTardeClaseAlicia == False:
        $ eventPeqSeVaUniPasillo2_visit = True
        stop music
        play sound "audio/effects/closeDoor.ogg"
        scene peqPasilloSaleDeSuHab_0
        call primeraVezPeq from _call_primeraVezPeq
        pause
        scene peqPasilloSaleDeSuHab_Scroll_0 at pandown2
        pause
        scene peqPasilloSaleDeSuHab_0 with dissolve
        pause(0.5)
        scene peqPasilloSaleDeSuHab_1 with dissolve
        pause
        scene peqPasilloSaleDeSuHab_2 with dissolve
        pause(0.4)
        scene peqPasilloSaleDeSuHab_3 with dissolve
        pause
        scene peqPasilloSaleDeSuHab_4 with dissolve
        hermanPeq "Eyy, dormilon!!"
        scene peqPasilloSaleDeSuHab_5 with dissolve
        pause
        scene peqPasilloSaleDeSuHab_8 with dissolve
        hermanPeq "Me voy a la Universidad si no te das prisa llegaras tarde!"
        scene peqPasilloSaleDeSuHab_3 with dissolve
        pause
        scene peqPasilloSaleDeSuHab_7 with dissolve
        pause
        scene peqPasilloSaleDeSuHab_9 with dissolve
        pause(0.2)
        scene peqPasilloSaleDeSuHab_12 with dissolve
        prota "Si, ahora me iba a vestir"
        scene peqPasilloSaleDeSuHab_11 with dissolve
        hermanPeq "Ya sabes que la profesora Alicia no te tiene mucho cariño"
        scene peqPasilloSaleDeSuHab_10 with dissolve
        hermanPeq "Nos vemos luego."
        scene peqPasilloSaleDeSuHab_11 with dissolve
        pause
        play sound "audio/effects/tacones.ogg"
        scene peqPasilloSaleDeSuHab_14 with dissolve
        pause
        scene peqPasilloSaleDeSuHab_15 with dissolve
        stop sound fadeout 3.0
        pause
        scene peqPasilloSaleDeSuHab_16 with dissolve
        pause
        scene peqPasilloSaleDeSuHab_17 with dissolve
        pause
        scene peqPasilloSaleDeSuHab_13 with dissolve
        stop sound
        pause
    return
