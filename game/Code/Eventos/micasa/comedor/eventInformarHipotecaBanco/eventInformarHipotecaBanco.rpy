label eventInformarHipotecaBanco():
    if numDiaVienenLosDelBanco < numDiaTotal and eventInformarHipotecaBanco == False:
        $ eventInformarHipotecaBanco = True
        play sound "audio/effects/timbre.ogg"
        scene informarHipotecaCasaEvent_0
        "DING DONG"
        scene informarHipotecaCasaEvent_1 with dissolve
        pause
        scene informarHipotecaCasaEvent_2 with dissolve
        pause
        stop music fadeout 2.0
        scene informarHipotecaCasaEvent_3 with dissolve
        pause
        play sound "audio/effects/abrirNevera.ogg"
        scene informarHipotecaCasaEvent_4 with dissolve
        pause
        scene informarHipotecaCasaEvent_5 with dissolve
        pause
        scene informarHipotecaCasaEvent_6 with dissolve
        pause
        scene informarHipotecaCasaEvent_7 with dissolve
        play music "audio/music/bossa-antigua-by-kevin-macleod.ogg" fadein 2.0
        mad "Hola, buenos días."
        scene informarHipotecaCasaEvent_8 with dissolve
        daren "Hola señora, somos trabajadores del banco National Bank, yo soy Daren y mi compañero se llama Adriano. Venimos a informarle de la situación de esta casa."
        scene informarHipotecaCasaEvent_9 with dissolve
        daren "Como sabrá esta casa ha pasado a ser propiedad del banco. A partir de ahora tendrá que cumplir con el nuevo contrato y pagar la hipoteca. Aquí está la firma de un tal John ... antiguo propietario"
        daren "Le enviaremos toda la documentación por burofax, esto solo ha sido una visita de cortesía"
        scene informarHipotecaCasaEvent_10 with dissolve
        daren "Que tenga un buen día."
        scene informarHipotecaCasaEvent_11 with dissolve
        pause
        scene informarHipotecaCasaEvent_12 with dissolve
        pause
        play sound "audio/effects/puertaEntrada.ogg"
        scene informarHipotecaCasaEvent_13 with dissolve
        pause
        stop music fadeout 2.0
        scene informarHipotecaCasaEvent_14 with dissolve
        madPensa "Esto es la peor pesadilla que se pueda tener."
        scene informarHipotecaCasaEvent_15 with dissolve
        prota "¿Que te pasa [nombreMad3]?"
        scene informarHipotecaCasaEvent_16 with dissolve
        mad "Son trabajadores del banco. Han venido a recordar que habrá que pagar la deuda de [nombrePad3]..."
        scene informarHipotecaCasaEvent_17 with dissolve
        pause
        scene informarHipotecaCasaEvent_18 with dissolve
        mad "Con lo que gane [nombrePad3], será imposible hacer frente a tantos gastos."
        scene informarHipotecaCasaEvent_19 with dissolve
        play music "audio/music/Marty_Gots_a_Plan.ogg" fadein 2.0
        prota "[nombreMad3], haremos lo que me dijiste."
        scene informarHipotecaCasaEvent_20 with dissolve
        prota "Juntos superaremos esto."
        scene informarHipotecaCasaEvent_21 with dissolve
        pause

        menu:
            "Abrazar a [nombreMad3]":
                call abrazoMadDelante from _call_abrazoMadDelante
            "No abrazar":
                call noAbrazar from _call_noAbrazar

        call nextTiempoDay(0) from _call_nextTiempoDay_28
    return

label abrazoMadDelante():
    play sound "audio/effects/abrazo1.ogg"
    scene informarHipotecaCasaEvent_22 with dissolve
    pause
    scene informarHipotecaCasaEvent_23 with dissolve
    prota "No voy a dejarte sola y que lo pases mal."
    scene informarHipotecaCasaEvent_24 with dissolve
    pause
    scene informarHipotecaCasaEvent_25 with dissolve
    play sound "audio/effects/roze1.ogg"
    pause 0.1
    scene informarHipotecaCasaEvent_26 with dissolve
    pause 0.2
    scene informarHipotecaCasaEvent_27 with dissolve
    play sound "audio/effects/roze3.ogg"
    pause 0.1
    scene informarHipotecaCasaEvent_28 with dissolve
    pause 0.1
    play sound "audio/effects/roze1.ogg"
    scene informarHipotecaCasaEvent_29 with dissolve
    pause 0.1
    scene informarHipotecaCasaEvent_30 with dissolve
    pause
    scene informarHipotecaCasaEvent_31 with dissolve
    pause
    scene informarHipotecaCasaEvent_32 with dissolve
    prota "No quiero verte mal. Seguro que podremos superar esto si nos ayudamos."
    scene informarHipotecaCasaEvent_33 with dissolve
    play sound "audio/effects/asentir.ogg"
    mad "Eh... sí, supongo..."
    call continuarInformarHipoteca from _call_continuarInformarHipoteca
    return

label noAbrazar():
    scene informarHipotecaCasaEvent_40 with dissolve
    pause
    scene informarHipotecaCasaEvent_41 with dissolve
    pause
    scene informarHipotecaCasaEvent_42 with dissolve
    menu:
        "Abrazar por detras":
            play sound "audio/effects/abrazoSusto.ogg"
            scene informarHipotecaCasaEvent_43 with hpunch
            pause
            scene informarHipotecaCasaEvent_44 with dissolve
            prota "No voy a dejarte sola y que lo pases mal."
            scene informarHipotecaCasaEvent_45 with dissolve
            pause
            #play sound "audio/effects/roze1.ogg"
            scene informarHipotecaCasaEvent_46 with dissolve
            pause 0.1
            play sound "audio/effects/roze1.ogg"
            scene informarHipotecaCasaEvent_47 with dissolve
            pause 0.1
            scene informarHipotecaCasaEvent_48 with dissolve
            pause 0.1
            play sound "audio/effects/roze3.ogg"
            scene informarHipotecaCasaEvent_49 with dissolve
            pause 0.1
            scene informarHipotecaCasaEvent_50 with dissolve
            pause 0.1
            play sound "audio/effects/roze1.ogg"
            scene informarHipotecaCasaEvent_51 with dissolve
            pause 0.1
            scene informarHipotecaCasaEvent_52 with dissolve
            pause
            scene informarHipotecaCasaEvent_53 with dissolve
            prota "Todo saldrá bien."
            scene informarHipotecaCasaEvent_54 with dissolve
            pause
            scene informarHipotecaCasaEvent_55 with dissolve
            play sound "audio/effects/asentir.ogg"
            mad "Eh... sí, supongo que tienes razón..."
            #play sound "audio/effects/golpecitoSuelo.ogg"
            scene informarHipotecaCasaEvent_56 with dissolve
            pause
            call continuarInformarHipoteca2 from _call_continuarInformarHipoteca2
    return


label continuarInformarHipoteca():
    scene informarHipotecaCasaEvent_34 with dissolve
    prota "Voy a buscar un empleo. Quiero ayudar en todo lo que pueda..."
    scene informarHipotecaCasaEvent_35 with dissolve
    mad "Gracias [nombreProta2], es muy amable por tu parte, pero tu ya tienes muchas cosas como la Universidad."
    scene informarHipotecaCasaEvent_34 with dissolve
    prota "No te preocupes, encontrare el modo de colaborar y que podamos salir de esta situación. "
    scene informarHipotecaCasaEvent_36 with dissolve
    mad "Gracias por preocuparte tanto y querer ayudar."
    scene informarHipotecaCasaEvent_37 with dissolve
    prota "De nada."
    play sound "audio/effects/tacones.ogg"
    scene informarHipotecaCasaEvent_38 with dissolve
    pause
    scene informarHipotecaCasaEvent_39 with dissolve
    pause
    scene informarHipotecaCasaEvent_60 with dissolve
    protaPensa "Iré a mi habitación a pensar y buscar un empleo por internet."

    return

label continuarInformarHipoteca2():
    scene informarHipotecaCasaEvent_56 with dissolve
    prota "Voy a buscar un empleo. Quiero ayudar en todo lo que pueda..."
    scene informarHipotecaCasaEvent_57 with dissolve
    mad "Gracias [nombreProta2], es muy amable por tu parte, pero tú ya tienes muchas cosas como la Universidad."
    scene informarHipotecaCasaEvent_58 with dissolve
    prota "No te preocupes encontrare el modo de colaborar y que podamos salir de esta situación."
    scene informarHipotecaCasaEvent_59 with dissolve
    mad "Gracias por querer ayudar."
    scene informarHipotecaCasaEvent_58 with dissolve
    prota "De nada."
    play sound "audio/effects/tacones.ogg"
    scene informarHipotecaCasaEvent_38 with dissolve
    pause
    scene informarHipotecaCasaEvent_39 with dissolve
    pause
    scene informarHipotecaCasaEvent_60 with dissolve
    protaPensa "Iré a mi habitación a pensar y buscar un empleo por internet."

    return
