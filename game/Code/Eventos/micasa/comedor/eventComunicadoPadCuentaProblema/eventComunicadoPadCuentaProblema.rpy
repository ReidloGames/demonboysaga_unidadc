label eventComunicadoPadCuentaProblema():
    if eventComunicadoPadCuentaProblema == False:
        $ eventComunicadoPadCuentaProblema = True
        scene padComunicaSeVa_0
        pause
        scene padComunicaSeVa_1 with dissolve
        pause
        scene padComunicaSeVa_2 with dissolve
        pause
        stop music fadeout 2.0
        scene padComunicaSeVa_3 with dissolve
        mad "Hola cielo, ¿has podido dormir bien?"
        pause
        scene padComunicaSeVa_4 with dissolve
        mad "Parece que hayas visto un fantasma."
        mad "Tranquilo, es normal con lo que viste ayer..."
        scene padComunicaSeVa_5 with dissolve
        mad "Juntos lo superaremos."
        scene padComunicaSeVa_6 with dissolve
        pause
        scene fondoNegro with dissolve
        "Se sientan..."
        scene padComunicaSeVa_7 with dissolve
        pause
        scene padComunicaSeVa_8 with dissolve
        pad "Siento teneros que avisar así."
        play music "audio/music/sneaky-adventure-by-kevin-macleod.ogg" fadein 3.0
        scene padComunicaSeVa_9 with dissolve
        pad "Pero os tengo que comunicar algo importante."
        scene padComunicaSeVa_10 with dissolve
        pad "[nombreMad3] y [nombreProta2] ya saben de la situación y ahora os tenía que informar a vosotras."
        scene padComunicaSeVa_11 with dissolve
        pad "Esto os sorprenderá, es difícil para mí y es un golpe duro de asimilar, pero ahora tenemos que estar unidos para superar este problema."
        scene padComunicaSeVa_12 with dissolve
        hermanMay "¿Pero dinos que está pasando?"
        scene padComunicaSeVa_13 with dissolve
        hermanMed "Dinos ya, lo que está pasando."
        scene padComunicaSeVa_14 with dissolve
        pause
        scene padComunicaSeVa_15 with dissolve
        pad "Hace unos días perdí el trabajo por una irresponsabilidad de la que estoy muy arrepentido."
        scene padComunicaSeVa_16 with dissolve
        pause
        scene padComunicaSeVa_17 with dissolve
        pad "A parte de que he tenido que hipotecar la casa."
        scene padComunicaSeVa_18 with dissolve
        play sound "audio/effects/grito.ogg"
        hermanMay "Oh dios santo, esto tiene que ser una broma."
        scene padComunicaSeVa_19 with dissolve
        hermanMed "Pero ¿cómo es posible?"
        scene padComunicaSeVa_20 with dissolve
        pad "Cometí un fallo y ahora tendré que pagarlo."
        scene padComunicaSeVa_21 with dissolve
        hermanMed "¡¡¡Lo vamos a pagar todos!!!"
        scene padComunicaSeVa_22 with dissolve
        pause
        scene padComunicaSeVa_23 with dissolve
        pad "Lo siento. Es cierto, mi fallo también lo pagareis vosotros y lo siento mucho. A partir de ahora ya no podréis depender del dinero como antes."
        scene padComunicaSeVa_24 with dissolve
        pad "[nombreMay], sé que te prometí que pagaría la inversión en tu negocio, pero no podre cumplir esa promesa."
        scene padComunicaSeVa_25 with dissolve
        hermanMay "Pero si acabo de pedir un crédito, no podre hacerme cargo yo sola. No tengo beneficios suficientes, acabo de abrir el negocio y solo tengo deudas por la inversión."
        scene padComunicaSeVa_26 with dissolve
        pad "Lo siento de verdad, soy consciente de que os he fallado."
        scene padComunicaSeVa_27 with dissolve
        pad "[nombreMed], a ti desgraciadamente también te afectara. No podre asumir otro año de tu master. Por suerte ya lo tienes pagado, pero tendrás que esforzarte más y aprobarlo."
        scene padComunicaSeVa_28 with dissolve
        hermanMed "..."
        scene padComunicaSeVa_29 with dissolve
        pad "Lo siento Sofia, ya que por mi culpa tendrás que volver a trabajar. Por mucho que quiera es imposible encontrar un trabajo como el que tenía y que nos permita vivir como antes."
        scene padComunicaSeVa_30 with dissolve
        mad "..."
        scene padComunicaSeVa_31 with dissolve
        pad "He hablado con mi primo George y me ha ofrecido un trabajo de conductor de camión. No es mucho, pero ayudara..."
        pad "Es todo muy precipitado lo sé. Pero no hay más tiempo y me tengo que marchar ahora."
        scene padComunicaSeVa_32 with dissolve
        pause
        scene padComunicaSeVa_33 with dissolve
        pause
        scene padComunicaSeVa_34 with dissolve
        hermanMay "Como me has podido hacer esto. No sé qué clase de tontería habrás hecho, pero me has decepcionado y creo que no solo a mí."
        scene padComunicaSeVa_35 with dissolve
        pad "Lo siento, espero que podáis perdonarme."
        scene minutosMasTarde with dissolve
        "Unos minutos mas tarde..."
        scene padComunicaSeVa_37 with dissolve
        pad "Ya es hora de irme con mi primo."
        scene padComunicaSeVa_38 with dissolve
        pause
        scene padComunicaSeVa_39 with dissolve
        pad "[nombreProta2], ahora serás el hombre de la casa. Tendrás que cuidar de ellas."
        pad "Estoy seguro de que lo harás muy bien."
        pad "Bueno a excepción de Max, que aún va con pañales."
        scene padComunicaSeVa_40 with dissolve
        pause
        scene padComunicaSeVa_41 with dissolve
        pad "Me voy, os iré informando."
        scene padComunicaSeVa_42 with dissolve
        pad "Lo siento querida."
        scene padComunicaSeVa_43 with dissolve
        pause
        scene padComunicaSeVa_44 with dissolve
        pause
        scene padComunicaSeVa_45 with dissolve
        play sound "audio/effects/closeDoor.ogg"
        #Sonido de puerta cerrar
        protaPensa "[nombrePad3] ya se fue, y [nombreMad3] está muy enfadada. Ni le ha dirigido la palabra, pero es normal después de lo que hizo."
        scene padComunicaSeVa_46 with dissolve
        protaPensa "Necesito ir al baño para refrescarme un poco y aclarar la mente."


        scene padComunicaSeVa_47 with dissolve
        play sound "audio/effects/open_door.ogg"
        pause
        scene padComunicaSeVa_48 with dissolve
        pause
        scene padComunicaSeVa_49 with dissolve
        pause
        scene padComunicaSeVa_50 with dissolve
        protaPensa "Ahora las cosas van a cambiar mucho..."
        scene padComunicaSeVa_39 with dissolve
        pad "Ahora seras el hombre de la casa..."
        scene padComunicaSeVa_51 with dissolve
        protaPensa "Es cierto, voy a ser el hombre de la casa..."
        stop music
        scene padComunicaSeVa_52 with dissolve
        protaPensa "..."
        scene protaSuenyoConMad_14 with dissolve
        pause
        scene protaSuenyoConMad_14 with dissolve
        protaPensa "Otra vez ese sueño..."
        scene protaSuenyoConMad_14 with dissolve
        mad "Lo quieres?"
        scene protaSuenyoConMad_15 with dissolve
        protaPensa "No puedo dejar de pensar en eso..."
        scene padComunicaSeVa_39 with dissolve
        pad "Ahora seras el hombre de la casa..."
        play music "audio/music/Disco_Medusae-kevin_macleod.ogg" fadein 3.0

        scene mayHabMayEnBragas1raMartes_5 with dissolve
        pause
        scene MedJardinTomaSol2daSabado_3 with dissolve
        pause
        scene peqPasilloSaleDeSuHab_16 with dissolve
        pause
        scene padComunicaSeVa_54 with dissolve
        protaPensa "Oh dios no puedo parar..."
        play music "audio/music/fearless.ogg" fadein 5.0
        scene padComunicaSeVa_55 with dissolve
        play sound "audio/effects/boing.ogg"
        protaPensa "..."
        scene fondoNegro with dissolve
        prota "Y como os iba diciendo..."
        scene padComunicaSeVa_56 with dissolve
        prota "Así es como empieza una nueva etapa."
        scene padComunicaSeVa_57 with dissolve
        pause
        stop music
        show finIntro_000
        pause
        hide finIntro_000
        scene fondoNegro with dissolve
        #"FIN VERSIÓN 0.2"
        #stop music
        $ puedoVerTv = True
        $ puedoIrADormir = True
        $ puedesIrAGaleria = True
        $ puedesIrHabPeq = True
        $ puedesIrHabMed = True
        $ puedesIrHabMay = True
        $ adelantarHora = True
        $ padInvisible = True
        $ mirarCambioDeHora = True
        $ puedoVolverMiHabitacion = True
        $ puedesDesplazarteLibreCasa = True
        $ madCocinaDarMamar3raInteracJueves_visit1 = False
        $ demonBoy = True
        call nextTiempoDay(0) from _call_nextTiempoDay_16
    return
