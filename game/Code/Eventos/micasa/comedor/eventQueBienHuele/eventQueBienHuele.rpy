label eventQueBienHuele():
    if eventQueBienHuele == False:
        $ eventQueBienHuele = True
        scene protaHueleBien_0
        pause
        scene protaHueleBien_1 with dissolve
        protaPensa "Que bien huele"
        scene protaHueleBien_2 with dissolve
        protaPensa "Creo que viene de la cocina"

    return
