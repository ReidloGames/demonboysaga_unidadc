label eventErikaSeVaDeFiesta():
    if numErikaSeVaDeFiesta < 2:
        $ eventErikaSeVaDeFiesta = True
        $ numErikaSeVaDeFiesta = numErikaSeVaDeFiesta +1
        stop music fadeout 2.0
        play sound "audio/effects/tacones.ogg"
        scene medSeVaDeFiestaSalePuerta_0
        pause
        scene medSeVaDeFiestaSalePuerta_1 with dissolve
        pause
        scene medSeVaDeFiestaSalePuerta_2 with dissolve
        pause
        scene medSeVaDeFiestaSalePuerta_3 with dissolve
        pause
        scene medSeVaDeFiestaSalePuerta_4 with dissolve
        stop sound fadeout 2.0
        pause
        if numErikaSeVaDeFiesta == 1:
            scene medSeVaDeFiestaSalePuerta_5 with dissolve
            protaPensa "¿A dónde va?"
            scene medSeVaDeFiestaSalePuerta_6 with dissolve
            protaPensa "¿Se va de fiesta? Menudo vestido más corto lleva..."
            scene medSeVaDeFiestaSalePuerta_7 with dissolve
            "Sigue intentándolo..."

        if numErikaSeVaDeFiesta == 2:
            $ numEricaSeVaDeFiestaActivarBorracha = numDiaTotal +2
            scene medSeVaDeFiestaSalePuerta_5 with dissolve
            pause
            scene medSeVaDeFiestaSalePuerta_6 with dissolve
            pause
            scene medSeVaDeFiestaSalePuerta_7 with dissolve
            protaPensa "Parece que se marcha de fiesta otra vez..."

        call nextTiempoDay(0) from _call_nextTiempoDay_36

    return
