label eventPadVuelveTrabjoTriste():
    if not eventPadVuelveTrabajoTriste and chicaMed and demonBoy == False:
        $ eventPadVuelveTrabajoTriste = True
        scene eventPadVuelveTrabajoTriste_0
        pause 1.0
        play sound "audio/effects/open_door.ogg"

        scene eventPadVuelveTrabajoTriste_1
        pause 1.0
        protaPensa "Oh, es [situPad2]. Ha llegado pronto de su reunión de negocios."
        scene eventPadVuelveTrabajoTriste_2
        pause 0.5
        scene eventPadVuelveTrabajoTriste_3
        pause 0.5
        scene eventPadVuelveTrabajoTriste_4
        pause 0.5
        padPensa "Oh Dios, ¿qué demonios hice?"
        scene eventPadVuelveTrabajoTriste_5
        protaPensa "Parece cansado. Habrá sido un día ajetreado."
        scene eventPadVuelveTrabajoTriste_6
        protaPensa "No me ha visto."
        scene eventPadVuelveTrabajoTriste_7
        pause 0.5
        scene eventPadVuelveTrabajoTriste_8
        pause 0.5
        scene eventPadVuelveTrabajoTriste_9
        pause 0.5
        protaPensa "Va directamente a su habitación. Querrá ir a descansar."
        scene eventPadVuelveTrabajoTriste_10
        pause 0.5
        protaPensa "Ahora que está solo es una buena oportunidad para pedirle algo de dinero."
        #call nextTiempoDay(0)
    return
