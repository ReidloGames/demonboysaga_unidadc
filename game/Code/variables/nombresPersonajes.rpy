label nombresPersonajes():
    #$ nombreProta = "Lucas"
    $ nombrePad = "John"
    $ nombreMad = "Sofia"
    $ nombrePeq = "Dana"
    $ nombreMed = "Erica"
    $ nombreMay = "Kara"
    $ nombreProfeLengua = "Alicia"
    $ nombreAbu = "Julia"
    $ nombreTia = ""


    #$ nombreProta2 = nombreProta
    $ nombrePad2 = "John"
    $ nombreMad2 = "Sofia"
    $ nombrePeq2 = "Dana"
    $ nombreMed2 = "Erica"
    $ nombreMay2 = "Kara"
    $ nombreAbu2 = "Julia"



    $ nombrePad3 = "John"
    $ nombreMad3 = "Sofia"
    $ nombrePeq3 = "Dana"
    $ nombreMed3 = "Erica"
    $ nombreMay3 = "Kara"
    $ nombreAbu3 = "Julia"


    $ nombreLucia = "Lucia"
    $ nombreClaire = "Claire"
    $ nombreAlicia = "Alicia"
    $ nombreCarla = "Carla"





    ##RelacionPersonaje
    if lang == "spanish":
        $ narrador = _("Narrador")
        $ pensamiento = _("Pensamiento")

        $ situProta =_("Compañero de piso")
        $ situPad = _("Tutor")
        $ situMad = _("Casera")
        $ situPeq = _("Compañera de Piso")
        $ situMed = _("Compañera de Piso")
        $ situMay = _("Compañera de Piso")
        $ situAbu = _("Amiga de [nombreMad]")
        $ situTia = _("Amiga de [nombreMad]")

        $ situPad2 = _("tutor")
        $ situMad2 = _("casera")
        $ situPeq2 = _("compañera de Piso")
        $ situMed2 = _("compañera de Piso")
        $ situMay2 = _("compañera de Piso")
        $ situAbu2 = _("Amiga de [nombreMad]")
        $ situTia2 = _("Amiga de [nombreMad]")
        $ situLucia = _("Madre de Mike")
        $ situClaire = _("Compañera Universidad")
        $ situAlicia = _("Profesora")
        $ situCarla = _("Jefa Amarzong")

    if lang == "english":
        $ narrador = _("Narrator")
        $ pensamiento = _("Thought")

        $ situProta =_("Roommate")
        $ situPad = _("Tutor")
        $ situMad = _("Landlady")
        $ situPeq = _("Roommate ")
        $ situMed = _("Roommate ")
        $ situMay = _("Roommate ")
        $ situAbu = _("Friend of [nombreMad]")


        $ situPad2 = _("tutor")
        $ situMad2 = _("landlady")
        $ situPeq2 = _("roommate")
        $ situMed2 = _("roommate")
        $ situMay2 = _("roommate")
        $ situAbu2 = _("friend of [nombreMad]")

        $ situLucia = _("Mike's mother")
        $ situClaire = _("University Partner")
        $ situAlicia = _("Teacher")
        $ situCarla = _("Boss Amarzong")


    return
