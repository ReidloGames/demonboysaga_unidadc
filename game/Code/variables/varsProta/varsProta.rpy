label varsProta():
    $ puedoVerTv = True
    $ adelantarHora = True
    $ puedoIrADormir = True
    $ puedesIrAGaleria = True
    $ puedesIrHabPeq = True
    $ puedesIrHabMed = True
    $ puedesIrHabMay = True
    $ irAmearEscuela = False
    $ hasDadoClaseAlicia = False
    $ protaHablaConPadrDinero = False
    $ pensarEnKaraPaja = False
    $ puedesActivarAlarmaColegio = False

    $ tienesQueComprarEquipo = False


    #Objects
    $ tienesAceiteCorporal = False
    $ tienesChocolateDana = False
    $ puedesComprarPrismaticos = False
    $ tienesPrismaticos = False
    $ tengoPastillasDormir = False

    #Mad
    $ madSalonSentarteConMadRevistaMasajePies = False
    $ hablarLibroYogaSofia = False
    #Escuela
    $ noPuedesIrteDeEscuela = False
    #Amarzond
    $ puedesIrAmarzond = False
    $ conoceLouie = False
    $ abrirPuertaAmarzong = False
    $ conocesLouie = False
    $ conocesCarla = False
    $ tienesTrabajoAmarzong = False

    #Biblioteca
    $ puedesIrBiblioteca = False
    $ conocesAdaBiblioteca = False
    $ preguntaLibroYogaBiblioteca = False

    #Tienda Kara
    $ puedesIrTiendaKara = False
    $ tienesEquipoGym = False
    $ escenaCompletaComprarTrajeDeporte = False

    #***********************************
    #Vars Peq
    $ hasHabladoJugarConConsola = False
    $ hasHabladoConPeqEnElColeTrabajosAcabados = False
    $ numVecesGymPreguntaJugar = 0
    $ numContJugarConsola = 0
    $ victoriaConsolaPeq = 0
    $ luchaAlmohadasPeq = False
    $ numDiaDiarioEventPeq = 0


    #Vars Erika
    $ numErikaSeVaDeFiesta = 0
    $ numDiaMedComedor3raInteracMiercoles_visit2 = 0
    $ numEricaSeVaDeFiestaActivarBorracha = 0

    #Vars May Kara
    $ preguntaComoEstaKaraCocinaCafe = False
    $ numDiaTotalKaraHablaCafe = 0
    $ tengoLlaveHabitacionKara = False
    $ hasEntradoDeNocheSuHabitacion = False
    $ prepararPastillasKara = False
    $ numContKaraEntraHabNoche = 0
    $ mamadaBocaKaraSpyNoche = False


    #Vars Mad
    $ numVecesChocqueCuloMadRecogeRopa = 0
    $ numContadorSiQuiereMasajePies = 0
    $ haVenidoEthan1 = False
    $ pensarComoHacerYogaConSofia = False
    $ numDiaTotalKaraCompraEquipGym = 0
    $ haVistoMadLavanderia = False

    #Vars Alicia Profesora
    $ numContadorAliciaVisit1 = 0
    $ numActivaAlarmaUniversidadAlicia = 0
    $ puedesEspiarAliciaCodigoMovil = False
    $ numContadorCodigoMovilAlicia = 0
    $ tengoCodigoPinAlicia = False

    #Vars Lucia
    $ numContadorMikeHacerDeberesJuntos = 0
        #Prismaticos
    $ espiarLuciaPrismaticosEscenas1 = False
    $ espiarLuciaPrismaticosEscenas2 = False
    $ espiarLuciaPrismaticosEscenas3 = False



    #Cursos:
    $ leccion1Massage = False

    #CambiandoseRopaChicasCasa***********
    #May
    $ cambiandoseMayEscena1 = False
    $ cambiandoseMayEscena2 = False
    $ cambiandoseMayEscena3 = False

    #Med
    $ cambiandoseMedEscena1 = False
    $ cambiandoseMedEscena2 = False
    $ cambiandoseMedEscena3 = False
    #Peq
    $ cambiandosePeqEscena1 = False
    $ cambiandosePeqEscena2 = False
    $ cambiandosePeqEscena3 = False
    #Mad
    $ cambiandoseMadEscena1 = False
    $ cambiandoseMadEscena2 = False
    $ cambiandoseMadEscena3 = False


    #EVENTS
    $ eventCambiateDeRopa = False
    $ eventQueBienHuele = False
    $ eventPensarSiPadTieneProblemas = False
    $ eventPeqPreguntaEncuentroAlicia = False
    $ eventSuenyoConMad = False
    $ eventComunicadoPadCuentaProblema = False
    $ eventoAccesosComodinTuto = False
    $ eventInformarHipotecaBanco = False
    $ eventEncuentraTrabajo = False
    $ eventPensaCursoMad = False
    $ eventKaraEricaSeVanDeCompras = False
    $ eventSofiaLavanderia = False


    #Objects
    $ tengoBolis = False
    $ puedesCogerBici = False
    $ tengoBicicleta = False

    return
