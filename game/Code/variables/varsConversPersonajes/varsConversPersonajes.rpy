label varsConversPersonajes():
    #Family
    call conversFamily() from _call_conversFamily
    #Mad
    call conversMad() from _call_conversMad
    call eventsMad() from _call_eventsMad
    call actionsRealizadasMad() from _call_actionsRealizadasMad
    #Pad
    call conversPad() from _call_conversPad
    call eventsPad() from _call_eventsPad

    #Peq
    call conversPeq() from _call_conversPeq
    call eventsPeq() from _call_eventsPeq
    call actionsRealizadasPeq() from _call_actionsRealizadasPeq

    #Med
    call conversMed() from _call_conversMed
    call eventsMed from _call_eventsMed

    #May
    call conversMay() from _call_conversMay
    call eventsMay() from _call_eventsMay

    #################################################################
    #UNIVERSIDAD:

    #Mike(Amigo)
    call eventsMike() from _call_eventsMike

    #Lucia(Madre de Mike)
    call conversLucia() from _call_conversLucia
    call eventsLucia() from _call_eventsLucia

    #Alicia(Profesora)
    call conversAlicia() from _call_conversAlicia
    call eventsAlicia() from _call_eventsAlicia

    #Claire
    call conversClaire() from _call_conversClaire
    call eventsClaire() from _call_eventsClaire

    return
