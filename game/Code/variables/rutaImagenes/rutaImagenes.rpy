label rutaImagenes():
    call rutaImgOpeningTuto from _call_rutaImgOpeningTuto
    call rutaImgOpening from _call_rutaImgOpening
    call rutaImgTutoAccesosComodin from _call_rutaImgTutoAccesosComodin
    call rutaImgMicasa from _call_rutaImgMicasa
    call rutaImgEscuela from _call_rutaImgEscuela
    call rutaImgTiendacomestibles from _call_rutaImgTiendacomestibles
    call rutaImagenesMapa from _call_rutaImagenesMapa
    call rutaImgAmarzong from _call_rutaImgAmarzong
    call rutaImgBiblioteca
    return


transform pandown1: #Arriba - Abajo
    crop (0, 0, 1280, 1440)
    linear 4 crop (0, 720, 1280, 720)

transform pandown1_0: #Arriba - Abajo
    crop (0, 0, 1280, 1440)
    linear 5 crop (0, 720, 1280, 720)

transform pandown2: #Abajo - Arriba
    crop (0, 720, 1280, 1440)
    linear 4 crop (0, 0, 1280, 1440)
