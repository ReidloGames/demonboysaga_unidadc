
label variables:
    $ zona = "micasa"
    $ room = "mihabitacion"
    $ destino = ""
    $ numDia = 0
    $ tiempoDia = 1
    $ numSemana = 0
    $ numDiaTotal = 0 #
    $ tiempoDiaStr = ""
    $ optionsOpenHud = False
    $ gameRunable = True
    #$ lang = ""
    $ parche = False
    $ demonBoy = False
    $ versionPremium = True
    $ ntrGame = False


    #HUD volver a mi habitacion icono
    $ puedoVolverMiHabitacion = False
    $ puedesDesplazarteLibreCasa = False


    #Persona que ocupa habitacion
    $ localizaMad = ""
    $ localizaMadComodin = ""
    $ localizaPad = ""
    $ localizaPeq = ""
    $ localizaPeqComodin = ""
    $ localizaMed = ""
    $ localizaMedComodin = ""
    $ localizaMay = ""
    $ localizaMayComodin = ""

    $ family4 = ""
    $ family5 = ""
    $ localizaPersonasMiCasaFas = 0

    #Esconder Frame Ayudas
    $ escondeFrameAyudasMad = False
    $ escondeFrameAyudasPad = False
    $ escondeFrameAyudasPeq = False
    $ escondeFrameAyudasMed = False
    $ escondeFrameAyudasMay = False
    $ escondeFrameAyudasLucia = True
    $ escondeFrameAyudasClaire = True
    $ escondeFrameAyudasAlicia = False

    #PersonasVisibles
    $ hermanPeqInvisible = False
    $ hermanMedInvisible = False
    $ hermanMayInvisible = False
    $ madInvisible = False
    $ padInvisible = False

    call rutaImagenes() from _call_rutaImagenes
    call rutaAnimacionesVideo() from _call_rutaAnimacionesVideo
    call nombresPersonajes() from _call_nombresPersonajes

    call mostrarChicasStats() from _call_mostrarChicasStats
    call statsProta() from _call_statsProta
    call statsChicas() from _call_statsChicas
    call mostrarChicasMovilWhats() from _call_mostrarChicasMovilWhats
    call escenasChicasSex() from _call_escenasChicasSex
    call textosSmsWhats() from _call_textosSmsWhats
    call varsDiario() from _call_varsDiario
    call varsProta() from _call_varsProta

    #CasaVars
    call varsMiHabitacion() from _call_varsMiHabitacion
    call varsHabPadres() from _call_varsHabPadres
    call varsHabPeq() from _call_varsHabPeq
    call varsHabMed() from _call_varsHabMed
    call varsHabMay() from _call_varsHabMay

    #EscuelaVars
    call despachoDirectorVars() from _call_despachoDirectorVars
    call pasilloWcVars() from _call_pasilloWcVars


    #ConversacionesPersonajes
    call varsConversPersonajes() from _call_varsConversPersonajes

    ##LimpiarPlatos
    $ puedesLimpiarPlatos = False
    $ platosHoyFregados = False

    ##EntrarBanyo################----
    $ hasEntradoBanyoMad = False
    $ hasEntradoBanyoPad = False
    $ hasEntradoBanyoPeq = False
    $ hasEntradoBanyoMed = False
    $ hasEntradoBanyoMay = False

    ##Entrada habitaciones cuando se cambian de ropa
    $ hasEntradoHabPadrCambiandose = False
    $ hasEntradoHabPeqCambiandose = False
    $ hasEntradoHabMedCambiandose = False
    $ hasEntradoHabMayCambiandose = False

    ##Entrada habitaciones cuando duermen
    $ hasEntradoHabMayNocheDuerme = False

    #ProtaSentadoSofaConMad4taMartes4ta
    $ protaSentadoSofaConMad4taMartes = False


    #Cambiandose de ropa en su habitacion
    $ cambiandoseMad = False
    $ cambiandosePeq = False
    $ cambiandoseMed = False

    #Trabajo
    $ hasTrabajadoHoy = False

    #Prismaticos
    $ hasMiradoPorPrismaticosLucia = False

    return

##Existe objetos en Tienda

label getObjectInventario(name):
    $ resultObject = False
    python:
        for item in player_inventory.itemlist:
            if item.name == name:
                resultObject = True
            else:
                resultObject = False
    return

label getObjectTiendaPa(name):
    $ resultObjectPa = False
    python:
        for item in tiendaConsumibles.itemlist:
            if item.name == name:
                resultObjectPa = True

            #else:
            #    resultObjectPa = False
    return



init python:
    class Inventory(object):
        def __init__(self):
            self.itemlist = []

        def get(self):
            return self.itemlist

        def add(self, x):
            self.itemlist.append(x)

        def sub(self, x):
            self.itemlist.remove(x)

    class Item(object):
        # Create a class list for all Item instances.
        itemlist = []

        def __init__(self, name, nameEsp, nameEng, desc, descEng, cost, type, imagen='', imagenh=''):
            self.name = name
            self.nameEsp = nameEsp
            self.nameEng = nameEng
            self.desc = desc
            self.descEng = descEng
            self.cost = cost
            self.type = type
            self._image ='/images/tienda/' + imagen + '.png'
            self._imageh ='/images/tienda/' + imagenh + '.png'

            # Add the item into class list
            self.itemlist.append([self])


        def buy(self):
            global player_inventory, player_gold
            player_inventory.add([self])
            player_gold -= self.cost

        def sell(self):
            global player_inventory, player_gold
            player_inventory.sub([self])
            player_gold += self.cost


# Create items
define bolis = Item(name="bolis",nameEsp="bolis",nameEng="pens", desc="Mis bolis",descEng="My pens", cost=10, type="object",imagen="bolis",imagenh="bolish" )
define bicicleta = Item(name="bicicleta",nameEsp="bicicleta",nameEng="bicycle", desc="Mi bicicleta",descEng="my bicycle", cost=0, type="object",imagen="bicicleta",imagenh="bicicletah" )
define aceiteCorporal = Item(name="aceiteCorporal",nameEsp="aceite corporal",nameEng="body oil", desc="Aceite corporal para masajes",descEng="Massage body oil", cost=5, type="object",imagen="aceiteCorporal",imagenh="aceiteCorporalh" )
define chocolate = Item(name="chocolate",nameEsp="chocolate",nameEng="chocolate", desc="Dulce muy bueno.",descEng="Very good sweet.", cost=5, type="object",imagen="chocolate",imagenh="chocolateh" )
define prismaticos = Item(name="prismaticos",nameEsp="prismáticos",nameEng="binoculars", desc="Para ver los pájaros...",descEng="To see the birds...", cost=30, type="object",imagen="prismaticos",imagenh="prismaticosh" )
define valerianasDormir = Item(name="valerianasDormir",nameEsp="pastillas para dormir",nameEng="sleeping pills", desc="Valerianas super potentes para dormir",descEng="Super potent valerian for sleep", cost=10, type="object",imagen="valerianasDormir",imagenh="valerianasDormirh" )


#define fresa = Item(name="fresa",nameEsp="fresa",nameEng="Strawberry", desc="fruta", cost=5, type="fruta",imagen="fresa",imagenh="fresah")


default tiendaConsumibles = Inventory()
default tiendaRopa = Inventory()
default player_inventory = Inventory() # Create player inventory

default player_gold = 0 # Add some gold
