screen getContentTiendaComestibles(nom):

    #frame:
    #xalign 0.5 ypos 50
    add "plantitienda" xpos 112 ypos 10
    $ i = 0
    $ xposi = 160
    $ yposi = 90

    hbox:
        xpos 425 ypos 22
        text _("{b}Tienda [nom]{/b}"):
            size 18
    for item in tiendaConsumibles.itemlist:

        hbox:
            xpos xposi ypos yposi
            #spacing 68
            $ xposi += 100
            if i == 6:
                $ i = -1
                $ yposi += 180
                $ xposi = 160

            vbox:
                spacing 5
                if lang == "spanish":
                    text "[item.nameEsp]":
                        xalign 0.5
                        size 9
                if lang == "english":
                    text "[item.nameEng]":
                        xalign 0.5
                        size 9

                #if persistent.lang is "english":
                #    text "[item.nameEsp]":
                #        xalign 0.5
                #        size 13
                imagemap:
                    #xpos 5 ypos 5
                    idle [item._image]
                    hover [item._imageh]
                    #hotspot (0, 0, 70, 70) action If(player_gold >= item.cost, true = Notify ("tienes dinero"),false = Notify("no money"))
                    hotspot (0, 0, 70, 70) action If(player_gold >= item.cost, true = Call ("comprarArticulo2",item),false = Show ("textNoMoney"))
                text "[item.cost]€":
                    xalign 0.5
                    size 12
            $ i +=1


    hbox:
        xpos 800 ypos 24
        text _("[player_gold]€"):
            size 17

    imagebutton:
            xalign 0.5 ypos 615
            idle "exit"
            hover "exith"
            #action [Return (""),Call("mostrarHud")]
            action Call("textosEmpleado")
return

label comprarArticulo2(itemObj):
    #hide screen getContentTienda
    call screen acceptcompra(itemObj)


screen acceptcompra(itemObj):
    add "modcompra" xpos 290 ypos 200
    hbox:
        if lang == "spanish":
            text _("Deseas comprar [itemObj.nameEsp] por el precio de [itemObj.cost]€ ?"):
                xpos 380 ypos 220
                size 20

        if lang == "english":
            text _("Deseas comprar [itemObj.nameEng] por el precio de [itemObj.cost]€ ?"):
                xpos 380 ypos 220
                size 20

        imagebutton:
            xpos 10 ypos 280
            idle "buttoncompra"
            hover "buttoncomprah"
            #action [Call("getContentTienda",itemObj.type),Hide("acceptcompra")]
            action Call("optionCompra","compra",itemObj)
        imagebutton:
            xpos 15 ypos 280
            idle "buttoncompra"
            hover "buttoncomprah"
            action Call("optionCompra","",itemObj)

        text _("Aceptar"):
            xpos -265 ypos 295
            size 18
            color "#000000"

        text _("Cancelar"):
            xpos -190 ypos 295
            size 18
            color "#000000"
return

label optionCompra(opt,itemObj):
    if opt =="compra":
        python:
            tiendaConsumibles.sub(itemObj)
            player_gold -= itemObj.cost
            player_inventory.add(itemObj)
        if itemObj == aceiteCorporal:
            $ tienesAceiteCorporal = True
        if itemObj == chocolate:
            $ tienesChocolateDana = True
        if itemObj == prismaticos:
            $ tienesPrismaticos = True
        if itemObj == valerianasDormir:
            $ tengoPastillasDormir = True



        hombreConsum "Muchas gracias, desea algo mas?"
        call screen getContentTiendaComestibles(_("Comestibles"))
    else:
        call screen getContentTiendaComestibles(_("Comestibles"))

return


label textosEmpleado():
    hombreConsum "Muchas gracias"
    call mostrarHud() from _call_mostrarHud_12
    return

screen textNoMoney():
    frame:
        xpos 910 ypos 14
        padding (10,10,10,10)
        hbox:
            #xpos 925 ypos 100
            text _("No tienes dinero suficiente"):
                size 17
                color "#000000"
            timer 0.7 action Hide('textNoMoney')
return
