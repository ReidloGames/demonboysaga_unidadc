label createItemsTienda:
    #NO USAR ESTA FUNCION PORQUE LOS OBJETOS SE REPETIRAN
    python:

        tiendaConsumibles.add(chocolate)
        #tiendaConsumibles.add(chocolate)
        #tiendaConsumibles.add(fresa)

        #tiendaRopa.add(fresa)
        #tiendaRopa.add(fresa)


return


label showTienda(nameTienda,queTienda):
    #call createItemsTienda  #<--- NO USAR SOLO PRUEBAS
    call esconderHud() from _call_esconderHud_1

    if queTienda == "Comestibles":

        scene tiendaConsumiblesComprar
        hombreConsum "Hola, que desea?"

        if numContadorSiQuiereMasajePies == 1 and tienesAceiteCorporal == False:
            call getObjectTiendaPa("aceiteCorporal")
            if resultObjectPa == False:
                python:
                    tiendaConsumibles.add(aceiteCorporal)

        #ChocolateDana
        if preguntarCocinaComoEstaTemaPad == True and animarHermanPeqSuHabitacion == False and tienesTrabajoAmarzong == True and tienesChocolateDana == False:
            call getObjectTiendaPa("chocolate")
            if resultObjectPa == False:
                python:
                    tiendaConsumibles.add(chocolate)

        #Prismaticos
        if puedesComprarPrismaticos == True and tienesPrismaticos == False:
            call getObjectTiendaPa("prismaticos")
            if resultObjectPa == False:
                python:
                    tiendaConsumibles.add(prismaticos)

        #PastillasDormir
        if hasEntradoDeNocheSuHabitacion == True and tengoPastillasDormir == False:
            call getObjectTiendaPa("valerianasDormir")
            if resultObjectPa == False:
                python:
                    tiendaConsumibles.add(valerianasDormir)


        call screen getContentTiendaComestibles(_(nameTienda))

        return
