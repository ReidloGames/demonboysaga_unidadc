label miniGameClick(name,num):

    ##VERSION MOUSE

    $ cont = 0 #continue variable
    $ arr_keys = ["a", "c", "e", "K_UP", "K_SPACE"] #list of keyboard inputs to be selected from. See https://www.pygame.org/docs/ref/key.html for more keys


    #call qte_setup(1, 0.5, 0.01, renpy.random.choice(arr_keys), renpy.random.randint(1, 9) * 0.1, renpy.random.randint(1, 9) * 0.1) from _call_qte_setup
    call qte_setup(2.5, 2.5, 0.025, renpy.random.choice(arr_keys), renpy.random.randint(1, 9) * 0.1, renpy.random.randint(1, 9) * 0.1) from _call_qte_setup
    # "Function Call" - see label qte_setup for detail on "function"
    # in the above, I randomly select a key from a previously defined set of keys (arr_keys), and randomise the location

    $ counter = 0 #counter variable
    while cont == 1 and counter < num:
        #call qte_setup(0.7, 0.5, 0.01, renpy.random.choice(arr_keys), renpy.random.randint(1, 9) * 0.1, renpy.random.randint(1, 9) * 0.1) from _call_qte_setup_1
        call qte_setup(2.5, 2.5, 0.025, renpy.random.choice(arr_keys), renpy.random.randint(1, 9) * 0.1, renpy.random.randint(1, 9) * 0.1) from _call_qte_setup_1
        $ counter = counter + 1 #increment/increase the counter each time. When it reaches 10, it will exit the loop
        # to repeat the qte events until it is missed

    if name == "esquivarGolpeJackPasilloWc":
        if counter == num: #if we reached 10 rounds without missing
            play sound "audio/effects/winGame.ogg"
            call esquivarGolpeJackPasilloWc from _call_esquivarGolpeJackPasilloWc

        #"5 rounds done"

        else:
            if name == "esquivarGolpeJackPasilloWc":
                call noEsquivarGolpeJackPasilloWc from _call_noEsquivarGolpeJackPasilloWc

            #play sound "sounds/miss.mp3"
            #"{b}GAME OVER{/b}"
    if name == "jugarConsolaPeq":
        if counter == num: #if we reached 10 rounds without missing
            play sound "audio/effects/winGame.ogg"
            call peqJugarConsolaVictoria
        else:
            if name == "jugarConsolaPeq":
                call peqJugarConsolaPerdido

    return



label qte_setup(time_start, time_max, interval, trigger_key, x_align, y_align):

    $ time_start = time_start
    $ time_max = time_max
    $ interval = interval
    $ trigger_key = trigger_key
    $ x_align = x_align
    $ y_align = y_align

    call screen qte_button
    # can change to "call screen qte_button" to switch to button mode

    $ cont = _return
    # 1 if key was hit in time, 0 if key not

    return


screen qte_button:
    #button press qte

    button:
        action Return(0) #miss
        align 0.5, 0.5
        #background "images/transparent.png"
        #to add a click sensor *outside* of button (if player presses outside button area) and return false


    timer interval repeat True action If(time_start > 0.0, true=SetVariable('time_start', time_start - interval*2), false=[Return(0), Hide('qte_button')])
    #see above

    vbox:
        xalign x_align yalign y_align spacing 25

        #button:
        imagebutton:
            action Return(1)
            xalign 0.5
            #xysize 50, 50
            idle "ninja"
            hover "ninja_S"
            #background Animation("#000", 0.5, "#fff", 0.5) #change to image, etc
            activate_sound "audio/effects/enterMovil.ogg"



        bar:
            value time_start
            range time_max
            xalign 0.5
            xmaximum 300
            if time_start < (time_max * 0.25):
                left_bar "#f00"
