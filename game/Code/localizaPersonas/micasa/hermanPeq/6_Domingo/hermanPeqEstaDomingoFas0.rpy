label hermanaPeqEstaDomingoFas0():
    if tiempoDia == 0: #Amanecer
        $ localizaPeq = "habHermanPeq"
    elif tiempoDia == 1:  #Manyana
        $ localizaPeq = ""
        if peqSuHabitacionMirandoEspejo2daInteracSabados_visit1__ == False and eventDiarioPersonalPeq == False:
            $ localizaPeqComodin = "cocina"
        else:
            $ localizaPeq = "cocina"
        if demonBoy == True:
            if peqSuHabitacionMirandoEspejo2daInteracSabados_visit1__ == False or eventDiarioPersonalPeq == False:
                $ localizaPeq = "habHermanPeq"
    elif tiempoDia == 2:  #Mediodia
        $ localizaPeq = ""
        if peqSuHabitacionMirandoEspejo2daInteracSabados_visit1__ == False and eventDiarioPersonalPeq == False:
            $ localizaPeqComodin = "salon"
        else:
            $ localizaPeq = "salon"
    elif tiempoDia == 3:  #Tarde
        $ localizaPeq = ""
    elif tiempoDia == 4:  #Noche
        $ localizaPeq = "habHermanPeq"
    else:
        $ localizaPeq =""
return
