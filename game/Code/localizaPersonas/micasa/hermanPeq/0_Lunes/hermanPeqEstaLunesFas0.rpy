label hermanaPeqEstaLunesFas0():
    if tiempoDia == 0: #Amanecer
        $ localizaPeq = "habHermanPeq"
    elif tiempoDia == 1:  #Manyana
        $ localizaPeq = ""
    elif tiempoDia == 2:  #Mediodia
        $ localizaPeq = "galeriaGym"
    elif tiempoDia == 3:  #Tarde
        if eventDiarioPersonalPeq == True:
            $ localizaPeq = "cocina"
    elif tiempoDia == 4:  #Noche
        $ localizaPeq = "habHermanPeq"
    else:
        $ localizaPeq =""
return
