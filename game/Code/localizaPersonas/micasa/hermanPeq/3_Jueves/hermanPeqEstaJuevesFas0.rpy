label hermanaPeqEstaJuevesFas0():
    if tiempoDia == 0: #Amanecer
        $ localizaPeq = "banyo"
    elif tiempoDia == 1:  #Manyana
        $ localizaPeq = ""
    elif tiempoDia == 2:  #Mediodia
        if eventDiarioPersonalPeq == False:
            $ localizaPeqComodin = "galeriaGym"
        else:
            $ localizaPeq = "galeriaGym"
    elif tiempoDia == 3:  #Tarde
        $ localizaPeq = "salon"
    elif tiempoDia == 4:  #Noche
        $ localizaPeq = "habHermanPeq"
    else:
        $ localizaPeq =""
return
