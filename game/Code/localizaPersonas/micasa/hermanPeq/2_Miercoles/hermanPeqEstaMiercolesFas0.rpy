label hermanaPeqEstaMiercolesFas0():
    if tiempoDia == 0: #Amanecer
        $ localizaPeq = "banyo"
    elif tiempoDia == 1:  #Manyana
        $ localizaPeq = ""
    elif tiempoDia == 2:  #Mediodia
        $ localizaPeq = "cocina"
    elif tiempoDia == 3:  #Tarde
        $ localizaPeq = ""
    elif tiempoDia == 4:  #Noche
        $ localizaPeq = "habHermanPeq"
    else:
        $ localizaPeq =""
return
