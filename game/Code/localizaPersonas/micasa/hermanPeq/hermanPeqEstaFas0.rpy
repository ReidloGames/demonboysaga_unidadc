label hermanPeqEstaFas0:
    if hermanPeqInvisible == False:
        #LUNES
        if numDia == 0:
            call hermanaPeqEstaLunesFas0() from _call_hermanaPeqEstaLunesFas0
        #Martes
        elif numDia == 1:
            call hermanaPeqEstaMartesFas0() from _call_hermanaPeqEstaMartesFas0
        #Miercoles
        elif numDia == 2:
            call hermanaPeqEstaMiercolesFas0() from _call_hermanaPeqEstaMiercolesFas0
        #Jueves
        elif numDia == 3:
            call hermanaPeqEstaJuevesFas0() from _call_hermanaPeqEstaJuevesFas0
        #Viernes
        elif numDia == 4:
            call hermanaPeqEstaViernesFas0() from _call_hermanaPeqEstaViernesFas0
        #Sabado
        elif numDia == 5:
            call hermanaPeqEstaSabadoFas0() from _call_hermanaPeqEstaSabadoFas0
        #Domingo
        elif numDia == 6:
            call hermanaPeqEstaDomingoFas0() from _call_hermanaPeqEstaDomingoFas0
    else:
        $ localizaPeq =""

    return
