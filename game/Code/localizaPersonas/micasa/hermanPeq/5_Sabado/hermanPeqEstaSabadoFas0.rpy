label hermanaPeqEstaSabadoFas0():
    if tiempoDia == 0: #Amanecer
        $ localizaPeq = ""
        if demonBoy == True:
            $ localizaPeq = "habHermanPeq"
    elif tiempoDia == 1:  #Manyana
        if peqSuHabitacionMirandoEspejo2daInteracSabados_visit1__ == False or eventDiarioPersonalPeq == False:
            $ localizaPeq = "habHermanPeq"
        else:
            $ localizaPeq = ""
    elif tiempoDia == 2:  #Mediodia
        $ localizaPeq = ""
        if peqSuHabitacionMirandoEspejo2daInteracSabados_visit1__ == True or eventDiarioPersonalPeq == True:
            $ localizaPeq = "galeriaGym"
    elif tiempoDia == 3:  #Tarde
        $ localizaPeq = ""
    elif tiempoDia == 4:  #Noche
        $ localizaPeq = "habHermanPeq"
    else:
        $ localizaPeq =""
return
