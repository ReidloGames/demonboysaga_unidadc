label localizaPersonasMiCasaFas0():
    $ localizaMad = ""
    $ localizaMadComodin = ""
    $ localizaPad = ""
    $ localizaPeq = ""
    $ localizaPeqComodin = ""
    $ localizaMed = ""
    $ localizaMedComodin = ""
    $ localizaMay = ""
    $ localizaMayComodin = ""
    $ family5 = ""
    $ family4 = ""
    #hermanPeq
    call hermanPeqEstaFas0() from _call_hermanPeqEstaFas0
    #hermanMed
    call hermanMedEstaFas0() from _call_hermanMedEstaFas0
    #hermanMay
    call hermanMayEstaFas0() from _call_hermanMayEstaFas0
    #Mad
    call madEstaFas0() from _call_madEstaFas0
    #Pad
    call padEstaFas0() from _call_padEstaFas0
    #Todos (Familia)
    call familyTodosEstaFas0() from _call_familyTodosEstaFas0

    return
