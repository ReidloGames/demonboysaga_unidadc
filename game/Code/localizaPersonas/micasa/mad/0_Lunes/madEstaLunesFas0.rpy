label madEstaLunesFas0():
    if tiempoDia == 0: #Amanecer
        $ localizaMad = "banyo"
    elif tiempoDia == 1:  #Manyana
        $ localizaMad = ""
    elif tiempoDia == 2:  #Mediodia
        $ localizaMad = "cocina"
    elif tiempoDia == 3:  #Tarde
        if leccion1Massage == True:
            $ localizaMad = "salon"
    elif tiempoDia == 4:  #Noche
        $ localizaMad = "habPadres"
    else:
        $ localizaMad =""
return
