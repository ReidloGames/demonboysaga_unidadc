label madEstaSabadoFas0():
    if tiempoDia == 0: #Amanecer
        $ localizaMad = "galeriaGym"
        #$ localizaMadComodin = "galeriaGym"
    elif tiempoDia == 1:  #Manyana
        if demonBoy == False:
            $ localizaMad = "jardinPis"
    elif tiempoDia == 2:  #Mediodia
        $ localizaMad = ""
        if haVenidoEthan1 == False:
            $ localizaMadComodin = "cocina"
        else:
            $ localizaMadComodin = ""
    elif tiempoDia == 3:  #Tarde
        $ localizaMad = ""
        if demonBoy == True:
            $ localizaMad = "habPadres"
    elif tiempoDia == 4:  #Noche
        $ localizaMad = "habPadres"
    else:
        $ localizaMad =""
return
