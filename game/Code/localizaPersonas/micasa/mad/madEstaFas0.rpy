label madEstaFas0:
    if madInvisible == False:
        #LUNES
        if numDia == 0:
            call madEstaLunesFas0() from _call_madEstaLunesFas0
        #Martes
        elif numDia == 1:
            call madEstaMartesFas0() from _call_madEstaMartesFas0
        #Miercoles
        elif numDia == 2:
            call madEstaMiercolesFas0() from _call_madEstaMiercolesFas0
        #Jueves
        elif numDia == 3:
            call madEstaJuevesFas0() from _call_madEstaJuevesFas0
        #Viernes
        elif numDia == 4:
            call madEstaViernesFas0() from _call_madEstaViernesFas0
        #Sabado
        elif numDia == 5:
            call madEstaSabadoFas0() from _call_madEstaSabadoFas0
        #Domingo
        elif numDia == 6:
            call madEstaDomingoFas0() from _call_madEstaDomingoFas0
    else:
        $ localizaMad =""

    return
