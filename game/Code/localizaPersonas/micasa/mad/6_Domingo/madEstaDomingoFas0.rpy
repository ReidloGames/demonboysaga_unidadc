label madEstaDomingoFas0():
    if tiempoDia == 0: #Amanecer
        if haVenidoEthan1 == False:
            $ localizaMad = "cocina"
        else:
            $ localizaMad = "banyo"
    elif tiempoDia == 1:  #Manyana
        $ localizaMad = ""
    elif tiempoDia == 2:  #Mediodia
        $ localizaMad = ""
        if haVenidoEthan1 == False:
            $ localizaMadComodin = "cocina"
        else:
            $ localizaMadComodin = ""
    elif tiempoDia == 3:  #Tarde
        if haVenidoEthan1 == False:
            $ localizaMad = "salon"
        else:
            $ localizaMad = "habPadres"
    elif tiempoDia == 4:  #Noche
        $ localizaMad = "habPadres"
    else:
        $ localizaMad =""
return
