label madEstaJuevesFas0():
    if tiempoDia == 0: #Amanecer
        if haVenidoEthan1 == False:
            $ localizaMadComodin = "cocina"
        else:
            $ localizaMadComodin = ""
    elif tiempoDia == 1:  #Manyana
        $ localizaMad = ""
    elif tiempoDia == 2:  #Mediodia
        $ localizaMad = "cocina"
        $ localizaMadComodin = "salon"
    elif tiempoDia == 3:  #Tarde
        $ localizaMad = "banyo"
    elif tiempoDia == 4:  #Noche
        $ localizaMad = "habPadres"
    else:
        $ localizaMad =""
return
