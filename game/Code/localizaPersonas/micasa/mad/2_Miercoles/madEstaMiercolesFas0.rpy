label madEstaMiercolesFas0():
    if tiempoDia == 0: #Amanecer
        $ localizaMad = "galeriaGym"
        #$ localizaMadComodin = "jardinPis"
    elif tiempoDia == 1:  #Manyana
        $ localizaMad = ""
    elif tiempoDia == 2:  #Mediodia
        $ localizaMad = ""
        if haVenidoEthan1 == False:
            $ localizaMadComodin = "salon"
        else:
            $ localizaMadComodin = ""
    elif tiempoDia == 3:  #Tarde
        $ localizaMad = "habPadres"
    elif tiempoDia == 4:  #Noche
        $ localizaMad = "habPadres"
    else:
        $ localizaMad =""
return
