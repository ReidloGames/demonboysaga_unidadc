label madEstaMartesFas0():
    if tiempoDia == 0: #Amanecer
        if eventMadRecogeRopaHabProta_visit:
            $ localizaMad = "galeriaGym"
        else:
            $ localizaMad = "habPadres"
    elif tiempoDia == 1:  #Manyana
        $ localizaMad = ""
    elif tiempoDia == 2:  #Mediodia
        if haVenidoEthan1 == False:
            $ localizaMad = "cocina"
        else:
            $ localizaMad = "galeriaGym"
    elif tiempoDia == 3:  #Tarde
        $ localizaMad = "salon"
    elif tiempoDia == 4:  #Noche
        $ localizaMad = "habPadres"
    else:
        $ localizaMad =""
return
