label madEstaViernesFas0():
    if tiempoDia == 0: #Amanecer
        $ localizaMad = "habPadres"
    elif tiempoDia == 1:  #Manyana
        $ localizaMad = ""
    elif tiempoDia == 2:  #Mediodia
        $ localizaMad = "galeriaGym"
        $ localizaMadComodin = "cocina"
    elif tiempoDia == 3:  #Tarde
        $ localizaMad = "salon"
    elif tiempoDia == 4:  #Noche
        $ localizaMad = "habPadres"
    else:
        $ localizaMad =""
return
