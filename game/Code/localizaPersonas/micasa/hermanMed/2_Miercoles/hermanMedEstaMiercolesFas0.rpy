label hermanaMedEstaMiercolesFas0():
    if tiempoDia == 0: #Amanecer
        $ localizaMed = "habHermanMed"
        $ localizaMedComodin = "jardinPis"
    elif tiempoDia == 1:  #Manyana
        $ localizaMed = ""
    elif tiempoDia == 2:  #Mediodia
        $ localizaMed = "comedor"
    elif tiempoDia == 3:  #Tarde
        $ localizaMed = ""
    elif tiempoDia == 4:  #Noche
        $ localizaMed = "habHermanMed"
    else:
        $ localizaMed =""
return
