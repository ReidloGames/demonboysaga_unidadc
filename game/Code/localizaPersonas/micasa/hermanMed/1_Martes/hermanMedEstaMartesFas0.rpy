label hermanaMedEstaMartesFas0():
    if tiempoDia == 0: #Amanecer
        $ localizaMed = "banyo"
    elif tiempoDia == 1:  #Manyana
        $ localizaMed = ""
    elif tiempoDia == 2:  #Mediodia
        $ localizaMedComodin = "salon"
    elif tiempoDia == 3:  #Tarde
        $ localizaMed = "habHermanMed"
    elif tiempoDia == 4:  #Noche
        $ localizaMed = "habHermanMed"
    else:
        $ localizaMed =""
return
