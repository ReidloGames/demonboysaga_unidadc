label hermanaMedEstaSabadoFas0():
    if tiempoDia == 0: #Amanecer
        $ localizaMed = ""
    elif tiempoDia == 1:  #Manyana
        $ localizaMed = "jardinPis"
    elif tiempoDia == 2:  #Mediodia
        $ localizaMed = ""
    elif tiempoDia == 3:  #Tarde
        $ localizaMed = "salon"
    elif tiempoDia == 4:  #Noche
        $ localizaMed = "habHermanMed"
    else:
        $ localizaMed =""
return
