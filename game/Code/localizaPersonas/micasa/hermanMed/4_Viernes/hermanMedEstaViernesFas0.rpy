label hermanaMedEstaViernesFas0():
    if tiempoDia == 0: #Amanecer
        $ localizaMed = "banyo"
    elif tiempoDia == 1:  #Manyana
        $ localizaMed = ""
    elif tiempoDia == 2:  #Mediodia
        $ localizaMedComodin = "comedor"
    elif tiempoDia == 3:  #Tarde
        $ localizaMed = "salon"
    elif tiempoDia == 4:  #Noche
        $ localizaMed = ""
    else:
        $ localizaMed =""
return
