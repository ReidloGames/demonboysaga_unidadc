label hermanMedEstaFas0:
    if hermanMedInvisible == False:
        #LUNES
        if numDia == 0:
            call hermanaMedEstaLunesFas0() from _call_hermanaMedEstaLunesFas0
        #Martes
        elif numDia == 1:
            call hermanaMedEstaMartesFas0() from _call_hermanaMedEstaMartesFas0
        #Miercoles
        elif numDia == 2:
            call hermanaMedEstaMiercolesFas0() from _call_hermanaMedEstaMiercolesFas0
        #Jueves
        elif numDia == 3:
            call hermanaMedEstaJuevesFas0() from _call_hermanaMedEstaJuevesFas0
        #Viernes
        elif numDia == 4:
            call hermanaMedEstaViernesFas0() from _call_hermanaMedEstaViernesFas0
        #Sabado
        elif numDia == 5:
            call hermanaMedEstaSabadoFas0() from _call_hermanaMedEstaSabadoFas0
        #Domingo
        elif numDia == 6:
            call hermanaMedEstaDomingoFas0() from _call_hermanaMedEstaDomingoFas0
    else:
        $ localizaMed = ""

    return
