label hermanaMedEstaLunesFas0():
    if tiempoDia == 0: #Amanecer
        $ localizaMed = "habHermanMed"
    elif tiempoDia == 1:  #Manyana
        $ localizaMed = "habHermanMed"
    elif tiempoDia == 2:  #Mediodia
        $ localizaMed = ""
    elif tiempoDia == 3:  #Tarde
        $ localizaMed = ""
    elif tiempoDia == 4:  #Noche
        $ localizaMed = "habHermanMed"
    else:
        $ localizaMed =""
    return
