label padEstaFas0:
    if padInvisible == False:
        #LUNES
        if numDia == 0:
            call padEstaLunesFas0() from _call_padEstaLunesFas0
        #Martes
        elif numDia == 1:
            call padEstaMartesFas0() from _call_padEstaMartesFas0
        #Miercoles
        elif numDia == 2:
            call padEstaMiercolesFas0() from _call_padEstaMiercolesFas0
        #Jueves
        elif numDia == 3:
            call padEstaJuevesFas0() from _call_padEstaJuevesFas0
        #Viernes
        elif numDia == 4:
            call padEstaViernesFas0() from _call_padEstaViernesFas0
        #Sabado
        elif numDia == 5:
            call padEstaSabadoFas0() from _call_padEstaSabadoFas0
        #Domingo
        elif numDia == 6:
            call padEstaDomingoFas0() from _call_padEstaDomingoFas0
    else:
        $ localizaPad =""
    return
