label padEstaViernesFas0():
    if tiempoDia == 0: #Amanecer
        $ localizaPad = ""
    elif tiempoDia == 1:  #Manyana
        $ localizaPad = ""
    elif tiempoDia == 2:  #Mediodia
        $ localizaPad = "salon"
    elif tiempoDia == 3:  #Tarde
        $ localizaPad = "salon"
    elif tiempoDia == 4:  #Noche
        $ localizaPad = "habPadres"
    else:
        $ localizaPad =""
return
