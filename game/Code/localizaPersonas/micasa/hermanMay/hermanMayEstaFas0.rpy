label hermanMayEstaFas0:
    if hermanMayInvisible == False:
        #LUNES
        if numDia == 0:
            call hermanaMayEstaLunesFas0() from _call_hermanaMayEstaLunesFas0
        #Martes
        elif numDia == 1:
            call hermanaMayEstaMartesFas0() from _call_hermanaMayEstaMartesFas0
        #Miercoles
        elif numDia == 2:
            call hermanaMayEstaMiercolesFas0() from _call_hermanaMayEstaMiercolesFas0
        #Jueves
        elif numDia == 3:
            call hermanaMayEstaJuevesFas0() from _call_hermanaMayEstaJuevesFas0
        #Viernes
        elif numDia == 4:
            call hermanaMayEstaViernesFas0() from _call_hermanaMayEstaViernesFas0
        #Sabado
        elif numDia == 5:
            call hermanaMayEstaSabadoFas0() from _call_hermanaMayEstaSabadoFas0
        #Domingo
        elif numDia == 6:
            call hermanaMayEstaDomingoFas0() from _call_hermanaMayEstaDomingoFas0
    else:
        $ localizaMay = ""

    return
