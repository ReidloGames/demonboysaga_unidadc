label hermanaMayEstaLunesFas0():
    if tiempoDia == 0: #Amanecer
        $ localizaMay = ""
    elif tiempoDia == 1:  #Manyana
        $ localizaMay = "banyo"
    elif tiempoDia == 2:  #Mediodia
        $ localizaMay = ""
    elif tiempoDia == 3:  #Tarde
        $ localizaMay = ""
    elif tiempoDia == 4:  #Noche
        $ localizaMay = "habHermanMay"
    else:
        $ localizaMay =""
return
