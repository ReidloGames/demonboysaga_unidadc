label nextZona(newRoom):
    $ room = newRoom
    $ destino = ""

    if demonBoy == True:
        call sonidosCambiandoseyDucha() from _call_sonidosCambiandoseyDucha

    call deshabilitaObjetosPersonas from _call_deshabilitaObjetosPersonas
    call eventosZonaTiempoDia() from _call_eventosZonaTiempoDia

    call escenariosGame() from _call_escenariosGame

    return


label escenariosGame():
    #MiCasa
    call localizaPersonas() from _call_localizaPersonas

    call deshabilitaPersonas() from _call_deshabilitaPersonas
    #call deshabilitaObjetosPersonas from _call_deshabilitaObjetosPersonas
    call zonasMiCasa from _call_zonasMiCasa

    #Mapa
    call zonasMiMapa from _call_zonasMiMapa
    #Escuela
    call zonasEscuela from _call_zonasEscuela
    #Tienda
    call zonasTiendaComestibles from _call_zonasTiendaComestibles
    #Amarzong
    call zonasDesp_amarzong from _call_zonasDesp_amarzong
    #TiendaKara
    call zonasTiendaKara from _call_zonasTiendaKara
    #Biblioteca
    call zonasDesp_Biblioteca

    return

label sonidosCambiandoseyDucha():
    if zona == "micasa":
        if room == "pasillo2":
            #PEQCambiandose
            if cambiandosePeq == True and hasEntradoHabPeqCambiandose == False:
                if numDia == 0 and tiempoDia == 0 or numDia == 4 and tiempoDia == 0:
                    stop sound
                    play sound "audio/effects/puertaCambiandose.ogg"
            #MEDCambiandose
            if cambiandoseMed == True and hasEntradoHabMedCambiandose == False:
                if numDia == 0 and tiempoDia == 0 or numDia == 2 and tiempoDia == 0 or numDia == 3 and tiempoDia == 0:
                    stop sound
                    play sound "audio/effects/puertaCambiandose.ogg"

            #MAYCambiandose
            if cambiandoseMay == True and hasEntradoHabMayCambiandose == False:
                if numDia == 1 and tiempoDia == 1 or numDia == 6 and tiempoDia == 0:
                    stop sound
                    play sound "audio/effects/puertaCambiandose.ogg"
        if room == "comedor":
            #MadCambiandose
            if cambiandoseMad == True and hasEntradoHabPadrCambiandose == False:
                if numDia == 1 and tiempoDia == 0 or numDia == 4 and tiempoDia == 0:
                    play sound "audio/effects/puertaCambiandose.ogg"

            #Ducha:
            #MadDucha
            if hasEntradoBanyoMad == False:
                if numDia == 0 and tiempoDia == 0 or numDia == 3 and tiempoDia == 3:
                    play sound "audio/effects/duchaCorta.ogg"

            #PeqDucha
            if hasEntradoBanyoPeq == False:
                if numDia == 2 and tiempoDia == 0 or numDia == 4 and tiempoDia == 0:
                    play sound "audio/effects/duchaCorta.ogg"
            #MedDucha
            if hasEntradoBanyoMed == False:
                if numDia == 1 and tiempoDia == 0 or numDia == 4 and tiempoDia == 0:
                    play sound "audio/effects/duchaCorta.ogg"
            #MayDucha
            if hasEntradoBanyoMay == False:
                if numDia == 0 and tiempoDia == 1 or numDia == 4 and tiempoDia == 1:
                    play sound "audio/effects/duchaCorta.ogg"

    return


label deshabilitaObjetosPersonas():
    #Al cambiar de habitacion hace que no arrastre objetos ni personas a otras salas

    #ZONAS MICASA
    hide screen banyoCoordenadas
    hide screen cocinaCoordenadas #
    hide screen comedorCoordenadas
    hide screen entradaCasaCoordenadas
    hide screen galeriaGymCoordenadas
    hide screen garageCoordenadas
    hide screen habHermanMayCoordenadas
    hide screen habHermanMedCoordenadas
    hide screen habHermanPeqCoordenadas
    hide screen habPadresCoordenadas
    hide screen jardinPisCoordenadas
    hide screen miHabitacionCoordenadas
    hide screen pasillo1Coordenadas
    hide screen pasillo2Coordenadas
    hide screen salonCoordenadas


    #Mad Cocina
    #hide screen cocinaCoordenadasMadAmanecer
    #hide screen cocinaCoordenadasMadManyana
    hide screen cocinaMediodiaLunesMad
    hide screen cocinaLimpiaMediodiaMartesMad
    hide screen cocinaCoordenadasMadMediodia
    hide screen cocinaDarMamarMediodiaJuevesMad
    hide screen cocinaDesayunoAmanecerDomingoMad
    #Mad Salon
    hide screen salonCoordenadasMadTarde
    hide screen salonTardeLunesMad
    hide screen salonTardeMartesMad
    hide screen salonTardeDomingoMad

    hide screen tvLeon
    hide screen tvLeon1
    #Mad Comedor
    hide screen comedorRevistaTardeSabadoMad
    hide screen comedorLimpiaAmanecerSabadoMad
    #Mad galeriaGym
    hide screen galeriaGymYogaAmanecerMiercolesMad
    hide screen galeriaGymLavaderoAmanecerJuevesMad
    #Mad habPadr
    hide screen habPadrPijamTardeMiercolesMad
    #Mad JardinPis
    hide screen jardinPisTomarSolManyanaSabadoMad

    #Pad habPadr
    hide screen habPadresCoordenadas
    hide screen habPadrAmanecerLunesPad
    hide screen habPadrManyanaMiercolesPad
    hide screen tvDeporte0
    hide screen tvDeporte1
    #Pad Salon
    hide screen salonMediodiaViernesPad
    hide screen tvDeporte0
    hide screen tvDeporte1


    #Peq galeriaGym
    hide screen galeriaGymLavaderoMediodiaLunesPeq
    #Peq habHermanPeq
    hide screen habHermanPeqMiraPcAmanecerMartesPeq
    hide screen reflejoEspejo
    hide screen habPeqMiraEspejoManyanaSabadoPeq
    hide screen habHermanPeqEncimaSuCamaAmanecerDomingoPeq
    #Peq Cocina
    hide screen cocinaCocinandoMediodiaMiercolesPeq
    #Peq Salon
    hide screen salonTardeJuevesPeq

    #Med###########################
    #Med HabHermanMed####
    hide screen habMedEstudiaCamaManyanaLunesMed
    hide screen habMedArmarioRopaMartesMed
    #Med Comedor
    hide screen comedorEstudiandoMediodiaMiercolesMed
    #Med Salon
    hide screen salonMiraMoviTardeJuevesMed
    hide screen salonMiraMovilTardeSabadoMed
    #Med PisicnaJardin
    hide screen jardinPisTomarSolManyanaSabadoMed

    #May #####################
    #May HabHermanMay
    hide screen habMayEnBragasAmanecerMartesMay
    hide screen habMayEnSuPcTardeMartesMay
    hide screen habMayEnSuPcTardeJuevesMay
    #May PisicnaJardin
    hide screen jardinPisTomarSolManyanaMiercolesMay
    #May Salon
    hide screen salonPintaUnyasTardeMiercolesMay
    #May galeriaGym
    hide screen galeriaGymGimnasiaAmanecerViernesMay
    #May cocina
    hide screen cocinaMicroAmanecerSabadoMay

    #Familia ####
    hide screen salonMiraTvTardeViernesFamily4
    hide screen salonMiraTvTardeViernesFamily5




    return
