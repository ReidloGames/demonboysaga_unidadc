label rutaImgAmarzong:
    call rutaImagenesLouie() from _call_rutaImagenesLouie
    call rutaImagenesCarla() from _call_rutaImagenesCarla


    #entradaAmarzong
    image entradaAmarzong = "images/escenas/amarzong/entradaAmarzong/entradaAmarzong.png"
    image entradaAmarzong_S = "images/escenas/amarzong/entradaAmarzong/entradaAmarzong_S.png"

    #Persona
    image louieTrabajo = "images/escenas/amarzong/entradaAmarzong/personas/Louie/louieTrabajo.png"
    image louieTrabajo_S = "images/escenas/amarzong/entradaAmarzong/personas/Louie/louieTrabajo_S.png"

    image carlaTrabajo = "images/escenas/amarzong/entradaAmarzong/personas/Carla/carlaTrabajo.png"
    image carlaTrabajo_S = "images/escenas/amarzong/entradaAmarzong/personas/Carla/carlaTrabajo_S.png"

    #habitacionAmarzong
    image habitacionAmarzong = "images/escenas/amarzong/habitacionAmarzong/habitacionAmarzong.png"
    image habitacionAmarzong_S = "images/escenas/amarzong/habitacionAmarzong/habitacionAmarzong_S.png"

    #Trabajo
    image trabajoBici_0 = "images/escenas/amarzong/trabajoAmarzong/trabajoBici_0.png"
    image trabajoBici_1 = "images/escenas/amarzong/trabajoAmarzong/trabajoBici_1.png"
    image trabajoBici_2 = "images/escenas/amarzong/trabajoAmarzong/trabajoBici_2.png"
    image trabajoBici_3 = "images/escenas/amarzong/trabajoAmarzong/trabajoBici_3.png"


    return
