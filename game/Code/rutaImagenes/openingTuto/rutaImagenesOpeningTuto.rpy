label rutaImgOpeningTuto:

    image op_1 = "images/escenas/openingtuto/opening/op_1.png"
    image op_2 = "images/escenas/openingtuto/opening/op_2.png"
    image op_3 = "images/escenas/openingtuto/opening/op_3.png"
    image op_4 = "images/escenas/openingtuto/opening/op_4.png"
    image prueba = "images/escenas/openingtuto/opening/prueba.png"
    image tuto_1 = "images/escenas/openingtuto/opening/tuto_1.png"
    image tuto_2 = "images/escenas/openingtuto/opening/tuto_2.png"
    image tuto_3 = "images/escenas/openingtuto/opening/tuto_3.png"
    image tuto_4 = "images/escenas/openingtuto/opening/tuto_4.png"
    image tuto_5 = "images/escenas/openingtuto/opening/tuto_5.png"
    image tuto_6 = "images/escenas/openingtuto/opening/tuto_6.png"
    image tuto_7 = "images/escenas/openingtuto/opening/tuto_7.png"
    image tuto_7_ = "images/escenas/openingtuto/opening/tuto_7_.png"
    image tuto_7__ = "images/escenas/openingtuto/opening/tuto_7__.png"
    image tuto_7___ = "images/escenas/openingtuto/opening/tuto_7___.png"
    image tuto_8 = "images/escenas/openingtuto/opening/tuto_8.png"
    image tuto_9 = "images/escenas/openingtuto/opening/tuto_9.png"
    image tuto_10 = "images/escenas/openingtuto/opening/tuto_10.png"


    return


label rutaImgOpening:
    image intro_01 = "images/escenas/openingIntro/intro_01.png"
    image intro_02 = "images/escenas/openingIntro/intro_02.png"
    image intro_02_REC = "images/escenas/openingIntro/intro_02_REC.png"
    image intro_03 = "images/escenas/openingIntro/intro_03.png"
    image intro_04 = "images/escenas/openingIntro/intro_04.png"
    image intro_05 = "images/escenas/openingIntro/intro_05.png"
    image intro_06 = "images/escenas/openingIntro/intro_06.png"
    image intro_07 = "images/escenas/openingIntro/intro_07.png"
    image intro_08 = "images/escenas/openingIntro/intro_08.png"
    return

label rutaImgTutoAccesosComodin:
    image comodin1 = "images/escenas/comodinTuto/comodin1.png"
    image comodin1_ = "images/escenas/comodinTuto/comodin1_.png"
    image comodin2 = "images/escenas/comodinTuto/comodin2.png"
    image comodin3 = "images/escenas/comodinTuto/comodin3.png"
    image comodin4 = "images/escenas/comodinTuto/comodin4.png"
    image comodin5 = "images/escenas/comodinTuto/comodin5.png"
    image comodin6 = "images/escenas/comodinTuto/comodin6.png"
    image comodin7 = "images/escenas/comodinTuto/comodin7.png"
    image comodin8 = "images/escenas/comodinTuto/comodin8.png"
    image comodin9 = "images/escenas/comodinTuto/comodin9.png"
    return
