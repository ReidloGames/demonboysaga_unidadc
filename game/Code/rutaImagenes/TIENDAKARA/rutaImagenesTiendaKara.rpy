label rutaImagenesTiendaKara():
    #Entrada1 TiendaKara1
    image tiendaKara_1 = "images/escenas/tiendaKara/tiendaKara1/tiendaKara_1.png"
    image tiendaKara_1_S = "images/escenas/tiendaKara/tiendaKara1/tiendaKara_1_S.png"
    image karaTiendaPers = "images/escenas/tiendaKara/tiendaKara1/personas/karaTiendaPers.png"
    image karaTiendaPers_S = "images/escenas/tiendaKara/tiendaKara1/personas/karaTiendaPers_S.png"

    # TiendaKara2
    image tiendaKara_2 = "images/escenas/tiendaKara/tiendaKara2/tiendaKara_2.png"
    image tiendaKara_2_S = "images/escenas/tiendaKara/tiendaKara2/tiendaKara_2_S.png"



    #ComprarTrajeGym
    image comprarEquipoGym_0 = "images/escenas2/storeKara/compraEquipoGym/comprarEquipoGym_0.webp"
    image comprarEquipoGym_1 = "images/escenas2/storeKara/compraEquipoGym/comprarEquipoGym_1.webp"
    image comprarEquipoGym_2 = "images/escenas2/storeKara/compraEquipoGym/comprarEquipoGym_2.webp"

    image comprarEquipoGym_2_0 = "images/escenas2/storeKara/compraEquipoGym/comprarEquipoGym_2_0.webp"
    image comprarEquipoGym_2_1 = "images/escenas2/storeKara/compraEquipoGym/comprarEquipoGym_2_1.webp"
    image comprarEquipoGym_2_2 = "images/escenas2/storeKara/compraEquipoGym/comprarEquipoGym_2_2.webp"

    image comprarEquipoGym_3 = "images/escenas2/storeKara/compraEquipoGym/comprarEquipoGym_3.webp"
    image comprarEquipoGym_4 = "images/escenas2/storeKara/compraEquipoGym/comprarEquipoGym_4.webp"
    image comprarEquipoGym_5 = "images/escenas2/storeKara/compraEquipoGym/comprarEquipoGym_5.webp"
    image comprarEquipoGym_6 = "images/escenas2/storeKara/compraEquipoGym/comprarEquipoGym_6.webp"
    image comprarEquipoGym_7 = "images/escenas2/storeKara/compraEquipoGym/comprarEquipoGym_7.webp"
    image comprarEquipoGym_8 = "images/escenas2/storeKara/compraEquipoGym/comprarEquipoGym_8.webp"
    image comprarEquipoGym_9 = "images/escenas2/storeKara/compraEquipoGym/comprarEquipoGym_9.webp"
    image comprarEquipoGym_10 = "images/escenas2/storeKara/compraEquipoGym/comprarEquipoGym_10.webp"
    image comprarEquipoGym_11 = "images/escenas2/storeKara/compraEquipoGym/comprarEquipoGym_11.webp"
    image comprarEquipoGym_12 = "images/escenas2/storeKara/compraEquipoGym/comprarEquipoGym_12.webp"
    image comprarEquipoGym_13 = "images/escenas2/storeKara/compraEquipoGym/comprarEquipoGym_13.webp"
    image comprarEquipoGym_14 = "images/escenas2/storeKara/compraEquipoGym/comprarEquipoGym_14.webp"
    image comprarEquipoGym_15 = "images/escenas2/storeKara/compraEquipoGym/comprarEquipoGym_15.webp"
    image comprarEquipoGym_16 = "images/escenas2/storeKara/compraEquipoGym/comprarEquipoGym_16.webp"
    image comprarEquipoGym_17 = "images/escenas2/storeKara/compraEquipoGym/comprarEquipoGym_17.webp"
    image comprarEquipoGym_18 = "images/escenas2/storeKara/compraEquipoGym/comprarEquipoGym_18.webp"
    image comprarEquipoGym_19 = "images/escenas2/storeKara/compraEquipoGym/comprarEquipoGym_19.webp"
    image comprarEquipoGym_20 = "images/escenas2/storeKara/compraEquipoGym/comprarEquipoGym_20.webp"
    image comprarEquipoGym_21 = "images/escenas2/storeKara/compraEquipoGym/comprarEquipoGym_21.webp"
    image comprarEquipoGym_22 = "images/escenas2/storeKara/compraEquipoGym/comprarEquipoGym_22.webp"
    image comprarEquipoGym_23 = "images/escenas2/storeKara/compraEquipoGym/comprarEquipoGym_23.webp"
    image comprarEquipoGym_24 = "images/escenas2/storeKara/compraEquipoGym/comprarEquipoGym_24.webp"
    image comprarEquipoGym_25 = "images/escenas2/storeKara/compraEquipoGym/comprarEquipoGym_25.webp"
    image comprarEquipoGym_26 = "images/escenas2/storeKara/compraEquipoGym/comprarEquipoGym_26.webp"
    image comprarEquipoGym_27 = "images/escenas2/storeKara/compraEquipoGym/comprarEquipoGym_27.webp"
    image comprarEquipoGym_28 = "images/escenas2/storeKara/compraEquipoGym/comprarEquipoGym_28.webp"
    image comprarEquipoGym_29 = "images/escenas2/storeKara/compraEquipoGym/comprarEquipoGym_29.webp"

    return
