label rutaImagenesLucia():
    image luciaLimpiezaPasilloWc = "images/escenas/escuela/pasillo_Wc/personas/Lucia/luciaLimpiezaPasilloWc.png"
    image luciaLimpiezaPasilloWc_S = "images/escenas/escuela/pasillo_Wc/personas/Lucia/luciaLimpiezaPasilloWc_S.png"

    #LuciaHablaConProta
    image luciaPasilloWcEscuelaLimpiando_0 = "images/escenas/escuela/pasillo_Wc/personas/Lucia/luciaHablaConProta/luciaPasilloWcEscuelaLimpiando_0.png"
    image luciaPasilloWcEscuelaLimpiando_1 = "images/escenas/escuela/pasillo_Wc/personas/Lucia/luciaHablaConProta/luciaPasilloWcEscuelaLimpiando_1.png"
    image luciaPasilloWcEscuelaLimpiando_2 = "images/escenas/escuela/pasillo_Wc/personas/Lucia/luciaHablaConProta/luciaPasilloWcEscuelaLimpiando_2.png"
    image luciaPasilloWcEscuelaLimpiando_3 = "images/escenas/escuela/pasillo_Wc/personas/Lucia/luciaHablaConProta/luciaPasilloWcEscuelaLimpiando_3.png"
    image luciaPasilloWcEscuelaLimpiando_4 = "images/escenas/escuela/pasillo_Wc/personas/Lucia/luciaHablaConProta/luciaPasilloWcEscuelaLimpiando_4.png"
    image luciaPasilloWcEscuelaLimpiando_5 = "images/escenas/escuela/pasillo_Wc/personas/Lucia/luciaHablaConProta/luciaPasilloWcEscuelaLimpiando_5.png"
    image luciaPasilloWcEscuelaLimpiando_6 = "images/escenas/escuela/pasillo_Wc/personas/Lucia/luciaHablaConProta/luciaPasilloWcEscuelaLimpiando_6.png"
    image luciaPasilloWcEscuelaLimpiando_7 = "images/escenas/escuela/pasillo_Wc/personas/Lucia/luciaHablaConProta/luciaPasilloWcEscuelaLimpiando_7.png"
    image luciaPasilloWcEscuelaLimpiando_8 = "images/escenas/escuela/pasillo_Wc/personas/Lucia/luciaHablaConProta/luciaPasilloWcEscuelaLimpiando_8.png"
    image luciaPasilloWcEscuelaLimpiando_9 = "images/escenas/escuela/pasillo_Wc/personas/Lucia/luciaHablaConProta/luciaPasilloWcEscuelaLimpiando_9.png"

        #YaHablado
    image luciaLimpiaEscuelaYavisto_1_0 = "images/escenas/escuela/pasillo_Wc/personas/Lucia/luciaHablaConProta/yaHablado/luciaLimpiaEscuelaYavisto_1_0.png"
    image luciaLimpiaEscuelaYavisto_1_1 = "images/escenas/escuela/pasillo_Wc/personas/Lucia/luciaHablaConProta/yaHablado/luciaLimpiaEscuelaYavisto_1_1.png"
    image luciaLimpiaEscuelaYavisto_2_0 = "images/escenas/escuela/pasillo_Wc/personas/Lucia/luciaHablaConProta/yaHablado/luciaLimpiaEscuelaYavisto_2_0.png"
    image luciaLimpiaEscuelaYavisto_2_1 = "images/escenas/escuela/pasillo_Wc/personas/Lucia/luciaHablaConProta/yaHablado/luciaLimpiaEscuelaYavisto_2_1.png"

    return
