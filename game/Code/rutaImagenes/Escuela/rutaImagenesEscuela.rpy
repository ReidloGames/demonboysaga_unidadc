
label rutaImgEscuela:
    call rutaImagenesLucia() from _call_rutaImagenesLucia
    call rutaImagenesAlicia() from _call_rutaImagenesAlicia
    call rutaImagenesEntrada2() from _call_rutaImagenesEntrada2
    call rutaImagenesBanyoHombres() from _call_rutaImagenesBanyoHombres
    call rutaImagenesPasilloWc() from _call_rutaImagenesPasilloWc
    call rutaImagenesObjects() from _call_rutaImagenesObjects
    call rutaImagenesClase() from _call_rutaImagenesClase
    #Entrada1
    image entradaEscuela1SG = "images/escenas/escuela/entradaEscuela1/entradaEscuela1.png"
    image entradaEscuela1_S = "images/escenas/escuela/entradaEscuela1/entradaEscuela1_S.png"

    #Entrada2
    image entradaEscuela2SG = "images/escenas/escuela/entradaEscuela2/entradaEscuela2.png"
    image entradaEscuela2_S = "images/escenas/escuela/entradaEscuela2/entradaEscuela2_S.png"

    #PasilloWC
    image pasilloWc = "images/escenas/escuela/pasillo_Wc/pasilloWC.png"
    image pasilloWc_S = "images/escenas/escuela/pasillo_Wc/pasilloWC_S.png"

    #BanyoHombres
    image banyoHombres = "images/escenas/escuela/wc_hombres/banyo_hombres.png"
    image banyoHombres_S = "images/escenas/escuela/wc_hombres/banyo_hombres_S.png"

    #BanyoMujeres
    image banyoMujeres = "images/escenas/escuela/wc_mujeres/banyo_mujeres.png"
    image banyoMujeres_S = "images/escenas/escuela/wc_mujeres/banyo_mujeres_S.png"

    #PasilloClase
    image pasilloClase = "images/escenas/escuela/pasilloClase/pasilloClase.png"
    image pasilloClase_S = "images/escenas/escuela/pasilloClase/pasilloClase_S.png"

    #Clase
    image clase = "images/escenas/escuela/clase/clase.png"
    image clase_S = "images/escenas/escuela/clase/clase_S.png"

    #PasilloDirector
    image pasilloDirector = "images/escenas/escuela/pasilloDirector/pasilloDirector.png"
    image pasilloDirector_S = "images/escenas/escuela/pasilloDirector/pasilloDirector_S.png"

    #DespachoDirector
    image despachoDirectorSN = "images/escenas/escuela/despachoDirector/despachoDirectorSN.png"
    image despachoDirector_S = "images/escenas/escuela/despachoDirector/despachoDirector_S.png"

    #TaquillaProta
    image taquillaProta = "images/escenas/escuela/taquillaProta/taquillaProta.png"
    image taquillaProta_S = "images/escenas/escuela/taquillaProta/taquillaProta_S.png"


    #PERSONAS**********
        #Entrada1
        #PEQ
    image peqPasilloEscuelaSentada = "images/escenas/escuela/entradaEscuela1/personas/peq/peqPasilloEscuelaSentada.png"
    image peqPasilloEscuelaSentada_S = "images/escenas/escuela/entradaEscuela1/personas/peq/peqPasilloEscuelaSentada_S.png"

    #PasilloClase
        #Mike
    image mikePasilloClase = "images/escenas/escuela/pasilloClase/personas/mike/mikePasilloClase.png"
    image mikePasilloClase_S = "images/escenas/escuela/pasilloClase/personas/mike/mikePasilloClase_S.png"

    return
