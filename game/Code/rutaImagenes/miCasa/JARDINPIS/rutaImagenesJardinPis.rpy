label rutaImagenesJardinPis():
    call rutaImagenesJardinPisMad() from _call_rutaImagenesJardinPisMad
    call rutaImagenesJardinPisMed() from _call_rutaImagenesJardinPisMed
    call rutaImagenesJardinPisMay() from _call_rutaImagenesJardinPisMay
    
    image madTomaSol1 = "images/escenas/micasa/jardin/personas/Mad/2da/madTomaSol1.png"
    image madTomaSol1_S = "images/escenas/micasa/jardin/personas/Mad/2da/madTomaSol1_S.png"

    image medTomaSol1 = "images/escenas/micasa/jardin/personas/Med/2da/medTomaSol1.png"
    image medTomaSol1_S = "images/escenas/micasa/jardin/personas/Med/2da/medTomaSol1_S.png"

    image mayTomandoSol1 = "images/escenas/micasa/jardin/personas/May/2da/mayTomandoSol1.png"
    image mayTomandoSol1_S = "images/escenas/micasa/jardin/personas/May/2da/mayTomandoSol1_S.png"


    return
