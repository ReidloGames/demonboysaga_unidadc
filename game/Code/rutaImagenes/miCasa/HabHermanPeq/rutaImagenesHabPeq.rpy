label rutaImagenesHabPeq():
    call rutaImagenesHabPeqPeq() from _call_rutaImagenesHabPeqPeq
    image peqMirandoSuPc1 = "images/escenas/micasa/hab_hijPeq/personas/Peq/1ra/peqMirandoSuPc1.png"
    image peqMirandoSuPc1_S = "images/escenas/micasa/hab_hijPeq/personas/Peq/1ra/peqMirandoSuPc1_S.png"
    image peqEspejoReflejo = "images/escenas/micasa/hab_hijPeq/personas/Peq/1ra/peqEspejoReflejo.png"

    image peqEncimaSuCama1 = "images/escenas/micasa/hab_hijPeq/personas/Peq/1ra/peqEncimaSuCama1.png"
    image peqEncimaSuCama1_S = "images/escenas/micasa/hab_hijPeq/personas/Peq/1ra/peqEncimaSuCama1_S.png"

    image peqMirandoEspejo1 = "images/escenas/micasa/hab_hijPeq/personas/Peq/2da/peqMirandoEspejo1.png"
    image peqMirandoEspejo1_S = "images/escenas/micasa/hab_hijPeq/personas/Peq/2da/peqMirandoEspejo1_S.png"

    return
