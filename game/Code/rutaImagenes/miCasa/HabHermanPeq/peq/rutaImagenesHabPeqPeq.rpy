label rutaImagenesHabPeqPeq():
    #Martes 1ra
    image peqSuHabitacionMiraPcMartes_0 = "images/escenas/micasa/hab_hijPeq/personas/Peq/1ra/peqSuHabitacionMiraPcMartes/peqSuHabitacionMiraPcMartes_0.webp"
    image peqSuHabitacionMiraPcMartes_1 = "images/escenas/micasa/hab_hijPeq/personas/Peq/1ra/peqSuHabitacionMiraPcMartes/peqSuHabitacionMiraPcMartes_1.webp"
    image peqSuHabitacionMiraPcMartes_2 = "images/escenas/micasa/hab_hijPeq/personas/Peq/1ra/peqSuHabitacionMiraPcMartes/peqSuHabitacionMiraPcMartes_2.webp"
    image peqSuHabitacionMiraPcMartes_3 = "images/escenas/micasa/hab_hijPeq/personas/Peq/1ra/peqSuHabitacionMiraPcMartes/peqSuHabitacionMiraPcMartes_3.webp"
    image peqSuHabitacionMiraPcMartes_4 = "images/escenas/micasa/hab_hijPeq/personas/Peq/1ra/peqSuHabitacionMiraPcMartes/peqSuHabitacionMiraPcMartes_4.webp"
    image peqSuHabitacionMiraPcMartes_5 = "images/escenas/micasa/hab_hijPeq/personas/Peq/1ra/peqSuHabitacionMiraPcMartes/peqSuHabitacionMiraPcMartes_5.webp"
    image peqSuHabitacionMiraPcMartes_6 = "images/escenas/micasa/hab_hijPeq/personas/Peq/1ra/peqSuHabitacionMiraPcMartes/peqSuHabitacionMiraPcMartes_6.webp"
    image peqSuHabitacionMiraPcMartes_7 = "images/escenas/micasa/hab_hijPeq/personas/Peq/1ra/peqSuHabitacionMiraPcMartes/peqSuHabitacionMiraPcMartes_7.webp"
    image peqSuHabitacionMiraPcMartes_8 = "images/escenas/micasa/hab_hijPeq/personas/Peq/1ra/peqSuHabitacionMiraPcMartes/peqSuHabitacionMiraPcMartes_8.webp"
    image peqSuHabitacionMiraPcMartes_9 = "images/escenas/micasa/hab_hijPeq/personas/Peq/1ra/peqSuHabitacionMiraPcMartes/peqSuHabitacionMiraPcMartes_9.webp"
    image peqSuHabitacionMiraPcMartes_10 = "images/escenas/micasa/hab_hijPeq/personas/Peq/1ra/peqSuHabitacionMiraPcMartes/peqSuHabitacionMiraPcMartes_10.webp"

    #Sabado 2da
    image peqSuHabitacionMirandoEspejo_1 = "images/escenas/micasa/hab_hijPeq/personas/peq/2da/peqMirandoEspejo2daSabado/peqSuHabitacionMirandoEspejo_1.webp"
    image peqSuHabitacionMirandoEspejo_2 = "images/escenas/micasa/hab_hijPeq/personas/peq/2da/peqMirandoEspejo2daSabado/peqSuHabitacionMirandoEspejo_2.webp"
    image peqSuHabitacionMirandoEspejo_3 = "images/escenas/micasa/hab_hijPeq/personas/peq/2da/peqMirandoEspejo2daSabado/peqSuHabitacionMirandoEspejo_3.webp"
    image peqSuHabitacionMirandoEspejo_4 = "images/escenas/micasa/hab_hijPeq/personas/peq/2da/peqMirandoEspejo2daSabado/peqSuHabitacionMirandoEspejo_4.webp"
    image peqSuHabitacionMirandoEspejo_5 = "images/escenas/micasa/hab_hijPeq/personas/peq/2da/peqMirandoEspejo2daSabado/peqSuHabitacionMirandoEspejo_5.webp"
    image peqSuHabitacionMirandoEspejo_6 = "images/escenas/micasa/hab_hijPeq/personas/peq/2da/peqMirandoEspejo2daSabado/peqSuHabitacionMirandoEspejo_6.webp"
    image peqSuHabitacionMirandoEspejo_7 = "images/escenas/micasa/hab_hijPeq/personas/peq/2da/peqMirandoEspejo2daSabado/peqSuHabitacionMirandoEspejo_7.webp"
    image peqSuHabitacionMirandoEspejo_8 = "images/escenas/micasa/hab_hijPeq/personas/peq/2da/peqMirandoEspejo2daSabado/peqSuHabitacionMirandoEspejo_8.webp"
    image peqSuHabitacionMirandoEspejo_9 = "images/escenas/micasa/hab_hijPeq/personas/peq/2da/peqMirandoEspejo2daSabado/peqSuHabitacionMirandoEspejo_9.webp"

    #Domingo 1ra
    image peqEnSuHabitacionSentadaCama_0 = "images/escenas/micasa/hab_hijPeq/personas/peq/1ra/peqSentadaCama1raDomingo/peqEnSuHabitacionSentadaCama_0.webp"
    image peqEnSuHabitacionSentadaCama_1 = "images/escenas/micasa/hab_hijPeq/personas/peq/1ra/peqSentadaCama1raDomingo/peqEnSuHabitacionSentadaCama_1.webp"
    image peqEnSuHabitacionSentadaCama_2 = "images/escenas/micasa/hab_hijPeq/personas/peq/1ra/peqSentadaCama1raDomingo/peqEnSuHabitacionSentadaCama_2.webp"
    image peqEnSuHabitacionSentadaCama_3 = "images/escenas/micasa/hab_hijPeq/personas/peq/1ra/peqSentadaCama1raDomingo/peqEnSuHabitacionSentadaCama_3.webp"
    image peqEnSuHabitacionSentadaCama_4 = "images/escenas/micasa/hab_hijPeq/personas/peq/1ra/peqSentadaCama1raDomingo/peqEnSuHabitacionSentadaCama_4.webp"
    image peqEnSuHabitacionSentadaCama_5 = "images/escenas/micasa/hab_hijPeq/personas/peq/1ra/peqSentadaCama1raDomingo/peqEnSuHabitacionSentadaCama_5.webp"
    image peqEnSuHabitacionSentadaCama_6 = "images/escenas/micasa/hab_hijPeq/personas/peq/1ra/peqSentadaCama1raDomingo/peqEnSuHabitacionSentadaCama_6.webp"
    image peqEnSuHabitacionSentadaCama_7 = "images/escenas/micasa/hab_hijPeq/personas/peq/1ra/peqSentadaCama1raDomingo/peqEnSuHabitacionSentadaCama_7.webp"
    image peqEnSuHabitacionSentadaCama_8 = "images/escenas/micasa/hab_hijPeq/personas/peq/1ra/peqSentadaCama1raDomingo/peqEnSuHabitacionSentadaCama_8.webp"
    image peqEnSuHabitacionSentadaCama_9 = "images/escenas/micasa/hab_hijPeq/personas/peq/1ra/peqSentadaCama1raDomingo/peqEnSuHabitacionSentadaCama_9.png"

    #AnimarPeqDomingo
    image peqAnimarSuHabitacion_0 = "images/escenas/micasa/hab_hijPeq/personas/peq/1ra/peqSentadaCama1raDomingo/animarPeqDomingo/peqAnimarSuHabitacion_0.webp"
    image peqAnimarSuHabitacion_1 = "images/escenas/micasa/hab_hijPeq/personas/peq/1ra/peqSentadaCama1raDomingo/animarPeqDomingo/peqAnimarSuHabitacion_1.webp"
    image peqAnimarSuHabitacion_2 = "images/escenas/micasa/hab_hijPeq/personas/peq/1ra/peqSentadaCama1raDomingo/animarPeqDomingo/peqAnimarSuHabitacion_2.webp"
    image peqAnimarSuHabitacion_3 = "images/escenas/micasa/hab_hijPeq/personas/peq/1ra/peqSentadaCama1raDomingo/animarPeqDomingo/peqAnimarSuHabitacion_3.webp"


    #DiarioPersonalPeq
    image verDiarioPeq_1 = "images/escenas/micasa/hab_hijPeq/personas/Peq/diario/verDiarioPeq_1.png"
    image verDiarioPeq_2 = "images/escenas/micasa/hab_hijPeq/personas/Peq/diario/verDiarioPeq_2.png"

    image peqEscribeDiarioPersonal_0 = "images/escenas/micasa/events/habPeq/diarioPersonalPeq/peqEscribeDiarioPersonal_0.webp"
    image peqEscribeDiarioPersonal_1 = "images/escenas/micasa/events/habPeq/diarioPersonalPeq/peqEscribeDiarioPersonal_1.webp"
    image peqEscribeDiarioPersonal_2 = "images/escenas/micasa/events/habPeq/diarioPersonalPeq/peqEscribeDiarioPersonal_2.webp"
    image peqEscribeDiarioPersonal_3 = "images/escenas/micasa/events/habPeq/diarioPersonalPeq/peqEscribeDiarioPersonal_3.webp"
    image peqEscribeDiarioPersonal_4 = "images/escenas/micasa/events/habPeq/diarioPersonalPeq/peqEscribeDiarioPersonal_4.webp"
    image peqEscribeDiarioPersonal_5 = "images/escenas/micasa/events/habPeq/diarioPersonalPeq/peqEscribeDiarioPersonal_5.webp"

    #PeqTocandoseComplejo
    image eventPeqSuHabitacionMirandoEspejo_0 = "images/escenas/micasa/events/habPeq/peqTocandoseComplejo/eventPeqSuHabitacionMirandoEspejo_0.webp"
    image eventPeqSuHabitacionMirandoEspejo_1 = "images/escenas/micasa/events/habPeq/peqTocandoseComplejo/eventPeqSuHabitacionMirandoEspejo_1.webp"
    image eventPeqSuHabitacionMirandoEspejo_2 = "images/escenas/micasa/events/habPeq/peqTocandoseComplejo/eventPeqSuHabitacionMirandoEspejo_2.webp"
    image eventPeqSuHabitacionMirandoEspejo_3 = "images/escenas/micasa/events/habPeq/peqTocandoseComplejo/eventPeqSuHabitacionMirandoEspejo_3.webp"
    image eventPeqSuHabitacionMirandoEspejo_4 = "images/escenas/micasa/events/habPeq/peqTocandoseComplejo/eventPeqSuHabitacionMirandoEspejo_4.webp"
    image eventPeqSuHabitacionMirandoEspejo_5 = "images/escenas/micasa/events/habPeq/peqTocandoseComplejo/eventPeqSuHabitacionMirandoEspejo_5.webp"
    image eventPeqSuHabitacionMirandoEspejo_6 = "images/escenas/micasa/events/habPeq/peqTocandoseComplejo/eventPeqSuHabitacionMirandoEspejo_6.webp"
    image eventPeqSuHabitacionMirandoEspejo_7 = "images/escenas/micasa/events/habPeq/peqTocandoseComplejo/eventPeqSuHabitacionMirandoEspejo_7.webp"
    image eventPeqSuHabitacionMirandoEspejo_8 = "images/escenas/micasa/events/habPeq/peqTocandoseComplejo/eventPeqSuHabitacionMirandoEspejo_8.webp"
    image eventPeqSuHabitacionMirandoEspejo_9 = "images/escenas/micasa/events/habPeq/peqTocandoseComplejo/eventPeqSuHabitacionMirandoEspejo_9.webp"
    image eventPeqSuHabitacionMirandoEspejo_10 = "images/escenas/micasa/events/habPeq/peqTocandoseComplejo/eventPeqSuHabitacionMirandoEspejo_10.webp"
    image eventPeqSuHabitacionMirandoEspejo_11 = "images/escenas/micasa/events/habPeq/peqTocandoseComplejo/eventPeqSuHabitacionMirandoEspejo_11.webp"
    image eventPeqSuHabitacionMirandoEspejo_12 = "images/escenas/micasa/events/habPeq/peqTocandoseComplejo/eventPeqSuHabitacionMirandoEspejo_12.webp"



    return
