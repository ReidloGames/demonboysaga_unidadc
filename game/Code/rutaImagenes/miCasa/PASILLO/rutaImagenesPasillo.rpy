label rutaImagenesPasillo():
    call rutaImagenesNoEntraHab() from _call_rutaImagenesNoEntraHab
    call rutaImagenesCambiandoseRopaChicas from _call_rutaImagenesCambiandoseRopaChicas

    image peqPasilloSaleDeSuHab_0 = "images/escenas/micasa/events/pasillo/peqSevaAlaUni/peqPasilloSaleDeSuHab_0.png"
    image peqPasilloSaleDeSuHab_Scroll_0 = "images/escenas/micasa/events/pasillo/peqSevaAlaUni/peqPasilloSaleDeSuHab_Scroll_0.png"
    image peqPasilloSaleDeSuHab_1 = "images/escenas/micasa/events/pasillo/peqSevaAlaUni/peqPasilloSaleDeSuHab_1.png"
    image peqPasilloSaleDeSuHab_2 = "images/escenas/micasa/events/pasillo/peqSevaAlaUni/peqPasilloSaleDeSuHab_2.png"
    image peqPasilloSaleDeSuHab_3 = "images/escenas/micasa/events/pasillo/peqSevaAlaUni/peqPasilloSaleDeSuHab_3.png"
    image peqPasilloSaleDeSuHab_4 = "images/escenas/micasa/events/pasillo/peqSevaAlaUni/peqPasilloSaleDeSuHab_4.png"
    image peqPasilloSaleDeSuHab_5 = "images/escenas/micasa/events/pasillo/peqSevaAlaUni/peqPasilloSaleDeSuHab_5.png"
    image peqPasilloSaleDeSuHab_6 = "images/escenas/micasa/events/pasillo/peqSevaAlaUni/peqPasilloSaleDeSuHab_6.png"
    image peqPasilloSaleDeSuHab_7 = "images/escenas/micasa/events/pasillo/peqSevaAlaUni/peqPasilloSaleDeSuHab_7.png"
    image peqPasilloSaleDeSuHab_8 = "images/escenas/micasa/events/pasillo/peqSevaAlaUni/peqPasilloSaleDeSuHab_8.png"
    image peqPasilloSaleDeSuHab_9 = "images/escenas/micasa/events/pasillo/peqSevaAlaUni/peqPasilloSaleDeSuHab_9.png"
    image peqPasilloSaleDeSuHab_10 = "images/escenas/micasa/events/pasillo/peqSevaAlaUni/peqPasilloSaleDeSuHab_10.png"
    image peqPasilloSaleDeSuHab_11 = "images/escenas/micasa/events/pasillo/peqSevaAlaUni/peqPasilloSaleDeSuHab_11.png"
    image peqPasilloSaleDeSuHab_12 = "images/escenas/micasa/events/pasillo/peqSevaAlaUni/peqPasilloSaleDeSuHab_12.png"
    image peqPasilloSaleDeSuHab_13 = "images/escenas/micasa/events/pasillo/peqSevaAlaUni/peqPasilloSaleDeSuHab_13.png"
    image peqPasilloSaleDeSuHab_14 = "images/escenas/micasa/events/pasillo/peqSevaAlaUni/peqPasilloSaleDeSuHab_14.png"
    image peqPasilloSaleDeSuHab_15 = "images/escenas/micasa/events/pasillo/peqSevaAlaUni/peqPasilloSaleDeSuHab_15.png"
    image peqPasilloSaleDeSuHab_16 = "images/escenas/micasa/events/pasillo/peqSevaAlaUni/peqPasilloSaleDeSuHab_16.png"
    image peqPasilloSaleDeSuHab_17 = "images/escenas/micasa/events/pasillo/peqSevaAlaUni/peqPasilloSaleDeSuHab_17.png"

    image peqPasilloEncuentroPasilloAlicia_0 = "images/escenas/micasa/events/pasillo/peqPreguntaEncuentroAlicia/peqPasilloEncuentroPasilloAlicia_0.png"
    image peqPasilloEncuentroPasilloAlicia_1 = "images/escenas/micasa/events/pasillo/peqPreguntaEncuentroAlicia/peqPasilloEncuentroPasilloAlicia_1.png"
    image peqPasilloEncuentroPasilloAlicia_2 = "images/escenas/micasa/events/pasillo/peqPreguntaEncuentroAlicia/peqPasilloEncuentroPasilloAlicia_2.png"
    image peqPasilloEncuentroPasilloAlicia_3 = "images/escenas/micasa/events/pasillo/peqPreguntaEncuentroAlicia/peqPasilloEncuentroPasilloAlicia_3.png"
    image peqPasilloEncuentroPasilloAlicia_4 = "images/escenas/micasa/events/pasillo/peqPreguntaEncuentroAlicia/peqPasilloEncuentroPasilloAlicia_4.png"
    image peqPasilloEncuentroPasilloAlicia_5 = "images/escenas/micasa/events/pasillo/peqPreguntaEncuentroAlicia/peqPasilloEncuentroPasilloAlicia_5.png"
    image peqPasilloEncuentroPasilloAlicia_6 = "images/escenas/micasa/events/pasillo/peqPreguntaEncuentroAlicia/peqPasilloEncuentroPasilloAlicia_6.png"
    image peqPasilloEncuentroPasilloAlicia_7 = "images/escenas/micasa/events/pasillo/peqPreguntaEncuentroAlicia/peqPasilloEncuentroPasilloAlicia_7.png"
    image peqPasilloEncuentroPasilloAlicia_8 = "images/escenas/micasa/events/pasillo/peqPreguntaEncuentroAlicia/peqPasilloEncuentroPasilloAlicia_8.png"
    image peqPasilloEncuentroPasilloAlicia_9 = "images/escenas/micasa/events/pasillo/peqPreguntaEncuentroAlicia/peqPasilloEncuentroPasilloAlicia_9.png"
    image peqPasilloEncuentroPasilloAlicia_10 = "images/escenas/micasa/events/pasillo/peqPreguntaEncuentroAlicia/peqPasilloEncuentroPasilloAlicia_10.png"
    image peqPasilloEncuentroPasilloAlicia_11 = "images/escenas/micasa/events/pasillo/peqPreguntaEncuentroAlicia/peqPasilloEncuentroPasilloAlicia_11.png"
    image peqPasilloEncuentroPasilloAlicia_12 = "images/escenas/micasa/events/pasillo/peqPreguntaEncuentroAlicia/peqPasilloEncuentroPasilloAlicia_12.png"
    image peqPasilloEncuentroPasilloAlicia_13 = "images/escenas/micasa/events/pasillo/peqPreguntaEncuentroAlicia/peqPasilloEncuentroPasilloAlicia_13.png"
    image peqPasilloEncuentroPasilloAlicia_14 = "images/escenas/micasa/events/pasillo/peqPreguntaEncuentroAlicia/peqPasilloEncuentroPasilloAlicia_14.png"
    image peqPasilloEncuentroPasilloAlicia_15 = "images/escenas/micasa/events/pasillo/peqPreguntaEncuentroAlicia/peqPasilloEncuentroPasilloAlicia_15.png"
    image peqPasilloEncuentroPasilloAlicia_16 = "images/escenas/micasa/events/pasillo/peqPreguntaEncuentroAlicia/peqPasilloEncuentroPasilloAlicia_16.png"

    return
