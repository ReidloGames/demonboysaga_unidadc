label rutaImagenesCambiandoseRopaChicas():
    #PEQ

    #scene1
    image cambiandoseHermanPeq_0 = "images/escenas/micasa/pasillo/cambiandoseRopa/Peq/cambiandoseHermanPeq_0.png"
    image cambiandoseHermanPeq_1 = "images/escenas/micasa/pasillo/cambiandoseRopa/Peq/cambiandoseHermanPeq_1.png"

    image cambiandoseHermanPeqNum1_0 = "images/escenas/micasa/pasillo/cambiandoseRopa/Peq/cambiandoseHermanPeqNum1_0.png"
    image cambiandoseHermanPeqNum1_1 = "images/escenas/micasa/pasillo/cambiandoseRopa/Peq/cambiandoseHermanPeqNum1_1.png"
    image cambiandoseHermanPeqNum1_2 = "images/escenas/micasa/pasillo/cambiandoseRopa/Peq/cambiandoseHermanPeqNum1_2.png"
    image cambiandoseHermanPeqNum1_3 = "images/escenas/micasa/pasillo/cambiandoseRopa/Peq/cambiandoseHermanPeqNum1_3.png"
    image cambiandoseHermanPeqNum1_4 = "images/escenas/micasa/pasillo/cambiandoseRopa/Peq/cambiandoseHermanPeqNum1_4.png"
    image cambiandoseHermanPeqNum1_5 = "images/escenas/micasa/pasillo/cambiandoseRopa/Peq/cambiandoseHermanPeqNum1_5.png"
    image cambiandoseHermanPeqNum1_6 = "images/escenas/micasa/pasillo/cambiandoseRopa/Peq/cambiandoseHermanPeqNum1_6.png"
    image cambiandoseHermanPeqNum1_7 = "images/escenas/micasa/pasillo/cambiandoseRopa/Peq/cambiandoseHermanPeqNum1_7.png"
    image cambiandoseHermanPeqNum1_8 = "images/escenas/micasa/pasillo/cambiandoseRopa/Peq/cambiandoseHermanPeqNum1_8.png"


    #scene2
    image cambiandoseHermanPeqNum2_1 = "images/escenas/micasa/pasillo/cambiandoseRopa/Peq/scene2/cambiandoseHermanPeqNum2_1.png"
    image cambiandoseHermanPeqNum2_2 = "images/escenas/micasa/pasillo/cambiandoseRopa/Peq/scene2/cambiandoseHermanPeqNum2_2.png"
    image cambiandoseHermanPeqNum2_3 = "images/escenas/micasa/pasillo/cambiandoseRopa/Peq/scene2/cambiandoseHermanPeqNum2_3.png"
    image cambiandoseHermanPeqNum2_4 = "images/escenas/micasa/pasillo/cambiandoseRopa/Peq/scene2/cambiandoseHermanPeqNum2_4.png"
    image cambiandoseHermanPeqNum2_5 = "images/escenas/micasa/pasillo/cambiandoseRopa/Peq/scene2/cambiandoseHermanPeqNum2_5.png"
    image cambiandoseHermanPeqNum2_6 = "images/escenas/micasa/pasillo/cambiandoseRopa/Peq/scene2/cambiandoseHermanPeqNum2_6.png"
    image cambiandoseHermanPeqNum2_7 = "images/escenas/micasa/pasillo/cambiandoseRopa/Peq/scene2/cambiandoseHermanPeqNum2_7.png"
    image cambiandoseHermanPeqNum2_8 = "images/escenas/micasa/pasillo/cambiandoseRopa/Peq/scene2/cambiandoseHermanPeqNum2_8.png"

    #scene3
    image cambiandoseHermanPeqNum3_1 = "images/escenas/micasa/pasillo/cambiandoseRopa/Peq/scene3/cambiandoseHermanPeqNum3_1.png"
    image cambiandoseHermanPeqNum3_2 = "images/escenas/micasa/pasillo/cambiandoseRopa/Peq/scene3/cambiandoseHermanPeqNum3_2.png"
    image cambiandoseHermanPeqNum3_3 = "images/escenas/micasa/pasillo/cambiandoseRopa/Peq/scene3/cambiandoseHermanPeqNum3_3.png"
    image cambiandoseHermanPeqNum3_4 = "images/escenas/micasa/pasillo/cambiandoseRopa/Peq/scene3/cambiandoseHermanPeqNum3_4.png"
    image cambiandoseHermanPeqNum3_5 = "images/escenas/micasa/pasillo/cambiandoseRopa/Peq/scene3/cambiandoseHermanPeqNum3_5.png"
    image cambiandoseHermanPeqNum3_6 = "images/escenas/micasa/pasillo/cambiandoseRopa/Peq/scene3/cambiandoseHermanPeqNum3_6.png"



    #MED
    image cambiandoseHermanMed_0 = "images/escenas/micasa/pasillo/cambiandoseRopa/Med/cambiandoseHermanMed_0.png"
    #scene1
    image cambiandoseHermanMedNum1_0 = "images/escenas/micasa/pasillo/cambiandoseRopa/Med/scene1/cambiandoseHermanMedNum1_0.png"
    image cambiandoseHermanMedNum1_1 = "images/escenas/micasa/pasillo/cambiandoseRopa/Med/scene1/cambiandoseHermanMedNum1_1.png"
    image cambiandoseHermanMedNum1_2 = "images/escenas/micasa/pasillo/cambiandoseRopa/Med/scene1/cambiandoseHermanMedNum1_2.png"
    image cambiandoseHermanMedNum1_3 = "images/escenas/micasa/pasillo/cambiandoseRopa/Med/scene1/cambiandoseHermanMedNum1_3.png"
    image cambiandoseHermanMedNum1_4 = "images/escenas/micasa/pasillo/cambiandoseRopa/Med/scene1/cambiandoseHermanMedNum1_4.png"

    #scene2
    image cambiandoseHermanMed_2 = "images/escenas/micasa/pasillo/cambiandoseRopa/Med/scene2/cambiandoseHermanMed_2.png"
    image cambiandoseHermanMedNum2_0 = "images/escenas/micasa/pasillo/cambiandoseRopa/Med/scene2/cambiandoseHermanMedNum2_0.png"
    image cambiandoseHermanMedNum2_1 = "images/escenas/micasa/pasillo/cambiandoseRopa/Med/scene2/cambiandoseHermanMedNum2_1.png"
    image cambiandoseHermanMedNum2_2 = "images/escenas/micasa/pasillo/cambiandoseRopa/Med/scene2/cambiandoseHermanMedNum2_2.png"
    image cambiandoseHermanMedNum2_3 = "images/escenas/micasa/pasillo/cambiandoseRopa/Med/scene2/cambiandoseHermanMedNum2_3.png"

    image cambiandoseHermanMedNum2_5 = "images/escenas/micasa/pasillo/cambiandoseRopa/Med/scene2/cambiandoseHermanMedNum2_5.png"
    image cambiandoseHermanMedNum2_6 = "images/escenas/micasa/pasillo/cambiandoseRopa/Med/scene2/cambiandoseHermanMedNum2_6.png"

    #scene3
    image cambiandoseHermanMedNum3_0 = "images/escenas/micasa/pasillo/cambiandoseRopa/Med/scene3/cambiandoseHermanMedNum3_0.png"
    image cambiandoseHermanMedNum3_1 = "images/escenas/micasa/pasillo/cambiandoseRopa/Med/scene3/cambiandoseHermanMedNum3_1.png"
    image cambiandoseHermanMedNum3_2 = "images/escenas/micasa/pasillo/cambiandoseRopa/Med/scene3/cambiandoseHermanMedNum3_2.png"
    image cambiandoseHermanMedNum3_3 = "images/escenas/micasa/pasillo/cambiandoseRopa/Med/scene3/cambiandoseHermanMedNum3_3.png"
    image cambiandoseHermanMedNum3_4 = "images/escenas/micasa/pasillo/cambiandoseRopa/Med/scene3/cambiandoseHermanMedNum3_4.png"
    image cambiandoseHermanMedNum3_5 = "images/escenas/micasa/pasillo/cambiandoseRopa/Med/scene3/cambiandoseHermanMedNum3_5.png"
    image cambiandoseHermanMedNum3_6 = "images/escenas/micasa/pasillo/cambiandoseRopa/Med/scene3/cambiandoseHermanMedNum3_6.png"


    #MAY
    image cambiandoseHermanMay_0 = "images/escenas/micasa/pasillo/cambiandoseRopa/May/scene1/cambiandoseHermanMay_0.png"
    #scene1
    image cambiandoseHermanMayNum1_0 = "images/escenas/micasa/pasillo/cambiandoseRopa/May/scene1/cambiandoseHermanMayNum1_0.png"
    image cambiandoseHermanMayNum1_1 = "images/escenas/micasa/pasillo/cambiandoseRopa/May/scene1/cambiandoseHermanMayNum1_1.png"
    image cambiandoseHermanMayNum1_2 = "images/escenas/micasa/pasillo/cambiandoseRopa/May/scene1/cambiandoseHermanMayNum1_2.png"
    image cambiandoseHermanMayNum1_3 = "images/escenas/micasa/pasillo/cambiandoseRopa/May/scene1/cambiandoseHermanMayNum1_3.png"
    image cambiandoseHermanMayNum1_4 = "images/escenas/micasa/pasillo/cambiandoseRopa/May/scene1/cambiandoseHermanMayNum1_4.png"

    #scene2
    image cambiandoseHermanMayNum2_0 = "images/escenas/micasa/pasillo/cambiandoseRopa/May/scene2/cambiandoseHermanMayNum2_0.png"
    image cambiandoseHermanMayNum2_1 = "images/escenas/micasa/pasillo/cambiandoseRopa/May/scene2/cambiandoseHermanMayNum2_1.png"
    image cambiandoseHermanMayNum2_2 = "images/escenas/micasa/pasillo/cambiandoseRopa/May/scene2/cambiandoseHermanMayNum2_2.png"
    image cambiandoseHermanMayNum2_3 = "images/escenas/micasa/pasillo/cambiandoseRopa/May/scene2/cambiandoseHermanMayNum2_3.png"

    #scene3
    image cambiandoseHermanMayNum3_0 = "images/escenas/micasa/pasillo/cambiandoseRopa/May/scene3/cambiandoseHermanMayNum3_0.png"
    image cambiandoseHermanMayNum3_1 = "images/escenas/micasa/pasillo/cambiandoseRopa/May/scene3/cambiandoseHermanMayNum3_1.png"
    image cambiandoseHermanMayNum3_2 = "images/escenas/micasa/pasillo/cambiandoseRopa/May/scene3/cambiandoseHermanMayNum3_2.png"
    image cambiandoseHermanMayNum3_3 = "images/escenas/micasa/pasillo/cambiandoseRopa/May/scene3/cambiandoseHermanMayNum3_3.png"
    image cambiandoseHermanMayNum3_4 = "images/escenas/micasa/pasillo/cambiandoseRopa/May/scene3/cambiandoseHermanMayNum3_4.png"


    return
