label rutaImagenesGaleriaGym():
    call rutaImagenesGaleriaGymMad() from _call_rutaImagenesGaleriaGymMad
    call rutaImagenesGaleriaGymPeq() from _call_rutaImagenesGaleriaGymPeq
    call rutaImagenesGaleriaGymMay() from _call_rutaImagenesGaleriaGymMay
    #Mad
    image madYoga1 = "images/escenas/micasa/galeriaGym/personas/Mad/1ra/madYoga.png"
    image madYoga1_S = "images/escenas/micasa/galeriaGym/personas/Mad/1ra/madYoga_S.png"

    image madLavadero1 = "images/escenas/micasa/galeriaGym/personas/Mad/1ra/madLavadero.png"
    image madLavadero1_S = "images/escenas/micasa/galeriaGym/personas/Mad/1ra/madLavadero_S.png"

    #Peq
    image peqHaciendoGym1 = "images/escenas/micasa/galeriaGym/personas/Peq/3ra/peqHaciendoGym1.png"
    image peqHaciendoGym1_S = "images/escenas/micasa/galeriaGym/personas/Peq/3ra/peqHaciendoGym1_S.png"

    #May
    image mayHaciendoGym1 = "images/escenas/micasa/galeriaGym/personas/May/1ra/mayHaciendoGym1.png"
    image mayHaciendoGym1_S = "images/escenas/micasa/galeriaGym/personas/May/1ra/mayHaciendoGym1_S.png"

    return
