label rutaImagenesSalon():
    call rutaImagenesSalonProta() from _call_rutaImagenesSalonProta
    call rutaImagenesSalonMad() from _call_rutaImagenesSalonMad
    call rutaImagenesSalonPad() from _call_rutaImagenesSalonPad
    call rutaImagenesSalonPeq() from _call_rutaImagenesSalonPeq
    call rutaImagenesSalonMed() from _call_rutaImagenesSalonMed
    call rutaImagenesSalonMay() from _call_rutaImagenesSalonMay
    #Mad
    image madSofaTv1 = "images/escenas/micasa/salon/personas/Mad/4ta/madSofaTv1.png"
    image madSofaTv1_S = "images/escenas/micasa/salon/personas/Mad/4ta/madSofaTv1_S.png"

    image leonTv1 = "images/escenas/micasa/salon/personas/Mad/4ta/leonTv.png"
    image leonTv2 = "images/escenas/micasa/salon/personas/Mad/4ta/leonTv1.png"

    image madMirandoRevista1 = "images/escenas/micasa/salon/personas/Mad/4ta/madMirandoRevista.png"
    image madMirandoRevista1_S = "images/escenas/micasa/salon/personas/Mad/4ta/madMirandoRevista_S.png"

    #Pad
    image padSentadoSofa1 = "images/escenas/micasa/salon/personas/Pad/3ra/padSentadoSofa1.png"
    image padSentadoSofa1_S = "images/escenas/micasa/salon/personas/Pad/3ra/padSentadoSofa1_S.png"

    image tvDeporte1 = "images/escenas/micasa/salon/personas/Pad/3ra/tvDeportes1.png"
    image tvDeporte2 = "images/escenas/micasa/salon/personas/Pad/3ra/tvDeportes2.png"

    #Peq
    image sofaEstirada1 = "images/escenas/micasa/salon/personas/Peq/4ta/sofaEstirada1.png"
    image sofaEstirada1_S = "images/escenas/micasa/salon/personas/Peq/4ta/sofaEstirada1_S.png"

    #Med
    image mirandoMovil1 = "images/escenas/micasa/salon/personas/Med/4ta/mirandoMovil1.png"
    image mirandoMovil1_S = "images/escenas/micasa/salon/personas/Med/4ta/mirandoMovil1_S.png"

    image mirandoMovil1ConPeq = "images/escenas/micasa/salon/personas/Med/4ta/mirandoMovil1ConPeq.png"
    image mirandoMovil1ConPeq_S = "images/escenas/micasa/salon/personas/Med/4ta/mirandoMovil1ConPeq_S.png"

    #May
    image mayPintaUnyas1 = "images/escenas/micasa/salon/personas/May/4ta/mayPintaUnyas1.png"
    image mayPintaUnyas1_S = "images/escenas/micasa/salon/personas/May/4ta/mayPintaUnyas1_S.png"

    #Familia
    image familia4_1 = "images/escenas/micasa/salon/personas/familia/4ta/familia4_1.png"
    image familia4_1_S = "images/escenas/micasa/salon/personas/familia/4ta/familia4_1_S.png"
    image familia5_1 = "images/escenas/micasa/salon/personas/familia/4ta/familia5_1.png"
    image familia5_1_S = "images/escenas/micasa/salon/personas/familia/4ta/familia5_1_S.png"



    return
