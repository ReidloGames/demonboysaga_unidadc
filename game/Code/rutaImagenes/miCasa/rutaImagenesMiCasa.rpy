
label rutaImgMicasa:
    #Habitacion Prota
    image habProtaSgD = "images/escenas/micasa/hab_prota/hab_ProtaSinGente_D.png"
    image habProtaSgN = "images/escenas/micasa/hab_prota/hab_ProtaSinGente_N.png"
    image habProtaS = "images/escenas/micasa/hab_prota/hab_Prota_S.png"
    image habProtaSinCesta = "images/escenas/micasa/hab_prota/habProtaSinCesta.png"


    image protaDorm_D = "images/escenas/micasa/hab_prota/protaTumbadoCama_D.png"
    image protaDorm_N = "images/escenas/micasa/hab_prota/protaTumbadoCama_N.png"

    #Pasillos3puertas
    image pasillo2 = "images/escenas/micasa/pasillo/pasillo_3_puertas.png"
    image pasillo2S = "images/escenas/micasa/pasillo/pasillo_3_puertasS.png"
    image puertaHabPeq = "images/escenas/micasa/pasillo/puertaHabPeq.png"
    image puertaHabMed = "images/escenas/micasa/pasillo/puertaHabMed.png"
    image puertaHabMay = "images/escenas/micasa/pasillo/puertaHabMay.png"

    #HabHermPeq
    image habHijPeqSgD = "images/escenas/micasa/hab_hijPeq/hab_hPequenaSinGente_D.png"
    image habHijPeqSgN = "images/escenas/micasa/hab_hijPeq/hab_hPequenaSinGente_N.png"
    image habHijPeqS = "images/escenas/micasa/hab_hijPeq/hab_hPequena_S.png"
    image habHijPeqCon0D = "images/escenas/micasa/hab_hijPeq/hab_hPequenaCon_0D.png"
    #HabHermMed
    image habHijMedSgD = "images/escenas/micasa/hab_hijMed/hab_hMedianaSinGente_D.png"
    image habHijMedSgN = "images/escenas/micasa/hab_hijMed/hab_hMedianaSinGente_N.png"
    image habHijMedS = "images/escenas/micasa/hab_hijMed/hab_hMediana_S.png"
    #HabHermMay
    image habHijMaySgD = "images/escenas/micasa/hab_hijMay/hab_hMayorSinGente_D.png"
    image habHijMaySgN = "images/escenas/micasa/hab_hijMay/hab_hMayorSinGente_N.png"
    image habHijMayS = "images/escenas/micasa/hab_hijMay/hab_hMayor_S.png"

    #HabPadr
    image habPadrSgD = "images/escenas/micasa/hab_Padr/hab_padresSinGente_D.png"
    image habPadrSgN = "images/escenas/micasa/hab_Padr/hab_padresSinGente_N.png"
    image habPadrS = "images/escenas/micasa/hab_Padr/hab_padres_S.png"

    #Pasillos2puertas
    image pasillo1 = "images/escenas/micasa/pasillo/pasillo_2_puertas.png"
    image pasillo1S = "images/escenas/micasa/pasillo/pasillo_2_puertasS.png"
    #GaleriaGym
    image galeriaGym = "images/escenas/micasa/galeriaGym/galeria.png"
    image galeriaGym_S = "images/escenas/micasa/galeriaGym/galeria_S.png"

    #Comedor
    image comedor = "images/escenas/micasa/comedor/salon_comedor.png"
    image comedor_S = "images/escenas/micasa/comedor/salon_comedor_S.png"
    image puertaBanyo = "images/escenas/micasa/comedor/puertaBanyo.png"
    image puertaPadr = "images/escenas/micasa/comedor/puertaPadr.png"

    #Salon
    image salon = "images/escenas/micasa/salon/salon.png"
    image salon_S = "images/escenas/micasa/salon/salon_S.png"
    #Objetos
    image caja = "images/escenas/micasa/salon/objects/caja.png"


    #Cocina
    image cocina = "images/escenas/micasa/cocina/cocina.png"
    image cocina_S = "images/escenas/micasa/cocina/cocina_S.png"
    #Baño
    image banyo = "images/escenas/micasa/banyo/banyo.png"
    image banyo_S = "images/escenas/micasa/banyo/banyo_S.png"

    #EntradaCasa
    image entradaCasaSgD = "images/escenas/micasa/entradaCasa/entradaCasaSinGente_D.png"
    image entradaCasaSgN = "images/escenas/micasa/entradaCasa/entradaCasaSinGente_N.png"
    image entradaCasaS = "images/escenas/micasa/entradaCasa/entradaCasa_S.png"
    #Jardin
    image jardinPisSgD = "images/escenas/micasa/jardin/jardin_piscinaSinGente_D.png"
    image jardinPisSgN = "images/escenas/micasa/jardin/jardin_piscinaSinGente_N.png"
    image jardinPisS = "images/escenas/micasa/jardin/jardin_piscina_S.png"
    #Garage
    image garageSg = "images/escenas/micasa/garage/garageSinGente.png"
    image garageSinBiciSg = "images/escenas/micasa/garage/garageSinBici.png"
    image garage_S = "images/escenas/micasa/garage/garage_S.png"

    call rutaImagenesMiHabitacion() from _call_rutaImagenesMiHabitacion
    call rutaImagenesCocina() from _call_rutaImagenesCocina
    call rutaImagenesSalon() from _call_rutaImagenesSalon
    call rutaImagenesComedor() from _call_rutaImagenesComedor
    call rutaImagenesGaleriaGym() from _call_rutaImagenesGaleriaGym
    call rutaImagenesHabPadr() from _call_rutaImagenesHabPadr
    call rutaImagenesJardinPis() from _call_rutaImagenesJardinPis
    call rutaImagenesHabPeq() from _call_rutaImagenesHabPeq
    call rutaImagenesHabMed() from _call_rutaImagenesHabMed
    call rutaImagenesPasillo() from _call_rutaImagenesPasillo


    return
