label rutaImagenesCocina():
    call rutaImagenesCocinaMad() from _call_rutaImagenesCocinaMad
    call rutaImagenesCocinaPeq() from _call_rutaImagenesCocinaPeq
    call rutaImagenesCocinaMay() from _call_rutaImagenesCocinaMay

    #Mad
    image mad1 = "images/escenas/micasa/cocina/personas/Mad/3ra/mad1.png"
    image mad1_S = "images/escenas/micasa/cocina/personas/Mad/3ra/mad1_S.png"

    image madLimpiaCocina1 = "images/escenas/micasa/cocina/personas/Mad/3ra/madLimpia1.png"
    image madLimpiaCocina1_S = "images/escenas/micasa/cocina/personas/Mad/3ra/madLimpia1_S.png"

    image madDarMamar1 = "images/escenas/micasa/cocina/personas/Mad/3ra/madDarMamar1.png"
    image madDarMamar1_S = "images/escenas/micasa/cocina/personas/Mad/3ra/madDarMamar1_S.png"

    image madDesayuno1 = "images/escenas/micasa/cocina/personas/Mad/1ra/madDesayuno.png"
    image madDesayuno1_S = "images/escenas/micasa/cocina/personas/Mad/1ra/madDesayuno_S.png"

    #Peq
    image PeqHaciendoComida1 = "images/escenas/micasa/cocina/personas/Peq/3ra/PeqHaciendoComida1.png"
    image PeqHaciendoComida1_S = "images/escenas/micasa/cocina/personas/Peq/3ra/PeqHaciendoComida1_S.png"

    #May
    image MayCocinaMicro1 = "images/escenas/micasa/cocina/personas/May/1ra/MayCocinaMicro1.png"
    image MayCocinaMicro1_S = "images/escenas/micasa/cocina/personas/May/1ra/MayCocinaMicro1_S.png"


return
