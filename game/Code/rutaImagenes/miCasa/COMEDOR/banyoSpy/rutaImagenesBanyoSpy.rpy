label rutaImagenesBanyoSpy():
    image spyBanyoEntra = "images/escenas/micasa/comedor/banyoSpy/spyBanyoEntra.png"

    #PEQ
    #scene1
    image spyBanyoHermanPeqNum1_0 = "images/escenas/micasa/comedor/banyoSpy/hermanPeq/escena1/spyBanyoHermanPeqNum1_0.webp"
    image spyBanyoHermanPeqNum1_1 = "images/escenas/micasa/comedor/banyoSpy/hermanPeq/escena1/spyBanyoHermanPeqNum1_1.webp"
    image spyBanyoHermanPeqNum1_2 = "images/escenas/micasa/comedor/banyoSpy/hermanPeq/escena1/spyBanyoHermanPeqNum1_2.webp"

    #scene2
    image spyBanyoHermanPeqNum2_0 = "images/escenas/micasa/comedor/banyoSpy/hermanPeq/escena2/spyBanyoHermanPeqNum2_0.webp"
    image spyBanyoHermanPeqNum2_1 = "images/escenas/micasa/comedor/banyoSpy/hermanPeq/escena2/spyBanyoHermanPeqNum2_1.webp"
    image spyBanyoHermanPeqNum2_2 = "images/escenas/micasa/comedor/banyoSpy/hermanPeq/escena2/spyBanyoHermanPeqNum2_2.webp"
    image spyBanyoHermanPeqNum2_3 = "images/escenas/micasa/comedor/banyoSpy/hermanPeq/escena2/spyBanyoHermanPeqNum2_3.webp"
    image spyBanyoHermanPeqNum2_4 = "images/escenas/micasa/comedor/banyoSpy/hermanPeq/escena2/spyBanyoHermanPeqNum2_4.webp"



    #MED
    #scene1
    image spyBanyoHermanMedNum1_0 = "images/escenas/micasa/comedor/banyoSpy/hermanMed/escena1/spyBanyoHermanMedNum1_0.webp"
    image spyBanyoHermanMedNum1_1 = "images/escenas/micasa/comedor/banyoSpy/hermanMed/escena1/spyBanyoHermanMedNum1_1.webp"
    image spyBanyoHermanMedNum1_2 = "images/escenas/micasa/comedor/banyoSpy/hermanMed/escena1/spyBanyoHermanMedNum1_2.webp"
    image spyBanyoHermanMedNum1_3 = "images/escenas/micasa/comedor/banyoSpy/hermanMed/escena1/spyBanyoHermanMedNum1_3.webp"

    #scene2
    image spyBanyoHermanMedNum2_0 = "images/escenas/micasa/comedor/banyoSpy/hermanMed/escena2/spyBanyoHermanMedNum2_0.webp"
    image spyBanyoHermanMedNum2_1 = "images/escenas/micasa/comedor/banyoSpy/hermanMed/escena2/spyBanyoHermanMedNum2_1.webp"
    image spyBanyoHermanMedNum2_2 = "images/escenas/micasa/comedor/banyoSpy/hermanMed/escena2/spyBanyoHermanMedNum2_2.webp"
    image spyBanyoHermanMedNum2_3 = "images/escenas/micasa/comedor/banyoSpy/hermanMed/escena2/spyBanyoHermanMedNum2_3.webp"



    #MAY
    #scene1
    image spyBanyoHermanMayNum1_0 = "images/escenas/micasa/comedor/banyoSpy/hermanMay/escena1/spyBanyoHermanMayNum1_0.webp"
    image spyBanyoHermanMayNum1_1 = "images/escenas/micasa/comedor/banyoSpy/hermanMay/escena1/spyBanyoHermanMayNum1_1.webp"
    image spyBanyoHermanMayNum1_2 = "images/escenas/micasa/comedor/banyoSpy/hermanMay/escena1/spyBanyoHermanMayNum1_2.webp"
    image spyBanyoHermanMayNum1_3 = "images/escenas/micasa/comedor/banyoSpy/hermanMay/escena1/spyBanyoHermanMayNum1_3.webp"

    #scene2
    image spyBanyoHermanMayNum2_0 = "images/escenas/micasa/comedor/banyoSpy/hermanMay/escena2/spyBanyoHermanMayNum2_0.webp"
    image spyBanyoHermanMayNum2_1 = "images/escenas/micasa/comedor/banyoSpy/hermanMay/escena2/spyBanyoHermanMayNum2_1.webp"
    image spyBanyoHermanMayNum2_2 = "images/escenas/micasa/comedor/banyoSpy/hermanMay/escena2/spyBanyoHermanMayNum2_2.webp"
    image spyBanyoHermanMayNum2_3 = "images/escenas/micasa/comedor/banyoSpy/hermanMay/escena2/spyBanyoHermanMayNum2_3.webp"


    #MAD
    #scene1
    image spyBanyoMadNum1_0 = "images/escenas/micasa/comedor/banyoSpy/mad/escena1/spyBanyoMadNum1_0.webp"
    image spyBanyoMadNum1_1 = "images/escenas/micasa/comedor/banyoSpy/mad/escena1/spyBanyoMadNum1_1.webp"
    image spyBanyoMadNum1_2 = "images/escenas/micasa/comedor/banyoSpy/mad/escena1/spyBanyoMadNum1_2.webp"
    image spyBanyoMadNum1_3 = "images/escenas/micasa/comedor/banyoSpy/mad/escena1/spyBanyoMadNum1_3.webp"
    image spyBanyoMadNum1_4 = "images/escenas/micasa/comedor/banyoSpy/mad/escena1/spyBanyoMadNum1_4.webp"
    image spyBanyoMadNum1_5 = "images/escenas/micasa/comedor/banyoSpy/mad/escena1/spyBanyoMadNum1_5.webp"
    image spyBanyoMadNum1_6 = "images/escenas/micasa/comedor/banyoSpy/mad/escena1/spyBanyoMadNum1_6.webp"

    #scene2
    image spyBanyoMadNum2_0 = "images/escenas/micasa/comedor/banyoSpy/mad/escena2/spyBanyoMadNum2_0.webp"
    image spyBanyoMadNum2_1 = "images/escenas/micasa/comedor/banyoSpy/mad/escena2/spyBanyoMadNum2_1.webp"
    image spyBanyoMadNum2_2 = "images/escenas/micasa/comedor/banyoSpy/mad/escena2/spyBanyoMadNum2_2.webp"
    image spyBanyoMadNum2_3 = "images/escenas/micasa/comedor/banyoSpy/mad/escena2/spyBanyoMadNum2_3.webp"

    return
