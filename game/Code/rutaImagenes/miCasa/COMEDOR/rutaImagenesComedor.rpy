label rutaImagenesComedor():
    call rutaImagenesComedorMed() from _call_rutaImagenesComedorMed
    call rutaImagenesSaludaAPad() from _call_rutaImagenesSaludaAPad
    call queBienHuele() from _call_queBienHuele
    call padComunicaProblema() from _call_padComunicaProblema
    call rutaImagenesCambiandoseRopaMad() from _call_rutaImagenesCambiandoseRopaMad

    image madRevista1 = "images/escenas/micasa/comedor/personas/Mad/4ta/madRevista1.png"
    image madRevista1_S = "images/escenas/micasa/comedor/personas/Mad/4ta/madRevista1_S.png"

    image madLimpia1 = "images/escenas/micasa/comedor/personas/Mad/1ra/madLimpia1.png"
    image madLimpia1_S = "images/escenas/micasa/comedor/personas/Mad/1ra/madLimpia1_S.png"

    image medEstudiandoComedor1 = "images/escenas/micasa/comedor/personas/Med/3ra/medEstudiandoComedor1.png"
    image medEstudiandoComedor1_S = "images/escenas/micasa/comedor/personas/Med/3ra/medEstudiandoComedor1_S.png"


    return
