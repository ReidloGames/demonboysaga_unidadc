label rutaImagenesGame():
    # Imagenes Fondos
    #image adultImage1 = "images/adultImage1.png"

    #titulo menu juego
    image titleGame = im.Scale("gui/titleGame.png",500,50)

    # Imagenes Hud
    image hudPlantilla = "images/hud/hud.png"
    #image mapa = im.Scale("images/hud/map.png",60,60)
    image movilHud = im.Scale("images/hud/movil.png",34,60)
    image movilHud_S = im.Scale("images/hud/movil_S.png",34,60)
    image amanecer = im.Scale("images/hud/amanecer.png",49,44)
    image manyana = im.Scale("images/hud/manyana.png",49,44)
    image mediodia = im.Scale("images/hud/mediodia.png",49,44)
    image tarde = im.Scale("images/hud/tarde.png",49,44)
    image noche = im.Scale("images/hud/noche.png",49,44)
    image amanecerdis = im.Scale("images/hud/amanecerdisabled.png",49,44)
    image manyanadis = im.Scale("images/hud/manyanadisabled.png",49,44)
    image mediodiadis = im.Scale("images/hud/mediodiadisabled.png",49,44)
    image tardedis = im.Scale("images/hud/tardedisabled.png",49,44)
    image nochedis = im.Scale("images/hud/nochedisabled.png",49,44)
    image reloj = im.Scale("images/hud/reloj.png",50,44)
    image relojh = im.Scale("images/hud/relojh.png",50,44)
    image relojbn = im.Scale("images/hud/relojbn.png",50,44)
    image mochila = im.Scale("images/hud/mochila.png",55,60)
    image mochila_S = im.Scale("images/hud/mochila_S.png",55,60)
    image genteCasa = im.Scale("images/hud/genteCasa.png",50,55)
    image genteCasa_S = im.Scale("images/hud/genteCasa_S.png",50,55)
    image volverMiHabitacion = im.Scale("images/hud/volverMiHabitacion.png",50,55)
    image volverMiHabitacion_S = im.Scale("images/hud/volverMiHabitacion_S.png",50,55)
    image volverMiHabitacion_Dis = im.Scale("images/hud/volverMiHabitacion_Dis.png",50,55)

    #Mapa MiCasa gente
    image mapGenteCasa = "images/escenas/mapaGenteMiCasa/mapaGenteMiCasa.png"
    image mapGenteCasa_S = "images/escenas/mapaGenteMiCasa/mapaGenteMiCasa_S.png"
    #Movil Escenas
    image fondoEscenas = "images/fondoEscenas.png"
    image marcoEscenas = "images/marcoEscenas.png"

    #Escenas
    #Mad
    image escena1Mad = "images/reproductorEscenas/escenaSofia1.png"
    image escena1Mad_S = "images/reproductorEscenas/escenaSofia1_S.png"
    image escena1MadCris = "images/reproductorEscenas/escenaSofia1_cris.png"

    #MayKara
    image escena1May = "images/reproductorEscenas/escenaKara1.png"
    image escena1May_S = "images/reproductorEscenas/escenaKara1_S.png"
    image escena1MayCris = "images/reproductorEscenas/escenaKara1_cris.png"

    #MedErica
    image escena1Med = "images/reproductorEscenas/escenaErica1.png"
    image escena1Med_S = "images/reproductorEscenas/escenaErica1_S.png"
    image escena1MedCris = "images/reproductorEscenas/escenaErica1_cris.png"

    #Claire
    image escena1Claire = "images/reproductorEscenas/escenaClaire1.png"
    image escena1Claire_S = "images/reproductorEscenas/escenaClaire1_S.png"
    image escena1ClaireCris = "images/reproductorEscenas/escenaClaire1_cris.png"

    image escena2Claire = "images/reproductorEscenas/escenaClaire2.png"
    image escena2Claire_S = "images/reproductorEscenas/escenaClaire2_S.png"
    image escena2ClaireCris = "images/reproductorEscenas/escenaClaire2_cris.png"





    # Imagenes Movil Pantalla principal
    image movil = im.Scale("images/movil/movil.png",300,550)
    image movilhoriz = "images/movil/movil_horiz.png"
    image statsGirls = im.Scale("images/movil/statsGirls.png",50,50)
    image statsGirls_S = im.Scale("images/movil/statsGirls_S.png",50,50)

    image smsapp = im.Scale("images/movil/smsWhats.png",50,50)
    image smsapp_S = im.Scale("images/movil/smsWhats_S.png",50,50)
    image statsProtaApp = im.Scale("images/movil/protaApp.png",50,50)
    image statsProtaApp_S = im.Scale("images/movil/protaApp_S.png",50,50)
    image exitapp = im.Scale("images/movil/exitapp.png",55,55)
    image exitapp_S = im.Scale("images/movil/exitapp_S.png",55,55)
    image help = im.Scale("images/movil/help.png",50,50)
    image help_S = im.Scale("images/movil/help_S.png",50,50)
    image escenasApp = im.Scale("images/movil/escenasChicasApp.png",50,50)
    image escenasApp_S = im.Scale("images/movil/escenasChicasApp_S.png",50,50)
    image iconPatreon = im.Scale("images/movil/iconPatreon.png",50,50)
    image iconPatreon_S = im.Scale("images/movil/iconPatreon_S.png",50,50)
    image iconSubscribestar = im.Scale("images/movil/iconSubscribestar.png",50,50)
    image iconSubscribestar_S = im.Scale("images/movil/iconSubscribestar_S.png",50,50)

    image iconChampions = im.Scale("images/movil/champions.png",50,50)
    image iconChampions_S = im.Scale("images/movil/champions_S.png",50,50)

    image plantillaNTR = im.Scale("images/movil/plantillaNTR.png",250,50)


    image flechaRet = im.Scale("images/movil/flechaReturn.png",150,50)
    image flechaReth = im.Scale("images/movil/flechaReturnh.png",150,50)
    image flechaRetmin = im.Scale("images/movil/flechaReturn.png",115,30)
    image flechaRetminh = im.Scale("images/movil/flechaReturnh.png",115,30)
    image flechaRetminCuad = im.Scale("images/movil/flechaReturn.png",80,30)
    image flechaRetminCuadh = im.Scale("images/movil/flechaReturnh.png",80,30)
    image botonInfoActions = im.Scale("images/movil/botonInfo.png",115,25)
    image botonInfoActions_S = im.Scale("images/movil/botonInfo_S.png",115,25)


    image libretaGracias = im.Scale("images/movil/libretaGracias.png",500,800)

    #Exit
    image PatreonLogo = im.Scale("gui/PatreonLogo.png",250,80)
    image PatreonLogo_S = im.Scale("gui/PatreonLogo_S.png",250,80)
    image SubscriberstarLogo = im.Scale("gui/SubscriberstarLogo.png",250,80)
    image SubscriberstarLogo_S = im.Scale("gui/SubscriberstarLogo_S.png",250,80)

    image yesExit = im.Scale("gui/yesExit.png",75,25)
    image yesExit_S = im.Scale("gui/yesExit_S.png",75,25)
    image noExit = im.Scale("gui/noExit.png",75,25)
    image noExit_S = im.Scale("gui/noExit_S.png",75,25)


    # Imagenes Movil Whats
    image fondoWhats = im.Scale("images/movil/fondoWhats.png",270,455)
    image fondoWhatsSL = im.Scale("images/movil/fondoWhatsSinLetras.png",270,455)
    image pruebaImgWhats = im.Scale("images/movil/pruebaSmsWhats.jpg",100,70)

    # Imagenes Movil chicas

    image protaCarnetBig = im.Scale("images/movil/protaApp.png",102,103)
    image censoredChi = im.Scale("images/movil/fotosCarnetChicas/censoredChicafondoNeg.png",62,73)
    image censoredChiAyuda = im.Scale("images/movil/fotosCarnetChicas/censoredChicafondoNeg.png",32,43)

    image madmini = im.Scale("images/movil/fotosCarnetChicas/madcarnet.png",32,39)
    image madminicomodin = im.Scale("images/movil/fotosCarnetChicas/madcarnetcomodin.png",28,35)
    image madcarnet = im.Scale("images/movil/fotosCarnetChicas/madcarnet.png",62,73)
    image madcarnetS = im.Scale("images/movil/fotosCarnetChicas/madcarnet_S.png",62,73)
    image madcarnetBig = im.Scale("images/movil/fotosCarnetChicas/madcarnet.png",86,103)
    image madcarnetMini = im.Scale("images/movil/fotosCarnetChicas/madcarnet.png",32,43)

    image padmini = im.Scale("images/movil/fotosCarnetChicas/padcarnet.png",32,39)
    image padcarnetMini = im.Scale("images/movil/fotosCarnetChicas/padcarnet.png",32,43)

    image maymini = im.Scale("images/movil/fotosCarnetChicas/maycarnet.png",32,39)
    image mayminicomodin = im.Scale("images/movil/fotosCarnetChicas/maycarnetcomodin.png",28,35)
    image maycarnet = im.Scale("images/movil/fotosCarnetChicas/maycarnet.png",62,73)
    image maycarnetS = im.Scale("images/movil/fotosCarnetChicas/maycarnet_S.png",62,73)
    image maycarnetBig = im.Scale("images/movil/fotosCarnetChicas/maycarnet.png",86,103)
    image maycarnetMini = im.Scale("images/movil/fotosCarnetChicas/maycarnet.png",32,43)

    image medmini = im.Scale("images/movil/fotosCarnetChicas/medcarnet.png",32,39)
    image medminicomodin = im.Scale("images/movil/fotosCarnetChicas/medcarnetcomodin.png",28,35)
    image medcarnet = im.Scale("images/movil/fotosCarnetChicas/medcarnet.png",62,73)
    image medcarnetS = im.Scale("images/movil/fotosCarnetChicas/medcarnet_S.png",62,73)
    image medcarnetBig = im.Scale("images/movil/fotosCarnetChicas/medcarnet.png",86,103)
    image medcarnetMini = im.Scale("images/movil/fotosCarnetChicas/medcarnet.png",32,43)

    image peqmini = im.Scale("images/movil/fotosCarnetChicas/peqcarnet.png",32,39)
    image peqminicomodin = im.Scale("images/movil/fotosCarnetChicas/peqcarnetcomodin.png",28,35)
    image peqcarnet = im.Scale("images/movil/fotosCarnetChicas/peqcarnet.png",62,73)
    image peqcarnetS = im.Scale("images/movil/fotosCarnetChicas/peqcarnet_S.png",62,73)
    image peqcarnetBig = im.Scale("images/movil/fotosCarnetChicas/peqcarnet.png",86,103)
    image peqcarnetMini = im.Scale("images/movil/fotosCarnetChicas/peqcarnet.png",32,43)

    image luciamini = im.Scale("images/movil/fotosCarnetChicas/luciacarnet.png",32,39)
    image luciacarnet = im.Scale("images/movil/fotosCarnetChicas/luciacarnet.png",62,73)
    image luciacarnetS = im.Scale("images/movil/fotosCarnetChicas/luciacarnet_S.png",62,73)
    image luciacarnetBig = im.Scale("images/movil/fotosCarnetChicas/luciacarnet.png",86,103)
    image luciacarnetMini = im.Scale("images/movil/fotosCarnetChicas/luciacarnet.png",32,43)

    image clairemini = im.Scale("images/movil/fotosCarnetChicas/clairecarnet.png",32,39)
    image clairecarnet = im.Scale("images/movil/fotosCarnetChicas/clairecarnet.png",62,73)
    image clairecarnetS = im.Scale("images/movil/fotosCarnetChicas/clairecarnet_S.png",62,73)
    image clairecarnetBig = im.Scale("images/movil/fotosCarnetChicas/clairecarnet.png",86,103)
    image clairecarnetMini = im.Scale("images/movil/fotosCarnetChicas/clairecarnet.png",32,43)

    image aliciamini = im.Scale("images/movil/fotosCarnetChicas/aliciacarnet.png",32,39)
    image aliciacarnet = im.Scale("images/movil/fotosCarnetChicas/aliciacarnet.png",62,73)
    image aliciacarnetS = im.Scale("images/movil/fotosCarnetChicas/aliciacarnet_S.png",62,73)
    image aliciacarnetBig = im.Scale("images/movil/fotosCarnetChicas/aliciacarnet.png",86,103)
    image aliciacarnetMini = im.Scale("images/movil/fotosCarnetChicas/aliciacarnet.png",32,43)

    image carlamini = im.Scale("images/movil/fotosCarnetChicas/carlacarnet.png",32,39)
    image carlacarnet = im.Scale("images/movil/fotosCarnetChicas/carlacarnet.png",62,73)
    image carlacarnetS = im.Scale("images/movil/fotosCarnetChicas/carlacarnet_S.png",62,73)
    image carlacarnetBig = im.Scale("images/movil/fotosCarnetChicas/carlacarnet.png",86,103)
    image carlacarnetMini = im.Scale("images/movil/fotosCarnetChicas/carlacarnet.png",32,43)



    #Ordenador
    image fondoOrdenador = "images/fondoOrdenador.png"
    image miequipoPc = im.Scale("images/ordenador/miEquipo.png",80,80)
    image buscador = im.Scale("images/ordenador/buscador.png",80,80)
    image buscador_S = im.Scale("images/ordenador/buscador_S.png",80,80)
    image imagenesOrdenador = im.Scale("images/ordenador/imagenesOrdenador.png",80,80)
    image imagenesOrdenador_S = im.Scale("images/ordenador/imagenesOrdenador_S.png",80,80)
    image troyano = im.Scale("images/ordenador/troyano.png",80,80)
    image troyano_S = im.Scale("images/ordenador/troyano_S.png",80,80)
    image webcam = im.Scale("images/ordenador/webcam.png",80,80)
    image webcam_S = im.Scale("images/ordenador/webcam_S.png",80,80)
    image utremy = im.Scale("images/ordenador/utremy.png",80,80)
    image utremy_S = im.Scale("images/ordenador/utremy_S.png",80,80)
    #ListaCursos
    image cursoMassage = "images/ordenador/cursos/cursoMassage.png"
    image cursoMassage_S = "images/ordenador/cursos/cursoMassage_S.png"


    # Imagenes Mapa
    image mapaCiudadD = "images/escenas/mapa/mapD.png"
    image mapaCiudadN = "images/escenas/mapa/mapN.png"
    image mapaCiudadS = "images/escenas/mapa/mapS.png"


    #Imagenes Tienda Inventario
    image plantitienda = "images/tienda/plantillatienda.png"
    image modcompra = "images/tienda/modalcompra.png"
    image buttoncompra = im.Scale("images/tienda/buttoncompra.png",150,50)
    image buttoncomprah = im.Scale("images/tienda/buttoncomprah.png",150,50)
    image exit = im.Scale("images/tienda/exit.png",150,50)
    image exith = im.Scale("images/tienda/exith.png",150,50)
    image empty = "images/tienda/empty.png"
    image chocolate = "images/tienda/chocolate.png"
    image chocolateh = "images/tienda/chocolateh.png"
    image fresa = "images/tienda/fresa.png"
    image fresah = "images/tienda/fresah.png"
    image bolis = "images/tienda/bolis.png"
    image bolish = "images/tienda/bolish.png"
    image aceiteCorporal = "images/tienda/aceiteCorporal.png"
    image aceiteCorporalh = "images/tienda/aceiteCorporalh.png"



    #Mochila
    image mochilaObjects = "images/mochila/mochilaobj.png"
    image pagDerecha = im.Scale("images/mochila/pagDerecha.png",60,120)
    image pagIzquierda = im.Scale("images/mochila/pagIzquierda.png",60,120)
    #Diario
    image diario = im.Scale("images/diariopers/diario.png",450,600)
    #TareasRealizadas
    image libretaTareas = im.Scale("images/movil/tareasRealizadas/libretaTareas.png",450,600)


    #ImagenesStatsSms
    image smsStats = im.Scale("images/statsSmsImg/sms.png",70,70)
    image corazonStats = im.Scale("images/statsSmsImg/corazon.png",70,70)
    image latigoStats = im.Scale("images/statsSmsImg/latigo.png",70,70)
    image newContact = im.Scale("images/statsSmsImg/newContact.png",70,70)
    image newUbication = im.Scale("images/statsSmsImg/newUbication.png",70,70)

    #ImagenesGenerales
    image minutosMasTarde = "images/minutosMasTarde.png"
    image fondoNegro = "images/fondoNegro.png"

    #ImagenesMiniGames
    image ninja = "images/minigames/ninja.png"
    image ninja_S = "images/minigames/ninja_S.png"




    return
