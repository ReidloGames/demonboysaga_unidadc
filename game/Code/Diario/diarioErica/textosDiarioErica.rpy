screen textosDiarioErica(nomChica): #Med
    zorder 96
    add "diario" xalign 0.5 ypos 100

    text "{b}{color=#000000}[nomChica]{/color}{/b}" xalign 0.608 ypos 108:
        size 8
    text _("{b} Diario Personal {/b}") xalign 0.5 ypos 150:
        size 15
        color "#7a3838"
    viewport:
        xalign 0.52 ypos 188
        draggable True
        scrollbars "vertical"
        xmaximum 400
        ymaximum 400

        vbox:
            spacing 3
            #if text1diaryEricaMed == True:
                #text _("{b}1){/b} **** Primer texto del diario ñeee ñeee ñeee a tomar fanta y diviertete en la casa del tiodorroomierda gilito tocan la guitarra cada mañana a las 9"):
                #    size 9
                #    color "#000000"
                #text "----------------------------------------------":
                #    color "#000000"

    vbox:
        imagebutton:
            xpos 600 ypos 660
            idle "flechaRetmin"
            hover "flechaRetminh"
            action [Hide("textosDiarioErica")]
return
