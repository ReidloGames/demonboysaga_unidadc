
label personajes:
    # Declara los personajes usados en el juego como en el ejemplo:
    #c558ff
    #Efects
    define flash = Fade(0.1, 0.0, 0.8, color="#f7f7ec")

    define narrador = Character("[narrador]", color="#c06c1a")
    define prota = Character("[nombreProta]", color="#384aa9")
    define protaPensa = Character("[nombreProta] ([pensamiento])", color="#5a67a8")

    define mad = Character("[nombreMad] ([situMad])", color="#aa3636")
    define madPensa = Character("[nombreMad] ([situMad]) ([pensamiento])", color="#a94848")

    define pad = Character("[nombrePad] ([situPad])", color="#1b6b85")
    define padPensa = Character("[nombrePad] ([situPad]) ([pensamiento])", color="#36778d")

    define hermanPeq = Character("[nombrePeq] ([situPeq])", color="#dc36ab")
    define hermanPeqPensa = Character("[nombrePeq] ([situPeq]) ([pensamiento])", color="#d959b3")

    define hermanMed = Character("[nombreMed] ([situMed])", color="#ba36ce")
    define hermanMedPensa = Character("[nombreMed] ([situMed])([pensamiento])", color="#bf51cf")

    define hermanMay = Character("[nombreMay] ([situMay])", color="#9a3361")
    define hermanMayPensa = Character("[nombreMay] ([situMay])([pensamiento])", color="#9a4b6f")

    define mike = Character("Mike", color="#13793c")

    define lucia = Character("Lucia", color="#9f09ac")

    define jack = Character("Jack", color="#173977")
    define claire = Character("Claire", color="#c93dba")
    define clairePensa = Character("Claire ([pensamiento])", color="#c93dba")

    define alicia = Character("Alicia", color="#3dc9a3")
    define directorUni = Character("Director", color="#3dc9a3")

    #Los del banco
    define daren = Character("Daren", color="#3dc9a3")
    define adriano = Character("Adriano", color="#3dc9a3")

    #Massajista
    define mandin = Character("Mandin", color="#b51f0b")
    define yun = Character("Yun", color="#dc0afb")

    #Amarzong
    define louie = Character("Louie", color="#13793c")
    define carla = Character("Carla", color="#3dc9a3")

    #Vecinos
    define ethan = Character("Ethan", color="#173977")

    #Biblioteca
    define ada = Character("Ada", color="#3dc9a3")

    #Tienda Kara
    define senyorMay = Character("Señor mayor", color="#3dc9a3")


    define hombreConsum = Character("Trabajador", color="#c8c8ff")
    return
