
screen mapaDestino():
    imagemap:
        if tiempoDia != 4:
            idle "mapaCiudadD"
        else:
            idle "mapaCiudadN"
        if optionsOpenHud == False:
            hover "mapaCiudadS"
            alpha False

            #miCasa
            hotspot (1100, 430, 130, 120):
                action [Play("sound","audio/effects/puertaEntrada.ogg"),SetVariable("zona","micasa"),Call("nextZona","entradaCasa"),]
                hovered SetVariable("destino",_("Mi Casa"))
                unhovered SetVariable("destino","")
            #Escuela
            hotspot (540, 160, 150, 150):
                if tiempoDia != 4:
                    if numDia == 5 or numDia == 6:
                        action Call("controlTiempoEntrada","escuelaCerrada")
                    else:
                        action [Play("sound","audio/effects/open_door.ogg"),SetVariable("zona","escuela"),Call("nextZona","entrada1")]
                        hovered SetVariable("destino",_("Universidad"))
                        unhovered SetVariable("destino","")
                else:
                    action Call("controlTiempoEntrada","escuelaNoche")




            #TiendaComestibles
            hotspot (1020, 270, 90, 60):
                if tiempoDia != 4:
                    if numDia == 6:
                        action Call("controlTiempoEntrada","tiendaCerrada")
                    else:
                        action [Play("sound","audio/effects/puertaAutomaTienda.ogg"),SetVariable("zona","tiendaComestibles"),Call("nextZona","entradaTiendaComestibles")]
                        hovered SetVariable("destino",_("Tienda comestibles"))
                        unhovered SetVariable("destino","")
                else:
                    action Call("controlTiempoEntrada","tiendaNoche")

            #Amarzong
            hotspot (291, 298, 85, 80):
                if puedesIrAmarzond == True:
                    if tiempoDia != 4:
                        if numDia == 6:
                            action Call("controlTiempoEntrada","tiendaCerrada")
                        else:
                            action [Play("sound","audio/effects/open_door.ogg"),SetVariable("zona","amarzong"),Call("nextZona","entradaAmarzong")]
                            hovered SetVariable("destino",_("Amarzong"))
                            unhovered SetVariable("destino","")
                    else:
                        action Call("controlTiempoEntrada","tiendaNoche")

            #Tienda Kara
            hotspot (255, 470, 60, 160):
                if puedesIrTiendaKara == True:
                    if tiempoDia != 4:
                        if numDia == 5 or numDia == 6:
                            action Call("controlTiempoEntrada","tiendaCerradaKaraFinde")
                        elif tiempoDia == 0 or tiempoDia == 3 or tiempoDia == 4:
                            action Call("controlTiempoEntrada","tiendaCerradaKara")

                        else:
                            action [Play("sound","audio/effects/open_door.ogg"),SetVariable("zona","tiendaKara"),Call("nextZona","tiendaKara1")]
                            hovered SetVariable("destino",_("Tienda ropa de Kara"))
                            unhovered SetVariable("destino","")
                    else:
                        action Call("controlTiempoEntrada","tiendaNoche")


            #Biblioteca
            hotspot (0, 516, 70, 100):
                if puedesIrBiblioteca == True:
                    if tiempoDia != 4:
                        if numDia == 5 or numDia == 6:
                            action Call("controlTiempoEntrada","bibliotecaFinde")
                        elif tiempoDia == 0 or tiempoDia == 3 or tiempoDia == 4:
                            action Call("controlTiempoEntrada","bibliotecaCerrada")
                            hovered SetVariable("destino",_("Biblioteca"))
                            unhovered SetVariable("destino","")

                        else:
                            action [Play("sound","audio/effects/open_door.ogg"),SetVariable("zona","biblioteca"),Call("nextZona","bibliotecaEntrada")]
                            hovered SetVariable("destino",_("Biblioteca"))
                            unhovered SetVariable("destino","")
                    else:
                        action Call("controlTiempoEntrada","bibliotecaNoche")
return


label controlTiempoEntrada(name):
    if tiempoDia != 4:
        scene mapaCiudadD
    else:
        scene mapaCiudadN


    if name == "escuelaCerrada":
        protaPensa "La Universidad esta cerrada el fin de semana"
    if name == "escuelaNoche":
        protaPensa "A estas horas la Universidad esta cerrada"
    if name == "tiendaCerrada":
        protaPensa "La tienda cierra los domingos."
    if name == "tiendaNoche":
        protaPensa "Ya es tarde, la tienda esta cerrada"
    if name == "tiendaCerradaKaraFinde":
        protaPensa "La tienda cierra los sabados y domingos."
    if name == "tiendaCerradaKara":
        protaPensa "La tienda esta cerrada."
    if name == "bibliotecaNoche":
        protaPensa "Ya es tarde, la biblioteca esta cerrada."
    if name == "bibliotecaFinde":
        protaPensa "La biblioteca cierra los sábados y domingos."
    if name == "bibliotecaCerrada":
        protaPensa "La biblioteca esta cerrada."

    return
