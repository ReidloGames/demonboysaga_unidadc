label mirarCambioDeHora():
    call deshabilitaObjetosPersonas() from _call_deshabilitaObjetosPersonas_1
    if llegoTardeClaseAlicia == False:
        show screen tengoQueIrUniversidad
    else:
        call nextTiempoDay(0) from _call_nextTiempoDay_15
    return


screen tengoQueIrUniversidad():
    zorder 96
    frame:
        xpos 910 ypos 14
        padding (10,10,10,10)
        hbox:
            text _("Tengo que ir a la Universidad"):
                color "#000000"
                size 14
            timer 3 action Hide('tengoQueIrUniversidad')
return
