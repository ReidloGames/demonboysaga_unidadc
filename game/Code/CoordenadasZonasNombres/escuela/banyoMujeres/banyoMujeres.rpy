label banyoMujeres:
    call screen banyoMujeresCoordenadas()

    return

screen banyoMujeresCoordenadas():
    imagemap:
        idle "banyoMujeres"
        if optionsOpenHud == False:
            hover "banyoMujeres_S"
            alpha False

            #pasilloWc
            hotspot (1135, 5, 100, 100):
                action Call("nextZona","pasilloWc")
                hovered SetVariable("destino",_("Pasillo Wc"))
                unhovered SetVariable("destino","")
return
