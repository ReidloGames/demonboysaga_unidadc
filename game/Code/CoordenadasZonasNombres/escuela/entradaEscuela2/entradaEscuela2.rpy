label entradaEscuela2:
    call screen entradaEscuela2Coordenadas()

    return

screen entradaEscuela2Coordenadas():
    imagemap:
        idle "entradaEscuela2SG"
        if optionsOpenHud == False:
            hover "entradaEscuela2_S"
            alpha False

            #Entrada1
            hotspot (1100, 10, 150, 150):
                action Call("nextZona","entrada1")
                hovered SetVariable("destino",_("Entrada 1"))
                unhovered SetVariable("destino","")

            #PasilloWc
            hotspot (20, 160, 160, 370):
                action Call("nextZona","pasilloWc")
                hovered SetVariable("destino",_("Pasillo Wc"))
                unhovered SetVariable("destino","")

            #TaquillaProta
            hotspot (579, 228, 60, 300):
                action [Play("sound","audio/effects/abrirTaquilla.ogg"),Call("nextZona","taquillaProta")]
                hovered SetVariable("destino",_("Mi taquilla"))
                unhovered SetVariable("destino","")



            #PasilloClase
            hotspot (875, 175, 380, 430):
                if eventMikeSaludoUniversidad == False:
                    action [Play("sound","audio/effects/open_door.ogg"),Call("nextZona","pasilloClase")]
                elif eventMikeSaludoUniversidad == True and irAmearEscuela == True:
                    action [Play("sound","audio/effects/open_door.ogg"),Call("nextZona","pasilloClase")]
                else:
                    action Call("msgIrAmear")
                hovered SetVariable("destino",_("Pasillo Clase"))
                unhovered SetVariable("destino","")

            #Alarma
            hotspot (806, 262, 50, 50):
                if puedesActivarAlarmaColegio == True:
                    action Call("activaAlarmaAlicia")
                    hovered SetVariable("destino",_("Alarma"))
                    unhovered SetVariable("destino","")



return

label msgIrAmear():
    scene entradaEscuela2SG
    protaPensa "Tengo que ir a mear"
    return
