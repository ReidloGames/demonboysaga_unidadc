label objects(nameObject):
    if nameObject == "bolis":
        call getObjectInventario("bolis") from _call_getObjectInventario_1
        if resultObject == False:
            play sound "audio/effects/cogerObject.ogg"
            $ tengoBolis = True
            python:
                player_inventory.add(bolis)
        else:
            $ tengoBolis = False

    if nameObject == "bicicleta":
        call getObjectInventario("bicicleta") from _call_getObjectInventario_3
        if resultObject == False:
            play sound "audio/effects/cogerObject.ogg"
            $ tengoBicicleta = True
            python:
                player_inventory.add(bicicleta)
        else:
            $ tengoBicicleta = False

    return
