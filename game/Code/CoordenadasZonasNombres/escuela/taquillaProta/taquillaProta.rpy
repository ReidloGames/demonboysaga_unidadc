label taquillaProta():
    call screen taquillaProta()

    return


screen taquillaProta():
    imagemap:
        idle "taquillaProta"
        if optionsOpenHud == False:
            hover "taquillaProta_S"
            alpha False
            #exit
            hotspot (1135, 5, 100, 100):
                action [Play("sound","audio/effects/cerrarTaquilla.ogg"),Call("nextZona","entrada2")]
                hovered SetVariable("destino",_("Salir"))
                unhovered SetVariable("destino","")

    ##Bolis
    if tengoBolis == False:
        imagebutton:
            xpos 870 ypos 10
            idle "boligrafos"
            if optionsOpenHud == False:
                hover "boligrafos_S"
                action Call("objects","bolis")
                hovered SetVariable("destino",_("Bolis"))
                unhovered SetVariable("destino","")


return
