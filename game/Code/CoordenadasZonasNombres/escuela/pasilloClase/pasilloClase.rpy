label pasilloClase:
    call screen pasilloClaseCoordenadas()

    return

screen pasilloClaseCoordenadas():
    imagemap:
        idle "pasilloClase"
        if optionsOpenHud == False:
            hover "pasilloClase_S"
            alpha False
            #entrada2
            hotspot (1140, 5, 100, 90):
                action Call("nextZona","entrada2")
                hovered SetVariable("destino",_("Entrada 2"))
                unhovered SetVariable("destino","")
            #entradaClase
            hotspot (150, 210, 70, 310):
                action [Play("sound","audio/effects/open_door.ogg"),Call("nextZona","clase")]
                hovered SetVariable("destino",_("Clase"))
                unhovered SetVariable("destino","")

            #pasilloProfesores
            hotspot (785, 210, 120, 270):
                action Call("nextZona","pasilloProfesores")
                hovered SetVariable("destino",_("Pasillo profesores"))
                unhovered SetVariable("destino","")





        #PERSONAS
        if eventClaireDirectorVisita2 == True:
            if hasDadoClaseAlicia == True:
                if numDia == 1 or numDia == 3:
                    if tiempoDia == 2:
                        imagebutton:
                            xpos 945 ypos 261
                            idle "mikePasilloClase"
                            if optionsOpenHud == False:
                                hover "mikePasilloClase_S"
                                action Call ("mikeHablaProtaPasilloClase")

return
