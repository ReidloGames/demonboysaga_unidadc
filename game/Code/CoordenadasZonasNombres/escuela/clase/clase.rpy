label clase:
    scene clase
    if numDia in (0 , 2 , 4) and tiempoDia == 2:
        show aliciaDespuesClase:
            xpos 825 ypos 229
    call screen claseCoordenadas()

    return

screen claseCoordenadas():
    imagemap:
        idle "clase"
        if optionsOpenHud == False:
            hover "clase_S"
            alpha False
            #PasilloClase
            hotspot (340, 105, 100, 330):
                action Call("nextZona","pasilloClase")
                hovered SetVariable("destino",_("Pasillo clase"))
                unhovered SetVariable("destino","")

            #DarClase
            hotspot (685, 322, 300, 250):
                action Call("hacerClase")
                hovered SetVariable("destino",_("Hora de clase"))
                unhovered SetVariable("destino","")


        #PERSONAS
        if hasDadoClaseAlicia == True:
            if numDia == 0 or numDia == 2 or numDia == 4:
                if tiempoDia == 2:
                    imagebutton:
                        xpos 825 ypos 229
                        idle "aliciaDespuesClase"
                        if optionsOpenHud == False:
                            if demonBoy == True:
                                hover "aliciaDespuesClase_S"
                                action Call ("aliciaEnClaseInterac")
return
