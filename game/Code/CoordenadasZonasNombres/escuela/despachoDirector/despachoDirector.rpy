label despachoDirector:
    if entrarDespachoDirector == True:
        call screen despachoDirectorCoordenadas()
    elif entrarDespachoDirector == False:
        call pasilloDirectorInteracciones from _call_pasilloDirectorInteracciones

    return

screen despachoDirectorCoordenadas():
    imagemap:
        idle "despachoDirectorSN"
        if optionsOpenHud == False:
            hover "despachoDirector_S"
            alpha False
            #entrada1
            hotspot (1140, 5, 100, 100):
                action Call("nextZona","pasilloDirector")
                hovered SetVariable("destino",_("Pasillo director"))
                unhovered SetVariable("destino","")
return
