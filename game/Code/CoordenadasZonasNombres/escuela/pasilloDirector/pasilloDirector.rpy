label pasilloDirector:
    call screen pasilloDirectorCoordenadas()

    return

screen pasilloDirectorCoordenadas():
    imagemap:
        idle "pasilloDirector"
        if optionsOpenHud == False:
            hover "pasilloDirector_S"
            alpha False
            #entrada1
            hotspot (1125, 5, 100, 100):
                action Call("nextZona","entrada1")
                hovered SetVariable("destino",_("Entrada 1"))
                unhovered SetVariable("destino","")

            #DespachoDirector
            hotspot (820, 210, 150, 330):
                #action Call("nextZona","despachoDirector")
                action Call("pasilloDirectorInteracciones")
                hovered SetVariable("destino",_("Despacho director"))
                unhovered SetVariable("destino","")
return
