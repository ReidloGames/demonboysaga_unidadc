label pasilloWc:
    scene pasilloWc
    if eventMikeSaludoUniversidad and numDia in (0,3,4) and tiempoDia in (1,2):
        show luciaLimpiezaPasilloWc:
            xpos 574 ypos 311

    call screen pasilloWcCoordenadas()

    return

screen pasilloWcCoordenadas():
    imagemap:
        idle "pasilloWc"
        if optionsOpenHud == False:
            hover "pasilloWc_S"
            alpha False
            #entrada2
            hotspot (1140, 5, 100, 100):
                action Call("nextZona","entrada2")
                hovered SetVariable("destino",_("entrada 2"))
                unhovered SetVariable("destino","")

            #banyoHombres
            hotspot (880, 110, 200, 535):
                hovered SetVariable("destino",_("Baño hombres"))
                unhovered SetVariable("destino","")
                if not chicaLucia:
                    action Call("hablaConLuciaAntesDeMensWc")
                else:
                    action [Play("sound","audio/effects/open_door.ogg"),Call("nextZona","banyoHombres")]

            #banyoMujeres
            hotspot (555, 160, 150, 385):
                action Call("entrarWcMujeres")
                hovered SetVariable("destino",_("Baño mujeres"))
                unhovered SetVariable("destino","")

            #PasilloFondo
            hotspot (260, 110, 170, 390):
                action Call("nextZona","pasilloSuperior")
                hovered SetVariable("destino",_("Pasillo superior"))
                unhovered SetVariable("destino","")



    #PERSONAS
    if eventMikeSaludoUniversidad == True and numDia == 0 or numDia == 3 or numDia == 4:
        if tiempoDia == 1 or tiempoDia == 2:
            imagebutton:
                focus_mask True
                xpos 574 ypos 311
                idle "luciaLimpiezaPasilloWc"
                if optionsOpenHud == False:
                    hover "luciaLimpiezaPasilloWc_S"
                    action Call ("luciaFriegaPasilloWcInterac")
return
