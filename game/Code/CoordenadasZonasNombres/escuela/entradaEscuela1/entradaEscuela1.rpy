label entradaEscuela1:
    call screen entradaEscuela1Coordenadas()

    return

screen entradaEscuela1Coordenadas():
    imagemap:
        idle "entradaEscuela1SG"
        if optionsOpenHud == False:
            hover "entradaEscuela1_S"
            alpha False
            #Entrada2
            hotspot (430, 210, 420, 200):
                action Call("nextZona","entrada2")
                hovered SetVariable("destino",_("entrada 2"))
                unhovered SetVariable("destino","")

            #pasilloDirector
            hotspot (970, 120, 350, 520):
                action Call("nextZona","pasilloDirector")
                hovered SetVariable("destino",_("Pasillo director"))
                unhovered SetVariable("destino","")

            #SalidaMapa
            hotspot (1140, 10, 100, 100):
                if noPuedesIrteDeEscuela == True:
                    action Call("msgNoPuedesIrte")
                else:
                    action [SetVariable("zona","miMapa"),Call ('nextZona',"miMapa")]
                hovered SetVariable("destino",_("Ciudad"))
                unhovered SetVariable("destino","")


        #PERSONAS
        if hasHabladoJugarConConsola == True and numDia >= 0 and numDia < 5:
            if tiempoDia == 1 and hasHabladoConPeqEnElColeTrabajosAcabados == False:
                if numDiaTotal > numDiaAparecePeqEnEscuelaFinEstudio +5:
                    imagebutton:
                        #xpos 25 ypos 350
                        xpos 77 ypos 332
                        idle "peqPasilloEscuelaSentada"
                        if optionsOpenHud == False:
                            hover "peqPasilloEscuelaSentada_S"
                            action Call ("peqPasilloEscuelaSentadaHablaProta")
return


label msgNoPuedesIrte():
    scene entradaEscuela1SG
    protaPensa "Ahora no puedo irme"

    return
