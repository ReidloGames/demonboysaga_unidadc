label banyoHombres:
    call screen banyoHombresCoordenadas()

    return

screen banyoHombresCoordenadas():
    imagemap:
        idle "banyoHombres"
        if optionsOpenHud == False:
            hover "banyoHombres_S"
            alpha False

            #pasilloWc
            hotspot (1135, 5, 100, 100):
                action Call("nextZona","pasilloWc")
                hovered SetVariable("destino",_("Pasillo Wc"))
                unhovered SetVariable("destino","")

            #Lavabo
            hotspot (660, 156, 40, 330):
                action Call("banyoHombresInterac")
                hovered SetVariable("destino",_("Cuarto de baño"))
                unhovered SetVariable("destino","")
return
