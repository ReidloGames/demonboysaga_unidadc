label zonasTiendaComestibles():
    if zona == "tiendaComestibles":
        $ renpy.music.play("audio/music/DarxieLand.ogg", loop=True, fadeout=1.0, fadein=1.0, if_changed=True)
        
        if room == "entradaTiendaComestibles":
            call entradaTiendaComestibles from _call_entradaTiendaComestibles
    return
