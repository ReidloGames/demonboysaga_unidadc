label habitacionAmarzong():
    call screen habitacionAmarzongCoordenadas()

    return

screen habitacionAmarzongCoordenadas():
    imagemap:
        idle "habitacionAmarzong"
        if optionsOpenHud == False:
            hover "habitacionAmarzong_S"
            alpha False
            #entradaAmarzong
            hotspot (238, 80, 180, 380):
                #action Call("nextZona","entradaAmarzong")
                action [Play("sound","audio/effects/open_door.ogg"),Call("nextZona","entradaAmarzong")]
                hovered SetVariable("destino",_("Entrada"))
                unhovered SetVariable("destino","")

        #PERSONAS
        if numDia >= 0 and numDia < 6:
            if tiempoDia == 0 or tiempoDia == 1 or tiempoDia == 2:
                imagebutton:
                    xpos 920 ypos 150
                    idle "carlaTrabajo"
                    if optionsOpenHud == False:
                        hover "carlaTrabajo_S"
                        action Call ("carlaTrabajoInterac")


return
