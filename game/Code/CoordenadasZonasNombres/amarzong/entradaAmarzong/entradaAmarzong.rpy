label entradaAmarzong:
    call screen entradaAmarzongCoordenadas()

    return

screen entradaAmarzongCoordenadas():
    imagemap:
        idle "entradaAmarzong"
        if optionsOpenHud == False:
            hover "entradaAmarzong_S"
            alpha False
            if abrirPuertaAmarzong == True:
                #habitacionAmarzong
                hotspot (1038, 140, 220, 440):
                    #action Call("nextZona","habitacionAmarzong")
                    action [Play("sound","audio/effects/open_door.ogg"),Call("nextZona","habitacionAmarzong")]
                    hovered SetVariable("destino",_("Almacen"))
                    unhovered SetVariable("destino","")

            #Salida
            hotspot (100, 148, 125, 303):
                action [SetVariable("zona","miMapa"),Call ('nextZona',"miMapa")]
                hovered SetVariable("destino",_("Salida"))
                unhovered SetVariable("destino","")





        #PERSONAS
        if numDia >= 0 and numDia < 6:
            if tiempoDia == 0 or tiempoDia == 1 or tiempoDia == 2:
                imagebutton:
                    xpos 488 ypos 246
                    idle "louieTrabajo"
                    if optionsOpenHud == False:
                        hover "louieTrabajo_S"
                        action Call ("conoceLouieTrabajoInterac")


return
