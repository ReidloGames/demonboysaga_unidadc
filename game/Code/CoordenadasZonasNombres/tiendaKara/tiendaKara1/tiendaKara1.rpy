label tiendaKara1():

    call screen tiendaKara1Coordenadas()
    return

screen tiendaKara1Coordenadas():
    imagemap:
        idle "tiendaKara_1"
        if optionsOpenHud == False:
            hover "tiendaKara_1_S"
            alpha False

            #tiendaKara Planta2
            hotspot (845, 220, 105, 130):
                action [Play("sound","audio/effects/open_door.ogg"),Call("nextZona","tiendaKara2")]
                hovered SetVariable("destino",_("Planta superior"))
                unhovered SetVariable("destino","")

            #Salida
            hotspot (1150, 18, 105, 95):
                action [SetVariable("zona","miMapa"),Call ('nextZona',"miMapa")]
                hovered SetVariable("destino",_("Salida"))
                unhovered SetVariable("destino","")





        #PERSONAS
        if numDia >= 0 and numDia < 6:
            if tiempoDia == 0 or tiempoDia == 1 or tiempoDia == 2:
                imagebutton:
                    xpos 448 ypos 174
                    idle "karaTiendaPers"
                    if optionsOpenHud == False:
                        hover "karaTiendaPers_S"
                        if hablarLibroYogaSofia == True:
                             action Call ("storeKara")



return
