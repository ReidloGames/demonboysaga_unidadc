
label zonasMiCasa():
    $ nombreOcupa = ""

    call localizaPersonas() from _call_localizaPersonas_1

    if zona == "micasa":

        $ renpy.music.play("audio/music/on_my_way.ogg", loop=True, fadeout=1.0, fadein=1.0, if_changed=True)

        if room == "mihabitacion":
            call miHabitacion from _call_miHabitacion
        elif room == "pasillo1":
            call pasillo1 from _call_pasillo1
        elif room == "galeriaGym":
            call galeriaGym from _call_galeriaGym
        elif room == "pasillo2":
            call pasillo2 from _call_pasillo2
        elif room == "habHermanPeq":
            call habHermanPeq from _call_habHermanPeq
        elif room == "habHermanMed":
            call habHermanMed from _call_habHermanMed
        elif room == "habHermanMay":
            call habHermanMay from _call_habHermanMay
        elif room == "comedor":
            call comedor from _call_comedor
        elif room == "salon":
            call salon from _call_salon
        elif room == "cocina":
            call cocina from _call_cocina
        elif room == "habPadres":
            call habPadres from _call_habPadres
        elif room == "banyo":
            call banyo from _call_banyo
        elif room == "entradaCasa":
            call entradaCasa from _call_entradaCasa
        elif room == "jardinPis":
            call jardinPis from _call_jardinPis
        elif room == "garage":
            call garage from _call_garage


    return
