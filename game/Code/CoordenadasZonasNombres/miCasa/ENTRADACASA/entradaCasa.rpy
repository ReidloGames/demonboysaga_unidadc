label entradaCasa():
    if localizaMad == "entradaCasa":
        call entradaCasaCoordenadasMad() from _call_entradaCasaCoordenadasMad

    call screen entradaCasaCoordenadas()
    return

screen entradaCasaCoordenadas():
    imagemap:
        if tiempoDia != 4:
            idle "entradaCasaSgD"
        else:
            idle "entradaCasaSgN"
        if optionsOpenHud == False:
            hover "entradaCasaS"
            alpha False
            #Comedor
            hotspot (0, 300, 150, 380):
                action [Play("sound","audio/effects/open_door.ogg"),Call("nextZona","comedor")]
                hovered SetVariable("destino",_("Comedor"))
                unhovered SetVariable("destino","")
            #jardinPiscina
            hotspot (600, 65, 200, 345):
                action [Play("sound","audio/effects/open_door.ogg"),Call("nextZona","jardinPis")]
                hovered SetVariable("destino",_("Jardín Piscina"))
                unhovered SetVariable("destino","")
            #Garage
            hotspot (949, 25, 150, 40):
                action [Play("sound","audio/effects/open_door.ogg"),Call("nextZona","garage")]
                hovered SetVariable("destino",_("Garaje"))
                unhovered SetVariable("destino","")
            #SalidaMapa
            hotspot (1185, 0, 150, 720):
                action [SetVariable("zona","miMapa"),Call ('nextZona',"miMapa")]
                hovered SetVariable("destino",_("Ciudad"))
                unhovered SetVariable("destino","")
