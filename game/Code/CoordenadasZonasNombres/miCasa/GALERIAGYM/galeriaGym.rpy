label galeriaGym:
    if localizaMad =="galeriaGym":
        call galeriaGymCoordenadasMad from _call_galeriaGymCoordenadasMad
    if localizaPeq =="galeriaGym":
        call galeriaGymCoordenadasPeq from _call_galeriaGymCoordenadasPeq
    if localizaMay =="galeriaGym":
        call galeriaGymCoordenadasMay from _call_galeriaGymCoordenadasMay

    call screen galeriaGymCoordenadas
    return

screen galeriaGymCoordenadas():
    imagemap:
        idle "galeriaGym"
        if optionsOpenHud == False:
            hover "galeriaGym_S"
            alpha False
            #SalidaPasillo
            hotspot (1160, 10, 125, 100):
                action [Play("sound","audio/effects/open_door.ogg"),Call("nextZona","pasillo1")]
                hovered SetVariable("destino",_("Pasillo 1"))
                unhovered SetVariable("destino","")

            #ZonaComodin
            if localizaMad != "galeriaGym" and localizaPad != "galeriaGym" and localizaPeq != "galeriaGym" and localizaMed != "galeriaGym" and localizaMay != "galeriaGym":
                if tiempoDia != 4:
                    if demonBoy == True:
                        hotspot (475, 595, 615, 150):
                            action Call("zonaComodinGym")
