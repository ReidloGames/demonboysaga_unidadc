
label banyo():
    if localizaMad == "banyo":
        call banyoCoordenadasMad() from _call_banyoCoordenadasMad

    call screen banyoCoordenadas()
    return

screen banyoCoordenadas():
    imagemap:
        idle "banyo"
        if optionsOpenHud == False:
            hover "banyo_S"
            alpha False
            #Comedor
            hotspot (1170, 10, 130, 80):
                action [Play("sound","audio/effects/open_door.ogg"),Call("nextZona","comedor")]
                hovered SetVariable("destino",_("Comedor"))
                unhovered SetVariable("destino","")
