label miHabitacion():
    hide screen protaEnCama
    if localizaMad == "miHabitacion":
        call miHabitacionCoordenadasMad() from _call_miHabitacionCoordenadasMad

    call screen miHabitacionCoordenadas()

    return


screen miHabitacionCoordenadas():

    imagemap:
        if tiempoDia != 4:
            if eventMadRecogeRopaHabProta_visit:
                idle "habProtaSinCesta"
            else:
                idle "habProtaSgD"
        else:
            idle "habProtaSgN"
        if optionsOpenHud == False:
            hover "habProtaS"
            alpha False
            hotspot (1105, 30, 100, 80):
                action [Play("sound","audio/effects/open_door.ogg"),Call("nextZona","pasillo1")]
                hovered SetVariable("destino",_("Pasillo 1"))
                unhovered SetVariable("destino","")

            #Cama
            hotspot (0, 445, 370, 260):
                action Call("opcionesMiCama")
                hovered SetVariable("destino",_("Cama"))
                unhovered SetVariable("destino","")

            #Ventana
            hotspot (310, 120, 145, 280):
                action Call("opcionesMiVentana")
                hovered SetVariable("destino",_("Ventana"))
                unhovered SetVariable("destino","")
            #Pc
            hotspot (760, 360, 65, 90):
                action [Show ('ordenadorOpcionesProta'),SetVariable("optionsOpenHud",True),Call("esconderHud")]
                hovered SetVariable("destino",_("Ordenador"))
                unhovered SetVariable("destino","")
            #TV
            hotspot (1110, 250, 300, 200):
                action [Call ('opcionesMiTv'),Call("esconderHud")]
                hovered SetVariable("destino",_("Tv"))
                unhovered SetVariable("destino","")

return
