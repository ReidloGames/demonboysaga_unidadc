label jardinPis():
    if localizaMad == "jardinPis":
        call jardinPisCoordenadasMad() from _call_jardinPisCoordenadasMad
    if localizaMed == "jardinPis":
        call jardinPisCoordenadasMed() from _call_jardinPisCoordenadasMed
    if localizaMay == "jardinPis":
        call jardinPisCoordenadasMay() from _call_jardinPisCoordenadasMay

    call screen jardinPisCoordenadas()
    return

screen jardinPisCoordenadas():
    imagemap:
        if tiempoDia != 4:
            idle "jardinPisSgD"
        else:
            idle "jardinPisSgN"
        if optionsOpenHud == False:
            hover "jardinPisS"
            alpha False
            #entradaCasa
            hotspot (1160, 5, 100, 80):
                action [Play("sound","audio/effects/open_door.ogg"),Call("nextZona","entradaCasa")]
                hovered SetVariable("destino",_("Entrada"))
                unhovered SetVariable("destino","")

        #ZonaComodin Mesa
        if localizaMad != "jardinPis" and localizaPad != "jardinPis" and localizaPeq != "jardinPis" and localizaMed != "jardinPis" and localizaMay != "jardinPis":
            if tiempoDia != 4:
                if demonBoy == True:
                    hotspot (346, 405, 260, 80):
                        action Call("zonaComodinJardinPis")
