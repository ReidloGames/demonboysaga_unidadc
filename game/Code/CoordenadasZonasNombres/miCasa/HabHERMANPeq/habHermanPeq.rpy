label habHermanPeq():
    if localizaPeq == "habHermanPeq":
        call habHermanPeqCoordenadasPeq() from _call_habHermanPeqCoordenadasPeq

    call screen habHermanPeqCoordenadas()
    return

screen habHermanPeqCoordenadas():
    imagemap:
        if tiempoDia != 4:
            idle "habHijPeqSgD"
        else:
            idle "habHijPeqSgN"

        if optionsOpenHud == False:
            hover "habHijPeqS"
            alpha False
            #Pasillo2
            hotspot (1180, 5, 130, 70):
                action [Play("sound","audio/effects/open_door.ogg"),Call("nextZona","pasillo2")]
                hovered SetVariable("destino",_("Pasillo 2"))
                unhovered SetVariable("destino","")

        if eventDiarioPersonalPeq == True:
            #DiarioPersonal
            hotspot (1030, 472, 150, 230):
                action Call("buscarCosasHabitacionPeq")
                hovered SetVariable("destino",_("Cajones"))
                unhovered SetVariable("destino","")
