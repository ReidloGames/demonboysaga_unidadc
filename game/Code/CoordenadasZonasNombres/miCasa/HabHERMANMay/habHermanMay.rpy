label habHermanMay():
    if localizaMay == "habHermanMay":
        call habHermanMayCoordenadasMay() from _call_habHermanMayCoordenadasMay

    call screen habHermanMayCoordenadas()
    return

screen habHermanMayCoordenadas():
    imagemap:
        if tiempoDia != 4:
            idle "habHijMaySgD"
        else:
            idle "habHijMaySgN"
        if optionsOpenHud == False:
            hover "habHijMayS"
            alpha False
            #Pasillo1
            hotspot (1180, 5, 130, 70):
                action [Play("sound","audio/effects/open_door.ogg"),Call("nextZona","pasillo2")]
                hovered SetVariable("destino",_("Pasillo 2"))
                unhovered SetVariable("destino","")
