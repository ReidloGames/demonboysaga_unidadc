label pasillo1():
    if localizaMad =="pasillo1":
        call pasillo1CoordenadasMad from _call_pasillo1CoordenadasMad

    call screen pasillo1Coordenadas
    return

screen pasillo1Coordenadas():
    imagemap:
        idle "pasillo1"
        if optionsOpenHud == False:
            hover "pasillo1S"
            alpha False
            #PuertaMihabitacion
            hotspot (520, 125, 165, 400):
                action [Play("sound","audio/effects/open_door.ogg"),Call("nextZona","mihabitacion")]
                hovered SetVariable("destino",_("Mi habitación"))
                unhovered SetVariable("destino","")
            #Pasillo2
            hotspot (940, 105, 120, 560):
                action Call("nextZona","pasillo2")
                hovered SetVariable("destino",_("Pasillo 2"))
                unhovered SetVariable("destino","")


            #galeriaGym
            hotspot (130, 90, 90, 600):
                if puedesIrAGaleria == True:
                    action [Play("sound","audio/effects/open_door.ogg"),Call("nextZona","galeriaGym")]
                    hovered SetVariable("destino",_("Galería"))
                    unhovered SetVariable("destino","")
                else:
                    action Show("noEsMomentoDeIrAlli")
                    hovered SetVariable("destino",_("Galería"))
                    unhovered SetVariable("destino","")
return


screen noEsMomentoDeIrAlli():
    frame:
        xpos 950 ypos 14
        padding (20,10,20,10)
        hbox:
            #xpos 925 ypos 100
            text _("No es momento de ir allí"):
                size 17
                color "#000000"
            timer 1.2 action Hide('noEsMomentoDeIrAlli')
return
