label comedor():
    call cambiandoseMad from _call_cambiandoseMad
    #if localizaMad == "comedor":
    #    call comedorCoordenadasMad() from _call_comedorCoordenadasMad
    if localizaMed == "comedor":
        call comedorCoordenadasMed() from _call_comedorCoordenadasMed
    scene comedor
    call screen comedorCoordenadas()
    return

screen comedorCoordenadas():

    imagemap:
        idle "comedor"
        if optionsOpenHud == False:
            hover "comedor_S"
            alpha False
            #Salon
            hotspot (2, 150, 110, 330):
                if protaHablarConDirectorDinero and not madCocinaLimpia3raInteracMartes_visit1 and not protaHablaConPadrDinero and not demonBoy:
                    action Call("noHayTiempoParaEso")
                else:
                    action Call("nextZona","salon")
                hovered SetVariable("destino",_("Salón"))
                unhovered SetVariable("destino","")

            #Cocina
            hotspot (180, 175, 160, 310):
                if chicoPad and tiempoDia == 0 and not padDePieHabPadr2daInteracMiercoles_visit1 and demonBoy == False:
                    action Call("noHayTiempoParaEso")
                elif eventKaraEricaSeVanDeCompras and not pensarEnKaraPaja:
                    action Call("noHayTiempoParaEso")
                else:
                    action [Play("sound","audio/effects/open_door.ogg"),Call("nextZona","cocina")]
                hovered SetVariable("destino",_("Cocina"))
                unhovered SetVariable("destino","")

            #HabPadres
            hotspot (560, 175, 150, 330):
                hovered SetVariable("destino", ("[nombreMad] & [nombrePad]"))
                unhovered SetVariable("destino","")
                if not llegoTardeClaseAlicia:
                    action Call("noHayTiempoParaEso")
                else:
                    action Call("puertaCerradaPadres")


            #EntradaCasa
            hotspot (950, 160, 100, 370):
                #if padSentadoCamaHabPadr1raInteracLunes_visit1 == True:
                if chicoPad and tiempoDia == 0 and not (padDePieHabPadr2daInteracMiercoles_visit1 and madCocinaDarMamar3raInteracJueves_visit1) and not demonBoy:
                    action Call("noHayTiempoParaEso")
                elif eventKaraEricaSeVanDeCompras and not pensarEnKaraPaja:
                    action Call("noHayTiempoParaEso")
                else:
                    action [Play("sound","audio/effects/open_door.ogg"),Call("nextZona","entradaCasa")]
                #else:
                #    action Call("saludarJhon")
                hovered SetVariable("destino",_("Entrada"))
                unhovered SetVariable("destino","")

            #Baño
            hotspot (1145, 100, 130, 535):
                hovered SetVariable("destino",_("Baño"))
                unhovered SetVariable("destino","")
                if not llegoTardeClaseAlicia:
                    action Call("noHayTiempoParaEso")
                else:
                    #action Call("nextZona","banyo")
                    action Call("hayAlguienEnBanyo")

            #Pasillo2
            hotspot (1150, 10, 130, 80):
                hovered SetVariable("destino",_("Pasillo 2"))
                unhovered SetVariable("destino","")
                if llegoTardeClaseAlicia:
                    if protaHablarConDirectorDinero and not madCocinaLimpia3raInteracMartes_visit1 and not protaHablaConPadrDinero:
                        action Call("noHayTiempoParaEso")
                    elif eventKaraEricaSeVanDeCompras and not pensarEnKaraPaja:
                        action Call("noHayTiempoParaEso")
                    elif chicoPad:
                        action Call("nextZona","pasillo2")
                    else:
                        action Call("noHayTiempoParaEso")
                else:
                    action Call("nextZona","pasillo2")

            #ZonaComodin Mesa
            if localizaMad != "comedor" and localizaPad != "comedor" and localizaPeq != "comedor" and localizaMed != "comedor" and localizaMay != "comedor":
                if tiempoDia != 4:
                    if demonBoy == True:
                        hotspot (0, 485, 200, 300):
                            action Call("zonaComodinComedor")

            if not chicaMed and chicaMay:
                imagebutton:
                    xpos 0 ypos 347
                    focus_mask True
                    idle "medEstudiandoComedor1"
                    hover "medEstudiandoComedor1_S"
                    action Call ("medEstudiando3raInteracMiercoles")
return

label saludarJhon():
    scene comedor
    protaPensa "Debería saludar a [nombrePad3]"

    return
