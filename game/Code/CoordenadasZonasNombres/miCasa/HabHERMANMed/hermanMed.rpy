label habHermanMed():
    if localizaMed == "habHermanMed":
        call habHermanMedCoordenadasMed() from _call_habHermanMedCoordenadasMed


    call screen habHermanMedCoordenadas()


    return

screen habHermanMedCoordenadas():
    imagemap:
        if tiempoDia != 4:
            idle "habHijMedSgD"
        else:
            idle "habHijMedSgN"
        if optionsOpenHud == False:
            hover "habHijMedS"
            alpha False
            #Pasillo1
            hotspot (1170, 5, 130, 70):
                action [Play("sound","audio/effects/open_door.ogg"),Call("nextZona","pasillo2")]
                hovered SetVariable("destino",_("Pasillo 2"))
                unhovered SetVariable("destino","")

            #DiarioPersonal
            if diaryEricaMed == True:
                hotspot (780, 260, 50, 70):
                    action Call("diarioSelectName",nombreMed)
                    hovered SetVariable("destino",_("Libros"))
                    unhovered SetVariable("destino","")
return
