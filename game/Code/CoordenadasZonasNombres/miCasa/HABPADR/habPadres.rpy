label habPadres():
    if localizaMad == "habPadres":
        call habPadresCoordenadasMad() from _call_habPadresCoordenadasMad
    if localizaPad == "habPadres":
        call habPadresCoordenadasPad() from _call_habPadresCoordenadasPad

    call screen habPadresCoordenadas()
    return

screen habPadresCoordenadas():
    imagemap:
        if tiempoDia != 4:
            idle "habPadrSgD"
        else:
            idle "habPadrSgN"
        if optionsOpenHud == False:
            hover "habPadrS"
            alpha False
            #Comedor
            hotspot (1160, 10, 130, 80):
                action [Play("sound","audio/effects/open_door.ogg"),Call("nextZona","comedor")]
                hovered SetVariable("destino",_("Comedor"))
                unhovered SetVariable("destino","")

        if not chicoPad and chicaMed:
            imagebutton:
                xpos 117 ypos 146
                idle "PadSentadoCama1"
                if optionsOpenHud == False:
                    hover "PadSentadoCama1_S"
                    action Call ("padSentadoCamaHabPadr1raInteracLunes")
        if chicoPad and not protaHablarConDirectorDinero and tiempoDia == 0:
            imagebutton:
                xpos 53 ypos 67
                idle "padrPensandoDePie1"
                if optionsOpenHud == False:
                    hover "padrPensandoDePie1_S"
                    action Call ("padDePieHabPadr2daInteracMiercoles")
return
