screen salonMediodiaViernesPad():
    zorder 94
    imagebutton:
        xpos 228 ypos 269
        idle "padSentadoSofa1"
        if optionsOpenHud == False:
            hover "padSentadoSofa1_S"
            action Call ("padMiraTv3raInteracViernes")


label suenaTvChangeimagesPad():
    show screen tvDeporte0()
    return


screen tvDeporte0():
    zorder 94
    imagebutton:
        xpos 0 ypos 0
        idle "tvDeporte1"
    timer 3.0 action [Show("tvDeporte1"),Hide("tvDeporte0")]

screen tvDeporte1():
    zorder 94
    imagebutton:
        xpos 0 ypos 0
        idle "tvDeporte2"
    timer 3.0 action [Show("tvDeporte0"),Hide("tvDeporte1")]
