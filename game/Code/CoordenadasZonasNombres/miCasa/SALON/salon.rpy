label salon():
    if localizaMad == "salon":
        call salonCoordenadasMad() from _call_salonCoordenadasMad
    if localizaPad == "salon":
        call salonCoordenadasPad() from _call_salonCoordenadasPad
    if localizaPeq == "salon":
        call salonCoordenadasPeq() from _call_salonCoordenadasPeq
    if localizaMed == "salon":
        call salonCoordenadasMed() from _call_salonCoordenadasMed
    if localizaMay == "salon":
        call salonCoordenadasMay() from _call_salonCoordenadasMay
    if family4 =="salon":
        call salonCoordenadasFamily4() from _call_salonCoordenadasFamily4
    if family5 =="salon":
        call salonCoordenadasFamily5() from _call_salonCoordenadasFamily5

    scene salon
    if not chicaMed and eventPeqPreguntaEncuentroAlicia:
        show mayPintaUnyas1:
            xpos 0 ypos 290
    call screen salonCoordenadas()
    return



screen salonCoordenadas():
    imagemap:
        idle "salon"
        if optionsOpenHud == False:
            hover "salon_S"
            alpha False
            #Salon
            hotspot (1150, 10, 100, 80):
                action Call("nextZona","comedor")
                hovered SetVariable("destino",_("Comedor"))
                unhovered SetVariable("destino","")
            #Tv

            hotspot (1046, 170, 195, 180):
                hovered SetVariable("destino",_("Tv"))
                unhovered SetVariable("destino","")
                if chicaMed:
                    action Call("verTvSalon")
                else:
                    action Call("verTvSalonPrimerDia")


            #ZonaComodin Sofa
            if localizaMad != "salon" and localizaPad != "salon" and localizaPeq != "salon" and localizaMed != "salon" and localizaMay != "salon":
                if tiempoDia != 4:
                    if demonBoy == True:
                        hotspot (0, 385, 410, 400):
                            action Call("zonaComodinSalon")
            if not chicaMed and eventPeqPreguntaEncuentroAlicia:
                imagebutton:
                    xpos 0 ypos 290
                    focus_mask True
                    idle "mayPintaUnyas1"
                    hover "mayPintaUnyas1_S"
                    action Call ("mayPintaUnyas4taInteracMiercoles")
