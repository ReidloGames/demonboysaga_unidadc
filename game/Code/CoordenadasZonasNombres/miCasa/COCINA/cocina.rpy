label cocina():
    if localizaMad == "cocina":
        call cocinaCoordenadasMad() from _call_cocinaCoordenadasMad
    if localizaPeq == "cocina":
        call cocinaCoordenadasPeq() from _call_cocinaCoordenadasPeq
    if localizaMay == "cocina":
        call cocinaCoordenadasMay() from _call_cocinaCoordenadasMay

    call screen cocinaCoordenadas()
    return

screen cocinaCoordenadas():
    imagemap:
        idle "cocina"
        if optionsOpenHud == False:
            hover "cocina_S"
            alpha False
            #Comedor
            hotspot (1160, 10, 130, 80):
                action [Play("sound","audio/effects/open_door.ogg"),Call("nextZona","comedor")]
                hovered SetVariable("destino",_("Comedor"))
                unhovered SetVariable("destino","")

            #LimpiaPlatos
            if puedesLimpiarPlatos == True:
                hotspot (531, 355, 127, 75):
                    action Call("lavaPlatos")
                    hovered SetVariable("destino",_("Fregadero"))
                    unhovered SetVariable("destino","")

            #ZonaComodin Mesa
            if localizaMad != "cocina" and localizaPad != "cocina" and localizaPeq != "cocina" and localizaMed != "cocina" and localizaMay != "cocina":
                if tiempoDia != 4:
                    if demonBoy == True:
                        hotspot (874, 394, 120, 300):
                            action Call("zonaComodinCocina")

            if tiempoDia == 0 and not protaHablarConDirectorDinero:
                imagebutton:
                    xpos 638 ypos 185
                    idle "madDarMamar1"
                    if optionsOpenHud == False:
                        hover "madDarMamar1_S"
                        action Call ("madCocinaDarMamar3raInteracJueves")
