label garage():
    if localizaMad == "garage":
        call garageCoordenadasMad() from _call_garageCoordenadasMad

    call screen garageCoordenadas()
    return

screen garageCoordenadas():
    imagemap:
        if tengoBicicleta == False:
            idle "garageSg"
        else:
            idle "garageSinBiciSg"

        if optionsOpenHud == False:
            hover "garage_S"
            alpha False
            #EntradaCasa
            hotspot (70, 120, 185, 250):
                action [Play("sound","audio/effects/open_door.ogg"),Call("nextZona","entradaCasa")]
                hovered SetVariable("destino",_("Entrada"))
                unhovered SetVariable("destino","")

            #Objects
            #Bicicleta
            if tengoBicicleta == False:
                if puedesCogerBici == True:
                    hotspot (50, 380, 120, 300):
                        action Call("objects","bicicleta")
                        hovered SetVariable("destino",_("Bicicleta"))
                        unhovered SetVariable("destino","")


        #if puedesCogerBici == False:
        #    imagebutton:
        #        xpos 870 ypos 10
        #        idle "boligrafos"
        #        if optionsOpenHud == False:
        #            hover "boligrafos_S"
        #            action Call("objects","bolis")
        #            hovered SetVariable("destino",_("Bolis"))
        #            unhovered SetVariable("destino","")
#
