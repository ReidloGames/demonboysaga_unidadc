label pasillo2():
    call cambiandosePeq() from _call_cambiandosePeq
    call cambiandoseMed() from _call_cambiandoseMed
    call cambiandoseMay() from _call_cambiandoseMay
    if localizaMad == "pasillo2":
        call pasillo2CoordenadasMad() from _call_pasillo2CoordenadasMad
    scene pasillo2
    call screen pasillo2Coordenadas()
    return

screen pasillo2Coordenadas():
    imagemap:
        idle "pasillo2"
        if optionsOpenHud == False:
            hover "pasillo2S"
            alpha False
            #Pasillo1
            hotspot (60, 170, 70, 450):
                action Call("nextZona","pasillo1")
                hovered SetVariable("destino",_("Pasillo 1"))
                unhovered SetVariable("destino","")

            #habHermaPeq
            hotspot (160, 170, 150, 320):
                hovered SetVariable("destino",_("Habitación [nombrePeq]"))
                unhovered SetVariable("destino","")
                if llegoTardeClaseAlicia:
                    if puedesIrHabPeq == True:
                        action Call("puertaCerradaPeq")
                    else:
                        action Show("noEsMomentoDeIrAlli")
                elif not llegoTardeClaseAlicia:
                    action Call("noHayTiempoParaEso")


            #habHermaMed
            hotspot (490, 170, 150, 320):
                hovered SetVariable("destino",_("Habitación [nombreMed]"))
                unhovered SetVariable("destino","")
                if llegoTardeClaseAlicia:
                    if puedesIrHabMed == True:
                        action Call("puertaCerradaMed")
                    else:
                        action Show("noEsMomentoDeIrAlli")
                elif not llegoTardeClaseAlicia:
                    action Call("noHayTiempoParaEso")



            #habHermaMay
            hotspot (805, 170, 150, 320):
                hovered SetVariable("destino",_("Habitación [nombreMay]"))
                unhovered SetVariable("destino","")
                if llegoTardeClaseAlicia:
                    if puedesIrHabMay == True:
                        action Call("puertaCerradaMay")
                    else:
                        action Show("noEsMomentoDeIrAlli")
                elif not llegoTardeClaseAlicia:
                    action Call("noHayTiempoParaEso")


            #comedor
            hotspot (1125, 175, 125, 350):
                action Call("nextZona","comedor")
                hovered SetVariable("destino",_("Comedor"))
                unhovered SetVariable("destino","")

label noHayTiempoParaEso():

    if destino == "Habitación [nombrePeq]":
        scene pasillo2
        protaPensa "[nombrePeq] ya se fue a Uni. Yo debería hacer lo mismo."
    elif destino in ("Habitación [nombreMed]", "Habitación [nombreMay]"):
        scene pasillo2
        protaPensa "No tengo tiempo para eso. Necesito llegar a la Uni."
    elif destino == "[nombreMad] & [nombrePad]":
        scene comedor
        protaPensa "No tengo tiempo para eso. Necesito llegar a la Uni."
    elif destino == "Baño":
        scene comedor
        protaPensa "No hay tiempo para ducharse ahora. Ya llego tarde a la Uni."
    elif destino in ("Pasillo 2", "Salón"):
        scene comedor
        if not chicaMad:
            protaPensa "Quiero ver qué se está cocinando."
        elif not chicaMed:
            if not chicaMay:
                protaPensa "Tengo ganas de ver la televisión en el salón."
            elif chicaMay:
                show medEstudiandoComedor1:
                    xpos 0 ypos 347
                protaPensa "[nombreMed] se ha fijado en mí a pesar de que actúa como si no lo hiciera. Debería saludar."
        elif not chicoPad:
            protaPensa "Ahora es un buen momento de hablar con [situPad2]."
        elif protaHablarConDirectorDinero and not madCocinaLimpia3raInteracMartes_visit1 and not protaHablaConPadrDinero:
            protaPensa "Creo que escucho a [nombreMad] en la cocina."
        elif eventKaraEricaSeVanDeCompras and not pensarEnKaraPaja:
            protaPensa "Estoy empalmado de ver el culo de Kara. Necesito ir a ver la televisión y hacerme una paja."
    elif destino in ("Entrada", "Cocina"):
        scene comedor
        if not padDePieHabPadr2daInteracMiercoles_visit1 and not demonBoy:
            protaPensa "Quiero ver a John, a ver si me da dinero."
        elif not madCocinaDarMamar3raInteracJueves_visit1 and not demonBoy:
            protaPensa "Debería ir a la cocina y beber un poco de leche antes de salir."
        elif eventKaraEricaSeVanDeCompras and not pensarEnKaraPaja:
            protaPensa "Estoy empalmado de ver el culo de Kara. Necesito ir a ver la televisión y hacerme una paja."


    return
