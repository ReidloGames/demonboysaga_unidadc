label zonasDesp_amarzong():
    if zona == "amarzong":
        $ renpy.music.play("audio/music/fretless-by-kevin-macleod.ogg", loop=True, fadeout=1.0, fadein=1.0, if_changed=True)

        if room == "entradaAmarzong":
            call entradaAmarzong from _call_entradaAmarzong
        elif room == "habitacionAmarzong":
            call habitacionAmarzong from _call_habitacionAmarzong

    return
