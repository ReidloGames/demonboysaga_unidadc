
label zonasEscuela():
    if zona == "escuela":
        $ renpy.music.play("audio/music/Sock_Hop.ogg", loop=True, fadeout=1.0, fadein=1.0, if_changed=True)

        if room == "entrada1":
            call entradaEscuela1 from _call_entradaEscuela1
        elif room == "entrada2":
            call entradaEscuela2 from _call_entradaEscuela2
        elif room == "pasilloWc":
            call pasilloWc from _call_pasilloWc_2
        elif room == "banyoHombres":
            call banyoHombres from _call_banyoHombres
        elif room == "banyoMujeres":
            call banyoMujeres from _call_banyoMujeres
        elif room == "pasilloClase":
            call pasilloClase from _call_pasilloClase_1
        elif room == "clase":
            call clase from _call_clase
        elif room == "pasilloProfesores":
            call pasilloProfesores from _call_pasilloProfesores
        elif room == "pasilloDirector":
            call pasilloDirector from _call_pasilloDirector_1
        elif room == "despachoDirector":
            call despachoDirector from _call_despachoDirector
        elif room == "pasilloSuperior":
            call pasilloSuperior from _call_pasilloSuperior
        elif room == "taquillaProta":
            call taquillaProta from _call_taquillaProta
        
    return
