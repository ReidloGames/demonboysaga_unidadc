label zonasTiendaKara():
    if zona == "tiendaKara":
        $ renpy.music.play("audio/music/fretless-by-kevin-macleod.ogg", loop=True, fadeout=1.0, fadein=1.0, if_changed=True)

        if room == "tiendaKara1":
            call tiendaKara1 from _call_tiendaKara1
        elif room == "tiendaKara2":
            call tiendaKara2 from _call_tiendaKara2

    return
