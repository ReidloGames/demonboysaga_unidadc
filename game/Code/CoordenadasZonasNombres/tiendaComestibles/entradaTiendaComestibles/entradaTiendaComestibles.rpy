label entradaTiendaComestibles():
    call screen entradaTiendaComestiblesCoordenadas()

    return

screen entradaTiendaComestiblesCoordenadas():
    imagemap:
        idle "entradaTiendaComestibles"
        if optionsOpenHud == False:
            hover "entradaTiendaComestibles_S"
            alpha False

            #salidaMapa
            hotspot (0, 50, 140, 520):
                action [SetVariable("zona","miMapa"),Call ('nextZona',"miMapa")]
                hovered SetVariable("destino",_("Salida"))
                unhovered SetVariable("destino","")

            #EncargadoTienda
            hotspot (650, 180, 160, 300):
                action Call("tiendaComestiblesInteraccion")
                hovered SetVariable("destino",_("Empleado"))
                unhovered SetVariable("destino","")
