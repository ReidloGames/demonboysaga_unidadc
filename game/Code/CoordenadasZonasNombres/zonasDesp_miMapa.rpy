
label zonasMiMapa():
    if zona == "miMapa":
        $ renpy.music.play("audio/music/Why_Did_You_Do_It__Everet_Almond.ogg", loop=True, fadeout=1.0, fadein=1.0, if_changed=True)
        $ renpy.sound.play("audio/effects/sonidoCiudad.ogg", loop = True)

        if room == "miMapa":
            call sePuedeVolverMiHabitacion() from _call_sePuedeVolverMiHabitacion
            call screen mapaDestino
    return
