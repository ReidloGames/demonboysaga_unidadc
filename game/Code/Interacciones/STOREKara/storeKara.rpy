label storeKara():
    call esconderHud
    scene comprarEquipoGym_0
    menu:
        "Equipo de gimnasia" if tienesEquipoGym == False:
            call comprarEquipoDeGymTiendaKara
        "Nada":
            call mostrarHud
            return



    return




label comprarEquipoDeGymTiendaKara():
    if escenaCompletaComprarTrajeDeporte == False:
        scene comprarEquipoGym_1 with dissolve
        prota "Hola Kara."
        scene comprarEquipoGym_2 with dissolve
        hermanMay "Que sorpresa, ¿que haces por aquí?"
        scene comprarEquipoGym_2_0 with dissolve
        prota "¿Te has oscurecido el pelo?"
        scene comprarEquipoGym_2_1 with dissolve
        hermanMay "Solo es un baño de color."
        scene comprarEquipoGym_2_2 with dissolve
        prota "Te sienta bien."
        stop music fadeout 2.0
        scene comprarEquipoGym_3 with dissolve
        prota "He venido porque quiero empezar a hacer un poco de ejercicio y necesito un traje de gimnasia. Y que mejor que comprarlo en tu tienda."
        scene comprarEquipoGym_4 with dissolve
        play music "audio/music/loopster-by-kevin-macleod.ogg" fadein 2.0

        hermanMay "Hahaha, eso está muy bien, así me gusta."
        hermanMay "Déjame que saque el material..."

        scene minutosMasTarde with dissolve
        "Unos minutos mas tarde..."
        scene comprarEquipoGym_5 with dissolve
        hermanMay "Para chico tengo este."
        scene comprarEquipoGym_6 with dissolve
        hermanMay "Si fuera para chica podría enseñarte mas variedad."
        scene comprarEquipoGym_7 with dissolve
        prota "Ese me gusta. Ya está bien."
        scene comprarEquipoGym_8 with dissolve
        prota "¿Y para chica que tienes?"
        scene comprarEquipoGym_9 with dissolve
        hermanMay "¿Que tienes alguna novia? Hahaha"
        scene comprarEquipoGym_10 with dissolve
        pause
        scene comprarEquipoGym_11 with dissolve
        hermanMay "Este me ha venido esta semana."
        scene comprarEquipoGym_12 with dissolve
        prota "¿Te importaría probártelo para hacerme una idea?"
        scene comprarEquipoGym_13 with dissolve
        hermanMay "¿Ahora? Lo que hay que hacer para vender algo... Hahaha."
        scene comprarEquipoGym_14 with dissolve
        hermanMay "Vigílame un momento la tienda."
        scene comprarEquipoGym_15 with dissolve
        pause
        scene comprarEquipoGym_16 with dissolve
        pause
        scene comprarEquipoGym_17 with dissolve
        protaPensa "Cerro la puerta..."
        scene minutosMasTarde with dissolve
        "Unos minutos mas tarde..."
        scene comprarEquipoGym_18 with dissolve
        prota "Uauuu, te queda genial."
        scene comprarEquipoGym_19 with dissolve
        hermanMay "¿A caso lo dudabas?"
        scene comprarEquipoGym_20 with dissolve
        hermanMay "Hahaha"
        scene comprarEquipoGym_21 with dissolve
        pause
        scene comprarEquipoGym_22 with dissolve
        prota "Oye, hacemos una cosa. ¿Qué tal si compro estos trajes de deporte y este se lo regalas a [nombreMad3]? Seguro que le tranquilizara ver que te van bien las cosas en la tienda. Y de paso se alegrará, recibir un regalo de tu parte."
        scene comprarEquipoGym_23 with dissolve
        play sound "audio/effects/sorpresa4.ogg"
        hermanMay "¿De verdad? Eso es una gran idea."
        scene comprarEquipoGym_24 with dissolve
        prota "Y cógete uno para ti también."
        scene comprarEquipoGym_25 with dissolve
        hermanMay "¿En serio? Hoy estas muy generoso. No sabía que tenías tanto dinero."
        scene comprarEquipoGym_26 with dissolve
        prota "Ehh... ¿cuánto costara?"

        $ escenaCompletaComprarTrajeDeporte = True
        call comprarEquipoGymTiendaKara

        scene comprarEquipoGym_27 with dissolve
        stop music fadeout 2.0
        senyorMay "Disculpe..."
        scene comprarEquipoGym_28 with dissolve
        senyorMay "Estoy buscando trajes de baño para mi mujer."
        scene comprarEquipoGym_29 with dissolve
        protaPensa "La dejare trabajar."
    else:
        call comprarEquipoGymTiendaKara


    call mostrarHud
    return


label comprarEquipoGymTiendaKara():
    menu:
        "Comprar equipos de deporte. 60€":
            if player_gold >= 60:
                "Comprado"
                play sound "audio/effects/money.ogg"
                $ player_gold = player_gold -60
                $ tienesEquipoGym = True
                $ numDiaTotalKaraCompraEquipGym = numDiaTotal

            else:
                "No tienes suficiente dinero"
                prota "Voy a sacar dinero, luego me paso."

    return
