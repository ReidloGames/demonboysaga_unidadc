label primeraVezMad():
    call esconderHud() from _call_esconderHud_10
    if chicaMad == False:
        #$ actionRealizadoMad1 = True
        $ chicaMad = True
        $ madWhats = True
        call avisosStatsSms("newContact") from _call_avisosStatsSms_4
        protaPensa "Ella es mi [situMad]. Se llama [nombreMad]"
        protaPensa "Es muy atractiva, cuando era más joven trabajaba para una revista de moda, pero por alguna razón dejo el empleo.\n{i}{size=15}(Imagino que para poder llevar las tareas de la casa, pero nunca me lo dijo){/size}{/i}"
        protaPensa "Es alta y joven y eso que ha tenido varios hijos, pero sigue teniendo un cuerpo increíble y aparenta menos años de los que tiene."

    return

label primeraVezPad():
    call esconderHud() from _call_esconderHud_11
    if chicoPad == False:
        $ chicoPad = True
        protaPensa "El es mi [situPad]. Se llama [nombrePad]."
        protaPensa "Trabaja de director en un banco."
        protaPensa "Como el gana mucho dinero, [nombreMad3] dejo su carrera de modelo para ocuparse de la casa."
        protaPensa "Gracias a el, vivimos aquí y no tenemos ningún tipo de problema económico"
    return

label primeraVezPeq():
    call esconderHud() from _call_esconderHud_12
    if chicaPeq == False:
        #$ actionRealizadoPeq1 = True
        $ chicaPeq = True
        $ peqWhats = True
        protaPensa "Ella es mi [situPeq2]. Se llama [nombrePeq] y tiene 18 años"
        call avisosStatsSms("newContact") from _call_avisosStatsSms_5
        protaPensa "Siempre me llevado genial con ella. Es muy dulce y la quiero mucho."
        protaPensa "Aunque es hermosa, siempre ha tenido muchos complejos e inseguridades ya que sus hermanas tienen mucho pecho y son mas altas que ella."
    return

label primeraVezMed():
    call esconderHud() from _call_esconderHud_31
    if chicaMed == False:
        #$ actionRealizadoMed1 = True
        $ chicaMed = True
        $ medWhats = True
        protaPensa "Ella se llama [nombreMed] y es mi [situMed2]."
        call avisosStatsSms("newContact") from _call_avisosStatsSms_7
        protaPensa "Nunca nos hemos llevado bien."
        protaPensa "No entiendo el por qué, pero la relación siempre ha sido muy distante."
        protaPensa "No me soporta y apenas he tenido relación más allá de la justa, para poder convivir en paz."
        protaPensa "Es como si yo fuera el ratón y ella el gato."
        protaPensa "Es una persona que le gusta mucho la fiesta y es bastante extrovertida."
        protaPensa "Actualmente esta estudiando un master en psicología."
        protaPensa "No es buena estudiante, pero el dinero de [nombrePad3] ayuda mucho."

    return

label primeraVezMay():
    call esconderHud() from _call_esconderHud_32
    if chicaMay == False:
        #$ actionRealizadoMay1 = True
        $ chicaMay = True
        $ mayWhats = True
        protaPensa "Ella se llama [nombreMay] y es mi [situMay2]."
        call avisosStatsSms("newContact") from _call_avisosStatsSms_8
        protaPensa "[nombreMay] es preciosa y tiene un físico impresionante. Creo que ha cogido los mejores genes de [nombreMad3] y la [nombreAbu2]."
        protaPensa "Es una persona responsable muy dulce y me llevo muy bien con ella."
        protaPensa "Hace relativamente poco abrió su propio negocio (con la ayuda económica de [nombrePad3]). Una tienda de ropa con prendas muy modernas y a la última moda."
        protaPensa "Es novata en el negocio, pero tiene mucha ilusión en que su tienda salga adelante y de sus frutos."

    return

label primeraVezLucia():
    call esconderHud() from _call_esconderHud_33
    if chicaLucia == False:
        $ chicaLucia = True
        protaPensa "Ella se llama [nombreLucia]."
        call avisosStatsSms("newContact") from _call_avisosStatsSms_9
        protaPensa "Es la madre de Mike y es la mujer de la limpieza de la escuela."
        protaPensa "Mike no lleva nada bien que su madre sea la chacha de la escuela e intenta evitarla todo lo que puede."
        protaPensa "[nombreLucia] es una mujer que tiene su encanto. No es tan atractiva como [nombreMad3] pero es muy presumida"
        protaPensa "Solo hay que ver como viste. No debe ser nada cómodo limpiar con esa vestimenta pero a los alumnos ya nos va bien."
        protaPensa "[nombreLucia] no puede permitirse perder el empleo. En casa de Mike las cosas no van muy bien económicamente."

    return
