label opcionesMiVentana():
    if tiempoDia >= 0 and tiempoDia < 4 :
        scene habProtaSgD
    else:
        scene habProtaSgN
    call getObjectInventario("prismaticos") from _call_getObjectInventario

    if resultObject == True:
        call mirarConPrismaticosVentana
    else:
        protaPensa "No tengo prismáticos"
        protaPensa "Si tuviera unos prismáticos podría mirar los edificios del horizonte"

    return



label mirarConPrismaticosVentana():
    call esconderHud()
    if tiempoDia < 4:
        scene miraPrismaticsVentanaLucia_0
    else:
        scene miraPrismaticsVentanaLucia_0N
    menu:
        "Espiar Lucia":
            call espiarLuciaPrismaticos()
        "Nada":
            call mostrarHud()




    return


label espiarLuciaPrismaticos():
    if hasMiradoPorPrismaticosLucia == False:
        play music "audio/music/Marty_Gots_a_Plan.ogg" fadein 1.0

        $ hasMiradoPorPrismaticosLucia = True

        #ESCENA1
        #lunes miercoles viernes domingo (1ra Hora)
        if tiempoDia == 0:
            if numDia == 0 or numDia == 2 or numDia == 4 or numDia == 6:

                $ randomNumLuciaPrismaticos = renpy.random.randint(1, 2)
                if randomNumLuciaPrismaticos == 1:
                    call espiarLuciaPrismaticosEscena1
                else:
                    scene miraPrismaticsVentanaLucia_1
                    protaPensa "No hay nadie."
            else:
                scene miraPrismaticsVentanaLucia_1
                protaPensa "No hay nadie."

        #ESCENA2
        # lunes miercoles viernes domingo (3ra hora )
        elif tiempoDia == 2:
            if numDia == 0 or numDia == 2 or numDia == 4 or numDia == 6:
                $ randomNumLuciaPrismaticos = renpy.random.randint(1, 2)
                if randomNumLuciaPrismaticos == 1:
                    call espiarLuciaPrismaticosEscena2
                else:
                    scene miraPrismaticsVentanaLucia_1
                    protaPensa "No hay nadie."
            else:
                scene miraPrismaticsVentanaLucia_1
                protaPensa "No hay nadie."


        #ESCENA3
        # lunes miercoles viernes domingo (4ta hora)
        elif tiempoDia == 3:
            if numDia == 0 or numDia == 2 or numDia == 4 or numDia == 6:
                $ randomNumLuciaPrismaticos = renpy.random.randint(1, 2)
                if randomNumLuciaPrismaticos == 1:
                    call espiarLuciaPrismaticosEscena3
                else:
                    scene miraPrismaticsVentanaLucia_1
                    protaPensa "No hay nadie."
            else:
                scene miraPrismaticsVentanaLucia_1
                protaPensa "No hay nadie."


        elif tiempoDia == 4:
            scene miraPrismaticsVentanaLucia_3
            pause 1
            scene miraPrismaticsVentanaLucia_4
            protaPensa "Esta durmiendo."

        else:
            scene miraPrismaticsVentanaLucia_1
            protaPensa "No hay nadie."
    else:
        protaPensa "Por ahora ya he tenido suficiente."



    call mostrarHud()
    return
