label espiarLuciaPrismaticosEscena1():
    $ espiarLuciaPrismaticosEscenas1 = True
    scene miraPrismaticsVentanaLucia_5 with dissolve
    pause 0.9
    scene miraPrismaticsVentanaLucia_6 with dissolve
    pause 0.9
    scene miraPrismaticsVentanaLucia_7 with dissolve
    pause 0.9


    return


label espiarLuciaPrismaticosEscena2():
    $ espiarLuciaPrismaticosEscenas2 = True
    scene miraPrismaticsVentanaLucia_8 with dissolve
    pause 0.9
    scene miraPrismaticsVentanaLucia_9 with dissolve
    pause

    return


label espiarLuciaPrismaticosEscena3():
    $ espiarLuciaPrismaticosEscenas3 = True
    scene miraPrismaticsVentanaLucia_11 with dissolve
    pause 0.9
    scene miraPrismaticsVentanaLucia_12 with dissolve
    pause 0.4
    scene miraPrismaticsVentanaLucia_13 with dissolve
    pause 0.9
    scene miraPrismaticsVentanaLucia_14 with dissolve
    pause 0.9
    scene miraPrismaticsVentanaLucia_15 with dissolve
    protaPensa "No sabía que Mike era tan cariñoso..." 

    return
