screen ordenadorOpcionesProta():
    zorder 96
    add "fondoOrdenador"
    frame:
        padding (10,10,10,10)
        xpos 565 ypos 40
        hbox:
            text _("{b}Ordenador [nombreProta]{/b}"):
                size 24
                color "#000000"
    hbox:
        imagebutton:
            xpos 25 ypos 100
            idle "miequipoPc"

        imagebutton:
            xpos 100 ypos 100
            idle "imagenesOrdenador"
            hover "imagenesOrdenador_S"
            action [Hide("ordenadorOpcionesProta"),Call("verImagenesOrdenador")]
    hbox:
        imagebutton:
            xpos 25 ypos 230
            idle "buscador"
            hover "buscador_S"
            action [Hide("ordenadorOpcionesProta"),Call("navegadorOrdenador")]

        if cursoHacking == True:
            imagebutton:
                xpos 100 ypos 230
                idle "troyano"
                hover "troyano_S"
    hbox:
        imagebutton:
            xpos 25 ypos 360
            idle "webcam"
            hover "webcam_S"
            action [Hide("ordenadorOpcionesProta"),Call("webcamPc")]

        imagebutton:
            xpos 100 ypos 360
            idle "utremy"
            hover "utremy_S"
            action [Hide("ordenadorOpcionesProta"),Call("listaCursos")]


        imagebutton:
            xpos 900 ypos 650
            idle "flechaRet"
            hover "flechaReth"
            action [Hide("ordenadorOpcionesProta"),SetVariable("optionsOpenHud",False),Call("mostrarHud")]
