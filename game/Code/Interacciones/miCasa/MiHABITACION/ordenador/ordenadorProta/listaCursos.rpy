label listaCursos():
    scene fondoOrdenador
    if verCursos  == True:
        if tiempoDia == 4:
            protaPensa "Es muy tarde para ponerme hacer cursos"
            call screen ordenadorOpcionesProta
        else:
            call screen ordenadorListaCursos
        return

    else:
        if tiempoDia != 4:
            scene protaEnPc_D
        else:
            scene protaEnPc_N

        protaPensa "No necesito hacer ningun curso"
        call screen ordenadorOpcionesProta
        return



screen ordenadorListaCursos():
    zorder 96
    add "fondoOrdenador"
    frame:
        padding (10,10,10,10)
        xpos 565 ypos 40
        hbox:
            text _("{b}Lista de cursos{/b}"):
                size 24
                color "#000000"
    hbox:
        imagebutton:
            xpos 505 ypos 120
            idle "cursoMassage"
            hover "cursoMassage_S"
            action Call('cursoMassage')



        imagebutton:
            xpos 700 ypos 650
            idle "flechaRetmin"
            hover "flechaRetminh"
            action [Hide("ordenadorListaCursos"),Show('ordenadorOpcionesProta')]
