label navegadorOrdenador():
    scene fondoOrdenador

    if usarNavegador == True:
        menu:
            "Nada":
                call screen ordenadorOpcionesProta
                return
            "Buscar algo 1":
                return

            "Buscar algo 2":
                return
    else:
        if tiempoDia != 4:
            scene protaEnPc_D
        else:
            scene protaEnPc_N

        protaPensa "No necesito buscar nada"
        call screen ordenadorOpcionesProta
        return
