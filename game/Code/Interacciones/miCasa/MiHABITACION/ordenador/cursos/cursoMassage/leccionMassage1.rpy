label leccionMassage1():
    $ leccion1Massage = True
    stop music fadeout 3.0
    scene curso1Massage_0
    play music "audio/music/finding-movement_by_kevin_macleod.ogg" fadein 2.0
    pause
    scene curso1Massage_1 with dissolve
    pause
    scene curso1Massage_2 with dissolve
    mandin "Hola, como ya sabéis soy Chei Mandin."
    scene curso1Massage_3 with dissolve
    mandin "Gracias por apuntaros a estas clases. Os aseguro que no os decepcionaran y que os daran una serie de habilidades únicas."
    scene curso1Massage_4 with dissolve
    mandin "Soy experto en todas las clases y tipos de masajes que existen en el mundo."
    scene curso1Massage_5 with dissolve
    mandin "Pero lo que más quiero es que triunféis en la vida..."
    scene curso1Massage_6 with dissolve
    mandin "Ven aquí Mai"
    play sound "audio/effects/tacones.ogg"
    scene curso1Massage_7 with dissolve
    pause
    scene curso1Massage_8 with dissolve
    pause
    scene curso1Massage_9 with dissolve
    pause
    stop sound fadeout 3.0
    scene curso1Massage_10 with dissolve
    pause
    scene curso1Massage_11 with dissolve
    pause
    scene curso1Massage_12 with dissolve
    pause
    play sound "audio/effects/boing.ogg"
    scene curso1Massage_13 with dissolve
    pause
    scene curso1Massage_14 with dissolve
    mandin "Ya me entendéis..."
    scene curso1Massage_15 with dissolve
    play sound "audio/effects/guinyo.ogg"
    pause
    scene curso1Massage_16 with dissolve
    pause
    scene curso1Massage_17 with dissolve
    mandin "Gracias Mai ya te puedes ir, solo quería que te conocieran mis estudiantes."
    scene curso1Massage_18 with dissolve
    pause
    scene curso1Massage_19 with dissolve
    mandin "Seguimos"
    scene curso1Massage_20 with dissolve
    mandin "Conozco todas las técnicas ancestrales de mi familia que han pasado de generación en generación."
    scene curso1Massage_21 with dissolve
    mandin "Mi tatarabuelo era masajista, mi bisabuelo era masajista, mi abuelo era masajista y mi padre era masajista."
    scene curso1Massage_22 with dissolve
    mandin "Durante todas estas generaciones hemos ido perfeccionando las técnicas."
    play sound "audio/effects/humm.ogg"
    scene curso1Massage_23 with dissolve
    mandin "Yo las llamo 'las técnicas Mandin'."
    scene curso1Massage_24 with dissolve
    mandin "Estas técnicas consisten en abrir los chacras del cuerpo femenino y hacer que la energía fluya."
    play sound "audio/effects/gong2.ogg"
    scene curso1Massage_25 with dissolve
    mandin "Cuando consigáis un buen nivel, os aseguro que ninguna mujer podrá resistir la técnica MANDIN."

    scene curso1Massage_26 with dissolve
    mandin "Empezamos la primera clase."
    scene curso1Massage_27 with dissolve
    mandin "Ven aquí Yun."
    scene curso1Massage_28 with dissolve
    pause
    scene curso1Massage_29 with dissolve
    mandin "Túmbate en la camilla por favor."
    scene curso1Massage_30 with dissolve
    pause
    scene curso1Massage_31 with dissolve
    pause
    play sound "audio/effects/camilla1.ogg"
    scene curso1Massage_32 with dissolve
    pause
    play sound "audio/effects/camilla2.ogg"
    scene curso1Massage_33 with dissolve
    pause
    scene curso1Massage_34 with dissolve
    pause
    scene curso1Massage_35 with dissolve
    pause
    play sound "audio/effects/camilla3.ogg"
    scene curso1Massage_36 with dissolve
    pause
    scene curso1Massage_37 with dissolve
    mandin "Oh espera Yun, mejor boca arriba."
    scene curso1Massage_38 with dissolve
    mandin "Espera que te ayudo."
    scene curso1Massage_39 with dissolve
    pause
    scene curso1Massage_40 with dissolve
    pause
    scene curso1Massage_41 with dissolve
    pause
    play sound "audio/effects/camilla2.ogg"
    scene curso1Massage_42 with hpunch
    pause
    #scene curso1Massage_43 with dissolve
    #pause
    play sound "audio/effects/camilla3.ogg"
    scene curso1Massage_44 with hpunch
    pause
    scene curso1Massage_45 with dissolve
    mandin "¿Todo bien Yun?"
    scene curso1Massage_46 with dissolve
    yun "Si, todo bien."
    scene curso1Massage_47 with dissolve
    mandin "Perfecto Yun, continuamos."
    scene curso1Massage_48 with dissolve
    pause
    scene curso1Massage_49 with dissolve
    pause
    scene curso1Massage_50 with dissolve
    pause
    scene curso1Massage_51 with dissolve
    pause
    scene curso1Massage_52 with dissolve
    mandin "Primera clase, los pies."
    scene curso1Massage_53 with dissolve
    pause
    play sound "audio/effects/roze1.ogg"
    scene curso1Massage_54 with dissolve
    pause
    play sound "audio/effects/roze3.ogg"
    scene curso1Massage_55 with dissolve
    mandin "Hay que transmitir que tú tienes el control de la situación. No hay que dudar y siempre con confianza."
    play sound "audio/effects/camilla2.ogg"
    scene curso1Massage_56 with dissolve
    pause
    scene curso1Massage_57 with dissolve
    pause
    scene curso1Massage_58 with dissolve
    mandin "Los pies tienen muchas terminaciones, tenemos que presionar estos puntos."
    play sound "audio/effects/massagePie1.ogg"
    scene curso1Massage_59 with dissolve
    pause
    scene curso1Massage_60 with dissolve
    pause
    play sound "audio/effects/massagePie2.ogg"
    scene curso1Massage_61 with dissolve
    pause
    scene curso1Massage_62 with dissolve
    mandin "Estos son las zonas y poco a poco, la tensión ira desapareciendo y se ira abriendo y relajando."
    scene curso1Massage_63 with dissolve
    pause
    scene curso1Massage_64 with dissolve
    pause
    scene curso1Massage_65 with dissolve
    mandin "Mirar lo relajada que esta, y como sus músculos no ofrecen resistencia ni tensión."
    scene curso1Massage_66 with dissolve
    pause
    scene curso1Massage_67 with dissolve
    pause
    scene curso1Massage_68 with dissolve
    pause
    scene curso1Massage_69 with dissolve
    pause
    scene curso1Massage_70 with dissolve
    mandin "Esta es la técnica Mandin pero no es fácil alcanzarla."
    play sound "audio/effects/rozeRopa1.ogg"
    scene curso1Massage_71 with dissolve
    pause
    scene curso1Massage_72 with dissolve
    pause
    scene curso1Massage_73 with dissolve
    pause
    play sound "audio/effects/rozeRopa2.ogg"
    scene curso1Massage_74 with dissolve
    pause
    scene curso1Massage_75 with dissolve
    mandin "Como has podido ver, Yun alcanzo un nivel de relajación absoluto."
    scene curso1Massage_76 with dissolve
    mandin "Practica mucho y esfuérzate."
    stop music fadeout 3.0
    scene curso1Massage_77 with dissolve
    pause
    scene curso1Massage_78 with dissolve
    protaPensa "Interesante, tendré que poner en práctica las técnicas Mandin."
    scene curso1Massage_79 with dissolve
    protaPensa "Podría usar a [nombreMad3] para empezar a practicar..."
    scene curso1Massage_80 with dissolve
    protaPensa "Me gusta la idea."

    call nextTiempoDay(0) from _call_nextTiempoDay_26
    call screen ordenadorListaCursos
    return
