label opcionesMiCama:
    if puedoIrADormir == True:
        if llegoTardeClaseAlicia == True:
            if tiempoDia >= 0 and tiempoDia < 4:
                if eventMadRecogeRopaHabProta_visit:
                    scene habProtaSinCesta
                else:
                    scene habProtaSgD
                show screen protaEnCama
                menu:
                    "Dormir hasta el dia siguiente":
                        call nextTiempoDay(4) from _call_nextTiempoDay_1
                        return

                    "Hacer una siesta":
                        call hacerUnaSiesta from _call_hacerUnaSiesta
                        return

                    "Pensar en como hacer yoga con [nombreMad3] y dormir" if preguntaHacerYogaJuntos == True and pensarComoHacerYogaConSofia == False:
                        call piensaComoHacerYogaConSofia()
                        return

                    "Nada":
                        hide screen protaEnCama
                        return
            else:
                scene habProtaSgN
                show screen protaEnCama
                menu:
                    "Dormir hasta el dia siguiente":
                        call dormirDiaSiguiente("") from _call_dormirDiaSiguiente
                        return
                    "Dormir hasta el dia siguiente (en calzoncillos)" if dormirEnBoxers == True:
                        call dormirDiaSiguiente("boxers") from _call_dormirDiaSiguiente_1
                    "Dormir hasta el dia siguiente (desnudo)" if dormirDesnudo == True:
                        call dormirDiaSiguiente("desnudo") from _call_dormirDiaSiguiente_2

                    "Nada":
                        return
        else:
            scene habProtaSgD
            protaPensa "No puedo dormir, tengo que ir a la Universidad"
            return
    else:
        if tiempoDia >= 0 and tiempoDia < 4:
            scene habProtaSgD
            protaPensa "No es momento de ir a la cama"
        else:
            scene habProtaSgN
            if protaHablarConDirectorDinero and not protaHablaConPadrDinero:
                protaPensa "Necesito preguntarle a [situMad] y [situPad] sobre los problemas de dinero."
            else:
                show screen protaEnCama
                menu:
                    "Dormir hasta el dia siguiente":
                        call dormirDiaSiguiente("")

                    "Nada":
                        return
        return




screen protaEnCama():
    #imagemap:
    if tiempoDia != 4:
        add "protaDorm_D" xpos 36 ypos 388
        #idle "protaDorm_D" xpos 36 ypos 388
    else:
        add "protaDorm_N" xpos 36 ypos 388
        #idle "protaDorm_N" xpos 36 ypos 388


label hacerUnaSiesta:
    call nextTiempoDay(0) from _call_nextTiempoDay_2
    return

label dormirDiaSiguiente(tipo):
    if tipo == "":
        call nextTiempoDay(0) from _call_nextTiempoDay_3
    if tipo == "boxers":
        return
    if tipo == "desnudo":
        return
    return
