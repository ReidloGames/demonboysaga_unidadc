label piensaComoHacerYogaConSofia():
    if pensarComoHacerYogaConSofia == False:
        stop music fadeout 3.0
        call esconderHud()
        $ pensarComoHacerYogaConSofia = True
        hide screen protaEnCama
        scene piensaHacerEjercicioConMad_0
        pause
        scene piensaHacerEjercicioConMad_1 with dissolve
        protaPensa "¿Como podría forzar la situación y hacer yoga con [nombreMad3]?"
        scene piensaHacerEjercicioConMad_2 with dissolve
        pause
        play sound "audio/effects/magia.ogg"
        scene piensaHacerEjercicioConMad_2_ with dissolve
        pause(0.2)
        scene piensaHacerEjercicioConMad_3 with dissolve #zoomout#blinds
        pause
        scene piensaHacerEjercicioConMad_4 with dissolve
        pause
        scene piensaHacerEjercicioConMad_5 with dissolve
        pause
        scene piensaHacerEjercicioConMad_6 with dissolve
        pause
        scene piensaHacerEjercicioConMad_7 with dissolve
        pause
        scene piensaHacerEjercicioConMad_1 with dissolve
        protaPensa "¿Y si voy a la biblioteca? seguro que allí encontrare un montón de libros sobre el tema."
        scene piensaHacerEjercicioConMad_2 with dissolve
        protaPensa "Y así, no se podra negar..."
        pause
        call avisosStatsSms("newUbication")
        narrador "Ahora tienes una nueva zona en el mapa.(Biblioteca)"
        $ puedesIrBiblioteca = True
        scene fondoNegro
        "Dormir..."

        call nextTiempoDay(4)
        call mostrarHud()


    return
