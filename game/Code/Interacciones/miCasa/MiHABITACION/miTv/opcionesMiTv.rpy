label opcionesMiTv():
    if puedoVerTv == True:
        if llegoTardeClaseAlicia == True:
            if tiempoDia >= 0 and tiempoDia < 4 :
                scene habProtaSgD
            else:
                scene habProtaSgN
            menu:
                "Jugar a la consola ":
                    call jugarConsola() from _call_jugarConsola
                    return

                "Nada":
                    return
        else:
            scene habProtaSgD
            protaPensa "No puedo jugar a la consola, tengo que ir a la Universidad"
            return
    else:
        if tiempoDia >= 0 and tiempoDia < 4 :
            scene habProtaSgD
        else:
            scene habProtaSgN
        protaPensa "No es momento de jugar a la consola"
        return



label jugarConsola():

    if tiempoDia >= 0 and tiempoDia < 4 :
        call esconderHud() from _call_esconderHud_21
        scene consola1 with dissolve
        pause(0.2)
        scene consola2 with dissolve
        pause(0.2)
        scene consola3 with dissolve
        $ renpy.sound.play("audio/effects/juegoTv.ogg")
        pause(0.2)
        scene consola5 with fade
        pause
        call nextTiempoDay(0) from _call_nextTiempoDay_6
        stop sound

    else:
        protaPensa "Es muy tarde, no tengo ganas de jugar a la consola."

    call mostrarHud() from _call_mostrarHud_18
    return
