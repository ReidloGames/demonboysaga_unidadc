label peqCocinaHaceComida3raInteracMiercoles_visit1():
    $ peqCocinaHaceComida3raInteracMiercoles_visit1__ = True
    $ conversPeqCocinaHaceComida3raMiercoles_Hablado = True
    pause
    stop music fadeout 1.0
    #play sound "audio/effects/freir2.ogg" volume 0.3 loop=True
    $ renpy.sound.play("audio/effects/freir2.ogg", loop = True)
    scene peqCocinaHaceComida3raMiercoles_1 with dissolve
    pause

    scene peqCocinaHaceComida3raMiercoles_2 with dissolve
    pause
    scene peqCocinaHaceComida3raMiercoles_3 with dissolve
    pause
    scene peqCocinaHaceComida3raMiercoles_4 with dissolve
    pause
    scene peqCocinaHaceComida3raMiercoles_5 with dissolve
    pause
    scene peqCocinaHaceComida3raMiercoles_6 with dissolve
    prota "Hola, iba a prepararme la comida."
    prota "¿Comemos juntos?"
    scene peqCocinaHaceComida3raMiercoles_7 with dissolve
    pause
    hermanPeq "¡Hola, Claro!"
    hermanPeq "Ayúdame a preparar la comida."
    scene peqCocinaHaceComida3raMiercoles_9 with dissolve
    pause
    stop sound
    scene minutosMasTarde
    "Unos minutos mas tarde..."
    pause
    scene peqCocinaHaceComida3raMiercoles_11 with dissolve
    play music "audio/music/fretless-by-kevin-macleod.ogg" fadein 2.0
    pause
    scene peqCocinaHaceComida3raMiercoles_14 with dissolve
    call preguntasPeqCocinaHaceComida3raMiercoles() from _call_preguntasPeqCocinaHaceComida3raMiercoles


    return

label preguntasPeqCocinaHaceComida3raMiercoles():
    menu:
        "¿Como te va la Universidad?" if hasHabladoConPeqEnElColeTrabajosAcabados == False:
            scene peqCocinaHaceComida3raMiercoles_10 with dissolve
            hermanPeq "Muy agobiada con muchos trabajos de clase."
            scene peqCocinaHaceComida3raMiercoles_18 with dissolve
            hermanPeq "El profesor Eduard no para de ponernos tareas..."
            scene peqCocinaHaceComida3raMiercoles_12 with dissolve
            prota "Sabes que si necesitas ayuda puedes contar conmigo."
            scene peqCocinaHaceComida3raMiercoles_18 with dissolve
            pause
            scene peqCocinaHaceComida3raMiercoles_16 with dissolve
            hermanPeq "Claro gracias :)"
            call preguntasPeqCocinaHaceComida3raMiercoles() from _call_preguntasPeqCocinaHaceComida3raMiercoles_1


        "¿Y tu como estas?" if hasHabladoConPeqEnElColeTrabajosAcabados == False:
            scene peqCocinaHaceComida3raMiercoles_18 with dissolve
            hermanPeq "Muy agobiada con muchos trabajos de clase."
            scene peqCocinaHaceComida3raMiercoles_13 with dissolve
            prota "Deberíamos hacer algo para que te diviertas y salgas de la rutina de la Universidad."
            scene peqCocinaHaceComida3raMiercoles_17 with dissolve
            call preguntasPeqCocinaHaceComida3raMiercoles() from _call_preguntasPeqCocinaHaceComida3raMiercoles_2

        "Preguntar por como estan las cosas de casa" if demonBoy == True and preguntarCocinaComoEstaTemaPad == False:
            $ preguntarCocinaComoEstaTemaPad = True
            scene peqCocinaPreguntaComoEsta_0 with dissolve
            prota "¿Y cómo estas? por todo lo que ha sucedido estos días en casa"
            scene peqCocinaPreguntaComoEsta_1 with dissolve
            prota "Sabes que puedes contarme todo lo que necesites y que me preocupa que estes bien."
            scene peqCocinaPreguntaComoEsta_2 with dissolve
            hermanPeq "Todo ha sido tan raro..."
            scene peqCocinaPreguntaComoEsta_3 with dissolve
            hermanPeq "Ha sido un shock, aún me cuesta creerme todo esto."
            scene peqCocinaPreguntaComoEsta_4 with dissolve
            hermanPeq "Cualquier cosa que necesites cuenta conmigo. Estoy para ayudarte."
            scene peqCocinaPreguntaComoEsta_5 with dissolve
            call avisosStatsSms("afecto") from _call_avisosStatsSms_13
            $ afectoPeq = afectoPeq +1
            "Despiértala el domingo y animarla."


        "Fin conversación":
            scene peqCocinaHaceComida3raMiercoles_19 with dissolve
            pause
            call nextTiempoDay(0) from _call_nextTiempoDay_20
            return
