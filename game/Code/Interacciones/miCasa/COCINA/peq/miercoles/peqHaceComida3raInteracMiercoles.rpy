label peqHaceComida3raInteracMiercoles():
    call esconderHud() from _call_esconderHud_47
    hide screen cocinaCocinandoMediodiaMiercolesPeq
    scene peqCocinaHaceComida3raMiercoles_0
    call primeraVezPeq() from _call_primeraVezPeq_6

    if conversPeqCocinaHaceComida3raMiercoles_Hablado == False:

        $ conversPeqCocinaHaceComida3raMiercoles_Hablado = True
        call peqCocinaHaceComida3raInteracMiercoles_visit1() from _call_peqCocinaHaceComida3raInteracMiercoles_visit1

    else:
        protaPensa "Ya he hablado con ella"
        scene cocina

    call mostrarHud() from _call_mostrarHud_42

    #show screen cocinaCoordenadas


    return
