label mayCocinaHaceCafe1raInteracSabado_visit1():
    $ conversMayCocinaHaceCafe1raSabado_Hablado = True
    $ mayCocinaHaceCafe1raInteracSabado_visit1__ = True
    if leccion1Massage == True:
        $ mayCocinaHaceCafe1raInteracSabado_visit1Fin = True
    pause
    stop music fadeout 2.0
    play sound "audio/effects/microondas.ogg"
    scene MayCocinaHaceCafe1raSabado_1 with dissolve
    protaPensa "Se está haciendo un café, la dejare tranquila por ahora."
    scene MayCocinaHaceCafe1raSabado_2 with dissolve
    pause
    stop sound fadeout 2.0
    scene MayCocinaHaceCafe1raSabado_3 with dissolve
    pause


    return
