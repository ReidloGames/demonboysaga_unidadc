label mayCocinaHaceCafe1raInteracSabado_visit2():
    stop music fadeout 2.0
    play sound "audio/effects/microondas.ogg"
    scene karaCocinaHablandoConProta_0
    pause
    scene karaCocinaHablandoConProta_1 with dissolve
    menu:
        "Preguntar como esta" if preguntaComoEstaKaraCocinaCafe == False:
            $ preguntaComoEstaKaraCocinaCafe = True
            $ numDiaTotalKaraHablaCafe = numDiaTotal +4
            scene karaCocinaHablandoConProta_2 with dissolve
            hermanMay "Estoy super estresada. Me tomo el café rápido y me voy a la tienda."
            scene karaCocinaHablandoConProta_3 with dissolve
            hermanMay "En los próximos días ha de venir el camión con el nuevo género que compre y aún no sé cómo hare para pagarlo."
            scene karaCocinaHablandoConProta_5 with dissolve
            hermanMay "Seguro que encuentras la manera. Esto solo es un bache. Ya verás que todo ira a mejor."
            scene karaCocinaHablandoConProta_4 with dissolve
            hermanMay "Eso espero..."

        "Nada":
            return

    stop sound
    call nextTiempoDay(0) from _call_nextTiempoDay_31

    return
