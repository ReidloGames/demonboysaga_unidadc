label mayTomaCafe1raInteracSabado():
    #MIERCOLES Y SABADO 1ra Hora
    call esconderHud() from _call_esconderHud_40
    hide screen cocinaMicroAmanecerSabadoMay
    scene MayCocinaHaceCafe1raSabado_0
    call primeraVezMay() from _call_primeraVezMay_4

    if conversMayCocinaHaceCafe1raSabado_Hablado == False:

        $ conversMayCocinaHaceCafe1raSabado_Hablado = True
        if mayCocinaHaceCafe1raInteracSabado_visit1Fin == False:
            call mayCocinaHaceCafe1raInteracSabado_visit1() from _call_mayCocinaHaceCafe1raInteracSabado_visit1

        elif mayCocinaHaceCafe1raInteracSabado_visit1Fin == True:
            call mayCocinaHaceCafe1raInteracSabado_visit2() from _call_mayCocinaHaceCafe1raInteracSabado_visit2


    else:
        protaPensa "Ya he estado aquí"
        scene cocina

    call mostrarHud() from _call_mostrarHud_35

    #show screen cocinaCoordenadas

    return
