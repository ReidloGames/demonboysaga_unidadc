label madCocina3raInteracLunes_visit1():
    $ madCocina3raInteracLunes_visit1 = True
    prota "Hola [nombreMad3]"
    stop music fadeout 3.0
    prota "Que estas haciendo?"
    scene madPreparaComida2 with dissolve
    mad "Hola [nombreProta2]"
    scene madPreparaComida3 with dissolve
    mad "estoy preparando la comida, es un plato que le gusta mucho a tu [situPad], risotto con setas"
    scene madPreparaComida4 with dissolve
    protaPensa "{i}{size=15}Se la ve contenta, le deben ir bien las cosas a mi [situPad]{/size}{/i}"
    prota "Huele muy bien, se te ve feliz ¿hay algo que haya que celebrar?"
    scene madPreparaComida5 with dissolve
    play sound "audio/effects/paloSarten1.ogg"
    mad "Bueno me ha contado tu [situPad] que esta tarde iba a cerrar un contrato importante en el banco y se llevaría una comisión generosa."
    scene madPreparaComida6 with dissolve
    play sound "audio/effects/paloSarten2.ogg"
    prota "Eso es increíble"
    scene madPreparaComida7 with dissolve
    mad "Si, las cosas van muy bien en casa"
    prota "Me alegro mucho, te dejo terminar el trabajo no sea que se te vaya a quemar"
    scene madPreparaComida6 with dissolve
    mad "gracias [nombreProta2]"
    $ escondeFrameAyudasMad = True
    return
