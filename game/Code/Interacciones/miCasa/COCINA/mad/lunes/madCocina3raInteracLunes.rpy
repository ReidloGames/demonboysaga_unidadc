label madCocina3raInteracLunes():
    call esconderHud() from _call_esconderHud_6
    hide screen cocinaMediodiaLunesMad
    scene madPreparaComida1
    call primeraVezMad() from _call_primeraVezMad_1

    if conversMadCocinaPrepaComida3raLunes_Hablado == False:

        if madCocina3raInteracLunes_visit1 == False:
            $ conversMadCocinaPrepaComida3raLunes_Hablado = True
            call madCocina3raInteracLunes_visit1() from _call_madCocina3raInteracLunes_visit1

        else:
            if demonBoy == True:
                call madEstaCocinandoNadaQueDecirRandom() from _call_madEstaCocinandoNadaQueDecirRandom
            protaPensa "No tengo nada mas que decirle"
            scene cocina
    else:
        protaPensa "Ya he hablado con ella"
        scene cocina

    call mostrarHud() from _call_mostrarHud_4
    #show screen cocinaCoordenadas

    return

label madEstaCocinandoNadaQueDecirRandom():
    $ madEstaCocinandoNadaQueDecirRandom = renpy.random.randint(1,2)

    if madEstaCocinandoNadaQueDecirRandom == 1:
        scene madPreparaComidaYaVisto_1_0 with dissolve
        pause
        scene madPreparaComidaYaVisto_1_1 with dissolve

    if madEstaCocinandoNadaQueDecirRandom == 2:
        scene madPreparaComidaYaVisto_2_0 with dissolve
        pause
        scene madPreparaComidaYaVisto_2_1 with dissolve
    return
