label madPideFregar1raInteracDomingo():
    call esconderHud() from _call_esconderHud_20
    hide screen cocinaDesayunoAmanecerDomingoMad
    scene madCocinaDesayuno1raDomingo_00
    call primeraVezMad() from _call_primeraVezMad_8

    if conversMadCocinaPideFregar1raDomingo_Hablado == False:
        
        if madCocinaPideFregar1raInteracDomingo_visit1 == False:
            $ conversMadCocinaPideFregar1raDomingo_Hablado = True
            call madCocinaPideFregar1raInteracDomingo_visit1() from _call_madCocinaPideFregar1raInteracDomingo_visit1

        else:
            protaPensa "No tengo nada mas que decirle"
            scene cocina
    else:
        protaPensa "Ya he hablado con ella"
        scene cocina

    call mostrarHud() from _call_mostrarHud_17
    #show screen cocinaCoordenadas

    return
