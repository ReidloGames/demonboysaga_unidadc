label madCocinaPideFregar1raInteracDomingo_visit1():
    $ madCocinaPideFregar1raInteracDomingo_visit1 = True
    scene madCocinaDesayuno1raDomingo_00
    pause
    scene madCocinaDesayuno1raDomingo_0 with dissolve
    pause
    stop music fadeout 3.0
    scene madCocinaDesayuno1raDomingo_1 with dissolve
    mad "Hola [nombreProta2]"
    scene madCocinaDesayuno1raDomingo_3 with dissolve
    #play sound "audio/effects/closeDoor.ogg"
    $ renpy.sound.play("audio/effects/freir.ogg", loop = True)
    pause(0.2)

    scene madCocinaDesayuno1raDomingo_4 with dissolve
    pause(0.2)
    scene madCocinaDesayuno1raDomingo_5 with dissolve
    mad "Te importaría limpiar los platos que hay en el fregadero?"
    scene madCocinaDesayuno1raDomingo_5 with dissolve
    pause(0.2)
    scene madCocinaDesayuno1raDomingo_10 with dissolve
    pause(0.2)
    scene madCocinaDesayuno1raDomingo_11 with dissolve
    pause(0.2)
    scene madCocinaDesayuno1raDomingo_12 with dissolve
    prota "Oh, claro"
    scene madCocinaDesayuno1raDomingo_13 with dissolve
    pause(0.2)
    scene madCocinaDesayuno1raDomingo_11 with dissolve
    pause(0.2)
    scene madCocinaDesayuno1raDomingo_12 with dissolve
    prota "Lo que necesites [nombreMad3]"
    scene madCocinaDesayuno1raDomingo_15 with dissolve
    pause
    scene madCocinaDesayuno1raDomingo_16 with dissolve
    pause
    scene madCocinaDesayuno1raDomingo_18 at pandown1_0
    pause
    scene madCocinaDesayuno1raDomingo_19 with dissolve
    pause
    stop sound fadeout 3.0
    scene madCocinaDesayuno1raDomingo_20 with dissolve
    pause
    scene madCocinaDesayuno1raDomingo_21 with dissolve
    pause
    scene madCocinaDesayuno1raDomingo_22 with dissolve
    pause
    scene madCocinaDesayuno1raDomingo_23 with dissolve
    pause
    $ renpy.sound.play("audio/effects/lavaPlatos2.ogg", loop = True)
    scene madCocinaDesayuno1raDomingo_24 with dissolve
    pause
    scene madCocinaDesayuno1raDomingo_25 with dissolve
    pause
    scene madCocinaDesayuno1raDomingo_26 with dissolve
    pause
    scene madCocinaDesayuno1raDomingo_27 with dissolve
    pause
    stop sound fadeout 3.0
    scene madCocinaDesayuno1raDomingo_28 with dissolve
    pause
    scene madCocinaDesayuno1raDomingo_29 with dissolve
    pause
    $ puedesLimpiarPlatos = True
    $ platosHoyFregados = True
    return
