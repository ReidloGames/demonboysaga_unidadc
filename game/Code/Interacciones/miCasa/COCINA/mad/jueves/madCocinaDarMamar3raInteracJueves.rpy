label madCocinaDarMamar3raInteracJueves():
    call esconderHud() from _call_esconderHud_9
    hide screen cocinaDarMamarMediodiaJuevesMad
    scene madCocinaDarMamar3raJueves_0
    call primeraVezMad() from _call_primeraVezMad_4

    if conversMadCocinaDarMamar3raJueves_Hablado == False:

        if madCocinaDarMamar3raInteracJueves_visit1 == False:
            $ conversMadCocinaDarMamar3raJueves_Hablado = True
            call madCocinaDarMamar3raInteracJueves_visit1() from _call_madCocinaDarMamar3raInteracJueves_visit1

        else:
            if demonBoy == True:
                call madDarDeMamarNadaQueDecirRandom() from _call_madDarDeMamarNadaQueDecirRandom
            protaPensa "No tengo nada mas que decirle"
            scene cocina

    else:
        call madDarDeMamarNadaQueDecirRandom() from _call_madDarDeMamarNadaQueDecirRandom_1
        protaPensa "Ya he hablado con ella"
        scene cocina

    call mostrarHud() from _call_mostrarHud_7
    #show screen cocinaCoordenadas

    return


label madDarDeMamarNadaQueDecirRandom():
    $ madDarDeMamarNadaQueDecirRandom = renpy.random.randint(1,2)

    if madDarDeMamarNadaQueDecirRandom == 1:
        scene madDarMamarYaVisto_1_0 with dissolve
        pause
        scene madDarMamarYaVisto_1_1 with dissolve

    if madDarDeMamarNadaQueDecirRandom == 2:
        scene madDarMamarYaVisto_2_0 with dissolve
        pause
        scene madDarMamarYaVisto_2_1 with dissolve


    return
