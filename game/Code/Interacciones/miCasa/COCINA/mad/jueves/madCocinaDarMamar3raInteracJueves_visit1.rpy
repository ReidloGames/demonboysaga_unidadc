label madCocinaDarMamar3raInteracJueves_visit1():
    $ madCocinaDarMamar3raInteracJueves_visit1 = True
    stop music fadeout 3.0
    protaPensa "{size=15}{i}Y ahí esta Max. Ahora mismo el niño mas afortunado de la Tierra{/size}{/i}"
    protaPensa "{size=15}{i}A mi tambien me han entrado ganas de leche.{/size}{/i}"
    scene madCocinaDarMamar3raJueves_01 with dissolve
    pause(0.4)
    scene madCocinaDarMamar3raJueves_02 with dissolve
    pause
    $ renpy.sound.play("audio/effects/abrirNevera.ogg", loop = False)
    scene madCocinaDarMamar3raJueves_1 with dissolve
    pause(0.4)
    scene madCocinaDarMamar3raJueves_2 with dissolve
    pause(0.4)
    scene madCocinaDarMamar3raJueves_7 with dissolve
    pause
    scene madCocinaDarMamar3raJueves_8 with dissolve
    pause
    scene madCocinaDarMamar3raJueves_9 with dissolve
    pause
    scene madCocinaDarMamar3raJueves_10 with dissolve
    protaPensa "{size=15}{i}Igual podría hechar un vistazo...{/size}{/i}"
    if not demonBoy:
        protaPensa "{size=15}{i}no, es demasiado arriesgado.{/size}{/i}"
    else:
        play music "audio/music/Sneaky_Snitch.ogg" fadein 3.0
        scene madCocinaDarMamar3raJueves_11 with dissolve
        $ contRetrocede = 0
        call bucleRetrocedeMirarMamar() from _call_bucleRetrocedeMirarMamar


        scene madCocinaDarMamar3raJueves_15 with dissolve
        pause
        scene madCocinaDarMamar3raJueves_16 with dissolve
        pause(0.7)
        scene madCocinaDarMamar3raJueves_17 with dissolve
        pause
        show madLDaDeMamarCocina
        pause(20)
        scene madCocinaDarMamar3raJueves_3 with dissolve
        pause(0.8)
        scene madCocinaDarMamar3raJueves_4 with dissolve
        pause
        scene madCocinaDarMamar3raJueves_5 with dissolve
        protaPensa "{size=15}{i}En mi situación, hasta al papa de Roma le saldrían cuernos{/size}{/i}"
        scene madCocinaDarMamar3raJueves_6 with dissolve
        pause
    call nextTiempoDay(0) from _call_nextTiempoDay_11
    #call nextZona("comedor") from _call_nextZona_10

    return

label bucleRetrocedeMirarMamar():
    while contRetrocede < 3:
        menu:
            "Retroceder" if contRetrocede < 4:
                call retrocedeMirarMamar() from _call_retrocedeMirarMamar
                #return


label retrocedeMirarMamar():
    $ contRetrocede += 1
    if contRetrocede == 1:
        scene madCocinaDarMamar3raJueves_12 with dissolve
    if contRetrocede == 2:
        scene madCocinaDarMamar3raJueves_13 with dissolve
    if contRetrocede == 3:
        scene madCocinaDarMamar3raJueves_14 with dissolve
    return
