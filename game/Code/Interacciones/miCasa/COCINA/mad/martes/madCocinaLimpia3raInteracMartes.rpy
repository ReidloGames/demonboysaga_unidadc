label madCocinaLimpia3raInteracMartes():
    call esconderHud() from _call_esconderHud_4
    hide screen cocinaLimpiaMediodiaMartesMad
    scene madLimpiaCocina_0
    call primeraVezMad() from _call_primeraVezMad

    if conversMadCocinaLimpia3raMartes_Hablado == False:

        if madCocinaLimpia3raInteracMartes_visit1 == False:
            $ conversMadCocinaLimpia3raMartes_Hablado = True
            call madCocinaLimpiaInterac_visit1() from _call_madCocinaLimpiaInterac_visit1
        else:
            if demonBoy == True:
                call madCocinaLimpiandoNadaQueDecirRandom() from _call_madCocinaLimpiandoNadaQueDecirRandom
            protaPensa "No tengo nada mas que decirle"
            scene cocina
    else:
        protaPensa "Ya he hablado con ella"
        scene cocina


    call mostrarHud() from _call_mostrarHud_2
    #show screen cocinaCoordenadas

    return

label madCocinaLimpiandoNadaQueDecirRandom():
    $ madCocinaLimpiandoNadaQueDecirRandom = renpy.random.randint(1,2)

    if madCocinaLimpiandoNadaQueDecirRandom == 1:
        scene madLimpiaCocinaYaVisto_1_0 with dissolve
        pause
        scene madLimpiaCocinaYaVisto_1_1 with dissolve

    if madCocinaLimpiandoNadaQueDecirRandom == 2:
        scene madLimpiaCocinaYaVisto_2_0 with dissolve
        pause
        scene madLimpiaCocinaYaVisto_2_1 with dissolve

    return
