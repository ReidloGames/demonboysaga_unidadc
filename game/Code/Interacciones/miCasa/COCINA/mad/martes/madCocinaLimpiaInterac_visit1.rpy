label madCocinaLimpiaInterac_visit1():
    $ madCocinaLimpia3raInteracMartes_visit1 = True
    if not demonBoy:
        $ puedoVerTv = True
    stop music fadeout 3.0
    scene madLimpiaCocina_0 with dissolve
    pause
    scene madLimpiaCocina_00 with dissolve
    pause
    scene madLimpiaCocina_1 with dissolve
    pause
    $ renpy.sound.play("audio/effects/paloEscoba.ogg", loop = False)
    scene madLimpiaCocina_2 with dissolve
    pause
    scene madLimpiaCocina_3 with dissolve
    pause
    $ renpy.sound.play("audio/effects/abrirNevera.ogg", loop = False)

    scene madLimpiaCocina_4 with dissolve
    pause
    scene madLimpiaCocina_5 with dissolve
    pause
    scene madLimpiaCocina_6 with dissolve
    pause
    scene madLimpiaCocina_7 with dissolve
    pause
    $ renpy.sound.play("audio/effects/caidaPaloGrit.ogg", loop = False)
    scene madLimpiaCocina_8 with dissolve
    pause
    $ renpy.sound.play("audio/effects/golpeCarton.ogg", loop = False)
    scene madLimpiaCocina_9 with dissolve
    pause
    $ renpy.sound.play("audio/effects/cerrarNevera.ogg", loop = False)

    scene madLimpiaCocina_11 with dissolve
    pause
    show madLimpiaSueloCocina
    pause(20)
    scene madLimpiaCocina_12 with dissolve
    protaPensa "Dios mio... aquí no se puede vivir..."
    protaPensa "Sera mejor que me vaya."
    protaPensa "Jugaré algunos videojuegos en mi habitación y hablaré con ambos esta noche."


    call nextTiempoDay(0) from _call_nextTiempoDay_19
    #call nextZona("comedor") from _call_nextZona_15
    return
