label lavaPlatos():
    #hide screen cocinaDesayunoAmanecerDomingoMad
    if platosHoyFregados == True:
        scene cocina
        protaPensa "Los platos ya estan fregados por hoy"
    elif localizaMad == "cocina" or localizaPad == "cocina" or localizaPeq == "cocina" or localizaMed == "cocina" or localizaMay == "cocina" or tiempoDia == 4:
        scene cocina
        protaPensa "Los lavaré en otro momento"
    else:
        call esconderHud() from _call_esconderHud_61
        hide screen cocinaDesayunoAmanecerDomingoMad
        hide madCocinaDesayuno1raDomingo_29
        scene madCocinaDesayuno1raDomingo_23
        stop music
        pause
        $ renpy.sound.play("audio/effects/lavaPlatos2.ogg", loop = True)
        scene madCocinaDesayuno1raDomingo_24 with dissolve
        pause
        scene madCocinaDesayuno1raDomingo_25 with dissolve
        pause
        scene madCocinaDesayuno1raDomingo_26 with dissolve
        pause
        scene madCocinaDesayuno1raDomingo_27 with dissolve
        pause
        stop sound fadeout 3.0
        scene madCocinaDesayuno1raDomingo_28 with dissolve
        pause
        scene madCocinaDesayuno1raDomingo_29 with dissolve
        pause
        if afectoMad == 0 and tiempoDia == 2 or tiempoDia == 3:
            scene platosFregados_0 with dissolve
            protaPensa "Esto ya está limpio."
            scene platosFregados_1 with dissolve
            mad "Ohhhh muchas gracias [nombreProta2], por ayudarme en las tareas de la casa."
            scene platosFregados_2 with dissolve
            pause
            scene platosFregados_3 with dissolve
            prota "De nada [nombreMad3], para lo que necesites estoy aquí."
            scene platosFregados_4 with dissolve
            pause

            call avisosStatsSms("afecto") from _call_avisosStatsSms_15
            $ afectoMad = 1

        $ platosHoyFregados = True
        call nextTiempoDay(0) from _call_nextTiempoDay
        call mostrarHud() from _call_mostrarHud_54
    return


label platosHoyFregados():
    $ platosHoyFregados = True
    return
