label medDePieArmario4taInteracMartes():
    call esconderHud() from _call_esconderHud_28
    hide screen habMedArmarioRopaMartesMed

    scene medEnSuHabitacionDepieArmario4taMartes_0
    call primeraVezMed() from _call_primeraVezMed_2

    if conversEnSuHabitacionDepieArmario4taMartes_Hablado == False:

        $ conversEnSuHabitacionDepieArmario4taMartes_Hablado = True
        call medEnSuHabitacionDepieArmario4taInteracMartes_visit1() from _call_medEnSuHabitacionDepieArmario4taInteracMartes_visit1

    else:
        protaPensa "Ya he hablado con ella"
        if tiempoDia != 4:
            scene habHijMedSgD
        else:
            scene habHijMedSgN

    call mostrarHud() from _call_mostrarHud_26

    #show screen habHermanMedCoordenadas

    return
