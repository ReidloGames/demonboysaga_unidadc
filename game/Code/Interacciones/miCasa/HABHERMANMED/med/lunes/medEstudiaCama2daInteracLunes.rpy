label medEstudiaCama2daInteracLunes():
    call esconderHud() from _call_esconderHud_22
    hide screen habMedEstudiaCamaManyanaLunesMed

    scene medEnSuHabitacionEstudia2daLunes_0
    call primeraVezMed() from _call_primeraVezMed

    if conversMedEnSuHabitacionEstudia2daLunes_Hablado == False:

        $ conversMedEnSuHabitacionEstudia2daLunes_Hablado = True
        call medEnSuHabitacionEstudia2daInteracLunes_visit1() from _call_medEnSuHabitacionEstudia2daInteracLunes_visit1

    else:
        protaPensa "Ya he hablado con ella"
        if tiempoDia != 4:
            scene habHijMedSgD
        else:
            scene habHijMedSgN


    call mostrarHud() from _call_mostrarHud_20

    #show screen habHermanMedCoordenadas
    return
