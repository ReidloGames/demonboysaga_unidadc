label mayEnPc4taInteracJueves():
    call esconderHud() from _call_esconderHud_23
    hide screen habMayEnSuPcTardeJuevesMay
    scene mayHabMayEnPc4taMartes_0
    call primeraVezMay() from _call_primeraVezMay

    if conversMayHabMayEnPc4taJueves_Hablado == False:
        $ conversMayHabMayEnPc4taJueves_Hablado = True
        call mayHabMayEnPc4taInteracJueves_visit1() from _call_mayHabMayEnPc4taInteracJueves_visit1

    else:
        protaPensa "Ya he hablado con ella"
        if tiempoDia != 4:
            scene habHijMaySgD
        else:
            scene habHijMaySgN

    call mostrarHud() from _call_mostrarHud_21

    #show screen habHermanMayCoordenadas

    return
