label mayHabMayEnPc4taInteracMartes_visit1():
    $ mayHabMayEnPc4taInteracMartes_visit1__ = True
    $ conversMayHabMayEnPc4taMartes_Hablado = True
    pause
    scene mayHabMayEnPc4taMartes_1 with dissolve
    pause
    play music "audio/music/Scheming_Weasel_faster.ogg" fadein 3.0
    scene mayHabMayEnPc4taMartes_2 with dissolve
    pause
    scene mayHabMayEnPc4taMartes_3 with dissolve
    pause
    scene mayHabMayEnPc4taMartes_4 with dissolve
    pause
    scene mayHabMayEnPc4taMartes_5 with dissolve
    prota "Hola, ¿estas trabajado?"
    scene mayHabMayEnPc4taMartes_15 with dissolve
    pause
    scene mayHabMayEnPc4taMartes_16 with dissolve
    hermanMay "Si, estoy seleccionando los pedidos para la tienda."
    scene mayHabMayEnPc4taMartes_9 with dissolve
    pause
    scene mayHabMayEnPc4taMartes_10 with dissolve
    prota "Cuando los tengas ya me los enseñaras."
    scene mayHabMayEnPc4taMartes_15 with dissolve
    pause
    scene mayHabMayEnPc4taMartes_16 with dissolve
    pause
    scene mayHabMayEnPc4taMartes_17 with dissolve
    hermanMay "¿Desde cuando tienes interés por la moda?"
    scene mayHabMayEnPc4taMartes_18 with dissolve
    hermanMay "jajajaja"
    scene mayHabMayEnPc4taMartes_11 with dissolve
    pause
    scene mayHabMayEnPc4taMartes_12 with dissolve
    prota "Solo me preocupo por las cosas de mi [situPeq2] favorita."
    scene mayHabMayEnPc4taMartes_22 with dissolve
    pause
    scene mayHabMayEnPc4taMartes_23 with dissolve
    hermanMay "Ya claro…"
    scene mayHabMayEnPc4taMartes_33 with dissolve
    prota "Te dejo trabajar."
    scene mayHabMayEnPc4taMartes_34 with dissolve
    pause
    scene mayHabMayEnPc4taMartes_35 with dissolve
    pause
    scene mayHabMayEnPc4taMartes_36 with dissolve
    pause


    return
