label mayEnPc4taInteracMartes():
    call esconderHud() from _call_esconderHud_25
    hide screen habMayEnSuPcTardeMartesMay
    scene mayHabMayEnPc4taMartes_0
    call primeraVezMay() from _call_primeraVezMay_1

    if conversMayHabMayEnPc4taMartes_Hablado == False:

        $ conversMayHabMayEnPc4taMartes_Hablado = True
        call mayHabMayEnPc4taInteracMartes_visit1() from _call_mayHabMayEnPc4taInteracMartes_visit1

    else:
        protaPensa "Ya he hablado con ella"

    call mostrarHud() from _call_mostrarHud_23

    #show screen habHermanMayCoordenadas

    return
