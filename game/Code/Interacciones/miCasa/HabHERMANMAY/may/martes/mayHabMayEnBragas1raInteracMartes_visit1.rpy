label mayHabMayEnBragas1raInteracMartes_visit1():
    $ mayHabMayEnBragas1raInteracMartes_visit1 = True
    pause
    play music "audio/music/OctoBlues.ogg"
    scene mayHabMayEnBragas1raMartes_Scroll at pandown2
    pause
    scene mayHabMayEnBragas1raMartes_2 with dissolve
    pause
    scene mayHabMayEnBragas1raMartes_3 with dissolve
    pause
    scene mayHabMayEnBragas1raMartes_4 with dissolve
    pause
    scene mayHabMayEnBragas1raMartes_5 with dissolve
    pause
    scene mayHabMayEnBragas1raMartes_6 with dissolve
    pause
    scene mayHabMayEnBragas1raMartes_7 with dissolve
    hermanMay "¿Vaya [nombreProta2], no sabes llamar a la puerta?"
    scene mayHabMayEnBragas1raMartes_8 with dissolve
    pause
    scene mayHabMayEnBragas1raMartes_9 with dissolve
    pause
    scene mayHabMayEnBragas1raMartes_10 with dissolve
    prota "Perdón la puerta no estaba cerrada y..."
    scene mayHabMayEnBragas1raMartes_11 with dissolve
    pause
    scene mayHabMayEnBragas1raMartes_12 with dissolve
    pause
    scene mayHabMayEnBragas1raMartes_13 with dissolve
    pause
    scene mayHabMayEnBragas1raMartes_14 with dissolve
    pause
    scene mayHabMayEnBragas1raMartes_15 with dissolve
    hermanMay "A lo mejor [nombreMed] tiene razón y eres un pervertido."
    scene mayHabMayEnBragas1raMartes_16 with dissolve
    pause
    scene mayHabMayEnBragas1raMartes_17 with dissolve
    pause
    scene mayHabMayEnBragas1raMartes_18 with dissolve
    pause
    scene mayHabMayEnBragas1raMartes_19 with dissolve
    pause
    scene mayHabMayEnBragas1raMartes_20 with dissolve
    hermanMay "Tranquilo estaba bromeando... Tampoco te vas asustar por ver a tu [situMay] en bragas. Pero no te acostumbres."
    scene mayHabMayEnBragas1raMartes_21 with dissolve
    pause
    scene mayHabMayEnBragas1raMartes_22 with dissolve
    pause
    scene mayHabMayEnBragas1raMartes_23 with dissolve
    pause
    scene mayHabMayEnBragas1raMartes_24 with dissolve
    pause
    scene mayHabMayEnBragas1raMartes_25 with dissolve
    protaPensa "{size=15}{i}Todo lo contrario {/size}{/i}"
    scene mayHabMayEnBragas1raMartes_26 with dissolve
    protaPensa "{size=15}{i}Que gran vista {/size}{/i}"
    scene mayHabMayEnBragas1raMartes_27 with dissolve
    pause
    scene mayHabMayEnBragas1raMartes_28 with dissolve
    prota "Solo pasaba a saludar y saber cómo te iba todo."
    scene mayHabMayEnBragas1raMartes_29 with dissolve
    pause
    scene mayHabMayEnBragas1raMartes_30 with dissolve
    pause
    scene mayHabMayEnBragas1raMartes_37 with dissolve
    pause
    scene mayHabMayEnBragas1raMartes_38 with dissolve
    hermanMay "Muy bien, estaba ordenando un poco la ropa del armario y aparte de eso muy contenta porque voy a realizar unos pedidos para la tienda de ropa."
    scene mayHabMayEnBragas1raMartes_39 with dissolve
    pause
    scene mayHabMayEnBragas1raMartes_43 with dissolve
    hermanMay "Hable con [nombrePad3] hace unos días y me lo financiara todo."
    scene mayHabMayEnBragas1raMartes_44 with dissolve
    hermanMay "Estoy muy feliz de tener mi propio negocio y creo que estos conjuntos de ropa le darán un plus diferente a la tienda."
    scene mayHabMayEnBragas1raMartes_45 with dissolve
    pause
    scene mayHabMayEnBragas1raMartes_46 with dissolve
    pause
    scene mayHabMayEnBragas1raMartes_47 with dissolve
    protaPensa "En esta casa todos dependen de [nombrePad3] es increíble."
    scene mayHabMayEnBragas1raMartes_48 with dissolve
    prota "Me alegro que te vaya todo bien"
    scene mayHabMayEnBragas1raMartes_49 with dissolve
    hermanMay "¡Si! Gracias. "
    scene mayHabMayEnBragas1raMartes_50 with dissolve
    pause
    scene mayHabMayEnBragas1raMartes_51 with dissolve
    $ escondeFrameAyudasMay = True
    pause

    return
