label mayEnBragas1raInteracMartes():
    call esconderHud() from _call_esconderHud_37
    hide screen habMayEnBragasAmanecerMartesMay
    scene mayHabMayEnBragas1raMartes_3
    call primeraVezMay() from _call_primeraVezMay_3

    if conversMayHabMayEnBragas1raMartes_Hablado == False:

        if mayHabMayEnBragas1raInteracMartes_visit1 == False:
            $ conversMayHabMayEnBragas1raMartes_Hablado = True
            call mayHabMayEnBragas1raInteracMartes_visit1() from _call_mayHabMayEnBragas1raInteracMartes_visit1

        else:
            protaPensa "No tengo nada mas que decirle"
            if tiempoDia != 4:
                scene habHijMaySgD
            else:
                scene habHijMaySgN
    else:
        protaPensa "Ya he hablado con ella"
        if tiempoDia != 4:
            scene habHijMaySgD
        else:
            scene habHijMaySgN

    call mostrarHud() from _call_mostrarHud_32
    #show screen habHermanMayCoordenadas

    return
