label peqEstiradaSofa4taInteracJueves():
    call esconderHud() from _call_esconderHud_39
    hide screen salonTardeJuevesPeq
    hide screen salonMiraMoviTardeJuevesMed
    scene peqEstiradaSofa4taJueves_0

    call primeraVezPeq() from _call_primeraVezPeq_5

    if conversPeqSalonEstiradaSofa4taJueves_Hablado == False:

        $ conversPeqSalonEstiradaSofa4taJueves_Hablado = True
        call peqSalonEstiradaSofa4taInteracJueves_visit1() from _call_peqSalonEstiradaSofa4taInteracJueves_visit1

    else:
        protaPensa "Ya he hablado con ella"
        scene salon

    call mostrarHud() from _call_mostrarHud_34

    #show screen salonCoordenadas

    return
