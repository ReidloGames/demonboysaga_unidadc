label peqSalonEstiradaSofa4taInteracJueves_visit1():
    $ peqSalonEstiradaSofa4taInteracJueves_visit1__ = True
    $ conversPeqSalonEstiradaSofa4taJueves_Hablado = True
    scene peqEstiradaSofa4taJueves_1
    pause
    stop music fadeout 5.0
    scene peqEstiradaSofa4taJueves_2 with dissolve
    play music "audio/music/Marty_Gots_a_Plan.ogg" fadein 2.0
    pause
    scene peqEstiradaSofa4taJueves_3 with dissolve
    prota "Hola [nombrePeq] ¿Puedo sentarme?"
    scene peqEstiradaSofa4taJueves_4 with dissolve
    pause
    scene peqEstiradaSofa4taJueves_5 with dissolve
    hermanPeq "Por supuesto, siéntate."
    scene peqEstiradaSofa4taJueves_05 with dissolve
    pause
    scene peqEstiradaSofa4taJueves_6 with dissolve
    $ golpearMed = False
    call menuCosquillasPies from _call_menuCosquillasPies

    return

label menuCosquillasPies():
    menu:
        "Hacer cosquillas en los pies de [nombrePeq]":
            if golpearMed == True:
                call hacerCosquillas from _call_hacerCosquillas
            else:
                call noCosquillas from _call_noCosquillas

            return
        "Golpear [nombreMed]" if golpearMed == False:
            call golpeaMed from _call_golpeaMed
            return



label noCosquillas():
    $ config.rollback_enabled = False
    $ golpearMed = False
    scene peqEstiradaSofa4taJueves_26 with dissolve
    pause
    scene peqEstiradaSofa4taJueves_27 with dissolve
    pause
    protaPensa "No puedo, [nombreMed] está a mi lado y me observa."
    scene peqEstiradaSofa4taJueves_25 with dissolve
    pause
    $ config.rollback_enabled = True
    call nextTiempoDay(0) from _call_nextTiempoDay_17
    return

label hacerCosquillas():
    $ golpearMed = False
    scene peqEstiradaSofa4taJueves_11 with dissolve
    pause
    scene peqEstiradaSofa4taJueves_12 with dissolve
    pause
    scene peqEstiradaSofa4taJueves_13 with dissolve
    pause
    scene peqEstiradaSofa4taJueves_14 with dissolve
    pause
    play sound "audio/effects/risas1.ogg"
    scene peqEstiradaSofa4taJueves_15 with dissolve
    pause
    scene peqEstiradaSofa4taJueves_16 with dissolve
    hermanPeq "jajajaja"
    scene peqEstiradaSofa4taJueves_17 with dissolve
    hermanPeq "¡¡¡Me haces cosquillas malo!!! ajajaja"
    scene peqEstiradaSofa4taJueves_18 with dissolve
    pause
    scene peqEstiradaSofa4taJueves_018 with dissolve
    pause
    scene peqEstiradaSofa4taJueves_19 with dissolve
    pause
    play sound "audio/effects/risas2.ogg"
    scene peqEstiradaSofa4taJueves_20 with dissolve
    pause
    scene peqEstiradaSofa4taJueves_21 with dissolve
    pause
    scene peqEstiradaSofa4taJueves_22 with dissolve
    hermanPeq "jajajaja"
    hermanPeq "Pórtate bien o tendrás guerra"
    scene peqEstiradaSofa4taJueves_23 with dissolve
    pause
    scene peqEstiradaSofa4taJueves_24 with dissolve
    pause
    scene peqEstiradaSofa4taJueves_25 with dissolve
    pause
    call nextTiempoDay(0) from _call_nextTiempoDay_18

    return

label golpeaMed():
    $ golpearMed = True
    play sound "audio/effects/golpeAu1.ogg"
    scene peqEstiradaSofa4taJueves_7 with dissolve
    pause
    scene peqEstiradaSofa4taJueves_8 with dissolve
    pause
    scene peqEstiradaSofa4taJueves_9 with dissolve
    hermanMed "Serás idiota."
    scene peqEstiradaSofa4taJueves_10 with dissolve
    pause
    scene peqEstiradaSofa4taJueves_25 with dissolve
    pause
    call menuCosquillasPies() from _call_menuCosquillasPies_1
    return
