label familyViendoTv4taInteracViernes_visit1(num):
    if num == 4:
        call viendoTvLos4 from _call_viendoTvLos4
    elif num == 5:
        call viendoTvLos5 from _call_viendoTvLos5

    return



label viendoTvLos4():
    $ conversfamilyViendoTv4taViernes_Hablado = True
    pause
    scene todosViendoTvSinPad_1 with dissolve
    stop music fadeout 2.0
    pause
    scene todosViendoTvSinPad_2 with dissolve
    "Te estábamos esperando."
    "Vamos a ver una peli."
    scene todosViendoTvSinPad_4 with dissolve
    pause
    scene todosViendoTvSinPad_5 with dissolve
    menu:
        "Sentarte con [nombreMay]":
            scene todosViendoTvSinPad_6 with dissolve
            pause
            play sound "audio/effects/sientaSofa.ogg"
            scene todosViendoTvSinPad_7 with hpunch
            pause
            scene todosViendoTvSinPad_15 with dissolve
            play sound "audio/effects/efectTv.ogg"
            pause
            stop sound fadeout 2.0
            scene todosViendoTvSinPad_16 with dissolve
            pause
            call avanzaTiempoFamilyTv() from _call_avanzaTiempoFamilyTv

        "Sentarte con [nombreMed]":
            scene todosViendoTvSinPad_11 with dissolve
            pause
            play sound "audio/effects/sientaSofa.ogg"
            scene todosViendoTvSinPad_8 with hpunch
            pause
            scene todosViendoTvSinPad_15 with dissolve
            play sound "audio/effects/efectTv.ogg"
            pause
            stop sound fadeout 2.0
            scene todosViendoTvSinPad_16 with dissolve
            pause
            call avanzaTiempoFamilyTv() from _call_avanzaTiempoFamilyTv_1

        "Sentarte con [nombreMad3]":
            scene todosViendoTvSinPad_12 with dissolve
            pause
            play sound "audio/effects/sientaSofa.ogg"
            scene todosViendoTvSinPad_9 with hpunch
            pause
            scene todosViendoTvSinPad_15 with dissolve
            play sound "audio/effects/efectTv.ogg"
            pause
            stop sound fadeout 2.0
            scene todosViendoTvSinPad_16 with dissolve
            pause
            call avanzaTiempoFamilyTv() from _call_avanzaTiempoFamilyTv_2

        "Sentarte con [nombrePeq]":
            scene todosViendoTvSinPad_13 with dissolve
            pause
            play sound "audio/effects/sientaSofa.ogg"
            scene todosViendoTvSinPad_10 with hpunch
            pause
            scene todosViendoTvSinPad_15 with dissolve
            play sound "audio/effects/efectTv.ogg"
            pause
            stop sound fadeout 2.0
            scene todosViendoTvSinPad_16 with dissolve
            pause
            call avanzaTiempoFamilyTv() from _call_avanzaTiempoFamilyTv_3
    return



label viendoTvLos5():
    $ conversfamilyViendoTv4taViernes_Hablado = True
    pause
    scene todosViendoTvConPad_1 with dissolve
    stop music fadeout 2.0
    pause
    scene todosViendoTvConPad_2 with dissolve
    "Te estábamos esperando."
    "Vamos a ver una peli."
    scene todosViendoTvConPad_4 with dissolve
    pause
    scene todosViendoTvConPad_5 with dissolve
    menu:
        "Sentarte con [nombreMay]":
            scene todosViendoTvConPad_6 with dissolve
            pause
            play sound "audio/effects/sientaSofa.ogg"
            scene todosViendoTvConPad_7 with hpunch
            pause
            scene todosViendoTvSinPad_15 with dissolve
            play sound "audio/effects/efectTv.ogg"
            pause
            stop sound fadeout 2.0
            scene todosViendoTvSinPad_16 with dissolve
            pause
            call avanzaTiempoFamilyTv() from _call_avanzaTiempoFamilyTv_4

        "Sentarte con [nombreMed]":
            scene todosViendoTvConPad_11 with dissolve
            pause
            play sound "audio/effects/sientaSofa.ogg"
            scene todosViendoTvConPad_8 with hpunch
            pause
            scene todosViendoTvSinPad_15 with dissolve
            play sound "audio/effects/efectTv.ogg"
            pause
            stop sound fadeout 2.0
            scene todosViendoTvSinPad_16 with dissolve
            pause
            call avanzaTiempoFamilyTv() from _call_avanzaTiempoFamilyTv_5

        "Sentarte con [nombreMad3]":
            scene todosViendoTvSinPad_12 with dissolve
            pause
            play sound "audio/effects/sientaSofa.ogg"
            scene todosViendoTvSinPad_9 with hpunch
            pause
            scene todosViendoTvSinPad_15 with dissolve
            play sound "audio/effects/efectTv.ogg"
            pause
            stop sound fadeout 2.0
            scene todosViendoTvSinPad_16 with dissolve
            pause
            call avanzaTiempoFamilyTv() from _call_avanzaTiempoFamilyTv_6

        "Sentarte con [nombrePeq]":
            scene todosViendoTvSinPad_13 with dissolve
            pause
            play sound "audio/effects/sientaSofa.ogg"
            scene todosViendoTvSinPad_10 with hpunch
            pause
            scene todosViendoTvSinPad_15 with dissolve
            play sound "audio/effects/efectTv.ogg"
            pause
            stop sound fadeout 2.0
            scene todosViendoTvSinPad_16 with dissolve
            pause
            call avanzaTiempoFamilyTv() from _call_avanzaTiempoFamilyTv_7

    return


label avanzaTiempoFamilyTv():
    call nextTiempoDay(0) from _call_nextTiempoDay_12
    return
