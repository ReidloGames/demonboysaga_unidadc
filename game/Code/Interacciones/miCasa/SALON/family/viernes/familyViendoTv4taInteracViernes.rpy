label familyViendoTv4taInteracViernes(num):

    call esconderHud() from _call_esconderHud_42
    hide screen salonMiraTvTardeViernesFamily5
    hide screen salonMiraTvTardeViernesFamily4
    if num == 5:
        scene todosViendoTvConPad_0

    elif num == 4:
        scene todosViendoTvSinPad_0

    if conversfamilyViendoTv4taViernes_Hablado == False:

        $ conversfamilyViendoTv4taViernes_Hablado = True
        call familyViendoTv4taInteracViernes_visit1(num) from _call_familyViendoTv4taInteracViernes_visit1

    else:
        protaPensa "Ya he estado con ellos"
        scene salon

    call mostrarHud() from _call_mostrarHud_37

    #show screen salonCoordenadas

    return
