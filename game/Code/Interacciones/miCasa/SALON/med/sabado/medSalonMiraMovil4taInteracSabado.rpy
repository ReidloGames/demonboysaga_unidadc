label medSalonMiraMovil4taInteracSabado():
    call esconderHud() from _call_esconderHud_46
    hide screen salonMiraMovilTardeSabadoMed
    scene medSalonMiraMovil4taSabado_0
    call primeraVezMed() from _call_primeraVezMed_4

    if conversMedSalonMiraMovil4taSabado_Hablado == False:

        $ conversMedSalonMiraMovil4taSabado_Hablado = True
        call medSalonMiraMovil4taInteracSabado_visit1() from _call_medSalonMiraMovil4taInteracSabado_visit1

    else:
        protaPensa "Ya he hablado con ella"
        scene salon

    call mostrarHud() from _call_mostrarHud_41

    #show screen salonCoordenadas

    return
