label verTvSalon():
    if llegoTardeClaseAlicia == True:
        if tiempoDia >= 0 and tiempoDia < 4 and demonBoy:
            if localizaMad == "salon" or localizaPad == "salon" or localizaPeq == "salon" or localizaMed == "salon" or localizaMay == "salon":
                scene salon
                protaPensa "Ahora no es momento de ver la Tv."
            else:
                call esconderHud() from _call_esconderHud_15
                scene salon
                menu:
                    "Ver Tv":
                        call verTvSalonSolo from _call_verTvSalonSolo
                        return
                    "No ver Tv":
                        call mostrarHud from _call_mostrarHud_19
                        return
        elif not demonBoy:
            call verTvSalonPrimerDia

        else:
            scene salon
            protaPensa "Es muy tarde, no tengo ganas de ver la Tv."
        call mostrarHud() from _call_mostrarHud_9
    else:
        scene salon
        protaPensa "Ahora no puedo ver la Tv. Tengo que ir a la Universidad"

    return

label verTvSalonPrimerDia:
    protaPensa "No puedo ver la televisión en este momento."
    return

label verTvSalonSolo():
    scene miraTvSalon_0
    pause
    stop music fadeout 2.0
    #scene miraTvSalon_1 with dissolve
    #pause(0.4)
    #scene miraTvSalon_2 with dissolve
    #pause(0.4)
    #scene miraTvSalon_3 with dissolve
    #pause(0.3)
    scene miraTvSalon_4 with dissolve
    pause(0.3)
    scene miraTvSalon_5 with dissolve
    pause(0.3)
    scene miraTvSalon_6 with dissolve
    pause(0.3)
    scene miraTvSalon_7 with dissolve
    pause(0.3)
    play sound "audio/effects/sientaSofa.ogg"
    scene miraTvSalon_8 with hpunch
    pause(0.3)
    scene miraTvSalon_9 with dissolve
    if pensarEnKaraPaja == False:
        if eventKaraEricaSeVanDeCompras:# and (cambiandoseMayEscena1 == True or cambiandoseMayEscena2 == True or cambiandoseMayEscena3 == True):
            call pensarEnKaraPaja() from _call_pensarEnKaraPaja
        else:
            call continuarMirandoTv() from _call_continuarMirandoTv
    else:
        call continuarMirandoTv() from _call_continuarMirandoTv_1
    #########

    call nextTiempoDay(0) from _call_nextTiempoDay_5
    pause
    return


label continuarMirandoTv():
    play sound "audio/effects/vozTv.ogg"
    pause
    scene miraTvSalon_10 with dissolve
    pause
    scene miraTvSalon_11 with dissolve
    pause
    stop sound fadeout 3.0
    scene miraTvSalon_12 with dissolve
    pause
    scene miraTvSalon_13 with dissolve
    pause
    scene miraTvSalon_14 with dissolve

    return
