label madSeVaConEthan():
    $ haVenidoEthan1 = True
    scene madSofaRevistaMasagePies_4 with dissolve
    pause
    scene madSofaRevistaMasagePies_5 with dissolve
    mad "Esta bien, un masaje en los pies no me ira nada mal."
    scene madSofaRevistaMasagePies_6 with dissolve
    prota "¡Genial!"
    scene madSofaRevistaMasagePies_7 with dissolve
    pause
    scene madSofaRevistaMasagePies_8 with dissolve
    pause
    scene madSofaRevistaMasagePies_9 with dissolve
    pause
    play sound "audio/effects/timbre.ogg"
    stop music fadeout 2.0
    scene madSofaRevistaMasagePies_10 with dissolve
    "¡Ding Dong!"
    scene madSofaRevistaMasagePies_3 with dissolve
    mad "Ya abro yo la puerta."
    scene madSofaRevistaMasagePies_12 with dissolve
    protaPensa "¿Joder, quien será ahora?"
    scene madSofaRevistaMasagePies_13 with dissolve
    pause
    play sound "audio/effects/abrirTaquilla.ogg"
    scene madSofaRevistaMasagePies_14 with dissolve
    protaPensa "Ese es Ethan, el vecino y amigo de [situPad2]. Suele venir a ver los partidos con el."
    protaPensa "Lo he pillado varias veces mirando a [nombreMad3] de una manera poco sana. No me fio de el."
    scene madSofaRevistaMasagePies_15 with dissolve
    ethan "Hola Sofia."
    scene madSofaRevistaMasagePies_16 with dissolve
    mad "Oh Ethan, que sorpresa."
    scene madSofaRevistaMasagePies_17 with dissolve
    pause
    scene madSofaRevistaMasagePies_18 with dissolve
    mad "Perdona que estoy con el pijama."
    play sound "audio/effects/rozeRopaFuerte.ogg"
    scene madSofaRevistaMasagePies_19 with dissolve
    pause
    scene madSofaRevistaMasagePies_20 with dissolve
    ethan "Oh no te preocupes, siento la molestia."
    scene madSofaRevistaMasagePies_21 with dissolve
    mad "No eres una molestia Ethan."
    scene madSofaRevistaMasagePies_22 with dissolve
    ethan "Gracias, siempre tan amable Sofia. He venido porque me quedado sin sal, y me preguntaba si me podías dar un poco."
    scene madSofaRevistaMasagePies_23 with dissolve
    mad "Oh claro no hay problema, pasa dame 1 min."
    scene madSofaRevistaMasagePies_14 with dissolve
    protaPensa "¿Sal? menuda mierda de excusa..."
    scene fondoNegro with dissolve
    "Unos segundos más tarde..."
    scene madSofaRevistaMasagePies_24 with dissolve
    mad "Toma Ethan coge la que necesites."
    scene madSofaRevistaMasagePies_25 with dissolve
    ethan "Genial, muchas gracias. ¿Y dónde está John? hace días que no le veo y tampoco me ha llamado"
    scene madSofaRevistaMasagePies_26 with dissolve
    mad "Oh bueno, ha habido algún pequeño problema y está trabajando fuera, pero todo bien."
    scene madSofaRevistaMasagePies_27 with dissolve
    ethan "Que raro no me dijo nada... Por cierto, me comprado un violoncello, por favor avisarme si molesto al tocarlo."
    scene madSofaRevistaMasagePies_28 with dissolve
    mad "Ohh que sorpresa, no sabía que tocabas instrumentos. Yo cuando era pequeña di un par de clases con el violoncello y me encantaba, pero ya hace mucho tiempo de eso y ya no me acuerdo de nada."
    scene madSofaRevistaMasagePies_29 with dissolve
    pause
    scene madSofaRevistaMasagePies_30 with dissolve
    pause
    scene madSofaRevistaMasagePies_31 with dissolve
    ethan "¿Oye y porque no te vienes un rato y te lo muestro? podría darte una clase rápida, es muy bonito tocar instrumentos."
    scene madSofaRevistaMasagePies_32 with dissolve
    mad "Ohhh seguro que lo haría fatal y tengo cosas que hacer."
    scene madSofaRevistaMasagePies_33 with dissolve
    ethan "Vamos anímate no tienes nada que perder seguro que te diviertes."
    scene madSofaRevistaMasagePies_34 with dissolve
    mad "hahaha tú lo que quieres es reírte de lo mal que lo haría..."
    scene madSofaRevistaMasagePies_35 with dissolve
    ethan "Que no, venga anímate seguro que lo haces genial y lo pasaremos bien."
    scene madSofaRevistaMasagePies_36 with dissolve
    mad "Esta bien, pero solo un rato, deja que me cambie de ropa."
    scene madSofaRevistaMasagePies_37 with dissolve
    ethan "Claro, sin problemas."
    scene fondoNegro with dissolve
    "Unos minutos más tarde..."
    scene madSofaRevistaMasagePies_38 with dissolve
    mad "Ya estoy lista."
    scene madSofaRevistaMasagePies_39 with dissolve
    ethan "¡Perfecto!, tu primera."
    scene madSofaRevistaMasagePies_40 with dissolve
    pause
    scene madSofaRevistaMasagePies_41 with dissolve
    pause
    scene madSofaRevistaMasagePies_42 with dissolve
    pause
    scene madSofaRevistaMasagePies_43 with dissolve
    protaPensa "Hijo de puta..."
    scene madSofaRevistaMasagePies_44 with dissolve

    menu:
        "Seguirles":
            scene fondoNegro with dissolve
            "Unos minutos más tarde..."
            play music "audio/music/Marty_Gots_a_Plan.ogg" fadein 2.0

            scene madSofaRevistaMasagePies_45 with dissolve
            pause
            scene madSofaRevistaMasagePies_46 with dissolve
            protaPensa "Ya me colado en su jardín."
            scene madSofaRevistaMasagePies_47 with dissolve
            protaPensa "Tienen que estar por aquí..."
            scene madSofaRevistaMasagePies_48 with dissolve
            protaPensa "Ahí están..."
            scene madSofaRevistaMasagePies_49 with dissolve
            pause
            scene madSofaRevistaMasagePies_50 with dissolve
            stop music fadeout 2.0
            mad "¡Caramba! que habitación tan acondicionada. Tienes muchos instrumentos y cosas para hacer gimnasia."
            scene madSofaRevistaMasagePies_51 with dissolve
            mad "John nunca me dijo que tocabas instrumentos de música."
            scene madSofaRevistaMasagePies_52 with dissolve
            pause
            scene madSofaRevistaMasagePies_53 with dissolve
            pause
            scene madSofaRevistaMasagePies_54 with dissolve
            ethan "Si desde pequeño que me gusta la música, es algo que me hace desconectar y relajarme."
            scene madSofaRevistaMasagePies_55 with dissolve
            ethan "Seguramente John no te dijo nada porque debe estar muy ocupado con su trabajo..."
            scene madSofaRevistaMasagePies_56 with dissolve
            mad "..."
            scene madSofaRevistaMasagePies_57 with dissolve
            ethan "Oh disculpa por no ofrecerte nada de beber, ahora vengo y traigo algo..."
            scene madSofaRevistaMasagePies_58 with dissolve
            mad "Oh no te preocupes, no es necesario, no me quedare mucho rato..."
            scene madSofaRevistaMasagePies_57 with dissolve
            ethan "Por supuesto que sí, ahora vengo. Como si estuvieras en tu casa..."
            scene madSofaRevistaMasagePies_59 with dissolve
            mad "Eres muy amable."
            scene madSofaRevistaMasagePies_60 with dissolve
            pause
            scene madSofaRevistaMasagePies_61 with dissolve
            madPensa "Ahí está el violoncello..."
            scene madSofaRevistaMasagePies_62 with dissolve
            madPensa "Es una habitación muy agradable..."
            scene madSofaRevistaMasagePies_63 with dissolve
            pause
            scene madSofaRevistaMasagePies_64 with dissolve
            pause
            scene madSofaRevistaMasagePies_65 with dissolve
            pause
            play sound "audio/effects/golpeMesa2.ogg"
            scene madSofaRevistaMasagePies_66 with dissolve
            pause
            scene madSofaRevistaMasagePies_67 with dissolve
            pause
            play sound "audio/effects/golpeMesa2.ogg"
            scene madSofaRevistaMasagePies_68 with dissolve
            pause
            scene madSofaRevistaMasagePies_69 with dissolve
            pause
            scene madSofaRevistaMasagePies_70 with dissolve
            ethan "Lo haces bien."
            scene madSofaRevistaMasagePies_71 with dissolve
            pause
            scene madSofaRevistaMasagePies_72 with dissolve
            pause
            scene madSofaRevistaMasagePies_73 with dissolve
            mad "Uhh me has asustado hahaha."
            scene madSofaRevistaMasagePies_74 with dissolve
            ethan "Sofia, te traído un poco de vino, no tenía nada más fresco."
            scene madSofaRevistaMasagePies_75 with dissolve
            mad "Oh bueno, está bien. Con un simple vaso de agua era suficiente."
            scene madSofaRevistaMasagePies_76 with dissolve
            pause
            scene madSofaRevistaMasagePies_77 with dissolve
            pause
            scene madSofaRevistaMasagePies_75 with dissolve
            mad "Esta muy rico gracias."
            scene madSofaRevistaMasagePies_79 with dissolve
            ethan "Si, es un vino que me gusta mucho. Me alegro que a ti también te guste."
            scene madSofaRevistaMasagePies_80 with dissolve
            ethan "Parece que se te ha desabrochado un botón de la camisa."
            scene madSofaRevistaMasagePies_81 with dissolve
            pause
            scene madSofaRevistaMasagePies_82 with dissolve
            mad "Uh!"
            play sound "audio/effects/sorpresa3.ogg"
            scene madSofaRevistaMasagePies_83 with dissolve
            mad "Que situación más incomoda, lo siento mucho que vergüenza, gracias por avisarme."
            scene madSofaRevistaMasagePies_84 with dissolve
            ethan "Seguramente golpeaste muy fuerte el saco y se te desabrocho el botón de la camisa. ¿A caso quieres matar a alguien? hahaha."
            scene madSofaRevistaMasagePies_85 with dissolve
            mad "¡Oh dios! que vergüenza, gracias."
            scene madSofaRevistaMasagePies_86 with dissolve
            ethan "Ese saco es la mejor manera de desestresarse... Es la mejor terapia."
            scene madSofaRevistaMasagePies_87 with dissolve
            mad "Hahaha"
            scene madSofaRevistaMasagePies_88 with dissolve
            mad "Voy a probar con el violoncelo o se hará tarde."
            scene madSofaRevistaMasagePies_89 with dissolve
            ethan "Claro todo tuyo."
            play sound "audio/effects/camilla3.ogg"
            scene madSofaRevistaMasagePies_90 with dissolve
            mad "Creo que era así..."
            play sound "audio/effects/violoncel1.ogg"
            scene madSofaRevistaMasagePies_91 with dissolve
            mad "Ohh que horror..."
            scene madSofaRevistaMasagePies_92 with dissolve
            mad "Han pasado tantos años que ni me acuerdo."
            scene madSofaRevistaMasagePies_93 with dissolve
            ethan "Con la música no se puede correr, hay que sentirla."
            play sound "audio/effects/camilla3.ogg"
            scene madSofaRevistaMasagePies_94 with dissolve
            ethan "Te has de relajar Sofia y te saldrá mejor..."
            scene madSofaRevistaMasagePies_95 with dissolve
            pause
            scene madSofaRevistaMasagePies_96 with dissolve
            protaPensa "Como intente algo ese cabrón, le voy a parar los pies."
            scene madSofaRevistaMasagePies_97 with dissolve
            pause
            scene madSofaRevistaMasagePies_98 with dissolve
            ethan "Así... Prueba ahora."
            scene madSofaRevistaMasagePies_99 with dissolve
            pause
            play sound "audio/effects/violoncel2.ogg"
            scene madSofaRevistaMasagePies_100 with dissolve
            ethan "Es cosa de paciencia perseverancia y de estar en sintonía y relajada."
            scene madSofaRevistaMasagePies_101 with dissolve
            ethan "Así te saldrá mejor."
            scene madSofaRevistaMasagePies_102 with dissolve
            mad "Esta bien..."
            $ numTocaVioloncello = 0
            while numTocaVioloncello < 2:
                scene madSofaRevistaMasagePies_102 with dissolve
                pause 0.2
                play sound "audio/effects/violoncel3.ogg"
                scene madSofaRevistaMasagePies_103 with dissolve
                pause 0.2
                scene madSofaRevistaMasagePies_104 with dissolve
                pause 0.2
                $ numTocaVioloncello = numTocaVioloncello +1
            scene fondoNegro with dissolve
            "Despues de unos minutos practicando..."
            scene madSofaRevistaMasagePies_105 with dissolve
            ethan "Mucho mejor Sofia. Con unas pocas clases más seguro que te volverías una profesional."
            scene madSofaRevistaMasagePies_106 with dissolve
            mad "Hahaha no te rías de mí."
            scene madSofaRevistaMasagePies_107 with dissolve
            ethan "Podemos practicar con las notas principales."
            scene madSofaRevistaMasagePies_108 with dissolve
            mad "Oh creo que ya es hora de que me vaya, ha estado divertido."
            scene madSofaRevistaMasagePies_109 with dissolve
            ethan "¿Tan pronto?"
            scene madSofaRevistaMasagePies_110 with dissolve
            ethan "¿Está bien porque no te vienes otro día y seguimos?"
            scene madSofaRevistaMasagePies_111 with dissolve
            mad "Oh bueno ya veremos a ver si saco un poco de tiempo."
            scene madSofaRevistaMasagePies_112 with dissolve
            ethan "Por supuesto, cuídate mucho."
            scene madSofaRevistaMasagePies_113 with dissolve
            pause
            scene madSofaRevistaMasagePies_114 with dissolve
            protaPensa "¡Maldito bastardo! ya veo donde van sus intenciones..."
            scene madSofaRevistaMasagePies_115 with dissolve
            protaPensa "Sera mejor que vuelva a casa antes que [nombreMad3]."

    return
