label madSalonLeeRevista4taInteracMartes():
    call esconderHud() from _call_esconderHud_7
    hide screen salonTardeMartesMad
    scene madLeeRevistaSalon4taMartes_00
    call primeraVezMad() from _call_primeraVezMad_2
    pause
    scene madLeeRevistaSalon4taMartes_0 with dissolve
    pause
    scene madLeeRevistaSalon4taMartes_1 with dissolve
    play music "audio/music/Scheming_Weasel_faster.ogg" fadein 3.0
    prota "Hola [nombreMad2]"
    scene madLeeRevistaSalon4taMartes_2 with dissolve
    prota "Hola [nombreProta2]"
    menu:
        "Sentarse" if protaSentadoSofaConMad4taMartes == False:
            $ protaSentadoSofaConMad4taMartes = True
            scene madLeeRevistaSalon4taMartes_3 with dissolve
            pause(0.3)
            $ renpy.sound.play("audio/effects/sentarseSofa.ogg", loop = False)
            scene madLeeRevistaSalon4taMartes_4 with hpunch

            call madSalonSentarteConMad4taInteracMartes from _call_madSalonSentarteConMad4taInteracMartes
            #pause
            return
        "Irte":
            call mostrarHud() from _call_mostrarHud_5
            return





    #hide screen salonTardeMartesMad
    #show screen salonCoordenadas

    return
