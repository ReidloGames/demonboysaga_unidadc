label madMiraTv4taInteracDomingo():
    call esconderHud() from _call_esconderHud_19
    hide screen salonTardeDomingoMad
    hide screen tvLeon
    hide screen tvLeon1
    scene madMiraTvSalon4taDomingo_0

    call primeraVezMad() from _call_primeraVezMad_7

    if conversMadSalonMiraTv4taDomingo_Hablado == False:

        call madSalonMiraTv4taInteracDomingo_visit1() from _call_madSalonMiraTv4taInteracDomingo_visit1

    else:
        protaPensa "Ya he hablado con ella"
        scene salon

    call mostrarHud() from _call_mostrarHud_14
    #show screen salonCoordenadas

    return
