label madSalonMiraTv4taInteracDomingo_visit1():
    #$ madSalonMiraTv4taInteracDomingo_visit1 = True
    $ conversMadSalonMiraTv4taDomingo_Hablado = True
    pause
    scene madMiraTvSalon4taDomingo_1
    pause(0.8)
    scene madMiraTvSalon4taDomingo_2 with dissolve
    pause
    scene madMiraTvSalon4taDomingo_3 with dissolve
    play music "audio/music/Sneaky_Snitch.ogg" fadein 2.0
    pause(0.2)
    scene madMiraTvSalon4taDomingo_4 with dissolve
    prota "Hola [nombreMad3]"
    scene madMiraTvSalon4taDomingo_3 with dissolve
    pause(0.2)
    scene madMiraTvSalon4taDomingo_4 with dissolve
    prota "estas mirando la tele?"
    scene madMiraTvSalon4taDomingo_3 with dissolve
    pause
    scene madMiraTvSalon4taDomingo_8 with dissolve
    pause(0.2)
    scene madMiraTvSalon4taDomingo_9 with dissolve
    pause(0.2)
    scene madMiraTvSalon4taDomingo_10 with dissolve
    mad "Hola [nombreProta2]"
    scene madMiraTvSalon4taDomingo_11 with dissolve
    pause(0.2)

    if eventComunicadoPadCuentaProblema == False:
        scene madMiraTvSalon4taDomingo_12 with dissolve
        mad "Iba a ver la tele con tu [situPad]"
        scene madMiraTvSalon4taDomingo_13 with dissolve
        scene madMiraTvSalon4taDomingo_11 with dissolve
        mad "Pero se ha tenido que quedar trabajando"
        scene madMiraTvSalon4taDomingo_13 with dissolve
        pause(0.1)

        scene madMiraTvSalon4taDomingo_12 with dissolve
        mad "Quieres ver la tele conmigo?"
        scene madMiraTvSalon4taDomingo_14 with dissolve
        pause
        scene madMiraTvSalon4taDomingo_19 with dissolve
        pause(0.4)
        scene madMiraTvSalon4taDomingo_16 with dissolve
        pause(0.5)
        scene madMiraTvSalon4taDomingo_17 with dissolve
        pause(0.4)
        scene madMiraTvSalon4taDomingo_18 with dissolve
        pause(0.4)
        scene madMiraTvSalon4taDomingo_19 with dissolve
        pause(0.4)
        scene madMiraTvSalon4taDomingo_20 with dissolve
        pause
    else:
        scene madMiraTvSalon4taDomingo_6 with dissolve
        prota "Puedo unirme y miramos una peli?"
        scene madMiraTvSalon4taDomingo_7 with dissolve
        pause
        scene madMiraTvSalon4taDomingo_12 with dissolve
        mad "Si, claro"
        scene madMiraTvSalon4taDomingo_13 with dissolve
        pause
    scene madMiraTvSalon4taDomingo_21 with dissolve
    pause

    scene madMiraTvSalon4taDomingo_22 with dissolve
    pause
    scene madMiraTvSalon4taDomingo_23 with dissolve
    pause(0.7)
    scene madMiraTvSalon4taDomingo_24 with dissolve

    pause(0.7)
    play sound "audio/effects/sientaSofa.ogg"
    scene madMiraTvSalon4taDomingo_25 with dissolve
    pause(0.7)
    scene madMiraTvSalon4taDomingo_26 with dissolve

    pause
    scene madMiraTvSalon4taDomingo_27 with dissolve
    pause(0.8)
    scene madMiraTvSalon4taDomingo_28 with dissolve
    pause
    scene madMiraTvSalon4taDomingo_29 with dissolve
    pause(0.4)
    scene madMiraTvSalon4taDomingo_30 with dissolve
    prota "En el canal 2 empieza una peli. ¿Quieres que la veamos?"
    scene madMiraTvSalon4taDomingo_29 with dissolve
    pause
    scene madMiraTvSalon4taDomingo_31 with dissolve
    pause
    scene madMiraTvSalon4taDomingo_32 with dissolve
    pause(0.4)
    scene madMiraTvSalon4taDomingo_33 with dissolve
    mad "Seguro que es mas interesante que lo que estoy viendo ahora"
    scene madMiraTvSalon4taDomingo_32 with dissolve
    pause(0.4)
    scene madMiraTvSalon4taDomingo_31 with dissolve
    pause
    scene madMiraTvSalon4taDomingo_29 with dissolve
    pause(0.4)
    scene madMiraTvSalon4taDomingo_30 with dissolve
    prota "Ahora te la pongo"
    scene madMiraTvSalon4taDomingo_29 with dissolve
    pause
    scene madMiraTvSalon4taDomingo_34 with dissolve
    pause
    scene madMiraTvSalon4taDomingo_35 with dissolve
    pause
    scene madMiraTvSalon4taDomingo_34 with dissolve
    pause(0.6)
    scene madMiraTvSalon4taDomingo_36 with dissolve
    pause
    scene madMiraTvSalon4taDomingo_37 with dissolve
    pause
    scene madMiraTvSalon4taDomingo_38 with dissolve
    pause
    scene madMiraTvSalon4taDomingo_39 with dissolve
    pause
    scene madMiraTvSalon4taDomingo_38 with dissolve
    pause
    call nextTiempoDay(0) from _call_nextTiempoDay_4
