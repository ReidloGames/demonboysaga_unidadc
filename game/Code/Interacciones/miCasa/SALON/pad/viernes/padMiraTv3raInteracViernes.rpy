label padMiraTv3raInteracViernes():
    call esconderHud() from _call_esconderHud_17
    hide screen salonMediodiaViernesPad
    scene padMiraTvSofaSalon3raViernes_0
    call primeraVezPad() from _call_primeraVezPad_2
    hide screen tvDeporte0
    hide screen tvDeporte1
    protaPensa "Esta mirando el fútbol. Mejor no molestarle."
    hide screen tvDeporte0
    hide screen tvDeporte1
    scene salon

    call mostrarHud() from _call_mostrarHud_11
    #show screen salonCoordenadas
