label mayPintaUnyas4taInteracMiercoles():
    call esconderHud() from _call_esconderHud_43
    hide screen salonPintaUnyasTardeMiercolesMay
    scene mayPintaUnyasSalon4taMiercoles_0
    call primeraVezMay() from _call_primeraVezMay_5

    if conversMayPintaUnyasSalon4taMiercoles_Hablado == False:

        $ conversMayPintaUnyasSalon4taMiercoles_Hablado = True
        call mayPintaUnyasSalon4taInteracMiercoles_visit1() from _call_mayPintaUnyasSalon4taInteracMiercoles_visit1

    else:
        protaPensa "Ya he estado aquí"
        scene salon

    call mostrarHud() from _call_mostrarHud_38

    #show screen salonCoordenadas

    return
