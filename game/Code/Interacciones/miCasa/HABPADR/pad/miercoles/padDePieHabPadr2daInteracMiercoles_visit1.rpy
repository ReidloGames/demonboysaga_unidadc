label padDePieHabPadr2daInteracMiercoles_visit1():
    $ padDePieHabPadr2daInteracMiercoles_visit1 = True
    scene padDePieHabPadr2daMiercoles_1
    pause
    scene padDePieHabPadr2daMiercoles_2 with dissolve
    pause(0.2)
    scene padDePieHabPadr2daMiercoles_3 with dissolve
    stop music fadeout 4.0
    prota "Hola [nombrePad3], va todo bien?"
    scene padDePieHabPadr2daMiercoles_2 with dissolve
    pause(0.2)
    scene padDePieHabPadr2daMiercoles_7 with dissolve
    pause(0.3)
    scene padDePieHabPadr2daMiercoles_8 with dissolve
    pad "si, todo va perfecto."
    scene padDePieHabPadr2daMiercoles_10 with dissolve
    pause(0.2)
    scene padDePieHabPadr2daMiercoles_3 with dissolve
    pause(0.3)
    scene padDePieHabPadr2daMiercoles_4 with dissolve
    pause(0.3)
    scene padDePieHabPadr2daMiercoles_5 with dissolve
    prota "Podrías darme algo de dinero?"
    scene padDePieHabPadr2daMiercoles_6 with dissolve
    pause(0.3)
    scene padDePieHabPadr2daMiercoles_8 with dissolve
    scene padDePieHabPadr2daMiercoles_9 with dissolve
    pad "Necesitas empezar a responsabilizarte y dejar de depender tanto de mi"
    scene padDePieHabPadr2daMiercoles_10 with dissolve
    pause(0.4)
    scene padDePieHabPadr2daMiercoles_8 with dissolve
    pad "Te doy 1$"
    $ player_gold += 1
    play sound "audio/effects/money.ogg"
    scene padDePieHabPadr2daMiercoles_11 with dissolve
    pause(0.2)
    play sound "audio/effects/sorpres.ogg"
    scene padDePieHabPadr2daMiercoles_12 with dissolve
    prota "Que?? pero si con eso no tengo ni para cacahuetes!!"
    scene padDePieHabPadr2daMiercoles_16 with dissolve
    pause(0.3)
    scene padDePieHabPadr2daMiercoles_17 with dissolve
    pad "Seguro que te apañas,tengo cosas que hacer."
    scene padDePieHabPadr2daMiercoles_14 with dissolve
    pause(0.3)
    scene padDePieHabPadr2daMiercoles_15 with dissolve
    protaPensa "{size=15}{i}Si fuera para Dana, Erika o Kara seguro que la cantidad tendría tres ceros detras.{/size}{/i}"
    scene padDePieHabPadr2daMiercoles_19 with dissolve
    pause
    protaPensa "{size=15}{i}Conmigo es un tacaño. Debería beber un poco de leche antes de irme a la Uni.{/size}{/i}"#What a cheapskate. I should drink some milk before I leave for Uni.
    scene padDePieHabPadr2daMiercoles_20 with dissolve
    pause(0.7)
    scene padDePieHabPadr2daMiercoles_21 with dissolve
    pause(0.9)
    scene padDePieHabPadr2daMiercoles_22 with dissolve
    pause(0.4)


    return
