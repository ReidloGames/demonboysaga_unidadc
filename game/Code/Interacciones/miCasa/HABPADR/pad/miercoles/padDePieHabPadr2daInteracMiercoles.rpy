label padDePieHabPadr2daInteracMiercoles():
    call esconderHud() from _call_esconderHud_5
    hide screen habPadrManyanaMiercolesPad
    scene padDePieHabPadr2daMiercoles_0
    call primeraVezPad() from _call_primeraVezPad_1

    if conversPadDePieHabPadr2daMiercoles_Hablado == False:

        if padDePieHabPadr2daInteracMiercoles_visit1 == False:
            $ conversPadDePieHabPadr2daMiercoles_Hablado = True
            call padDePieHabPadr2daInteracMiercoles_visit1() from _call_padDePieHabPadr2daInteracMiercoles_visit1

        else:
            protaPensa "No tengo nada mas que decirle"
            if tiempoDia != 4:
                scene habPadrSgD
            else:
                scene habPadrSgN
    else:
        protaPensa "Ya he hablado con el"
        if tiempoDia != 4:
            scene habPadrSgD
        else:
            scene habPadrSgN

    call mostrarHud() from _call_mostrarHud_3
    #show screen habPadresCoordenadas

    return
