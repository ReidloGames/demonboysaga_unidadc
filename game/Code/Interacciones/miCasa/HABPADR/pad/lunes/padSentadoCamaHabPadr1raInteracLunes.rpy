label padSentadoCamaHabPadr1raInteracLunes():
    call esconderHud() from _call_esconderHud_3
    hide screen habPadrAmanecerLunesPad
    scene padSentadoCamaHabPadr1raLunes_0
    call primeraVezPad() from _call_primeraVezPad

    if conversPadSentadoCamaHabPadr1raLunes_Hablado == False:

        if padSentadoCamaHabPadr1raInteracLunes_visit1 == False:
            $ conversPadSentadoCamaHabPadr1raLunes_Hablado = True
            call padSentadoCamaHabPadr1raInteracLunes_visit1() from _call_padSentadoCamaHabPadr1raInteracLunes_visit1

        else:
            protaPensa "No tengo nada mas que decirle"
            if tiempoDia != 4:
                scene habPadrSgD
            else:
                scene habPadrSgN
    else:
        protaPensa "Ya he hablado con el"
        if tiempoDia != 4:
            scene habPadrSgD
        else:
            scene habPadrSgN

    #call mostrarHud() from _call_mostrarHud_1
    #show screen habPadresCoordenadas

    return
