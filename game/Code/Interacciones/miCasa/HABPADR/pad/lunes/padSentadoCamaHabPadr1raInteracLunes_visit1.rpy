label padSentadoCamaHabPadr1raInteracLunes_visit1():
    $ padSentadoCamaHabPadr1raInteracLunes_visit1 = True
    $ escondeFrameAyudasPad = True
    pause
    scene padSentadoCamaHabPadr1raLunes_0
    stop music fadeout 4.0
    prota "Hola [situPad2]"
    scene padSentadoCamaHabPadr1raLunes_1 with dissolve
    pause(0.2)
    scene padSentadoCamaHabPadr1raLunes_2 with dissolve
    pause(0.2)
    scene padSentadoCamaHabPadr1raLunes_3 with dissolve
    pad "Hola [nombreProta2]"
    scene padSentadoCamaHabPadr1raLunes_4 with dissolve
    prota "Que haces aquí tan solo? "
    protaPensa "{size=15}{i}Esta pensativo...{/size}{/i}"
    scene padSentadoCamaHabPadr1raLunes_5 with dissolve
    pad "Oh, nada. Solo necesitaba desconectar un poco la mente."
    scene padSentadoCamaHabPadr1raLunes_6 with dissolve
    pause(0.2)
    scene padSentadoCamaHabPadr1raLunes_7 with dissolve
    pause
    scene padSentadoCamaHabPadr1raLunes_8 with dissolve
    $ renpy.sound.play("audio/effects/tonoMovil.ogg", loop = True)
    "!RIIING!"
    pause(0.3)
    scene padSentadoCamaHabPadr1raLunes_9 with dissolve
    pause
    scene padSentadoCamaHabPadr1raLunes_10 with dissolve
    pause
    scene padSentadoCamaHabPadr1raLunes_11 with dissolve
    pause
    scene padSentadoCamaHabPadr1raLunes_12 with dissolve
    pause(0.2)

    scene padSentadoCamaHabPadr1raLunes_13 with dissolve
    pad "Si no te importa hablamos mas tarde"
    scene padSentadoCamaHabPadr1raLunes_14 with dissolve
    pause
    scene padSentadoCamaHabPadr1raLunes_17 with dissolve
    pause(0.2)
    scene padSentadoCamaHabPadr1raLunes_18 with dissolve
    prota "Si, ningún problema"
    scene padSentadoCamaHabPadr1raLunes_19 with dissolve
    pause
    scene padSentadoCamaHabPadr1raLunes_20 with dissolve
    pause(0.2)
    scene padSentadoCamaHabPadr1raLunes_21 with dissolve
    pause(0.2)
    scene padSentadoCamaHabPadr1raLunes_22 with dissolve
    pause
    scene padSentadoCamaHabPadr1raLunes_23 with dissolve
    protaPensa "{size=15}{i}Tiene cara de preocupación. No me da buena espina.{/size}{/i}"
    pause
    $ puedesDesplazarteLibreCasa = True
    call espiarPadLlamadaTelefono from _call_espiarPadLlamadaTelefono
    hide screen habPadresCoordenadas
    #call nextZona("comedor")
    return


label espiarPadLlamadaTelefono():
    $ puedoVolverMiHabitacion = True
    $ puedesDesplazarteLibreCasa = True
    $ adelantarHora = True
    $ puedoVerTv = False
    menu:
        "Espiar":
            scene padSentadoCamaHabPadr1raLunes_25 with dissolve
            pause(0.4)
            stop sound
            scene padSentadoCamaHabPadr1raLunes_26 with dissolve
            pause(0.2)
            scene padSentadoCamaHabPadr1raLunes_27 with dissolve
            pad "Si, te pagare la proxima semana."
            scene padSentadoCamaHabPadr1raLunes_28 with dissolve
            pause(0.2)
            scene padSentadoCamaHabPadr1raLunes_29 with dissolve
            pad "Así sera..."
            scene padSentadoCamaHabPadr1raLunes_30 with dissolve
            protaPensa "{size=15}{i}Me voy antes de que me vea.{/size}{/i}"
            scene padSentadoCamaHabPadr1raLunes_31 with dissolve
            call nextTiempoDay(0) from _call_nextTiempoDay_7
            pause
            #hide screen habPadresCoordenadas

            call nextZona("comedor") from _call_nextZona
            return
        "Irte":
            scene padSentadoCamaHabPadr1raLunes_24 with dissolve
            stop sound
            call nextTiempoDay(0) from _call_nextTiempoDay_8
            pause
            #hide screen habPadresCoordenadas

            call nextZona("comedor") from _call_nextZona_1
            return
