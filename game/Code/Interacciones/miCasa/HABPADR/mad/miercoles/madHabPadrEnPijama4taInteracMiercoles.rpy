label madHabPadrEnPijama4taInteracMiercoles():
    call esconderHud() from _call_esconderHud_14
    hide screen habPadrPijamTardeMiercolesMad
    scene madHabPadrEnPijama4taMiercoles_0
    call primeraVezMad() from _call_primeraVezMad_5

    if conversMadHabPadrEnPijama4taMiercoles_Hablado == False:

        if madHabPadrEnPijama4taInteracMiercoles_visit1 == False:
            $ conversMadHabPadrEnPijama4taMiercoles_Hablado = True
            call madHabPadrEnPijama4taInteracMiercoles_visit1() from _call_madHabPadrEnPijama4taInteracMiercoles_visit1

        elif demonBoy == True:
            if madHabPadrEnPijama4taInteracMiercoles_visit1 == True and madHabPadrEnPijama4taInteracMiercoles_visit2 == False:
                $ madHabPadrEnPijama4taInteracMiercoles_visit2 = True
                call madHabPadrEnPijama4taInteracMiercoles_visit2 from _call_madHabPadrEnPijama4taInteracMiercoles_visit2
            else:
                protaPensa "Ya he hablado con ella"

        else:
            protaPensa "No tengo nada mas que decirle"
            if tiempoDia != 4:
                scene habPadrSgD
            else:
                scene habPadrSgN

    else:
        protaPensa "Ya he hablado con ella"
        if tiempoDia != 4:
            scene habPadrSgD
        else:
            scene habPadrSgN

    call mostrarHud() from _call_mostrarHud_8
    #show screen habPadresCoordenadas

    return
