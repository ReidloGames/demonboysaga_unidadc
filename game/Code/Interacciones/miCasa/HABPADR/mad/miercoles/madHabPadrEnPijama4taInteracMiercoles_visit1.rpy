label madHabPadrEnPijama4taInteracMiercoles_visit1():
    $ madHabPadrEnPijama4taInteracMiercoles_visit1 = True
    pause
    scene madHabPadrEnPijama4taMiercoles_1
    stop music fadeout 4.0
    prota "Hola"
    scene madHabPadrEnPijama4taMiercoles_2 with dissolve
    pause (0.5)
    play music "audio/music/OctoBlues.ogg"
    scene madHabPadrEnPijama4taMiercoles_2_Scroll at pandown2
    pause
    scene madHabPadrEnPijama4taMiercoles_3 with dissolve
    protaPensa "{size=15}{i}Dios mio!! Se estaba poniendo el pijama. {/size}{/i}"
    scene madHabPadrEnPijama4taMiercoles_4 with dissolve
    mad "Deberías llamar a la puerta."
    scene madHabPadrEnPijama4taMiercoles_5 with dissolve
    mad "Me estaba cambiando de ropa..."
    scene madHabPadrEnPijama4taMiercoles_6 with dissolve
    prota "Pensé que estaba abierta, no me di cuenta."
    stop music fadeout 4.0
    scene madHabPadrEnPijama4taMiercoles_7 with dissolve
    mad "Esta bien, no pasa nada. Pero fijate para la proxima vez."
    scene madHabPadrEnPijama4taMiercoles_9 with dissolve

    pause(0.4)
    play music "audio/music/Scheming_Weasel_faster.ogg" fadein 5.0

    scene madHabPadrEnPijama4taMiercoles_10 with dissolve
    prota "Te queda muy bien este conjunto de noche"
    scene madHabPadrEnPijama4taMiercoles_9 with dissolve
    pause
    scene madHabPadrEnPijama4taMiercoles_7 with dissolve
    pause(0.4)
    $ renpy.sound.play("audio/effects/aix.ogg", loop = False)
    scene madHabPadrEnPijama4taMiercoles_8 with dissolve
    pause
    scene madHabPadrEnPijama4taMiercoles_11 with dissolve
    prota "Adios [nombreMad2]"
    scene madHabPadrEnPijama4taMiercoles_12 with dissolve
    pause(0.4)
    scene madHabPadrEnPijama4taMiercoles_11 with dissolve
    pause
    call nextTiempoDay(0) from _call_nextTiempoDay_13
    call nextZona("comedor") from _call_nextZona_13

return
