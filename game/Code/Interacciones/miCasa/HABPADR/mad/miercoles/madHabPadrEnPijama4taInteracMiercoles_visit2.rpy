label madHabPadrEnPijama4taInteracMiercoles_visit2():
    stop music fadeout 2.0
    play sound "audio/effects/susto.ogg"
    #scene madHabPadrEnPijama4taMiercolesPart2_0
    scene madHabPadrEnPijama4taMiercolesPart2_1 with dissolve
    mad "Me has asustado"
    mad "Tienes que llamar a la puerta"
    scene madHabPadrEnPijama4taMiercolesPart2_2 with dissolve
    prota "Perdón, pero necesitaba hablar contigo y con las prisas..."
    scene madHabPadrEnPijama4taMiercolesPart2_3 with dissolve
    mad "Dime..."
    play music "audio/music/Marty_Gots_a_Plan.ogg" fadein 2.0
    scene madHabPadrEnPijama4taMiercolesPart2_4 with dissolve

    menu:
        "Dar las buenas noches":
            scene madHabPadrEnPijama4taMiercolesPart2_12 with dissolve
            prota "Solo quería darte las buenas noches."
            scene madHabPadrEnPijama4taMiercolesPart2_13 with dissolve
            mad "Ohh, gracias igualmente."
            return
        "Preguntar como esta" if preguntarComoEstaMad == False:
            call preguntarComoEstaMad from _call_preguntarComoEstaMad
            return
        "Informar de tu nuevo trabajo" if tienesTrabajoAmarzong == True and leccion1Massage == True and informarNuevoTrabajoMad == False and preguntarComoEstaMad == True:
            if player_gold >= 10:
                call informarDeTuNuevoTrabajo from _call_informarDeTuNuevoTrabajo

            else:
                protaPensa "Necesito 10€ para poderle dar a [nombreMad3] y que confie en mi."
                scene madHabPadrEnPijama4taMiercolesPart2_12 with dissolve
                prota "Buenas noches."
                scene madHabPadrEnPijama4taMiercolesPart2_13 with dissolve
                mad "Gracias, buenas noches."
            return

        "Decirle que no te gusta Ethan" if haVenidoEthan1 == True:
            call decirAMadNoTeGustaEthan from _call_decirAMadNoTeGustaEthan
        "Hablar del libro de yoga" if preguntaLibroYogaBiblioteca == True and hablarLibroYogaSofia == False:
            call hablaLibroYogaSofia

    return





label preguntarComoEstaMad():
    $ preguntarComoEstaMad = True
    scene madHabPadrEnPijama4taMiercolesPart2_5 with dissolve
    mad "Todo esto ha sido muy duro, de la noche a la mañana todo ha cambiado."
    scene madHabPadrEnPijama4taMiercolesPart2_6 with dissolve
    mad "[situPad2] no esta y ahora tengo toda la responsabilidad de la casa."
    scene madHabPadrEnPijama4taMiercolesPart2_7 with dissolve
    mad "Estoy mirando empleos."
    scene madHabPadrEnPijama4taMiercolesPart2_8 with dissolve
    mad "Creo que he podido encontrar uno. Aún no estoy del todo segura."
    scene madHabPadrEnPijama4taMiercolesPart2_9 with dissolve
    mad "Eso es fantástico."
    scene madHabPadrEnPijama4taMiercolesPart2_10 with dissolve
    prota "Yo me esforzare al máximo."
    scene madHabPadrEnPijama4taMiercolesPart2_11 with dissolve
    prota "No te preocupes."
    scene madHabPadrEnPijama4taMiercolesPart2_12 with dissolve
    prota "Buenas noches."
    scene madHabPadrEnPijama4taMiercolesPart2_13 with dissolve
    mad "Gracias, buenas noches."

    return

label informarDeTuNuevoTrabajo():
    $ informarNuevoTrabajoMad = True
    scene madHabPadrEnPijama4taMiercolesPart2_noticia0 with dissolve
    prota "Tengo una noticia muy buena que contarte."
    scene madHabPadrEnPijama4taMiercolesPart2_noticia1 with dissolve
    mad "¿Que ha pasado?"
    scene madHabPadrEnPijama4taMiercolesPart2_noticia2 with dissolve
    prota "Ya encontré trabajo."
    play sound "audio/effects/sorpresa.ogg"
    scene madHabPadrEnPijama4taMiercolesPart2_noticia3 with dissolve
    pause
    scene madHabPadrEnPijama4taMiercolesPart2_noticia4 with dissolve
    pause
    scene madHabPadrEnPijama4taMiercolesPart2_noticia5 with dissolve
    prota "Estoy haciendo un curso para ser masajista. Me lo está enseñando un profesional en la materia."
    scene madHabPadrEnPijama4taMiercolesPart2_noticia6 with dissolve
    prota "Ellos me enseñan la teoría y a cambio algunos días voy para ayudarles."
    scene madHabPadrEnPijama4taMiercolesPart2_noticia7 with dissolve
    prota "Hay gente deportista y gente mayor que necesitan cuidados y ellos me pagan."
    scene madHabPadrEnPijama4taMiercolesPart2_noticia8 with dissolve
    prota "Los masajes les ayudan mucho. También va gente que necesita un reconfortante masaje."
    scene madHabPadrEnPijama4taMiercolesPart2_noticia9 with dissolve
    prota "Son gente muy profesional."
    scene madHabPadrEnPijama4taMiercolesPart2_noticia10 with dissolve
    protaPensa "Menuda mentira, lo primero que se me ocurrió..."
    scene madHabPadrEnPijama4taMiercolesPart2_noticia11 with dissolve
    mad "¡¡Oh!! eso está muy bien. Que gran sorpresa."
    scene madHabPadrEnPijama4taMiercolesPart2_noticia12 with dissolve
    prota "Si, lo único que me han dicho que como están saturados de gente no pueden dedicarme el tiempo necesario para poner en práctica las teorías."
    scene madHabPadrEnPijama4taMiercolesPart2_noticia13 with dissolve
    mad "¿Como? ¿Y cómo aprendes entonces?"
    scene madHabPadrEnPijama4taMiercolesPart2_noticia14 with dissolve
    prota "Claro, ahí está el problema, me dijeron que practicara con algún conocido y como me estan pagando creo que no hay nada que perder. A parte me está gustando mucho, ya que puedo ayudar a los demás."
    scene madHabPadrEnPijama4taMiercolesPart2_noticia15 with dissolve
    prota "He pensado que podría practicar contigo."
    play sound "audio/effects/sorpresa1.ogg"
    scene madHabPadrEnPijama4taMiercolesPart2_noticia16 with dissolve
    mad "¿Conmigo? Pero [nombreProta2], estoy muy liada ahora mismo. Tengo muchas cosas en la cabeza."
    scene madHabPadrEnPijama4taMiercolesPart2_noticia17 with dissolve
    prota "Pero el otro día dijiste que juntos saldríamos de este problema. Estoy ganando dinero, pensé que me ayudarías..."
    scene madHabPadrEnPijama4taMiercolesPart2_noticia18 with dissolve
    play sound "audio/effects/money.ogg"
    prota "Mira aquí está el primer pago."
    $ player_gold = player_gold -10
    scene madHabPadrEnPijama4taMiercolesPart2_noticia19 with dissolve
    mad "Ehh...Es muy amable de tu parte."
    scene madHabPadrEnPijama4taMiercolesPart2_noticia20 with dissolve
    mad "¿Pero en que consiste esto?"
    scene madHabPadrEnPijama4taMiercolesPart2_noticia21 with dissolve
    prota "Nada, solo son masajes relajantes. A parte creo que te podrían ir bien."
    scene madHabPadrEnPijama4taMiercolesPart2_noticia22 with dissolve
    mad "¿Y no tienes a nadie más?"
    scene madHabPadrEnPijama4taMiercolesPart2_noticia23 with dissolve
    prota "Necesito alguien de confianza, pensaba que tú me ayudarías..."
    scene madHabPadrEnPijama4taMiercolesPart2_noticia24 with dissolve
    mad "Esta bien..."
    scene madHabPadrEnPijama4taMiercolesPart2_noticia25 with dissolve
    prota "Genial, ya verás que todo ira mejorando."
    scene madHabPadrEnPijama4taMiercolesPart2_noticia26 with dissolve
    mad "Es bueno ver cómo te esfuerzas."
    scene madHabPadrEnPijama4taMiercolesPart2_noticia27 with dissolve
    prota "Por ti lo que sea [nombreMad3]."
    scene madHabPadrEnPijama4taMiercolesPart2_noticia28 with dissolve
    protaPensa "No me lo puedo creer, se ha tragado la mentira."
    scene madHabPadrEnPijama4taMiercolesPart2_noticia29 with dissolve
    prota "Buenas noches."

    return


label decirAMadNoTeGustaEthan():
    scene madEthanMalasIntenciones0 with dissolve
    prota "[nombreMad3], tengo que decirte que no me gusta Ethan."
    scene madEthanMalasIntenciones1 with dissolve
    prota "No creo que sea muy adecuado que vayas a su casa."
    scene madEthanMalasIntenciones2 with dissolve
    mad "¿Porque dices eso? Ethan es amigo de [nombrePad3] desde hace mucho tiempo. Me esta ayudando a tocar el violoncello y me lo paso bien."
    scene madEthanMalasIntenciones3 with dissolve
    mad "Ha venido varias veces a casa. ¿A qué viene esto?"
    scene madEthanMalasIntenciones4 with dissolve
    prota "..."
    scene madEthanMalasIntenciones5 with dissolve
    prota "Nada, dejalo..."
    scene madEthanMalasIntenciones4 with dissolve
    prota "Buenas noches."


    return

label hablaLibroYogaSofia():
    if hablarLibroYogaSofia == False:
        $ hablarLibroYogaSofia = True
        scene madHabHablaLibroYoga_0 with dissolve
        pause
        scene madHabHablaLibroYoga_1 with dissolve
        prota "El otro día mientras estaba en la biblioteca me encontré de casualidad con este libro."
        scene madHabHablaLibroYoga_2 with dissolve
        prota "Es un libro para aprender hacer yoga con ejercicios para dos."
        scene madHabHablaLibroYoga_3 with dissolve
        prota "Ahora si que podemos hacer ejercicio juntos."
        scene madHabHablaLibroYoga_4 with dissolve
        mad "Oh... A ver."
        scene madHabHablaLibroYoga_5 with dissolve
        pause
        scene madHabHablaLibroYoga_6 with dissolve
        play sound "audio/effects/sorpresa2.ogg"
        mad "Pero estos ejercicios son un poco..."
        scene madHabHablaLibroYoga_7 with dissolve
        prota "Oh, no te preocupes por la dificultad es como todo. Al principio será un poco difícil y luego iremos mejorando."
        scene madHabHablaLibroYoga_8 with dissolve
        mad "{size=14} Pero...{/size}"
        scene madHabHablaLibroYoga_9 with dissolve
        prota "Estoy muy contento de que podamos hacer ejercicio juntos."
        scene madHabHablaLibroYoga_10 with dissolve
        prota "Gracias [nombreMad3]. Me voy a trabajar."
        scene madHabHablaLibroYoga_11 with dissolve
        pause

    return
