label peqSentadaCama1raInteracDomingo():
    call esconderHud() from _call_esconderHud_29
    hide screen habHermanPeqEncimaSuCamaAmanecerDomingoPeq

    scene peqEnSuHabitacionSentadaCama_0
    call primeraVezPeq() from _call_primeraVezPeq_1

    if conversPeqEnSuHabitacionSentadaCama1raDomingo_Hablado == False:

        $ conversPeqEnSuHabitacionSentadaCama1raDomingo_Hablado = True
        call peqSuHabitacionSentadaCama1raInteracDomingo_visit1() from _call_peqSuHabitacionSentadaCama1raInteracDomingo_visit1

    else:
        protaPensa "Ya he hablado con ella"
        if tiempoDia != 4:
            scene habHijPeqSgD
        else:
            scene habHijPeqSgN

    call mostrarHud() from _call_mostrarHud_27

    #show screen habHermanPeqCoordenadas

    return
