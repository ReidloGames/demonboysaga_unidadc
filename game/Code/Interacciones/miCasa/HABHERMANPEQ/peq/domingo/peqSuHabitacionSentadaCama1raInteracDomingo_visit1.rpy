label peqSuHabitacionSentadaCama1raInteracDomingo_visit1():
    $ peqSuHabitacionSentadaCama1raInteracDomingo_visit1__ = True
    $ conversPeqEnSuHabitacionSentadaCama1raDomingo_Hablado = True
    pause
    #scene peqEnSuHabitacionSentadaCama_1 with dissolve
    #pause
    stop music fadeout 2.0
    scene peqEnSuHabitacionSentadaCama_2 with dissolve
    pause
    scene peqEnSuHabitacionSentadaCama_3 with dissolve
    prota "¡¡Buenos días!!"
    scene peqEnSuHabitacionSentadaCama_4 with dissolve
    pause
    if afectoPeq > 0 and preguntarCocinaComoEstaTemaPad == True and animarHermanPeqSuHabitacion == False:
        menu:
            "Decirle que se anime":
                if tienesChocolateDana == True:
                    $ animarHermanPeqSuHabitacion = True
                    scene peqAnimarSuHabitacion_0 with dissolve
                    prota "Tengo un regalo para ti"
                    scene peqAnimarSuHabitacion_1 with dissolve
                    hermanPeq "¿De verdad?"
                    scene peqAnimarSuHabitacion_2 with dissolve
                    hermanPeq "Ohhh el chocolate que más me gusta. ¡Muchas gracias!"
                    scene peqAnimarSuHabitacion_0 with dissolve
                    prota "Para que te animes un poco."
                    scene peqAnimarSuHabitacion_3 with dissolve
                    if afectoPeq == 1:
                        call avisosStatsSms("afecto") from _call_avisosStatsSms_16
                        $ afectoPeq = afectoPeq +1
                        python:
                            for item in player_inventory.itemlist:
                                if item.name == "chocolate":
                                    player_inventory.sub(item)

                else:
                    call peqNoSeAnimaEnCama from _call_peqNoSeAnimaEnCama
                    "¿Como podrías animarla? tal vez con un regalo..."

            ##

    else:
        call peqNoSeAnimaEnCama from _call_peqNoSeAnimaEnCama_1


    return



label peqNoSeAnimaEnCama():
    scene peqEnSuHabitacionSentadaCama_5 with dissolve
    hermanPeq "¡¡Tengo sueño!!"
    scene peqEnSuHabitacionSentadaCama_6 with dissolve
    hermanPeq "Tengo muchas tareas que hacer para la Universidad."
    scene peqEnSuHabitacionSentadaCama_7 with dissolve
    prota "Está bien, entonces no te duermas."
    scene peqEnSuHabitacionSentadaCama_3 with dissolve
    prota "No quiero que [nombreMad3] me regañe por entretenerte."
    scene peqEnSuHabitacionSentadaCama_8 with dissolve
    hermanPeqPensa "Que pereza..."


    return
