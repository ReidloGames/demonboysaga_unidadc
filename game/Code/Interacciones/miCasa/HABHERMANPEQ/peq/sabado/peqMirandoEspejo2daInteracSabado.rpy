label peqMirandoEspejo2daInteracSabado():
    call esconderHud() from _call_esconderHud_30
    hide screen habPeqMiraEspejoManyanaSabadoPeq

    scene peqSuHabitacionMirandoEspejo_1
    call primeraVezPeq() from _call_primeraVezPeq_2

    if conversPeqSuHabitacionMirandoEspejo2daSabado_Hablado == False:

        $ conversPeqSuHabitacionMirandoEspejo2daSabado_Hablado = True
        call peqSuHabitacionMirandoEspejo2daInteracSabados_visit1() from _call_peqSuHabitacionMirandoEspejo2daInteracSabados_visit1

    else:
        protaPensa "Ya he estado aquí"
        if tiempoDia != 4:
            scene habHijPeqSgD
        else:
            scene habHijPeqSgN

    call mostrarHud() from _call_mostrarHud_28

    #show screen habHermanPeqCoordenadas

    return
