label peqMiraPc1raInteracMartes():
    call esconderHud() from _call_esconderHud_35
    hide screen habHermanPeqMiraPcAmanecerMartesPeq
    hide screen reflejoEspejo
    scene peqSuHabitacionMiraPcMartes_0
    call primeraVezPeq() from _call_primeraVezPeq_3

    if conversPeqSuHabitacionMiraPc1raMartes_Hablado == False:

        $ conversPeqSuHabitacionMiraPc1raMartes_Hablado = True
        call peqSuHabitacionMiraPc1raInteracMartes_visit1() from _call_peqSuHabitacionMiraPc1raInteracMartes_visit1

    else:
        protaPensa "Ya he hablado con ella"
        if tiempoDia != 4:
            scene habHijPeqSgD
        else:
            scene habHijPeqSgN

    call mostrarHud() from _call_mostrarHud_30

    #show screen habHermanPeqCoordenadas

    return
