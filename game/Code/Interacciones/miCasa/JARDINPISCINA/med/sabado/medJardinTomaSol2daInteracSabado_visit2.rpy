label medJardinTomaSol2daInteracSabado_visit2():
    $ conversMedJardinTomaSol2daSabado_HabladoVisit2__ = True
    $ conversMedJardinTomaSol2daSabado_HabladoVisit2 = True

    scene MedJardinTomaSol2daSabado_Fase2_0
    play music "audio/music/hiddenAgenda_by_kevin_macleod.ogg" fadein 2.0
    pause
    scene MedJardinTomaSol2daSabado_Fase2_1 with dissolve
    pause
    scene MedJardinTomaSol2daSabado_Fase2_2 with dissolve
    protaPensa "Si me ganase su confianza podría intentar ponerle crema en la espalda."

    return
