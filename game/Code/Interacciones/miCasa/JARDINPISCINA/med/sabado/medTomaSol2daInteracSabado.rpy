label medTomaSol2daInteracSabado():
    call esconderHud() from _call_esconderHud_41
    hide screen jardinPisTomarSolManyanaSabadoMad
    hide screen jardinPisTomarSolManyanaSabadoMed
    scene MedJardinTomaSol2daSabado_0
    call primeraVezMed() from _call_primeraVezMed_3

    if conversMedJardinTomaSol2daSabado_Hablado == False:

        $ conversMedJardinTomaSol2daSabado_Hablado = True
        call medJardinTomaSol2daInteracSabado_visit1() from _call_medJardinTomaSol2daInteracSabado_visit1
    elif demonBoy == True:
        if conversMedJardinTomaSol2daSabado_HabladoVisit2 == False:
            call medJardinTomaSol2daInteracSabado_visit2 from _call_medJardinTomaSol2daInteracSabado_visit2
        else:
            protaPensa "Ya he hablado con ella"

    else:
        protaPensa "Ya he hablado con ella"
        if tiempoDia != 4:
            scene jardinPisSgD
        else:
            scene jardinPisSgN

    call mostrarHud() from _call_mostrarHud_36

    #show screen jardinPisCoordenadas

    return
