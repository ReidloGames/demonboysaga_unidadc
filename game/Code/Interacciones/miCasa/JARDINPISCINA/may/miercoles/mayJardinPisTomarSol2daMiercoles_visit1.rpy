label mayJardinPisTomarSol2daMiercoles_visit1():
    $ mayJardinPisTomarSol2daInteracMiercoles_visit1 = True
    $ mayJardinPisTomarSol2daMiercoles_visit1__ = True
    pause
    scene hermanMayTomaSol_8 with dissolve
    pause
    scene hermanMayTomaSol_9 with dissolve
    pause
    scene hermanMayTomaSol_10 with dissolve
    pause
    scene hermanMayTomaSol_11 with dissolve
    pause
    scene hermanMayTomaSol_12 with dissolve
    play music "audio/music/Sneaky_Snitch.ogg" fadein 3.0
    protaPensa "{size=15}{i} Parece que no me ha visto. Esta relajada.{/size}{/i}"
    scene hermanMayTomaSol_13 with dissolve
    pause
    scene hermanMayTomaSol_14 with dissolve
    pause
    scene hermanMayTomaSol_15 with dissolve
    pause
    scene hermanMayTomaSol_16 with dissolve
    pause
    scene hermanMayTomaSol_2 with dissolve
    protaPensa "{size=15}{i} Tiene un cuerpo increíble...{/size}{/i}"
    scene hermanMayTomaSol_3 with dissolve
    pause
    scene hermanMayTomaSol_4 with dissolve
    protaPensa "{size=15}{i} Me estoy poniendo enfermo. Dios mío.{/size}{/i}"
    scene hermanMayTomaSol_5 with dissolve
    pause
    scene hermanMayTomaSol_6 with dissolve
    protaPensa "{size=15}{i} Lo que daría por ver lo que hay allí debajo.{/size}{/i}"
    scene hermanMayTomaSol_7 with dissolve
    pause
    scene hermanMayTomaSol_17 with dissolve
    pause
    scene hermanMayTomaSol_18 with dissolve
    pause
    scene hermanMayTomaSol_19 with dissolve
    stop music
    play sound "audio/effects/ramaRota.ogg"
    "¡¡¡CRAAAACK!!!"
    scene hermanMayTomaSol_20 with dissolve
    pause
    play sound "audio/effects/susto.ogg"
    scene hermanMayTomaSol_21 with dissolve
    pause
    scene hermanMayTomaSol_22 with dissolve
    pause
    scene hermanMayTomaSol_23 with dissolve
    pause
    scene hermanMayTomaSol_24 with dissolve
    pause
    scene hermanMayTomaSol_25 with dissolve
    hermanMay "Me has asustado. No sabía que estabas aquí."
    scene hermanMayTomaSol_26 with dissolve
    pause
    scene hermanMayTomaSol_27 with dissolve
    prota "Perdóname, no sabía si dormías y no quería molestarte."
    scene hermanMayTomaSol_31 with dissolve
    pause
    scene hermanMayTomaSol_32 with dissolve
    pause
    scene hermanMayTomaSol_33 with dissolve
    hermanMay "Solo quería relajarme y tomar un poco el sol."
    play music "audio/music/sneaky-adventure-by-kevin-macleod.ogg" fadein 3.0
    pause
    scene hermanMayTomaSol_34 with dissolve
    pause
    scene hermanMayTomaSol_35 with dissolve
    pause
    scene hermanMayTomaSol_36 with dissolve
    pause
    scene hermanMayTomaSol_37 with dissolve
    pause
    scene hermanMayTomaSol_38 with dissolve
    prota "¿Te has puesto protector solar? No me gustaría ver a mi hermana transformada en una gamba"
    scene hermanMayTomaSol_39 with dissolve
    pause
    scene hermanMayTomaSol_43 with dissolve
    hermanMay "jajaja"
    scene hermanMayTomaSol_44 with dissolve
    hermanMay "Si, ya casi no quedaba, pero me pude poner suficiente protector solar."
    scene hermanMayTomaSol_37 with dissolve
    protaPensa "He llegado tarde. ¡Maldición!"
    scene hermanMayTomaSol_29 with dissolve
    prota "Bien te dejo que sigas relajándote."
    scene hermanMayTomaSol_40 with dissolve
    pause
    scene hermanMayTomaSol_41 with dissolve
    hermanMay "Gracias"
    scene hermanMayTomaSol_42 with dissolve
    pause
    scene hermanMayTomaSol_48 with dissolve
    pause
    scene hermanMayTomaSol_49 with dissolve
    pause
    scene hermanMayTomaSol_50 with dissolve
    pause
    scene hermanMayTomaSol_51 with dissolve
    pause
    scene hermanMayTomaSol_52 with dissolve
    pause
    #call nextTiempoDay(0) from _call_nextTiempoDay_21

    return
