label mayTomandoSol2daInteracMiercoles():
    call esconderHud() from _call_esconderHud_45
    hide screen jardinPisTomarSolManyanaMiercolesMay
    scene hermanMayTomaSol_0
    call primeraVezMay() from _call_primeraVezMay_6

    if conversMayJardinPisTomarSol2daMiercoles_Hablado == False:

        if mayJardinPisTomarSol2daInteracMiercoles_visit1 == False:
            $ conversMayJardinPisTomarSol2daMiercoles_Hablado = True
            call mayJardinPisTomarSol2daMiercoles_visit1() from _call_mayJardinPisTomarSol2daMiercoles_visit1
        else:
            protaPensa "No tengo nada mas que decirle"
            if tiempoDia != 4:
                scene jardinPisSgD
            else:
                scene jardinPisSgN

    else:
        protaPensa "Ya he hablado con ella"
        if tiempoDia != 4:
            scene jardinPisSgD
        else:
            scene jardinPisSgN

    call mostrarHud() from _call_mostrarHud_40

    #show screen jardinPisCoordenadas

    return
