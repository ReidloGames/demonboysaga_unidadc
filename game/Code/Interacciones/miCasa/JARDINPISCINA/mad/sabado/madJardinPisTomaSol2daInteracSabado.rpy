label madJardinPisTomaSol2daInteracSabado():
    call esconderHud() from _call_esconderHud_18
    hide screen jardinPisTomarSolManyanaSabadoMad
    hide screen jardinPisTomarSolManyanaSabadoMed
    scene madJardinTomaSol2daSabado_0
    call primeraVezMad() from _call_primeraVezMad_6

    if conversMadJardinPisTomaSol2daSabado_Hablado == False:

        if madJardinPisTomaSol2daInteracSabado_visit1 == False:
            $ conversMadJardinPisTomaSol2daSabado_Hablado = True
            call madJardinPisTomaSol2daInteracSabado_visit1() from _call_madJardinPisTomaSol2daInteracSabado_visit1

        else:
            protaPensa "No tengo nada mas que decirle"
            if tiempoDia != 4:
                scene jardinPisSgD
            else:
                scene jardinPisSgN
    else:
        protaPensa "Ya he hablado con ella"
        if tiempoDia != 4:
            scene jardinPisSgD
        else:
            scene jardinPisSgN

    call mostrarHud() from _call_mostrarHud_13
    #show screen jardinPisCoordenadas

    return
