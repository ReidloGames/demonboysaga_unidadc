label madJardinPisTomaSol2daInteracSabado_visit1():
    $ madJardinPisTomaSol2daInteracSabado_visit1 = True
    pause
    scene madJardinTomaSol2daSabado_1 with dissolve
    pause
    scene madJardinTomaSol2daSabado_2 with dissolve
    pause
    stop music fadeout 2.0
    play music "audio/music/Scheming_Weasel_faster.ogg" fadein 3.0
    scene madJardinTomaSol2daSabado_3 with dissolve
    pause
    scene madJardinTomaSol2daSabado_4 with dissolve
    pause
    scene madJardinTomaSol2daSabado_5 with dissolve
    pause
    scene madJardinTomaSol2daSabado_6 with dissolve
    mad "Ey tambien viniste a tomar el sol?"
    scene madJardinTomaSol2daSabado_10 with dissolve

    pause
    scene madJardinTomaSol2daSabado_9 with dissolve
    pause(0.9)
    scene madJardinTomaSol2daSabado_8 with dissolve
    prota "No [nombreMad3], vine para preguntar si necesitabas que te ayudara en algo."
    scene madJardinTomaSol2daSabado_7 with dissolve
    pause
    scene madJardinTomaSol2daSabado_11 with dissolve
    mad "Ohhh, hoy estas muy servicial te preocupas por tu [situMad]. Gracias cariño"
    play sound "audio/effects/kiss.ogg"
    scene madJardinTomaSol2daSabado_9 with dissolve
    pause(0.6)
    scene madJardinTomaSol2daSabado_20 with dissolve
    pause
    scene madJardinTomaSol2daSabado_10 with dissolve
    pause(0.6)
    scene madJardinTomaSol2daSabado_16 with dissolve
    pause
    scene madJardinTomaSol2daSabado_10 with dissolve
    if eventComunicadoPadCuentaProblema == False:
        mad "No te preocupes, hoy tu [situPad] traera la comida, así que me puedo relajar un poco"
    else:
        mad "No te preocupes, solo voy a relajarme un poco"
    scene madJardinTomaSol2daSabado_8 with dissolve
    prota "Genial entonces"
    scene madJardinTomaSol2daSabado_7 with dissolve
    pause
    scene madJardinTomaSol2daSabado_16 with dissolve
    pause(0.4)
    scene madJardinTomaSol2daSabado_17 with dissolve
    prota "El sol esta muy fuerte, te has puesto protector solar?"
    scene madJardinTomaSol2daSabado_18 with dissolve
    pause(0.4)
    scene madJardinTomaSol2daSabado_12 with dissolve
    mad "Oh, si me puse gracias cielo"
    scene madJardinTomaSol2daSabado_13 with dissolve
    pause
    scene madJardinTomaSol2daSabado_14 with dissolve
    pause
    scene madJardinTomaSol2daSabado_18 with dissolve
    pause(0.3)
    scene madJardinTomaSol2daSabado_19 with dissolve
    pause
    scene madJardinTomaSol2daSabado_15 with dissolve
    pause

    return
