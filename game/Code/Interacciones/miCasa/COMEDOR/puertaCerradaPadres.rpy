label puertaCerradaPadres():
    if puertaCerradaPadres == True and tiempoDia == 4: #de noche cuando duermen
        scene puertaPadr
        if protaHablarConDirectorDinero == True and protaHablaConPadrDinero == False:
            if numDia == 1 or numDia == 2 or numDia == 3 or numDia == 4 or numDia == 5 or numDia == 6:
                call protaHablaConPadrDinero() from _call_protaHablaConPadrDinero
        else:
            play sound "audio/effects/closeDoor.ogg"
            if demonBoy == False:
                protaPensa "La puerta de la habitación esta cerrada"
            else:
                protaPensa "Tengo que encontrar la manera de poder entrar por la noche"
    elif tiempoDia == 0 and not protaHablarConDirectorDinero:
        play sound "audio/effects/open_door.ogg"
        call nextZona("habPadres")
    elif cambiandoseMad == True:
        if hasEntradoHabPadrCambiandose == False:
            scene puertaPadr
            call madCambiandoseRopa() from _call_madCambiandoseRopa
        else:
            call noIrHabPadrOtraVez() from _call_noIrHabPadrOtraVez
    else:
        play sound "audio/effects/open_door.ogg"
        call nextZona("habPadres") from _call_nextZona_7
    return


label madCambiandoseRopa():
    menu:
        "No entrar":
            return
        #"Llamar a la puerta" if demonBoy == True:
        #    $ hasEntradoHabPadrCambiandose = True
        #    return
        "Entrar en la habitación":
            if demonBoy == False:
                $ hasEntradoHabPadrCambiandose = True
                call noEntrarEscenaMad() from _call_noEntrarEscenaMad
            else:
                $ hasEntradoHabPadrCambiandose = True
                #call noEntrarEscenaMad() from _call_noEntrarEscenaMad_1
                call cambiandoseMadEntrando() from _call_cambiandoseMadEntrando
            return


label noIrHabPadrOtraVez():
    scene comedor
    protaPensa "Ya he estado aquí"
    return

label noEntrarEscenaMad():
    scene protaNoEntraHabMad_0 with dissolve
    pause
    scene protaNoEntraHabMad_1 with dissolve
    pause
    scene protaNoEntraHabMad_2 with dissolve
    protaPensa "Mejor no.."
    pause
    return
