label cambiandoseMadEntrando():
    call esconderHud() from _call_esconderHud_57
    $ randomNumMadCambiandose = renpy.random.randint(1, 3)

    if randomNumMadCambiandose == 1:
        call cambiandoseMadEscena1 from _call_cambiandoseMadEscena1
    if randomNumMadCambiandose == 2:
        call cambiandoseMadEscena2 from _call_cambiandoseMadEscena2
    if randomNumMadCambiandose == 3:
        call cambiandoseMadEscena3 from _call_cambiandoseMadEscena3

    call mostrarHud() from _call_mostrarHud_50

    return




label cambiandoseMadEscena1():
    $ cambiandoseMadEscena1 = True
    scene protaNoEntraHabMad_1
    play music "audio/music/Marty_Gots_a_Plan.ogg" fadein 2.0
    pause
    scene cambiandoseMadNum1_0 with dissolve
    pause
    play sound "audio/effects/cajon.ogg"
    scene cambiandoseMadNum1_1 with dissolve
    pause
    scene cambiandoseMadNum1_2 with dissolve
    pause
    scene cambiandoseMadNum1_3 with dissolve
    protaPensa "Joder!! Me voy antes de que se de la vuelta."

    return

label cambiandoseMadEscena2():
    $ cambiandoseMadEscena2 = True
    scene protaNoEntraHabMad_2
    play music "audio/music/Scheming_Weasel_faster.ogg" fadein 2.0
    pause
    scene cambiandoseMadNum2_0 with dissolve
    menu:
        "Esperar":
            play sound "audio/effects/susto.ogg"
            scene cambiandoseMadNum2_1 with dissolve
            stop music
            pause
            scene cambiandoseMadNum2_2 with dissolve
            mad "No puedes entrar así. Tienes que llamar a la puerta."
            scene cambiandoseMadNum2_3 with dissolve
            prota "Perdón, no me fije en que la puerta estaba cerrada."
            scene cambiandoseMadNum2_4 with dissolve
            prota "..."
        "Irte":
            return
    return

label cambiandoseMadEscena3():
    $ cambiandoseMadEscena3 = True
    scene protaNoEntraHabMad_1
    play music "audio/music/hiddenAgenda_by_kevin_macleod.ogg" fadein 2.0
    pause
    scene cambiandoseMadNum3_0 with dissolve
    pause
    play sound "audio/effects/roze1.ogg"
    scene cambiandoseMadNum3_1 with dissolve
    pause
    scene cambiandoseMadNum3_2 with dissolve
    pause
    scene cambiandoseMadNum3_3 with dissolve
    protaPensa "Llegará el día en que ese culito será mío..."

    return
