label medComedor3raInteracMiercoles_visit2():
    pause
    scene medComedorEstudiando3raMiercoles_1 with dissolve
    pause
    scene medComedorEstudiando3raMiercoles_2 with dissolve
    pause
    stop music fadeout 2.0
    scene medComedorEstudiando3raMiercoles_fase2_0 with dissolve
    hermanMed "¿Otra vez tú por aquí?"
    scene medComedorEstudiando3raMiercoles_fase2_1 with dissolve
    hermanMed "Que pesado eres."
    play music "audio/music/hiddenAgenda_by_kevin_macleod.ogg" fadein 2.0
    scene medComedorEstudiando3raMiercoles_fase2_2 with dissolve
    menu:
        "Preguntar por [nombrePad3]" if eventErikaSeVaDeFiesta == False:
            $ medComedor3raInteracMiercoles_visit2__ = True
            $ numDiaMedComedor3raInteracMiercoles_visit2 = numDiaTotal
            scene  medComedorEstudiando3raMiercoles_fase2_3 with dissolve
            prota "Solo quería saber si te encontrabas bien después de la noticia de [nombrePad3]."
            scene  medComedorEstudiando3raMiercoles_fase2_4 with dissolve
            pause
            scene  medComedorEstudiando3raMiercoles_fase2_5 with dissolve
            hermanMed "Déjame en paz, no necesito a nadie. Ni a papa ni a ti."
            scene  medComedorEstudiando3raMiercoles_fase2_6 with dissolve
            protaPensa "Como de costumbre sigue con sus malas pulgas."
            scene  medComedorEstudiando3raMiercoles_fase2_7 with dissolve
            prota "No deberías tratarme así, para empezar solo me preocupo por ti, aparte de que podría ayudarte con tus estudios."
            scene  medComedorEstudiando3raMiercoles_fase2_8 with dissolve
            hermanMed "No me hagas reír."
            scene  medComedorEstudiando3raMiercoles_fase2_9 with dissolve
            hermanMed "Tu ayudarme? ¿en qué?"
            scene  medComedorEstudiando3raMiercoles_fase2_10 with dissolve
            prota "Yo no soy mal estudiante, igual necesitas otras formas de ver las cosas."
            scene  medComedorEstudiando3raMiercoles_fase2_11 with dissolve
            hermanMed "Vale, deja de contarme cuentos chinos."
            scene  medComedorEstudiando3raMiercoles_fase2_12 with dissolve
            hermanMed "Adiós."

            return
        "Irte":
            scene medComedorEstudiando3raMiercoles_4 with dissolve
            prota "Nada ,dejalo..."
            scene medComedorEstudiando3raMiercoles_fase2_5 with dissolve
            hermanMed "Siempre molestando..."




    return
