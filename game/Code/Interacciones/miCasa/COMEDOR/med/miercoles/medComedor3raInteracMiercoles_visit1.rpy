label medComedor3raInteracMiercoles_visit1():
    $ medComedor3raInteracMiercoles_visit1 = True
    $ escondeFrameAyudasMed = True
    pause
    scene medComedorEstudiando3raMiercoles_1 with dissolve
    pause
    scene medComedorEstudiando3raMiercoles_2 with dissolve
    pause
    stop music fadeout 2.0
    scene medComedorEstudiando3raMiercoles_3 with dissolve
    pause
    scene medComedorEstudiando3raMiercoles_4 with dissolve
    prota "¿Estas estudiando?"
    scene medComedorEstudiando3raMiercoles_5 with dissolve
    hermanMed "¿A caso no es evidente?"
    scene medComedorEstudiando3raMiercoles_6 with dissolve
    protaPensa "{size=15}{i}Está estudiando, pero tiene la seguridad que si suspende, papaíto le pagara el curso todas las veces que haga falta hasta que apruebe el master.{/size}{/i}"
    protaPensa "{size=15}{i}Así cualquiera.{/size}{/i}"
    scene medComedorEstudiando3raMiercoles_7 with dissolve
    prota "Bueno, pues ánimos con los estudios."
    scene medComedorEstudiando3raMiercoles_8 with dissolve
    hermanMed "No necesito tu ayuda ni tus ánimos..."
    scene medComedorEstudiando3raMiercoles_9 with dissolve
    protaPensa "{size=15}{i}Que estúpida. Parece un ángel, pero es una bruja...{/size}{/i}"
    protaPensa "{size=15}{i}Aunque está muy buena, pero no se puede tener todo en la vida.{/size}{/i}"

    return
