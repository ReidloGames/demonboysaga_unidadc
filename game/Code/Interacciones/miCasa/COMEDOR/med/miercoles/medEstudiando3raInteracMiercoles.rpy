label medEstudiando3raInteracMiercoles():
    call esconderHud() from _call_esconderHud_24
    hide screen comedorEstudiandoMediodiaMiercolesMed
    scene medComedorEstudiando3raMiercoles_0
    call primeraVezMed() from _call_primeraVezMed_1

    if conversMedComedorEstudiando3raMiercoles_Hablado == False:

        if medComedor3raInteracMiercoles_visit1 == False:
            $ conversMedComedorEstudiando3raMiercoles_Hablado = True
            call medComedor3raInteracMiercoles_visit1() from _call_medComedor3raInteracMiercoles_visit1
        if demonBoy == True:
            if medComedor3raInteracMiercoles_visit2 == False:
                $ conversMedComedorEstudiando3raMiercoles_Hablado = True
                call medComedor3raInteracMiercoles_visit2() from _call_medComedor3raInteracMiercoles_visit2

        else:
            protaPensa "No tengo nada mas que decirle"
            scene comedor
    else:
        protaPensa "Ya he hablado con ella"
        scene comedor

    if not eventPadVuelveTrabajoTriste:
        call eventPadVuelveTrabjoTriste()
    call mostrarHud() from _call_mostrarHud_22
    #show screen comedorCoordenadas

return
