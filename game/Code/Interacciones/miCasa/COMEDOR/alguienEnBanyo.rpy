label hayAlguienEnBanyo():
    if localizaMad == "banyo" or localizaPad == "banyo" or localizaPeq == "banyo" or localizaMed == "banyo" or localizaMay == "banyo":
        scene puertaBanyo
        call esconderHud() from _call_esconderHud_16


        if localizaMad == "banyo" and hasEntradoBanyoMad == False:
            protaPensa "Hay alguien dentro"
            call menuEntrarBanyo() from _call_menuEntrarBanyo
        elif localizaPad == "banyo" and hasEntradoBanyoPad == False:
            protaPensa "Hay alguien dentro"
            call menuEntrarBanyo() from _call_menuEntrarBanyo_1
        elif localizaPeq == "banyo" and hasEntradoBanyoPeq == False:
            protaPensa "Hay alguien dentro"
            call menuEntrarBanyo() from _call_menuEntrarBanyo_2
        elif localizaMed == "banyo" and hasEntradoBanyoMed == False:
            protaPensa "Hay alguien dentro"
            call menuEntrarBanyo() from _call_menuEntrarBanyo_3
        elif localizaMay == "banyo" and hasEntradoBanyoMay == False:
            protaPensa "Hay alguien dentro"
            call menuEntrarBanyo() from _call_menuEntrarBanyo_4
        else:
            protaPensa "No puedo volver a entrar"

    else:
        play sound "audio/effects/open_door.ogg"
        call nextZona("banyo") from _call_nextZona_6
    call mostrarHud() from _call_mostrarHud_10
return

label menuEntrarBanyo():

        menu:
            "No entrar":
                return

            "Entrar en baño" if localizaMad == "banyo" and demonBoy == True:
                $ hasEntradoBanyoMad = True
                call spyBanyoMadEntrando() from _call_spyBanyoMadEntrando
                return
            #"Entrar en baño" if localizaPad == "banyo" and demonBoy == True:
            #    $ hasEntradoBanyoPad = True
            #    return
            "Entrar en baño" if localizaPeq == "banyo" and demonBoy == True:
                $ hasEntradoBanyoPeq = True
                call spyBanyoPeqEntrando() from _call_spyBanyoPeqEntrando
                return
            "Entrar en baño" if localizaMed == "banyo" and demonBoy == True:
                $ hasEntradoBanyoMed = True
                call spyBanyoMedEntrando() from _call_spyBanyoMedEntrando
                return
            "Entrar en baño" if localizaMay == "banyo" and demonBoy == True:
                $ hasEntradoBanyoMay = True
                call spyBanyoMayEntrando() from _call_spyBanyoMayEntrando
                return
