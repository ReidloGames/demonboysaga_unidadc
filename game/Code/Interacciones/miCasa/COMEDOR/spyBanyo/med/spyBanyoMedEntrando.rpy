label spyBanyoMedEntrando():
    call esconderHud() from _call_esconderHud_50
    $ randomNumMedSpyBanyo = renpy.random.randint(1, 2)

    if randomNumMedSpyBanyo == 1:
        call spyBanyoMedEscena1 from _call_spyBanyoMedEscena1
    if randomNumMedSpyBanyo == 2:
        call spyBanyoMedEscena2 from _call_spyBanyoMedEscena2

    call mostrarHud() from _call_mostrarHud_43

    return


label spyBanyoMedEscena1():
    scene spyBanyoEntra
    play music "audio/music/hiddenAgenda_by_kevin_macleod.ogg" fadein 2.0
    pause
    scene spyBanyoHermanMedNum1_0
    pause
    play sound "audio/effects/agua1.ogg"
    scene spyBanyoHermanMedNum1_1
    pause
    play sound "audio/effects/agua2.ogg"
    scene spyBanyoHermanMedNum1_2
    pause
    scene spyBanyoHermanMedNum1_3
    protaPensa "Está muy relajada, si supiera que la estoy mirando..."

    return

label spyBanyoMedEscena2():
    scene spyBanyoEntra
    pause
    play music "audio/music/Marty_Gots_a_Plan.ogg" fadein 2.0
    scene spyBanyoHermanMedNum2_0
    menu:
        "Seguir mirando":
            play sound "audio/effects/sorpresa3.ogg"
            scene spyBanyoHermanMedNum2_1 with dissolve
            pause
            stop music fadeout 2.0
            scene spyBanyoHermanMedNum2_3 with dissolve
            hermanMed "¿Que haces entrando con la puerta cerrada? ¡Sal de mi vista pervertido!"
            scene spyBanyoHermanMedNum2_2 with dissolve
            prota "¡Ya me voy!"
        "Irte":
            return



    return
