label spyBanyoMadEntrando():
    call esconderHud() from _call_esconderHud_54
    $ randomNumMadSpyBanyo = renpy.random.randint(1, 2)

    if randomNumMadSpyBanyo == 1:
        call spyBanyoMadEscena1 from _call_spyBanyoMadEscena1
    if randomNumMadSpyBanyo == 2:
        call spyBanyoMadEscena2 from _call_spyBanyoMadEscena2

    call mostrarHud() from _call_mostrarHud_47

    return

label spyBanyoMadEscena1():
    scene spyBanyoEntra
    pause
    play music "audio/music/Marty_Gots_a_Plan.ogg" fadein 2.0
    scene spyBanyoMadNum1_0 with dissolve
    pause
    play sound "audio/effects/roze3.ogg"
    scene spyBanyoMadNum1_1 with dissolve
    pause
    scene spyBanyoMadNum1_2 with dissolve

    menu:
        "Seguir mirando":
            scene spyBanyoMadNum1_3 with dissolve
            stop music
            play sound "audio/effects/susto.ogg"
            mad "Ehh!"
            scene spyBanyoMadNum1_4 with dissolve
            mad "[nombreProta2], no puedes entrar si la puerta esta cerrada."
            scene spyBanyoMadNum1_5 with dissolve
            prota "Perdón, iba con prisas y no me di cuenta."
            scene spyBanyoMadNum1_6 with dissolve
            pause
            return
        "Irte":
            return


    return

label spyBanyoMadEscena2():
    scene spyBanyoEntra
    stop music fadeout 2.0
    pause
    play sound "audio/effects/ducha2.ogg"
    scene spyBanyoMadNum2_0 with dissolve
    pause
    scene spyBanyoMadNum2_1 with dissolve
    pause
    scene spyBanyoMadNum2_2 with dissolve
    pause
    scene spyBanyoMadNum2_3 with dissolve
    protaPensa "Incluso con la cortina, se puede ver que mi [situMad] tiene un culo increíble y unos pechos enormes y bonitos."
    stop sound fadeout 2.0
    return
