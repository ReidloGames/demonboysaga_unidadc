label spyBanyoPeqEntrando():
    call esconderHud() from _call_esconderHud_56
    $ randomNumPeqSpyBanyo = renpy.random.randint(1, 2)

    if randomNumPeqSpyBanyo == 1:
        call spyBanyoPeqEscena1 from _call_spyBanyoPeqEscena1
    if randomNumPeqSpyBanyo == 2:
        call spyBanyoPeqEscena2 from _call_spyBanyoPeqEscena2

    call mostrarHud() from _call_mostrarHud_49

    return



label spyBanyoPeqEscena1():
    scene spyBanyoEntra
    stop music fadeout 2.0
    pause
    play sound "audio/effects/ducha2.ogg"
    scene spyBanyoHermanPeqNum1_0
    pause
    scene spyBanyoHermanPeqNum1_1 with dissolve
    pause
    scene spyBanyoHermanPeqNum1_2 with dissolve
    pause
    stop sound fadeout 2.0

    return


label spyBanyoPeqEscena2():
    scene spyBanyoEntra
    pause
    play music "audio/music/hiddenAgenda_by_kevin_macleod.ogg" fadein 2.0
    scene spyBanyoHermanPeqNum2_0 with dissolve

    menu:
        "Seguir mirando":
            play sound "audio/effects/sorpresa5.ogg"
            scene spyBanyoHermanPeqNum2_1 with dissolve
            pause
            ##audio
            scene spyBanyoHermanPeqNum2_4 with dissolve
            stop music
            hermanPeq "¿Cómo entras sin llamar a la puerta? ¡Vete!"
            scene spyBanyoHermanPeqNum2_3 with dissolve
            prota "Perdón necesitaba ir al baño y no me fije."
        "Irte":
            return

    return
