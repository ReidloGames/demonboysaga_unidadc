label spyBanyoMayEntrando():
    call esconderHud() from _call_esconderHud_52
    $ randomNumMaySpyBanyo = renpy.random.randint(1, 2)

    if randomNumMaySpyBanyo == 1:
        call spyBanyoMayEscena1 from _call_spyBanyoMayEscena1
    if randomNumMaySpyBanyo == 2:
        call spyBanyoMayEscena2 from _call_spyBanyoMayEscena2

    call mostrarHud() from _call_mostrarHud_45

    return


label spyBanyoMayEscena1():
    scene spyBanyoEntra
    pause
    play music "audio/music/hiddenAgenda_by_kevin_macleod.ogg" fadein 2.0
    scene spyBanyoHermanMayNum1_0 with dissolve
    pause
    play sound "audio/effects/roze1.ogg"
    scene spyBanyoHermanMayNum1_1 with dissolve
    pause
    play sound "audio/effects/roze3.ogg"
    scene spyBanyoHermanMayNum1_2 with dissolve
    pause
    scene spyBanyoHermanMayNum1_3 with dissolve
    protaPensa "Oh dios, se le salen los pechos. Ojalá pudiera estar más cerca."

    return


label spyBanyoMayEscena2():
    scene spyBanyoEntra
    pause
    play music "audio/music/Scheming_Weasel_faster.ogg" fadein 2.0
    scene spyBanyoHermanMayNum2_0 with dissolve
    menu:
        "Seguir mirando":
            play sound "audio/effects/sorpresa3.ogg"
            scene spyBanyoHermanMayNum2_1
            pause
            stop music
            scene spyBanyoHermanMayNum2_2
            play sound "audio/effects/roze3.ogg"
            hermanMay "¡Ey! ¿Por qué entras así?"
            scene spyBanyoHermanMayNum2_3
            prota "Tenía ganas de orinar y no sabía que había alguien dentro..."
            scene spyBanyoHermanMayNum2_2
            hermanMay "Sal y espérate."
        "Irte":
            return

    return
