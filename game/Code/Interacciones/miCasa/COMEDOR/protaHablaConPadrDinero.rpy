label protaHablaConPadrDinero():
    menu:
        "Llamar a la puerta y preguntar por el tema del dinero de la Universidad":
            call protaHablaConPadrDineroHabPadr from _call_protaHablaConPadrDineroHabPadr
        "Nada":
            return

    return


label protaHablaConPadrDineroHabPadr():
    call esconderHud() from _call_esconderHud_38
    $ protaHablaConPadrDinero = True
    $ escondeFrameAyudasMad = True
    $ puedesIrHabPeq = True
    $ puedesIrHabMed = True
    $ puedesIrHabMay = True

    scene protaPreguntaPagoEscuela_0
    pause
    scene protaPreguntaPagoEscuela_1 with dissolve

    pause
    play sound "audio/effects/toc.ogg"
    scene protaPreguntaPagoEscuela_2 with dissolve
    pause

    scene protaPreguntaPagoEscuela_1 with dissolve
    pause
    play sound "audio/effects/toc.ogg"
    scene protaPreguntaPagoEscuela_2 with dissolve
    "Adelante, puedes pasar"
    scene protaPreguntaPagoEscuela_3 with dissolve
    stop music
    pause
    scene protaPreguntaPagoEscuela_4 with dissolve
    pause
    scene protaPreguntaPagoEscuela_5 with dissolve
    play music "audio/music/sneaky-adventure-by-kevin-macleod.ogg" fadein 3.0
    prota "Hola, siento molestar."
    scene protaPreguntaPagoEscuela_6 with dissolve
    mad "¿Que pasa [nombreProta2]? es raro verte aquí a estas horas."
    scene protaPreguntaPagoEscuela_7 with dissolve
    prota "Bueno quería veros a los dos porque ha pasado algo extraño en la universidad."
    scene protaPreguntaPagoEscuela_8 with dissolve
    prota "Igual no es nada, pero quería informaros por si pasaba algo extraño."
    scene protaPreguntaPagoEscuela_9 with dissolve
    prota "Me dijo el director que no se ha realizado el pago de la cuota y no parecía muy contento."
    scene protaPreguntaPagoEscuela_10 with dissolve
    play sound "audio/effects/como.ogg"
    mad "¿¿¿Como???"
    scene protaPreguntaPagoEscuela_11 with dissolve
    mad "¿John sabes algo de esto?"
    scene protaPreguntaPagoEscuela_12 with dissolve
    pad "Bueno... yo..."
    scene protaPreguntaPagoEscuela_13 with dissolve
    pause
    scene protaPreguntaPagoEscuela_14 with dissolve
    mad "¿Que ha pasado?"
    scene protaPreguntaPagoEscuela_15 with dissolve
    pad "No sabía cómo contarte esto, pero tuve un problema en el trabajo..."
    scene protaPreguntaPagoEscuela_16 with dissolve
    mad "¿Que clase de problema?"
    scene protaPreguntaPagoEscuela_17 with dissolve
    pad "Perdóname por favor..."
    scene protaPreguntaPagoEscuela_18 with dissolve
    mad "Me estas asustando..."
    scene protaPreguntaPagoEscuela_19 with dissolve
    pause
    scene protaPreguntaPagoEscuela_20 with dissolve
    pad "No sé cómo decirte esto... pero el otro día tuve una cena de empresa y me emborraché demasiado."
    scene protaPreguntaPagoEscuela_21 with dissolve
    pad "La cuestión es que una cosa llevo a la otra y empezamos a jugar al póker."
    scene protaPreguntaPagoEscuela_22 with dissolve
    mad "Oh dios... que no sea lo que me estoy imaginando."
    scene protaPreguntaPagoEscuela_23 with dissolve
    pad "Empezamos a jugar, yo borracho y..."
    scene protaPreguntaPagoEscuela_24 with dissolve
    mad "¡¡¡Y que!!! dilo ya!!"
    scene protaPreguntaPagoEscuela_25 with dissolve
    pad "Me aposté la casa y perdí..."
    scene protaPreguntaPagoEscuela_26 with dissolve
    play sound "audio/effects/grito.ogg"
    mad "¡¡¡NO!!!"
    scene protaPreguntaPagoEscuela_27 with dissolve
    pause
    play sound "audio/effects/grito.ogg"
    scene protaPreguntaPagoEscuela_28 with dissolve
    mad "¡¡¡Como me has podido hacer esto!!!"
    scene protaPreguntaPagoEscuela_29 with dissolve
    pause
    scene protaPreguntaPagoEscuela_30 with dissolve
    protaPensa "Esto pinta muy mal sabía que pasaba algo malo..."
    scene protaPreguntaPagoEscuela_31 with dissolve
    pause
    scene protaPreguntaPagoEscuela_32 with dissolve
    pause
    scene protaPreguntaPagoEscuela_33 with dissolve
    protaPensa "Oh dios santo, con todo lo que está pasando y yo mirando el culo de mi [situMad2]. Demasiadas cosas por procesar."
    scene protaPreguntaPagoEscuela_34 with dissolve
    pause
    scene protaPreguntaPagoEscuela_35 with dissolve
    pad "No solo eso, el banco se enteró de mi estado y de que estaba jugando al póker y apostando..."
    scene protaPreguntaPagoEscuela_36 with dissolve
    pad "Y me despidió, porque considero que una persona con mi cargo no puede ser confiable y no puede dirigir las gestiones del banco si hace esta clase de cosas..."
    pad "Estoy muy arrepentido, me deje llevar…"
    scene protaPreguntaPagoEscuela_37 with dissolve
    mad "Me va a coger un ataque. ¡¡¡Eres un irresponsable!!!"
    scene protaPreguntaPagoEscuela_38 with dissolve
    mad "Me has estado engañando, pensando que se podía confiar en ti. Me has hecho daño."
    scene protaPreguntaPagoEscuela_39 with dissolve
    pad "Yo no quería... pero el alcohol. Me deje llevar."
    scene protaPreguntaPagoEscuela_40 with dissolve
    pad "Pero lo arreglare..."
    scene protaPreguntaPagoEscuela_41 with dissolve
    pad "Hare que no nos quiten la casa..."
    scene protaPreguntaPagoEscuela_42 with dissolve
    pad "Mi primo George me ha ofrecido un empleo..."
    scene protaPreguntaPagoEscuela_43 with dissolve
    mad "¡¡¡Pero si tu primo es camionero!!! estás loco!!!"
    play sound "audio/effects/grito2.ogg"
    scene protaPreguntaPagoEscuela_44 with dissolve

    mad "Estoy con un demente, no me lo puedo creer."
    scene protaPreguntaPagoEscuela_45 with dissolve
    mad "Me va a dar algo..."
    scene protaPreguntaPagoEscuela_46 with dissolve
    pad "Lo siento..."
    scene protaPreguntaPagoEscuela_47 with dissolve
    mad "¡¡¡No quiero verte!!!"
    scene protaPreguntaPagoEscuela_48 with dissolve
    mad "Ahora tendré que volver a trabajar. Esto hará que cambie todas nuestras vidas."
    scene protaPreguntaPagoEscuela_49 with dissolve
    mad "Oh dios que has hecho..."
    scene protaPreguntaPagoEscuela_50 with dissolve
    mad "Lo siento cielo, por haber tenido que vivir esta situación yo tampoco me lo esperaba, no te preocupes lo hablaremos mañana, lo arreglaremos..."
    pad "Lo siento..."
    scene protaPreguntaPagoEscuela_51 with dissolve
    prota "Me voy, os dejo hablar de esto."
    scene protaPreguntaPagoEscuela_52 with dissolve
    protaPensa "No me creo que este pasando esto… ¿Y ahora que pasara con todo? es muy fuerte..."
    call mostrarHud() from _call_mostrarHud_33

    return
