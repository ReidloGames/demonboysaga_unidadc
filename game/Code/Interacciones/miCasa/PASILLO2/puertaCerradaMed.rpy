label puertaCerradaMed():
    if puertaCerradaMed == True and tiempoDia == 4: #de noche cuando duermen
        scene puertaHabMed
        play sound "audio/effects/closeDoor.ogg"
        if demonBoy == False:
            protaPensa "La puerta de la habitación esta cerrada"
        else:
            protaPensa "Tengo que encontrar la manera de poder entrar por la noche"

    elif cambiandoseMed == True:                    #Se estan cambiando de ropa
        if hasEntradoHabMedCambiandose == False:
            scene puertaHabMed
            protaPensa "La puerta esta cerrada..."
            call medCambiandoseRopa() from _call_medCambiandoseRopa
        else:
            call noIrHabMedOtraVez() from _call_noIrHabMedOtraVez
    else:
        play sound "audio/effects/open_door.ogg"
        call nextZona("habHermanMed") from _call_nextZona_5
    return


label medCambiandoseRopa():
    menu:
        "No entrar":
            return
        #"Llamar a la puerta" if demonBoy == True:
        #    $ hasEntradoHabMedCambiandose = True
        #    return
        "Entrar en la habitación":
            if demonBoy == False:
                $ hasEntradoHabMedCambiandose = True
                call noEntrarEscenaMed() from _call_noEntrarEscenaMed
            else:
                $ hasEntradoHabMedCambiandose = True
                #call noEntrarEscenaMed() from _call_noEntrarEscenaMed_1
                call cambiandoseMedEntrando() from _call_cambiandoseMedEntrando
            return


label noIrHabMedOtraVez():
    scene pasillo2
    protaPensa "Ya he estado aquí"
    return

label noEntrarEscenaMed():
    scene protaNoEntraHabMed_0 with dissolve
    pause
    scene protaNoEntraHabMed_1 with dissolve
    pause
    scene protaNoEntraHabMed_2 with dissolve
    protaPensa "Mejor no.."
    pause
    return
