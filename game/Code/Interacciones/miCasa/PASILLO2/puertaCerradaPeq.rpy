label puertaCerradaPeq():
    if puertaCerradaPeq == True and tiempoDia == 4: #de noche cuando duermen
        scene puertaHabPeq
        play sound "audio/effects/closeDoor.ogg"
        if demonBoy == False:
            protaPensa "La puerta de la habitación esta cerrada"
        else:
            protaPensa "Tengo que encontrar la manera de poder entrar por la noche"

    elif cambiandosePeq == True:                    #Se estan cambiando de ropa
        if hasEntradoHabPeqCambiandose == False:
            scene puertaHabPeq
            protaPensa "La puerta esta cerrada..."
            call peqCambiandoseRopa() from _call_peqCambiandoseRopa
        else:
            call noIrHabPeqOtraVez() from _call_noIrHabPeqOtraVez
    else:
        play sound "audio/effects/open_door.ogg"
        call nextZona("habHermanPeq") from _call_nextZona_3
    return


label peqCambiandoseRopa():
    menu:
        "No entrar":
            return
        #"Llamar a la puerta" if demonBoy == True:
        #    $ hasEntradoHabPeqCambiandose = True
        #    return
        "Entrar en la habitación":
            if demonBoy == False:
                $ hasEntradoHabPeqCambiandose = True
                call noEntrarEscenaPeq() from _call_noEntrarEscenaPeq
            else:
                $ hasEntradoHabPeqCambiandose = True
                #call noEntrarEscenaPeq() from _call_noEntrarEscenaPeq_1
                call cambiandosePeqEntrando from _call_cambiandosePeqEntrando
            return


label noIrHabPeqOtraVez():
    scene pasillo2
    protaPensa "Ya he estado aquí"
    return

label noEntrarEscenaPeq():
    scene protaNoEntraHabPeq_0 with dissolve
    pause
    scene protaNoEntraHabPeq_1 with dissolve
    pause
    scene protaNoEntraHabPeq_2 with dissolve
    protaPensa "Mejor no.."
    pause
    return
