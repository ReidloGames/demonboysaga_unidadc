label cambiandoseMedEntrando():
    call esconderHud() from _call_esconderHud_51
    $ randomNumMedCambiandose = renpy.random.randint(1, 3)

    if randomNumMedCambiandose == 1:
        call cambiandoseMedEscena1 from _call_cambiandoseMedEscena1
    if randomNumMedCambiandose == 2:
        call cambiandoseMedEscena2 from _call_cambiandoseMedEscena2
    if randomNumMedCambiandose == 3:
        call cambiandoseMedEscena3 from _call_cambiandoseMedEscena3

    call mostrarHud() from _call_mostrarHud_44
    return


#scene1
label cambiandoseMedEscena1():
    $ cambiandoseMedEscena1 = True
    stop music fadeout 2.0
    play sound "audio/effects/abrirPuertaSigilo.ogg"
    scene cambiandoseHermanMed_0
    pause
    play music "audio/music/Marty_Gots_a_Plan.ogg" fadein 2.0
    play sound "audio/effects/spray.ogg"
    scene cambiandoseHermanMedNum1_0 with dissolve
    pause
    scene cambiandoseHermanMedNum1_1 with dissolve
    pause
    scene cambiandoseHermanMedNum1_2 with dissolve
    pause
    scene cambiandoseHermanMedNum1_3 with dissolve
    pause
    scene cambiandoseHermanMedNum1_4 with dissolve
    protaPensa "Menudo cuerpo tiene. Me voy antes de que me vea."

    return

#scene2
label cambiandoseMedEscena2():
    $ cambiandoseMedEscena2 = True
    stop music fadeout 2.0
    play sound "audio/effects/abrirPuertaSigilo.ogg"
    scene cambiandoseHermanMed_0
    play music "audio/music/hiddenAgenda_by_kevin_macleod.ogg" fadein 2.0
    pause
    scene cambiandoseHermanMedNum2_0 with dissolve
    pause
    scene cambiandoseHermanMedNum2_1 with dissolve
    pause
    play sound "audio/effects/roze1.ogg"
    scene cambiandoseHermanMedNum2_2 with dissolve
    pause
    play sound "audio/effects/roze3.ogg"
    scene cambiandoseHermanMedNum2_3 with dissolve
    pause
    play sound "audio/effects/roze1.ogg"
    scene cambiandoseHermanMedNum2_5 with dissolve
    pause
    scene cambiandoseHermanMed_2  with dissolve
    protaPensa "Tiene un culo increíble. Lástima de su mal humor."
    return

#scene3
label cambiandoseMedEscena3():
    $ cambiandoseMedEscena3 = True
    stop music fadeout 2.0
    play sound "audio/effects/abrirPuertaSigilo.ogg"
    scene cambiandoseHermanMed_0
    pause
    play music "audio/music/Scheming_Weasel_faster.ogg" fadein 2.0
    scene cambiandoseHermanMedNum3_0 with dissolve
    pause
    scene cambiandoseHermanMedNum3_1 with dissolve
    menu:
        "Continuar mirando":
            scene cambiandoseHermanMedNum3_2 with dissolve
            play sound "audio/effects/sorpresa4.ogg"
            pause
            play sound "audio/effects/susto1.ogg"
            scene cambiandoseHermanMedNum3_3 with dissolve
            pause
            ##sonido sorpresa
            scene cambiandoseHermanMedNum3_4 with dissolve
            hermanMed "¡Eres un cerdo pervertido!"
            scene cambiandoseHermanMedNum3_6 with dissolve
            prota "¡Perdón! No me di cuenta de que la puerta estaba cerrada."
            scene cambiandoseHermanMedNum3_5 with dissolve
            hermanMed "¡Sal de mi habitación, pervertido!"
        "Irte":
            return

    return
