label cambiandoseMayEntrando():
    call esconderHud() from _call_esconderHud_55
    $ randomNumMayCambiandose = renpy.random.randint(1, 3)

    if randomNumMayCambiandose == 1:
        call cambiandoseMayEscena1 from _call_cambiandoseMayEscena1
    if randomNumMayCambiandose == 2:
        call cambiandoseMayEscena2 from _call_cambiandoseMayEscena2
    if randomNumMayCambiandose == 3:
        call cambiandoseMayEscena3 from _call_cambiandoseMayEscena3

    call mostrarHud() from _call_mostrarHud_48

    return

#scene1
label cambiandoseMayEscena1():
    $ cambiandoseMayEscena1 = True
    scene cambiandoseHermanMay_0
    play sound "audio/effects/abrirPuertaSigilo.ogg"
    pause
    play music "audio/music/Marty_Gots_a_Plan.ogg" fadein 2.0
    scene cambiandoseHermanMayNum1_0 with dissolve
    pause
    play sound "audio/effects/roze3.ogg"
    scene cambiandoseHermanMayNum1_1 with dissolve
    pause
    play sound "audio/effects/golpecitoSuelo.ogg"
    scene cambiandoseHermanMayNum1_2 with dissolve
    pause
    scene cambiandoseHermanMayNum1_3 with dissolve
    pause
    scene cambiandoseHermanMayNum1_4 with dissolve
    protaPensa "Haría tantas cosas en ese culo..."

    return

#scene2
label cambiandoseMayEscena2():
    $ cambiandoseMayEscena2 = True
    scene cambiandoseHermanMay_0
    play sound "audio/effects/abrirPuertaSigilo.ogg"
    pause
    play music "audio/music/hiddenAgenda_by_kevin_macleod.ogg" fadein 2.0
    scene cambiandoseHermanMayNum2_0 with dissolve
    pause
    scene cambiandoseHermanMayNum2_1 with dissolve
    pause
    play sound "audio/effects/percha.ogg"
    scene cambiandoseHermanMayNum2_2 with dissolve
    pause
    scene cambiandoseHermanMayNum2_3 with dissolve
    protaPensa "Oh dios, esta desnuda. Tiene unas tetas alucinantes, si me viera estoy muerto."

    return

#scene3
label cambiandoseMayEscena3():
    $ cambiandoseMayEscena3 = True
    scene cambiandoseHermanMay_0
    play sound "audio/effects/abrirPuertaSigilo.ogg"
    pause
    play music "audio/music/Scheming_Weasel_faster.ogg" fadein 2.0
    scene cambiandoseHermanMayNum3_0 with dissolve
    pause
    play sound "audio/effects/roze1.ogg"
    scene cambiandoseHermanMayNum3_1 with dissolve

    menu:
        "Continuar mirando":
            play sound "audio/effects/sorpresa3.ogg"
            scene cambiandoseHermanMayNum3_2 with dissolve
            pause
            scene cambiandoseHermanMayNum3_3 with dissolve
            prota "Oh lo siento, no sabía que te estabas cambiando de ropa."
            scene cambiandoseHermanMayNum3_4 with dissolve
            hermanMay "Tienes que llamar a la puerta antes de entrar"
            scene cambiandoseHermanMayNum3_3 with dissolve
            prota "Perdón, no me di cuenta."
            return
        "Irte":
            return


    return
