label cambiandosePeqEntrando():
    call esconderHud() from _call_esconderHud_49
    $ randomNumPeqCambiandose = renpy.random.randint(1, 3)

    if randomNumPeqCambiandose == 1:
        call cambiandosePeqEscena1 from _call_cambiandosePeqEscena1
    if randomNumPeqCambiandose == 2:
        call cambiandosePeqEscena2 from _call_cambiandosePeqEscena2
    if randomNumPeqCambiandose == 3:
        call cambiandosePeqEscena3 from _call_cambiandosePeqEscena3

    call mostrarHud() from _call_mostrarHud_1
    return

#scene1
label cambiandosePeqEscena1():
    $ cambiandosePeqEscena1 = True
    scene cambiandoseHermanPeq_0
    play music "audio/music/Marty_Gots_a_Plan.ogg" fadein 2.0
    pause
    scene cambiandoseHermanPeq_1 with dissolve
    pause
    scene cambiandoseHermanPeqNum1_0 with dissolve
    pause
    scene cambiandoseHermanPeqNum1_1 with dissolve
    pause
    scene cambiandoseHermanPeqNum1_2 with dissolve
    pause
    scene cambiandoseHermanPeqNum1_3 with dissolve
    pause
    play sound "audio/effects/teclear.ogg"
    scene cambiandoseHermanPeqNum1_4 with dissolve
    stop sound
    pause
    scene cambiandoseHermanPeqNum1_5 with dissolve
    pause
    scene cambiandoseHermanPeqNum1_6 with dissolve
    pause
    scene cambiandoseHermanPeqNum1_7 with dissolve
    pause
    scene cambiandoseHermanPeqNum1_8 with dissolve
    protaPensa "Uff..."

    return


#scene2
label cambiandosePeqEscena2():
    $ cambiandosePeqEscena2 = True
    scene cambiandoseHermanPeqNum2_1
    pause
    play music "audio/music/Scheming_Weasel_faster.ogg" fadein 2.0
    scene cambiandoseHermanPeqNum2_2 with dissolve
    pause
    play sound "audio/effects/roze3.ogg"
    scene cambiandoseHermanPeqNum2_3 with dissolve
    pause
    play sound "audio/effects/goma.ogg"
    scene cambiandoseHermanPeqNum2_4 with dissolve
    pause
    scene cambiandoseHermanPeqNum2_5 with dissolve
    pause
    scene cambiandoseHermanPeqNum2_6 with dissolve
    protaPensa "Me voy antes de que me vea"
    scene cambiandoseHermanPeqNum2_7 with dissolve
    pause
    scene cambiandoseHermanPeqNum2_8 with dissolve
    pause

    return

#scene3
label cambiandosePeqEscena3():
    $ cambiandosePeqEscena3 = True
    play music "audio/music/Sneaky_Snitch.ogg" fadein 2.0
    play sound "audio/effects/sorpresa2.ogg"
    scene cambiandoseHermanPeqNum3_1
    pause(0.2)
    play sound "audio/effects/sorpresa3.ogg"
    scene cambiandoseHermanPeqNum3_2 with dissolve
    stop music fadeout 2.0
    hermanPeq "EEEEEY!!!"
    scene cambiandoseHermanPeqNum3_3 with dissolve
    pause
    scene cambiandoseHermanPeqNum3_4 with dissolve
    hermanPeq "¿No sabes llamar a la puerta antes de entrar?"
    scene cambiandoseHermanPeqNum3_5 with dissolve
    play sound "audio/effects/sorpresaHombre.ogg"
    prota "Perdón, no me di cuenta. Ya me voy."
    scene cambiandoseHermanPeqNum3_6 with dissolve
    protaPensa "Ohh le vi las tetas, son rosaditas... Increíble."

    return
