label puertaCerradaMay():
    if puertaCerradaMay == True and tiempoDia == 4: #de noche cuando duermen
        scene puertaHabMay
        play sound "audio/effects/closeDoor.ogg"
        if demonBoy == False:
            protaPensa "La puerta de la habitación esta cerrada"
        else:
            if tengoLlaveHabitacionKara == False:
                protaPensa "Tengo que encontrar la manera de poder entrar por la noche"
            else:
                if hasEntradoHabMayNocheDuerme == False:
                    call spyDeNocheKara
                else:
                    protaPensa "Mejor no volver a entrar."


    elif cambiandoseMay == True:                    #Se estan cambiando de ropa
        if hasEntradoHabMayCambiandose == False:
            scene puertaHabMay
            protaPensa "La puerta esta cerrada..."
            call mayCambiandoseRopa() from _call_mayCambiandoseRopa
        else:
            call noIrHabMayOtraVez() from _call_noIrHabMayOtraVez
    else:
        play sound "audio/effects/open_door.ogg"
        call nextZona("habHermanMay") from _call_nextZona_8
    return


label mayCambiandoseRopa():
    menu:
        "No entrar":
            return
        #"Llamar a la puerta" if demonBoy == True:
        #    $ hasEntradoHabMayCambiandose = True
        #    return
        "Entrar en la habitación":
            if demonBoy == False:
                $ hasEntradoHabMayCambiandose = True
                call noEntrarEscenaMay() from _call_noEntrarEscenaMay
            else:
                $ hasEntradoHabMayCambiandose = True
                #call noEntrarEscenaMay() from _call_noEntrarEscenaMay_1
                call cambiandoseMayEntrando() from _call_cambiandoseMayEntrando
            return


label noIrHabMayOtraVez():
    scene pasillo2
    protaPensa "Ya he estado aquí"
    return

label noEntrarEscenaMay():
    scene protaNoEntraHabMay_0 with dissolve
    pause
    scene protaNoEntraHabMay_1 with dissolve
    pause
    scene protaNoEntraHabMay_2 with dissolve
    protaPensa "Mejor no.."
    pause
    return
