label mayHaceGymGaleria1raInteracViernes_visit1():
    $ mayHaceGymGaleria1raInteracViernes_visit1 = True
    pause
    scene mayHaceGymGaleria1raViernes_1 with dissolve
    pause
    stop music fadeout 3.0
    scene mayHaceGymGaleria1raViernes_2 with dissolve
    pause
    scene mayHaceGymGaleria1raViernes_3 with dissolve
    pause
    play music "audio/music/Marty_Gots_a_Plan.ogg" fadein 2.0
    scene mayHaceGymGaleria1raViernes_4 with dissolve
    pause
    scene mayHaceGymGaleria1raViernes_5 with dissolve
    prota "¿Trabajando duro?"
    scene mayHaceGymGaleria1raViernes_6 with dissolve
    pause
    scene mayHaceGymGaleria1raViernes_7 with dissolve
    hermanMay "Hay que mantenerse en forma"
    scene mayHaceGymGaleria1raViernes_8 with dissolve
    pause
    scene mayHaceGymGaleria1raViernes_9 with dissolve
    pause
    scene mayHaceGymGaleria1raViernes_10 with dissolve
    prota "Pero si tú tienes un cuerpo de modelo."
    scene mayHaceGymGaleria1raViernes_11 with dissolve
    pause
    scene mayHaceGymGaleria1raViernes_8 with dissolve
    hermanMay "jajajaja"
    scene mayHaceGymGaleria1raViernes_7 with dissolve
    hermanMay "Que exagerado. Tú que me ves con buenos ojos."
    scene mayHaceGymGaleria1raViernes_12 with dissolve
    hermanMay "Me tengo que cuidar ya que cuando compro nuevas prendas para la tienda, me las pruebo para ver cómo me quedan. Me hago una idea de si les gustara a los clientes. A parte que me gusta estar en buena forma."
    scene mayHaceGymGaleria1raViernes_13 with dissolve
    hermanMay "No puedo permitirme tener un cuerpo en baja forma porque la ropa no me quedaría bien y en una tienda de moda vende la imagen."
    scene mayHaceGymGaleria1raViernes_14 with dissolve
    pause
    scene mayHaceGymGaleria1raViernes_15 with dissolve
    pause
    scene mayHaceGymGaleria1raViernes_14 with dissolve
    prota "En eso tienes toda la razón. Si necesitas que te dé un golpe de mano házmelo saber."
    scene mayHaceGymGaleria1raViernes_7 with dissolve
    hermanMay "Muchas gracias"
    scene mayHaceGymGaleria1raViernes_17 with dissolve
    pause
    scene mayHaceGymGaleria1raViernes_18 with dissolve
    pause
    scene mayHaceGymGaleria1raViernes_19 with dissolve
    pause
    scene mayHaceGymGaleria1raViernes_20 with dissolve
    pause
    scene mayHaceGymGaleria1raViernes_21 with dissolve
    pause
    scene mayHaceGymGaleria1raViernes_22 with dissolve
    pause
    scene mayHaceGymGaleria1raViernes_23 with dissolve
    pause
    scene mayHaceGymGaleria1raViernes_24 with dissolve
    pause
    scene mayHaceGymGaleria1raViernes_25 with dissolve
    pause
    call nextTiempoDay(0) from _call_nextTiempoDay_10
    #call nextZona("pasillo1") from _call_nextZona_9

    return
