label mayHaceGym1raInteracViernes():
    call esconderHud() from _call_esconderHud_34
    hide screen galeriaGymGimnasiaAmanecerViernesMay

    if conversMayHaceGymGaleria1raViernes_Hablado == False:
        scene mayHaceGymGaleria1raViernes_0
        call primeraVezMay() from _call_primeraVezMay_2

        if mayHaceGymGaleria1raInteracViernes_visit1 == False:
            $ conversMayHaceGymGaleria1raViernes_Hablado = True
            call mayHaceGymGaleria1raInteracViernes_visit1() from _call_mayHaceGymGaleria1raInteracViernes_visit1

        else:
            if demonBoy == True:
                call mayHaceGymNadaQueDecirRandom() from _call_mayHaceGymNadaQueDecirRandom
            protaPensa "No tengo nada mas que decirle"
            scene galeriaGym
    else:
        protaPensa "Ya he hablado con ella"
        scene galeriaGym

    call mostrarHud() from _call_mostrarHud_29
    #show screen galeriaGymCoordenadas

    return

label mayHaceGymNadaQueDecirRandom():
    $ mayHaceGymNadaQueDecirRandom = renpy.random.randint(1,2)

    if mayHaceGymNadaQueDecirRandom == 1:
        scene mayHaceGymYaVisto_1_0 with dissolve
        pause
        scene mayHaceGymYaVisto_1_1 with dissolve
        pause
        scene mayHaceGymYaVisto_1_2 with dissolve

    if mayHaceGymNadaQueDecirRandom == 2:
        scene mayHaceGymYaVisto_2_0 with dissolve
        pause
        scene mayHaceGymYaVisto_2_1 with dissolve
        pause
        scene mayHaceGymYaVisto_2_2 with dissolve

    return
