label peqHaceGym3raInteracLunes():
    call esconderHud() from _call_esconderHud_36
    hide screen galeriaGymLavaderoMediodiaLunesPeq
    scene peqHaceGymGaleria3raLunes_0
    call primeraVezPeq() from _call_primeraVezPeq_4

    if conversPeqGaleriaHaciendoGym3raLunes_Hablado == False:

        if peqGaleriaHaciendoGym3raInteracLunes_visit1 == False:
            $ conversPeqGaleriaHaciendoGym3raLunes_Hablado = True
            call peqGaleriaHaciendoGym3raInteracLunes_visit1() from _call_peqGaleriaHaciendoGym3raInteracLunes_visit1
        elif peqGaleriaHaciendoGym3raInteracLunes_visit2 == False and animarHermanPeqSuHabitacion == True:
            call hablarConPeqEnGym from _call_hablarConPeqEnGym

        else:
            protaPensa "No tengo nada mas que decirle"
            scene galeriaGym
    else:
        protaPensa "Ya he hablado con ella"
        scene galeriaGym

    call mostrarHud() from _call_mostrarHud_31
    #show screen galeriaGymCoordenadas

    return



label hablarConPeqEnGym():
    scene peqHaceGymGaleria3raLunes_5 with dissolve
    pause
    menu:
        "¿Te vienes a jugar a la consola?":
            if hasHabladoConPeqEnElColeTrabajosAcabados == True:
                scene peqHaceGymGaleria3raLunes_11 with dissolve
                hermanPeq "Claro!"
                call peqJugarConsola
            else:
                $ numVecesGymPreguntaJugar = numVecesGymPreguntaJugar +1
                $ hasHabladoJugarConConsola = True
                if numVecesGymPreguntaJugar == 1:
                    $ numDiaAparecePeqEnEscuelaFinEstudio = numDiaTotal
                scene hablarConPeqEnGymJugarConsola_0 with dissolve
                pause
                scene hablarConPeqEnGymJugarConsola_1 with dissolve
                hermanPeq "Me encantaría, pero aún no he acabado las tareas de clase."
                scene hablarConPeqEnGymJugarConsola_2 with dissolve
                prota "Que pena, en otra ocasión entonces..."
                call nextTiempoDay(0) from _call_nextTiempoDay_34


    return
