label peqJugarConsola():
    stop music
    play music "audio/music/funkorama-by-kevin-macleod.ogg" fadein 1.0

    $ numContJugarConsola = numContJugarConsola +1
    if numContJugarConsola == 1:
        scene protaPeqJugandoConsola_0 with dissolve
        pause
        scene protaPeqJugandoConsola_1 with dissolve
        pause
        scene protaPeqJugandoConsola_2 with dissolve
        pause
        scene protaPeqJugandoConsola_3 with dissolve
        pause
        scene protaPeqJugandoConsola_4 with dissolve
        hermanPeq "¡Hahaha que malo eres!"
        scene protaPeqJugandoConsola_5 with dissolve
        hermanPeq "Maldita sea..."

    if numContJugarConsola > 1:
        scene protaPeqJugandoConsola_0 with dissolve
        pause
        scene protaPeqJugandoConsola_1 with dissolve
        pause
        scene protaPeqJugandoConsola_2 with dissolve
        pause
        scene protaPeqJugandoConsola_3 with dissolve
        pause
        scene consola5 with dissolve
        if victoriaConsolaPeq < 2:
            call miniGameClick("jugarConsolaPeq",5)
        else:
            $ luchaAlmohadasPeq = True
            scene protaPeqJugandoConsola_7 with dissolve
            prota "Admítelo soy mejor que tu"
            stop music fadeout 2.0
            scene protaPeqJugandoConsola_10 with dissolve
            hermanPeq "¡Calla!"
            scene protaPeqJugandoConsola_11 with dissolve
            pause
            play sound "audio/effects/sientaSofa.ogg"
            scene protaPeqJugandoConsola_12 with dissolve
            pause

            scene protaPeqJugandoConsola_13 with dissolve
            prota "¡¡Oh!! quieres guerra!"
            play music "audio/music/funkorama-by-kevin-macleod.ogg" fadein 1.0
            scene protaPeqJugandoConsola_14 with dissolve
            pause
            scene protaPeqJugandoConsola_16 with dissolve
            hermanPeq "¿Tienes miedo?"
            scene protaPeqJugandoConsola_15 with dissolve
            prota "¡Te vas a enterar!"
            scene protaPeqJugandoConsola_17 with dissolve
            pause
            play sound "audio/effects/massagePie1.ogg"
            scene protaPeqJugandoConsola_18 with dissolve
            prota "¡No puedes conmigo!"

            scene protaPeqJugandoConsola_19 with dissolve
            pause
            play sound "audio/effects/golpeAu1.ogg"
            scene protaPeqJugandoConsola_20 with dissolve
            pause
            scene protaPeqJugandoConsola_21 with dissolve
            pause
            scene protaPeqJugandoConsola_22 with dissolve
            pause
            play sound "audio/effects/golpeMesa2.ogg"
            scene protaPeqJugandoConsola_23 with hpunch
            pause
            scene protaPeqJugandoConsola_24 with dissolve
            hermanPeq "¡Toma! ¡Te ganado!"
            scene protaPeqJugandoConsola_25 with dissolve
            prota "Ni lo sueñes..."
            scene protaPeqJugandoConsola_26 with dissolve
            hermanPeq "¡Te vas a enterar!"
            scene protaPeqJugandoConsola_27 with dissolve
            pause
            scene protaPeqJugandoConsola_28 with dissolve
            pause
            play sound "audio/effects/sentarseSofa.ogg"
            scene protaPeqJugandoConsola_29 with hpunch
            pause
            stop music
            scene protaPeqJugandoConsola_30 with dissolve
            prota "¡Yo soy más fuerte!"
            play music "audio/music/ChillWave_kevinMacLeod.ogg" fadein 1.0

            scene protaPeqJugandoConsola_31 with dissolve
            pause
            scene protaPeqJugandoConsola_32 with dissolve
            hermanPeq "¡Vale, me rindo!"
            scene protaPeqJugandoConsola_33 with dissolve
            prota "¿Seguro?"
            scene protaPeqJugandoConsola_34 with dissolve
            pause
            scene protaPeqJugandoConsola_35 with dissolve
            pause
            scene protaPeqJugandoConsola_36 with dissolve
            hermanPeq "¡Sal, que pesas mucho!"
            scene protaPeqJugandoConsola_37 with dissolve
            prota "¡Di que soy el mejor y el más fuerte!"
            scene protaPeqJugandoConsola_35 with dissolve
            hermanPeq "No..."
            scene protaPeqJugandoConsola_39 with dissolve
            pause
            scene protaPeqJugandoConsola_40 with dissolve
            prota "Entonces te ahogare con el cojín."
            scene protaPeqJugandoConsola_41 with dissolve
            pause
            scene protaPeqJugandoConsola_42 with dissolve
            pause
            play sound "audio/effects/golpeMesa2.ogg"
            scene protaPeqJugandoConsola_43 with dissolve
            pause
            play sound "audio/effects/sorpresa3.ogg"
            scene protaPeqJugandoConsola_44 with dissolve
            prota "¡Toma!"
            scene protaPeqJugandoConsola_45 with dissolve
            hermanPeq "Vale, vale."
            hermanPeq "Eres el mejor..."
            scene protaPeqJugandoConsola_46 with dissolve
            prota "¿Y qué más?"
            scene protaPeqJugandoConsola_47 with dissolve
            hermanPeq "Y el más fuerte..."
            scene protaPeqJugandoConsola_48 with dissolve
            prota "Hahahaha te ganado."
            stop music fadeout 3.0
            scene protaPeqJugandoConsola_49 with dissolve
            hermanPeq "Me dejado ganar, para que luego no te sientas mal hahahahaha"
            scene protaPeqJugandoConsola_50 with dissolve
            prota "¿Entonces habrá revancha?"

            if eventPeqComplejoEspejo == True:
                "FIN DANA VERSION 0.5"

        

    call nextTiempoDay(0)

    return



label peqJugarConsolaPerdido():
    scene protaPeqJugandoConsola_4 with dissolve
    hermanPeq "¡Hahaha que malo eres!"
    scene protaPeqJugandoConsola_5 with dissolve
    hermanPeq "Maldita sea..."

    return

label peqJugarConsolaVictoria():
    $ victoriaConsolaPeq = victoriaConsolaPeq +1
    if victoriaConsolaPeq == 1:
        scene protaPeqJugandoConsola_6 with dissolve
        prota "¡Soy el mejor!"
        scene protaPeqJugandoConsola_7 with dissolve
        prota "¡Te dado una paliza!"
        scene protaPeqJugandoConsola_8 with dissolve
        prota "Has tenido suerte..."
        return
    elif victoriaConsolaPeq == 2:
        scene protaPeqJugandoConsola_6 with dissolve
        prota "¡Soy el mejor!"
        scene protaPeqJugandoConsola_7 with dissolve
        prota "¡Te vuelto a ganar y de paliza otra vez!"
        scene protaPeqJugandoConsola_9 with dissolve
        pause
        return
