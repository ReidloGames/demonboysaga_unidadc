label peqGaleriaHaciendoGym3raInteracLunes_visit1():
    $ peqGaleriaHaciendoGym3raInteracLunes_visit1 = True
    $ escondeFrameAyudasPeq = True
    pause
    scene peqHaceGymGaleria3raLunes_1 with dissolve
    pause
    scene peqHaceGymGaleria3raLunes_2 with dissolve
    pause
    scene peqHaceGymGaleria3raLunes_3 with dissolve
    pause
    scene peqHaceGymGaleria3raLunes_4 with dissolve
    protaPensa "{size=15}{i}Es una suerte que en esta casa todos hagan tanto ejercicio.{/size}{/i}"
    scene peqHaceGymGaleria3raLunes_5 with dissolve
    prota"¡¡Hola!!"
    scene peqHaceGymGaleria3raLunes_6 with dissolve
    hermanPeq "¡Hey!"
    scene peqHaceGymGaleria3raLunes_7 with dissolve
    prota"¿Trabajando el físico?"
    scene peqHaceGymGaleria3raLunes_8 with dissolve
    pause
    scene peqHaceGymGaleria3raLunes_9 with dissolve
    pause
    scene peqHaceGymGaleria3raLunes_10 with dissolve
    hermanPeq "Si, no como tu..."
    scene peqHaceGymGaleria3raLunes_11 with dissolve
    pause
    scene peqHaceGymGaleria3raLunes_12 with dissolve
    pause
    scene peqHaceGymGaleria3raLunes_5 with dissolve
    pause
    scene peqHaceGymGaleria3raLunes_13 with dissolve
    pause
    scene peqHaceGymGaleria3raLunes_14 with dissolve
    prota "Espera haces mal el ejercicio tienes que levantar más la pierna."
    scene peqHaceGymGaleria3raLunes_15 with dissolve
    prota "Te ayudo."
    scene peqHaceGymGaleria3raLunes_16 with dissolve
    hermanPeq "No es necesario"
    scene peqHaceGymGaleria3raLunes_14 with dissolve
    prota "Solo quiero ayudarte."
    stop music fadeout 3.0
    play music "audio/music/hiddenAgenda_by_kevin_macleod.ogg" fadein 3.0
    scene peqHaceGymGaleria3raLunes_10 with dissolve
    hermanPeq "Bueno, esta bien."
    scene peqHaceGymGaleria3raLunes_17 with dissolve
    pause
    scene peqHaceGymGaleria3raLunes_18 with dissolve
    pause
    scene peqHaceGymGaleria3raLunes_19 with dissolve
    pause
    scene peqHaceGymGaleria3raLunes_20 with dissolve
    pause
    scene peqHaceGymGaleria3raLunes_21 with dissolve

    menu:
        "Subir mas":
            scene peqHaceGymGaleria3raLunes_24 with dissolve
            pause
            scene peqHaceGymGaleria3raLunes_22 with dissolve
            pause
            scene peqHaceGymGaleria3raLunes_23 with dissolve
            pause

            scene peqHaceGymGaleria3raLunes_25 with dissolve
            pause
            scene peqHaceGymGaleria3raLunes_24 with dissolve
            pause

            menu:
                "Seguir":
                    call seguirPierna1() from _call_seguirPierna1
                    return
                "Dejar pierna":
                    call dejarPierna() from _call_dejarPierna
                    return


    return

label seguirPierna1():
    scene peqHaceGymGaleria3raLunes_24_ with dissolve
    pause
    scene peqHaceGymGaleria3raLunes_26 with dissolve
    pause
    play sound "audio/effects/dolor1.ogg"
    scene peqHaceGymGaleria3raLunes_27 with dissolve
    hermanPeq "¡¡Hay, que duele!!"
    scene peqHaceGymGaleria3raLunes_28 with dissolve
    prota "El dolor es parte del entrenamiento."
    scene peqHaceGymGaleria3raLunes_29 with dissolve
    pause
    scene peqHaceGymGaleria3raLunes_24_ with dissolve
    pause
    menu:
        "Seguir":
            call seguirPierna2() from _call_seguirPierna2
            return
        "Dejar pierna":
            call dejarPierna() from _call_dejarPierna_1
            return

    return

label seguirPierna2():
    scene peqHaceGymGaleria3raLunes_30 with dissolve
    pause
    scene peqHaceGymGaleria3raLunes_31 with dissolve
    play sound "audio/effects/dolor1.ogg"
    hermanPeq "¡No tan fuerte bestia! ¡No puedo más!"
    scene peqHaceGymGaleria3raLunes_32 with dissolve
    hermanPeq "¡Ay que me desmontas!"
    scene peqHaceGymGaleria3raLunes_50 with dissolve
    pause 0.2
    scene peqHaceGymGaleria3raLunes_51 with dissolve
    pause 0.2
    scene peqHaceGymGaleria3raLunes_52 with dissolve
    pause 0.2
    play sound "audio/effects/resbala.ogg"
    scene peqHaceGymGaleria3raLunes_53 with dissolve
    pause 0.2
    scene peqHaceGymGaleria3raLunes_54 with dissolve
    pause 0.2
    #scene peqHaceGymGaleria3raLunes_55 with dissolve
    #pause 0.2
    scene peqHaceGymGaleria3raLunes_56 with dissolve
    pause 0.2

    play sound "audio/effects/fiuu.ogg"
    #scene peqHaceGymGaleria3raLunes_59 with dissolve
    #pause 0.1
    #scene peqHaceGymGaleria3raLunes_60 with dissolve
    #pause 0.2
    scene peqHaceGymGaleria3raLunes_61 with dissolve
    pause 0.2
    #scene peqHaceGymGaleria3raLunes_62 with dissolve
    #pause 0.2
    #scene peqHaceGymGaleria3raLunes_63 with dissolve
    #pause 0.1
    scene peqHaceGymGaleria3raLunes_64 with dissolve
    pause 0.2

    #scene peqHaceGymGaleria3raLunes_65 with dissolve
    #pause 0.1
    scene peqHaceGymGaleria3raLunes_66 with dissolve
    pause 0.2

    #scene peqHaceGymGaleria3raLunes_67 with dissolve
    #pause 0.1
    #scene peqHaceGymGaleria3raLunes_68 with dissolve
    #pause 0.2
    scene peqHaceGymGaleria3raLunes_69 with dissolve
    pause 0.2
    stop sound fadeout 3.0
    #scene peqHaceGymGaleria3raLunes_70 with dissolve
    #pause 0.2
    #scene peqHaceGymGaleria3raLunes_71 with dissolve
    #pause 0.2
    #scene peqHaceGymGaleria3raLunes_72 with dissolve
    #pause 0.2
    play sound "audio/effects/patapam2.ogg"
    scene peqHaceGymGaleria3raLunes_73 with dissolve
    pause 0.1


    stop sound fadeout 3.0
    #scene peqHaceGymGaleria3raLunes_74 with dissolve
    #pause 0.2
    #scene peqHaceGymGaleria3raLunes_75 with dissolve
    #pause 0.2
    #scene peqHaceGymGaleria3raLunes_76 with dissolve
    #pause 0.2
    #stop sound

    scene peqHaceGymGaleria3raLunes_77 with dissolve
    play sound "audio/effects/boing.ogg"
    pause
    #scene peqHaceGymGaleria3raLunes_78 with dissolve
    #pause 0.2
    scene peqHaceGymGaleria3raLunes_81 with dissolve
    pause
    scene peqHaceGymGaleria3raLunes_79 with dissolve
    pause
    scene peqHaceGymGaleria3raLunes_80 with dissolve
    pause

    scene peqHaceGymGaleria3raLunes_82 with dissolve
    pause
    scene peqHaceGymGaleria3raLunes_83 with dissolve
    pause
    scene peqHaceGymGaleria3raLunes_84 with dissolve
    pause
    scene peqHaceGymGaleria3raLunes_85 with dissolve
    pause
    scene peqHaceGymGaleria3raLunes_86 with dissolve
    hermanPeq "!!!Eres un bruto!!! no podía aguantar más."
    scene peqHaceGymGaleria3raLunes_87 with dissolve
    pause
    scene peqHaceGymGaleria3raLunes_88 with dissolve
    pause
    prota "Perdona, solo quería ayudar. ¿Estas bien?"
    scene peqHaceGymGaleria3raLunes_89 with dissolve
    pause
    scene peqHaceGymGaleria3raLunes_90 with dissolve
    hermanPeqPensa "{size=15}{i}Eso es... ¿tan grande? {/size}{/i}"
    scene peqHaceGymGaleria3raLunes_91 with dissolve
    hermanPeqPensa "{size=15}{i}No puede ser. No es posible. {/size}{/i}"
    scene peqHaceGymGaleria3raLunes_92 with dissolve
    pause
    hermanPeq "jajaja"
    scene peqHaceGymGaleria3raLunes_93 with dissolve
    hermanPeq "Eres un patoso y un pervertido."
    hermanPeq "¿Que clase de ejercicio es este?"
    scene peqHaceGymGaleria3raLunes_94 with dissolve
    prota "¿La carretilla?"
    scene peqHaceGymGaleria3raLunes_95 with dissolve
    hermanPeq "¿Pero has visto cómo estamos?"
    scene peqHaceGymGaleria3raLunes_96 with dissolve
    pause
    scene peqHaceGymGaleria3raLunes_97 with dissolve
    pause
    scene peqHaceGymGaleria3raLunes_98 with dissolve
    pause
    scene peqHaceGymGaleria3raLunes_99 with dissolve
    pause
    scene peqHaceGymGaleria3raLunes_100 with dissolve
    pause
    scene peqHaceGymGaleria3raLunes_101 with dissolve
    pause
    scene peqHaceGymGaleria3raLunes_102 with dissolve
    hermanPeqPensa "{size=15}{i}Oh Dios... no te muevas tanto. {/size}{/i}"
    scene peqHaceGymGaleria3raLunes_103 with dissolve
    hermanPeq "Venga sal y deja de hacer el tonto que esta pose es un poco rara."
    scene peqHaceGymGaleria3raLunes_104 with dissolve
    prota "¿Seguro? jajaja"
    play sound "audio/effects/bofetada.ogg"
    scene peqHaceGymGaleria3raLunes_105 with dissolve
    hermanPeq "¡¡¡Splash!!!"
    stop music fadeout 3.0
    hermanPeq "Te la has ganado"
    scene peqHaceGymGaleria3raLunes_106 with dissolve
    pause
    scene peqHaceGymGaleria3raLunes_107 with dissolve
    prota "Perdona jajaja igual sí que estoy bajo de forma y tenga que entrenar como tú."
    scene peqHaceGymGaleria3raLunes_108 with dissolve
    prota "Te dejo entrenar."
    scene peqHaceGymGaleria3raLunes_109 with dissolve
    hermanPeq "¡¡Adios tonto!!"
    scene peqHaceGymGaleria3raLunes_110 with dissolve
    hermanPeqPensa "{size=15}{i}Que me ha pasado...{/size}{/i}"
    scene peqHaceGymGaleria3raLunes_111 with dissolve
    pause
    scene peqHaceGymGaleria3raLunes_112 with dissolve
    prota "¿Que ha sido eso? Buff... mi amiguito de abajo quería saludar."
    scene peqHaceGymGaleria3raLunes_113 with dissolve
    pause

    return



label dejarPierna():
    scene peqHaceGymGaleria3raLunes_33 with dissolve
    pause
    scene peqHaceGymGaleria3raLunes_34 with dissolve
    pause
    play sound "audio/effects/boing.ogg"
    scene peqHaceGymGaleria3raLunes_35 with dissolve
    pause
    scene peqHaceGymGaleria3raLunes_36 with dissolve
    pause
    scene peqHaceGymGaleria3raLunes_37 with dissolve
    pause
    scene peqHaceGymGaleria3raLunes_38 with dissolve
    hermanPeqPensa "{size=15}{i}Eso es... ¿Tan grande?{/size}{/i}"
    scene peqHaceGymGaleria3raLunes_39 with dissolve
    hermanPeqPensa "{size=15}{i}No puede ser. No es posible.{/size}{/i}"
    scene peqHaceGymGaleria3raLunes_40 with dissolve
    pause
    scene peqHaceGymGaleria3raLunes_41 with dissolve
    pause
    scene peqHaceGymGaleria3raLunes_42 with dissolve
    prota "¿Todo bien?"
    scene peqHaceGymGaleria3raLunes_43 with dissolve
    pause
    stop music fadeout 3.0
    scene peqHaceGymGaleria3raLunes_44 with dissolve
    pause
    scene peqHaceGymGaleria3raLunes_45 with dissolve
    pause
    scene peqHaceGymGaleria3raLunes_46 with dissolve
    pause
    scene peqHaceGymGaleria3raLunes_47 with dissolve
    hermanPeq "Si claro."
    scene peqHaceGymGaleria3raLunes_48 with dissolve
    hermanPeq "Gracias"
    scene peqHaceGymGaleria3raLunes_48 with dissolve
    hermanPeq "¡¡Adios tonto!!"
    scene peqHaceGymGaleria3raLunes_49 with dissolve
    pause

    return
