label madHaceGym1raInteracMiercoles():
    call esconderHud() from _call_esconderHud_8
    hide screen galeriaGymYogaAmanecerMiercolesMad
    scene madHaceGymGaleria1raMiercoles_0
    call primeraVezMad() from _call_primeraVezMad_3

    if conversMadGaleriaHaciendoGym1raMiercoles_Hablado == False:

        if madGaleriaHaciendoGym1raInteracMiercoles_visit1 == False:
            $ conversMadGaleriaHaciendoGym1raMiercoles_Hablado = True
            call MadGaleriaHaciendoGym1raInteracMiercoles_visit1() from _call_MadGaleriaHaciendoGym1raInteracMiercoles_visit1
        elif eventMadHaceYoga == True:
            call hablaConMadYoga()

        else:
            protaPensa "No tengo nada mas que decirle"
            scene galeriaGym
    else:
        protaPensa "Ya he hablado con ella"
        scene galeriaGym

    call mostrarHud() from _call_mostrarHud_6
    #show screen galeriaGymCoordenadas

    return
