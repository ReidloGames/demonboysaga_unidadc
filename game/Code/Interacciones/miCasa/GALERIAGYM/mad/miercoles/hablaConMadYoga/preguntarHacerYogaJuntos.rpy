label preguntarHacerYogaJuntos():
    stop music fadeout 2.0
    $ preguntaHacerYogaJuntos = True
    scene madGimHablandoYoga_1 with dissolve
    prota "Estaba pensando lo que me dijiste el otro día."
    scene madGimHablandoYoga_2 with dissolve
    mad "¿Qué te dije?"
    scene madGimHablandoYoga_3 with dissolve
    play music "audio/music/holiday-weasel-by-kevin-macleod.ogg" fadein 2.0
    prota "Que podría hacer un poco de ejercicio..."
    scene madGimHablandoYoga_4 with dissolve
    mad "Ahhh si, deberías empezar a cuidarte. Siempre estás en la consola o mirando la tele."
    scene madGimHablandoYoga_5 with dissolve
    prota "¿Y porque no hacemos un poco de yoga juntos? Leí que hay ejercicios de yoga en pareja y podría ser más divertido."
    scene madGimHablandoYoga_6 with dissolve
    prota "Hacer ejercicio solo es muy aburrido..."
    scene madGimHablandoYoga_7 with dissolve
    mad "Oh... Entiendo que se necesita constancia y puede ser aburrido, pero eso no es posible, ya que no conozco una rutina de ejercicios para dos."
    scene madGimHablandoYoga_8 with dissolve
    mad "Lo siento..."
    scene madGimHablandoYoga_9 with dissolve
    prota "Vaya..."

    call nextTiempoDay(0)

    return
