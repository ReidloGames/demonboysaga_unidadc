label MadGaleriaHaciendoGym1raInteracMiercoles_visit1():
    $ madGaleriaHaciendoGym1raInteracMiercoles_visit1 = True
    pause
    scene madHaceGymGaleria1raMiercoles_1 with dissolve
    protaPensa "{size=15}{i}Oh diooos!!{/size}{/i}"
    protaPensa "{size=15}{i}Hay que reconocer que mi [situMad2] tiene un cuerpazo.{/size}{/i}"
    protaPensa "{size=15}{i}Ese culo respingon y ese pecho firme...{/size}{/i}"
    stop music fadeout 3.0
    prota "Hola [situMad2]"
    scene madHaceGymGaleria1raMiercoles_2 with dissolve
    mad "Hola"
    prota "A ti no te hace falta hacer ejercicio. Estas muy bien"
    scene madHaceGymGaleria1raMiercoles_3 with dissolve
    mad "Gracias [nombreProta2], pero si no me cuidara un poco seguro que aumentaría de peso y me vería mal. Hay que tener un poco de fuerza de voluntad"
    scene madHaceGymGaleria1raMiercoles_4 with dissolve
    mad "Tu tambien deberías cuidarte ya que haces poco deporte y estas mucho con el ordenador"
    scene madHaceGymGaleria1raMiercoles_5 with dissolve
    pause
    play music "audio/music/Scheming_Weasel_faster.ogg" fadein 5.0

    scene madHaceGymGaleria1raMiercoles_6 with dissolve
    pause
    scene madHaceGymGaleria1raMiercoles_7 with dissolve
    prota "Bueno igual si debería hacer un poco de gimnasia."
    scene madHaceGymGaleria1raMiercoles_8 with dissolve
    pause
    scene madHaceGymGaleria1raMiercoles_8 with dissolve
    pause(0.3)
    scene madHaceGymGaleria1raMiercoles_9 with dissolve
    prota "Pero lo que digo es verdad. Tienes un cuerpo espectacular. Seguro que cuando modelabas te salían muchos pretendientes y te los tenías que sacar de encima"
    scene madHaceGymGaleria1raMiercoles_10 with dissolve
    pause(0.4)
    scene madHaceGymGaleria1raMiercoles_11 with dissolve
    mad "Ehh.. no tampoco tanto.."
    scene madHaceGymGaleria1raMiercoles_12 with dissolve
    mad "Es muy amable por tu parte, pero no creo que esa sea una conversación muy apropiada para hablar contigo."
    scene madHaceGymGaleria1raMiercoles_14 with dissolve
    mad "A parte de que esa época ya paso"
    scene madHaceGymGaleria1raMiercoles_9 with dissolve
    pause(0.3)
    scene madHaceGymGaleria1raMiercoles_15 with dissolve
    prota "Pero porque dejaste tu trabajo? nunca me lo dijiste. Y tampoco pasa nada por decir que mi [situMad] es muy atractiva"
    scene madHaceGymGaleria1raMiercoles_16 with dissolve
    mad "Esta bien, esta bien.Me gustaba mi trabajo pero la vida cambia y aparecen nuevas obligaciones como cuidar de la casa y todo lo que conlleva. Y ahora podemos cambiar de tema?"
    scene madHaceGymGaleria1raMiercoles_17 with dissolve
    madPensa "{size=15}{i}Hoy esta rarísimo...{/i}"
    scene madHaceGymGaleria1raMiercoles_18 with dissolve
    protaPensa "{size=15}{i}Nunca le gusta hablar de su pasado{/size}{/i}"
    scene madHaceGymGaleria1raMiercoles_19 with dissolve
    prota "Algun dia me enseñaras algun trabajo de cuando modelabas? Solo es curiosidad, para saber que hacías antes."
    scene madHaceGymGaleria1raMiercoles_20 with dissolve
    pause(0.4)
    scene madHaceGymGaleria1raMiercoles_21 with dissolve
    mad "Si.. bueno, supongo que los guarde en algún lugar. Solo son fotografías."
    scene madHaceGymGaleria1raMiercoles_22 with dissolve
    mad "Dejemos el tema, ahora quiero terminar la sesión de yoga"
    scene madHaceGymGaleria1raMiercoles_23 with dissolve
    prota "Esta bien nos vemos [situMad2]"
    stop music fadeout 3.0
    scene madHaceGymGaleria1raMiercoles_24 with dissolve
    protaPensa "{size=15}{i}Es inutil, no suelta prenda...Pero supongo que al fin podre ver alguna imagen de ella ejerciendo de modelo{/size}{/i}"
    call nextTiempoDay(0) from _call_nextTiempoDay_14
    #call nextZona("pasillo1") from _call_nextZona_14
    return
