label louieTrabajo():
    menu:
        "Trabajar":
            call trabajarConAmarzong from _call_trabajarConAmarzong
        "Salir":
            return

    return




label trabajarConAmarzong():
    play music "audio/music/local-forecast_slower-by-kevin-macleod.ogg" fadein 2.0
    if hasTrabajadoHoy == False:
        $ tienesTrabajoAmarzong = True

        $ hasTrabajadoHoy = True
        scene trabajoBici_0
        pause
        scene trabajoBici_1 with dissolve
        pause
        scene trabajoBici_2 with dissolve
        pause
        scene trabajoBici_3 with dissolve
        pause
        play sound "audio/effects/money.ogg"
        $ player_gold = player_gold +10
        call nextTiempoDay(0) from _call_nextTiempoDay_23
    else:
        scene amarzongEntradaTrabajo_7
        protaPensa "Por hoy ya he trabajado suficiente."
    return
