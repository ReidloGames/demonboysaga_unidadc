label conoceLouieTrabajoInterac():
    if conocesLouie == True:
        if conocesCarla == False:
            scene amarzongEntradaTrabajo_7
            protaPensa "Tengo que hablar con Carla"
        else:
            if tengoBicicleta == False:
                scene amarzongEntradaTrabajo_1
                louie "Para empezar a trabajar necesitas un transporte"
                scene amarzongEntradaTrabajo_2 with dissolve
                pause
            else:
                scene amarzongEntradaTrabajo_1
                call louieTrabajo from _call_louieTrabajo

    else:
        call esconderHud() from _call_esconderHud_53
        $ conocesLouie = True
        $ abrirPuertaAmarzong = True
        scene amarzongEntradaTrabajo_0
        prota "Hola, soy [nombreProta2]."
        scene amarzongEntradaTrabajo_1 with dissolve
        prota "Envie una solicitud para trabajar con vosotros."
        scene amarzongEntradaTrabajo_2 with dissolve
        louie "Hola [nombreProta2]. Te estabamos esperando,me llamo Louie."
        scene amarzongEntradaTrabajo_3 with dissolve
        louie "Ves esa puerta a mi izquierda? Allí esta mi mujer, se llama Carla."
        scene amarzongEntradaTrabajo_4 with dissolve
        louie "Ella te lo explicara todo para que puedas empezar a trabajar con nosotros."
        scene amarzongEntradaTrabajo_5 with dissolve
        louie "Espero que te gusten las condiciones del trabajo y te sientas muy cómodo."
        scene amarzongEntradaTrabajo_6 with dissolve
        pause
        scene amarzongEntradaTrabajo_7 with dissolve
        prota "Muchas gracias."
        call mostrarHud() from _call_mostrarHud_46

    return
