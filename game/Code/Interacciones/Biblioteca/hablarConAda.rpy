label hablarConAda():
    if conocesAdaBiblioteca == False:
        scene protaBiblioHablaConAda_00
        protaPensa "Ella se llama Ada. Casi todos los chicos de la Uni vienen a esta biblioteca para verle las tetas."
    else:
        scene protaBiblioHablaConAda_0


    menu:
        "Preguntar por libro de yoga" if preguntaLibroYogaBiblioteca == False:
            call preguntaLibroYogaBiblioteca()
        "Nada":
            return




    return




label preguntaLibroYogaBiblioteca():
    $ conocesAdaBiblioteca = True
    $ preguntaLibroYogaBiblioteca = True
    call esconderHud()
    scene protaBiblioHablaConAda_0 with dissolve
    pause
    scene protaBiblioHablaConAda_1 with dissolve
    protaPensa "Hola, estoy buscando un libro. ¿me puedes ayudar?"
    scene protaBiblioHablaConAda_2 with dissolve
    protaPensa "Que tetas tiene. Son incluso más grandes que las de [nombreMad3]."
    scene protaBiblioHablaConAda_3 with dissolve
    ada "Hola, por supuesto. ¿Qué clase de libro estas buscando?"
    scene protaBiblioHablaConAda_4 with dissolve
    prota "Estoy buscando un libro de yoga con ejercicios, para hacer en pareja."
    scene protaBiblioHablaConAda_5 with dissolve
    ada "Un chico deportista..."
    scene protaBiblioHablaConAda_6 with dissolve
    ada "Sin problemas, tenemos varios al final del pasillo en la última estantería."
    scene protaBiblioHablaConAda_7 with dissolve
    ada "A mi también me encanta el yoga y el ejercicio en general."
    scene protaBiblioHablaConAda_8 with dissolve
    pause
    scene protaBiblioHablaConAda_9 with dissolve
    ada "Si no fuera por el ejercicio estaría como una ballena ya que tengo que estar todo el día sentada en la silla. Pero adoro los libros y las novelas..."
    scene protaBiblioHablaConAda_10 with dissolve
    ada "Oh, perdona. Te estoy contando mi vida."
    scene protaBiblioHablaConAda_11 with dissolve
    prota "Oh no te preocupes, eres muy amable."
    scene protaBiblioHablaConAda_12 with dissolve
    ada "Si necesitas cualquier libro, avísame y te ayudare encantada."
    scene protaBiblioHablaConAda_13 with dissolve
    prota "Muchas gracias."
    scene protaBiblioHablaConAda_14 with dissolve
    protaPensa "No me canso de ver esas tetas con esa camisa tan apretada y a punto de explotar."
    scene protaBiblioHablaConAda_15 with dissolve
    protaPensa "A buscar el libro..."
    scene protaBiblioHablaConAda_16 with dissolve
    protaPensa "Aquí están..."
    scene protaBiblioHablaConAda_17 with dissolve
    protaPensa "Ejercicios de yoga nivel principiante para hacer en grupo."
    protaPensa "Aquí hay otro..."
    scene protaBiblioHablaConAda_18 with dissolve
    protaPensa "Sube la libido con estos ejercicios de yoga. Ideal para hacer con tu pareja."
    protaPensa "Este sería perfecto, pero [nombreMad3] lo rechazaría y yo quedaría como un deprabado."
    scene protaBiblioHablaConAda_19 with dissolve
    protaPensa "A no ser..."
    protaPensa "Me llevo los dos e intercambio las portadas. Y así será un libro de ejercicios normal..."
    scene protaBiblioHablaConAda_20 with dissolve
    protaPensa "¡Fantástico!"
    protaPensa "Ya tengo el libro adecuado."



    call mostrarHud()

    return
