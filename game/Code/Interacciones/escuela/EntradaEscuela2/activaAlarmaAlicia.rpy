label activaAlarmaAlicia():
    stop music fadeout 3.0
    call esconderHud() from _call_esconderHud_60
    $ puedesActivarAlarmaColegio = False
    $ numActivaAlarmaUniversidadAlicia = numActivaAlarmaUniversidadAlicia +1
    scene protaActivaAlarmaParaMovilAlicia_0
    protaPensa "Si activo la alarma la gente tendrá que salir y con un poco de suerte el móvil de Alicia estará en la mesa"
    scene protaActivaAlarmaParaMovilAlicia_1 with dissolve
    play sound "audio/effects/alarmFuego.ogg"
    pause
    scene protaActivaAlarmaParaMovilAlicia_2 with dissolve
    pause
    scene protaActivaAlarmaParaMovilAlicia_3 with dissolve
    pause
    scene protaActivaAlarmaParaMovilAlicia_4 with dissolve
    pause
    scene protaActivaAlarmaParaMovilAlicia_5 with dissolve
    play music "audio/music/loopster-by-kevin-macleod.ogg" fadein 2.0

    protaPensa "El plan está saliendo a las mil maravillas..."
    scene protaActivaAlarmaParaMovilAlicia_6 with dissolve
    protaPensa "Es un plan perfecto sin fisuras..."
    scene protaActivaAlarmaParaMovilAlicia_7 with dissolve
    pause
    play sound "audio/effects/alarmFuego.ogg"
    scene protaActivaAlarmaParaMovilAlicia_8 with dissolve
    pause
    if numActivaAlarmaUniversidadAlicia == 1:
        scene protaActivaAlarmaParaMovilAlicia_9 with dissolve
        jack "¡¡Tu, tonto!!"
        scene protaActivaAlarmaParaMovilAlicia_10 with dissolve
        jack "¿A caso te quieres quemar como un pollo?"
        scene protaActivaAlarmaParaMovilAlicia_11 with dissolve
        protaPensa "Otra vez el retrasado este. Parece que nació y se golpeó la cabeza."
        scene protaActivaAlarmaParaMovilAlicia_12 with dissolve
        jack "¡Sal de aquí!"
    else:
        scene protaActivaAlarmaParaMovilAlicia_13 with dissolve
        pause
        scene protaActivaAlarmaParaMovilAlicia_14 with dissolve
        protaPensa "¡Ahí está el teléfono!"
        scene protaActivaAlarmaParaMovilAlicia_15 with dissolve
        protaPensa "A ver que tenemos aquí..."
        scene protaActivaAlarmaParaMovilAlicia_16 with dissolve
        protaPensa "Maldición está bloqueado! Tengo que averiguar el código de acceso..."
        $ puedesEspiarAliciaCodigoMovil = True

    call nextTiempoDay(0) from _call_nextTiempoDay_33
    call mostrarHud() from _call_mostrarHud_53
    return
