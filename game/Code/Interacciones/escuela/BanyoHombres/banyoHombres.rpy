label banyoHombresInterac():
    if eventMikeSaludoUniversidad == True and irAmearEscuela == False:
        play sound "audio/effects/openWc.ogg"
        call mearEnBanyo() from _call_mearEnBanyo
    else:
        scene banyoHombres
        protaPensa "No tengo ganas de mear"

    return

label mearEnBanyo():
    call esconderHud() from _call_esconderHud_26
    $ irAmearEscuela = True
    scene protaMeandoWcUniversidad_0
    pause
    scene protaMeandoWcUniversidad_1 with dissolve
    pause
    scene protaMeandoWcUniversidad_2 with dissolve
    play sound "audio/effects/orinar.ogg"
    pause
    scene protaMeandoWcUniversidad_3 with dissolve
    pause
    stop sound fadeout 2.0
    scene protaMeandoWcUniversidad_3 with dissolve
    pause
    call mostrarHud() from _call_mostrarHud_24
    return
