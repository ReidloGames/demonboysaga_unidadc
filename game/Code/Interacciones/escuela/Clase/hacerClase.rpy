label hacerClase():

    if numDia >=0 and numDia <=4:
        if tiempoDia == 0:
            scene clase
            protaPensa "Las clases empiezan por la mañana"

        elif tiempoDia == 1:
            call hacerClaseAlicia() from _call_hacerClaseAlicia


        else:
            #scene clase
            protaPensa "Por hoy, se han terminado las clases"

    return



label hacerClaseAlicia():
    call esconderHud() from _call_esconderHud_27
    $ hasDadoClaseAlicia = True
    scene aliciaDandoClaseEntraProtaEvent_16
    play music "audio/music/airport-lounge-by-kevin-macleod.ogg" fadein 2.0
    alicia "Chicos y chicas empieza la clase, ir sacando los libros"
    scene aliciaDandoClaseEntraProtaEvent_19 with dissolve
    alicia "........."
    scene aliciaDandoClaseEntraProtaEvent_20 with dissolve
    alicia "Bien, por hoy hemos terminado la clase."
    call nextTiempoDay(0) from _call_nextTiempoDay_9
    call mostrarHud() from _call_mostrarHud_25
    return
