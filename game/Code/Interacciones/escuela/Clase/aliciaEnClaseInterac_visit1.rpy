label aliciaEnClaseInterac_visit1():
    $ aliciaEnClaseInterac_visit1 = True
    $ numContadorAliciaVisit1 =  numContadorAliciaVisit1 +1
    pause
    scene aliciaEnClaseInterac_visit1_0 with dissolve
    pause
    scene aliciaEnClaseInterac_visit1_1 with dissolve
    pause
    scene aliciaEnClaseInterac_visit1_2 with dissolve
    pause
    scene aliciaEnClaseInterac_visit1_3 with dissolve
    pause
    scene aliciaEnClaseInterac_visit1_4 with dissolve
    pause
    scene aliciaEnClaseInterac_visit1_5 with dissolve
    protaPensa "¿Que estará mirando en el teléfono que se ha puesto tan contenta?"

    if numContadorAliciaVisit1 > 2 and eventClaireDirectorVisita2 == True:
        if puedesEspiarAliciaCodigoMovil == True:
            menu:
                "Intentar obtener el código de acceso":
                    call protaIntentaObtenerCodigoMovilAlicia from _call_protaIntentaObtenerCodigoMovilAlicia

                "Nada":
                    return
        else:
            call protaPiensaCrearDistracionAlicia from _call_protaPiensaCrearDistracionAlicia


    return


label protaPiensaCrearDistracionAlicia():
    scene aliciaEnClaseInterac_visitDistrac_0 with dissolve
    protaPensa "Necesito acceder a ese teléfono"
    scene aliciaEnClaseInterac_visitDistrac_1 with dissolve
    protaPensa "Podría crear una distracción..."
    $ puedesActivarAlarmaColegio = True

    return

label protaIntentaObtenerCodigoMovilAlicia():
    play music "audio/music/Marty_Gots_a_Plan.ogg" fadein 2.0
    scene protaObtenCodigoMovilAlicia_0
    protaPensa "Tengo que obtener el código..."
    scene protaObtenCodigoMovilAlicia_1 with dissolve
    pause
    scene protaObtenCodigoMovilAlicia_2 with dissolve
    pause
    scene protaObtenCodigoMovilAlicia_3 with dissolve
    pause
    $ randomNumObtenCodigo = renpy.random.randint(1, 3)

    #Obtienes el codigo
    if randomNumObtenCodigo == 1 or randomNumObtenCodigo == 2:
        scene protaObtenCodigoMovilAlicia_4 with dissolve
        play sound "audio/effects/winGame.ogg"
        protaPensa "¡Genial! Ya tengo un número..."
        protaPensa "Me voy antes de que sospeche de mi."
        $ numContadorCodigoMovilAlicia = numContadorCodigoMovilAlicia +1
        if numContadorCodigoMovilAlicia >= 4:
            protaPensa "¡Ya tengo el código!"
            $ tengoCodigoPinAlicia = True
    else:
        stop music
        scene protaObtenCodigoMovilAlicia_5 with dissolve
        alicia "Que estás haciendo ¿tonterías como siempre?"
        scene protaObtenCodigoMovilAlicia_6 with dissolve
        prota "Oh, el zapato lo tengo desabrochado..."
        scene protaObtenCodigoMovilAlicia_7 with dissolve
        protaPensa "Me voy antes de que sospeche de mí."


    return
