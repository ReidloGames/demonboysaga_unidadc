label aliciaEnClaseInterac():
    call esconderHud() from _call_esconderHud_58

    scene aliciaEnClaseInterac_visit1_00

    if conversAliciaEnClase_Hablado == False:

        $ conversAliciaEnClase_Hablado = True
        if tengoCodigoPinAlicia == False:
            call aliciaEnClaseInterac_visit1() from _call_aliciaEnClaseInterac_visit1
        else:
            "Fin versión 0.4 Alicia"
            

    else:
        protaPensa "Ya he hablado con ella"

    call mostrarHud() from _call_mostrarHud_51

    return
