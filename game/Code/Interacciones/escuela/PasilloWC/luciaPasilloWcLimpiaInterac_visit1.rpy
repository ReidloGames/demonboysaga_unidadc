label luciaPasilloWcLimpiaInterac_visit1():
    $ conversLuciaPasilloWcLimpia_Hablado = True
    pause
    scene luciaPasilloWcEscuelaLimpiando_1
    pause
    scene luciaPasilloWcEscuelaLimpiando_2 with dissolve
    pause
    scene luciaPasilloWcEscuelaLimpiando_3 with dissolve
    stop music fadeout 3.0
    pause
    scene luciaPasilloWcEscuelaLimpiando_4 with dissolve
    prota "Buenos días Lucia."
    scene luciaPasilloWcEscuelaLimpiando_6 with dissolve
    lucia "Buenos días cielo."
    scene luciaPasilloWcEscuelaLimpiando_7 with dissolve
    lucia "Si ves a mi Mike dale un beso de mi parte."
    scene luciaPasilloWcEscuelaLimpiando_8 with dissolve
    lucia "Nunca lo veo por la escuela."
    scene luciaPasilloWcEscuelaLimpiando_5 with dissolve
    prota "Claro, sin problema."
    scene luciaPasilloWcEscuelaLimpiando_9 with dissolve
    pause

    return
