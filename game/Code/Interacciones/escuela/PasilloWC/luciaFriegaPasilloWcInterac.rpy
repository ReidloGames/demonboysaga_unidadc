label hablaConLuciaAntesDeMensWc:
    scene pasilloWc
    show luciaLimpiezaPasilloWc:
        xpos 574 ypos 311
    protaPensa "Debería tener tiempo para saludar."


label luciaFriegaPasilloWcInterac():
    call esconderHud() from _call_esconderHud_44
    hide screen cocinaCocinandoMediodiaMiercolesPeq
    scene luciaPasilloWcEscuelaLimpiando_0
    call primeraVezLucia() from _call_primeraVezLucia

    if conversLuciaPasilloWcLimpia_Hablado == False:

        $ conversLuciaPasilloWcLimpia_Hablado = True
        call luciaPasilloWcLimpiaInterac_visit1() from _call_luciaPasilloWcLimpiaInterac_visit1

    else:
        if demonBoy == True:
            call luciaFriegaPasilloNadaQueDecirRandom() from _call_luciaFriegaPasilloNadaQueDecirRandom
        protaPensa "Ya he hablado con ella"

    call mostrarHud() from _call_mostrarHud_39

    return

label luciaFriegaPasilloNadaQueDecirRandom():
    $ luciaFriegaPasilloNadaQueDecirRandom = renpy.random.randint(1,2)

    if luciaFriegaPasilloNadaQueDecirRandom == 1:
        scene luciaLimpiaEscuelaYavisto_1_0 with dissolve
        pause
        scene luciaLimpiaEscuelaYavisto_1_1 with dissolve

    if luciaFriegaPasilloNadaQueDecirRandom == 2:
        scene luciaLimpiaEscuelaYavisto_2_0 with dissolve
        pause
        scene luciaLimpiaEscuelaYavisto_2_1 with dissolve


    return
