label pasilloDirectorInteracciones():
    if entrarDespachoDirector == True:
        call nextZona("despachoDirector") from _call_nextZona_4
    elif entrarDespachoDirector == False:
        scene pasilloDirector
        call noIrAlliDespachoDirector() from _call_noIrAlliDespachoDirector
    return

label noIrAlliDespachoDirector():
    protaPensa "No necesito ir allí"
    call pasilloDirector from _call_pasilloDirector
    return
