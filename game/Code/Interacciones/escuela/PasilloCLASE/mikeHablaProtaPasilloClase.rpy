label mikeHablaProtaPasilloClase():
    call esconderHud() from _call_esconderHud_59
    scene maxPasilloEscuelaHablandoProta_0
    prota "¡Ey Mike!"
    scene maxPasilloEscuelaHablandoProta_1 with dissolve
    menu:
        "Hacer tareas de clase juntos":
            call hacerTareasDeCLaseJuntosMike from _call_hacerTareasDeCLaseJuntosMike

    call nextTiempoDay(0) from _call_nextTiempoDay_30
    $ zona = "miMapa"
    call nextZona("miMapa") from _call_nextZona_10

    call mostrarHud() from _call_mostrarHud_52
    #minutosMasTarde
    #fondoNegro
    return


label hacerTareasDeCLaseJuntosMike():
    play music "audio/music/funkorama-by-kevin-macleod.ogg" fadein 2.0
    $ numContadorMikeHacerDeberesJuntos = numContadorMikeHacerDeberesJuntos +1
    prota "Tenemos muchos trabajos de clase. ¿Qué te parece si hacemos juntos las tareas?"
    scene maxPasilloEscuelaHablandoProta_2 with dissolve
    mike "¡Por mi perfecto! al menos será más divertido."
    scene maxPasilloEscuelaHablandoProta_3 with dissolve
    mike "¿Vamos a mi casa? allí estaremos tranquilos y si sobra un poco de tiempo podríamos jugar al Call of tuty."
    scene maxPasilloEscuelaHablandoProta_4 with dissolve
    prota "¡Me parece perfecto!"
    scene maxPasilloEscuelaHablandoProta_5 with dissolve
    pause
    scene minutosMasTarde with dissolve
    "Unos minutos más tarde..."
    scene maxPasilloEscuelaHablandoParaEstudiar_6 with dissolve
    pause

    if numContadorMikeHacerDeberesJuntos == 1:
        call hacerTareasDeCLaseJuntosMikeVez1 from _call_hacerTareasDeCLaseJuntosMikeVez1
    else:
        if numContadorMikeHacerDeberesJuntos == 2:
            call hacerTareasDeCLaseJuntosMikeVez2 from _call_hacerTareasDeCLaseJuntosMikeVez2
        elif numContadorMikeHacerDeberesJuntos == 3:
            call hacerTareasDeCLaseJuntosMikeVez3 from _call_hacerTareasDeCLaseJuntosMikeVez3
        else:
            call hacerTareasDeCLaseJuntosMikeVez1 from _call_hacerTareasDeCLaseJuntosMikeVez1_1


    return

label hacerTareasDeCLaseJuntosMikeVez1():
    scene maxPasilloEscuelaHablandoParaEstudiar_7 with dissolve
    stop music fadeout 2.0
    prota "Esto es muy aburrido..."
    scene maxPasilloEscuelaHablandoParaEstudiar_8 with dissolve
    mike "Un poco más a ver si podemos acabar los trabajos."
    scene maxPasilloEscuelaHablandoParaEstudiar_9 with dissolve
    prota "Que remedio..."
    scene minutosMasTarde with dissolve
    pause
    scene maxPasilloEscuelaHablandoParaEstudiar_10 with dissolve
    prota "Se ha hecho tarde y tengo que irme. ¡Nos vemos y gracias por todo!"
    mike "Una lástima, jugaremos en otra ocasión. ¡Hasta mañana!"

    return

label hacerTareasDeCLaseJuntosMikeVez2():
    scene maxPasilloEscuelaHablandoParaEstudiar_7 with dissolve
    pause
    scene maxPasilloEscuelaHablandoParaEstudiar_11 with dissolve
    prota  "Necesito ir al baño."
    mike "Claro, ya sabes arriba la última puerta."
    scene maxPasilloEscuelaHablandoParaEstudiar_12 with dissolve
    pause
    scene maxPasilloEscuelaHablandoParaEstudiar_13 with dissolve
    pause
    scene maxPasilloEscuelaHablandoParaEstudiar_14 with dissolve
    protaPensa "Esa puerta del fondo es el baño."
    scene maxPasilloEscuelaHablandoParaEstudiar_15 with dissolve
    protaPensa "La primera puerta es la habitación de Lucia. Nunca entre en ella y la del medio es la de Mike."
    scene maxPasilloEscuelaHablandoParaEstudiar_16 with dissolve
    pause
    scene fondoNegro with dissolve
    "Mear..."
    scene maxPasilloEscuelaHablandoParaEstudiar_10 with dissolve
    prota "Se ha hecho tarde y tengo que irme. ¡Nos vemos y gracias por todo!"
    mike "Una lástima, jugaremos en otra ocasión. ¡Hasta mañana!"

    return

label hacerTareasDeCLaseJuntosMikeVez3():
    scene maxPasilloEscuelaHablandoParaEstudiar_7 with dissolve
    pause
    scene maxPasilloEscuelaHablandoParaEstudiar_11 with dissolve
    prota  "Necesito ir al baño."
    scene maxPasilloEscuelaHablandoParaEstudiar_12 with dissolve
    pause
    scene maxPasilloEscuelaHablandoParaEstudiar_13 with dissolve
    pause
    scene maxPasilloEscuelaHablandoParaEstudiar_14 with dissolve
    pause
    scene maxPasilloEscuelaHablandoParaEstudiar_15 with dissolve
    protaPensa "Lucia no está en casa..."
    menu:
        "Entrar en la habitación de Lucia":
            play music "audio/music/Marty_Gots_a_Plan.ogg" fadein 2.0
            scene maxPasilloEscuelaHablandoParaEstudiar_17 with dissolve
            protaPensa "Tengo curiosidad, un vistazo rápido..."
            scene maxPasilloEscuelaHablandoParaEstudiar_18 with dissolve
            protaPensa "Esta es la habitación de Lucia..."
            scene maxPasilloEscuelaHablandoParaEstudiar_19 with dissolve
            protaPensa "Tiene una gran cama, y un gran ventanal."
            scene maxPasilloEscuelaHablandoParaEstudiar_20 with dissolve
            pause
            scene maxPasilloEscuelaHablandoParaEstudiar_21 with dissolve
            protaPensa "¡No me lo puedo creer! Se ve muy lejos, pero desde aquí puedo ver mi casa y la ventana de mi habitación."
            scene maxPasilloEscuelaHablandoParaEstudiar_22 with dissolve
            protaPensa "Se me está ocurriendo una gran idea... Me voy antes de que me vean."
            scene fondoNegro with dissolve
            "..."
            stop music fadeout 2.0
            pause
            scene maxPasilloEscuelaHablandoParaEstudiar_10 with dissolve
            prota "Se ha hecho tarde y tengo que irme. ¡Nos vemos y gracias por todo!"
            mike "Una lástima, jugaremos en otra ocasión. ¡Hasta mañana!"
            $ puedesComprarPrismaticos = True

    return
