label peqPasilloEscuelaSentadaHablaProta():
    stop music fadeout 2.0
    call esconderHud() from _call_esconderHud_62
    $ hasHabladoConPeqEnElColeTrabajosAcabados = True
    scene protaHablaConPeqEscuela_0
    prota "¡Ey Dana!"
    scene protaHablaConPeqEscuela_1 with dissolve
    prota "¿Qué haces ahí?"
    scene protaHablaConPeqEscuela_2 with dissolve
    hermanPeq "Hola [nombreProta2]"
    scene protaHablaConPeqEscuela_3 with dissolve
    hermanPeq "Acabo de entregar unas tareas importantes de clase y me estaba dando un respiro."
    scene protaHablaConPeqEscuela_4 with dissolve
    hermanPeq "Esos trabajos me estaban estresando mucho. Me quitado un peso de encima."
    scene protaHablaConPeqEscuela_5 with dissolve
    prota "Me alegro mucho. Ahora tendras mas tiempo para hacer otras cosas. Siempre estas con trabajos de la universidad..."
    scene protaHablaConPeqEscuela_6 with dissolve
    hermanPeq "¡Si! Voy tirando a clase o se me hará tarde."
    scene protaHablaConPeqEscuela_7 with dissolve
    prota "Nos vemos luego."
    scene protaHablaConPeqEscuela_8 with dissolve
    pause
    #call nextTiempoDay(0) from _call_nextTiempoDay_35
    call mostrarHud() from _call_mostrarHud_55
    return
