label zonaComodinGym():
    $ numHoraActual = tiempoDia
    $ accionOpcion = False
    call entrarEnMenuGym from _call_entrarEnMenuGym
    scene galeriaGym

    menu:
        #"Esperar a [nombreMad3] para hacer gimnasia" if numDia == 5 and tiempoDia == 0:     #SABADO
        #    scene minutosMasTarde
        #    pause
        #    call madHaceGym1raInteracMiercoles from _call_madHaceGym1raInteracMiercoles
        #    call functionTimeComodin from _call_functionTimeComodin
        "Esperar a [nombrePeq] para hacer gimnasia" if numDia == 3 and tiempoDia == 2 and eventDiarioPersonalPeq == False:      #JUEVES
            scene minutosMasTarde
            pause
            call peqHaceGym3raInteracLunes from _call_peqHaceGym3raInteracLunes
            call functionTimeComodin from _call_functionTimeComodin_1
        "Esperar a [nombreMay] para hacer gimnasia" if numDia == 1 and tiempoDia == 0:      #MARTES
            scene minutosMasTarde
            pause
            call mayHaceGym1raInteracViernes from _call_mayHaceGym1raInteracViernes
            call functionTimeComodin from _call_functionTimeComodin_2

        "Salir" if accionOpcion == True:
            return


    return


label entrarEnMenuGym():
    #if numDia == 5 and tiempoDia == 0:          #Mad
    #    $ accionOpcion = True
    if numDia == 3 and tiempoDia == 2 and eventDiarioPersonalPeq == False:        #Peq
        $ accionOpcion = True
    elif numDia == 1 and tiempoDia == 0:        #May
        $ accionOpcion = True
    else:
        $ accionOpcion = False
    return
