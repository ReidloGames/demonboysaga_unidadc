label zonaComodinJardinPis():
    $ numHoraActual = tiempoDia
    $ accionOpcion = False
    call entrarEnMenuJardinPis from _call_entrarEnMenuJardinPis
    scene jardinPisSgD

    menu:
        "Esperar a [nombreMay] a que venga a tomar el sol" if numDia == 6 and tiempoDia == 1:     #DOMINGO
            $ accionOpcion = True
            scene minutosMasTarde
            pause
            #show screen jardinPisTomarSolManyanaMiercolesMay
            call mayTomandoSol2daInteracMiercoles from _call_mayTomandoSol2daInteracMiercoles
            call functionTimeComodin from _call_functionTimeComodin_14

        #"Esperar a [nombreMad3] a que venga a tomar el sol" if numDia == 2 and tiempoDia == 0:     #MIERCOLES
        #    $ accionOpcion = True
        #    scene minutosMasTarde
        #    pause
        #    call madJardinPisTomaSol2daInteracSabado
        #    call functionTimeComodin
        "Esperar a [nombreMed] a que venga a tomar el sol" if numDia == 2 and tiempoDia == 0:     #MIERCOLES
            $ accionOpcion = True
            scene minutosMasTarde
            pause
            #show screen jardinPisTomarSolManyanaSabadoMed
            call medTomaSol2daInteracSabado from _call_medTomaSol2daInteracSabado
            call functionTimeComodin from _call_functionTimeComodin_15

        "Salir" if accionOpcion == True:
            return


    return


label entrarEnMenuJardinPis():
    #if numDia == 6 and tiempoDia == 1:              #May
    #    $ accionOpcion = True
    if numDia == 2 and tiempoDia == 0:            #Mad  Med
        $ accionOpcion = True
    else:
        $ accionOpcion = False
    return
