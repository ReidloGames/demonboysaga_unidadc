label zonaComodinSalon():
    $ numHoraActual = tiempoDia
    $ accionOpcion = False
    call entrarEnMenuSalon from _call_entrarEnMenuSalon
    scene salon


    menu:
        "Esperar a [nombreMad3] a que mire su revista" if numDia == 3 and tiempoDia == 2:     #JUEVES
            $ accionOpcion = True
            scene minutosMasTarde
            pause
            call madSalonLeeRevista4taInteracMartes from _call_madSalonLeeRevista4taInteracMartes
            call functionTimeComodin from _call_functionTimeComodin_3


        "Esperar a [nombreMad3] para mirar Tv" if numDia == 2 and tiempoDia == 2 and haVenidoEthan1 == False:      #MIERCOLES
            $ accionOpcion = True
            scene minutosMasTarde
            pause
            call madMiraTv4taInteracDomingo from _call_madMiraTv4taInteracDomingo
            call functionTimeComodin from _call_functionTimeComodin_4
            $ accionOpcion = True
        "Esperar a [nombrePeq]" if numDia == 6 and tiempoDia == 2 and peqSuHabitacionMirandoEspejo2daInteracSabados_visit1__ == False and eventDiarioPersonalPeq == False:      #DOMINGO
            $ accionOpcion = True
            scene minutosMasTarde
            pause
            call peqEstiradaSofa4taInteracJueves from _call_peqEstiradaSofa4taInteracJueves
            call functionTimeComodin from _call_functionTimeComodin_5
            $ accionOpcion = True
        "Esperar a [nombreMed]" if numDia == 1 and tiempoDia == 2:     #MARTES
            $ accionOpcion = True
            scene minutosMasTarde
            pause
            call medSalonMiraMovil4taInteracSabado from _call_medSalonMiraMovil4taInteracSabado
            call functionTimeComodin from _call_functionTimeComodin_6
            $ accionOpcion = True
        "Esperar a [nombreMay]" if numDia == 5 and tiempoDia == 2:      #SABADO
            $ accionOpcion = True
            scene minutosMasTarde
            pause
            call mayPintaUnyas4taInteracMiercoles from _call_mayPintaUnyas4taInteracMiercoles
            call functionTimeComodin from _call_functionTimeComodin_7
            $ accionOpcion = True


        "Salir" if accionOpcion == True:
            return

    return


label entrarEnMenuSalon():
    if numDia == 3 and tiempoDia == 2:
        $ accionOpcion = True
    elif numDia == 2 and tiempoDia == 2 and haVenidoEthan1 == False:
        $ accionOpcion = True
    elif numDia == 6 and tiempoDia == 2 and peqSuHabitacionMirandoEspejo2daInteracSabados_visit1__ == False and eventDiarioPersonalPeq == False:
        $ accionOpcion = True
    elif numDia == 1 and tiempoDia == 2:
        $ accionOpcion = True
    elif numDia == 5 and tiempoDia == 2:
        $ accionOpcion = True
    else:
        $ accionOpcion = False
    return
