label zonaComodinComedor():
    $ numHoraActual = tiempoDia
    $ accionOpcion = False
    call entrarEnMenuComedor from _call_entrarEnMenuComedor
    scene comedor

    menu:
        "Esperar a [nombreMed] para estudiar master" if numDia == 4 and tiempoDia == 2:      #Viernes
            scene minutosMasTarde
            pause
            call medEstudiando3raInteracMiercoles from _call_medEstudiando3raInteracMiercoles
            call functionTimeComodin from _call_functionTimeComodin_16

        "Salir" if accionOpcion == True:
            return


    return


label entrarEnMenuComedor():
    if numDia == 4 and tiempoDia == 2:      #Med
        $ accionOpcion = True
    else:
        $ accionOpcion = False
    return
