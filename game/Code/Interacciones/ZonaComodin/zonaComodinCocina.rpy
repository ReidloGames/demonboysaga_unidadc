label zonaComodinCocina():
    $ numHoraActual = tiempoDia
    $ accionOpcion = False
    call entrarEnMenuCocina from _call_entrarEnMenuCocina
    scene cocina

    menu:
        #MAD
        "Esperar a [nombreMad3] para hacer la comida" if numDia == 4 and tiempoDia == 2:      #Viernes
            scene minutosMasTarde
            pause
            call madCocina3raInteracLunes() from _call_madCocina3raInteracLunes
            call functionTimeComodin from _call_functionTimeComodin_8
        "Esperar a [nombreMad3] para hacer limpieza de casa" if numDia == 5 and tiempoDia == 2 and haVenidoEthan1 == False:      #Sabado
            scene minutosMasTarde
            pause
            call madCocinaLimpia3raInteracMartes() from _call_madCocinaLimpia3raInteracMartes
            call functionTimeComodin from _call_functionTimeComodin_9

        "Esperar a [nombreMad3] para que de teta a Max" if numDia == 6 and tiempoDia == 2 and haVenidoEthan1 == False:      #Domingo
            scene minutosMasTarde
            pause
            call madCocinaDarMamar3raInteracJueves() from _call_madCocinaDarMamar3raInteracJueves
            call functionTimeComodin from _call_functionTimeComodin_10

        "Esperar a [nombreMad3] para que friege los platos" if numDia == 3 and tiempoDia == 0 and haVenidoEthan1 == False:      #Jueves"
            scene minutosMasTarde
            pause
            call madPideFregar1raInteracDomingo() from _call_madPideFregar1raInteracDomingo
            call functionTimeComodin from _call_functionTimeComodin_11

        #PEQ
        "Esperar a [nombrePeq] para comer juntos" if numDia == 6 and tiempoDia == 1 and peqSuHabitacionMirandoEspejo2daInteracSabados_visit1__ == False and eventDiarioPersonalPeq == False:        #Domingo
            scene minutosMasTarde
            pause
            call peqHaceComida3raInteracMiercoles() from _call_peqHaceComida3raInteracMiercoles
            call functionTimeComodin from _call_functionTimeComodin_12

        #MAY
        "Esperar a que venga [nombreMay]" if numDia == 2 and tiempoDia == 0:        #Miercoles
            scene minutosMasTarde
            pause
            call mayTomaCafe1raInteracSabado() from _call_mayTomaCafe1raInteracSabado
            call functionTimeComodin from _call_functionTimeComodin_13

        "Salir" if accionOpcion == True:
            return

    return


label entrarEnMenuCocina():
    if numDia == 4 and tiempoDia == 2:      #Mad
        $ accionOpcion = True
    elif numDia == 5 and tiempoDia == 2 and haVenidoEthan1 == False:    #Mad
        $ accionOpcion = True
    elif numDia == 6 and tiempoDia == 2 and haVenidoEthan1 == False:    #Mad
        $ accionOpcion = True
    elif numDia == 3 and tiempoDia == 0 and haVenidoEthan1 == False:    #Mad
        $ accionOpcion = True
    elif numDia == 6 and tiempoDia == 1 and peqSuHabitacionMirandoEspejo2daInteracSabados_visit1__ == False and eventDiarioPersonalPeq == False:    #Peq
        $ accionOpcion = True
    elif numDia == 2 and tiempoDia == 0:    #May
        $ accionOpcion = True
    else:
        $ accionOpcion = False
    return
