screen mochilaHud():
    zorder 90
    imagebutton:
        xalign 0.57 ypos 10
        idle "mochila"
        hover "mochila_S"
        action Call ("getMochila")

label getMochila:
    play sound "audio/effects/cremallera.ogg"
    #e "inventario: .[player_inventory.itemlist[0][0].name]"
    call esconderHud() from _call_esconderHud_2
    #show screen getContentMochila(0)
    call paginadorMochila(0) from _call_paginadorMochila
    return

label paginadorMochila(pag):
    hide screen getContentMochila
    hide screen infoObjects

    $ xposi = 0
    $ yposi = 0

    if pag == 0:
        show screen getContentMochila(0)
    if pag == 1:
        show screen getContentMochila(1)
    return



screen getContentMochila(pag):
    zorder 96

    if pag == 0:

        add "mochilaObjects" xpos 112 ypos 10
        text _("{b}Inventario{/b}"):
            xpos 450 ypos 17
            size 25
        $ i = 0
        $ count = 0
        $ xposi = 159
        $ yposi = 90

        for item in player_inventory.itemlist:
            #if count <= 20:
            if count < 21:
                hbox:
                    xpos xposi ypos yposi
                    #spacing 68
                    $ xposi += 100
                    if i == 6:
                        $ i = -1
                        $ yposi += 180
                        $ xposi = 160

                    vbox:
                        spacing 5
                        if lang == "spanish":
                            text "[item.nameEsp]":
                                xalign 0.5
                                size 10
                        if lang == "english":
                            text "[item.nameEng]":
                                xalign 0.5
                                size 10
                        imagemap:
                            #xpos 5 ypos 5
                            idle [item._image]
                            hover [item._imageh]
                            hotspot (0, 0, 70, 70) action Call ("infoObject",item)

                $ i += 1
            $ count += 1

        if count > 21:
            imagebutton:
                    xalign 0.69 ypos 255
                    idle "pagDerecha"
                    hover "pagDerecha"
                    action [Hide ("getContentMochila"),Call ("paginadorMochila",1) ]


    if pag == 1:

        add "mochilaObjects" xpos 112 ypos 10
        text _("{b}Inventario{/b}"):
            xpos 450 ypos 17
            size 25
        $ i = 0
        $ count = 0
        $ xposi = 159
        $ yposi = 90

        for item in player_inventory.itemlist:

            if count >= 21:

                hbox:
                    xpos xposi ypos yposi
                    #spacing 68
                    $ xposi += 100
                    if i == 6:
                        $ i = -1
                        $ yposi += 180
                        $ xposi = 160

                    vbox:
                        spacing 5
                        if lang == "spanish":
                            text "[item.nameEsp]":
                                xalign 0.5
                                size 10
                        if lang == "english":
                            text "[item.nameEng]":
                                xalign 0.5
                                size 10
                        imagemap:
                            #xpos 5 ypos 5
                            idle [item._image]
                            hover [item._imageh]
                            hotspot (0, 0, 70, 70) action Call ("infoObject",item)

                $ i += 1
            $ count += 1
        #if count > 20:
        #if count > 3:
        #    imagebutton:
        #            xalign 0.69 ypos 255
        #            idle "pagDerecha"
        #            hover "pagDerecha"
        #            action [Hide ("getContentMochila"),Show ("paginadorMochila",1) ]


        imagebutton:
            xalign 0.09 ypos 255
            idle "pagIzquierda"
            hover "pagIzquierda"
            action [Hide ("getContentMochila"),Call ("paginadorMochila",0) ]



    imagebutton:
            xalign 0.67 ypos 18
            idle "flechaRetmin"
            hover "flechaRetminh"
            action [Hide ("getContentMochila"),Call ("mostrarHud") ]

return

label infoObject(obj):
    show screen infoObjects(obj)

screen infoObjects(obj):
    zorder 96
    frame:
        xpos 910 ypos 14
        padding (10,10,10,10)
        hbox:
            if lang == "spanish":
                text _("[obj.desc]"):
                    color "#000000"
                    size 14
                timer 3 action Hide('infoObjects')
            if lang == "english":
                text _("[obj.descEng]"):
                    color "#000000"
                    size 14
                timer 3 action Hide('infoObjects')
return
