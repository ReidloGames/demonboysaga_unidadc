label reinicioVarsSoloTiempo():
    call reinicioAlarmaColegioAlicia from _call_reinicioAlarmaColegioAlicia
    call reinicioHasMiradoPorPrismaticosLucia

    return

#Alicia
label reinicioAlarmaColegioAlicia():
    $ puedesActivarAlarmaColegio = False
    return

#Prismaticos Lucia
label reinicioHasMiradoPorPrismaticosLucia():
    $ hasMiradoPorPrismaticosLucia = False
    return
