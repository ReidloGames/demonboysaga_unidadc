
label nextTiempoDay(tiempoHasta):

    if tiempoHasta == 0:
        $ tiempoDia += 1
    elif tiempoHasta == 1:
        $ tiempoDia += 2
    elif tiempoHasta == 2:
        $ tiempoDia += 3
    elif tiempoHasta == 3:
        $ tiempoDia += 4
    elif tiempoHasta == 4:
        $ tiempoDia += 5

    if tiempoDia > 4:
        $ tiempoDia = 0
        call nextChangeDay from _call_nextChangeDay
    show screen diaImageHud()
    call eventosZonaTiempoDia from _call_eventosZonaTiempoDia_1
    call reinicioVarsSoloTiempo from _call_reinicioVarsSoloTiempo

    return

label nextChangeDay:
    $ numDiaTotal += 1
    $ numDia += 1

    call activarAyudas() from _call_activarAyudas
    call reinicioVarsEntrarBanyo() from _call_reinicioVarsEntrarBanyo
    call reinicioPlatosFregados() from _call_reinicioPlatosFregados
    call reinicioHasTrabajadoHoy() from _call_reinicioHasTrabajadoHoy
    call reinicioVarsEntroCambiandose() from _call_reinicioVarsEntroCambiandose
    call reinicioVarsHasDadoClaseAlicia() from _call_reinicioVarsHasDadoClaseAlicia
    call reinicioVarsEntroNoche

    call reinicioVarsFamily() from _call_reinicioVarsFamily
    call reinicioVarsMad() from _call_reinicioVarsMad
    call reinicioVarsPad() from _call_reinicioVarsPad
    call reinicioVarsPeq() from _call_reinicioVarsPeq
    call reinicioVarsMed() from _call_reinicioVarsMed
    call reinicioVarsMay() from _call_reinicioVarsMay
    call reinicioVarsAlicia() from _call_reinicioVarsAlicia
    call reinicioVarsHasDadoClaseAlicia() from _call_reinicioVarsHasDadoClaseAlicia_1

    call reinicioEvents() from _call_reinicioEvents

    if numDia > 6:
        $ numDia = 0
        $ numSemana = numSemana +1

    return


screen diaHud():
    zorder 90
    if numDia > 6:
        $ numDia = 0
    hbox:
        xalign 0.48 ypos 15
        if numDia == 0:
            text(_("Lunes")):
                size 16.5
        elif numDia == 1:
            text(_("Martes")):
                size 16.5
        elif numDia == 2:
            text(_("Miercoles")):
                size 16.5
        elif numDia == 3:
            text(_("Jueves")):
                size 16.5
        elif numDia == 4:
            text(_("Viernes")):
                size 16.5
        elif numDia == 5:
            text(_("Sabado")):
                size 16.5
        elif numDia == 6:
            text(_("Domingo")):
                size 16.5
return

screen diaImageHud():
    zorder 90
    if tiempoDia > 4:
        $ tiempoDia = 0
    viewport:
        hbox:
            xpos 191 ypos 16
            imagemap:
                if tiempoDia == 0:
                    ground "amanecer"
                    $ tiempoDiaStr =_("Amanecer")
                else:
                    ground "amanecerdis"
            imagemap:
                xpos 5
                if tiempoDia == 1:
                    ground "manyana"
                    $ tiempoDiaStr =_("Mañana")
                else:
                    ground "manyanadis"
            imagemap:
                xpos 10
                if tiempoDia == 2:
                    ground "mediodia"
                    $ tiempoDiaStr =_("Mediodia")
                else:
                    ground "mediodiadis"
            imagemap:
                xpos 15
                if tiempoDia == 3:
                    ground "tarde"
                    $ tiempoDiaStr =_("Tarde")
                else:
                    ground "tardedis"
            imagemap:
                xpos 20
                if tiempoDia == 4:
                    ground "noche"
                    $ tiempoDiaStr =_("Noche")
                else:
                    ground "nochedis"
            imagemap:
                xpos 41
                if zona == "micasa" and room != "banyo" and room != "habHermanPeq" and room != "habHermanMed" and room != "habHermanMay" and room != "habPadres" or zona =="miMapa":
                    ground "reloj"
                    hover "relojh"
                    if adelantarHora == True:
                        if tiempoDia >= 0 and tiempoDia < 4 :
                            #hotspot (0, 0, 200, 200) action Call("nextTiempoDay",0)
                            hotspot (0, 0, 200, 200) action Call("mirarCambioDeHora")
                        else:
                            hotspot (0, 0, 200, 200) action Show("esTardeIrCama")
                    else:
                        hotspot (0, 0, 200, 200) action Show("noEsMomentoDeCambiarHora")

                else:
                    ground "relojbn"

        hbox:
            xalign 0.48 ypos 43

            if tiempoDiaStr == "Amanecer":
                text(_("Amanecer")):
                    size 16.5
            elif tiempoDiaStr == "Mañana":
                text(_("Mañana")):
                    size 16.5
            elif tiempoDiaStr == "Mediodia":
                text(_("Mediodia")):
                    size 16.5
            elif tiempoDiaStr == "Tarde":
                text(_("Tarde")):
                    size 16.5
            elif tiempoDiaStr == "Noche":
                text(_("Noche")):
                    size 16.5
return

screen esTardeIrCama():
    frame:
        xpos 950 ypos 14
        padding (20,10,20,10)
        hbox:
            #xpos 925 ypos 100
            text _("Es tarde, hay que ir a la cama"):
                size 17
                color "#000000"
            timer 1.2 action Hide('esTardeIrCama')
return

screen noEsMomentoDeCambiarHora():
    frame:
        xpos 950 ypos 14
        padding (20,10,20,10)
        hbox:
            #xpos 925 ypos 100
            text _("No es momento de hacer el vago"):
                size 17
                color "#000000"
            timer 1.2 action Hide('noEsMomentoDeCambiarHora')
return

label reinicioVarsEntrarBanyo:
    #Banyo
    $ hasEntradoBanyoMad = False
    $ hasEntradoBanyoPad = False
    $ hasEntradoBanyoPeq = False
    $ hasEntradoBanyoMed = False
    $ hasEntradoBanyoMay = False

    return

label reinicioVarsEntroCambiandose:
    $ hasEntradoHabPadrCambiandose = False
    $ hasEntradoHabPeqCambiandose = False
    $ hasEntradoHabMedCambiandose = False
    $ hasEntradoHabMayCambiandose = False
    return

label reinicioVarsEntroNoche:
    $ hasEntradoHabMayNocheDuerme = False
    return

label reinicioPlatosFregados:
    $ platosHoyFregados = False
    return

label reinicioHasTrabajadoHoy:
    $ hasTrabajadoHoy = False
    return

label reinicioEvents:
    $ eventMadRecogeRopaHabProta_visit = False
    return

label reinicioVarsHasDadoClaseAlicia():
    $ hasDadoClaseAlicia = False
    return
