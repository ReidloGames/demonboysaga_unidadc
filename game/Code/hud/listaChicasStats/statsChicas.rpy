screen listaChicas():
    zorder 96
    add "movilhoriz" xalign 0.5 ypos 0.1
    text _("Estadísticas"):
        xpos 565 ypos 100
        size 21
    hbox:
        xalign 0.50 ypos 0.2
        spacing 70
        if chicaMad == True:
            imagebutton:
                idle "madcarnet"
                hover "madcarnetS"
                action [Hide("listaChicas"),Call("chicaSelect","madcarnetBig",nombreMad)]
        else:
            imagemap:
                ground "censoredChi"
        if chicaPeq == True:
            imagebutton:
                idle "peqcarnet"
                hover "peqcarnetS"
                action [Hide("listaChicas"),Call("chicaSelect","peqcarnetBig",nombrePeq)]
        else:
            imagemap:
                ground "censoredChi"

        if chicaMed == True:
            imagebutton:
                idle "medcarnet"
                hover "medcarnetS"
                action [Hide("listaChicas"),Call("chicaSelect","medcarnetBig",nombreMed)]
        else:
            imagemap:
                ground "censoredChi"

        if chicaMay == True:
            imagebutton:
                idle "maycarnet"
                hover "maycarnetS"
                action [Hide("listaChicas"),Call("chicaSelect","maycarnetBig",nombreMay)]
        else:
            imagemap:
                ground "censoredChi"

        if chicaLucia == True:
            imagebutton:
                idle "luciacarnet"
                hover "luciacarnetS"
                action [Hide("listaChicas"),Call("chicaSelect","luciacarnetBig",nombreLucia)]
        else:
            imagemap:
                ground "censoredChi"



    hbox:
        xalign 0.50 ypos 0.35
        spacing 70


        if chicaClaire == True:
            imagebutton:
                idle "clairecarnet"
                hover "clairecarnetS"
                action [Hide("listaChicas"),Call("chicaSelect","clairecarnetBig",nombreClaire)]
        else:
            imagemap:
                ground "censoredChi"

        if chicaAlicia == True:
            imagebutton:
                idle "aliciacarnet"
                hover "aliciacarnetS"
                action [Hide("listaChicas"),Call("chicaSelect","aliciacarnetBig",nombreAlicia)]
        else:
            imagemap:
                ground "censoredChi"

        if chicaCarla == True:
            imagebutton:
                idle "carlacarnet"
                hover "carlacarnetS"
                action [Hide("listaChicas"),Call("chicaSelect","carlacarnetBig",nombreCarla)]
        else:
            imagemap:
                ground "censoredChi"

        imagemap:
            ground "censoredChi"
        imagemap:
            ground "censoredChi"


        #No borrar son coordenadas exactas para añadir mas chicas
    #hbox:
    #    xalign 0.50 ypos 0.495
    #    spacing 70
    #    imagemap:
    #        ground "cara1"
    #    imagemap:
    #        ground "cara1"
    #    imagemap:
    #        ground "cara1"
    #    imagemap:
    #        ground "cara1"
    #    imagemap:
    #        ground "cara1"

    #hbox:
    #    xalign 0.50 ypos 0.640
    #    spacing 70
    #    imagemap:
    #        ground "cara1"
    #    imagemap:
    #        ground "cara1"
    #    imagemap:
    #        ground "cara1"
    #    imagemap:
    #        ground "cara1"
    #    imagemap:
    #        ground "cara1"
    ####################################################
    hbox:
        imagebutton:
            xpos 870 ypos 535
            idle "flechaRet"
            hover "flechaReth"
            action [Hide("listaChicas"),Show("opcionesMovil")]


label chicaSelect(img,nomChica):
    if nomChica == "Sofia":
        show screen listaChicas2(img,nomChica,situMad2,afectoMad,submisMad)
    elif nomChica == "Dana":
        show screen listaChicas2(img,nomChica,situPeq2,afectoPeq,submisPeq)
    elif nomChica == "Erica":
        show screen listaChicas2(img,nomChica,situMed2,afectoMed,submisMed)
    elif nomChica == "Kara":
        show screen listaChicas2(img,nomChica,situMay2,afectoMay,submisMay)
    elif nomChica == "Lucia":
        show screen listaChicas2(img,nomChica,situLucia,afectoLucia,submisLucia)
    elif nomChica == "Claire":
        show screen listaChicas2(img,nomChica,situClaire,afectoClaire,submisClaire)
    elif nomChica == "Alicia":
        show screen listaChicas2(img,nomChica,situAlicia,afectoAlicia,submisAlicia)
    elif nomChica == "Carla":
        show screen listaChicas2(img,nomChica,situCarla,afectoCarla,submisCarla)

    return

screen listaChicas2(img,nomChica,situa,afecto,sumision):
    zorder 96
    add "movil" xalign 0.5 ypos 100

    vbox:
        imagemap:
            xpos 595 ypos 155
            ground [img]

        text _("Nombre: [nomChica]"):
            xpos 550 ypos 170
            size 17
        text _("Relación: [situa]"):
            xpos 550 ypos 170
            size 14
        text _("Afecto: [afecto]"):
            xpos 550 ypos 200
            size 15
        text _("Sumisión: [sumision]"):
            xpos 550 ypos 210
            size 15

        #if nomChica == nombreMad:
        #    imagebutton:
        #        xpos 582 ypos 355
        #        idle "botonInfoActions"
        #        hover "botonInfoActions_S"
        #        action [Hide("listaChicas2"),Show("infoActionRealizadoMad")]
        #if nomChica == nombrePeq:
        #    imagebutton:
        #        xpos 582 ypos 355
        #        idle "botonInfoActions"
        #        hover "botonInfoActions_S"
        #        action [Hide("listaChicas2"),Show("infoActionRealizadoPeq")]
        #if nomChica == nombreMed:
        #    imagebutton:
        #        xpos 582 ypos 355
        #        idle "botonInfoActions"
        #        hover "botonInfoActions_S"
        #        action [Hide("listaChicas2"),Show("infoActionRealizadoMed")]
        #if nomChica == nombreMay:
        #    imagebutton:
        #        xpos 582 ypos 355
        #        idle "botonInfoActions"
        #        hover "botonInfoActions_S"
        #        action [Hide("listaChicas2"),Show("infoActionRealizadoMay")]
#
        imagebutton:
            xpos 582 ypos 365
            idle "flechaRetmin"
            hover "flechaRetminh"
            action [Hide("listaChicas2"),Show("listaChicas")]
return
