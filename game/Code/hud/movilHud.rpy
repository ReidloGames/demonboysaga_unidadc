screen movilHud():
    zorder 90
    imagebutton:
        xalign 0.62 ypos 10
        idle "movilHud"
        hover "movilHud_S"
        action [Play("sound","audio/effects/enterMovil.ogg"),Show ('opcionesMovil'),SetVariable("optionsOpenHud",True),Call("esconderHud")]



screen opcionesMovil():
    zorder 96
    add "movil" xalign 0.5 ypos 100

    imagebutton:
        xalign 0.44 ypos 190
        idle "statsGirls"
        hover "statsGirls_S"
        action [Hide("opcionesMovil"),Show("listaChicas")]
    imagebutton:
        xalign 0.5 ypos 190
        idle "smsapp"
        hover "smsapp_S"
        action [Hide("opcionesMovil"),Show("smsWhats")]
    imagebutton:
        xalign 0.56 ypos 190
        idle "statsProtaApp"
        hover "statsProtaApp_S"
        action [Hide("opcionesMovil"),Show("protaStatsScreen")]

    imagebutton:
        xalign 0.44 ypos 290
        idle "escenasApp"
        hover "escenasApp_S"
        action [Hide ('opcionesMovil'),Show("listaChicasEscenas")]
    imagebutton:
        xalign 0.5 ypos 290
        idle "help"
        hover "help_S"
        if demonBoy == False:
            action [Hide ('opcionesMovil'),Show("ayudasJuego",tipoAcces="movil")]
        else:
            action [Hide ('opcionesMovil'),Show("ayudasJuegoDemonBoyTrue",tipoAcces="movil")]

    imagebutton:
        xalign 0.56 ypos 290
        idle "iconChampions"
        hover "iconChampions_S"
        action [Hide ('opcionesMovil'),Show("graciasPatreons")]


    imagebutton:
        xalign 0.44 ypos 390
        idle "iconPatreon"
        hover "iconPatreon_S"
        action OpenURL("www.patreon.com/reidloGames")
    imagebutton:
        xalign 0.5 ypos 390
        idle "iconSubscribestar"
        hover "iconSubscribestar_S"
        action OpenURL("www.subscribestar.com/reidloGames")

    #xalign 0.5 ypos 450
    add "plantillaNTR" xalign 0.5 ypos 480
    hbox:
        #xpos 500 ypos 460
        text "{b}NTR:{/b}" xpos 525 ypos 493
        textbutton "YES" action SetVariable("ntrGame", True) xpos 550 ypos 490
        textbutton "NO" action SetVariable("ntrGame", False) xpos 570 ypos 490


    imagebutton:
        xalign 0.50 ypos 550
        idle "exitapp"
        hover "exitapp_S"
        action [Play("sound","audio/effects/exitMovil.ogg"),Hide ('opcionesMovil'),Hide("listaChicas"),SetVariable("optionsOpenHud",False),Call("mostrarHud")]
