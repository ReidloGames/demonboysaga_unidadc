screen ayudasDirectHud():
    zorder 90
    imagebutton:
        xalign 0.672 ypos 14
        idle "help"
        hover "help_S"
        action Call ("getAyudasDirectHud")
return


label getAyudasDirectHud():
    call esconderHud() from _call_esconderHud_48
    if demonBoy == False:
        show screen ayudasJuego("direct")
    else:
        show screen ayudasJuegoDemonBoyTrue("direct")
    return
