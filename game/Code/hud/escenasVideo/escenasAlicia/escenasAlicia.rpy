screen escenasAlicia(nomChica):
    zorder 96
    add "fondoEscenas"
    hbox:
        text _("{b}Escenas [nomChica]{/b}"):
            xpos 565 ypos 40
            size 24
        imagebutton:
            xpos 870 ypos 40
            idle "flechaRetmin"
            hover "flechaRetminh"
            action [Hide("escenasAlicia"),Show("listaChicasEscenas")]

return
