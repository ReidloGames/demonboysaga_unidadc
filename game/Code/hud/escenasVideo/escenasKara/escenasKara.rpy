screen escenasKara(nomChica):
    zorder 96
    add "fondoEscenas"
    hbox:
        text _("{b}Escenas [nomChica]{/b}"):
            xpos 565 ypos 40
            size 24
        imagebutton:
            xpos 870 ypos 40
            idle "flechaRetmin"
            hover "flechaRetminh"
            action [Hide("escenasKara"),Show("listaChicasEscenas")]
    vbox:
        xpos 100 ypos 100
        viewport:
            draggable True
            scrollbars "vertical"
            xmaximum 1150
            ymaximum 500
#
            #grid 4 1:
            grid 1 1:
                spacing 40
                xalign 0.2
                #FILA1
                #Escena1
                if pensarEnKaraPaja == True:
                    frame:
                        padding (10,10,10,10)
                        imagebutton:
                            idle "escena1May"
                            hover "escena1May_S"
                            action [Hide("escenasKara"),Call("pensarEnKaraPajaEscenaReproductor")]
#
                else:
                    frame:
                        padding (10,10,10,10)
                        imagemap:
                            ground "escena1MayCris"
#
    #            #if escena2_Kara == True:
    #            frame:
    #                padding (10,10,10,10)
    #                imagemap:
    #                    ground "marcoEscenas"
    #            #if escena3_Kara == True:
    #            frame:
    #                padding (10,10,10,10)
    #                imagemap:
    #                    ground "marcoEscenas"
    #            #if escena4_Kara == True:
    #            frame:
    #                padding (10,10,10,10)
    #                imagemap:
    #                    ground "marcoEscenas"
#


return
