
#deshabilitaObjetosPersonas
screen listaChicasEscenas():
    zorder 96
    add "movilhoriz" xalign 0.5 ypos 0.1
    text _("Escenas"):
        xpos 565 ypos 100
        size 21
    hbox:
        xalign 0.50 ypos 0.2
        spacing 70
        if chicaMad == True:
            imagebutton:
                #xpos 345 ypos 146
                idle "madcarnet"
                hover "madcarnetS"
                action [Hide("listaChicasEscenas"),Call("chicaSelectEscenas",nombreMad)]
        else:
            imagemap:
                ground "censoredChi"
        if chicaPeq == True:
            imagebutton:
                idle "peqcarnet"
                hover "peqcarnetS"
                action [Hide("listaChicasEscenas"),Call("chicaSelectEscenas",nombrePeq)]
        else:
            imagemap:
                ground "censoredChi"

        if chicaMed == True:
            imagebutton:
                idle "medcarnet"
                hover "medcarnetS"
                action [Hide("listaChicasEscenas"),Call("chicaSelectEscenas",nombreMed)]
        else:
            imagemap:
                ground "censoredChi"

        if chicaMay == True:
            imagebutton:
                idle "maycarnet"
                hover "maycarnetS"
                action [Hide("listaChicasEscenas"),Call("chicaSelectEscenas",nombreMay)]
        else:
            imagemap:
                ground "censoredChi"

        if chicaLucia == True:
            imagebutton:
                idle "luciacarnet"
                hover "luciacarnetS"
                action [Hide("listaChicasEscenas"),Call("chicaSelectEscenas",nombreLucia)]
        else:
            imagemap:
                ground "censoredChi"


    hbox:
        xalign 0.50 ypos 0.35
        spacing 70


        if chicaClaire == True:
            imagebutton:
                idle "clairecarnet"
                hover "clairecarnetS"
                action [Hide("listaChicasEscenas"),Call("chicaSelectEscenas",nombreClaire)]
        else:
            imagemap:
                ground "censoredChi"

        if chicaAlicia == True:
            imagebutton:
                idle "aliciacarnet"
                hover "aliciacarnetS"
                action [Hide("listaChicasEscenas"),Call("chicaSelectEscenas",nombreAlicia)]
        else:
            imagemap:
                ground "censoredChi"

        if chicaCarla == True:
            imagebutton:
                idle "carlacarnet"
                hover "carlacarnetS"
                action [Hide("listaChicas"),Call("chicaSelectEscenas",nombreCarla)]
        else:
            imagemap:
                ground "censoredChi"

        imagemap:
            ground "censoredChi"
        imagemap:
            ground "censoredChi"


    #No borrar son coordenadas exactas para añadir mas chicas
    #hbox:
    #    xalign 0.50 ypos 0.35
    #    spacing 70
    #    imagemap:
    #        ground "cara1"
    #    imagemap:
    #        ground "cara1"
    #    imagemap:
    #        ground "cara1"
    #    imagemap:
    #        ground "cara1"
    #    imagemap:
    #        ground "cara1"

    #hbox:
    #    xalign 0.50 ypos 0.495
    #    spacing 70
    #    imagemap:
    #        ground "cara1"
    #    imagemap:
    #        ground "cara1"
    #    imagemap:
    #        ground "cara1"
    #    imagemap:
    #        ground "cara1"
    #    imagemap:
    #        ground "cara1"

    #hbox:
    #    xalign 0.50 ypos 0.640
    #    spacing 70
    #    imagemap:
    #        ground "cara1"
    #    imagemap:
    #        ground "cara1"
    #    imagemap:
    #        ground "cara1"
    #    imagemap:
    #        ground "cara1"
    #    imagemap:
    #        ground "cara1"
    ####################################################
    hbox:
        imagebutton:
            xpos 870 ypos 535
            idle "flechaRet"
            hover "flechaReth"
            action [Hide("listaChicasEscenas"),Show("opcionesMovil")]

label chicaSelectEscenas(nomChica):
    if nomChica == "Sofia":
        show screen escenasSofia(nomChica)
    elif nomChica == "Dana":
        show screen escenasDana(nomChica)
    elif nomChica == "Erica":
        show screen escenasErica(nomChica)
    elif nomChica == "Kara":
        show screen escenasKara(nomChica)
    elif nomChica == "Lucia":
        show screen escenasLucia(nomChica)
    elif nomChica == "Claire":
        show screen escenasClaire(nomChica)
    elif nomChica == "Alicia":
        show screen escenasAlicia(nomChica)
    elif nomChica == "Carla":
        show screen escenasCarla(nomChica)


    return
