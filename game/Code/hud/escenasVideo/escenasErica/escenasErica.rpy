screen escenasErica(nomChica):
    zorder 96
    add "fondoEscenas"
    hbox:
        text _("{b}Escenas [nomChica]{/b}"):
            xpos 565 ypos 40
            size 24
        imagebutton:
            xpos 870 ypos 40
            idle "flechaRetmin"
            hover "flechaRetminh"
            action [Hide("escenasErica"),Show("listaChicasEscenas")]
    vbox:
        xpos 100 ypos 100
        viewport:
            draggable True
            scrollbars "vertical"
            xmaximum 1150
            ymaximum 500

            grid 1 1:
                spacing 40
                xalign 0.2
                #FILA1
                if eventEricaBorrachaPillada == True:
                    frame:
                        padding (10,10,10,10)
                        imagebutton:
                            idle "escena1Med"
                            hover "escena1Med_S"
                            action [Hide("escenasErica"),Call("ericaBorrachaEscenaReproductor")]


                else:
                    frame:
                        padding (10,10,10,10)
                        imagemap:
                            ground "escena1MedCris"
#
    #            #if escena2_Erica == True:
    #            frame:
    #                padding (10,10,10,10)
    #                imagemap:
    #                    ground "marcoEscenas"
    #            #if escena3_Erica == True:
    #            frame:
    #                padding (10,10,10,10)
    #                imagemap:
    #                    ground "marcoEscenas"
    #            #if escena4_Erica == True:
    #            frame:
    #                padding (10,10,10,10)
    #                imagemap:
    #                    ground "marcoEscenas"



return
