screen escenasClaire(nomChica):
    zorder 96
    add "fondoEscenas"
    hbox:
        text _("{b}Escenas [nomChica]{/b}"):
            xpos 565 ypos 40
            size 24
        imagebutton:
            xpos 870 ypos 40
            idle "flechaRetmin"
            hover "flechaRetminh"
            action [Hide("escenasClaire"),Show("listaChicasEscenas")]

    vbox:
        xpos 100 ypos 100
        viewport:
            draggable True
            scrollbars "vertical"
            xmaximum 1150
            ymaximum 500

            grid 2 1:
                spacing 40
                xalign 0.2
                #FILA1
                if protaHablarConDirectorDinero == True:
                    frame:
                        padding (10,10,10,10)
                        imagebutton:
                            idle "escena1Claire"
                            hover "escena1Claire_S"
                            action [Hide("escenasClaire"),Call("eventProtaHablarConDirectorDineroEscenaReproductor")]


                else:
                    frame:
                        padding (10,10,10,10)
                        imagemap:
                            ground "escena1ClaireCris"

                #SCENE2
                if eventClaireDirectorVisita2 == True:
                    frame:
                        padding (10,10,10,10)
                        imagebutton:
                            idle "escena2Claire"
                            hover "escena2Claire_S"
                            action [Hide("escenasClaire"),Call("eventClaireDirectorVisita2EscenaReproductor")]
                else:
                    frame:
                        padding (10,10,10,10)
                        imagemap:
                            ground "escena2ClaireCris"

    #            #if escena3_Erica == True:
    #            frame:
    #                padding (10,10,10,10)
    #                imagemap:
    #                    ground "marcoEscenas"
    #            #if escena4_Erica == True:
    #            frame:
    #                padding (10,10,10,10)
    #                imagemap:
    #                    ground "marcoEscenas"



return
