screen smsWhats():
    zorder 96
    add "movil" xalign 0.5 ypos 100
    add "fondoWhats" xalign 0.5 ypos 150
    hbox:
        spacing 12
        xpos 536 ypos 200
        if madWhats == True:
            imagebutton:
                idle "madcarnet"
                hover "madcarnetS"
                action [Hide("smsWhats"),Call("smsWhatsSelect",nombreMad)]


        if peqWhats == True:
            imagebutton:
                idle "peqcarnet"
                hover "peqcarnetS"
                action [Hide("smsWhats"),Call("smsWhatsSelect",nombrePeq)]

        if medWhats == True:
            imagebutton:
                idle "medcarnet"
                hover "medcarnetS"
                action [Hide("smsWhats"),Call("smsWhatsSelect",nombreMed)]

    hbox:
        spacing 12
        xpos 536 ypos 290
        if mayWhats == True:
            imagebutton:
                idle "maycarnet"
                hover "maycarnetS"
                action [Hide("smsWhats"),Call("smsWhatsSelect",nombreMay)]

    hbox:
        imagebutton:
            xpos 580 ypos 560
            idle "flechaRetmin"
            hover "flechaRetminh"
            action [Hide("smsWhats"),Show("opcionesMovil")]


label smsWhatsSelect(nomChica):
    show screen textosWhats(nomChica)
    return


screen textosWhats(nomChica):
    zorder 96
    add "movil" xalign 0.5 ypos 100
    add "fondoWhatsSL" xalign 0.5 ypos 150
    text "{b}{color=#3d5166}[nomChica]{/color}{/b}" xalign 0.5 ypos 155:
        size 24

    viewport:
        xalign 0.51 ypos 190
        draggable True
        scrollbars "vertical"
        xmaximum 250
        ymaximum 300

        ##SOFIA
        if nomChica == "Sofia":
            vbox:
                spacing 15
                if textWhats1Mad == True:
                    null
                    #frame:
                    #    #left_padding 15 right_padding 15 top_padding 5
                    #    padding (15,15,15,15)
                    #    text _("1-dfdfdffgjh jhgjhgjg ukghkghk iyhih igighiuh ihiuh iuuhiuh"):
                    #        size 14
                    #        color "#000000"
                    #imagebutton:
                    #    idle "pruebaImgWhats"
                    #    hover "pruebaImgWhats"
                    #    action [Hide("textosWhats"),Show("smsWhats")]


        ##DANA Peq
        if nomChica == "Dana":
            vbox:
                spacing 15
                if textWhats1Peq == True:
                    null
                    #frame:
                    #    #left_padding 15 right_padding 15 top_padding 5
                    #    padding (15,15,15,15)
                    #    text _("1-Esta es Dana"):
                    #        size 14
                    #        color "#000000"
                    #imagebutton:
                    #    idle "pruebaImgWhats"
                    #    hover "pruebaImgWhats"
                    #    action [Hide("textosWhats"),Show("smsWhats")]



        ##ERICA Med
        if nomChica == "Erica":
            vbox:
                spacing 15
                if textWhats1Med == True:
                    null
                    #frame:
                    #    #left_padding 15 right_padding 15 top_padding 5
                    #    padding (15,15,15,15)
                    #    text _("1-Esta es Erica"):
                    #        size 14
                    #        color "#000000"
                    #imagebutton:
                    #    idle "pruebaImgWhats"
                    #    hover "pruebaImgWhats"
                    #    action [Hide("textosWhats"),Show("smsWhats")]



        ##KARA May
        if nomChica == "Kara":
            vbox:
                spacing 15
                if textWhats1May == True:
                    null
                    #frame:
                    #    #left_padding 15 right_padding 15 top_padding 5
                    #    padding (15,15,15,15)
                    #    text _("1-Esta es Kara"):
                    #        size 14
                    #        color "#000000"
                    #imagebutton:
                    #    idle "pruebaImgWhats"
                    #    hover "pruebaImgWhats"
                    #    action [Hide("textosWhats"),Show("smsWhats")]



    vbox:
        imagebutton:
            xpos 580 ypos 560
            idle "flechaRetmin"
            hover "flechaRetminh"
            action [Hide("textosWhats"),Show("smsWhats")]
