screen hudPlantilla():
    zorder 89
    add "hudPlantilla" xalign 0.05 ypos 10


label esconderHud():
    $ optionsOpenHud = True
    hide screen hudPlantilla
    hide screen diaHud
    hide screen movilHud
    hide screen diaImageHud
    hide screen mochilaHud
    hide screen dineroHud
    hide screen destinoHud
    hide screen genteCasaHud
    hide screen ayudasDirectHud
    hide screen casaVolverHabitacion
    return


label mostrarHud():
    $ optionsOpenHud = False
    show screen hudPlantilla()
    show screen diaHud()
    show screen movilHud()
    show screen diaImageHud()
    show screen mochilaHud()
    show screen dineroHud()
    show screen destinoHud()
    show screen genteCasaHud()
    show screen ayudasDirectHud()
    show screen casaVolverHabitacion()
    return


label avisosStatsSms(nomAviso):
    #sms
    #afecto
    #sumision
    if nomAviso == "sms":
        show screen avisosStatsSmsScreen("sms")
        play sound "audio/effects/tonoSms.ogg"
        #with Pause(1)
        pause
    if nomAviso == "afecto":
        show screen avisosStatsSmsScreen("afecto")
        play sound "audio/effects/kiss.ogg"
        #with Pause(1)
        pause

    if nomAviso == "sumision":
        show screen avisosStatsSmsScreen("sumision")
        play sound "audio/effects/latigo.ogg"
        #with Pause(1)
        pause
    if nomAviso == "newContact":
        show screen avisosStatsSmsScreen("newContact")
        play sound "audio/effects/newfriend.ogg"
        #with Pause(1)
        pause
    if nomAviso == "newUbication":
        show screen avisosStatsSmsScreen("newUbication")
        play sound "audio/effects/newfriend.ogg"
        #with Pause(1)
        pause

    hide screen avisosStatsSmsScreen

return

screen avisosStatsSmsScreen(nomAviso):
    zorder 96
    hbox:
        xpos 1200 ypos 10
        if nomAviso == "sms":
            imagemap:
                ground "smsStats"

        if nomAviso == "afecto":
            imagemap:
                ground "corazonStats"

        if nomAviso == "sumision":
            imagemap:
                ground "latigoStats"

        if nomAviso == "newContact":
            imagemap:
                ground "newContact"

        if nomAviso == "newUbication":
            imagemap:
                ground "newUbication"


        #xpos 925 ypos 100
        #text _("No tiendes dinero suficiente"):
        #    size 17
        #    color "#000000"
        timer 1.7 action Hide('avisosStatsSmsScreen')
