screen casaVolverHabitacion():
    zorder 90
    if zona == "miMapa":
        if puedoVolverMiHabitacion == True:
            imagebutton:
                xalign 0.725 ypos 10
                idle "volverMiHabitacion"
                hover "volverMiHabitacion_S"

                action [Play("sound","audio/effects/puertaEntrada.ogg"),SetVariable("zona","micasa"),Call("nextZona","mihabitacion"),]
                #action Call("sePuedeVolverMiHabitacion")

        else:
            imagebutton:
                xalign 0.725 ypos 10
                idle "volverMiHabitacion_Dis"

return


label sePuedeVolverMiHabitacion():
    if llegoTardeClaseAlicia == True:
        $ puedoVolverMiHabitacion = False
        $ puedesDesplazarteLibreCasa = False
    #if eventQueBienHuele == True:
    #    $ puedoVolverMiHabitacion = True
    #    $ puedesDesplazarteLibreCasa = True
    if eventSuenyoConMad == True:
        $ puedoVolverMiHabitacion = False
        $ puedesDesplazarteLibreCasa = False
    if eventComunicadoPadCuentaProblema == True:
        $ puedoVolverMiHabitacion = True
        $ puedesDesplazarteLibreCasa = True


    return
