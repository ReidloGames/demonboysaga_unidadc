label reinicioVarsAlicia():
    call reinicioConversAliciaEnClaseYaHablado() from _call_reinicioConversAliciaEnClaseYaHablado

    return


label reinicioConversAliciaEnClaseYaHablado():
    $ conversAliciaEnClase_Hablado = False
    return
