label reinicioVarsMay():
    #HabMay##
    #Martes
    call reinicioConversMayHabMayEnBragas1raMartesYaHablado() from _call_reinicioConversMayHabMayEnBragas1raMartesYaHablado
    call reinicioConversMayHabMayEnPc4taMartesYaHablado() from _call_reinicioConversMayHabMayEnPc4taMartesYaHablado
    ###JardinPisicna
    #Miercoles
    call reinicioConversjardinPisTomarSol2daMiercolesYaHablado() from _call_reinicioConversjardinPisTomarSol2daMiercolesYaHablado
    ##Salon##
    #Miercoles
    call reinicioConversMayPintaUnyasSalon4taMiercolesYaHablado() from _call_reinicioConversMayPintaUnyasSalon4taMiercolesYaHablado
    #Jueves
    call reinicioConversMayHabMayEnPc4taJuevesYaHablado() from _call_reinicioConversMayHabMayEnPc4taJuevesYaHablado

    ###GaleriaGym
    #Viernes
    call reinicioConversMayHaceGymGaleria1raViernesYaHablado() from _call_reinicioConversMayHaceGymGaleria1raViernesYaHablado

    ##Sabado##
    #Sabado
    call reinicioConversMayCocinaHaceCafe1raSabadoYaHablado() from _call_reinicioConversMayCocinaHaceCafe1raSabadoYaHablado

    return

label reinicioConversMayHabMayEnBragas1raMartesYaHablado():
    $ conversMayHabMayEnBragas1raMartes_Hablado = False
    return

label reinicioConversMayHabMayEnPc4taMartesYaHablado():
    $ conversMayHabMayEnPc4taMartes_Hablado = False
    return

label reinicioConversjardinPisTomarSol2daMiercolesYaHablado():
    $ conversMayJardinPisTomarSol2daMiercoles_Hablado = False
    $ mayJardinPisTomarSol2daInteracMiercoles_visit1 = False
    return

label reinicioConversMayHabMayEnPc4taJuevesYaHablado():
    $ conversMayHabMayEnPc4taJueves_Hablado = False
    return

label reinicioConversMayPintaUnyasSalon4taMiercolesYaHablado():
    $ conversMayPintaUnyasSalon4taMiercoles_Hablado = False
    return

label reinicioConversMayHaceGymGaleria1raViernesYaHablado():
    $ conversMayHaceGymGaleria1raViernes_Hablado = False
    return

label reinicioConversMayCocinaHaceCafe1raSabadoYaHablado():
    $ conversMayCocinaHaceCafe1raSabado_Hablado = False
    return
