label reinicioVarsMed():
    #HabMed
    call reinicioConversMedEnSuHabitacionEstudia2daLunesYaHablado() from _call_reinicioConversMedEnSuHabitacionEstudia2daLunesYaHablado
    call reinicioConversMedEnSuHabitacionDepieArmario4taMartesYaHablado() from _call_reinicioConversMedEnSuHabitacionDepieArmario4taMartesYaHablado
    #Comedor
    call reinicioConversMedComedorEstudiando3raMiercolesYaHablado() from _call_reinicioConversMedComedorEstudiando3raMiercolesYaHablado
    #JardinPis
    call reinicioConversMedJardinTomaSol2daSabadoYaHablado() from _call_reinicioConversMedJardinTomaSol2daSabadoYaHablado
    #Salon
    call reinicioConversMedSalonMiraMovil4taSabadoYaHablado() from _call_reinicioConversMedSalonMiraMovil4taSabadoYaHablado


    return



label reinicioConversMedEnSuHabitacionEstudia2daLunesYaHablado():
    $ conversMedEnSuHabitacionEstudia2daLunes_Hablado = False
    return



label reinicioConversMedEnSuHabitacionDepieArmario4taMartesYaHablado():
    $ conversEnSuHabitacionDepieArmario4taMartes_Hablado = False
    return

label reinicioConversMedComedorEstudiando3raMiercolesYaHablado():
    $ conversMedComedorEstudiando3raMiercoles_Hablado = False
    return

label reinicioConversMedJardinTomaSol2daSabadoYaHablado():
    if demonBoy == False:
        $ conversMedJardinTomaSol2daSabado_Hablado = False
    $ conversMedJardinTomaSol2daSabado_HabladoVisit2 = False
    return

label reinicioConversMedSalonMiraMovil4taSabadoYaHablado():
    $ conversMedSalonMiraMovil4taSabado_Hablado = False
    return
