label reinicioVarsPeq():
    #Lunes
    call reinicioConversPeqGaleriaHaciendoGym3raLunesYaHablado() from _call_reinicioConversPeqGaleriaHaciendoGym3raLunesYaHablado
    #Martes
    call reinicioConversPeqSuHabitacionMiraPc1raMartesYaHablado() from _call_reinicioConversPeqSuHabitacionMiraPc1raMartesYaHablado
    #Miercoles
    call reinicioConversPeqCocinaHaceComida3raMiercolesYaHablado() from _call_reinicioConversPeqCocinaHaceComida3raMiercolesYaHablado
    #Jueves
    call reinicioConversPeqSalonEstiradaSofa4taJuevesYaHablado() from _call_reinicioConversPeqSalonEstiradaSofa4taJuevesYaHablado
    #Sabado
    call reinicioConversPeqSuHabitacionMirandoEspejo2daSabadoYaHablado() from _call_reinicioConversPeqSuHabitacionMirandoEspejo2daSabadoYaHablado
    #Domingo
    call reinicioConversPeqEnSuHabitacionSentadaCama1raDomingoYaHablado() from _call_reinicioConversPeqEnSuHabitacionSentadaCama1raDomingoYaHablado

    return


label reinicioConversPeqGaleriaHaciendoGym3raLunesYaHablado():
    $ conversPeqGaleriaHaciendoGym3raLunes_Hablado = False
    return

label reinicioConversPeqSuHabitacionMiraPc1raMartesYaHablado():
    $ conversPeqSuHabitacionMiraPc1raMartes_Hablado = False
    return

label reinicioConversPeqCocinaHaceComida3raMiercolesYaHablado():
    $ conversPeqCocinaHaceComida3raMiercoles_Hablado = False
    return

label reinicioConversPeqSalonEstiradaSofa4taJuevesYaHablado():
    $ conversPeqSalonEstiradaSofa4taJueves_Hablado = False
    return

label reinicioConversPeqSuHabitacionMirandoEspejo2daSabadoYaHablado():
    $ conversPeqSuHabitacionMirandoEspejo2daSabado_Hablado = False
    return

label reinicioConversPeqEnSuHabitacionSentadaCama1raDomingoYaHablado():
    $ conversPeqEnSuHabitacionSentadaCama1raDomingo_Hablado = False
    return
