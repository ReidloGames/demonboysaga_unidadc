label reinicioVarsPad():
    call reinicioConversPadSentadoCamaHabPadr1raLunesYaHablado from _call_reinicioConversPadSentadoCamaHabPadr1raLunesYaHablado
    call reinicioConversPadDePieHabPadr2daMiercolesYaHablado from _call_reinicioConversPadDePieHabPadr2daMiercolesYaHablado

    return



label reinicioConversPadSentadoCamaHabPadr1raLunesYaHablado():
    $ conversPadSentadoCamaHabPadr1raLunes_Hablado = False
    return

label reinicioConversPadDePieHabPadr2daMiercolesYaHablado():
    $ conversPadDePieHabPadr2daMiercoles_Hablado = False
    return
