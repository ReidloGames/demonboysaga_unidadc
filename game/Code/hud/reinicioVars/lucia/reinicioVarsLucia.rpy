label reinicioVarsLucia():
    call reinicioConversLuciaPasilloWcLimpiaYaHablado() from _call_reinicioConversLuciaPasilloWcLimpiaYaHablado

    return


label reinicioConversLuciaPasilloWcLimpiaYaHablado():
    $ conversLuciaPasilloWcLimpia_Hablado = False

    return
