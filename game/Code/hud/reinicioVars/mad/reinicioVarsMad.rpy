label reinicioVarsMad():
    call reinicioConversMadCocinaPrepaComidaYaHablado() from _call_reinicioConversMadCocinaPrepaComidaYaHablado
    call reinicioProtaSentadoSofaConMad() from _call_reinicioProtaSentadoSofaConMad
    call reinicioConversMadHaciendoGymYaHablado() from _call_reinicioConversMadHaciendoGymYaHablado
    call reinicioMadHabPadrEnPijama4taYaHablado() from _call_reinicioMadHabPadrEnPijama4taYaHablado
    call reinicioConversMadCocinaDarMamar3raYaHablado() from _call_reinicioConversMadCocinaDarMamar3raYaHablado
    call reinicioConversMadJardinPisTomaSol2daYaHablado() from _call_reinicioConversMadJardinPisTomaSol2daYaHablado
    call reinicioConversMadSalonMiraTv4taYaHablado() from _call_reinicioConversMadSalonMiraTv4taYaHablado


    return





label reinicioConversMadCocinaPrepaComidaYaHablado:
    $ conversMadCocinaPrepaComida3raLunes_Hablado = False
    $ conversMadCocinaLimpia3raMartes_Hablado = False
    return

label reinicioProtaSentadoSofaConMad():
    $ protaSentadoSofaConMad4taMartes = False
    return

label reinicioConversMadHaciendoGymYaHablado:
    $ conversMadGaleriaHaciendoGym1raMiercoles_Hablado = False
    return

label reinicioMadHabPadrEnPijama4taYaHablado:
    $ conversMadHabPadrEnPijama4taMiercoles_Hablado = False
    #fase2
    $ madHabPadrEnPijama4taInteracMiercoles_visit2 = False
    return

label reinicioConversMadCocinaDarMamar3raYaHablado():
    $ conversMadCocinaDarMamar3raJueves_Hablado = False
    return

label reinicioConversMadJardinPisTomaSol2daYaHablado():
    $ conversMadJardinPisTomaSol2daSabado_Hablado = False
    return

label reinicioConversMadSalonMiraTv4taYaHablado():
    $ conversMadSalonMiraTv4taDomingo_Hablado = False
    return
