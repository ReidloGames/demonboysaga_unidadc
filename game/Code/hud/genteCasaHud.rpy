screen genteCasaHud():
    zorder 90
    if zona == "micasa":
        imagebutton:
            xalign 0.725 ypos 10
            idle "genteCasa"
            hover "genteCasa_S"
            action Call ("getGenteCasa")
return

label getGenteCasa:
    call esconderHud() from _call_esconderHud_13
    #call sePuedeVolverMiHabitacion() from _call_sePuedeVolverMiHabitacion_1
    show screen getContentGenteCasa()
    return

screen getContentGenteCasa():
    zorder 96
    modal True
    #add "mapGenteCasa" xalign 0.05 ypos 10
    imagemap:
        idle "mapGenteCasa"
        if zona == "micasa":
            #if room == "mihabitacion":
            if puedesDesplazarteLibreCasa == True:
                hover "mapGenteCasa_S"
                #MiHabitacion
                hotspot (105, 150, 260, 180):
                    action [Hide ("getContentGenteCasa"),Play("sound","audio/effects/open_door.ogg"),Call("nextZona","mihabitacion")]
                #GYM
                hotspot (25, 310, 220, 190):
                    action [Hide ("getContentGenteCasa"),Play("sound","audio/effects/open_door.ogg"),Call("nextZona","galeriaGym")]
                #Pasillo1
                hotspot (250, 310, 135, 80):
                    action [Hide ("getContentGenteCasa"),Play("sound","audio/effects/open_door.ogg"),Call("nextZona","pasillo1")]
                #Pasillo2
                hotspot (400, 310, 380, 80):
                    action [Hide ("getContentGenteCasa"),Play("sound","audio/effects/open_door.ogg"),Call("nextZona","pasillo2")]
                #Comedor
                hotspot (790, 240, 210, 160):
                    action [Hide ("getContentGenteCasa"),Play("sound","audio/effects/open_door.ogg"),Call("nextZona","comedor")]
                #Salon
                hotspot (790, 50, 130, 170):
                    action [Hide ("getContentGenteCasa"),Play("sound","audio/effects/open_door.ogg"),Call("nextZona","salon")]
                #Cocina
                hotspot (1000, 210, 200, 80):
                    action [Hide ("getContentGenteCasa"),Play("sound","audio/effects/open_door.ogg"),Call("nextZona","cocina")]
                #Entrada
                hotspot (900, 400, 75, 180):
                    action [Hide ("getContentGenteCasa"),Play("sound","audio/effects/open_door.ogg"),Call("nextZona","entradaCasa")]
                #jardinPis
                hotspot (980, 400, 280, 180):
                    action [Hide ("getContentGenteCasa"),Play("sound","audio/effects/open_door.ogg"),Call("nextZona","jardinPis")]
                #Garage
                hotspot (730, 550, 170, 100):
                    action [Hide ("getContentGenteCasa"),Play("sound","audio/effects/open_door.ogg"),Call("nextZona","garage")]



    #mihabitacion#####
    if localizaPeq == "mihabitacion" and hermanPeqInvisible == False:
        add "peqmini" xalign 0.20 yalign 0.27
    if localizaMed == "mihabitacion" and hermanMedInvisible == False:
        add "medmini" xalign 0.23 yalign 0.27
    if localizaMay == "mihabitacion" and hermanMayInvisible == False:
        add "maymini" xalign 0.26 yalign 0.27
    if localizaMad == "mihabitacion" and madInvisible == False:
        add "madmini" xalign 0.20 yalign 0.37
    if localizaPad == "mihabitacion" and padInvisible == False:
        add "padmini" xalign 0.23 yalign 0.37
    #pasillo1#####
    if localizaPeq == "pasillo1" and hermanPeqInvisible == False:
        add "peqmini" xalign 0.205 yalign 0.47
    if localizaMed == "pasillo1" and hermanMedInvisible == False:
        add "medmini" xalign 0.235 yalign 0.47
    if localizaMay == "pasillo1" and hermanMayInvisible == False:
        add "maymini" xalign 0.265 yalign 0.47
    if localizaMad == "pasillo1" and madInvisible == False:
        add "madmini" xalign 0.205 yalign 0.53
    if localizaPad == "pasillo1" and padInvisible == False:
        add "padmini" xalign 0.235 yalign 0.53
    #pasillo2
    if localizaPeq == "pasillo2" and hermanPeqInvisible == False:
        add "peqmini" xalign 0.38 yalign 0.47
    if localizaMed == "pasillo2" and hermanMedInvisible == False:
        add "medmini" xalign 0.41 yalign 0.47
    if localizaMay == "pasillo2" and hermanMayInvisible == False:
        add "maymini" xalign 0.44 yalign 0.47
    if localizaMad == "pasillo2" and madInvisible == False:
        add "madmini" xalign 0.47 yalign 0.47
    if localizaPad == "pasillo2" and padInvisible == False:
        add "padmini" xalign 0.5 yalign 0.47
    #galeriaGym
    if localizaPeq == "galeriaGym" and hermanPeqInvisible == False:
        add "peqmini" xalign 0.05 yalign 0.47
    if localizaMed == "galeriaGym" and hermanMedInvisible == False:
        add "medmini" xalign 0.08 yalign 0.47
    if localizaMay == "galeriaGym" and hermanMayInvisible == False:
        add "maymini" xalign 0.11 yalign 0.47
    if localizaMad == "galeriaGym" and madInvisible == False:
        add "madmini" xalign 0.05 yalign 0.57
    if localizaPad == "galeriaGym" and padInvisible == False:
        add "padmini" xalign 0.08 yalign 0.57
    #galeriaGymComodin
    if localizaPeqComodin == "galeriaGym" and hermanPeqInvisible == False and demonBoy == True:
        add "peqminicomodin" xalign 0.05 yalign 0.40
    if localizaMedComodin == "galeriaGym" and hermanMedInvisible == False and demonBoy == True:
        add "medminicomodin" xalign 0.08 yalign 0.40
    if localizaMayComodin == "galeriaGym" and hermanMayInvisible == False and demonBoy == True:
        add "mayminicomodin" xalign 0.11 yalign 0.40
    if localizaMadComodin == "galeriaGym" and madInvisible == False and demonBoy == True:
        add "madminicomodin" xalign 0.14 yalign 0.40



    #habHermanPeq
    if localizaPeq == "habHermanPeq" and hermanPeqInvisible == False:
        add "peqmini" xalign 0.31 yalign 0.27
    if localizaMed == "habHermanPeq" and hermanMedInvisible == False:
        add "medmini" xalign 0.34 yalign 0.27
    if localizaMay == "habHermanPeq" and hermanMayInvisible == False:
        add "maymini" xalign 0.37 yalign 0.27
    if localizaMad == "habHermanPeq" and madInvisible == False:
        add "madmini" xalign 0.31 yalign 0.37
    if localizaPad == "habHermanPeq" and padInvisible == False:
        add "padmini" xalign 0.34 yalign 0.37
    #habHermanMed
    if localizaPeq == "habHermanMed" and hermanPeqInvisible == False:
        add "peqmini" xalign 0.42 yalign 0.27
    if localizaMed == "habHermanMed" and hermanMedInvisible == False:
        add "medmini" xalign 0.45 yalign 0.27
    if localizaMay == "habHermanMed" and hermanMayInvisible == False:
        add "maymini" xalign 0.48 yalign 0.27
    if localizaMad == "habHermanMed" and madInvisible == False:
        add "madmini" xalign 0.42 yalign 0.37
    if localizaPad == "habHermanMed" and padInvisible == False:
        add "padmini" xalign 0.45 yalign 0.37
    #habHermanMay
    if localizaPeq == "habHermanMay" and hermanPeqInvisible == False:
        add "peqmini" xalign 0.525 yalign 0.27
    if localizaMed == "habHermanMay" and hermanMedInvisible == False:
        add "medmini" xalign 0.555 yalign 0.27
    if localizaMay == "habHermanMay" and hermanMayInvisible == False:
        add "maymini" xalign 0.585 yalign 0.27
    if localizaMad == "habHermanMay" and madInvisible == False:
        add "madmini" xalign 0.525 yalign 0.37
    if localizaPad == "habHermanMay" and padInvisible == False:
        add "padmini" xalign 0.555 yalign 0.37
    #comedor
    if localizaPeq == "comedor" and hermanPeqInvisible == False:
        add "peqmini" xalign 0.65 yalign 0.38
    if localizaMed == "comedor" and hermanMedInvisible == False:
        add "medmini" xalign 0.68 yalign 0.38
    if localizaMay == "comedor" and hermanMayInvisible == False:
        add "maymini" xalign 0.71 yalign 0.38
    if localizaMad == "comedor" and madInvisible == False:
        add "madmini" xalign 0.65 yalign 0.48
    if localizaPad == "comedor" and padInvisible == False:
        add "padmini" xalign 0.68 yalign 0.48
    #comedorComodin
    if localizaPeqComodin == "comedor" and hermanPeqInvisible == False and demonBoy == True:
        add "peqminicomodin" xalign 0.65 yalign 0.52
    if localizaMedComodin == "comedor" and hermanMedInvisible == False and demonBoy == True:
        add "medminicomodin" xalign 0.68 yalign 0.52
    if localizaMayComodin == "comedor" and hermanMayInvisible == False and demonBoy == True:
        add "mayminicomodin" xalign 0.71 yalign 0.52
    if localizaMadComodin == "comedor" and madInvisible == False and demonBoy == True:
        add "madminicomodin" xalign 0.74 yalign 0.52



    #salon
    if localizaPeq == "salon" and hermanPeqInvisible == False:
        add "peqmini" xalign 0.64 yalign 0.11
    if localizaMed == "salon" and hermanMedInvisible == False:
        add "medmini" xalign 0.67 yalign 0.11
    if localizaMay == "salon" and hermanMayInvisible == False:
        add "maymini" xalign 0.70 yalign 0.11
    if localizaMad == "salon" and madInvisible == False:
        add "madmini" xalign 0.64 yalign 0.23
    if localizaPad == "salon" and padInvisible == False:
        add "padmini" xalign 0.67 yalign 0.23
    #SalonComodin
    if localizaPeqComodin == "salon" and hermanPeqInvisible == False and demonBoy == True:
        add "peqminicomodin" xalign 0.63 yalign 0.03
    if localizaMedComodin == "salon" and hermanMedInvisible == False and demonBoy == True:
        add "medminicomodin" xalign 0.66 yalign 0.03
    if localizaMayComodin == "salon" and hermanMayInvisible == False and demonBoy == True:
        add "mayminicomodin" xalign 0.69 yalign 0.03
    if localizaMadComodin == "salon" and madInvisible == False and demonBoy == True:
        add "madminicomodin" xalign 0.72 yalign 0.03


    #cocina
    if localizaPeq == "cocina" and hermanPeqInvisible == False:
        add "peqmini" xalign 0.81 yalign 0.319
    if localizaMed == "cocina" and hermanMedInvisible == False:
        add "medmini" xalign 0.84 yalign 0.319
    if localizaMay == "cocina" and hermanMayInvisible == False:
        add "maymini" xalign 0.87 yalign 0.319
    if localizaMad == "cocina" and madInvisible == False:
        add "madmini" xalign 0.81 yalign 0.38
    if localizaPad == "cocina" and padInvisible == False:
        add "padmini" xalign 0.84 yalign 0.38
    #ComodinCocina
    if localizaPeqComodin == "cocina" and hermanMayInvisible == False and demonBoy == True:
        add "peqminicomodin" xalign 0.82 yalign 0.26
    if localizaMedComodin == "cocina" and hermanMayInvisible == False and demonBoy == True:
        add "medminicomodin" xalign 0.85 yalign 0.26
    if localizaMayComodin == "cocina" and hermanMayInvisible == False and demonBoy == True:
        add "mayminicomodin" xalign 0.88 yalign 0.26
    if localizaMadComodin == "cocina" and madInvisible == False and demonBoy == True:
        add "madminicomodin" xalign 0.91 yalign 0.26



    #habPadres
    if localizaPeq == "habPadres" and hermanPeqInvisible == False:
        add "peqmini" xalign 0.83 yalign 0.46
    if localizaMed == "habPadres" and hermanMedInvisible == False:
        add "medmini" xalign 0.86 yalign 0.46
    if localizaMay == "habPadres" and hermanMayInvisible == False:
        add "maymini" xalign 0.89 yalign 0.46
    if localizaMad == "habPadres" and madInvisible == False:
        add "madmini" xalign 0.83 yalign 0.56
    if localizaPad == "habPadres" and padInvisible == False:
        add "padmini" xalign 0.86 yalign 0.56
    #banyo
    if localizaPeq == "banyo" and hermanPeqInvisible == False:
        add "peqmini" xalign 0.6 yalign 0.61
    if localizaMed == "banyo" and hermanMedInvisible == False:
        add "medmini" xalign 0.63 yalign 0.61
    if localizaMay == "banyo" and hermanMayInvisible == False:
        add "maymini" xalign 0.66 yalign 0.61
    if localizaMad == "banyo" and madInvisible == False:
        add "madmini" xalign 0.6 yalign 0.71
    if localizaPad == "banyo" and padInvisible == False:
        add "padmini" xalign 0.63 yalign 0.71


    #entradaCasa
    if localizaPeq == "entradaCasa" and hermanPeqInvisible == False:
        add "peqmini" xalign 0.728 yalign 0.60
    if localizaMed == "entradaCasa" and hermanMedInvisible == False:
        add "medmini" xalign 0.755 yalign 0.60
    if localizaMay == "entradaCasa" and hermanMayInvisible == False:
        add "maymini" xalign 0.745 yalign 0.68
    if localizaMad == "entradaCasa" and madInvisible == False:
        add "madmini" xalign 0.728 yalign 0.76
    if localizaPad == "entradaCasa" and padInvisible == False:
        add "padmini" xalign 0.755 yalign 0.76

    #jardinPis
    if localizaPeq == "jardinPis" and hermanPeqInvisible == False:
        add "peqmini" xalign 0.85 yalign 0.66
    if localizaMed == "jardinPis" and hermanMedInvisible == False:
        add "medmini" xalign 0.88 yalign 0.66
    if localizaMay == "jardinPis" and hermanMayInvisible == False:
        add "maymini" xalign 0.91 yalign 0.66
    if localizaMad == "jardinPis" and madInvisible == False:
        add "madmini" xalign 0.85 yalign 0.76
    if localizaPad == "jardinPis" and padInvisible == False:
        add "padmini" xalign 0.88 yalign 0.76
    #ComodinJardinPis
    if localizaPeqComodin == "jardinPis" and hermanMayInvisible == False and demonBoy == True:
        add "peqminicomodin" xalign 0.95 yalign 0.81
    if localizaMedComodin == "jardinPis" and hermanMayInvisible == False and demonBoy == True:
        add "medminicomodin" xalign 0.92 yalign 0.81
    if localizaMayComodin == "jardinPis" and hermanMayInvisible == False and demonBoy == True:
        add "mayminicomodin" xalign 0.98 yalign 0.81
    if localizaMadComodin == "jardinPis" and madInvisible == False and demonBoy == True:
        add "madminicomodin" xalign 0.89 yalign 0.81


    #garage
    if localizaPeq == "garage" and hermanPeqInvisible == False:
        add "peqmini" xalign 0.6 yalign 0.814
    if localizaMed == "garage" and hermanMedInvisible == False:
        add "medmini" xalign 0.64 yalign 0.814
    if localizaMay == "garage" and hermanMayInvisible == False:
        add "maymini" xalign 0.68 yalign 0.814
    if localizaMad == "garage" and madInvisible == False:
        add "madmini" xalign 0.6 yalign 0.88
    if localizaPad == "garage" and padInvisible == False:
        add "padmini" xalign 0.68 yalign 0.88


    imagebutton:
        xalign 0.78 ypos 18
        idle "flechaRetminCuad"
        hover "flechaRetminCuadh"
        action [Hide ("getContentGenteCasa"),Call ("mostrarHud") ]
return
