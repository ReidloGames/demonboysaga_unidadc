screen ayudasJuego(tipoAcces):
    zorder 96
    add "movilhoriz" xalign 0.5 ypos 0.1
    viewport:
        xpos 170 ypos 120
        #xalign 0.51 ypos 190
        draggable True
        scrollbars "vertical"
        xmaximum 880
        ymaximum 400
        vbox:
            #JHON
            if escondeFrameAyudasPad == False:
                frame:
                    padding (20,10,20,10)
                    xpos 100
                    hbox:
                        spacing 20
                        xmaximum 750
                        if chicoPad == True:
                            add "padcarnetMini"
                        else:
                            add "censoredChiAyuda"
                        if padSentadoCamaHabPadr1raInteracLunes_visit1 == False:
                            text _("{color=#000000}{size=12}{b}[nombrePad]:{/b}{/size}{size=11} Saludalo antes de ir a la Universidad{/size}{/color}")




            #SOFIA
            if escondeFrameAyudasMad == False:
                frame:
                    padding (20,10,20,10)
                    xpos 100
                    hbox:
                        spacing 20
                        xmaximum 750
                        if chicaMad == True:
                            add "madcarnetMini"
                        else:
                            add "censoredChiAyuda"
                        if madCocina3raInteracLunes_visit1 == False:
                            text _("{color=#000000}{size=12}{b}[nombreMad]:{/b}{/size}{size=11} Habla con ella en la cocina, el lunes al mediodia{/size}{/color}")
                        if protaHablarConDirectorDinero == True:
                            text _("{color=#000000}{size=12}{b}[nombreMad]:{/b}{/size}{size=11} Habla con [nombreMad3] y con [nombrePad3] por la noche en su habitación sobre el problema del pago de la Universidad{/size}{/color}")
                        if demonBoy == True:
                            if madHabPadrEnPijama4taInteracMiercoles_visit1 == False:
                                text _("{color=#000000}{size=12}{b}[nombreMad]:{/b}{/size}{size=11} Visita a [nombreMad3] en su habitación, el miercoles o sábado por la tarde {/size}{/color}")


            #DANA
            if escondeFrameAyudasPeq == False:
                frame:
                    padding (20,10,20,10)
                    xpos 100
                    hbox:
                        spacing 20
                        xmaximum 750
                        if chicaPeq == True:
                            add "peqcarnetMini"
                        else:
                            add "censoredChiAyuda"
                        if peqGaleriaHaciendoGym3raInteracLunes_visit1 == False:
                            text _("{color=#000000}{size=12}{b}[nombrePeq]:{/b}{/size}{size=11} Visitala en la sala del gimnasio de casa, el lunes a tercera hora.{/size}{/color}")

            #ERICA
            if escondeFrameAyudasMed == False:
                frame:
                    padding (20,10,20,10)
                    xpos 100
                    hbox:
                        spacing 20
                        xmaximum 750
                        if chicaMed == True:
                            add "medcarnetMini"
                        else:
                            add "censoredChiAyuda"
                        if medComedor3raInteracMiercoles_visit1 == False:
                            text _("{color=#000000}{size=12}{b}[nombreMed]:{/b}{/size}{size=11} Visitala en el comedor, el miercoles a tercera hora.{/size}{/color}")

            #KARA
            if escondeFrameAyudasMay == False:
                frame:
                    padding (20,10,20,10)
                    xpos 100
                    hbox:
                        spacing 20
                        xmaximum 750
                        if chicaMay == True:
                            add "maycarnetMini"
                        else:
                            add "censoredChiAyuda"
                        if mayHabMayEnBragas1raInteracMartes_visit1 == False:
                            text _("{color=#000000}{size=12}{b}[nombreMay]:{/b}{/size}{size=11} Visitala en su habitación el martes a primera hora.{/size}{/color}")

            #LUCIA
            if escondeFrameAyudasLucia == False:
                frame:
                    padding (20,10,20,10)
                    xpos 100
                    hbox:
                        spacing 20
                        xmaximum 750
                        if chicaLucia == True:
                            add "luciacarnetMini"
                            text _("{color=#000000}{size=11}{b}[nombreLucia]:{/b} [ayudaMad1] {/size}{/color}")
                        else:
                            add "censoredChiAyuda"

            #CLAIRE
            if escondeFrameAyudasClaire == False:
                frame:
                    padding (20,10,20,10)
                    xpos 100
                    hbox:
                        spacing 20
                        xmaximum 750
                        if chicaClaire == True:
                            add "clairecarnetMini"
                        else:
                            add "censoredChiAyuda"
                        text _("{color=#000000}{size=11}{b}[nombreClaire]:{/b} [ayudaMad1] {/size}{/color}")

            #ALICIA
            if escondeFrameAyudasAlicia == False:
                frame:
                    padding (20,10,20,10)
                    xpos 100
                    hbox:
                        spacing 20
                        xmaximum 750
                        if chicaAlicia == True:
                            add "aliciacarnetMini"
                        else:
                            add "censoredChiAyuda"
                        if llegoTardeClaseAlicia == False:
                            text _("{color=#000000}{size=12}{b}[nombreAlicia]:{/b}{/size}{size=11} No llegues tarde a clase.{/size}{/color}")
                        if madCocina3raInteracLunes_visit1 == True and padSentadoCamaHabPadr1raInteracLunes_visit1 == True and peqGaleriaHaciendoGym3raInteracLunes_visit1 == True:
                            if mayHabMayEnBragas1raInteracMartes_visit1 == True and medComedor3raInteracMiercoles_visit1 == True:
                                text _("{color=#000000}{size=12}{b}[nombreAlicia]:{/b}{/size}{size=11} Haz clase con Alicia el martes, miercoles, jueves o viernes.{/size}{/color}")


    vbox:
        imagebutton:
            xpos 600 ypos 540
            idle "flechaRet"
            hover "flechaReth"
            if tipoAcces == "movil":
                action [Show ('opcionesMovil'),Hide("ayudasJuego")]
            if tipoAcces == "direct":
                action [Hide ("ayudasJuego"),Call("mostrarHud") ]
