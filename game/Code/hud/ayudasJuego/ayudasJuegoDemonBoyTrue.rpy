screen ayudasJuegoDemonBoyTrue(tipoAcces):
    zorder 96
    add "movilhoriz" xalign 0.5 ypos 0.1
    viewport:
        xpos 170 ypos 120
        #xalign 0.51 ypos 190
        draggable True
        scrollbars "vertical"
        xmaximum 880
        ymaximum 400
        vbox:
            #SOFIA
                frame:
                    xpos 100
                    vbox:
                        spacing 20
                        xmaximum 750



                        if madHabPadrEnPijama4taInteracMiercoles_visit1 == False:
                            text _(" {image=madcarnetMini} {color=#000000}{size=12}{b}[nombreMad]:{/b}{/size}{size=11} Visita a [nombreMad3] en {b} Su Habitación {/b}  {i} (el Miércoles o Sábado por la tarde) {/i} {/size}{/color} ")

                        if madHabPadrEnPijama4taInteracMiercoles_visit1 == True and preguntarComoEstaMad == False:
                            text _(" {image=madcarnetMini} {color=#000000}{size=12}{b}[nombreMad]:{/b}{/size}{size=11} Preguntale a [nombreMad3] como se encuentra, {b} en Su Habitación {/b} {i} (el Miércoles o Sábado por la tarde) {/i} {/size}{/color} ")


                        if eventInformarHipotecaBanco == False and preguntarComoEstaMad == True:
                            text _(" {image=madcarnetMini} {color=#000000}{size=12}{b}[nombreMad]:{/b}{/size}{size=11} Esperar acontecimientos... {/size}{/color} ")

                        if tienesTrabajoAmarzong == True and leccion1Massage == True and informarNuevoTrabajoMad == False:
                            text _(" {image=madcarnetMini} {color=#000000}{size=12}{b}[nombreMad]:{/b}{/size}{size=11} Infórmale de tu nuevo trabajo, en {b} en Su Habitación {/b}  {i}(el Miercoles o Sábado por la tarde) {/i}{/size}{/color} ")

                        if informarNuevoTrabajoMad == True:
                            #dar de mamar
                            if madCocinaDarMamar3raInteracJueves_visit1 == False:
                                text _(" {image=madcarnetMini} {color=#000000}{size=12}{b}[nombreMad]:{/b}{/size}{size=11} Investiga que hace [nombreMad3] en la {b} Cocina  {/b} {i} (el Jueves o el Domingo al Mediodía) {/i} {/size}{/color} ")
                            #limpieza
                            if madCocinaLimpia3raInteracMartes_visit1 == False:
                                text _(" {image=madcarnetMini} {color=#000000}{size=12}{b}[nombreMad]:{/b}{/size}{size=11} Visita a [nombreMad3] en la {b} Cocina{/b}   {i} (el Martes o Sábado al Mediodía) {/i} {/size}{/color} ")
                            #yoga
                            if madGaleriaHaciendoGym1raInteracMiercoles_visit1 == False:
                                text _(" {image=madcarnetMini} {color=#000000}{size=12}{b}[nombreMad]:{/b}{/size}{size=11} Observa como [nombreMad3] hace yoga en el {b} Almacen gimnasio {/b}  {i} (el Miercoles o Sábado al Amanecer) {/i}  {/size}{/color} ")
                        #fregarPlatosMAd
                        #if madCocinaPideFregar1raInteracDomingo_visit1 == False and madCocinaDarMamar3raInteracJueves_visit1 == True:
                        #    text _(" {image=madcarnetMini} {color=#000000}{size=12}{b}[nombreMad]:{/b}{/size}{size=11} Friega los platos en la {b}  Cocina el Jueves o Domingo a 1ra hora {/b} {/size}{/color} ")
                        if madCocinaDarMamar3raInteracJueves_visit1 == True and madCocinaLimpia3raInteracMartes_visit1 == True and madGaleriaHaciendoGym1raInteracMiercoles_visit1 == True and informarNuevoTrabajoMad == True:
                            if leccion1Massage == True and madSalonSentarteConMadRevistaMasajePies == False:
                                text _(" {image=madcarnetMini} {color=#000000}{size=12}{b}[nombreMad]:{/b}{/size}{size=11} Dar un masaje de pies a [nombreMad3]. {b} Salón {/b}  {i} (Lunes o Martes por la Tarde o Jueves al Mediodía) {/i}  {/size}{/color} ")



            #DANA
                frame:
                    #padding (20,10,20,10)
                    xpos 100
                    hbox:
                        spacing 20
                        xmaximum 750

                        if demonBoy == True and preguntarCocinaComoEstaTemaPad == False:
                            text _(" {image=peqcarnetMini} {color=#000000}{size=12}{b}[nombrePeq]:{/b}{/size}{size=11} Habla con ella sobre la situación de casa. {b} Cocina {/b}  {i} (Miercoles al mediodía o Domingo por la mañana) {/i} {/size}{/color}")

                        if animarHermanPeqSuHabitacion == True and hasHabladoJugarConConsola == False:
                            text _(" {image=peqcarnetMini} {color=#000000}{size=12}{b}[nombrePeq]:{/b}{/size}{size=11} Habla con ella cuando esté haciendo gimnasia. {b} Gym {/b}  {i} (Lunes o Jueves al mediodía) {/i} {/size}{/color}")

                        if hasHabladoConPeqEnElColeTrabajosAcabados == True and eventDiarioPersonalPeq == False:
                            text _(" {image=peqcarnetMini} {color=#000000}{size=12}{b}[nombrePeq]:{/b}{/size}{size=11} Observa que hace. {b} Su Habitación {/b}  {i} {/i} {/size}{/color}")

                        if eventDiarioPersonalPeq == True:
                            if peqSuHabitacionMirandoEspejo2daInteracSabados_visit1__ == False:
                                 text _(" {image=peqcarnetMini} {color=#000000}{size=12}{b}[nombrePeq]:{/b}{/size}{size=11} ¿Que hace? {b} Su Habitación {/b}  {i} (Sabado o Domingo a 2nda hora) {/i} {/size}{/color}")


        #ERICA

                frame:
                    #padding (20,10,20,10)
                    xpos 100
                    vbox:
                        spacing 20
                        xmaximum 750

                        if conversMedJardinTomaSol2daSabado_Hablado == False:
                            text _(" {image=medcarnetMini} {color=#000000}{size=12}{b}[nombreMed]:{/b}{/size}{size=11} Visítala en la {b} Piscina {/b}  {i}(el Miércoles al Amanecer o el Sábado por la Mañana){/i} {/size}{/color} ")
                        if conversMedJardinTomaSol2daSabado_HabladoVisit2__ == False and conversMedJardinTomaSol2daSabado_Hablado == True:
                            text _(" {image=medcarnetMini} {color=#000000}{size=12}{b}[nombreMed]:{/b}{/size}{size=11} Visítala en la {b} Piscina {/b}  {i}(el Miércoles al Amanecer o el Sábado por la Mañana){/i}  {/size}{/color} ")

                        if medComedor3raInteracMiercoles_visit2__ == False:
                            text _(" {image=medcarnetMini} {color=#000000}{size=12}{b}[nombreMed]:{/b}{/size}{size=11} Habla con ella en el {b} Comedor {/b}  {i} (el Miércoles o Viernes al Mediodía) {/i} {/size}{/color} ")

                        if medComedor3raInteracMiercoles_visit2__ == True and leccion1Massage == True and eventErikaSeVaDeFiesta == False:
                            text _(" {image=medcarnetMini} {color=#000000}{size=12}{b}[nombreMed]:{/b}{/size}{size=11} Erica se va? {b} Comedor {/b}  {i} (el Viernes por la Tarde) {/i} {/size}{/color} ")

#cambiandoseMayEscena1
        #KARA

                frame:
                    xpos 100
                    vbox:
                        spacing 20
                        xmaximum 750

                        if cambiandoseMayEscena1 == True or cambiandoseMayEscena2 == True or cambiandoseMayEscena3 == True:
                            if pensarEnKaraPaja == False:
                                text _(" {image=maycarnetMini} {color=#000000}{size=12}{b}[nombreMay]:{/b}{/size}{size=11} Mirar televisión en el {b} Salón {/b} {/size}{/color} ")
                        if leccion1Massage == True:
                            if preguntaComoEstaKaraCocinaCafe == False:
                                text _(" {image=maycarnetMini} {color=#000000}{size=12}{b}[nombreMay]:{/b}{/size}{size=11} Habla con ella {b} Cocina {/b}  {i}(el Miércoles o Sábado al Amanecer){/i} {/size}{/color} ")
                        if preguntaComoEstaKaraCocinaCafe == True:
                            if eventMaySeHaDejadoLLavesTienda == False:
                                text _(" {image=maycarnetMini} {color=#000000}{size=12}{b}[nombreMay]:{/b}{/size}{size=11} Esperar acontecimientos... {/size}{/color} ")
                        if hasEntradoDeNocheSuHabitacion == True and tengoPastillasDormir == False:
                            text _(" {image=maycarnetMini} {color=#000000}{size=12}{b}[nombreMay]:{/b}{/size}{size=11} Comprar pastillas para dormir {/size}{/color} ")

            #LUCIA
        #    if escondeFrameAyudasLucia == False:
        #        frame:
        #            padding (20,10,20,10)
        #            xpos 100
        #            hbox:
        #                spacing 20
        #                xmaximum 750
        #                if chicaLucia == True:
        #                    add "luciacarnetMini"
        #                    text _("{color=#000000}{size=11}{b}[nombreLucia]:{/b} [ayudaMad1] {/size}{/color}")
        #                else:
        #                    add "censoredChiAyuda"

            #CLAIRE
        #    if escondeFrameAyudasClaire == False:
        #        frame:
        #            padding (20,10,20,10)
        #            xpos 100
        #            hbox:
        #                spacing 20
        #                xmaximum 750
        #                if chicaClaire == True:
        #                    add "clairecarnetMini"
        #                else:
        #                    add "censoredChiAyuda"
        #                text _("{color=#000000}{size=11}{b}[nombreClaire]:{/b} [ayudaMad1] {/size}{/color}")

            #ALICIA
        #    if escondeFrameAyudasAlicia == False:
        #        frame:
        #            padding (20,10,20,10)
        #            xpos 100
        #            hbox:
        #                spacing 20
        #                xmaximum 750
        #                if chicaAlicia == True:
        #                    add "aliciacarnetMini"
        #                else:
        #                    add "censoredChiAyuda"
        #                if llegoTardeClaseAlicia == False:
        #                    text _("{color=#000000}{size=12}{b}[nombreAlicia]:{/b}{/size}{size=11} No llegues tarde a clase.{/size}{/color}")
        #                if madCocina3raInteracLunes_visit1 == True and padSentadoCamaHabPadr1raInteracLunes_visit1 == True and peqGaleriaHaciendoGym3raInteracLunes_visit1 == True:
        #                    if mayHabMayEnBragas1raInteracMartes_visit1 == True and medComedor3raInteracMiercoles_visit1 == True:
        #                        text _("{color=#000000}{size=12}{b}[nombreAlicia]:{/b}{/size}{size=11} Haz clase con Alicia el martes, miercoles, jueves o viernes.{/size}{/color}")


    vbox:
        imagebutton:
            xpos 600 ypos 540
            idle "flechaRet"
            hover "flechaReth"
            if tipoAcces == "movil":
                action [Show ('opcionesMovil'),Hide("ayudasJuegoDemonBoyTrue")]
            if tipoAcces == "direct":
                action [Hide ("ayudasJuegoDemonBoyTrue"),Call("mostrarHud") ]
