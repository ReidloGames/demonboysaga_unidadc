
screen protaStatsScreen():
    zorder 96
    add "movil" xalign 0.5 ypos 100

    vbox:
        imagemap:
            xpos 595 ypos 155
            ground "protaCarnetBig"

        text _("Dias totales: [numDiaTotal]"):
            xpos 600 ypos 150
            size 12

        text _("Nombre: [nombreProta]"):
            xpos 550 ypos 170
            size 17

        text _("Carisma: [carisma]"):
            xpos 550 ypos 190
            size 15
        text _("Intelecto: [intelecto]"):
            xpos 550 ypos 200
            size 15
        text _("Físico: [fisico]"):
            xpos 550 ypos 210
            size 15

        #imagebutton:
        #    xpos 582 ypos 355
        #    idle "botonInfoActions"
        #    hover "botonInfoActions_S"
        #    action [Hide("protaStatsScreen"),Show("infoActionRealizadoProta")]

        imagebutton:
            xpos 582 ypos 365
            idle "flechaRetmin"
            hover "flechaRetminh"
            action [Hide("protaStatsScreen"),Show("opcionesMovil")]
return
