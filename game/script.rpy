
# Coloca el código de tu juego en este archivo.

define days = ["Lunes","Martes","Miercoles","Jueves","Viernes","Sabado","Domingo"]


call rutaImagenesGame() from _call_rutaImagenesGame
call splashscreen() from _call_splashscreen

init:

    call configGameVisual from _call_configGameVisual
    call variables from _call_variables
    call personajes from _call_personajes




python:
    if renpy.loadable("parche.rpy"):
        config.label_overrides["historiaOpening"] = "historiaOpening_p"




label splashscreen:
    python:
        _preferences.set_volume('music', 0.2)
        _preferences.set_volume('sfx', 1)
    image adultImage1 = "images/adultImage1.png"
    image adultImage2 = "images/adultImage2.png"
    image adultImage3 = "images/adultImage3.png"
    show adultImage1 with dissolve
    pause
    show adultImage2 with dissolve
    pause
    show adultImage3 with dissolve
    pause
    scene fondoNegro
    "{size=16}-If you change the language, close the game and run it again.\n-If you come from version 0.1 start a new game. \n\n-Si cambias el idioma, cierra el juego y ejecutalo de nuevo.\n-Si vienes de la versión 0.1 empieza una partida de nuevo.{/size}"
    pause
return

init -3 python:
    if persistent.lang is None:
        persistent.lang = "spanish"
    if persistent.lang is "english":
        persistent.lang = "english"


    lang = persistent.lang



label start:
    $ cont = 0 #continue variable
    $ arr_keys = ["a", "c", "e", "K_UP", "K_SPACE"] #list of keyboard inputs to be selected from. See https://www.pygame.org/docs/ref/key.html for more keys

    stop music fadeout 1.0
    call openingTuto() from _call_openingTuto
    call openingIntro() from _call_openingIntro

    show screen hudPlantilla()
    show screen diaHud()
    show screen movilHud()
    show screen diaImageHud()
    show screen mochilaHud()
    show screen dineroHud()
    show screen destinoHud()
    show screen genteCasaHud()
    show screen ayudasDirectHud()
    show screen casaVolverHabitacion()




    while gameRunable == True:
        if renpy.loadable("parche.rpy"):
            call historiaOpening_p from _call_historiaOpening_p_1 #patx


        call escenariosGame() from _call_escenariosGame_1
